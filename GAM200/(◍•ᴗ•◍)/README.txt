/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
Project: gam200_vertical_slice
Author: AvocadoGames
Creation date: 12/14/2020
----------------------------------------------------------------------------------------------------------*/
•	Player controls with keyboard:
	WADS: Move the player
	Space: Jump and double jump
	Left click: Light attack
	Right click: Heavy attack
	LShift: Dash
	Q: Blaster(not working, just visual)
	E: Shield

•	Player controls with keyboard:
	Left joystick: Movement
	A button: Jump and double jump
	X button: Light attack
	Y button: Heavy attack
	B button: Dash
	Left low trigger: Blaster(not working, just visual)
	Right low trigger: Shield

•	Purification:
	So as to be triggered, the player must be colliding with the enemy that activated that state.
	There will be some icons that look like joystick positions but are triggered with the d-pad.
•	Editor:
	Press LeftCtrl+F8 to open the editor.

IMPORTANT: 
Inmortality is activated by default, the player can lose health points but will never die.

If the 	(◍•ᴗ•◍) project does not compile it might be because of Spine. Simply unload (◍•ᴗ•◍) and compile the spine projects. 
They will by default put the output in the correct folder to compile. It must also be mentioned that those .lib and .dll files must be put in the .exe folder to execute the project outside visual studio.

There are two gam200fa20_AvocadoGames_vertical_slice.zip files. One in the project folder and another one in the Bin/x64/Release folder. The first one contains the data folder itself and the other one contains a full build of the game.
Extract gam200fa20_AvocadoGames_vertical_slice.zip into the project folder to get the correct data folder. If uncompressing in Bin/x64/Release folder overrite everything inside that folder.

The project can only be build in x64.
