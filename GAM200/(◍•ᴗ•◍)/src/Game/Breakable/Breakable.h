#pragma once
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"

class Breakable : public LogicComponent
{
	SM_RTTI_DECL(Breakable, LogicComponent);
public:
	Breakable();

	void Initialize() override;
	void Update() override;
private:
	ShotReceiver* shot;
};
