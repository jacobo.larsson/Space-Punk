#include "../../GameObject/GameObject.h"
#include "../../Space/Space.h"
#include "../ShotReceiver/ShotReceiver.h"
#include "Breakable.h"

Breakable::Breakable()
{
	SetName("Breakable");
}

void Breakable::Initialize()
{
	LogicComponent::Initialize();
	shot = mOwner->GetComp<ShotReceiver>();
}

void Breakable::Update()
{
	if (shot)
	{
		if (shot->AmIShot())
		{
			GetOwner()->GetParentSpace()->DestroyObject(mOwner);
			

		}
	}
}
