#include <spine/spine.h>
#include <spine/Extension.h>

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Archetype/ArchetypeFunctions.h"
#include "../../Physics/RigidBody.h"
#include "../../Game/Health/Health.h"
#include "../../Animation/SM_Animation.h"
#include "../Health/Health.h"
#include "TurretEnemySystem.h"
#include "TurretEnemyStates.h"
#include "../../Time/Time.h"
#include "../../RayCast/RayCastTest.h"
#include <glm/gtx/vector_angle.hpp>

#pragma region TurretEnemyCheckForPlayer
TurretEnemyCheckForPlayer::TurretEnemyCheckForPlayer(const char* name) : State(name)
{
	
}

void TurretEnemyCheckForPlayer::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyCheckForPlayer::LogicEnter()
{
	OwnerTransform = castedMachine->ShootingPoint;
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Idle")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Idle", true);
	mTimeInState = 0;
}

void TurretEnemyCheckForPlayer::LogicUpdate()
{
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
	{ 
		PlayerTransform = glm::vec2(castedMachine->pTransform->GetWorldPosition().x, castedMachine->pTransform->GetWorldPosition().y);
		PlayerTransform.x += castedMachine->pCollider->GetCollideBox().GetPosition().x + castedMachine->pCollider->GetCollideBox().GetScale().x/2;
		PlayerTransform.y += castedMachine->pCollider->GetCollideBox().GetPosition().y;
		Direction = glm::normalize(PlayerTransform - OwnerTransform);
		float angle = (atan(Direction.y / Direction.x) * 180.0f / 3.1415f) + 90 * cos(mActor->GetComp<TransformComponent>()->GetOrientation());
		if (PlayerTransform.x > mActor->GetComp<TransformComponent>()->GetWorldPosition().x)
			angle += 180;
		castedMachine->mBack_Piece->getData().setRotation(angle);
		if (abs(glm::distance(PlayerTransform, OwnerTransform)) < castedMachine->Range && mTimeInState > castedMachine->FireRate)
		{
			DistanceWithRayCollision = CheckRayWithVector(OwnerTransform, Direction, castedMachine->Range, &RaycastObject, &RayCollisionPoint, castedMachine->mPlatforms);

			float Dist1 = glm::distance(PlayerTransform, OwnerTransform);
			float Dist2 = Dist1;
			if (RaycastObject != nullptr)
				Dist2 = glm::distance(glm::vec2(RaycastObject->GetComp<TransformComponent>()->GetPosition().x, RaycastObject->GetComp<TransformComponent>()->GetPosition().y), OwnerTransform);
			if (Dist1 <= Dist2 || DistanceWithRayCollision == -1)
			{
				mOwnerStateMachine->ChangeState("TurretEnemyShoot");
			}
		}
	}
}

void TurretEnemyCheckForPlayer::LogicExit() {}
#pragma endregion

#pragma region TurretEnemyShoot
TurretEnemyShoot::TurretEnemyShoot(const char* name) : State(name), mLastBulletShot(nullptr)
{
}

void TurretEnemyShoot::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyShoot::LogicEnter()
{
	OwnerTransform = castedMachine->ShootingPoint;
	mTimeInState = 0;
	BulletTime = 0;
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Attack")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Attack");
}

void TurretEnemyShoot::LogicUpdate()
{
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
	{
		PlayerTransform = glm::vec2(castedMachine->pTransform->GetWorldPosition().x, castedMachine->pTransform->GetWorldPosition().y);
		PlayerTransform.x += castedMachine->pCollider->GetCollideBox().GetPosition().x + castedMachine->pCollider->GetCollideBox().GetScale().x / 2;
		PlayerTransform.y += castedMachine->pCollider->GetCollideBox().GetPosition().y;
		Direction = glm::normalize(PlayerTransform - OwnerTransform);
		float angle = (atan(Direction.y / Direction.x) * 180.0f / 3.1415f) + 90 * cos(mActor->GetComp<TransformComponent>()->GetOrientation());
		if (PlayerTransform.x > mActor->GetComp<TransformComponent>()->GetWorldPosition().x)
			angle += 180;
		castedMachine->mBack_Piece->getData().setRotation(angle);

		if (mTimeInState >= castedMachine->mySkelRenderable->GetAnimDuration() - 0.15f)
		{
			castedMachine->mBullets[castedMachine->last_used_bullet]->SetEnabled(true);
			mLastBulletShot = castedMachine->mBullets[castedMachine->last_used_bullet];
			if (castedMachine->last_used_bullet < 4)
				castedMachine->last_used_bullet++;
			else
				castedMachine->last_used_bullet = 0;
			mLastBulletShot->GetComp<RigidBody>()->Velocity = castedMachine->ShootSpeed * Direction;
			mLastBulletShot->GetComp<TransformComponent>()->SetPosition(glm::vec3(OwnerTransform.x, OwnerTransform.y, 0.0f));
			castedMachine->Ammo--;


			if (castedMachine->Ammo > 2)
			{
				mOwnerStateMachine->ChangeState("TurretEnemyCheckForPlayer");
			}
			else
				mOwnerStateMachine->ChangeState("TurretEnemyShootCone");
		}
	}
}

void TurretEnemyShoot::LogicExit() {}
#pragma endregion

#pragma region TurretEnemyReload
TurretEnemyReload::TurretEnemyReload(const char* name) : State(name)
{
}

void TurretEnemyReload::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyReload::LogicEnter()
{
	OwnerTransform = castedMachine->ShootingPoint;
	mTimeInState = 0;
	castedMachine->last_used_bullet = 0;
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Idle")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Idle", true);
}

void TurretEnemyReload::LogicUpdate()
{
	PlayerTransform = glm::vec2(castedMachine->pTransform->GetWorldPosition().x, castedMachine->pTransform->GetWorldPosition().y);
	PlayerTransform.x += castedMachine->pCollider->GetCollideBox().GetPosition().x + castedMachine->pCollider->GetCollideBox().GetScale().x / 2;
	PlayerTransform.y += castedMachine->pCollider->GetCollideBox().GetPosition().y;
	Direction = glm::normalize(PlayerTransform - OwnerTransform);
	float angle = (atan(Direction.y / Direction.x) * 180.0f / 3.1415f) + 90 * cos(mActor->GetComp<TransformComponent>()->GetOrientation());
	if (PlayerTransform.x > mActor->GetComp<TransformComponent>()->GetWorldPosition().x)
		angle += 180;
	castedMachine->mBack_Piece->getData().setRotation(angle);
	if (mTimeInState > castedMachine->ReloadTime)
	{
		castedMachine->Ammo = castedMachine->maxbullets;
		mOwnerStateMachine->ChangeState("TurretEnemyCheckForPlayer");
	}
}

void TurretEnemyReload::LogicExit() {}
#pragma endregion

#pragma region TurretEnemyDie
TurretEnemyDie::TurretEnemyDie(const char* name) : State(name) {}

void TurretEnemyDie::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyDie::LogicEnter()
{
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Death")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Death", true);
	mTimeInState = 0;
}

void TurretEnemyDie::LogicUpdate()
{
	if (mTimeInState > castedMachine->mySkelRenderable->GetCurrentAnimation()->getDuration())
		SceneSys->GetMainSpace()->DestroyObject(mActor);
}

void TurretEnemyDie::LogicExit() {}
#pragma endregion

#pragma region TurretEnemyReset

TurretEnemyReset::TurretEnemyReset(const char* name) : State(name)
{
}

void TurretEnemyReset::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyReset::LogicEnter()
{
	mTimeInState = 0;
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Damage")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Damage", true);

	mActor->GetComp<Health>()->Damage(castedMachine->mHDamage);

	if (mActor->GetComp<Health>()->GetHealth() <= 0.0f) {

		if ((rand() % 101) > (100 - castedMachine->purif_prob))
			mOwnerStateMachine->ChangeState("PurificationState");
		else
			mOwnerStateMachine->ChangeState("TurretEnemyDie");
	}
}

void TurretEnemyReset::LogicUpdate()
{
	if(mTimeInState > castedMachine->mySkelRenderable->GetCurrentAnimation()->getDuration())
		castedMachine->ChangeState("TurretEnemyCheckForPlayer");
}

void TurretEnemyReset::LogicExit()
{
	if (castedMachine != nullptr) {
		castedMachine->Shot = false;
		castedMachine->mHDamage = 0;
		castedMachine->mHResetTime = 0;

	}
}
#pragma endregion

#pragma region TurretEnemyBurst
TurretEnemyBurst::TurretEnemyBurst(const char* name) : State(name)
{
}

void TurretEnemyBurst::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyBurst::LogicEnter()
{
	OwnerTransform = castedMachine->ShootingPoint;
	mTimeInState = 0;
	if (castedMachine->mySkelRenderable->GetCurrentAnimationName() != "Turret_Attack")
		castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Attack");
}

void TurretEnemyBurst::LogicUpdate()
{
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
	{
		PlayerTransform = glm::vec2(castedMachine->pTransform->GetWorldPosition().x, castedMachine->pTransform->GetWorldPosition().y);
		PlayerTransform.x += castedMachine->pCollider->GetCollideBox().GetPosition().x + castedMachine->pCollider->GetCollideBox().GetScale().x / 2;
		PlayerTransform.y += castedMachine->pCollider->GetCollideBox().GetPosition().y;
		Direction = glm::normalize(PlayerTransform - OwnerTransform);
		float angle = (atan(Direction.y / Direction.x) * 180.0f / 3.1415f) + 90 * cos(mActor->GetComp<TransformComponent>()->GetOrientation());
		if (PlayerTransform.x > mActor->GetComp<TransformComponent>()->GetWorldPosition().x)
			angle += 180;
		castedMachine->mBack_Piece->getData().setRotation(angle);

		if (mTimeInState >= castedMachine->mySkelRenderable->GetAnimDuration() - 0.18)
		{
			if (mTimeInState >= castedMachine->BurstRate)
			{

				castedMachine->mBullets[castedMachine->last_used_bullet]->SetEnabled(true);
				mLastBulletShot = castedMachine->mBullets[castedMachine->last_used_bullet];
				if (castedMachine->last_used_bullet < 4)
					castedMachine->last_used_bullet++;
				else
					castedMachine->last_used_bullet = 0;
				mLastBulletShot->GetComp<RigidBody>()->Velocity = castedMachine->ShootSpeed * Direction;
				mLastBulletShot->GetComp<TransformComponent>()->SetPosition(glm::vec3(OwnerTransform.x, OwnerTransform.y, 0.0f));
				castedMachine->Ammo--;

				if (castedMachine->Ammo > 0)
					mOwnerStateMachine->ChangeState("TurretEnemyShoot");
				else
					mOwnerStateMachine->ChangeState("TurretEnemyReload");
			}
		}
	}
}

void TurretEnemyBurst::LogicExit()
{
}
#pragma endregion


#pragma region TurretEnemyShootCone
TurretEnemyShootCone::TurretEnemyShootCone(const char* name) : State(name)
{

}

void TurretEnemyShootCone::Initialize()
{
	castedMachine = dynamic_cast<TurretEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void TurretEnemyShootCone::LogicEnter()
{
	OwnerTransform = castedMachine->ShootingPoint;
	mTimeInState = 0;
	castedMachine->mySkelRenderable->SetCurrentAnimation("Turret_Attack");
}

void TurretEnemyShootCone::LogicUpdate()
{
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
	{
		PlayerTransform = glm::vec2(castedMachine->pTransform->GetWorldPosition().x, castedMachine->pTransform->GetWorldPosition().y);
		PlayerTransform.x += castedMachine->pCollider->GetCollideBox().GetPosition().x + castedMachine->pCollider->GetCollideBox().GetScale().x / 2;
		PlayerTransform.y += castedMachine->pCollider->GetCollideBox().GetPosition().y;
		Direction = glm::normalize(PlayerTransform - OwnerTransform);
		float angle = (atan(Direction.y / Direction.x) * 180.0f / 3.1415f) + 90 * cos(mActor->GetComp<TransformComponent>()->GetOrientation());
		if (PlayerTransform.x > mActor->GetComp<TransformComponent>()->GetWorldPosition().x)
			angle += 180;
		castedMachine->mBack_Piece->getData().setRotation(angle);


		if (mTimeInState >= castedMachine->mySkelRenderable->GetAnimDuration() - 0.18)
		{
			for (unsigned i = 0; i < 3; i++)
			{
				castedMachine->mBullets[castedMachine->last_used_bullet]->SetEnabled(true);
				mLastBulletShot = castedMachine->mBullets[castedMachine->last_used_bullet];
				if (castedMachine->last_used_bullet < 4)
					castedMachine->last_used_bullet++;
				else
					castedMachine->last_used_bullet = 0;
				mLastBulletShot->GetComp<RigidBody>()->Velocity = castedMachine->ShootSpeed * Direction;
				if (i == 1)
				{
					mLastBulletShot->GetComp<RigidBody>()->Velocity.y *= sin(0.15f);
				}
				else if (i == 2)
				{
					mLastBulletShot->GetComp<RigidBody>()->Velocity.y *= sin(-0.94f);
				}
				mLastBulletShot->GetComp<TransformComponent>()->SetPosition(glm::vec3(OwnerTransform.x, OwnerTransform.y, 0.0f));
			}
			mOwnerStateMachine->ChangeState("TurretEnemyReload");
		}
	}
}

void TurretEnemyShootCone::LogicExit()
{

}
#pragma endregion