#include <iostream>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Factory/Factory.h"
#include "../../Time/Time.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../Health/Health.h"
#include "../../Physics/RigidBody.h"
#include "BulletTimer.h"


BulletTimer::BulletTimer()
{
	SetName("BulletTimer");
	mTimeExisting = 0.0f;
}

void BulletTimer::Initialize()
{
	LogicComponent::Initialize();
	for (unsigned i = 0; i < mGasExplosion.size(); i++)
	{
		mGasExplosion[i] = CreateFromArchetype("GasExplosion", mOwner->GetParentSpace(), mOwner->GetComp<TransformComponent>()->GetWorldPosition());
		mGasExplosion[i]->SetEnabled(false);
	}
}

void BulletTimer::Update()
{
	if(mOwner->Enabled())
		mTimeExisting += (float)TimeSys->GetFrameTime();

	if (mTimeExisting >= mLifeTime)
	{
		if (turretactor)
		{
			
			mOwner->SetEnabled(false);
			mOwnerTrans->SetWorldPosition(pos);
			mRigidBody->Velocity = glm::vec2();
			mTimeExisting = 0.0f;
		}
		else
		{
			GetOwner()->GetParentSpace()->DestroyObject(mOwner);

		}
	}
}

void BulletTimer::Shutdown()
{
	LogicComponent::Shutdown();
}

bool BulletTimer::Edit()
{
	ImGui::DragFloat("mLifeTime", &mLifeTime, 1.0f, 0.0f, 10.0f);
	return false;
}

void BulletTimer::OnCollisionStarted(GameObject* other)
{
	if (!other->CompareTag("Player"))
	{
		for (unsigned i = 0; i < mGasExplosion.size(); i++)
		{
			mGasExplosion[i]->GetComp<TransformComponent>()->SetWorldPosition(mOwner->GetComp<TransformComponent>()->GetWorldPosition());
			mGasExplosion[i]->SetEnabled(true);
		}
	}
	
	if (turretactor)
	{
		mOwner->SetEnabled(false);
		mOwnerTrans->SetWorldPosition(pos);
		mRigidBody->Velocity = glm::vec2();
		mTimeExisting = 0.0f;
	}
	else
	{
		GetOwner()->GetParentSpace()->DestroyObject(mOwner);
	}
}

BulletTimer* BulletTimer::Clone()
{
	BulletTimer* temp = FactorySys->Create<BulletTimer>();
	(*temp) = (*this);
	return temp;
}

BulletTimer& BulletTimer::operator=(const BulletTimer& rhs)
{
	mTimeExisting = rhs.mTimeExisting;
	
	mLifeTime = rhs.mLifeTime;

	return (*this);
}

void BulletTimer::SecondInit()
{
	
	TurretEnemyMachine* turretmachine;
	
	turretmachine = reinterpret_cast<TurretEnemyMachine*>(turretactor->GetComp<BrainComponent>()->GetStateMachine("TurretEnemyMachine"));
	
	mOwnerTrans = mOwner->GetComp<TransformComponent>();
	mRigidBody = mOwner->GetComp<RigidBody>();
}

void BulletTimer::ToJson(nlohmann::json& j)
{

	j["mLifeTime"] = mLifeTime;
}

void BulletTimer::FromJson(const nlohmann::json& j)
{
	if (j.find("mLifeTime") != j.end())
		mLifeTime = j["mLifeTime"];
}
