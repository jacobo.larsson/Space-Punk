#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Time/Time.h"
#include "../../Collisions/Collisions.h"
#include "../../Collisions/ICollider.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Input/Input.h"
#include "../../Factory/Factory.h"
#include "../../Game/Purification/Purification.h"

#include "../TurretEnemy/BulletTimer.h"
#include "../TurretEnemy/TurretEnemyStates.h"
#include "../TurretEnemy/TurretEnemySystem.h"

#include "../Hitbox/Hitbox.h"

TurretEnemyMachine::TurretEnemyMachine()
{
	mName = "TurretEnemyMachine";
	
	mInitialState = new TurretEnemyCheckForPlayer("TurretEnemyCheckForPlayer");
	mCurrentState = mInitialState;
	AddState(mInitialState);

	AddState(new TurretEnemyReload("TurretEnemyReload"));
	AddState(new TurretEnemyDie("TurretEnemyDie"));
	AddState(new TurretEnemyReset("TurretEnemyReset"));
	AddState(new PurificationState("PurificationState"));
}

TurretEnemyMachine::~TurretEnemyMachine() { StateMachine::~StateMachine(); }

void TurretEnemyMachine::Initialize()
{
	if (ArenaMode == false)
	{
		AddState(new TurretEnemyShoot("TurretEnemyShoot"));
		AddState(new TurretEnemyShootCone("TurretEnemyShootCone"));
	}
	else
		AddState(new TurretEnemyBurst("TurretEnemyShoot"));

	mOwnerTransform = mActor->GetComp<TransformComponent>();
	if (mOwnerTransform == nullptr)
	{
		std::cout << mActor->GetName() << " missing  transform component" << std::endl;
		return;
	}
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
	{
		pCollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>();
		pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	}
	if (pTransform == nullptr)
	{
		std::cout << "Player missing transform component" << std::endl;
		return;
	}
	mySkelRenderable = mActor->GetComp<SkeletonRenderable>();
	if (mySkelRenderable == nullptr)
	{
		std::cout << "Missing skeleton renderable" << std::endl;
		return;
	}
	mBack_Piece = mySkelRenderable->mSkeleton->findBone("Flower_Back_Piece");

	ShootingPoint = mActor->GetComp<TransformComponent>()->GetPosition();
	ShootingPoint.x += ShootingOffset.x;
	ShootingPoint.y += ShootingOffset.y;

	for (unsigned i = 0; i < 5; i++)
	{
		mBullets[i] = CreateFromArchetype("Bullet", mActor->GetParentSpace(), ShootingPoint);
		mBullets[i]->GetComp<BulletTimer>()->turretactor = GetActor();
		mBullets[i]->GetComp<BulletTimer>()->SecondInit();
		mBullets[i]->SetEnabled(false);
		mBullets[i]->GetComp<BulletTimer>()->pos = ShootingPoint;
	}
	mOwnerHeight = mOwnerTransform->GetWorldScale().y;
	mOwnerWidth = mOwnerTransform->GetWorldScale().x;
	Range = abs(Range);//We force the range to be positive
	BurstRate = abs(BurstRate);//We force the BurstRate to be positive
	Range += mOwnerWidth;
	if(ArenaMode == true)
		Ammo = 3;
	maxbullets = Ammo;//With this we are storing the maximum amount of bullets for later use so that if is changed in the editor it will get also changed

	std::for_each(mActor->GetParentSpace()->GetComponents<Collider>().begin(), mActor->GetParentSpace()->GetComponents<Collider>().end(), [&](auto& it)
	{
		Collider* myCol = reinterpret_cast<Collider*>(it);

		glm::vec3 ObbPos = myCol->GetCollideBox().GetWorldPosition();
		glm::vec3 ObbScale = myCol->GetCollideBox().GetScale();
		float ObbRot = myCol->GetCollideBox().GetOrientation();

		if (OrientedRectToStaticCircle(ObbPos, ObbScale.x, ObbScale.y, ObbRot, mActor->GetComp<TransformComponent>()->GetWorldPosition(), Range))
		{
			if(myCol->GetOwner()->CompareTag("Platform") || myCol->GetOwner()->CompareTag("Wall"))
				mPlatforms.push_back(myCol->GetOwner());
		}
	});

	mySkelRenderable->SetMixValue("Turret_Idle", "Turret_Attack", 0.8);
	mySkelRenderable->SetMixValue("Turret_Idle", "Turret_Damage", 0.4);
	mySkelRenderable->SetMixValue("Turret_Idle", "Turret_Death", 0.7);
	mySkelRenderable->SetMixValue("Turret_Attack", "Turret_Idle", 0.2);


	StateMachine::Initialize();
}

bool TurretEnemyMachine::Edit()
{
	bool changed = false;
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		ImGui::Checkbox("ArenaMode", &ArenaMode);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragFloat("ShootSpeed", &ShootSpeed);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragFloat("ReloadTime", &ReloadTime);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragInt("Ammo", &Ammo);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragFloat("FireRate", &FireRate);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;		
		ImGui::DragFloat("BurstRate", &BurstRate);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			BurstRate = abs(BurstRate);//This will force the range to always have positive values
			changed = true;
		}
		ImGui::DragFloat("Range", &Range);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			Range = abs(Range);//This will force the range to always have positive values
			changed = true;
		}
		ImGui::DragFloat("XOffset", &ShootingOffset.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragFloat("YOffset", &ShootingOffset.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::DragInt("PurificationProb", &purif_prob);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	Space* _Space = mActor->GetParentSpace();
	if (_Space != nullptr)
	{
		if (SettingsSys->debug_draw)
		{
			_Space->DrawCircle(mActor->GetComp<TransformComponent>()->GetPosition(), Range, Colors::red);
			_Space->DrawCircle(glm::vec2(ShootingOffset.x + mActor->GetComp<TransformComponent>()->GetPosition().x, ShootingOffset.y + mActor->GetComp<TransformComponent>()->GetPosition().y), 5, Colors::green);
		}

	}
	return changed;
}

void TurretEnemyMachine::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Hitbox") && GetCurrentStateName() != "TurretEnemyDie" && GetCurrentStateName()!="PurificationState")
	{
		if (Hitbox* hbox = other->GetComp<Hitbox>()) {
			if (!hbox->mEnemy) { //if it is a player hitbox
				mHDamage = hbox->mDamage;
				mHResetTime = hbox->mKTime;
			}
			
		 }
		ChangeState("TurretEnemyReset");
	}
}

void TurretEnemyMachine::ResetBullets()
{
	
	for (unsigned i = 0; i < 5; i++)
	{
		if (mActor)
		{
			mBullets[i]->GetComp<TransformComponent>()->SetWorldPosition(mActor->GetComp<TransformComponent>()->GetWorldPosition());
		}
		mBullets[i]->SetEnabled(false);
	}
	last_used_bullet = 0;
}



void TurretEnemyMachine::Update()
{
	StateMachine::Update();

	Space* _Space = mActor->GetParentSpace();
	if (_Space != nullptr)
	{
		if (SettingsSys->debug_draw)
		{
			_Space->DrawCircle(mActor->GetComp<TransformComponent>()->GetPosition(), Range, Colors::red);
			_Space->DrawCircle(glm::vec2(ShootingOffset.x + mActor->GetComp<TransformComponent>()->GetPosition().x, ShootingOffset.y + mActor->GetComp<TransformComponent>()->GetPosition().y), 5, Colors::green);
		}
	}
}

void TurretEnemyMachine::Shutdown()
{
	StateMachine::Clear();
}

TurretEnemyMachine& TurretEnemyMachine::operator=(const TurretEnemyMachine& rhs)
{
	ReloadTime	= rhs.ReloadTime;
	Ammo		= rhs.Ammo;
	FireRate	= rhs.FireRate;
	Range		= rhs.Range;
	ArenaMode	= rhs.ArenaMode;
	BurstRate	= rhs.BurstRate;
	ShootingOffset.x = rhs.ShootingOffset.x;
	ShootingOffset.y = rhs.ShootingOffset.y;
	purif_prob = rhs.purif_prob;
	return *this;
}

TurretEnemyMachine* TurretEnemyMachine::Clone()
{
	TurretEnemyMachine* temp = FactorySys->Create<TurretEnemyMachine>();
	*temp = *this;
	return temp;
}

void TurretEnemyMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["ShootSpeed"] = ShootSpeed;
	j["ReloadTime"] = ReloadTime;
	j["Ammo"] = Ammo;
	j["FireRate"] = FireRate;
	j["Range"] = Range;
	j["ArenaMode"] = ArenaMode;
	j["BurstRate"] = BurstRate;
	j["ShootingOffset_X"] = ShootingOffset.x;
	j["ShootingOffset_Y"] = ShootingOffset.y;
	j["PurificationProb"] = purif_prob;
}
void TurretEnemyMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("ShootSpeed") != j.end())
		ShootSpeed = j["ShootSpeed"];
	if (j.find("ReloadTime") != j.end())
		ReloadTime = j["ReloadTime"];
	if (j.find("Ammo") != j.end())
		Ammo = j["Ammo"];
	if (j.find("FireRate") != j.end())
		FireRate = j["FireRate"];
	if (j.find("Range") != j.end())
		Range = j["Range"];
	if (j.find("ArenaMode") != j.end())
		ArenaMode = j["ArenaMode"];	
	if (j.find("BurstRate") != j.end())
		BurstRate = j["BurstRate"];	
	if (j.find("ShootingOffset_X") != j.end())
		ShootingOffset.x = j["ShootingOffset_X"];
	if (j.find("ShootingOffset_Y") != j.end())
		ShootingOffset.y = j["ShootingOffset_Y"];
	if (j.find("PurificationProb") != j.end())
		purif_prob = j["PurificationProb"];
}
