#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"


class BasicEnemyMachine;
class TurretEnemyMachine;

class TurretEnemyCheckForPlayer : public State
{
public:
	TurretEnemyCheckForPlayer(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine = nullptr;

	GameObject* RaycastObject = nullptr;
	glm::vec2 RayCollisionPoint = glm::vec2();
	float DistanceWithRayCollision = 0.0f;
	glm::vec2 OwnerTransform = glm::vec2();
	glm::vec2 PlayerTransform = glm::vec2();
	glm::vec2 Direction = glm::vec2();
};

class TurretEnemyShoot : public State
{
public:
	TurretEnemyShoot(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();
	
	TurretEnemyMachine* castedMachine;
	GameObject* mLastBulletShot;
	float BulletTime;
	
	glm::vec2 OwnerTransform = glm::vec2();
	glm::vec2 PlayerTransform = glm::vec2();
	glm::vec2 Direction = glm::vec2();
};

class TurretEnemyBurst : public State
{
public:
	TurretEnemyBurst(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine;
	GameObject* mLastBulletShot;
	float BulletTime;

	glm::vec2 OwnerTransform = glm::vec2();
	glm::vec2 PlayerTransform = glm::vec2();
	glm::vec2 Direction = glm::vec2();
};

class TurretEnemyShootCone : public State
{
public:
	TurretEnemyShootCone(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine;
	GameObject* mLastBulletShot;
	float BulletTime;


	glm::vec2 OwnerTransform = glm::vec2();
	glm::vec2 PlayerTransform = glm::vec2();
	glm::vec2 Direction = glm::vec2();
};

class TurretEnemyReload : public State
{
public:
	TurretEnemyReload(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine;
	glm::vec2 OwnerTransform = glm::vec2();
	glm::vec2 PlayerTransform = glm::vec2();
	glm::vec2 Direction = glm::vec2();
};

class TurretEnemyDie : public State
{
public:
	TurretEnemyDie(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine;
};

class TurretEnemyReset : public State
{
public:
	TurretEnemyReset(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	TurretEnemyMachine* castedMachine;
	float ResetTime = 0.1f;
};