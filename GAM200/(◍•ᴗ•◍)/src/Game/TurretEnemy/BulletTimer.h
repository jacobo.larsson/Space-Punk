#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Game/TurretEnemy/TurretEnemySystem.h"
#pragma once

class BulletTimer : public LogicComponent
{
	SM_RTTI_DECL(BulletTimer, LogicComponent);

public:
	BulletTimer();
	~BulletTimer() {}

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	void OnCollisionStarted(GameObject* other)override;

	BulletTimer* Clone() override;
	BulletTimer& operator=(const BulletTimer& rhs);
	void SecondInit();

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	//TurretEnemyMachine* turretmachine;
	glm::vec3 pos;
	TransformComponent* mOwnerTrans;
	RigidBody* mRigidBody;
	GameObject* turretactor;
	std::array<GameObject*, 2/*mMaxClouds*/> mGasExplosion;

private:
	float mTimeExisting=0.f;
	float mLifeTime=5.f;

};