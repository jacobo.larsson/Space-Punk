#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/IComp.h"
#include "../../RTTI/SM_RTTI.h"


#include "../../Input/Input.h" //Input for movement

#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"
#include "../../Animation/SM_Animation.h"
#include "../../StateMachines/StateMachine/StateMachine.h"

class TurretEnemyMachine : public StateMachine
{
	SM_RTTI_DECL(TurretEnemyMachine, StateMachine);

public:

	TurretEnemyMachine();
	~TurretEnemyMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	bool Edit()override;
	void OnCollisionStarted(GameObject* other) override;
	void ResetBullets();
	TurretEnemyMachine& operator=(const TurretEnemyMachine& rhs);
	TurretEnemyMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	//Variables
	TransformComponent* mOwnerTransform = nullptr;
	TransformComponent* pTransform = nullptr;

	SkeletonRenderable* mySkelRenderable = nullptr;
	spine::Bone* mBack_Piece;
	Collider* pCollider = nullptr;

	float mOwnerWidth = 0.0f;
	float mOwnerHeight = 0.0f;

	std::array<GameObject*, 5> mBullets;
	std::vector<GameObject*> mPlatforms;
	unsigned last_used_bullet = 0;
	float ShootSpeed = 1.0f;
	float ReloadTime = 3.0f;
	int Ammo = 4;
	unsigned maxbullets = Ammo;
	float FireRate = 1.5f;
	float Range = 0.0f;
	float BurstRate = 0.1f;
	bool Shot = false;
	bool ArenaMode = false;	
	glm::vec3 ShootingPoint = glm::vec3();
	glm::vec2 ShootingOffset = glm::vec2();

	//Damage receiving variables
	float mHDamage = 0;
	float mHResetTime = 0;

	int purif_prob = 50;
};