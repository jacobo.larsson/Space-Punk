#include  "../../Input/Input.h"
#include "GameInput.h"

bool GameInput::purificationUsingGamepad = false;

bool GameInput::LeftKeyPressed()
{
	int x;
	int y;
	InputSys->get_left_joystick_state(x, y);
	if (InputSys->key_is_triggered(keys::A) || x  < -20000){

	return true;
	}
	return false;
}

bool GameInput::RightKeyPressed()
{
	int x;
	int y;
	InputSys->get_left_joystick_state(x, y);
	if (InputSys->key_is_triggered(keys::D) || x > 20000) {

		return true;
	}
	return false;
}

bool GameInput::DownKeyPressed()
{
	int x;
	int y;
	InputSys->get_left_joystick_state(x, y);
	if (InputSys->key_is_triggered(keys::S) || y < -20000) {

		return true;
	}
	return false;
}

bool GameInput::UpKeyPressed()
{
	int x;
	int y;
	InputSys->get_left_joystick_state(x, y);
	if (InputSys->key_is_triggered(keys::W) || y > +20000) {

		return true;
	}
	return false;
}

bool GameInput::JumpKeyPressed()
{

	if (InputSys->key_is_triggered(keys::Space) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_A)) {

		return true;
	}
	return false;
}

bool GameInput::DashKeyPressed()
{
	if (InputSys->key_is_triggered(keys::LeftShift) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_B)) {

		return true;
	}
	return false;
}

bool GameInput::LightKeyPressed()
{
	if (InputSys->mouse_is_triggered(mouse_buttons::Left_click) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_X)) {

		if (InputSys->mouse_is_triggered(mouse_buttons::Left_click)) {

			purificationUsingGamepad = false;

		}
		else {

			purificationUsingGamepad = true;

		}
		return true;
	}
	return false;
}

bool GameInput::HeavyKeyPressed()
{
	if (InputSys->mouse_is_triggered(mouse_buttons::Right_click) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_Y)) {


		if (InputSys->mouse_is_triggered(mouse_buttons::Right_click)) {

			purificationUsingGamepad = false;

		}
		else {

			purificationUsingGamepad = true;
		}
		
		return true;

	}
	return false;
}

bool GameInput::ShootKeyPressed()
{
	if (InputSys->key_is_down(keys::Q) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER)) 
	{
		return true;
	}
	return false;
}
bool GameInput::ShieldKeyPressed()
{

	if (InputSys->key_is_down(keys::E) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER)) {

		return true;
	}
	return false;
	
}


//purification related
bool GameInput::PurificationBShiftPressed() {

	if (InputSys->key_is_down(keys::LeftShift) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_B)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationXClickPressed()
{
	if (InputSys->mouse_is_triggered(mouse_buttons::Left_click) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_X)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationASpacePressed()
{
	if (InputSys->key_is_down(keys::Space) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_A)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationYRightClickPressed()
{
	if (InputSys->mouse_is_triggered(mouse_buttons::Right_click) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_Y)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationUpWPressed()
{
	if (InputSys->key_is_down(keys::W) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_DPAD_UP)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationDownSPressed()
{
	if (InputSys->key_is_down(keys::S) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_DPAD_DOWN)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationLeftAPressed()
{
	if (InputSys->key_is_down(keys::A) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_DPAD_LEFT)) {

		return true;
	}
	return false;
}

bool GameInput::PurificationRigthDPressed()
{
	if (InputSys->key_is_down(keys::D) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_DPAD_RIGHT)) {

		return true;
	}
	return false;
}

