#pragma once
#include <vector>


namespace GameInput {

	bool LeftKeyPressed();
	bool RightKeyPressed();
	bool DownKeyPressed();
	bool UpKeyPressed();
	bool JumpKeyPressed();
	bool DashKeyPressed();
	bool LightKeyPressed();
	bool HeavyKeyPressed();
	bool ShieldKeyPressed();
	bool ShootKeyPressed();

	bool PurificationBShiftPressed();
	bool PurificationXClickPressed();
	bool PurificationASpacePressed();
	bool PurificationYRightClickPressed();
	bool PurificationUpWPressed();
	bool PurificationDownSPressed();
	bool PurificationLeftAPressed();
	bool PurificationRigthDPressed();

	extern bool purificationUsingGamepad;
	static std::vector<bool (*)()> allInputFunctions;
}