#pragma once 
#include <string> 
#include <glm/glm.hpp> 
#include "../../RTTI/IBase.h" 
#include "../../RTTI/SM_RTTI.h" 
#include "../../RTTI/IComp.h" 
#include "../../EventSystem/EventHandler.h" 
#include "../../LogicSystem/LogicComponent.h" 
#include "../../Transform2D/TransformComponent.h" 
#include "../../Graphics/Font/Font.h" 

class Checkpoint : public LogicComponent
{
	SM_RTTI_DECL(Checkpoint, IComp);
public:

	Checkpoint();
	Checkpoint(Checkpoint& rhs);
	~Checkpoint();
	Checkpoint& operator=(const Checkpoint& rhs);
	Checkpoint* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;



	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	static void ToLoadFromCheckpoint();
	void SaveToJson();

	bool Edit()override;
	static bool next_frame;
private:

	GameObject* player;

	TransformComponent* owner_transform;
	TransformComponent* player_transform;
	bool activated = false;
	int own_num;
	static int last_checkpoint;
	static int checkpoint_num;
	static bool anycheck;
	
};