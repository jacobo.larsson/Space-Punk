#include "../../Extern/ImGui/imgui.h" 
#include "../../Extern/ImGui/imgui_impl_sdl.h" 
#include "../../Extern/ImGui/imgui_impl_opengl3.h" 
#include "../../AssertSystem/AssertSystem.h" 

#include "../../Factory/Factory.h" 
#include "../../Scene/Scene.h" 
#include "../../Space/Space.h" 
#include "../../Collisions/Collisions.h" 
#include "../../PropertySystem/PropertyMap.h" 
#include "../../Changer/Changer.h" 
#include "../../Input/Input.h" 
#include "../../Time/Time.h" 
#include "../../Editor/Menus/Menus.h" 
#include "../../ThreadPool/ThreadPool.h"
#include "../Health/Health.h"

#include "Checkpoint.h"

int Checkpoint::last_checkpoint = 0;
int Checkpoint::checkpoint_num = 0;
bool Checkpoint::anycheck =false;
bool Checkpoint::next_frame =false;
Checkpoint::Checkpoint()
{
	mName = "Checkpoint";
}
Checkpoint::Checkpoint(Checkpoint& rhs)
{


}
Checkpoint::~Checkpoint()
{


}
Checkpoint& Checkpoint::operator=(const Checkpoint& rhs)
{

	return *this;
}
Checkpoint* Checkpoint::Clone()
{

	Checkpoint* temp = FactorySys->Create<Checkpoint>();
	(*temp) = (*this);
	return temp;
}

void Checkpoint::OnCreate()
{


}
void Checkpoint::Initialize()
{
	GameObject* object_owner = GetOwner();
	LogicComponent::Initialize();
	player = SceneSys->GetMainSpace()->FindObjectByName("Player");
	owner_transform = object_owner->GetComp<TransformComponent>();
	if(player)
		player_transform = player->GetComp<TransformComponent>();
	own_num = checkpoint_num;
	checkpoint_num++;

}
void Checkpoint::Update()
{
	
	if (activated == false)
	{
		if (player)
		{

			if (owner_transform && player_transform)
			{

				if (StaticRectToStaticRect(player_transform->GetWorldPosition(), player_transform->GetScale().x, player_transform->GetScale().y,
					owner_transform->GetWorldPosition(), owner_transform->GetScale().x, owner_transform->GetScale().y))
				{

					Renderable* own_rend = mOwner->GetComp<Renderable>();

					if (own_rend)
					{
						own_rend->SetModulationColor(Colors::red);

					}
					GameObject* player = SceneSys->GetMainSpace()->FindObjectByName("Player");
					Health* to_restore = player->GetComp<Health>();

					if (to_restore)
					{
						to_restore->Recover(100.f);

					}
					SaveToJson();
					anycheck = true;
					activated = true;
					last_checkpoint = own_num;

				}
			}
		}
	}

	if (InputSys->key_is_down(keys::M))
	{
		ToLoadFromCheckpoint();
	}
}
void Checkpoint::Shutdown()
{
}




void Checkpoint::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	j["activated"] = activated;
	j["own_num"] = own_num;
}
void Checkpoint::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	
	if (j.find("activated") != j.end())
		activated = j["activated"];
	
	if (j.find("own_num") != j.end())
		own_num = j["own_num"];
}

void Checkpoint::ToLoadFromCheckpoint()
{
	if (anycheck)
	{
		std::string outString("./data/Saves/" "Checkpoint_" + std::to_string(last_checkpoint) + ".lvl");


		SceneSys->ChangeLevelGameCompleteName(outString);
		SceneSys->to_change = true;
	}
	else
	{
		std::string test = "./data/Levels/MainMenu.lvl";
		SceneSys->ChangeLevelGameCompleteName(test);
		SceneSys->to_change = true;
	}
}

void Checkpoint::SaveToJson()
{
	ThreadP->enqueue([&]
		{
			json h;
			SceneSys->ToJson(h);



			std::string to_save = "./data/Saves/" "Checkpoint_" + std::to_string(own_num) + ".lvl";
			std::ofstream fp_out(to_save);
			if (fp_out.is_open() && fp_out.good())
			{
				fp_out << std::setw(4) << h;
				fp_out.close();
			}
		}
	);
}


bool Checkpoint::Edit()
{

	return false;

}