#include <iostream>
#include <FMOD/fmod.hpp>
#include <glm/glm.hpp>

#include "../../Input/Input.h" //Input for movement

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/PhysicSystem.h"
#include "../../Audio/SoundEmitter.h"
#include "../../Audio/Voice.h"
#include "../../Space/Space.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Game/BasicEnemy/BasicEnemySystem.h"
#include "../../Game/TurretEnemy/TurretEnemySystem.h"
#include "../../Game/ChargerEnemy/ChargerEnemySystem.h"
#include "../../Game/FlyingEnemy/FlyingEnemySystem.h"
#include "../../Game/Health/Health.h"
#include "../../Game/ShotReceiver/ShotReceiver.h"
#include "../../Time/Time.h"
#include "../CameraFollow/CameraMachine.h"
#include "../CameraFollow/CameraStates.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "MovementStates.h"
#include "PlayerMovement.h"
#include "../../RayCast/Raycast.h"
#include "../../RayCast/RaycastTest.h"
#include "../GameInput/GameInput.h"
#include "../../Animation/SM_Animation.h"
#include "../../Graphics/Particle System/ParticleSystem.h"
#include "../Checkpoint/Checkpoint.h"
#include "../CameraFollow/CameraEffect/CameraEffect.h"
#include "../CameraFollow/CameraMachine.h"


#pragma region PlayerIdle
PlayerIdle::PlayerIdle(const char* name) : State(name), locked(false) {

}

void PlayerIdle::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
	pTransform = mActor->GetComp<TransformComponent>();
	State::Initialize();
}

void PlayerIdle::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->InputBuffer.clear();

	if (castedMachine->mySkelRend) 
	{
		if (castedMachine->pRigidbody->Velocity.y < -100 ) 
		{
			if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Jump/Nero_Jump_Down4")
				castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Down4");
		}
		else 
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Idle",true);
	}
}

void PlayerIdle::LogicUpdate()
{
	if (castedMachine->pRigidbody != nullptr)
		castedMachine->pRigidbody->Velocity.x = 0;

	if (GameInput::JumpKeyPressed())
	{
		if (castedMachine->canJump) 
		{
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerJumping"));
			castedMachine->canJump = false;
		}
	}
	else if (GameInput::RightKeyPressed() || GameInput::LeftKeyPressed()) 
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerMoving"));
	else if (GameInput::DashKeyPressed() && castedMachine->currentDashCooldown == 0) 
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerDashingFast"));
	else if (GameInput::ShootKeyPressed()) 
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShoot"));
	else
	{
		//if (!castedMachine->mIsInTheAir) {
			castedMachine->BrakeX();
			castedMachine->pRigidbody->SetDirection(0.0f);

		//}
		//else {
		//	castedMachine->CurrentVelocity = castedMachine->mPersistentVelocity;
		//	castedMachine->UpdatePosition();
		//}
	}

	CombatInputs newInput = castedMachine->GetCombatInput();

	if (newInput != CombatInputs::Invalid && !castedMachine->hasattackedintheair) 
	{
		ComboList previousAttack = ComboList::ComboStart;
		mOwnerStateMachine->ChangeState("PlayerAttack1");
	}

	castedMachine->UpdatePosition();

	if (castedMachine->pRigidbody->Velocity.y < -100 /*&& mTimeInState > 0.5*/) 
	{
		if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Jump/Nero_Jump_Down4")
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Down4");
	}

	float Epsilon = .1f;
	if (castedMachine->pRigidbody->Velocity.y > 0 - Epsilon && castedMachine->pRigidbody->Velocity.y < 0 + Epsilon)
	{
		if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Idle")
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Idle");
	}

	std::cout << "Idle " << castedMachine->CurrentVelocity <<std::endl;

}

void PlayerIdle::LogicExit()
{
}
#pragma endregion PlayerIdle

#pragma region PlayerJumping
PlayerJumping::PlayerJumping(const char* name) : State(name)
{
}

void PlayerJumping::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void PlayerJumping::LogicEnter()
{
	Jump();
	castedMachine->mIsInTheAir = true;
	doubleJumped = false;
	castedMachine->MaxXVelocity -= 100;

	hasprepared = false;
	hasreachedpeek = false;
	hasgonedown = false;
	hasrecuperated = false;
	mDelayedDoubleJump  = false;
	mTimeInState = 0.0f;
}

void PlayerJumping::LogicUpdate()
{
	if (GameInput::RightKeyPressed())
	{
		castedMachine->Accelerate(castedMachine->Acceleration);
		castedMachine->FacingRight = true;
	}
	else if (GameInput::LeftKeyPressed()) 
	{
		castedMachine->Accelerate(-castedMachine->Acceleration);
		castedMachine->FacingRight = false;
	}
	else 
		castedMachine->BrakeX();

	if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) 
		castedMachine->BrakeX();

	if (GameInput::JumpKeyPressed() && !doubleJumped && mTimeInState) 
	{

		if (mTimeInState < 0.2f) {
			castedMachine->JumpVelocity += 150;
			Jump();
			doubleJumped = true;
			castedMachine->JumpVelocity -= 150;


		}else if (mTimeInState < 0.4f) {
				castedMachine->JumpVelocity += 100;
				Jump();
				doubleJumped = true;
				castedMachine->JumpVelocity -= 100;


		}
		else {
					Jump();
					doubleJumped = true;
			

		}
	}


	if (GameInput::DashKeyPressed() && castedMachine->currentDashCooldown == 0) 
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerDashingFast"));
	if (GameInput::ShootKeyPressed())
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShoot"));

	castedMachine->UpdatePosition();
	castedMachine->PreviousFacingRight = castedMachine->FacingRight;
	CombatInputs newInput = castedMachine->GetCombatInput();
	if (newInput != CombatInputs::Invalid && !castedMachine->hasattackedintheair) 
		mOwnerStateMachine->ChangeState("PlayerAttack1");


	//Anim Related
	/////////////////////////
	if (!hasprepared && mTimeInState > prepTime) 
	{
		hasprepared = true;
		if (castedMachine->mySkelRend) 
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Up2", true);
	}

	if (mTimeInState > 0.5f && !hasreachedpeek && abs(castedMachine->pRigidbody->Velocity.y) < 1.0f)
	{
		hasreachedpeek = true;
		if (castedMachine->mySkelRend) 
		{
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Peak3", false);
			prepTime = castedMachine->mySkelRend->GetCurrentAnimation()->getDuration() + mTimeInState;
		}
	}
	if (castedMachine->pRigidbody->Velocity.y < -200.0f) 
	{
		if (castedMachine->mySkelRend)
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Down4", true);
	}
}

void PlayerJumping::LogicExit()
{
	if (castedMachine)
		castedMachine->MaxXVelocity += 100;
}


void PlayerJumping::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Platform"))
	{
		if (castedMachine->mySkelRend)
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Recuperation5", false);
		mOwnerStateMachine->ChangeState("PlayerIdle");
		castedMachine->mIsInTheAir = true;

	}
}

void PlayerJumping::Jump()
{
	if (castedMachine->mySkelRend)
	{
		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Anticipation1", false);
		prepTime = castedMachine->mySkelRend->GetAnimDuration() + mTimeInState;
		bool hasprepared = false;
		bool hasreachedpeek = false;
		bool hasgonedown = false;
		bool hasrecuperated = false;
	}
	castedMachine->HasJumped = true;
	castedMachine->pRigidbody->Velocity.y = castedMachine->JumpVelocity;
}
#pragma endregion PlayerJumping

#pragma region PlayerMoving
PlayerMoving::PlayerMoving(const char* name) : State(name) {}

void PlayerMoving::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);

	pTransform = mActor->GetComp<TransformComponent>();
}

void PlayerMoving::LogicEnter()
{
	if (castedMachine->mParticleEmitter) {
		castedMachine->mParticleEmitter->mActiveSystem = true;
		castedMachine->mParticleEmitter->Load("./data/Particles/DustPart.json");
	}
	castedMachine->InputBuffer.clear();
	if (!castedMachine->hasVelocity) {

		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Walk", true);
		isrunning = false;

	}
	else {

		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Run", true);
		isrunning = true;
	}
	
	if (castedMachine->pRigidbody->Velocity.y < -10 /*&& mTimeInState > 0.5*/) {
		if (castedMachine->mParticleEmitter)
			castedMachine->mParticleEmitter->mActiveSystem = false;
		if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Jump/Nero_Jump_Down4")
		{
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Down4");
		}
	}
}
void PlayerMoving::LogicUpdate()
{
	if (mActor->GetComp<RigidBody>() != NULL)
		mActor->GetComp<RigidBody>()->Velocity.x = 0;

	if (GameInput::RightKeyPressed()) {
		castedMachine->Accelerate(castedMachine->Acceleration);

		castedMachine->FacingRight = true;
		if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
			castedMachine->hasVelocity = true;/////////////////////////////////////////////////
			castedMachine->BrakeX();
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerIdle"));
		}

		if (castedMachine->pRigidbody != nullptr)
		{
			castedMachine->pRigidbody->SetDirection(1.0f);
		}
	}
	else if (GameInput::LeftKeyPressed()) {
		castedMachine->Accelerate(-castedMachine->Acceleration);

		castedMachine->FacingRight = false;
		if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
			castedMachine->BrakeX();
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerIdle"));
			castedMachine->hasVelocity = true;

		}

		if (castedMachine->pRigidbody != nullptr)
		{
			castedMachine->pRigidbody->SetDirection(-1.0f);
		}
	}
	else {
		//castedMachine->BrakeX();
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerIdle"));
		castedMachine->hasVelocity = false;
	}


	//Animation changes
	if (!isrunning && abs(castedMachine->CurrentVelocity) > 400) {
		if (castedMachine->mySkelRend) {

			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Run", true);
			if (castedMachine->mParticleEmitter)
				castedMachine->mParticleEmitter->mActiveSystem = true;

		}
		isrunning = true;
	}

	if (GameInput::JumpKeyPressed()) {
		if (castedMachine->canJump) {
			if (!(castedMachine->mIsInTheAir && castedMachine->HasJumped)) {

				mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerJumping"));
				castedMachine->canJump = false;
			}
		}
	}

	if (GameInput::DashKeyPressed() && castedMachine->currentDashCooldown == 0) {
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerDashingFast"));
	}
	if (GameInput::ShootKeyPressed()) {
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShoot"));
	}

	//if (GameInput::ShieldKeyPressed()) {
	//	mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShielding"));
	//}


	castedMachine->UpdatePosition();

	castedMachine->PreviousFacingRight = castedMachine->FacingRight;
	CombatInputs newInput = castedMachine->GetCombatInput();
	if (newInput != CombatInputs::Invalid && !castedMachine->hasattackedintheair) {
		mOwnerStateMachine->ChangeState("PlayerAttack1");
	}


	if (castedMachine->pRigidbody->Velocity.y < -100 /*&& mTimeInState > 0.5*/) {
		if (castedMachine->mParticleEmitter)
			castedMachine->mParticleEmitter->mActiveSystem = false;
		if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Jump/Nero_Jump_Down4")
		{
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Down4");
		}
	}

	if (castedMachine->pRigidbody->Velocity.y > 10 /*&& mTimeInState > 0.5*/) {
		if (castedMachine->mParticleEmitter)
			castedMachine->mParticleEmitter->mActiveSystem = false;
		if (castedMachine->mySkelRend->GetCurrentAnimationName() != "Nero_Jump/Nero_Jump_Up2")
		{
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Jump/Nero_Jump_Up2");
		}
	}

	std::cout << "Moving " << castedMachine->CurrentVelocity << std::endl;

}

void PlayerMoving::LogicExit()
{
	if (castedMachine->mParticleEmitter)
		castedMachine->mParticleEmitter->mActiveSystem = false;
}

void PlayerMoving::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Platform"))
	{
		castedMachine->canJump = true;

		if (castedMachine->mySkelRend->GetCurrentAnimationName() == "Nero_Jump/Nero_Jump_Down4")
		{
			if (castedMachine->mParticleEmitter)
				castedMachine->mParticleEmitter->mActiveSystem = true;
			castedMachine->mySkelRend->SetCurrentAnimation("Nero_Run" ,true);
		}
	}
}
#pragma endregion PlayerMoving

#pragma region PlayerDashing

#pragma region  PlayerDashingFast
PlayerDashingFast::PlayerDashingFast(const char* name) :State(name)
{

}
void PlayerDashingFast::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*> (mOwnerStateMachine);
}

void PlayerDashingFast::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->CurrentVelocity = (castedMachine->FacingRight) ? 1900 : -1900;
	if (castedMachine->mySkelRend)
		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard", true);
	castedMachine->mDashing = true;
}
void PlayerDashingFast::LogicUpdate()
{
	castedMachine->UpdatePosition();
	mOwnerStateMachine->ChangeState("PlayerDashRecovering");
}

void PlayerDashingFast::LogicExit()
{
}

#pragma endregion

#pragma region PlayerDashRecovering

PlayerDashRecovering::PlayerDashRecovering(const char* name) : State(name)
{
}

void PlayerDashRecovering::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*> (mOwnerStateMachine);
}

void PlayerDashRecovering::LogicEnter()
{
	if (castedMachine->mParticleEmitter) {
		castedMachine->mParticleEmitter->mActiveSystem = true;
		castedMachine->mParticleEmitter->looping = false;
		castedMachine->mParticleEmitter->Load("./data/Particles/DashPart.json");
	}
	mTimeInState = 0.0f;

	if (castedMachine->mySkelRend)
		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Dash", true);
}

void PlayerDashRecovering::LogicUpdate()
{
	castedMachine->CurrentVelocity -= castedMachine->FacingRight ? 120 : -120;
	castedMachine->UpdatePosition();
	if (mTimeInState > 0.2f && !castedMachine->mHoverboardTransition)
		castedMachine->ChangeState("PlayerIdle");
	else if (mTimeInState > 0.2f)
	{
		castedMachine->ChangeState("HoverBoardState");
		castedMachine->mHoverboardTransition = false;
	}

	if ((GameInput::HeavyKeyPressed() || GameInput::LightKeyPressed())) {
		castedMachine->previousAttack = ComboList::LightNormal2;
		castedMachine->InputBuffer.clear();
		castedMachine->InputBuffer.push_back(CombatInputs::LightAttack);
		castedMachine->InputBuffer.push_back(CombatInputs::LightAttack);
		castedMachine->InputBuffer.push_back(CombatInputs::HeavyAttack);
		castedMachine->ChangeState("PlayerAttack3");
	}
}
void PlayerDashRecovering::LogicExit()
{

	//castedMachine->mParticleEmitter->mActiveSystem = false;
	castedMachine->mDashing = false;
	castedMachine->currentDashCooldown = castedMachine->DashCooldown;
}

#pragma endregion

#pragma endregion PlayerDashing

#pragma region PlayerShoot
PlayerShoot::PlayerShoot(const char* name) : State(name)
{
}

void PlayerShoot::Initialize()
{
	mCastedMachine = dynamic_cast<MovementMachine*> (mOwnerStateMachine);
}

void PlayerShoot::LogicEnter()
{
	mTimeInState = 0.0f;
	mCastedMachine->mBlasterCounter = 0.0f;
	auto _PlayerPosition = mCastedMachine->pTransform->GetWorldPosition();
	std::vector<GameObject*>* _ShootableObjects = mActor->GetParentSpace()->FindAllObjectsByTag("Shootable");
	if (_ShootableObjects)
	{
		Ray* _ActualRay = mCastedMachine->FacingRight ? mCastedMachine->PlayerRightRay : mCastedMachine->PlayerLeftRay;
		mDistance = CheckRayWithVector( _ActualRay, &RayCastObject, &collisionPoint, *_ShootableObjects);
		if (SettingsSys->debug_draw)
		{
			glm::vec2 _Direction{ 10000.0f, 0.0f };
			if (!mCastedMachine->FacingRight)
				_Direction *= -1.0f;
			mActor->GetParentSpace()->DrawLine(glm::vec2{ _PlayerPosition }+_ActualRay->GetOrigin(), glm::vec2{ _PlayerPosition }+ _ActualRay->GetOrigin() + _Direction, Colors::orange);
		}
	}
	if (mCastedMachine->mySkelRend)
		mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Blaster");
	if (mDistance != -1.0f && RayCastObject &&  mDistance < mCastedMachine->mBlasterRange)
	{
		if (RayCastObject->HasComp<ShotReceiver>())
			RayCastObject->GetComp<ShotReceiver>()->SetShot(true);
	}

	if (mCastedMachine->mParticleEmitter) {

		mCastedMachine->mParticleEmitter->mActiveSystem = true;

		mCastedMachine->mParticleEmitter->Load("./data/Particles/BlasterPart.json");

		mCastedMachine->mParticleEmitter->props.mInitialOffset.y = 0.4 ;

		mCastedMachine->mParticleEmitter->mTransform.mPosition.x = (mCastedMachine->FacingRight) ? 40 : -45;
		mCastedMachine->mParticleEmitter->props.mVelocity.x = (mCastedMachine->FacingRight) ? 300 : -300;
	}

}
void PlayerShoot::LogicUpdate()
{
	if (mTimeInState > 0.65f && mCastedMachine)
		mOwnerStateMachine->ChangeState("PlayerIdle");
}
void PlayerShoot::LogicExit()
{
	if (mCastedMachine->mParticleEmitter) {

		mCastedMachine->mParticleEmitter->mActiveSystem = false;
	}
}

#pragma endregion PlayerShoot

#pragma region PlayerShielding

PlayerShielding::PlayerShielding(const char* name) : State(name)
{
}

void PlayerShielding::Initialize()
{
	castedMachine = reinterpret_cast<MovementMachine*>(mOwnerStateMachine);
}

void PlayerShielding::LogicEnter()
{
	if (!castedMachine) {
		return;
	}
	mTimeInState = 0.0f;
	castedMachine->shielding = true;
	if (castedMachine->mySkelRend)
		castedMachine->mySkelRend->SetCurrentAnimation("Nero_Shield");
}

void PlayerShielding::LogicUpdate()
{
	if (!castedMachine) {
		return;
	}
	if (mTimeInState > 0.8f) {
		castedMachine->ChangeState("PlayerIdle");
	}
}

void PlayerShielding::LogicExit()
{
	if (!castedMachine) {
		return;
	}
	
	castedMachine->shielding = false;
}


#pragma endregion PlayerShielding

#pragma region PlayerPurification
PlayerPurification::PlayerPurification(const char* name): State(name)
{
}

void PlayerPurification::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);

	State::Initialize();
}

void PlayerPurification::LogicEnter()
{
	castedMachine->InputBuffer.clear();
	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
		if (mCamera)
		{
			
			BrainComponent* brain = camera->GetComp<BrainComponent>();

			if (brain)
			{
				CameraMachine* camfollow = reinterpret_cast<CameraMachine*>(camera->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));

				if (camfollow)
				{
					auto state = reinterpret_cast<CameraPurificationState*>(camfollow->GetState("CameraPurificationState"));
					
					if(state)
						state->mPreviousMode = camfollow->GetCurrentStateName();
					camfollow->ChangeState("CameraPurificationState");
				}
			}
		}
	}
	Physys->slowmo = true;
}

void PlayerPurification::LogicExit()
{
	
	Physys->slowmo = false;

	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
		if (mCamera)
		{
			BrainComponent* brain = camera->GetComp<BrainComponent>();

			if (brain)
			{
				CameraMachine* camfollow = reinterpret_cast<CameraMachine*>(camera->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));

				if (camfollow)
				{
					auto state = reinterpret_cast<CameraPurificationState*>(camfollow->GetState("CameraPurificationState"));
					
					if (state)
					{
						if (state->mPreviousMode == "CameraFollowStateFollow")
							state->mCastedMachine->ChangeState("CameraFollowStateIdle");
						else
							state->mCastedMachine->ChangeState(state->mPreviousMode.c_str());
					}
				}
			}
		}
	}
	Health* p_health = mActor->GetComp<Health>();
	if (p_health)
		p_health->Recover(10.f);
}

#pragma endregion PlayerPurification


#pragma region PlayerDamaged

PlayerDamaged::PlayerDamaged(const char* name) : State(name)
{
}

void PlayerDamaged::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void PlayerDamaged::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->mySkelRend->SetCurrentAnimation("Nero_Damaged");
	castedMachine->mHealth->Damage(castedMachine->mDamageReceived);
	if (castedMachine->mHealth->GetHealth() <= 0) {
		mOwnerStateMachine->ChangeState("PlayerDie");
	}
	if (castedMachine->mParticleEmitter) {
		castedMachine->mParticleEmitter->Load("./data/Particles/DamagePart2.json");
		castedMachine->mParticleEmitter->mActiveSystem = true;
	}


	if (castedMachine->mCameraMachine) {
		CameraEffect _Effect;
		_Effect.mEffectType = CameraEffectLists::CameraShake;
		_Effect.mDuration = 0.3f;

		castedMachine->mCameraMachine->AddEffect(_Effect);
		castedMachine->mCameraMachine->AddEffect(_Effect);
		castedMachine->mCameraMachine->AddEffect(_Effect);
		castedMachine->mCameraMachine->AddEffect(_Effect);

	}
}

void PlayerDamaged::LogicUpdate()
{
	if (mTimeInState < castedMachine->mKnockbackTime) 
	{
		castedMachine->CurrentVelocity = (castedMachine->FacingRight ? -1 : 1) * castedMachine->mKnockbackSpeed;
		castedMachine->UpdatePosition();
	}
	else if (castedMachine->mHealth->GetHealth() > 0 && mTimeInState > castedMachine->mySkelRend->GetCurrentAnimation()->getDuration()) 
		mOwnerStateMachine->ChangeState("PlayerIdle");
}

void PlayerDamaged::LogicExit()
{
	if (castedMachine->mParticleEmitter)
		castedMachine->mParticleEmitter->mActiveSystem = false;
	castedMachine->mInvulnerability = 1.5;

}

#pragma endregion PlayerDamaged
#pragma region PlayerDie

PlayerDie::PlayerDie(const char* name) : State(name)
{
}

void PlayerDie::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void PlayerDie::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->mySkelRend->SetCurrentAnimation("Nero_Death");
}

void PlayerDie::LogicUpdate()
{
	if (mTimeInState > 2.5) 
		Checkpoint::ToLoadFromCheckpoint();
}

#pragma endregion PlayerDamaged
