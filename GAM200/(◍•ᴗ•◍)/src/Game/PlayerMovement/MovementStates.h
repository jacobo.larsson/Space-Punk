#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../CameraFollow/CameraFollow.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"

class MovementMachine;
class TransformComponent;

class PlayerIdle : public State
{
public:
	PlayerIdle(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	bool locked;
	TransformComponent* pTransform;
	MovementMachine* castedMachine;
};

class PlayerJumping : public State
{
public:
	PlayerJumping(const char* name = nullptr);

	void Initialize()override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionStarted(GameObject* other)override;
	void Jump();

	MovementMachine* castedMachine;
	bool doubleJumped;
	float prepTime;
	bool hasprepared = false;
	bool hasreachedpeek = false;
	bool hasgonedown = false;
	bool hasrecuperated = false;

	bool mDelayedDoubleJump = false;
	
};

class PlayerMoving : public State
{
public:
	PlayerMoving(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionStarted(GameObject* other)override;

	bool Right = false;
	bool isrunning = false;

	TransformComponent* pTransform;
	MovementMachine* castedMachine;
};

#pragma region PlayerDashing
class PlayerDashingFast : public State {
public:
	PlayerDashingFast(const char* name = nullptr);

	void Initialize() override;
	void LogicUpdate()override;
	void LogicEnter() override;
	void LogicExit()override;

	MovementMachine* castedMachine;	
};

class PlayerDashRecovering : public State {
public:
	PlayerDashRecovering(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	MovementMachine*  castedMachine;
};

class PlayerShoot : public State {
public:
	PlayerShoot(const char* name = nullptr);

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	MovementMachine* mCastedMachine = nullptr;

	GameObject* RayCastObject = NULL;
	glm::vec2 collisionPoint;
	float mDistance = -1.0f;
};

#pragma endregion


#pragma region PlayerShielding 

class PlayerShielding : public State {
public:
	PlayerShielding(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
		
	MovementMachine* castedMachine;
};

#pragma endregion


#pragma region PlayerPurification 

class PlayerPurification : public State
{
public:
	PlayerPurification(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit()override;

	TransformComponent* pTransform;
	CameraComponent* mCamera;
	glm::vec2 viewrect;
	glm::vec2 newviewrect;
	glm::vec3 original_pos;
	glm::vec3 inital_pos;
	MovementMachine* castedMachine;
};
#pragma endregion

#pragma region PlayerDamaged 

class PlayerDamaged : public State
{
public:
	PlayerDamaged(const char* name = nullptr);

	void LogicEnter() override;
	void LogicUpdate()override;
	void Initialize() override;
	void LogicExit()override;

	TransformComponent* pTransform;
	CameraComponent* mCamera;
	glm::vec2 viewrect;
	glm::vec2 newviewrect;
	glm::vec3 original_pos;
	glm::vec3 inital_pos;
	MovementMachine* castedMachine;
};
#pragma endregion

#pragma region PlayerDie 

class PlayerDie : public State
{
public:
	PlayerDie(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	MovementMachine* castedMachine;
};
#pragma endregion