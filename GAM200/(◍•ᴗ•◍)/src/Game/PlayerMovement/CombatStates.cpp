#include <iostream>
#include <math.h>

#include <glm/glm.hpp>
#include <glm/integer.hpp>

#include "../../Input/Input.h" //Input for movement

#include "../../GameObject/GameObject.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "MovementStates.h"
#include "PlayerMovement.h"
#include "../../Graphics/Renderable/Renderable.h"

#include "../../Archetype/ArchetypeFunctions.h"
#include "../../Debug/Console.h"

#include "../../Animation/SM_Animation.h"
#include "../Hitbox/Hitbox.h"
#include "../GameInput/GameInput.h"


PlayerAttack1::PlayerAttack1(const char* name) : State(name)
{}

void PlayerAttack1::Initialize()
{
	castedMachine = reinterpret_cast<MovementMachine*>(mOwnerStateMachine);
}

void PlayerAttack1::LogicEnter()
{	
	mTimeInState = 0.0f;

	//starting point, this is hardcodd because the start of the combos are fixed
	if (castedMachine && castedMachine->InputBuffer[0] == CombatInputs::LightAttack) 
		currentAttack = ComboList::LightNormal1;

	if (castedMachine && castedMachine->InputBuffer[0] == CombatInputs::HeavyAttack)
		currentAttack = ComboList::SpammedHeavy1;
	
	//retrieve the actual information of the combo, properties and everything
	currentComboAttack = &(castedMachine->mAllAttacks[currentAttack]);
	
	if (castedMachine->mySkelRend)
		castedMachine->mySkelRend->SetCurrentAnimation(currentComboAttack->mAnimation.c_str());
}

void PlayerAttack1::LogicUpdate()
{

	
	if (castedMachine->mIsInTheAir) {

		castedMachine->CurrentVelocity = castedMachine->mPersistentVelocity;
		//Input for keeping movemen in the air
		if (GameInput::RightKeyPressed()) {
			castedMachine->Accelerate(castedMachine->Acceleration);

			castedMachine->FacingRight = true;
			if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
				castedMachine->hasVelocity = true;
				
				mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerIdle"));
			}

			if (castedMachine->pRigidbody != nullptr)
			{
				castedMachine->pRigidbody->SetDirection(1.0f);
			}
		}
		else if (GameInput::LeftKeyPressed()) {
			castedMachine->Accelerate(-castedMachine->Acceleration);

			castedMachine->FacingRight = false;
			if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
				
				mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerIdle"));
				castedMachine->hasVelocity = true;

			}

			if (castedMachine->pRigidbody != nullptr)
			{
				castedMachine->pRigidbody->SetDirection(-1.0f);
			}
		}


	}
	//if we need to put the hitbox
	if (mTimeInState > currentComboAttack->mDelay )
	{
		glm::vec3 _TargetPosition{ castedMachine->pTransform->GetWorldPosition() };
		_TargetPosition.y += castedMachine->pTransform->GetWorldScale().y / 2 + 50.0f;
		_TargetPosition.x += (castedMachine->FacingRight) ? 50 : -50;
		_TargetPosition.x += ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mScale.x / 2;
		_TargetPosition.z = 0;

		castedMachine->UpdateHitbox(_TargetPosition, currentComboAttack->mScale ,currentComboAttack->mDamage, currentComboAttack->mKnockback);

		if (!castedMachine->mIsInTheAir)
		castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x;
	}

	if (mTimeInState > currentComboAttack->minputMargin.x&& mTimeInState < currentComboAttack->minputMargin.y) 
		castedMachine->GetCombatInput();

	//attack displacement
	//if we havent finished moving
	if (mTimeInState < currentComboAttack->mDisplacement.y )
	{
		if (!castedMachine->mIsInTheAir) {

			double current = (sin(mTimeInState * (3.14 / 2 * currentComboAttack->mDisplacement.y) + (3 * 3.14) / 2));
		
			castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x * current * current;
			if (abs(castedMachine->CurrentVelocity) < 0) 
				castedMachine->CurrentVelocity = 0;

		}
		
		
		if (!castedMachine->mIsInTheAir)

		castedMachine->DisplaceCombo(castedMachine->mHitbox, currentComboAttack->mDisplacement.x);
	}
	else 
		castedMachine->CurrentVelocity = 0;

	if (mTimeInState > currentComboAttack->mTime) 
	{
		// Disable the hitbox when the attack is finished
		if (castedMachine->mHitbox) 
			castedMachine->mHitbox->SetEnabled(false);

		if (mTimeInState > currentComboAttack->mTime + currentComboAttack->mRecovery) 
		{
			//if there are no more input, end the combo
			if (castedMachine && castedMachine->InputBuffer.size() < 2 || castedMachine->mIsInTheAir)
			{
				castedMachine->EndCombo();
				return;
			}

			if (castedMachine->InputBuffer[1] == CombatInputs::Invalid) 
			{
				castedMachine->EndCombo();
				return;
			}
			else 
				mOwnerStateMachine->ChangeState("PlayerAttack2");
		}
	}
	if (castedMachine->mIsInTheAir) {

		castedMachine->CurrentVelocity = castedMachine->mPersistentVelocity;
	}
	std::cout << "Attack1 " << castedMachine->CurrentVelocity <<std::endl;
	castedMachine->UpdatePosition();
}

void PlayerAttack1::LogicExit()
{
	if (castedMachine)
	{
		//set the previous attack
		castedMachine->previousAttack = currentAttack;
		castedMachine->mHitbox->SetEnabled(false);
		if(castedMachine->mIsInTheAir)
		castedMachine->CurrentVelocity = castedMachine->mPersistentVelocity;
		
	}
}

PlayerAttack2::PlayerAttack2(const char* name) : State(name)
{}

void PlayerAttack2::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
}

void PlayerAttack2::LogicEnter()
{
	mTimeInState = 0.0f;

	ComboList &ifHeavy = castedMachine->mAllAttacks[castedMachine->previousAttack].mHeavyTransfer;
	ComboList &ifLight = castedMachine->mAllAttacks[castedMachine->previousAttack].mLightTransfer;
	
	CombatInputs Input = castedMachine->InputBuffer[1];

	currentComboAttack = (Input == CombatInputs::LightAttack) ? 
							&castedMachine->mAllAttacks[ifLight] : 
							&castedMachine->mAllAttacks[ifHeavy] ;

	currentAttack = (Input == CombatInputs::LightAttack) ?
		ifLight :
		ifHeavy;

	if (castedMachine->mySkelRend)
		castedMachine->mySkelRend->SetCurrentAnimation(currentComboAttack->mAnimation.c_str());
}

void PlayerAttack2::LogicUpdate()
{
	// Update the hitbox position and properties when the attack is ready to be done
	if (mTimeInState > currentComboAttack->mDelay)
	{
		if (castedMachine->mHitbox ) 
		{

			glm::vec3 _TargetPos = castedMachine->pTransform->GetWorldPosition();
			_TargetPos.y += castedMachine->pTransform->GetWorldScale().y / 2;
			_TargetPos.y += 50;
			_TargetPos.x += (castedMachine->FacingRight) ? 50 : -50;
			_TargetPos.x += ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mScale.x / 2;
			_TargetPos.z = 0;

			castedMachine->UpdateHitbox(_TargetPos, currentComboAttack->mScale, currentComboAttack->mDamage, currentComboAttack->mKnockback);
			if (!castedMachine->mIsInTheAir)
			castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x;
		}
	}
	

	castedMachine->GetCombatInput();

	//attack displacement
	//if we havent finished moving
	if (mTimeInState < currentComboAttack->mDisplacement.y )
	{

		if (!castedMachine->mIsInTheAir) {

			//reduce the velocity by the time we have spent
			double current = (sin(mTimeInState * (3.14 / 2 * currentComboAttack->mDisplacement.y) + (3 * 3.14) / 2));

			castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x * current * current;
			if (abs(castedMachine->CurrentVelocity) < 0)
				castedMachine->CurrentVelocity = 0;
		}
		
		std::cout << castedMachine->CurrentVelocity << std::endl;
		castedMachine->UpdatePosition();
		castedMachine->DisplaceCombo(castedMachine->mHitbox, currentComboAttack->mDisplacement.x);
	}
	else
		castedMachine->CurrentVelocity = 0;

	if (mTimeInState > currentComboAttack->minputMargin.x&& mTimeInState < currentComboAttack->minputMargin.y)
		castedMachine->GetCombatInput();

	if (mTimeInState > currentComboAttack->mTime) 
	{
		if (mTimeInState > currentComboAttack->mTime + currentComboAttack->mRecovery) 
		{
			//if there are no more input, end the combo
			if (castedMachine && castedMachine->InputBuffer.size() < 3) 
			{
				castedMachine->EndCombo();
				return;
			}

			if (castedMachine->InputBuffer[2] == CombatInputs::Invalid) 
			{
				castedMachine->EndCombo();
				return;
			}
			else
				mOwnerStateMachine->ChangeState("PlayerAttack3");
		}
	}
}

void PlayerAttack2::LogicExit()
{
	if (castedMachine)
		castedMachine->previousAttack = currentAttack;

	if (currentComboAttack && currentComboAttack->mLightTransfer && currentComboAttack->mLightTransfer == ComboList::ComboEnd) 
		castedMachine->EndCombo();

	if (castedMachine->mHitbox)
		castedMachine->mHitbox->SetEnabled(false);
}


PlayerAttack3::PlayerAttack3(const char* name) : State(name)
{}

void PlayerAttack3::Initialize()
{
	castedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
}

void PlayerAttack3::LogicEnter()
{
	concat = false;
	mTimeInState = 0.0f;
	//retrieve from input bufer

	ComboList& ifHeavy = castedMachine->mAllAttacks[castedMachine->previousAttack].mHeavyTransfer;
	ComboList& ifLight = castedMachine->mAllAttacks[castedMachine->previousAttack].mLightTransfer;

	CombatInputs Input = castedMachine->InputBuffer[2];

	currentComboAttack = (Input == CombatInputs::LightAttack) ?
		&castedMachine->mAllAttacks[ifLight] :
		&castedMachine->mAllAttacks[ifHeavy];


	currentAttack = (Input == CombatInputs::LightAttack) ?
		ifLight :
		ifHeavy;
	if (!castedMachine->mIsInTheAir)
	castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x;

	if (castedMachine->mySkelRend) 
	{
		if (currentAttack == ComboList::HeavyNormal3) 
		{
			//case when the animation can be one between two
			castedMachine->lastHitAnimIndex = !castedMachine->lastHitAnimIndex;
			castedMachine->mySkelRend->SetCurrentAnimation(castedMachine->lastHitAnims[castedMachine->lastHitAnimIndex]);
		}
		else 
			castedMachine->mySkelRend->SetCurrentAnimation(currentComboAttack->mAnimation.c_str());
	}
}

void PlayerAttack3::LogicUpdate()
{
	//if we need to put the hitbox
	if (mTimeInState > currentComboAttack->mDelay) 
	{
		if (castedMachine->mHitbox )
		{
			glm::vec3 _TargetPosition = castedMachine->pTransform->GetWorldPosition();
			_TargetPosition.y += castedMachine->pTransform->GetWorldScale().y / 2;
			_TargetPosition.y += 50;
			_TargetPosition.x += (castedMachine->FacingRight) ? 50 : -50;
			_TargetPosition.x += ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mScale.x / 2;
			_TargetPosition.z = 0;

			castedMachine->UpdateHitbox(_TargetPosition, currentComboAttack->mScale, currentComboAttack->mDamage, currentComboAttack->mKnockback);
			castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x;
		}
	}
	if (mTimeInState > currentComboAttack->mTime) 
	{
		if (!concat)
			castedMachine->EndCombo();
		else 
			mOwnerStateMachine->ChangeState("PlayerAttack1");
		return;
	}

	//attack displacement
	//if we havent finished moving
	if (mTimeInState < currentComboAttack->mDisplacement.y )
	{
		if (!castedMachine->mIsInTheAir) {

			double current =   (sin(mTimeInState * (3.14 /  2 * currentComboAttack->mDisplacement.y) + (3 * 3.14) / 2));

			castedMachine->CurrentVelocity = ((castedMachine->FacingRight) ? 1 : -1) * currentComboAttack->mDisplacement.x * current * current;
			if (abs(castedMachine->CurrentVelocity) < 0)
				castedMachine->CurrentVelocity = 0;
		}

		castedMachine->UpdatePosition();
		if (!castedMachine->mIsInTheAir)

		castedMachine->DisplaceCombo(castedMachine->mHitbox, currentComboAttack->mDisplacement.x);
	}

	if (mTimeInState > currentComboAttack->minputMargin.x&& mTimeInState < currentComboAttack->minputMargin.y) 
	{
		//retrieve input
		if (castedMachine->GetCombatInput() != CombatInputs::Invalid && concat == false && castedMachine->canJump) 
		{
			//we are repeating some atack, therefore
			//1 save which is the one we have introduced
			CombatInputs lastinput = castedMachine->InputBuffer[castedMachine->InputBuffer.size()-1];

			//clear the inut buffer to re use it
			castedMachine->InputBuffer.clear();

			//set the variable to concat
			concat = true;

			//add the one we added to the previous buffer to the new one
			castedMachine->InputBuffer.push_back(lastinput);
		}
	}
}

void PlayerAttack3::LogicExit()
{
	castedMachine->previousAttack = currentAttack;
	castedMachine->mHitbox->SetEnabled(false);
}
