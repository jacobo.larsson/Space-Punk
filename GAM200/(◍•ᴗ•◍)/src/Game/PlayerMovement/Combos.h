#pragma once



enum ComboList {

	LightNormal1,
	LightNormal2,
	LightNormal3,
	HeavyNormal3,
	UpwardsHeavy2,
	PointBlankNormal3,
	HeavyerHeavy3,
	SpammedHeavy1,
	ComboStart,
	ComboEnd

};

struct ComboAttack {

	ComboAttack() {}
	ComboAttack(bool LightTriggered, float time, float recovery, glm::vec2 inputMargin, glm::vec2 knockback,glm::vec2 scale, glm::vec2 Displacement, ComboList LightTransfer, ComboList HeavyTransfer,float dmg, float delay, std::string anim = "") : mTriggeredByLight(LightTriggered),
																							mTime(time),
																							mScale(scale),
																							mDisplacement(Displacement),
																							mRecovery(recovery),
																							mKnockback(knockback),
																							minputMargin(inputMargin),
																							mLightTransfer(LightTransfer),
																							mHeavyTransfer(HeavyTransfer),
																							mDamage(dmg),
																							mDelay(delay),
																							mAnimation(anim) {}



	//attack that enters this combo (Light or  heavy)
	bool mTriggeredByLight;  //true -> light, false -> heavy

	float mDamage;

	float mDelay;

	float mTime; //time it lasts from start to end

	float mRecovery; //time from the end of the attack (if there is no combo following) until the player can move / attack again.

	glm::vec2 minputMargin; //first and end of the time in the combo whe we can register new input
	
	glm::vec2 mKnockback; //knockback it applies to the enemies

	glm::vec2 mScale; //scale of the hitbox

	glm::vec2 mDisplacement; // x is velocity, y is time

	//To What transfers with a heavy and a light attack
	ComboList mLightTransfer, mHeavyTransfer;

	std::string mAnimation;

};