#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/RayCaster.h"
#include "../../StateMachines/SuperState/SuperState.h"

#include "MovementStates.h"
#include "CombatStates.h"
#include "Combos.h"

class RigidBody;
class TransformComponent;
class SkeletonAnimComp;
class SkeletonRenderable;
class Renderable;
class Health;
class ParticleEmitter;
class CameraMachine;
class Hitbox;
class Collider;

enum CombatInputs {
	Invalid,
	LightAttack,
	HeavyAttack,
};



class MovementMachine : public StateMachine
{
	SM_RTTI_DECL(MovementMachine, StateMachine);

public:
	MovementMachine();
	~MovementMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	MovementMachine& operator=(const MovementMachine& rhs);
	MovementMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;
	void OnCollisionPersisted(GameObject* other) override;
	void OnCollisionEnded(GameObject* other) override;
	bool IsAbovePlatfrom(GameObject *other);
	void Accelerate(float acc);
	void UpdatePosition();
	void BrakeX();
	CombatInputs GetCombatInput();
	void EndCombo();
	void DisplaceCombo(GameObject* hitbox, float velocity);

	void UpdateHitbox(glm::vec3& _Position, glm::vec2& _Scale, float _Damage, glm::vec2 & _Knocback);

	void DashUpdate();


	float MaxXVelocity = 500.0f;
	float CurrentVelocity = 0;
	float yVelocity = 0;
	float yMaxVelocity = 400.0f;
	float Acceleration = 1000.0f;
	bool FacingRight = true;
	bool PreviousFacingRight = true;
	bool canJump = true;
	bool hasVelocity = false;
	float JumpVelocity = 700;

	
	bool shielding = false;
	bool mDashing = false;

	bool hasattackedintheair = false;

	// Player components
	TransformComponent* pTransform = nullptr;
	RigidBody* pRigidbody = nullptr;
	Collider* pCollider = nullptr;
	RayCaster* pRaycaster = nullptr;
	SkeletonRenderable* mySkelRend = nullptr;
	Health* mHealth = nullptr;
	ParticleEmitter* mParticleEmitter = nullptr;
	Ray* PlayerDownRay = nullptr;
	Ray* PlayerDown2Ray = nullptr;
	Ray* PlayerDown3Ray = nullptr;
	Ray* PlayerLeftRay = nullptr;
	Ray* PlayerRightRay = nullptr;

	std::vector<CombatInputs> InputBuffer;
	ComboList previousAttack = ComboList::ComboStart;

	ComboAttack mAllAttacks[ComboList::ComboEnd - 1];
	float comboSpeed;

	float attackCd = 0.0f;
	float mBlasterCooldown = 0.0f;
	float mBlasterCounter = 0.0f;
	float mBlasterRange = 0.0f;

	// Current platform
	GameObject* mPlatform = nullptr;

	// Hitbox object to attack enemies
	GameObject* mHitbox = nullptr;
	TransformComponent* mHitboxTransform = nullptr;
	Hitbox* mHitboxComp = nullptr;

	int lastHitAnimIndex = 0;
	const char* lastHitAnims[2];// {"Nero_Heavy_Sword", "Nero_Heavy_Sword_2"}

	//For receiving damage
	float mDamageReceived = 0;
	float mKnockbackTime = 0;
	float mKnockbackSpeed = 0;

	float DashCooldown = 1;
	float currentDashCooldown = 0;

	float mInvulnerability = 0;

	bool mIsInTheAir = false;
	float mPersistentVelocity;

	CameraMachine* mCameraMachine = nullptr;

	// Hoverboard area entering flag
	bool mHoverboardTransition = false;
	bool HasJumped = false;



	//Dash related
	int mDashesAvailable = 3;
	int mcurrentDashCharge = 0; //0 to 1

	GameObject* mDashbars[3];

	TransformComponent* DBTransforms[3];

};
