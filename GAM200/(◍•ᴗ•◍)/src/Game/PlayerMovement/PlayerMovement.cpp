#include <iostream>

#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h" //Input for movement
#include "../../Factory/Factory.h" 

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Collisions/ICollider.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Game/HoverBoard/HoverBoard.h"

#include "../../Time/Time.h"

#include "../GameInput/GameInput.h"
#include "../../Animation/SM_Animation.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../Graphics/Particle System/ParticleSystem.h"

#include "../../RayCast/Raycast.h"
#include "../../RayCast/RaycastTest.h"

#include "../../Archetype/ArchetypeFunctions.h"

#include "MovementStates.h"
#include "PlayerMovement.h"
#include "../Hitbox/Hitbox.h"
#include "../Health/Health.h"

#include "../CameraFollow/CameraMachine.h"
#include "../../Space/Space.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"

#pragma region MovementMachine

MovementMachine::MovementMachine() {

	mName = "MovementMachine";

	mInitialState = new PlayerIdle("PlayerIdle");
	mCurrentState = mInitialState;

	AddState(mInitialState);
	AddState(new PlayerMoving("PlayerMoving"));
	AddState(new PlayerJumping("PlayerJumping"));
	AddState(new PlayerDashingFast("PlayerDashingFast"));
	AddState(new PlayerDashRecovering("PlayerDashRecovering"));
	AddState(new PlayerAttack1("PlayerAttack1"));
	AddState(new PlayerAttack2("PlayerAttack2"));
	AddState(new PlayerAttack3("PlayerAttack3"));
	AddState(new PlayerShielding("PlayerShielding"));
	AddState(new PlayerShoot("PlayerShoot"));
	AddState(new HoverBoardState("HoverBoardState"));
	AddState(new PlayerPurification("PlayerPurification"));
	AddState(new PlayerDamaged("PlayerDamaged"));
	AddState(new PlayerDie("PlayerDie"));
	

	//bool												LightTriggered  time   recovery  inputMargin,      knockback sca           Displacement             LightTransfer                HeavyTransfer
	mAllAttacks[ComboList::LightNormal1] = ComboAttack(true, 0.23f, 0.1f, glm::vec2(0, 0.7), glm::vec2(0, 0), glm::vec2(50, 15), glm::vec2(60, .1f), ComboList::LightNormal2, ComboList::PointBlankNormal3,0,0, "Nero_Combo_Frames/Nero_Combo_Light01");
	mAllAttacks[ComboList::LightNormal2] = ComboAttack(true, 0.23f, 0.07f, glm::vec2(0, 0.5), glm::vec2(0, 0), glm::vec2(50, 15), glm::vec2(60, .1f), ComboList::HeavyNormal3, ComboList::HeavyerHeavy3, 0, 0,  "Nero_Combo_Frames/Nero_Combo_Light02");
	mAllAttacks[ComboList::HeavyNormal3] = ComboAttack(true, 0.5f, 0.3f, glm::vec2(0, 0.5), glm::vec2(10, 0), glm::vec2(70, 40), glm::vec2(230, 0.15f), ComboList::ComboEnd, ComboList::ComboEnd, 0, 0, "Nero_Heavy_Sword");
	mAllAttacks[ComboList::PointBlankNormal3] = ComboAttack(true, 0.5f, 0.3f, glm::vec2(0, 0), glm::vec2(10.0f, 0.f), glm::vec2(150, 20), glm::vec2(-400, 0.15f), ComboList::ComboEnd, ComboList::ComboEnd, 0, 0, "Nero_Blaster_Combo");
	mAllAttacks[ComboList::HeavyerHeavy3] = ComboAttack(false, 0.4f, 0.25f, glm::vec2(0, 0), glm::vec2(10.f, 0.0f), glm::vec2(50, 70), glm::vec2(300, 0.25f), ComboList::ComboEnd, ComboList::ComboEnd, 0, 0, "Nero_Combo_Frames/Nero_Combo_Heavier_Heavy");
	mAllAttacks[ComboList::SpammedHeavy1] = ComboAttack(false, 0.5f, 0.3f, glm::vec2(0, 0), glm::vec2(10.0f, 0.0f), glm::vec2(50, 70), glm::vec2(30, 3), ComboList::LightNormal1, ComboList::SpammedHeavy1, 0, 0, "Nero_Heavy_Sword_2");


	lastHitAnims[0] = "Nero_Combo_Frames/Nero_Combo_Heavy_Upwards";
	lastHitAnims[1] = "Nero_Combo_Frames/Nero_Combo_Downwards_Heavy";
}

MovementMachine::~MovementMachine() { StateMachine::~StateMachine(); }

void MovementMachine::Initialize()
{
	srand(time(0));
	
	// Get player's components
	pTransform		 = mActor->GetComp<TransformComponent>();
	pRigidbody		 = mActor->GetComp<RigidBody>();
	pCollider		 = mActor->GetComp<Collider>();
	pRaycaster		 = mActor->GetComp<RayCaster>();
	mySkelRend		 = mActor->GetComp<SkeletonRenderable>();
	mHealth			 = mActor->GetComp<Health>();
	mParticleEmitter = mActor->GetComp<ParticleEmitter>();

	// Get the rays of the player
	PlayerDownRay  = pRaycaster->GetRay("Down");
	PlayerDown2Ray = pRaycaster->GetRay("Down2");
	PlayerDown3Ray = pRaycaster->GetRay("Down3");
	PlayerLeftRay  = pRaycaster->GetRay("Left");
	PlayerRightRay = pRaycaster->GetRay("Right");

	//Mix values
	if (mySkelRend) {

		if (mySkelRend->AnimationExists("Nero_Idle") && mySkelRend->AnimationExists("Nero_Hoverboard/Nero_Hoverboard_Idle"));
		mySkelRend->SetMixValue("Nero_Idle", "Nero_Hoverboard/Nero_Hoverboard_Idle", 0.3);

		if (mySkelRend->AnimationExists("Nero_Run") && mySkelRend->AnimationExists("Nero_Walk"));
		mySkelRend->SetMixValue("Nero_Run", "Nero_Walk", 0.2);

		if (mySkelRend->AnimationExists("Nero_Walk") && mySkelRend->AnimationExists("Nero_Run"));
		mySkelRend->SetMixValue("Nero_Walk", "Nero_Run", 0.2);

		if (mySkelRend->AnimationExists("Nero_Idle") && mySkelRend->AnimationExists("Nero_Combo_Frames/Nero_Combo_Light01"));
		mySkelRend->SetMixValue("Nero_Idle", "Nero_Combo_Frames/Nero_Combo_Light01", 0.07);

		if (mySkelRend->AnimationExists("Nero_Dash") && mySkelRend->AnimationExists("Nero_Idle"));
		mySkelRend->SetMixValue("Nero_Dash", "Nero_Idle", 0.2);

		if (mySkelRend->AnimationExists("Nero_Dash") && mySkelRend->AnimationExists("Nero_Run"));
		mySkelRend->SetMixValue("Nero_Dash", "Nero_Run", 0.2);

		if (mySkelRend->AnimationExists("Nero_Dash") && mySkelRend->AnimationExists("Nero_Walk"));
		mySkelRend->SetMixValue("Nero_Dash", "Nero_Walk", 0.2);


		if (mySkelRend->AnimationExists("Nero_Jump/Nero_Jump_Peak3") && mySkelRend->AnimationExists("Nero_Jump/Nero_Jump_Down4"));
		mySkelRend->SetMixValue("Nero_Jump/Nero_Jump_Peak3", "Nero_Jump/Nero_Jump_Down4", 0.2);


		if (mySkelRend->AnimationExists("Nero_Combo_Frames/Nero_Combo_Light01") && mySkelRend->AnimationExists("Nero_Combo_Frames/Nero_Combo_Light02"));
		mySkelRend->SetMixValue("Nero_Combo_Frames/Nero_Combo_Light01", "Nero_Combo_Frames/Nero_Combo_Light02", 0.07);

		if (mySkelRend->AnimationExists("Nero_Damaged") && mySkelRend->AnimationExists("Nero_Idle"));
		mySkelRend->SetMixValue("Nero_Damaged", "Nero_Idle", 0.02);


		mySkelRend->SetCurrentAnimation("Nero_Idle", true);
	}

	//Initialize the re usable hitbox
	mHitbox = CreateFromArchetype("Hitbox",
		mActor->GetParentSpace(),
		 glm::vec3(9999, 9999, 9999) + glm::vec3(FacingRight ? 50 : -50, 0, 0));
	// Initialize the hitbox
	if (mHitbox != nullptr)
	{
		mHitbox->SetEnabled(false);
		mHitboxTransform = mHitbox->GetComp<TransformComponent>();
		mHitboxComp = mHitbox->GetComp<Hitbox>();
	}

	// Disable the particle emitter
	if (mParticleEmitter) 
		mParticleEmitter->mActiveSystem = false;

	mCameraMachine = reinterpret_cast<CameraMachine * >(mActor->GetParentSpace()->FindObjectByName("Camera")->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));

	StateMachine::Initialize();

	//mySkelRend->SetMixValue("Nero_Dashing", )
	//Dash bars
	//mDashbars[0] = CreateFromArchetype("DashBar", mActor->GetParentSpace());
	//mDashbars[1] = CreateFromArchetype("DashBar", mActor->GetParentSpace());
	//mDashbars[2] = CreateFromArchetype("DashBar", mActor->GetParentSpace());
	//
	//
	//DBTransforms[0] = mDashbars[0]->GetComp<TransformComponent>();
	//DBTransforms[1] = mDashbars[1]->GetComp<TransformComponent>();
	//DBTransforms[2] = mDashbars[2]->GetComp<TransformComponent>();
	//
	//DBTransforms[0]->SetWorldPosition(pTransform->GetWorldPosition());
	//DBTransforms[1]->SetWorldPosition(pTransform->GetWorldPosition());
	//DBTransforms[2]->SetWorldPosition(pTransform->GetWorldPosition());
	//
	//DBTransforms[0]->SetWorldPosition(DBTransforms[0]->GetWorldPosition() + glm::vec3(-50, 100, 0));
	//DBTransforms[1]->SetWorldPosition(DBTransforms[1]->GetWorldPosition() + glm::vec3(0, 100, 0));
	//DBTransforms[2]->SetWorldPosition(DBTransforms[2]->GetWorldPosition() + glm::vec3(50, 100, 0));


}

void MovementMachine::Update()
{
	// Move the hitbox to the player's position
	if(mHitboxTransform)
		mHitboxTransform->SetWorldPosition(pTransform->GetWorldPosition());

	if (mPlatform)
		canJump = IsAbovePlatfrom(mPlatform);

	mBlasterCounter += (float)TimeSys->GetFrameTime();

	if (mInvulnerability > 0) 
	{
		mInvulnerability -= TimeSys->GetFrameTime();
		if (mInvulnerability < 0) 
			mInvulnerability = 0;
	}

	if (currentDashCooldown > 0) 
	{
		currentDashCooldown -= TimeSys->GetFrameTime();
		if (currentDashCooldown < 0)
			currentDashCooldown = 0;
	}

	if (mHealth && mHealth->GetHealth() <= 0) 
		ChangeState("PlayerDie");


	StateMachine::Update();
	if ((GameInput::HeavyKeyPressed() || GameInput::LightKeyPressed()) && currentDashCooldown > 0) {
		previousAttack = ComboList::LightNormal2;
		InputBuffer.clear();
		InputBuffer.push_back(CombatInputs::LightAttack);
		InputBuffer.push_back(CombatInputs::LightAttack);
		InputBuffer.push_back(CombatInputs::HeavyAttack);
		ChangeState("PlayerAttack3");
	}

	DashUpdate();
}

void MovementMachine::Shutdown()
{
	StateMachine::Clear();
}

bool MovementMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		ImGui::PushID(&MaxXVelocity);
		ImGui::DragFloat("Max X velocity", &MaxXVelocity);
		ImGui::PopID();

		ImGui::PushID(&yMaxVelocity);
		ImGui::DragFloat("Max Y velocity", &yMaxVelocity);
		ImGui::PopID();

		ImGui::PushID(&Acceleration);
		ImGui::DragFloat("Acceleration", &Acceleration);
		ImGui::PopID();

		ImGui::PushID(&mBlasterCooldown);
		ImGui::DragFloat("Blaster Cooldown", &mBlasterCooldown);
		ImGui::PopID();

		ImGui::PushID(&mBlasterRange);
		ImGui::DragFloat("Blaster Range", &mBlasterRange);
		ImGui::PopID();

		ImGui::PushID(&JumpVelocity);
		ImGui::DragFloat("JumpSpeed", &JumpVelocity);
		ImGui::PopID();

		if (ImGui::CollapsingHeader("Combos")) {

			if (ImGui::CollapsingHeader("LightNormal1")) {

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::LightNormal1].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::LightNormal1].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal1].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::LightNormal1].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::LightNormal1].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::LightNormal1].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::LightNormal1].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal1].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::LightNormal1].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::LightNormal1].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::LightNormal1].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal1].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal1].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("LightNormal2")) {

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::LightNormal2].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::LightNormal2].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal2].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::LightNormal2].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::LightNormal2].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::LightNormal2].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::LightNormal2].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal2].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::LightNormal2].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::LightNormal2].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::LightNormal2].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::LightNormal2].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::LightNormal2].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("HeavyNormal3")) {

				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::HeavyNormal3].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::HeavyNormal3].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyNormal3].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::HeavyNormal3].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::HeavyNormal3].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::HeavyNormal3].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::HeavyNormal3].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyNormal3].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::HeavyNormal3].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::HeavyNormal3].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::HeavyNormal3].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::HeavyNormal3].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyNormal3].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("UpwardsHeavy2")) {

				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::UpwardsHeavy2].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::UpwardsHeavy2].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::UpwardsHeavy2].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::UpwardsHeavy2].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::UpwardsHeavy2].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::UpwardsHeavy2].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.y);
				ImGui::PopID();
			ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("PointBlankNormal3")) {

				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::PointBlankNormal3].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::PointBlankNormal3].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::PointBlankNormal3].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::PointBlankNormal3].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::PointBlankNormal3].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::PointBlankNormal3].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::PointBlankNormal3].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::PointBlankNormal3].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::PointBlankNormal3].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::PointBlankNormal3].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("HeavyerHeavy3")) {

				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::HeavyerHeavy3].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::HeavyerHeavy3].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyerHeavy3].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::HeavyerHeavy3].mRecovery);
				ImGui::PopID();

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::HeavyerHeavy3].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::HeavyerHeavy3].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);

				ImGui::Dummy(ImVec2(0, 5));

				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();

				ImGui::PushID(&mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
			if (ImGui::CollapsingHeader("SpammedHeavy1")) {

				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mDamage);
				ImGui::DragFloat("Damage", &mAllAttacks[ComboList::SpammedHeavy1].mDamage);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mDelay);
				ImGui::DragFloat("Delay", &mAllAttacks[ComboList::SpammedHeavy1].mDelay);
				ImGui::PopID();

				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mTime);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::SpammedHeavy1].mTime);
				ImGui::PopID();
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mRecovery);
				ImGui::DragFloat("Recovery", &mAllAttacks[ComboList::SpammedHeavy1].mRecovery);
				ImGui::PopID();


				ImGui::Dummy(ImVec2(0, 5));


				ImGui::Text("InputMargin");
				ImGui::Columns(2, "InputMargin", true);
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].minputMargin.x);
				ImGui::DragFloat("Start", &mAllAttacks[ComboList::SpammedHeavy1].minputMargin.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].minputMargin.y);
				ImGui::DragFloat("End", &mAllAttacks[ComboList::SpammedHeavy1].minputMargin.y);
				ImGui::PopID();
				ImGui::Columns(1);


				ImGui::Dummy(ImVec2(0, 5));


				ImGui::Text("Knockback");
				ImGui::Columns(2, "Knockback", true);
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mKnockback.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::SpammedHeavy1].mKnockback.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mKnockback.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::SpammedHeavy1].mKnockback.y);
				ImGui::PopID();
				ImGui::Columns(1);


				ImGui::Dummy(ImVec2(0, 5));


				ImGui::Text("Hitbox Scale");
				ImGui::Columns(2, "Hitbox scale", true);
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mScale.x);
				ImGui::DragFloat("x", &mAllAttacks[ComboList::SpammedHeavy1].mScale.x);
				ImGui::PopID();
				ImGui::NextColumn();
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mScale.y);
				ImGui::DragFloat("y", &mAllAttacks[ComboList::SpammedHeavy1].mScale.y);
				ImGui::PopID();
				ImGui::Columns(1);


				ImGui::Dummy(ImVec2(0, 5));


				ImGui::Text("Displacement");
				ImGui::Columns(2, "Displacement", true);
				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.x);
				ImGui::DragFloat("Speed", &mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.x);
				ImGui::PopID();
				ImGui::NextColumn();


				ImGui::PushID(&mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.y);
				ImGui::DragFloat("Time", &mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.y);
				ImGui::PopID();
				ImGui::Columns(1);
			}
		}
		ImGui::End();
	}
	else
		ImGui::End();
	return false;
}

MovementMachine& MovementMachine::operator=(const MovementMachine& rhs)
{
	MaxXVelocity = rhs.MaxXVelocity;
	CurrentVelocity = rhs.CurrentVelocity;
	yVelocity = rhs.yVelocity;
	yMaxVelocity = rhs.yMaxVelocity;
	Acceleration = rhs.Acceleration;

	FacingRight = rhs.FacingRight;
	PreviousFacingRight = rhs.PreviousFacingRight;
	canJump = rhs.canJump;

	return *this;
}

MovementMachine* MovementMachine::Clone()
{
	MovementMachine* temp = FactorySys->Create<MovementMachine>();
	*temp = *this;
	return temp;
}

void MovementMachine::ToJson(nlohmann::json& j) {
	IBase::ToJson(j);

	j["MaxXVelocity"] = MaxXVelocity;
	j["MaxYVelocity"] = yMaxVelocity;
	j["Acceleration"] = Acceleration;
	j["mBlasterCooldown"] = mBlasterCooldown;
	j["mBlasterRange"] = mBlasterRange;

	//combos
	j["LightNormal1Damage"] = mAllAttacks[ComboList::LightNormal1].mDamage;
	j["LightNormal1Delay"] = mAllAttacks[ComboList::LightNormal1].mDelay;
	j["LightNormal1Time"] = mAllAttacks[ComboList::LightNormal1].mTime;
	j["LightNormal1Recovery"] = mAllAttacks[ComboList::LightNormal1].mRecovery;
	j["LightNormal1InputMarginx"] = mAllAttacks[ComboList::LightNormal1].minputMargin.x;
	j["LightNormal1InputMarginy"] = mAllAttacks[ComboList::LightNormal1].minputMargin.y;
	j["LightNormal1Knockbackx"] = mAllAttacks[ComboList::LightNormal1].mKnockback.x;
	j["LightNormal1Knockbacky"] = mAllAttacks[ComboList::LightNormal1].mKnockback.y;
	j["LightNormal1Scalex"] = mAllAttacks[ComboList::LightNormal1].mScale.x;
	j["LightNormal1Scaley"] = mAllAttacks[ComboList::LightNormal1].mScale.y;
	j["LightNormal1Displacementx"] = mAllAttacks[ComboList::LightNormal1].mDisplacement.x;
	j["LightNormal1Displacementy"] = mAllAttacks[ComboList::LightNormal1].mDisplacement.y;

	j["LightNormal2Damage"] = mAllAttacks[ComboList::LightNormal2].mDamage;
	j["LightNormal2Delay"] = mAllAttacks[ComboList::LightNormal2].mDelay;
	j["LightNormal2Time"] = mAllAttacks[ComboList::LightNormal2].mTime;
	j["LightNormal2Recovery"] = mAllAttacks[ComboList::LightNormal2].mRecovery;
	j["LightNormal2InputMarginx"] = mAllAttacks[ComboList::LightNormal2].minputMargin.x;
	j["LightNormal2InputMarginy"] = mAllAttacks[ComboList::LightNormal2].minputMargin.y;
	j["LightNormal2Knockbackx"] = mAllAttacks[ComboList::LightNormal2].mKnockback.x;
	j["LightNormal2Knockbacky"] = mAllAttacks[ComboList::LightNormal2].mKnockback.y;
	j["LightNormal2Scalex"] = mAllAttacks[ComboList::LightNormal2].mScale.x;
	j["LightNormal2Scaley"] = mAllAttacks[ComboList::LightNormal2].mScale.y;
	j["LightNormal2Displacementx"] = mAllAttacks[ComboList::LightNormal2].mDisplacement.x;
	j["LightNormal2Displacementy"] = mAllAttacks[ComboList::LightNormal2].mDisplacement.y;


	j["LightNormal3Damage"] = mAllAttacks[ComboList::LightNormal3].mDamage;
	j["LightNormal3Delay"] = mAllAttacks[ComboList::LightNormal3].mDelay;
	j["LightNormal3Time"] = mAllAttacks[ComboList::LightNormal3].mTime;
	j["LightNormal3Recovery"] = mAllAttacks[ComboList::LightNormal3].mRecovery;
	j["LightNormal3InputMarginx"] = mAllAttacks[ComboList::LightNormal3].minputMargin.x;
	j["LightNormal3InputMarginy"] = mAllAttacks[ComboList::LightNormal3].minputMargin.y;
	j["LightNormal3Knockbackx"] = mAllAttacks[ComboList::LightNormal3].mKnockback.x;
	j["LightNormal3Knockbacky"] = mAllAttacks[ComboList::LightNormal3].mKnockback.y;
	j["LightNormal3Scalex"] = mAllAttacks[ComboList::LightNormal3].mScale.x;
	j["LightNormal3Scaley"] = mAllAttacks[ComboList::LightNormal3].mScale.y;
	j["LightNormal3Displacementx"] = mAllAttacks[ComboList::LightNormal3].mDisplacement.x;
	j["LightNormal3Displacementy"] = mAllAttacks[ComboList::LightNormal3].mDisplacement.y;


	j["HeavyNormal3Damage"] = mAllAttacks[ComboList::HeavyNormal3].mDamage;
	j["HeavyNormal3Delay"] = mAllAttacks[ComboList::HeavyNormal3].mDelay;
	j["HeavyNormal3Time"] = mAllAttacks[ComboList::HeavyNormal3].mTime;
	j["HeavyNormal3Recovery"] = mAllAttacks[ComboList::HeavyNormal3].mRecovery;
	j["HeavyNormal3InputMarginx"] = mAllAttacks[ComboList::HeavyNormal3].minputMargin.x;
	j["HeavyNormal3InputMarginy"] = mAllAttacks[ComboList::HeavyNormal3].minputMargin.y;
	j["HeavyNormal3Knockbackx"] = mAllAttacks[ComboList::HeavyNormal3].mKnockback.x;
	j["HeavyNormal3Knockbacky"] = mAllAttacks[ComboList::HeavyNormal3].mKnockback.y;
	j["HeavyNormal3Scalex"] = mAllAttacks[ComboList::HeavyNormal3].mScale.x;
	j["HeavyNormal3Scaley"] = mAllAttacks[ComboList::HeavyNormal3].mScale.y;
	j["HeavyNormal3Displacementx"] = mAllAttacks[ComboList::HeavyNormal3].mDisplacement.x;
	j["HeavyNormal3Displacementy"] = mAllAttacks[ComboList::HeavyNormal3].mDisplacement.y;


	j["UpwardsHeavy2Damage"] = mAllAttacks[ComboList::UpwardsHeavy2].mDamage;
	j["UpwardsHeavy2Delay"] = mAllAttacks[ComboList::UpwardsHeavy2].mDelay;
	j["UpwardsHeavy2Time"] = mAllAttacks[ComboList::UpwardsHeavy2].mTime;
	j["UpwardsHeavy2Recovery"] = mAllAttacks[ComboList::UpwardsHeavy2].mRecovery;
	j["UpwardsHeavy2InputMarginx"] = mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.x;
	j["UpwardsHeavy2InputMarginy"] = mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.y;
	j["UpwardsHeavy2Knockbackx"] = mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.x;
	j["UpwardsHeavy2Knockbacky"] = mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.y;
	j["UpwardsHeavy2Scalex"] = mAllAttacks[ComboList::UpwardsHeavy2].mScale.x;
	j["UpwardsHeavy2Scaley"] = mAllAttacks[ComboList::UpwardsHeavy2].mScale.y;
	j["UpwardsHeavy2Displacementx"] = mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.x;
	j["UpwardsHeavy2Displacementy"] = mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.y;


	j["PointBlankNormal3Damage"] = mAllAttacks[ComboList::PointBlankNormal3].mDamage;
	j["PointBlankNormal3Delay"] = mAllAttacks[ComboList::PointBlankNormal3].mDelay;
	j["PointBlankNormal3Time"] = mAllAttacks[ComboList::PointBlankNormal3].mTime;
	j["PointBlankNormal3Recovery"] = mAllAttacks[ComboList::PointBlankNormal3].mRecovery;
	j["PointBlankNormal3InputMarginx"] = mAllAttacks[ComboList::PointBlankNormal3].minputMargin.x;
	j["PointBlankNormal3InputMarginy"] = mAllAttacks[ComboList::PointBlankNormal3].minputMargin.y;
	j["PointBlankNormal3Knockbackx"] = mAllAttacks[ComboList::PointBlankNormal3].mKnockback.x;
	j["PointBlankNormal3Knockbacky"] = mAllAttacks[ComboList::PointBlankNormal3].mKnockback.y;
	j["PointBlankNormal3Scalex"] = mAllAttacks[ComboList::PointBlankNormal3].mScale.x;
	j["PointBlankNormal3Scaley"] = mAllAttacks[ComboList::PointBlankNormal3].mScale.y;
	j["PointBlankNormal3Displacementx"] = mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.x;
	j["PointBlankNormal3Displacementy"] = mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.y;


	j["HeavyerHeavy3Damage"] = mAllAttacks[ComboList::HeavyerHeavy3].mDamage;
	j["HeavyerHeavy3Delay"] = mAllAttacks[ComboList::HeavyerHeavy3].mDelay;
	j["HeavyerHeavy3Time"] = mAllAttacks[ComboList::HeavyerHeavy3].mTime;
	j["HeavyerHeavy3Recovery"] = mAllAttacks[ComboList::HeavyerHeavy3].mRecovery;
	j["HeavyerHeavy3InputMarginx"] = mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.x;
	j["HeavyerHeavy3InputMarginy"] = mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.y;
	j["HeavyerHeavy3Knockbackx"] = mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.x;
	j["HeavyerHeavy3Knockbacky"] = mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.y;
	j["HeavyerHeavy3Scalex"] = mAllAttacks[ComboList::HeavyerHeavy3].mScale.x;
	j["HeavyerHeavy3Scaley"] = mAllAttacks[ComboList::HeavyerHeavy3].mScale.y;
	j["HeavyerHeavy3Displacementx"] = mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.x;
	j["HeavyerHeavy3Displacementy"] = mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.y;

	j["SpammedHeavy1Damage"] = mAllAttacks[ComboList::SpammedHeavy1].mDamage;
	j["SpammedHeavy1Delay"] = mAllAttacks[ComboList::SpammedHeavy1].mDelay;
	j["SpammedHeavy1Time"] = mAllAttacks[ComboList::SpammedHeavy1].mTime;
	j["SpammedHeavy1Recovery"] = mAllAttacks[ComboList::SpammedHeavy1].mRecovery;
	j["SpammedHeavy1InputMarginx"] = mAllAttacks[ComboList::SpammedHeavy1].minputMargin.x;
	j["SpammedHeavy1InputMarginy"] = mAllAttacks[ComboList::SpammedHeavy1].minputMargin.y;
	j["SpammedHeavy1Knockbackx"] = mAllAttacks[ComboList::SpammedHeavy1].mKnockback.x;
	j["SpammedHeavy1Knockbacky"] = mAllAttacks[ComboList::SpammedHeavy1].mKnockback.y;
	j["SpammedHeavy1Scalex"] = mAllAttacks[ComboList::SpammedHeavy1].mScale.x;
	j["SpammedHeavy1Scaley"] = mAllAttacks[ComboList::SpammedHeavy1].mScale.y;
	j["SpammedHeavy1Displacementx"] = mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.x;
	j["SpammedHeavy1Displacementy"] = mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.y;
}

void MovementMachine::FromJson(const nlohmann::json& j) {
	IBase::FromJson(j);

	if (j.find("MaxXVelocity") != j.end())
		MaxXVelocity = j["MaxXVelocity"];

	if (j.find("MaxYVelocity") != j.end())
		yMaxVelocity = j["MaxYVelocity"];

	if (j.find("Acceleration") != j.end())
		Acceleration = j["Acceleration"];

	if (j.find("mBlasterCooldown") != j.end())
		mBlasterCooldown = j["mBlasterCooldown"];

	if (j.find("mBlasterRange") != j.end())
		mBlasterRange = j["mBlasterRange"];
		
	if (j.find("LightNormal1Damage") != j.end())
		mAllAttacks[ComboList::LightNormal1].mDamage = j["LightNormal1Damage"];
	if (j.find("LightNormal1Delay") != j.end())
		mAllAttacks[ComboList::LightNormal1].mDelay = j["LightNormal1Delay"];

	if (j.find("LightNormal1Time") != j.end())
		mAllAttacks[ComboList::LightNormal1].mTime = j["LightNormal1Time"];
	if (j.find("LightNormal1Recovery") != j.end())
		mAllAttacks[ComboList::LightNormal1].mRecovery = j["LightNormal1Recovery"];
	if (j.find("LightNormal1InputMarginx") != j.end())
		mAllAttacks[ComboList::LightNormal1].minputMargin.x = j["LightNormal1InputMarginx"];
	if (j.find("LightNormal1InputMarginy") != j.end())
		mAllAttacks[ComboList::LightNormal1].minputMargin.y = j["LightNormal1InputMarginy"];
	if (j.find("LightNormal1Knockbackx") != j.end())
		mAllAttacks[ComboList::LightNormal1].mKnockback.x = j["LightNormal1Knockbackx"];
	if (j.find("LightNormal1Knockbacky") != j.end())
		mAllAttacks[ComboList::LightNormal1].mKnockback.y = j["LightNormal1Knockbacky"];
	if (j.find("LightNormal1Scalex") != j.end())
		mAllAttacks[ComboList::LightNormal1].mScale.x = j["LightNormal1Scalex"];
	if (j.find("LightNormal1Scaley") != j.end())
		mAllAttacks[ComboList::LightNormal1].mScale.y = j["LightNormal1Scaley"];
	if (j.find("LightNormal1Displacementx") != j.end())
		mAllAttacks[ComboList::LightNormal1].mDisplacement.x = j["LightNormal1Displacementx"];
	if (j.find("LightNormal1Displacementy") != j.end())
		mAllAttacks[ComboList::LightNormal1].mDisplacement.y = j["LightNormal1Displacementy"];



	if (j.find("LightNormal2Damage") != j.end())
		mAllAttacks[ComboList::LightNormal2].mDamage = j["LightNormal2Damage"];
	if (j.find("LightNormal2Delay") != j.end())
		mAllAttacks[ComboList::LightNormal2].mDelay = j["LightNormal2Delay"];

	if (j.find("LightNormal2Time") != j.end())
		mAllAttacks[ComboList::LightNormal2].mTime = j["LightNormal2Time"];
	if (j.find("LightNormal2Recovery") != j.end())
		mAllAttacks[ComboList::LightNormal2].mRecovery = j["LightNormal2Recovery"];
	if (j.find("LightNormal2InputMarginx") != j.end())
		mAllAttacks[ComboList::LightNormal2].minputMargin.x = j["LightNormal2InputMarginx"];
	if (j.find("LightNormal2InputMarginy") != j.end())
		mAllAttacks[ComboList::LightNormal2].minputMargin.y = j["LightNormal2InputMarginy"];
	if (j.find("LightNormal2Knockbackx") != j.end())
		mAllAttacks[ComboList::LightNormal2].mKnockback.x = j["LightNormal2Knockbackx"];
	if (j.find("LightNormal2Knockbacky") != j.end())
		mAllAttacks[ComboList::LightNormal2].mKnockback.y = j["LightNormal2Knockbacky"];
	if (j.find("LightNormal2Scalex") != j.end())
		mAllAttacks[ComboList::LightNormal2].mScale.x = j["LightNormal2Scalex"];
	if (j.find("LightNormal2Scaley") != j.end())
		mAllAttacks[ComboList::LightNormal2].mScale.y = j["LightNormal2Scaley"];
	if (j.find("LightNormal2Displacementx") != j.end())
		mAllAttacks[ComboList::LightNormal2].mDisplacement.x = j["LightNormal2Displacementx"];
	if (j.find("LightNormal2Displacementy") != j.end())
		mAllAttacks[ComboList::LightNormal2].mDisplacement.y = j["LightNormal2Displacementy"];


	if (j.find("LightNormal3Damage") != j.end())
		mAllAttacks[ComboList::LightNormal3].mDamage = j["LightNormal3Damage"];
	if (j.find("LightNormal3Delay") != j.end())
		mAllAttacks[ComboList::LightNormal3].mDelay = j["LightNormal3Delay"];

	if (j.find("LightNormal3Time") != j.end())
		mAllAttacks[ComboList::LightNormal3].mTime = j["LightNormal3Time"];
	if (j.find("LightNormal3Recovery") != j.end())
		mAllAttacks[ComboList::LightNormal3].mRecovery = j["LightNormal3Recovery"];
	if (j.find("LightNormal3InputMarginx") != j.end())
		mAllAttacks[ComboList::LightNormal3].minputMargin.x = j["LightNormal3InputMarginx"];
	if (j.find("LightNormal3InputMarginy") != j.end())
		mAllAttacks[ComboList::LightNormal3].minputMargin.y = j["LightNormal3InputMarginy"];
	if (j.find("LightNormal3Knockbackx") != j.end())
		mAllAttacks[ComboList::LightNormal3].mKnockback.x = j["LightNormal3Knockbackx"];
	if (j.find("LightNormal3Knockbacky") != j.end())
		mAllAttacks[ComboList::LightNormal3].mKnockback.y = j["LightNormal3Knockbacky"];
	if (j.find("LightNormal3Scalex") != j.end())
		mAllAttacks[ComboList::LightNormal3].mScale.x = j["LightNormal3Scalex"];
	if (j.find("LightNormal3Scaley") != j.end())
		mAllAttacks[ComboList::LightNormal3].mScale.y = j["LightNormal3Scaley"];
	if (j.find("LightNormal3Displacementx") != j.end())
		mAllAttacks[ComboList::LightNormal3].mDisplacement.x = j["LightNormal3Displacementx"];
	if (j.find("LightNormal3Displacementy") != j.end())
		mAllAttacks[ComboList::LightNormal3].mDisplacement.y = j["LightNormal3Displacementy"];



	if (j.find("HeavyNormal3Damage") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mDamage = j["HeavyNormal3Damage"];
	if (j.find("HeavyNormal3Delay") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mDelay = j["HeavyNormal3Delay"];
	
	if (j.find("HeavyNormal3Time") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mTime = j["HeavyNormal3Time"];
	if (j.find("HeavyNormal3Recovery") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mRecovery = j["HeavyNormal3Recovery"];
	if (j.find("HeavyNormal3InputMarginx") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].minputMargin.x = j["HeavyNormal3InputMarginx"];
	if (j.find("HeavyNormal3InputMarginy") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].minputMargin.y = j["HeavyNormal3InputMarginy"];
	if (j.find("HeavyNormal3Knockbackx") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mKnockback.x = j["HeavyNormal3Knockbackx"];
	if (j.find("HeavyNormal3Knockbacky") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mKnockback.y = j["HeavyNormal3Knockbacky"];
	if (j.find("HeavyNormal3Scalex") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mScale.x = j["HeavyNormal3Scalex"];
	if (j.find("HeavyNormal3Scaley") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mScale.y = j["HeavyNormal3Scaley"];
	if (j.find("HeavyNormal3Displacementx") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mDisplacement.x = j["HeavyNormal3Displacementx"];
	if (j.find("HeavyNormal3Displacementy") != j.end())
		mAllAttacks[ComboList::HeavyNormal3].mDisplacement.y = j["HeavyNormal3Displacementy"];


	if (j.find("UpwardsHeavy2Damage") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mDamage = j["UpwardsHeavy2Damage"];
	if (j.find("UpwardsHeavy2Delay") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mDelay = j["UpwardsHeavy2Delay"];

	if (j.find("UpwardsHeavy2Time") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mTime = j["UpwardsHeavy2Time"];
	if (j.find("UpwardsHeavy2Recovery") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mRecovery = j["UpwardsHeavy2Recovery"];
	if (j.find("UpwardsHeavy2InputMarginx") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.x = j["UpwardsHeavy2InputMarginx"];
	if (j.find("UpwardsHeavy2InputMarginy") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].minputMargin.y = j["UpwardsHeavy2InputMarginy"];
	if (j.find("UpwardsHeavy2Knockbackx") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.x = j["UpwardsHeavy2Knockbackx"];
	if (j.find("UpwardsHeavy2Knockbacky") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mKnockback.y = j["UpwardsHeavy2Knockbacky"];
	if (j.find("UpwardsHeavy2Scalex") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mScale.x = j["UpwardsHeavy2Scalex"];
	if (j.find("UpwardsHeavy2Scaley") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mScale.y = j["UpwardsHeavy2Scaley"];
	if (j.find("UpwardsHeavy2Displacementx") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.x = j["UpwardsHeavy2Displacementx"];
	if (j.find("UpwardsHeavy2Displacementy") != j.end())
		mAllAttacks[ComboList::UpwardsHeavy2].mDisplacement.y = j["UpwardsHeavy2Displacementy"];


	if (j.find("PointBlankNormal3Damage") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mDamage = j["PointBlankNormal3Damage"];
	if (j.find("PointBlankNormal3Delay") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mDelay = j["PointBlankNormal3Delay"];

	if (j.find("PointBlankNormal3Time") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mTime = j["PointBlankNormal3Time"];
	if (j.find("PointBlankNormal3Recovery") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mRecovery = j["PointBlankNormal3Recovery"];
	if (j.find("PointBlankNormal3InputMarginx") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].minputMargin.x = j["PointBlankNormal3InputMarginx"];
	if (j.find("PointBlankNormal3InputMarginy") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].minputMargin.y = j["PointBlankNormal3InputMarginy"];
	if (j.find("PointBlankNormal3Knockbackx") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mKnockback.x = j["PointBlankNormal3Knockbackx"];
	if (j.find("PointBlankNormal3Knockbacky") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mKnockback.y = j["PointBlankNormal3Knockbacky"];
	if (j.find("PointBlankNormal3Scalex") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mScale.x = j["PointBlankNormal3Scalex"];
	if (j.find("PointBlankNormal3Scaley") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mScale.y = j["PointBlankNormal3Scaley"];
	if (j.find("PointBlankNormal3Displacementx") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.x = j["PointBlankNormal3Displacementx"];
	if (j.find("PointBlankNormal3Displacementy") != j.end())
		mAllAttacks[ComboList::PointBlankNormal3].mDisplacement.y = j["PointBlankNormal3Displacementy"];


	if (j.find("HeavyerHeavy3Damage") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mDamage = j["HeavyerHeavy3Damage"];
	if (j.find("HeavyerHeavy3Delay") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mDelay = j["HeavyerHeavy3Delay"];

	if (j.find("HeavyerHeavy3Time") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mTime = j["HeavyerHeavy3Time"];
	if (j.find("HeavyerHeavy3Recovery") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mRecovery = j["HeavyerHeavy3Recovery"];
	if (j.find("HeavyerHeavy3InputMarginx") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.x = j["HeavyerHeavy3InputMarginx"];
	if (j.find("HeavyerHeavy3InputMarginy") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].minputMargin.y = j["HeavyerHeavy3InputMarginy"];
	if (j.find("HeavyerHeavy3Knockbackx") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.x = j["HeavyerHeavy3Knockbackx"];
	if (j.find("HeavyerHeavy3Knockbacky") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mKnockback.y = j["HeavyerHeavy3Knockbacky"];
	if (j.find("HeavyerHeavy3Scalex") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mScale.x = j["HeavyerHeavy3Scalex"];
	if (j.find("HeavyerHeavy3Scaley") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mScale.y = j["HeavyerHeavy3Scaley"];
	if (j.find("HeavyerHeavy3Displacementx") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.x = j["HeavyerHeavy3Displacementx"];
	if (j.find("HeavyerHeavy3Displacementy") != j.end())
		mAllAttacks[ComboList::HeavyerHeavy3].mDisplacement.y = j["HeavyerHeavy3Displacementy"];


	if (j.find("SpammedHeavy1Damage") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mDamage = j["SpammedHeavy1Damage"];
	if (j.find("SpammedHeavy1Delay") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mDelay = j["SpammedHeavy1Delay"];

	if (j.find("SpammedHeavy1Time") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mTime = j["SpammedHeavy1Time"];
	if (j.find("SpammedHeavy1Recovery") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mRecovery = j["SpammedHeavy1Recovery"];
	if (j.find("SpammedHeavy1InputMarginx") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].minputMargin.x = j["SpammedHeavy1InputMarginx"];
	if (j.find("SpammedHeavy1InputMarginy") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].minputMargin.y = j["SpammedHeavy1InputMarginy"];
	if (j.find("SpammedHeavy1Knockbackx") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mKnockback.x = j["SpammedHeavy1Knockbackx"];
	if (j.find("SpammedHeavy1Knockbacky") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mKnockback.y = j["SpammedHeavy1Knockbacky"];
	if (j.find("SpammedHeavy1Scalex") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mScale.x = j["SpammedHeavy1Scalex"];
	if (j.find("SpammedHeavy1Scaley") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mScale.y = j["SpammedHeavy1Scaley"];
	if (j.find("SpammedHeavy1Displacementx") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.x = j["SpammedHeavy1Displacementx"];
	if (j.find("SpammedHeavy1Displacementy") != j.end())
		mAllAttacks[ComboList::SpammedHeavy1].mDisplacement.y = j["SpammedHeavy1Displacementy"];
}

void MovementMachine::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Enemy")) 
	{
		if (!mDashing && GetCurrentStateName() != "PlayerDie" && mInvulnerability == 0)
		{
			if (Hitbox* hb = other->GetComp<Hitbox>())
			{
				if (hb->mEnemy) 
				{
					mDamageReceived = hb->mDamage;
					mKnockbackSpeed = hb->mKSpeed;
					mKnockbackTime = hb->mKTime;

					ChangeState("PlayerDamaged");
				}
			}
		}
	}
	if (other->CompareTag("Platform"))
	{
		if (mySkelRend->GetCurrentAnimationName() == "Nero_Jump/Nero_Jump_Down4") 
		{
			std::string state = GetCurrentStateName();
			if (state == "PlayerRunning")
				mySkelRend->SetCurrentAnimation("Nero_Run");
			if (state == "PlayerIdle")
				mySkelRend->SetCurrentAnimation("Nero_Idle");
		}

		mPlatform = other;
		canJump = IsAbovePlatfrom(other);
		mIsInTheAir = false;

	}
	StateMachine::OnCollisionStarted(other);
}

void MovementMachine::OnCollisionPersisted(GameObject* other)
{
	if (other->CompareTag("Platform")) {

		mPlatform = other;
		mIsInTheAir = false;
	}
	StateMachine::OnCollisionPersisted(other);
}


void MovementMachine::OnCollisionEnded(GameObject* other)
{
	if (other->CompareTag("Platform"))
		mPlatform = nullptr;
	StateMachine::OnCollisionEnded(other);
}

bool MovementMachine::IsAbovePlatfrom(GameObject* other)
{
	//raycasta with out vector of platforms
	GameObject* RayCastObject = NULL;
	glm::vec2 collisionPoint;

	GameObject* RayCastObject2 = NULL;
	glm::vec2 collisionPoint2;

	GameObject* RayCastObject3 = NULL;
	glm::vec2 collisionPoint3;
	float distance = CheckRayWithObject(PlayerDownRay, &collisionPoint, other);
	float distance2 = CheckRayWithObject(PlayerDown2Ray, &collisionPoint2, other);
	float distance3 = CheckRayWithObject(PlayerDown3Ray, &collisionPoint3, other);

	if (distance != -1) 
	{
		if (distance2 < pTransform->GetWorldScale().y / 2 + 1) 
		{
			hasattackedintheair = false;
			return true;
		}
	}

	if (distance2 != -1) 
	{
		if (distance2 < pTransform->GetWorldScale().y / 2 + 1) 
		{
			hasattackedintheair = false;
			return true;
		}
	}

	if (distance3 != -1) 
	{
		if (distance3 < pTransform->GetWorldScale().y / 2 + 1) 
		{
			hasattackedintheair = false;
			return true;
		}
	}
	return false;
}

void MovementMachine::Accelerate(float acc)
{
	//hang the velocity by accelerating
	CurrentVelocity += acc * (float)TimeSys->GetFrameTime();
	//clamp the velocity with max and min velocity
	CurrentVelocity = glm::clamp(CurrentVelocity, -MaxXVelocity, MaxXVelocity);
}
void MovementMachine::UpdatePosition()
{

	//std::cout << std::boolalpha<<mIsInTheAir << std::endl;
	glm::vec3 position = pTransform->GetWorldPosition();

	
	glm::vec3 newPosition = glm::vec3(position.x + CurrentVelocity * TimeSys->GetFrameTime(), position.y + yVelocity * TimeSys->GetFrameTime(), position.z);
	pTransform->SetWorldPosition(newPosition);
	
	attackCd -= (float)TimeSys->GetFrameTime();
	if (mySkelRend)
		mySkelRend->SetFlipX(!FacingRight);

	mPersistentVelocity = CurrentVelocity;
}

void MovementMachine::BrakeX()
{
	CurrentVelocity = 0;
}

CombatInputs MovementMachine::GetCombatInput()
{
	//only if it can
	if (attackCd > 0) 
		return CombatInputs::Invalid;

	attackCd = 0;

	if (GameInput::LightKeyPressed()) {
		InputBuffer.push_back(CombatInputs::LightAttack);
		return CombatInputs::LightAttack;
	}

	if (GameInput::HeavyKeyPressed()) {
		InputBuffer.push_back(CombatInputs::HeavyAttack);
		return CombatInputs::HeavyAttack;
	}

	return CombatInputs::Invalid;
}

void MovementMachine::EndCombo()
{
	ChangeState("PlayerIdle");
	hasattackedintheair = false;
	InputBuffer.clear();
	attackCd = 0.25f;
}

void MovementMachine::DisplaceCombo(GameObject* hitbox, float velocity)
{
	if (hitbox)
		mHitboxTransform->SetWorldPosition(mHitboxTransform->GetWorldPosition() + glm::vec3((FacingRight ? 1 : -1) * velocity * (float)TimeSys->GetFrameTime(), 0.0f, 0));
}

void MovementMachine::UpdateHitbox(glm::vec3& _Position, glm::vec2& _Scale, float _Damage, glm::vec2& _Knocback)
{
	mHitbox->SetEnabled(true);
	mHitboxTransform->SetWorldScale(glm::vec3{ _Scale, 1.0f });
	mHitboxTransform->SetWorldPosition(_Position);
	mHitboxComp->mEnemy = false;
	mHitboxComp->mKnockback = true;
	mHitboxComp->mDamage = _Damage;
	mHitboxComp->mKSpeed = _Knocback.x;
	mHitboxComp->mKTime = _Knocback.y;
}

void MovementMachine::DashUpdate()
{
	//for (int i = 0; i < 3; i++) {
	//	if (mDashesAvailable >= i) {
	//		mDashbars[i]->GetComp<Renderable>()->SetVisible(true);
	//	}
	//	else {
	//		mDashbars[i]->GetComp<Renderable>()->SetVisible(false);
	//
	//	}
	//}
	//
	//glm::vec3 currentScale = DBTransforms[mDashesAvailable - 1]->GetScale();
	//currentScale.x 
	//DBTransforms[mDashesAvailable-1]->SetScale( )

}

#pragma endregion

