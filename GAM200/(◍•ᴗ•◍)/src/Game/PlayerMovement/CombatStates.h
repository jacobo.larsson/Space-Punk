#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"
#include "Combos.h"


class MovementMachine;


class PlayerAttack1 : public State
{
public:
	PlayerAttack1(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	TransformComponent* pTransform;
	MovementMachine* castedMachine;
	
	glm::vec2 Gravity;
	ComboList currentAttack;
	ComboAttack* currentComboAttack;
};

class PlayerAttack2 : public State
{
public:
	PlayerAttack2(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;


	TransformComponent* pTransform = nullptr;
	MovementMachine* castedMachine = nullptr;
	
	glm::vec2 Gravity;
	ComboList currentAttack;
	ComboAttack* currentComboAttack = nullptr;
};

class PlayerAttack3 : public State
{
public:
	PlayerAttack3(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	TransformComponent* pTransform = nullptr;
	MovementMachine* castedMachine = nullptr;
	
	glm::vec2 Gravity;
	ComboList currentAttack;
	ComboAttack* currentComboAttack = nullptr;

	//know if after this we are going to concatenate with another attack
	bool concat = false;
};