#include "../../Extern/ImGui/imgui.h" 
#include "../../Extern/ImGui/imgui_impl_sdl.h" 
#include "../../Extern/ImGui/imgui_impl_opengl3.h" 
#include "../../AssertSystem/AssertSystem.h" 

#include "../../Factory/Factory.h" 
#include "../../Scene/Scene.h" 
#include "../../Space/Space.h" 
#include "../../Collisions/Collisions.h" 
#include "../../PropertySystem/PropertyMap.h" 
#include "../../Changer/Changer.h" 
#include "../../Input/Input.h" 
#include "../../Time/Time.h" 
#include "../../Editor/Menus/Menus.h" 
#include "../../ThreadPool/ThreadPool.h"

#include "GoBackToMenu.h"

GoBackToMenu::GoBackToMenu()
{

	mName = "GoBackToMenu";


}
GoBackToMenu::GoBackToMenu(GoBackToMenu& rhs)
{




}
GoBackToMenu::~GoBackToMenu()
{




}
GoBackToMenu& GoBackToMenu::operator=(const GoBackToMenu& rhs)
{

	return *this;


}
GoBackToMenu* GoBackToMenu::Clone()
{


	GoBackToMenu* temp = FactorySys->Create<GoBackToMenu>();
	(*temp) = (*this);
	return temp;

}

void GoBackToMenu::OnCreate()
{

	


}
void GoBackToMenu::Initialize()
{

	GameObject* object_owner = GetOwner();
	LogicComponent::Initialize();
	GameObject* player = SceneSys->GetMainSpace()->FindObjectByName("Player");
	owner_transform = object_owner->GetComp<TransformComponent>();
	if (player)
		player_transform = player->GetComp<TransformComponent>();


}
void GoBackToMenu::Update()
{
	if (owner_transform && player_transform)
	{
		if (StaticRectToStaticRect(player_transform->GetWorldPosition(), player_transform->GetScale().x, player_transform->GetScale().y,
			owner_transform->GetWorldPosition(), owner_transform->GetScale().x, owner_transform->GetScale().y))
		{


			std::string test = "./data/Levels/MainMenu.lvl";
			SceneSys->ChangeLevelGameCompleteName(test);
			SceneSys->to_change = true;




		}
	}

}
void GoBackToMenu::Shutdown()
{




}



void GoBackToMenu::ToJson(nlohmann::json& j)
{

	IBase::ToJson(j);


}
void GoBackToMenu::FromJson(const nlohmann::json& j)
{


	IBase::FromJson(j);

}


bool GoBackToMenu::Edit()
{

	return false;


}