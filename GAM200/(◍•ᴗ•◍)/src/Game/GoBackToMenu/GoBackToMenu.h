#pragma once 
#include <string> 
#include <glm/glm.hpp> 
#include "../../RTTI/IBase.h" 
#include "../../RTTI/SM_RTTI.h" 
#include "../../RTTI/IComp.h" 
#include "../../EventSystem/EventHandler.h" 
#include "../../LogicSystem/LogicComponent.h" 
#include "../../Transform2D/TransformComponent.h"

class GoBackToMenu : public LogicComponent
{
	SM_RTTI_DECL(GoBackToMenu, IComp);
public:

	GoBackToMenu();
	GoBackToMenu(GoBackToMenu& rhs);
	~GoBackToMenu();
	GoBackToMenu& operator=(const GoBackToMenu& rhs);
	GoBackToMenu* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;



	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;

private:
	TransformComponent* owner_transform;
	TransformComponent* player_transform;
};