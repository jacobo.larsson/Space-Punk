#pragma once
#include <glm/glm.hpp>
#include "../../StateMachines/StateMachine/StateMachine.h"

class CameraMachine;

class CameraStaticState : public State
{
public:
	CameraStaticState(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	float _MovingCounter = 0.0f;
	float _SizeCounter = 0.0f;

	CameraMachine* mCastedMachine;

	glm::vec3 mOriginalPosition;
	glm::vec3 mFocusPosition;

	glm::vec2 mOriginalSize;
	glm::vec2 mFocusSize;
};

class CameraPurificationState : public State
{
public:
	friend class PlayerPurification;
	CameraPurificationState(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	CameraMachine* mCastedMachine;
	std::string mPreviousMode;
	float _Counter = 0;

};

class CameraHoverboardState : public State
{
public:
	CameraHoverboardState(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	CameraMachine* mCastedMachine;
};

class CameraArenaState: public State
{
public:
	CameraArenaState(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	CameraMachine* mCastedMachine;

	glm::vec2 mViewRectangle = glm::vec2{ 0, 0 };
	glm::vec2 mOriginalSize = glm::vec2{ 0, 0 };
	glm::vec2 mCenter = glm::vec2{ 0, 0 };
	glm::vec3 mFocusPosition = glm::vec3{ 0, 0, 0 };
	glm::vec3 mOriginalPosition = glm::vec3{ 0, 0, 0 };
	glm::vec3 mTransitionPosition = glm::vec3{ 0, 0, 0 };

	float mYOffset = 0.0f;
	float mTopMargin = 0.0f;
	float mBotMargin = 0.0f;
	float mRightMargin = 0.0f;
	float mLeftMargin = 0.0f;

	float mSizeCounter = 0.0f;
	float mPositionCounter = 0.0f;
};

class CameraFollowStateIdle :public State
{
public:
	CameraFollowStateIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	CameraMachine* mCastedMachine;

	float _MovingCounter = 0.0f;
	float _SizeCounter = 0.0f;

	glm::vec3 mOriginalPosition;
	glm::vec3 mFocusPosition;

	glm::vec2 mOriginalSize;
	glm::vec2 mFocusSize;

	float mMaxDistanceX = 125.0f;
	float mMaxDistanceY = 125.0f; 
	float mResetTime = 0.0f;
	float mYOffset = 0.0f;
};
