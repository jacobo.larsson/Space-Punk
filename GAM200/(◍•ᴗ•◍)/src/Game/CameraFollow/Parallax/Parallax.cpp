#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Factory/Factory.h"

#include "../../../Space/Space.h"
#include "../../../Scene/Scene.h"

#include "../../../GameObject/GameObject.h"
#include "../../../Transform2D/TransformComponent.h"

#include "Parallax.h"

ParallaxComponent::ParallaxComponent()
{
    SetName("ParallaxComponent");
}

void ParallaxComponent::Initialize()
{
    mOwnerTransform = mOwner->GetComp<TransformComponent>();
    mObjectivePosition = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<TransformComponent>();

    LogicComponent::Initialize();
}

void ParallaxComponent::Update()
{
    auto _FocusPosition = mObjectivePosition->GetWorldPosition();
    auto _CameraPosition = mOwnerTransform->GetWorldPosition();
    _FocusPosition.x *= mRelativePositionX;
    _FocusPosition.y *= mRelativePositionY;
    _FocusPosition.z = _CameraPosition.z;

    mOwnerTransform->SetWorldPosition(_FocusPosition);
}

bool ParallaxComponent::Edit()
{
    bool changed = false;
    ImGui::PushID(&mRelativePositionX);
    ImGui::DragFloat("My Relative position X", &mRelativePositionX, 0.05f, 0.00f, 1.0f);
    ImGui::PopID();
    ImGui::PushID(&mRelativePositionY);
    ImGui::DragFloat("My Relative position X", &mRelativePositionY, 0.05f, 0.00f, 1.0f);
    ImGui::PopID();
    return changed;
}

ParallaxComponent* ParallaxComponent::Clone()
{
    ParallaxComponent* temp = FactorySys->Create<ParallaxComponent>();
    (*temp) = (*this);
    return temp;
}

ParallaxComponent& ParallaxComponent::operator=(const ParallaxComponent& rhs)
{
    return (*this);
}

void ParallaxComponent::ToJson(nlohmann::json& j)
{
    j["mRelativePositionX"] = mRelativePositionX;
    j["mRelativePositionY"] = mRelativePositionY;
}

void ParallaxComponent::FromJson(const nlohmann::json& j)
{
    if(j.find("mRelativePositionX") != j.end())
        mRelativePositionX = j["mRelativePositionX"];
    if(j.find("mRelativePositionY") != j.end())
        mRelativePositionY = j["mRelativePositionY"];
}
