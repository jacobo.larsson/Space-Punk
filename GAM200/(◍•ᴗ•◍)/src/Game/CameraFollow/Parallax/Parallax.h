#pragma once
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"

class TransformComponent;

class ParallaxComponent : public LogicComponent
{
	SM_RTTI_DECL(ParallaxComponent, LogicComponent);
public:
	ParallaxComponent();

	void Initialize()override;
	void Update()override;

	bool Edit()override;
	ParallaxComponent* Clone() override;
	ParallaxComponent& operator=(const ParallaxComponent& rhs);

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;
private:
	TransformComponent* mOwnerTransform = nullptr;
	TransformComponent* mObjectivePosition = nullptr;
	float mRelativePositionX = 0.0f;
	float mRelativePositionY = 0.0f;
};