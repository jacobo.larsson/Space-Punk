#include <glm/glm.hpp>

#include "../../Time/Time.h"

#include "../../Debug/Console.h"
#include "../../Scene/Scene.h" // TODO: Remove this when debugging finished
#include "../../Space/Space.h" // TODO: Remove this when debugging finished
#include "../../GameObject/GameObject.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"

#include "CameraMachine.h"
#include "CameraStates.h"

CameraStaticState::CameraStaticState(const char* name):
State(name)
{
}

void CameraStaticState::Initialize()
{
	mCastedMachine = reinterpret_cast<CameraMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void CameraStaticState::LogicEnter()
{
	ConsoleSys->DebugPrint("CameraStaticState Entered");
	mOriginalPosition = mCastedMachine->mCameraTransform->GetWorldPosition();
	mFocusPosition.z = mOriginalPosition.z;
	mOriginalSize = mCastedMachine->mCameraComponent->mCamera.GetViewRectangle();
	_MovingCounter = 0.0f;
	_SizeCounter = 0.0f;
}

void CameraStaticState::LogicUpdate()
{
	_MovingCounter += TimeSys->GetFrameTime()/mCastedMachine->mTransitionSpeed;
	_SizeCounter += TimeSys->GetFrameTime()/mCastedMachine->mTransitionSpeed;

	if(_MovingCounter < 1)
		mCastedMachine->mCameraTransform->SetWorldPosition(glm::mix(mOriginalPosition, mFocusPosition, _MovingCounter));

	if(_SizeCounter < 1)
		mCastedMachine->mCameraComponent->mCamera.mViewRectangle = (glm::mix(mOriginalSize, mFocusSize, _SizeCounter));
}

void CameraStaticState::LogicExit()
{
}

CameraPurificationState::CameraPurificationState(const char* name) :
State(name)
{
}

void CameraPurificationState::Initialize()
{
	mCastedMachine = reinterpret_cast<CameraMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void CameraPurificationState::LogicEnter()
{
	ConsoleSys->DebugPrint("CameraPurificationState Entered");
	_Counter = 0.0f;

	CameraEffect _Effect;
	_Effect.mEffectType = CameraEffectLists::CameraPulse;
	_Effect.mDuration = 1.f;
	_Effect.originalposy = mCastedMachine->mCameraTransform->GetWorldPosition().y;
	_Effect.each_add =1.f;
	_Effect.first_enter = false;
	_Effect.pulse_counter = 0.f;
	_Effect.pulse_limit = 1.f;
	_Effect.pulse_intermediate = 0.5f;

	_Effect.mCameraTransform = mCastedMachine->mCameraTransform;
	_Effect.mCamera = &mCastedMachine->mCameraComponent->mCamera;
	mCastedMachine->AddEffect(_Effect);
}

void CameraPurificationState::LogicUpdate()
{
	glm::vec2 sc = mCastedMachine->mCameraComponent->GetCamera()->GetViewRectangle();

	_Counter += TimeSys->GetFrameTime()/4;

	if (_Counter < 1)
	{
		
		sc.x = glm::mix(1280.f, 640.f, _Counter);
		sc.y = glm::mix(720.f, 360.f, _Counter);
		mCastedMachine->mCameraComponent->GetCamera()->SetViewRectangle(sc);
	}
	else
	{
		if(mPreviousMode == "CameraFollowStateFollow")
			mCastedMachine->ChangeState("CameraFollowStateIdle");
		else
			mCastedMachine->ChangeState(mPreviousMode.c_str());
	}
}

void CameraPurificationState::LogicExit()
{
	mCastedMachine->mEffectsBuffer.clear();
}

CameraHoverboardState::CameraHoverboardState(const char* name) :
State(name)
{
}

void CameraHoverboardState::Initialize()
{
	mCastedMachine = reinterpret_cast<CameraMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void CameraHoverboardState::LogicEnter()
{
	ConsoleSys->DebugPrint("CameraHoverboardState Entered");
}

void CameraHoverboardState::LogicUpdate()
{
	auto _PlayerPosition = mCastedMachine->mPlayerTransform->GetWorldPosition();
	auto _CameraPosition = mCastedMachine->mCameraTransform->GetWorldPosition();
	glm::vec3 _FocusPosition;
	_FocusPosition.x = glm::mix(_CameraPosition.x, _PlayerPosition.x, 0.1f);
	_FocusPosition.y = glm::mix(_CameraPosition.y, _PlayerPosition.y, 0.1f);
	_FocusPosition.z = 20;
	mCastedMachine->mCameraTransform->SetWorldPosition(_FocusPosition);
}

void CameraHoverboardState::LogicExit()
{
}

CameraArenaState::CameraArenaState(const char* name) :
State(name)
{
}

void CameraArenaState::Initialize()
{
	mCastedMachine = reinterpret_cast<CameraMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void CameraArenaState::LogicEnter()
{
	ConsoleSys->DebugPrint("CameraArenaState Entered");
	mOriginalSize = mCastedMachine->mCameraComponent->mCamera.GetViewRectangle();
	mOriginalPosition = mCastedMachine->mCameraTransform->GetWorldPosition();

	auto _CameraPosition = mCastedMachine->mCameraTransform->GetWorldPosition();

	mSizeCounter = 0.0f; 
	mPositionCounter = 0.0f;
	mTransitionPosition.x = mCenter.x;
	mTransitionPosition.y = mCenter.y;
	mTransitionPosition.z = _CameraPosition.z;
	if ((_CameraPosition.x < mLeftMargin) || (_CameraPosition.x > mRightMargin) ||
		((_CameraPosition.y) < mBotMargin) || ((_CameraPosition.y) > mTopMargin))
	{
		if (_CameraPosition.x < mCenter.x)
			mTransitionPosition.x = mLeftMargin;
		else if (_CameraPosition.x > mCenter.x)
			mTransitionPosition.x = mRightMargin;

		if (_CameraPosition.y > mCenter.y)
			mTransitionPosition.y = mTopMargin;
		else if (_CameraPosition.y < mCenter.y)
			mTransitionPosition.y = mBotMargin;
	}

}

void CameraArenaState::LogicUpdate()
{
	auto _CameraPosition = mCastedMachine->mCameraTransform->GetWorldPosition();
	auto _PlayerPosition = mCastedMachine->mPlayerTransform->GetWorldPosition();

	mSizeCounter += TimeSys->GetFrameTime() / mCastedMachine->mTransitionSpeed;
	mPositionCounter += TimeSys->GetFrameTime() / mCastedMachine->mTransitionSpeed;
	if(mSizeCounter < 1.0f)
		mCastedMachine->mCameraComponent->mCamera.mViewRectangle = (glm::mix(mOriginalSize, mViewRectangle, mSizeCounter));

	if (mPositionCounter < 1.0f)
	{
		//if(((_PlayerPosition.x > mLeftMargin) && (_PlayerPosition.x < mRightMargin)) ||
		//	(((_PlayerPosition.y) > mBotMargin) && ((_PlayerPosition.y) < mTopMargin)))
		//{
		//	mFocusPosition.x = glm::mix(_CameraPosition.x, _PlayerPosition.x, 1.0f);
		//	mFocusPosition.y = glm::mix(_CameraPosition.y, _PlayerPosition.y + mYOffset, 1.0f);
		//	mFocusPosition.z = mOriginalPosition.z;
		//	mCastedMachine->mCameraTransform->SetWorldPosition(mFocusPosition);
		//	return;
		//}

		// If the camera is outside
		if ((_CameraPosition.x < mLeftMargin) || (_CameraPosition.x > mRightMargin) ||
			((_CameraPosition.y ) < mBotMargin) || ((_CameraPosition.y) > mTopMargin))
		{
			mFocusPosition.x = glm::mix( mOriginalPosition.x, mTransitionPosition.x, mPositionCounter);
			mFocusPosition.y = glm::mix( mOriginalPosition.y, mTransitionPosition.y, mPositionCounter);
			mFocusPosition.z = mOriginalPosition.z;
			mCastedMachine->mCameraTransform->SetWorldPosition(mFocusPosition);
			return;
		}
	}
	
	mFocusPosition = _CameraPosition;

	// Actual arena behavior
	if ((_PlayerPosition.x > mLeftMargin) && (_PlayerPosition.x < mRightMargin))
	{
		mFocusPosition.x = glm::mix(_CameraPosition.x, _PlayerPosition.x, 1.0f);
	}
	if ( ( (_PlayerPosition.y + mYOffset) > mBotMargin ) && ( (_PlayerPosition.y + mYOffset) < mTopMargin) )
	{
		mFocusPosition.y = glm::mix(_CameraPosition.y, _PlayerPosition.y + mYOffset, 1.0f);
	}
	mFocusPosition.z = 20.0f;
	mCastedMachine->mCameraTransform->SetWorldPosition(mFocusPosition);
}

void CameraArenaState::LogicExit()
{
}

CameraFollowStateIdle::CameraFollowStateIdle(const char* name):
State(name)
{
}

void CameraFollowStateIdle::Initialize()
{
	mCastedMachine = reinterpret_cast<CameraMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void CameraFollowStateIdle::LogicEnter()
{
	mOriginalPosition = mCastedMachine->mCameraTransform->GetWorldPosition();
	mFocusPosition = mCastedMachine->mPlayerTransform->GetWorldPosition();
	mFocusPosition.z = mOriginalPosition.z;
	mOriginalSize = mCastedMachine->mCameraComponent->mCamera.GetViewRectangle();
	_MovingCounter = 0.0f;
	_SizeCounter = 0.0f;
}

void CameraFollowStateIdle::LogicUpdate()
{
	auto _Space = SceneSys->GetMainSpace();
	auto _PlayerPosition = mCastedMachine->mPlayerTransform->GetWorldPosition();
	_PlayerPosition.y += mYOffset;
	auto _CameraPosition = mCastedMachine->mCameraTransform->GetWorldPosition();

	bool _UpdateY = true;
	
	//_Space->Drawrectangle(glm::vec2(_CameraPosition.x, _CameraPosition.y - mYOffset), 2 * mMaxDistanceX, 2 * mMaxDistanceY, Colors::red);
	mFocusPosition = _CameraPosition;

	// If the player is ouside of the idling box, start moving the camera
	if(glm::abs(_PlayerPosition.y - _CameraPosition.y) > mMaxDistanceY ||
	   glm::abs(_PlayerPosition.x - _CameraPosition.x) > mMaxDistanceX)
	{
		if (glm::abs(_PlayerPosition.x - _CameraPosition.x) > mMaxDistanceX)
		{
			mFocusPosition.x = glm::mix(_CameraPosition.x, _PlayerPosition.x, 0.05f);
		}
		if (glm::abs(_PlayerPosition.y - _CameraPosition.y) > mMaxDistanceY)
		{
			mFocusPosition.y = glm::mix(_CameraPosition.y, _PlayerPosition.y, 0.05f);
			_UpdateY = false;
		}
		mFocusPosition.z = 20.0f;
		if (_UpdateY)
		{
			mFocusPosition.y = glm::mix(_CameraPosition.y, _PlayerPosition.y, 0.05f);
		}
		mCastedMachine->mCameraTransform->SetWorldPosition(mFocusPosition);

		_MovingCounter = 0.0f;
		mResetTime = 0.0f;
	}
	// Although the player is inside the box, the camera needs to stil move on the y-axis
	else 
	{
		mResetTime += TimeSys->GetFrameTime();
		mFocusPosition = _CameraPosition;

		if (glm::abs(_CameraPosition.x - _PlayerPosition.x) < 10.0f)
			mResetTime = 0.0f;
		if (mResetTime > 1.0f)
			mFocusPosition.x = _PlayerPosition.x;
		mFocusPosition.y = _PlayerPosition.y;
		mCastedMachine->mCameraTransform->SetWorldPosition(glm::mix(_CameraPosition, mFocusPosition, 0.05f));
	}

	// Transition of size when changing modes
	_SizeCounter += TimeSys->GetFrameTime() / mCastedMachine->mTransitionSpeed;
	if (_SizeCounter < 1)
		mCastedMachine->mCameraComponent->mCamera.mViewRectangle = (glm::mix(mOriginalSize, mFocusSize, _SizeCounter));
}

void CameraFollowStateIdle::LogicExit()
{
}

