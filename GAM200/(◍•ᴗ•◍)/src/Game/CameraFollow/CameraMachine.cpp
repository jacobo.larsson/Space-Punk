#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Input/Input.h" // TODO: Remove this header

#include "../../Transform2D/TransformComponent.h"

#include "CameraEffect/CameraEffect.h"

#include "CameraStates.h"
#include "CameraMachine.h"

CameraMachine::CameraMachine()
{
	SetName("CameraMachine");
	
	mInitialState = new CameraFollowStateIdle("CameraFollowStateIdle");
	mCurrentState = mInitialState; 
	AddState(mInitialState);
	AddState(new CameraStaticState("CameraStaticState"));
	AddState(new CameraHoverboardState("CameraHoverboardState"));
	AddState(new CameraArenaState("CameraArenaState"));
	AddState(new CameraPurificationState("CameraPurificationState"));
}

CameraMachine::~CameraMachine()
{
}

CameraMachine& CameraMachine::operator=(const CameraMachine& _Rhs)
{
	return *this;
}

CameraMachine* CameraMachine::Clone()
{
	return nullptr;
}

void CameraMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
}

void CameraMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
}

void CameraMachine::Initialize()
{
	mPlayerTransform = reinterpret_cast<TransformComponent*>(SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp("TransformComponent"));
	mCameraTransform = reinterpret_cast<TransformComponent*>( SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp("TransformComponent"));
	mCameraComponent = reinterpret_cast<CameraComponent*>( SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp("CameraComponent"));
	StateMachine::Initialize();
}

void CameraMachine::Update()
{
	StateMachine::Update();
	if (InputSys->key_is_triggered(keys::G))
	{
		CameraEffect _Effect;
		_Effect.mEffectType = CameraEffectLists::CameraShake;
		_Effect.mDuration = 0.3f;
		_Effect.mMagnitude = 3.0f;
		AddEffect(_Effect);
	}
	PerformEffects();
}

void CameraMachine::Shutdown()
{
	StateMachine::Shutdown();
}

bool CameraMachine::Edit()
{
	return false;
}

void CameraMachine::AddEffect(CameraEffectLists _EffectType, float _Duration)
{
	CameraEffect _Effect;
	_Effect.mDuration = _Duration;
	_Effect.mEffectType = _EffectType;
	_Effect.mCamera = &mCameraComponent->mCamera;
	_Effect.mCameraTransform = mCameraTransform;
}

void CameraMachine::AddEffect(CameraEffect& _Effect)
{
	_Effect.mCamera = &mCameraComponent->mCamera;
	_Effect.mCameraTransform = mCameraTransform;
	_Effect.mOriginalPosition = mCameraTransform->GetWorldPosition();
	mEffectsBuffer.push_back(_Effect);
}

void CameraMachine::PerformEffects()
{
	std::vector<CameraEffect> _ToRemove;
	std::sort(mEffectsBuffer.begin(), mEffectsBuffer.end());
	for (auto& _Effect : mEffectsBuffer)
	{
		(*(_Effect.mActions[_Effect.mEffectType]))(_Effect);
		if (_Effect.mActiveTime > _Effect.mDuration)
			_ToRemove.push_back(_Effect);
	}
	for (auto& _Removing : _ToRemove)
	{
		auto _Found = std::find(mEffectsBuffer.begin(), mEffectsBuffer.end(), _Removing);
		if(_Found != mEffectsBuffer.end())
			mEffectsBuffer.erase(_Found);
	}
}
