#pragma once
#include <map>
#include <glm/glm.hpp>

enum CameraEffectLists
{
	CameraNone,
	CameraShake,
	CameraPulse,
	CameraCount
};

class Camera;
class TransformComponent;
struct CameraEffect
{
	CameraEffect();
	bool operator==(const CameraEffect& _Effect);
	bool operator<(const CameraEffect& _Effect);

	static std::map < CameraEffectLists, void(*)(CameraEffect&)> mActions;
	CameraEffectLists mEffectType = CameraNone;

	float mDuration = 0.0f;
	float mActiveTime = 0.0f;

	Camera* mCamera = nullptr;
	TransformComponent* mCameraTransform = nullptr;


	glm::vec3 mOriginalPosition;
	float mMagnitude = 1.0f;


	float pulse_counter = 0.f;
	float pulse_limit =1.f;
	bool first_enter = false;
	float pulse_intermediate = 0.5f;
	float originalposy=0.f;
	float each_add = 1.f;

};

void VoidEffect(CameraEffect& _Effect);
void ShakeEffect(CameraEffect& _Effect);
void PulseEffect(CameraEffect& _Effect);