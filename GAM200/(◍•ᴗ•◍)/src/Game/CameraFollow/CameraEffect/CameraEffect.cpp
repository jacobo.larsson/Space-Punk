#include "../../../Time/Time.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Graphics/Camera/Camera.h"
#include "CameraEffect.h"


std::map < CameraEffectLists, void(*)(CameraEffect&)> CameraEffect::mActions;
CameraEffect::CameraEffect()
{
	mActions[CameraEffectLists::CameraNone] = &ShakeEffect;
	mActions[CameraEffectLists::CameraShake] = &ShakeEffect;
	mActions[CameraEffectLists::CameraPulse] = &PulseEffect;
}

bool CameraEffect::operator==(const CameraEffect& _Effect)
{
	bool _Result = false;

	_Result |= (mEffectType == _Effect.mEffectType);
	_Result |= (mDuration == _Effect.mDuration);
	_Result |= (mActiveTime == _Effect.mActiveTime);

	return _Result;
}

bool CameraEffect::operator<(const CameraEffect& _Effect)
{
	return (mDuration - mActiveTime) < (_Effect.mDuration - _Effect.mActiveTime);
}

void VoidEffect(CameraEffect& _Effect)
{
}

void ShakeEffect(CameraEffect& _Effect)
{
	_Effect.mActiveTime += TimeSys->GetFrameTime();
	if (_Effect.mDuration > _Effect.mActiveTime)
	{
		glm::vec3 _Displa{ (float)(rand() % 21 - 10) / 10, (float)(rand() % 21 - 10) / 10, 0 };
		//glm::vec3 mFocusPosition = _Effect.mOriginalPosition + (((int)TimeSys->GetFrameCounter() % 2 == 1)? glm::vec3(-1.0f, 0.0f, 0.0f): glm::vec3(1.0f, 0.0f, 0.0f));
		glm::vec3 mFocusPosition = _Effect.mCameraTransform->GetWorldPosition() + _Displa * _Effect.mMagnitude;
		_Effect.mCameraTransform->SetWorldPosition(mFocusPosition);
	}
}

void PulseEffect(CameraEffect& _Effect)
{
	
	if (_Effect.mDuration > _Effect.mActiveTime)
	{
		//Do logic
		

		if (_Effect.pulse_counter > _Effect.pulse_limit)
		{
			_Effect.first_enter = false;
			_Effect.pulse_counter = 0.f;




			if (_Effect.pulse_intermediate > 0.01)
			{
				_Effect.pulse_limit /= 2.f;
				_Effect.pulse_intermediate /= 2.f;
			}
		}

		if (_Effect.pulse_counter < _Effect.pulse_intermediate)
		{
			
			glm::vec3 mFocusPosition = _Effect.mCameraTransform->GetWorldPosition();
			_Effect.mCameraTransform->SetWorldPosition(glm::vec3(mFocusPosition.x, mFocusPosition.y + 1, mFocusPosition.z));
			
		}
		else
		{
			if (_Effect.first_enter == false)
			{
				float difference = _Effect.mCameraTransform->GetWorldPosition().y - _Effect.originalposy;
				
				_Effect.each_add = ((difference * 2) /(_Effect.pulse_limit- _Effect.pulse_intermediate))/60.f;
				_Effect.first_enter = true;
			}
			glm::vec3 mFocusPosition = _Effect.mCameraTransform->GetWorldPosition();
			_Effect.mCameraTransform->SetWorldPosition(glm::vec3(mFocusPosition.x, mFocusPosition.y - 1, mFocusPosition.z));
			

		}


		_Effect.pulse_counter += TimeSys->GetFrameTime();

	}



	_Effect.mActiveTime += TimeSys->GetFrameTime()/4;
}
