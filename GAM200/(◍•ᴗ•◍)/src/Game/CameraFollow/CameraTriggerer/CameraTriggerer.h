#pragma once
#include <glm/glm.hpp>
#include <map>
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"

class GameObject;
class CameraMachine;

class CameraTriggerer :public LogicComponent
{
	SM_RTTI_DECL(CameraTriggerer, LogicComponent);
public:
	CameraTriggerer();
	~CameraTriggerer();

	CameraTriggerer& operator=(const CameraTriggerer& rhs);
	CameraTriggerer* Clone() override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void OnCollisionStarted(GameObject* _Other)override;
	void OnCollisionEnded(GameObject* _Other)override;
private:

	bool EditStaticState();
	void TriggerStaticState();
	void ToJsonStaticState(nlohmann::json& j);
	void FromJsonStaticState(const nlohmann::json& j);

	bool EditPurificationState();
	void TriggerPurificationState();
	void ToJsonPurificationState(nlohmann::json& j);
	void FromJsonPurificationState(const nlohmann::json& j);

	bool EditHoverboardState();
	void TriggerHoverboardState();
	void ToJsonHoverboardState(nlohmann::json& j);
	void FromJsonHoverboardState(const nlohmann::json& j);

	bool EditFollowState();
	void TriggerFollowState();
	void ToJsonFollowState(nlohmann::json& j);
	void FromJsonFollowState(const nlohmann::json& j);

	bool EditArenaState();
	void TriggerArenaState();
	void ToJsonArenaState(nlohmann::json& j);
	void FromJsonArenaState(const nlohmann::json& j);

	bool mTriggered = false;
	bool mTriggerAlwaysTheSame = false;

	std::string mCameraState = "CameraStaticState";
	std::string mPreviousState = "CameraStaticState";
	std::map<std::string, std::pair<bool((CameraTriggerer::*)()), void((CameraTriggerer::*)())>> mEdits;
	GameObject* mCamera;
	CameraMachine* mCameraMachine;

	// Shared variables
	float mTransitionSpeed = 0.0f;
	bool mTriggerOnce = false;
	glm::vec2 mPlayerFirstPosition = glm::vec2{ 0.0f, 0.0f };

	// Static mode related
	glm::vec3 mFocusPosition;
	glm::vec2 mFocusSize;

	// Follow mode related
	float mMaxDistanceX = 0.0f;
	float mMaxDistanceY = 0.0f;
	float mYOffSet = 0.0f;

	// Arena mode related
	glm::vec2 mCenter = glm::vec2{ 0, 0 };
	glm::vec2 mViewRectangle = glm::vec2{ 0, 0 };
	glm::vec2 mHorizontalMargins = glm::vec2{ 0, 0 };
	glm::vec2 mVerticalMargins = glm::vec2{ 0, 0 };

};