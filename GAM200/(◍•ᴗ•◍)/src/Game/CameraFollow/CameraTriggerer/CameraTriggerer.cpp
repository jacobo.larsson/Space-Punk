#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Factory/Factory.h"
#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Editor/Menus/Menus.h"

#include "../../../GameObject/GameObject.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../CameraStates.h"
#include "../CameraMachine.h"

#include "CameraTriggerer.h"

CameraTriggerer::CameraTriggerer()
{
    SetName("CameraTriggerer");
    mEdits["CameraStaticState"].first = &CameraTriggerer::EditStaticState;
    mEdits["CameraStaticState"].second = &CameraTriggerer::TriggerStaticState;

    mEdits["CameraPurificationState"].first = &CameraTriggerer::EditPurificationState;
    mEdits["CameraPurificationState"].second = &CameraTriggerer::TriggerPurificationState;

    mEdits["CameraHoverboardState"].first = &CameraTriggerer::EditHoverboardState;
    mEdits["CameraHoverboardState"].second = &CameraTriggerer::TriggerHoverboardState;

    mEdits["CameraFollowStateIdle"].first = &CameraTriggerer::EditFollowState;
    mEdits["CameraFollowStateIdle"].second = &CameraTriggerer::TriggerFollowState;

    mEdits["CameraArenaState"].first = &CameraTriggerer::EditArenaState;
    mEdits["CameraArenaState"].second = &CameraTriggerer::TriggerArenaState;
}

CameraTriggerer::~CameraTriggerer()
{
}

CameraTriggerer& CameraTriggerer::operator=(const CameraTriggerer& rhs)
{
	return *this;
}

CameraTriggerer* CameraTriggerer::Clone()
{
    CameraTriggerer* temp = FactorySys->Create<CameraTriggerer>();
    (*temp) = (*this);
    return temp;
}

void CameraTriggerer::ToJson(nlohmann::json& j)
{
    IBase::ToJson(j);
    j["mCameraState"] = mCameraState;
    if (mCamera)
    {
        j["Camera"]["UID"] = mCamera->GetUID();
        j["Camera"]["Name"] = mCamera->GetName();
    }
    // Shared
    j["mTransitionSpeed"] = mTransitionSpeed;
    j["mTriggerOnce"] = mTriggerOnce;
    j["mTriggerAlwaysTheSame"] = mTriggerAlwaysTheSame;

    // Static
    ToJsonStaticState(j["CameraStaticState"]);
    // Follow mode
    ToJsonFollowState(j["CameraFollowState"]);
    // Arena Mode
    ToJsonArenaState(j["CameraArenaState"]);
}

void CameraTriggerer::FromJson(const nlohmann::json& j)
{
    IBase::FromJson(j);

    if (j.find("mCameraState") != j.end())
        mCameraState = j["mCameraState"].get<std::string>();
    if (j.find("Camera") != j.end())
    {
        Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
        if (_space)
            _space->AddPointerToLink(&mCamera, j["Camera"]["UID"], j["Camera"]["Name"].get<std::string>());
    }
    // Shared
    if (j.find("mTransitionSpeed") != j.end())
        mTransitionSpeed = j["mTransitionSpeed"];
    if (j.find("mTriggerOnce") != j.end())
        mTriggerOnce = j["mTriggerOnce"];
    if (j.find("mTriggerAlwaysTheSame") != j.end())
        mTriggerAlwaysTheSame = j["mTriggerAlwaysTheSame"];

    // Static Mode
    if (j.find("CameraStaticState") != j.end())
        FromJsonStaticState(j["CameraStaticState"]);
    // Follow Mode
    if (j.find("CameraFollowState") != j.end())
        FromJsonFollowState(j["CameraFollowState"]);
    // Arena Mode
    if (j.find("CameraArenaState") != j.end())
        FromJsonArenaState(j["CameraArenaState"]);
}

void CameraTriggerer::Initialize()
{
    mCamera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
    mCameraMachine = reinterpret_cast<CameraMachine*>(mCamera->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));
}

void CameraTriggerer::Update()
{
}

void CameraTriggerer::Shutdown()
{
}

bool CameraTriggerer::Edit()
{
    bool changed = false;

    std::string _CameraName;
    _CameraName += "This trigger will activate: ";
    
    if (mCamera)
        _CameraName += mCamera->GetName();

    ImGui::Text(_CameraName.c_str());

    if (ImGui::BeginDragDropTarget())
    {
        if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
        {
            IM_ASSERT(payload->DataSize == sizeof(GameObject*));
            GameObject* _Object = *((GameObject**)payload->Data);
            if (_Object->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"))
            {
                mCamera = _Object;
            }
        }
        ImGui::EndDragDropTarget();
    }

    static int _CurrentIndex = 0;
    const char* _Items[] = { "CameraStaticState", "CameraPurificationState", "CameraHoverboardState", "CameraArenaState", "CameraFollowStateIdle" };
    if (ImGui::BeginCombo("Camera States", _Items[_CurrentIndex]))
    {
        for (unsigned i = 0; i < IM_ARRAYSIZE(_Items); i++)
        {
            if (ImGui::Selectable(_Items[i]))
                mCameraState = _Items[i];
        }
        ImGui::EndCombo();
    }

    ImGui::Text("My State: ");
    ImGui::SameLine();
    ImGui::Text(mCameraState.c_str());

    if(ImGui::CollapsingHeader("Shared variables"))
    { 
        ImGui::DragFloat("TransitionSpeed", &mTransitionSpeed, 0.1f, 0.0f, 15.0f);
        ImGui::Checkbox("Trigger just once", &mTriggerOnce);
        ImGui::Checkbox("Trigger alwayse the same state", &mTriggerAlwaysTheSame);
    }

    changed = (this->*mEdits[mCameraState].first)();

	return changed;
}

void CameraTriggerer::OnCollisionStarted(GameObject* _Other)
{
    //if (mTriggerOnce && mTriggered)
    //    return;
    //if (_Other->CompareTag("Player"))
    //{
    //    if (mCameraMachine != nullptr && (!mTriggered || mTriggerAlwaysTheSame))
    //    {
    //        mPreviousState = mCameraMachine->GetCurrentStateName();
    //        mCameraMachine->ChangeState(mCameraState.c_str());
    //        mCameraMachine->mTransitionSpeed = mTransitionSpeed;
    //        (this->*mEdits[mCameraState].second)();
    //        mTriggered = true;
    //    }
    //    else if (mTriggered)
    //    {
    //        mCameraMachine->ChangeState(mPreviousState.c_str());
    //        // mCameraMachine->mTransitionSpeed = mTransitionSpeed;
    //        // (this->*mEdits[mPreviousState].second)();
    //        mTriggered = false;
    //    }
    //}
    if (mTriggerOnce && mTriggered)
        return;
    if (_Other->CompareTag("Player"))
    {
        TransformComponent* _PlayerTransform = _Other->GetComp<TransformComponent>();
        mPlayerFirstPosition = glm::vec2{_PlayerTransform->GetWorldPosition()};
    }

}

void CameraTriggerer::OnCollisionEnded(GameObject* _Other)
{
    if (mTriggerOnce && mTriggered)
        return;
    if (_Other->CompareTag("Player"))
    {

        TransformComponent* _PlayerTransform = _Other->GetComp<TransformComponent>();
        glm::vec2 _PlayerPosition { _PlayerTransform->GetWorldPosition() };

        if (mCameraMachine != nullptr && (!mTriggered || mTriggerAlwaysTheSame) && mPlayerFirstPosition.x < _PlayerPosition.x)
        {
            mPreviousState = mCameraMachine->GetCurrentStateName();
            mCameraMachine->ChangeState(mCameraState.c_str());
            mCameraMachine->mTransitionSpeed = mTransitionSpeed;
            (this->*mEdits[mCameraState].second)();
            mTriggered = true;
        }
        else if (mTriggered && mPlayerFirstPosition.x > _PlayerPosition.x)
        {
            mCameraMachine->ChangeState(mPreviousState.c_str());
            // mCameraMachine->mTransitionSpeed = mTransitionSpeed;
            // (this->*mEdits[mPreviousState].second)();
            mTriggered = false;
        }
    }
}

bool CameraTriggerer::EditStaticState()
{
    bool changed = false;
    if (ImGui::CollapsingHeader("CameraStaticState"))
    {
        // Focus position
        {
            ImGui::Text("Focus Position");
            ImGui::PushID("Focus Position");
            ImGui::Columns(2, "Position", true);

            ImGui::DragFloat("X", &mFocusPosition.x);
            if (ImGui::IsItemDeactivatedAfterEdit())
                changed = true;

            ImGui::NextColumn();
            ImGui::DragFloat("Y", &mFocusPosition.y);
            if (ImGui::IsItemDeactivatedAfterEdit())
                changed = true;
            ImGui::Columns(1);
            ImGui::PopID();

            if (auto _Space = mOwner->GetParentSpace())
            {
                _Space->DrawLine(glm::vec2{ mFocusPosition.x - 5, mFocusPosition.y - 5 }, glm::vec2{ mFocusPosition.x + 5, mFocusPosition.y + 5 }, Colors::red);
                _Space->DrawLine(glm::vec2{ mFocusPosition.x - 5, mFocusPosition.y + 5 }, glm::vec2{ mFocusPosition.x + 5, mFocusPosition.y - 5 }, Colors::red);
            }
        }
        // View rectangle
        {
            ImGui::Text("View Rectangle");
            ImGui::PushID("View Rectangle");
            ImGui::Columns(2, "View Rectangle", true);

            ImGui::DragFloat("X", &mFocusSize.x, 1.0f, 1.0f);
            if (ImGui::IsItemDeactivatedAfterEdit())
            {
                mFocusSize.y = mFocusSize.x * 9 / 16;
                changed = true;
            }
            ImGui::NextColumn();
            ImGui::Columns(1);

            ImGui::PopID();

            if (auto _Space = mOwner->GetParentSpace())
            {
                _Space->Drawrectangle(mFocusPosition, (int)mFocusSize.x, (int)mFocusSize.y, Colors::orange);
            }
        }
    }
    return changed;
}

void CameraTriggerer::TriggerStaticState()
{
    auto _State = reinterpret_cast<CameraStaticState*>(mCameraMachine->GetState("CameraStaticState"));
    _State->mFocusPosition = mFocusPosition;
    _State->mFocusSize = mFocusSize;
}

void CameraTriggerer::ToJsonStaticState(nlohmann::json& j)
{
    j["mFocusPosition"].push_back(mFocusPosition.x);
    j["mFocusPosition"].push_back(mFocusPosition.y);

    j["mFocusSize"].push_back(mFocusSize.x);
    j["mFocusSize"].push_back(mFocusSize.y);
}

void CameraTriggerer::FromJsonStaticState(const nlohmann::json& j)
{
    if (j.find("mFocusPosition") != j.end())
    {
        mFocusPosition.x = j["mFocusPosition"][0];
        mFocusPosition.y = j["mFocusPosition"][1];
    }
    if (j.find("mFocusSize") != j.end())
    {
        mFocusSize.x = j["mFocusSize"][0];
        mFocusSize.y = j["mFocusSize"][1];
    }
}

bool CameraTriggerer::EditPurificationState()
{
    return false;
}

void CameraTriggerer::TriggerPurificationState()
{
    auto _State = reinterpret_cast<CameraPurificationState*>(mCameraMachine->GetState("CameraPurificationState"));
    _State->mPreviousMode = mCameraMachine->GetCurrentStateName();
}

void CameraTriggerer::ToJsonPurificationState(nlohmann::json& j)
{
}

void CameraTriggerer::FromJsonPurificationState(const nlohmann::json& j)
{
}

bool CameraTriggerer::EditHoverboardState()
{
    return false;
}

void CameraTriggerer::TriggerHoverboardState()
{
}

void CameraTriggerer::ToJsonHoverboardState(nlohmann::json& j)
{
}

void CameraTriggerer::FromJsonHoverboardState(const nlohmann::json& j)
{
}

bool CameraTriggerer::EditFollowState()
{
    bool changed = false;
    if (ImGui::CollapsingHeader("CameraFollowState"))
    {
        // View rectangle
        {
            ImGui::Text("View Rectangle");
            ImGui::PushID("View Rectangle");
            ImGui::Columns(2, "View Rectangle", true);

            ImGui::DragFloat("X", &mFocusSize.x, 1.0f, 1.0f);
            if (ImGui::IsItemDeactivatedAfterEdit())
            {
                mFocusSize.y = mFocusSize.x * 9 / 16;
                changed = true;
            }
            ImGui::NextColumn();
            ImGui::Columns(1);

            ImGui::PopID();

            if (auto _Space = mOwner->GetParentSpace())
            {
                _Space->Drawrectangle(mOwner->GetComp<TransformComponent>()->GetWorldPosition(), (int)mFocusSize.x, (int)mFocusSize.y, Colors::orange);
                _Space->Drawrectangle(mOwner->GetComp<TransformComponent>()->GetWorldPosition(), (int)2*mMaxDistanceX, (int)2*mMaxDistanceY, Colors::red);
            }
        }
        ImGui::DragFloat("Max Distance X", &mMaxDistanceX, 5.0f, 0.0f);
        ImGui::DragFloat("Max Distance Y", &mMaxDistanceY, 5.0f, 0.0f);
        ImGui::DragFloat("Y OffSet", &mYOffSet, 5.0f, 0.0f);
    }
    return changed;
}

void CameraTriggerer::TriggerFollowState()
{
    auto _StateIdle = reinterpret_cast<CameraFollowStateIdle*>(mCameraMachine->GetState("CameraFollowStateIdle"));
    _StateIdle->mFocusSize = mFocusSize;
    _StateIdle->mMaxDistanceX = mMaxDistanceX;
    _StateIdle->mMaxDistanceY = mMaxDistanceY;

    _StateIdle->mYOffset = mYOffSet;
}

void CameraTriggerer::ToJsonFollowState(nlohmann::json& j)
{
    j["mFocusSize"].push_back(mFocusSize.x);
    j["mFocusSize"].push_back(mFocusSize.y);
    j["mMaxDistanceX"] = mMaxDistanceX;
    j["mMaxDistanceY"] = mMaxDistanceY;
    j["mYOffSet"] = mYOffSet;
}

void CameraTriggerer::FromJsonFollowState(const nlohmann::json& j)
{
    if (j.find("mMaxDistanceX") != j.end())
        mMaxDistanceX = j["mMaxDistanceX"];
    if (j.find("mMaxDistanceY") != j.end())
        mMaxDistanceY = j["mMaxDistanceY"];
    if (j.find("mYOffSet") != j.end())
        mYOffSet = j["mYOffSet"];
}

bool CameraTriggerer::EditArenaState()
{
    bool changed = false;
    {
        ImGui::Text("Center");
        ImGui::PushID("Center");
        ImGui::Columns(2, "Center", true);
        ImGui::DragFloat("X", &mCenter.x);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;

        ImGui::NextColumn();
        ImGui::DragFloat("Y", &mCenter.y);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;

        ImGui::Columns(1);
        ImGui::PopID(); 
    }
    {
        ImGui::Text("Margins");
        ImGui::PushID("HorizontalMargins");
        ImGui::Columns(2, "HorizontalMargins", true);
        ImGui::DragFloat("Left", &mHorizontalMargins.x, 1.0f, 0.0f);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;

        ImGui::NextColumn();
        ImGui::DragFloat("Right", &mHorizontalMargins.y, 1.0f, 0.0f);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;
        ImGui::Columns(1);
        ImGui::PopID();
    }
    {
        ImGui::PushID("VerticalMargins");
        ImGui::Columns(2, "VerticalMargins", true);
        ImGui::DragFloat("Bot", &mVerticalMargins.x, 1.0f, 0.0f);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;

        ImGui::NextColumn();
        ImGui::DragFloat("Top", &mVerticalMargins.y, 1.0f, 0.0f);
        if (ImGui::IsItemDeactivatedAfterEdit())
            changed = true;
        ImGui::Columns(1);
        ImGui::PopID();
    }
    {
        ImGui::DragFloat("YOffSet", &mYOffSet);
    }
    {
        ImGui::PushID("Size");
        ImGui::DragFloat("Width", &mViewRectangle.x, 1.0f, 0.0f);
        if (ImGui::IsItemDeactivatedAfterEdit())
        {
            changed = true;
            mViewRectangle.y = mViewRectangle.x * 9 / 16;
        }
        ImGui::PopID();
    }
    // Debug Lines
    if (Space* _Space = mOwner->GetParentSpace())
    {
        auto _Position = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
        static bool _DrawCameraMargins = false;
        MenusSys->HelpMenu("Activating this there will be debug lines showing where the camera can move.");
        ImGui::Checkbox("Draw Camera Center Margins", &_DrawCameraMargins);
        // Drawn in yellow the margins of the center of the camera
        if(_DrawCameraMargins)
        {
            glm::vec2 botRightMargin(_Position.x + mCenter.x + mHorizontalMargins.y,
                                     _Position.y + mCenter.y - mVerticalMargins.x);
            glm::vec2 topRightMargin(_Position.x + mCenter.x + mHorizontalMargins.y,
                                     _Position.y + mCenter.y + mVerticalMargins.y);
            glm::vec2 botLeftMargin(_Position.x + mCenter.x - mHorizontalMargins.x,
                                    _Position.y + mCenter.y - mVerticalMargins.x);
            glm::vec2 topLeftMargin(_Position.x + mCenter.x - mHorizontalMargins.x,
                                    _Position.y + mCenter.y + mVerticalMargins.y);
            _Space->DrawLine(botRightMargin, topRightMargin, Colors::yellow);
            _Space->DrawLine(botLeftMargin, topLeftMargin, Colors::yellow);
            _Space->DrawLine(botLeftMargin, botRightMargin, Colors::yellow);
            _Space->DrawLine(topLeftMargin, topRightMargin, Colors::yellow);
        }

        static bool _DrawCameraSize = false;
        MenusSys->HelpMenu("Activating this there will be debug lines showing what the camera can see.");
        ImGui::Checkbox("Draw Camera View Margins", &_DrawCameraSize);
        if(_DrawCameraSize)
        // Drawn in red the margins of what the camera can see
        {
            glm::vec2 botRightMargin(_Position.x + mCenter.x + mHorizontalMargins.y + mViewRectangle.x / 2,
                                     _Position.y + mCenter.y - mVerticalMargins.x - mViewRectangle.y / 2);
            glm::vec2 topRightMargin(_Position.x + mCenter.x + mHorizontalMargins.y + mViewRectangle.x / 2,
                                     _Position.y + mCenter.y + mVerticalMargins.y + mViewRectangle.y / 2 + mYOffSet);
            glm::vec2 botLeftMargin(_Position.x + mCenter.x - mHorizontalMargins.x - mViewRectangle.x / 2,
                                    _Position.y + mCenter.y - mVerticalMargins.x - mViewRectangle.y / 2);
            glm::vec2 topLeftMargin(_Position.x + mCenter.x - mHorizontalMargins.x - mViewRectangle.x / 2,
                                    _Position.y + mCenter.y + mVerticalMargins.y + mViewRectangle.y / 2 + mYOffSet);

            _Space->DrawLine(botRightMargin, topRightMargin, Colors::red);
            _Space->DrawLine(botLeftMargin, topLeftMargin, Colors::red);
            _Space->DrawLine(botLeftMargin, botRightMargin, Colors::red);
            _Space->DrawLine(topLeftMargin, topRightMargin, Colors::red);

            // Drawn in green the actual camera size
            _Space->Drawrectangle(glm::vec2{ _Position } + mCenter, mViewRectangle.x, mViewRectangle.y, Colors::green);
        }
        _Space->DrawCircle(glm::vec2{ _Position } + mCenter, 5.0f, Colors::green);
    }

    return changed;
}

void CameraTriggerer::TriggerArenaState()
{
    auto _StateIdle = reinterpret_cast<CameraArenaState*>(mCameraMachine->GetState("CameraArenaState"));
    auto _Position = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
    glm::vec2 _ActualCenter = glm::vec2{ _Position } + mCenter;

    _StateIdle->mViewRectangle = mViewRectangle;
    _StateIdle->mCenter = _ActualCenter;
    _StateIdle->mYOffset = mYOffSet;

    _StateIdle->mTopMargin = _ActualCenter.y + mVerticalMargins.x; 
    _StateIdle->mBotMargin = _ActualCenter.y - mVerticalMargins.y;

    _StateIdle->mLeftMargin =  _ActualCenter.x - mHorizontalMargins.x;
    _StateIdle->mRightMargin = _ActualCenter.x + mHorizontalMargins.y;
}

void CameraTriggerer::ToJsonArenaState(nlohmann::json& j)
{
    j["mCenter"].push_back(mCenter.x);
    j["mCenter"].push_back(mCenter.y);

    j["mViewRectangle"].push_back(mViewRectangle.x);
    j["mViewRectangle"].push_back(mViewRectangle.y);

    j["mHorizontalMargins"].push_back(mHorizontalMargins.x);
    j["mHorizontalMargins"].push_back(mHorizontalMargins.y);

    j["mVerticalMargins"].push_back(mVerticalMargins.x);
    j["mVerticalMargins"].push_back(mVerticalMargins.y);
}

void CameraTriggerer::FromJsonArenaState(const nlohmann::json& j)
{
    if (j.find("mCenter") != j.end())
    {
        mCenter.x = j["mCenter"][0];
        mCenter.y = j["mCenter"][1];
    }
    if (j.find("mViewRectangle") != j.end())
    {
        mViewRectangle.x = j["mViewRectangle"][0];
        mViewRectangle.y = j["mViewRectangle"][1];
    }
    if (j.find("mHorizontalMargins") != j.end())
    {
        mHorizontalMargins.x = j["mHorizontalMargins"][0];
        mHorizontalMargins.y = j["mHorizontalMargins"][1];
    }
    if (j.find("mVerticalMargins") != j.end())
    {
        mVerticalMargins.x = j["mVerticalMargins"][0];
        mVerticalMargins.y = j["mVerticalMargins"][1];
    }
}
