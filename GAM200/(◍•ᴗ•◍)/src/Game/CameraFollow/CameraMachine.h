#pragma once
#include "../../RTTI/SM_RTTI.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../CameraFollow/CameraEffect/CameraEffect.h"
#include "../../Graphics/Camera/Camera.h"

class GameObject;
class TransformComponent;
struct CameraEffect;

class CameraMachine :public StateMachine
{
	friend class CameraStaticState;
	friend class CameraPurificationState;
	friend class CameraHoverboardState;
	friend class CameraArenaState;
	friend class CameraFollowStateIdle;
	friend class CameraFollowStateFollow;

	friend class CameraTriggerer;

	SM_RTTI_DECL(CameraMachine, StateMachine);

public:

	CameraMachine();
	~CameraMachine();

	CameraMachine& operator=(const CameraMachine& _Rhs);
	CameraMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void AddEffect(CameraEffectLists _EffectType, float _Duration);
	void AddEffect(CameraEffect & _Effect);
	TransformComponent* mCameraTransform;
private:

	void PerformEffects();

	
	TransformComponent* mPlayerTransform;
	CameraComponent*	mCameraComponent;

	glm::vec3 mFocusPosition = glm::vec3{0,0,20};
	float mTransitionSpeed = 0.005f;

	std::vector<CameraEffect> mEffectsBuffer;
};