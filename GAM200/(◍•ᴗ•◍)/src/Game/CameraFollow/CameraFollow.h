#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/IComp.h"
#include "../../RTTI/SM_RTTI.h"

#include "../../LogicSystem/LogicComponent.h"
#include "../../Graphics/Camera/Camera.h"
#include "../../Time/Time.h"
#include "../../PropertySystem/PropertyMap.h"


class GameObject;
class TransformComponent;
class RigidBody;

enum CameraMode
{
	FollowMode,
	ArenaMode,
	HoverboardMode,
	PurificationMode
};

class CameraFollow : public LogicComponent
{
	SM_RTTI_DECL(CameraFollow, LogicComponent);
public:
	CameraFollow();
	~CameraFollow() {}

	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override
	{
		LogicComponent::Shutdown();
	}
	bool Edit()override;
	CameraFollow* Clone() override;
	CameraFollow& operator=(const CameraFollow& rhs);

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void ChangeMode(CameraMode newMode);
	CameraMode GetMode();
	void SetMargins(float _left, float _right, float _bot, float _top);
	void SetTargetSize(unsigned _width, unsigned _height);
	void SetYOffSet(float _YOffSet);

private:
	CameraMode mMode = CameraMode::FollowMode;

	GameObject* mPlayer = nullptr;//Variable for the PlayerObject

	TransformComponent* mPlayerTransform = nullptr;//Variable for the PlayerTransform
	TransformComponent* mOwnerTransform = nullptr;//Variable for the CameraTransform

	Camera* mCamera = nullptr;//Variable for the camera
	RigidBody* mPlayerRB = nullptr;//Variable for the PlayerRigidBody

	glm::vec3 mFocusPosition = glm::vec3();
	
	glm::vec2 mOriginalSize = glm::vec2();//Variable to store the original camera viewport
	glm::vec2 mTargetSize = glm::vec2();

	bool  cReturning = false;//Variable to know if the camera is moving or returning
	float mCurrentDistance = 0.0f;
	float mOriginalVelocity = 0.0f;
	float mPrevDirection = 0.0f;
	float counter = 0.f;

	PropertyMap mProperties;
};