#include <glm/glm.hpp>

#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Factory/Factory.h"

#include "../../GameObject/GameObject.h"

#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Input/Input.h"

#include "CameraFollow.h"

CameraFollow::CameraFollow() :mMode(FollowMode) 
{
    SetName("CameraFollow");
    // Arena Margins
    {
        mProperties.AddProperty<float>("mLeftMargin");
        mProperties.AddProperty<float>("mRightMargin");
        mProperties.AddProperty<float>("mTopMargin");
        mProperties.AddProperty<float>("mBotMargin");
    }
    // Camera Follow Variables Margins
    {
        mProperties.AddProperty<float>("mTransitionSpeed");
        mProperties.AddProperty<float>("cZCoord");
        mProperties.AddProperty<float>("mYOffset");
        mProperties.AddProperty<float>("mVelocity");
        mProperties.AddProperty<float>("mAcceleration");
        mProperties.AddProperty<float>("mMaxDistance"); 
        mProperties.AddProperty<float>("mMaxVelocity"); 
    }
}

void CameraFollow::Initialize()
{
    LogicComponent::Initialize();

    mCamera = mOwner->GetComp<CameraComponent>()->GetCamera();
    mOwnerTransform = mOwner->GetComp<TransformComponent>();
    mProperties(1.0f, "cZCoord") = mOwnerTransform->GetWorldPosition().z;
    mOriginalSize = mTargetSize = mCamera->GetViewRectangle();
    mOriginalVelocity = mProperties(1.0f, "mVelocity");

    GameObject* player = mOwner->GetParentSpace()->FindObjectByName("Player");
    if (player)
    {
        glm::vec3 playerPos(player->GetComp<TransformComponent>()->GetPosition().x, player->GetComp<TransformComponent>()->GetPosition().y + mProperties(1.0f, "mYOffset"), mProperties(1.0f, "cZCoord"));
        mOwnerTransform->SetWorldPosition(playerPos);
        mPlayer = player;
    }

    if (mPlayer)
    {
        mPlayerTransform = mPlayer->GetComp<TransformComponent>();
        mPlayerRB = mPlayer->GetComp<RigidBody>();
        mFocusPosition = mPlayerTransform->GetWorldPosition();
        mFocusPosition.y += mProperties(1.0f, "mYOffset");
        mFocusPosition.z = mProperties(1.0f, "cZCoord");
    }
   

}

void CameraFollow::Update()
{
    switch (mMode)
    {
    case FollowMode:
    {
        mCamera->mViewRectangle = glm::mix(mCamera->mViewRectangle, mOriginalSize, mProperties(1.0f, "mTransitionSpeed"));
        glm::vec3 playerPos(mPlayerTransform->GetPosition().x, mPlayerTransform->GetPosition().y + mProperties(1.0f, "mYOffset"), mProperties(1.0f, "cZCoord"));

        mCurrentDistance = glm::abs(mOwnerTransform->GetWorldPosition().x - playerPos.x);

        // Safety distance to avoid the camera vibrating when the player is stopped
        if (mPlayerRB->GetDirection() == 0.0f)
        {
            mProperties(1.0f, "mVelocity") = mOriginalVelocity;
            if (playerPos.x  - mOwnerTransform->GetWorldPosition().x  < 0.0f && mCurrentDistance > 2.5f)
            {
                mFocusPosition.x -= mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            }
            else if (playerPos.x  - mOwnerTransform->GetWorldPosition().x > 0.0f && mCurrentDistance > 2.5f)
            {
                mFocusPosition.x += mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            }

            mFocusPosition.y = playerPos.y;
            mOwnerTransform->SetWorldPosition(mFocusPosition);
            return;
        }
        // Player is going right and the camera already passed her
        if (mPlayerRB->GetDirection() > 0.0f && (playerPos.x < mOwnerTransform->GetWorldPosition().x ))
        {

            if (glm::abs(mProperties(1.0f, "mMaxVelocity") > mProperties(1.0f, "mVelocity")))
                mProperties(1.0f, "mVelocity") += mProperties(1.0f, "mAcceleration") * TimeSys->GetFrameTime();
            mFocusPosition.x += mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            mFocusPosition.y = playerPos.y;
            mOwnerTransform->SetWorldPosition(mFocusPosition);
        }
        // Player is going left and the camera already passed her
        else if ((mPlayerRB->GetDirection() < 0.0f)&& (playerPos.x > mOwnerTransform->GetWorldPosition().x))
        {

            if (glm::abs(mProperties(1.0f, "mMaxVelocity") > mProperties(1.0f, "mVelocity")))
                mProperties(1.0f, "mVelocity") += mProperties(1.0f, "mAcceleration") * TimeSys->GetFrameTime();
            mFocusPosition.x -= mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            mFocusPosition.y = playerPos.y;
            mOwnerTransform->SetWorldPosition(mFocusPosition);
        }
        // Player is going right and the camera did not pass her
        else if ((playerPos.x > mOwnerTransform->GetWorldPosition().x))
        {
            if (glm::abs(mProperties(1.0f, "mMaxVelocity") > mProperties(1.0f, "mVelocity")))
                mProperties(1.0f, "mVelocity") += mProperties(1.0f, "mAcceleration") * TimeSys->GetFrameTime();

            mFocusPosition.x += mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            mFocusPosition.y = playerPos.y;
            mOwnerTransform->SetWorldPosition(mFocusPosition);
        }
        // Player is going left and the camera did not pass her
        else if ((playerPos.x < mOwnerTransform->GetWorldPosition().x))
        {
            if(glm::abs(mProperties(1.0f, "mMaxVelocity") > mProperties(1.0f, "mVelocity")))
                mProperties(1.0f, "mVelocity") += mProperties(1.0f, "mAcceleration") * TimeSys->GetFrameTime();

            mFocusPosition.x -= mProperties(1.0f, "mVelocity") * TimeSys->GetFrameTime();
            mFocusPosition.y = playerPos.y;
            mOwnerTransform->SetWorldPosition(mFocusPosition);
        }

        
        break;
    }
    case ArenaMode:
    {
        mCamera->mViewRectangle = glm::mix(mCamera->mViewRectangle, mTargetSize, mProperties(1.0f, "mTransitionSpeed"));
        glm::vec3 tmp = mOwnerTransform->GetWorldPosition();
        if ((mPlayerTransform->GetWorldPosition().x > mProperties(1.0f, "mLeftMargin")) && (mPlayerTransform->GetWorldPosition().x < mProperties(1.0f, "mRightMargin")) )
        {
            tmp.x = glm::mix(mOwnerTransform->GetWorldPosition().x, mPlayerTransform->GetWorldPosition().x, 1.0f);
        }
        if (((mPlayerTransform->GetWorldPosition().y + mProperties(1.0f, "mYOffset")) > mProperties(1.0f, "mBotMargin")) && ((mPlayerTransform->GetWorldPosition().y + 100) < mProperties(1.0f, "mTopMargin")))
        {
            tmp.y = glm::mix(mOwnerTransform->GetWorldPosition().y, mPlayerTransform->GetWorldPosition().y + mProperties(1.0f, "mYOffset"), 1.0f);
        }
        mOwnerTransform->SetWorldPosition(tmp);
    }
        break;
    case HoverboardMode:
    {
        glm::vec3 pos = mPlayerTransform->GetPosition();
        pos.x = glm::mix(mOwnerTransform->GetWorldPosition().x, mPlayerTransform->GetWorldPosition().x, 0.1f);
        pos.y = glm::mix(mOwnerTransform->GetWorldPosition().y, mPlayerTransform->GetWorldPosition().y, 0.1f);
        pos.z = 20;
        mOwner->GetComp<TransformComponent>()->SetWorldPosition(pos);
        break;
    }
    case PurificationMode:
    {
        CameraComponent* camview = nullptr;
        camview = mOwner->GetComp<CameraComponent>();
        glm::vec2 sc = camview->GetCamera()->GetViewRectangle();

        counter += TimeSys->GetFrameTime()/4;
        
        if (counter < 1)
        {
            sc.x = glm::mix(1280.f, 640.f, counter);

            sc.y = glm::mix(720.f, 360.f, counter);
        }
        
        camview->GetCamera()->SetViewRectangle(sc);
        break;
    }
    default:
        break;
    }
}

void CameraFollow::LateUpdate()
{
    switch (mMode)
    {
    case FollowMode:
    {
        glm::vec3 playerPos(mPlayerTransform->GetPosition().x, mPlayerTransform->GetPosition().y + mProperties(1.0f, "mYOffset"), mProperties(1.0f, "cZCoord"));
        mCurrentDistance = glm::abs(mOwnerTransform->GetWorldPosition().x - playerPos.x);
        float _Direction = (mOwnerTransform->GetWorldPosition().x - playerPos.x) / glm::abs(mOwnerTransform->GetWorldPosition().x - playerPos.x);

        if (mCurrentDistance > mProperties(1.0f, "mMaxDistance"))
        {
            if (_Direction > 0.0f)
            {
                mFocusPosition.x = playerPos.x + mProperties(1.0f, "mMaxDistance");
                mFocusPosition.y = playerPos.y;
                mOwnerTransform->SetWorldPosition(mFocusPosition);
            }
            else if (_Direction < 0.0f)
            {
                mFocusPosition.x = playerPos.x - mProperties(1.0f, "mMaxDistance");
                mFocusPosition.y = playerPos.y;
                mOwnerTransform->SetWorldPosition(mFocusPosition);
            }
            return;
        }
    }
        break;
    case ArenaMode:
        break;
    case HoverboardMode:
        break;
    default:
        break;
    }
    
}

bool CameraFollow::Edit()
{
    if (ImGui::BeginDragDropTarget())
    {
        if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
        {
            IM_ASSERT(payload->DataSize == sizeof(GameObject*));
            GameObject* _Object = *((GameObject**)payload->Data);
            if (_Object->CompareTag("Player"))
            {
                mPlayer = _Object;
            }
        }
        ImGui::EndDragDropTarget();
    }
    ImGui::DragFloat("mYOffset", &mProperties(1.0f, "mYOffset"));
    ImGui::DragFloat("mTransitionSpeed", &mProperties(1.0f, "mTransitionSpeed"));
    ImGui::DragFloat("mVelocity", &mProperties(1.0f, "mVelocity"));
    ImGui::DragFloat("mAcceleration", &mProperties(1.0f, "mAcceleration"));
    ImGui::DragFloat("mMaxDistance", &mProperties(1.0f, "mMaxDistance"));
    ImGui::DragFloat("mMaxVelocity", &mProperties(1.0f, "mMaxVelocity"));
    return false;
}

CameraFollow* CameraFollow::Clone()
{
    CameraFollow* temp = FactorySys->Create<CameraFollow>();
    (*temp) = (*this);
    return temp;
}

CameraFollow& CameraFollow::operator=(const CameraFollow& rhs)
{
    return *this;
}

void CameraFollow::ToJson(nlohmann::json& j)
{
    IBase::ToJson(j);
    mPlayer = SceneSys->GetMainSpace()->FindObjectByName("Player");
    j["pObject"] = mPlayer->GetName();
    j["pObjectID"] = mPlayer->GetUID();
    mProperties.ToJson(j);
    j["cReturning"] = cReturning;
}

void CameraFollow::FromJson(const nlohmann::json& j)
{
    IBase::FromJson(j); 
    mProperties.FromJson(j);
}

void CameraFollow::ChangeMode(CameraMode newMode)
{
    mMode = newMode;
}

CameraMode CameraFollow::GetMode()
{
    return mMode;
}

void CameraFollow::SetMargins(float _left, float _right, float _bot, float _top)
{
     mProperties(1.0f, "mLeftMargin") = _left;
     mProperties(1.0f, "mRightMargin") = _right;
     mProperties(1.0f, "mBotMargin") = _bot;
     mProperties(1.0f, "mTopMargin") = _top;
}

void CameraFollow::SetTargetSize(unsigned _width, unsigned _height)
{
    // TODO: Assert system in here
    mTargetSize.x = _width;
    mTargetSize.y = _height;
}

void CameraFollow::SetYOffSet(float _YOffSet)
{
    mProperties(1.0f, "mYOffset") = _YOffSet;
}
