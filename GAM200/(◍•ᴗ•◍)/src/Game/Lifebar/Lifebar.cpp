#include "../../Time/Time.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"

#include "../../Transform2D/TransformComponent.h"
#include "../../Input/Input.h"
#include "../Health/Health.h"

#include "Lifebar.h"

LifebarMachine::LifebarMachine() {
	mName = "LifebarMachine";

	mInitialState = new LifebarNormal("LifebarNormal");
	mCurrentState = mInitialState;
	AddState(mInitialState);
}

LifebarMachine::~LifebarMachine() { StateMachine::~StateMachine(); }

void LifebarMachine::Initialize()
{
	mHealth = reinterpret_cast<Health*>(SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Health>());
	
	if (mHealth)
	{
		initial_health = mHealth->mHealth;
		previous_health = mHealth->mHealth;
	}
	mTransform = mActor->GetComp<TransformComponent>();
	StateMachine::Initialize();
}

void LifebarMachine::Update()
{
	if (InputSys->key_is_down(keys::V))
	{
		mHealth->Damage(10.f);
		
	}


	if (mTransform != nullptr && mHealth)
	{
		if (previous_health != mHealth->mHealth)
		{
			float percentage = mHealth->mHealth / initial_health;
			float new_x_scale = 250.f * percentage;


			float scale_diff = mTransform->GetWorldScale().x - new_x_scale;

			mTransform->SetWorldScale(glm::vec2(new_x_scale, 25));
			mTransform->SetWorldPosition(glm::vec3(mTransform->GetPosition().x - scale_diff/2, mTransform->GetPosition().y, 0));
			previous_health = mHealth->mHealth;
		}
	}

	StateMachine::Update();
}

void LifebarMachine::Shutdown()
{
	StateMachine::Clear();
}

LifebarMachine& LifebarMachine::operator=(const LifebarMachine& rhs)
{
	return *this;
}

LifebarMachine* LifebarMachine::Clone()
{
	LifebarMachine* temp = FactorySys->Create<LifebarMachine>();
	*temp = *this;
	return temp;
}

void LifebarMachine::ToJson(nlohmann::json& j) { IBase::ToJson(j); }

void LifebarMachine::FromJson(const nlohmann::json& j) { IBase::FromJson(j); }

LifebarNormal::LifebarNormal(const char* name) : State(name)
{}

void LifebarNormal::Initialize()
{
	mCastedMachine = reinterpret_cast<LifebarMachine*>(mOwnerStateMachine);
}

void LifebarNormal::LogicEnter()
{}

void LifebarNormal::LogicUpdate()
{}

void LifebarNormal::LogicExit()
{}

