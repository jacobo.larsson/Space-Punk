#pragma once
#include "../../StateMachines/StateMachine/StateMachine.h"

class Health;
class TransformComponent;

class LifebarMachine : public StateMachine
{
	SM_RTTI_DECL(LifebarMachine, LifebarMachine);

public:

	LifebarMachine();
	~LifebarMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	LifebarMachine& operator=(const LifebarMachine& rhs);
	LifebarMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	Health* mHealth;
	float initial_health;
	float previous_health = 0.f;
	TransformComponent* mTransform;
};

class LifebarNormal : public State
{
public:
	LifebarNormal(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	LifebarMachine* mCastedMachine = nullptr;
};


