#pragma once
#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"


class SimpleMovementMachine;


class SimplePlayerIdle : public State
{
public:
	SimplePlayerIdle(const char* name = nullptr);


	void LogicEnter() override;
	void Initialize() override;
	void LogicExit();

	void LogicUpdate();
	TransformComponent* pTransform;
	SimpleMovementMachine* castedMachine;
};


class SimplePlayerJumping : public State
{
public:
	SimplePlayerJumping(const char* name = nullptr);
	void LogicEnter() override;

	void LogicExit();

	void Initialize();

	void LogicUpdate();
	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionPersisted(GameObject* other)override;
	void Jump();

	SimpleMovementMachine* castedMachine;
	bool doubleJumped;
};




class SimplePlayerMoving : public State
{
public:
	SimplePlayerMoving(const char* name = nullptr);
	void Initialize() override;


	void LogicEnter() override;
	void LogicExit();

	void LogicUpdate();
	void OnCollisionPersisted(GameObject* other)override;

	bool Right = false;

	TransformComponent* pTransform;
	SimpleMovementMachine* castedMachine;
};

#pragma region SimplePlayerDashing
class SimplePlayerDashingFast : public State {
public:
	SimplePlayerDashingFast(const char* name = nullptr);
	void Initialize() override;


	void LogicEnter() override;
	void LogicExit();

	void LogicUpdate();
	SimpleMovementMachine* castedMachine;

	void OnCollisionStarted(GameObject* other)override;

};



class SimplePlayerDashRecovering : public State {
public:
	SimplePlayerDashRecovering(const char* name = nullptr);
	void Initialize() override;


	void LogicEnter() override;
	void LogicExit();

	void LogicUpdate();
	void OnCollisionStarted(GameObject* other)override;

	SimpleMovementMachine* castedMachine;
};

#pragma endregion