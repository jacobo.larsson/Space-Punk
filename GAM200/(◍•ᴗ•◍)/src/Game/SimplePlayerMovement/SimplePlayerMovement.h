#pragma once

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/RayCaster.h"
#include "../../StateMachines/SuperState/SuperState.h"


class RigidBody;
class TransformComponent;


class SimpleMovementMachine : public StateMachine
{
	SM_RTTI_DECL(SimpleMovementMachine, StateMachine);

public:

	SimpleMovementMachine();
	~SimpleMovementMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	SimpleMovementMachine& operator=(const SimpleMovementMachine& rhs);
	SimpleMovementMachine* Clone()override;


	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;
	void Accelerate(float acc);
	void UpdatePosition();
	void BrakeX();

	float MaxXVelocity = 500.0f;
	float CurrentVelocity = 0;
	float yVelocity = 0;
	float yMaxVelocity = 400.0f;
	float Acceleration = 2000.0f;
	bool FacingRight = true;
	bool PreviousFacingRight = true;
	bool canJump = true;

	TransformComponent* pTransform;
	RigidBody* pRigidbody;


};
