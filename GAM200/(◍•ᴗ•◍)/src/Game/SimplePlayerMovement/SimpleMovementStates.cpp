﻿#include <iostream>

#include <glm/glm.hpp>

#include "../../Input/Input.h" //Input for movement
#include "../GameInput/GameInput.h"

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Game/BasicEnemy/BasicEnemySystem.h"
#include "../../Game/TurretEnemy/TurretEnemySystem.h"
#include "../../Game/ChargerEnemy/ChargerEnemySystem.h"
#include "../../Game/FlyingEnemy/FlyingEnemySystem.h"
#include "../../Time/Time.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/Raycast.h"

#include "SimpleMovementStates.h"
#include "SimplePlayerMovement.h"




#pragma region SimplePlayerIdle
SimplePlayerIdle::SimplePlayerIdle(const char* name) : State(name){

}
void SimplePlayerIdle::LogicEnter()
{
	//castedMachine->CurrentVelocity = 0;
	castedMachine->canJump = true;
	mTimeInState = 0.0f;
}

void SimplePlayerIdle::Initialize()
{
	castedMachine = dynamic_cast<SimpleMovementMachine*>(mOwnerStateMachine);
	pTransform = mActor->GetComp<TransformComponent>();
	State::Initialize();
}

void SimplePlayerIdle::LogicExit()
{
}

void SimplePlayerIdle::LogicUpdate()
{
	if (GameInput::JumpKeyPressed()) {
		if (castedMachine->canJump) {
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerJumping"));
			castedMachine->canJump = false;
		}
	}
	else if (GameInput::RightKeyPressed() || GameInput::LeftKeyPressed()) {

		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerMoving"));

	}
	else if (GameInput::DashKeyPressed()) {
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerDashingFast"));
	}

	else
	{
		castedMachine->BrakeX();
	}

	castedMachine->UpdatePosition();
}
#pragma endregion SimplePlayerIdle



#pragma region SimplePlayerJumping
SimplePlayerJumping::SimplePlayerJumping(const char* name) : State(name)
{
}

void SimplePlayerJumping::LogicEnter()
{
	Jump();
	doubleJumped = false;
}

void SimplePlayerJumping::LogicExit()
{
}

void SimplePlayerJumping::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<SimpleMovementMachine*>(mOwnerStateMachine);
}

void SimplePlayerJumping::LogicUpdate()
{
	if (GameInput::RightKeyPressed()) {

		castedMachine->Accelerate(castedMachine->Acceleration);

		castedMachine->FacingRight = true;


	}
	else if (GameInput::LeftKeyPressed()) {

		castedMachine->Accelerate(-castedMachine->Acceleration);

		castedMachine->FacingRight = false;

	}
	else {
		castedMachine->BrakeX();
	}

	if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
		castedMachine->BrakeX();
	}

	if (GameInput::JumpKeyPressed() && !doubleJumped) {
		Jump();
		doubleJumped = true;
	}

	if (GameInput::DashKeyPressed()) {
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerDashingFast"));
	}
	
	castedMachine->UpdatePosition();
	castedMachine->PreviousFacingRight = castedMachine->FacingRight;

}

void SimplePlayerJumping::OnCollisionStarted(GameObject* other)
{


	if (other->CompareTag("Platform") ) {
		castedMachine->canJump = true;
		mOwnerStateMachine->ChangeState("SimplePlayerIdle");
	}


}

void SimplePlayerJumping::OnCollisionPersisted(GameObject* other)
{
}

void SimplePlayerJumping::Jump()
{
	mActor->GetComp<RigidBody>()->Velocity.y = 600;
}

#pragma endregion SimplePlayerJumping




#pragma region SimplePlayerMoving
SimplePlayerMoving::SimplePlayerMoving(const char* name) : State(name) {}

void SimplePlayerMoving::Initialize()
{
	castedMachine = dynamic_cast<SimpleMovementMachine*>(mOwnerStateMachine);
	pTransform = mActor->GetComp<TransformComponent>();
	State::Initialize();
}

void SimplePlayerMoving::LogicEnter()
{
	mTimeInState = 0.0f;
}

void SimplePlayerMoving::LogicExit()
{
}


void SimplePlayerMoving::LogicUpdate()
{
	if (GameInput::RightKeyPressed()) {
		castedMachine->Accelerate(castedMachine->Acceleration);

		castedMachine->FacingRight = true;
		if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
			castedMachine->BrakeX();
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerIdle"));
		}
	}
	else if (GameInput::LeftKeyPressed()) {
		castedMachine->Accelerate(-castedMachine->Acceleration);

		castedMachine->FacingRight = false;
		if (castedMachine->FacingRight != castedMachine->PreviousFacingRight) {
			castedMachine->BrakeX();
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerIdle"));
		}
	}
	else {
		castedMachine->BrakeX();
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerIdle"));
	}


	if (GameInput::JumpKeyPressed()) {
		if (castedMachine->canJump) {
			mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerJumping"));
			castedMachine->canJump = false;
		}
	}

	if (GameInput::DashKeyPressed()) {
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("SimplePlayerDashingFast"));
	}

	castedMachine->UpdatePosition();
	castedMachine->PreviousFacingRight = castedMachine->FacingRight;
}

void SimplePlayerMoving::OnCollisionPersisted(GameObject* other)
{
	if (other->CompareTag("Platform")) {
		castedMachine->canJump = true;
		mOwnerStateMachine->ChangeState("SimplePlayerIdle");
	}

}


#pragma endregion SimplePlayerMoving




#pragma region SimplePlayerDashing

#pragma region  SimplePlayerDashingFast
SimplePlayerDashingFast::SimplePlayerDashingFast(const char* name) :State(name)
{

}
void SimplePlayerDashingFast::Initialize()
{
	castedMachine = dynamic_cast<SimpleMovementMachine*> (mOwnerStateMachine);
}

void SimplePlayerDashingFast::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->pTransform->SetScale(glm::vec2(60, 50));

	castedMachine->CurrentVelocity = (castedMachine->FacingRight) ? 800.0f : -800.0f;

}

void SimplePlayerDashingFast::LogicExit()
{
}

void SimplePlayerDashingFast::LogicUpdate()
{

	castedMachine->UpdatePosition();
	if (mTimeInState > 0.35f) {

		mOwnerStateMachine->ChangeState("SimplePlayerDashRecovering");

	}
}

void SimplePlayerDashingFast::OnCollisionStarted(GameObject* other)
{


}

#pragma endregion

#pragma region SimplePlayerDashRecovering

SimplePlayerDashRecovering::SimplePlayerDashRecovering(const char* name) : State(name)
{
}

void SimplePlayerDashRecovering::Initialize()
{
	castedMachine = dynamic_cast<SimpleMovementMachine*> (mOwnerStateMachine);
}

void SimplePlayerDashRecovering::LogicEnter()
{
	castedMachine->CurrentVelocity = (castedMachine->FacingRight) ? 50.0f : -50.0f;


	castedMachine->pTransform->SetScale(glm::vec2(50, 50));



}


void SimplePlayerDashRecovering::LogicUpdate()
{


	castedMachine->UpdatePosition();
	if (mTimeInState > 0.4f) {

		//set again the dash machine to its intiial state
		//mOwnerStateMachine->ChangeState("PlayerDashing");
		//mOwnerStateMachine->Reset();
		castedMachine->ChangeState("SimplePlayerIdle");

	}

}

void SimplePlayerDashRecovering::OnCollisionStarted(GameObject* other)
{

}

void SimplePlayerDashRecovering::LogicExit()
{

}



#pragma endregion

