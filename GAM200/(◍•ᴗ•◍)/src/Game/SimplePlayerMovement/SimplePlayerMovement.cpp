﻿#include <iostream>

#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h" //Input for movement
#include "../GameInput/GameInput.h"
#include "../../Factory/Factory.h" 

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"

#include "SimplePlayerMovement.h"
#include "SimpleMovementStates.h"


#pragma region SimpleMovementMachine

SimpleMovementMachine::SimpleMovementMachine() {
	mName = "SimpleMovementMachine";

	mInitialState = new SimplePlayerIdle("SimplePlayerIdle");
	mCurrentState = mInitialState;

	AddState(mInitialState);
	AddState(new SimplePlayerMoving("SimplePlayerMoving"));
	AddState(new SimplePlayerJumping("SimplePlayerJumping"));
}

SimpleMovementMachine::~SimpleMovementMachine() { StateMachine::~StateMachine(); }

void SimpleMovementMachine::Initialize()
{
	StateMachine::Initialize();

	srand(time(0));
	pTransform = mActor->GetComp<TransformComponent>();
	pRigidbody = mActor->GetComp<RigidBody>();

}

void SimpleMovementMachine::Update()
{
	StateMachine::Update();
}

void SimpleMovementMachine::Shutdown()
{
	StateMachine::Clear();
}

bool SimpleMovementMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		ImGui::DragFloat("Max X velocity", &MaxXVelocity);
		ImGui::DragFloat("Max Y velocity", &yMaxVelocity);
		ImGui::DragFloat("Acceleration", &Acceleration);
		ImGui::DragFloat("Max X velocity", &MaxXVelocity);
		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	return false;
}

SimpleMovementMachine& SimpleMovementMachine::operator=(const SimpleMovementMachine& rhs)
{
	MaxXVelocity = rhs.MaxXVelocity;
	CurrentVelocity = rhs.CurrentVelocity;
	yVelocity = rhs.yVelocity;
	yMaxVelocity = rhs.yMaxVelocity;
	Acceleration = rhs.Acceleration;

	FacingRight = rhs.FacingRight;
	PreviousFacingRight = rhs.PreviousFacingRight;
	canJump = rhs.canJump;

	return *this;
}

SimpleMovementMachine* SimpleMovementMachine::Clone()
{
	SimpleMovementMachine* temp = FactorySys->Create<SimpleMovementMachine>();
	*temp = *this;
	return temp;
}

void SimpleMovementMachine::ToJson(nlohmann::json& j) { IBase::ToJson(j); }

void SimpleMovementMachine::FromJson(const nlohmann::json& j) { IBase::FromJson(j); }

void SimpleMovementMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);
}

void SimpleMovementMachine::Accelerate(float acc)
{
	//hang the velocity by accelerating
	CurrentVelocity += acc * (float)TimeSys->GetFrameTime();
	//clamp the velocity with max and min velocity
	CurrentVelocity = glm::clamp(CurrentVelocity, -MaxXVelocity, MaxXVelocity);
	//std::cout << "current Velocity " << CurrentVelocity << std::endl;

}
void SimpleMovementMachine::UpdatePosition()
{
	glm::vec3 position = pTransform->GetWorldPosition();
	glm::vec3 newPosition = glm::vec3(position.x + CurrentVelocity * TimeSys->GetFrameTime(), position.y + yVelocity * TimeSys->GetFrameTime(), position.z);
	pTransform->SetWorldPosition(newPosition);
}

void SimpleMovementMachine::BrakeX()
{
	CurrentVelocity = 0;
}

#pragma endregion
