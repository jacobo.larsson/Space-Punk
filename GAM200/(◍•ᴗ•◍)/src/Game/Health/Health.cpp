#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Factory/Factory.h"

#include "Health.h"

Health::Health()
{
	SetName("Health");
}

Health::~Health()
{
}

Health& Health::operator=(const Health& rhs)
{
	mHealth = rhs.mHealth;
	return *this;
}

Health* Health::Clone()
{
	Health* temp = FactorySys->Create<Health>();
	(*temp) = (*this);
	return temp;
}

void Health::Initialize()
{
}

void Health::Update()
{
	CheckDie();
}

void Health::Shutdown()
{
}

bool Health::Edit()
{
	bool changed = false;
	ImGui::PushID(&mHealth);
	ImGui::DragFloat("Health", &mHealth, 0.2f, 1.0f);
	ImGui::PopID();
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	return changed;
}

void Health::ToJson(nlohmann::json& j)
{
	j["mHealth"] = mHealth;
	j["can_get_damaged"] = can_get_damaged;
}

void Health::FromJson(const nlohmann::json& j)
{
	if(j.find("mHealth") != j.end())
		mHealth= j["mHealth"];

	if (j.find("can_get_damaged") != j.end())
		can_get_damaged = j["can_get_damaged"];
}

float Health::GetHealth()
{
	return mHealth;
}

void Health::CheckDie()
{
}



void Health::Damage(float _HP)
{
	if (can_get_damaged && _HP >= 0.0f)
		mHealth -= _HP;
}

void Health::Recover(float _HP)
{
	if (mHealth < 100.0f)
	{
		if (mHealth + _HP > 100.f)
			mHealth = 100.f;
		else
			mHealth += _HP;
	}

}
