#pragma once
#include "../../RTTI/SM_RTTI.h"

#include "../../LogicSystem/LogicComponent.h"
#include "../../Cheats/Cheats.h"

class Health : public LogicComponent
{

	friend class Cheats;
	friend class LifebarMachine;
	SM_RTTI_DECL(Health, LogicComponent);
public:
	Health();
	~Health();
	Health& operator=(const Health& rhs);
	Health* Clone()override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	float GetHealth();
	void CheckDie();
	void Damage(float _HP);
	void Recover(float _HP);
private:
	float mHealth =10.0f;
	bool can_get_damaged = true;
};