#pragma once

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../RTTI/SM_RTTI.h"

class TransformComponent;
class SkeletonRenderable;
class ShotReceiver;
class Renderable;
class Collider;
class Health;
class Ray;

class BasicEnemyMachine : public StateMachine
{
	SM_RTTI_DECL(BasicEnemyMachine, StateMachine);

public:

	BasicEnemyMachine();
	~BasicEnemyMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void OnCollisionStarted(GameObject* other) override;

	BasicEnemyMachine& operator=(const BasicEnemyMachine& rhs);
	BasicEnemyMachine* Clone()override;

	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void AddPlatform(GameObject* _Platform);
	bool CheckOutOfBounds();
	void Flip();


	//External Pointers
	TransformComponent* _TargetTransform = nullptr;
	TransformComponent* mOwnerTransform = nullptr;
	SkeletonRenderable* mSkelRen = nullptr;
	std::vector<GameObject*> mPlatforms;
	Collider* _TargetCollider = nullptr;
	Collider* mOwnerCollider= nullptr;
	Renderable* mRenderable = nullptr;
	GameObject* _Target = nullptr;
	ShotReceiver* mShot = nullptr;
	Health* mHealth = nullptr;
	Ray* mRightRay = nullptr;
	Ray* mLeftRay = nullptr;

	//DoPath
	bool mMoveInRange = false;
	float mRightRange = 0.0f; 
	float mMoveSpeed = 0.0f;
	float mLeftRange = 0.0f;

	//GoToPlayer
	float mFollowingRange = 0.0f;
	float mChaseSpeed = 0.0f;
	glm::vec2 mViewRange;

	//Attack
	float mBackwardsDistance = 0.0f;
	float mJumpAttackSpeed = 0.0f;
	float mBackAttackSpeed = 0.0f;
	float mAttackDistance = 0.0f;
	float mAttackCooldown = 0.0f;
	glm::vec2 mAttackRange;

	//Reset
	bool KnockbackDirectionRight;
	GameObject* receivedHitbox;
	float mHDamage;
	float mHKSPeed;
	float mHKTime;

	//Others
	float mStunDuration = 0.0f;
	bool mFacingRight = false;
	float mDirection = 1.0f;
	bool ArenaMode = false;
	int purif_prob = 50;
	bool Shot = false;
};