#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"


class BasicEnemyTicketMachine;
class Renderable;

class BasicEnemyTicketIdle: public State
{
public:
	BasicEnemyTicketIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicUpdate()override;
	void LogicEnter() override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;
};

class BasicEnemyTicketDoPath: public State
{
public:
	BasicEnemyTicketDoPath(const char* name = nullptr);

	void Initialize() override;
	void LogicUpdate()override;
	void LogicEnter() override;
	void LogicExit()override;

	glm::vec2 mRightMargin = glm::vec2(0,0);
	BasicEnemyTicketMachine* castedMachine;
	glm::vec2 mLeftMargin = glm::vec2(0,0);
	bool getting_out_edge = false;
};

class BasicEnemyTicketGoToPlayer : public State
{
public:
	BasicEnemyTicketGoToPlayer(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;
};

class BasicEnemyTicketAttack : public State
{
public:
	BasicEnemyTicketAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;

	float direction;

	bool Attacking = false;
	bool GoingBackwards = true;
	bool AttackaAnim = false;

	float AttackAnimDur;
	float AttackAnimTimer = 0.0;
	float distance_covered = 0.0f;

	glm::vec3 _EnemyPos;
};

class BasicEnemyTicketDie : public State
{
public:
	BasicEnemyTicketDie(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;

	float DieAnimDur;
	float DieAnimTimer = 0.0;
};

class BasicEnemyTicketReset : public State
{
public:
	BasicEnemyTicketReset(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;
	glm::vec2 constantKnockback;
	float mKnockbackSpeed;
	float mResetTime = 0;
	Color real_color;
};

class BasicEnemyTicketStunned : public State
{
public:
	BasicEnemyTicketStunned(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyTicketMachine* castedMachine;
	float mShakeDir = 1;
};
