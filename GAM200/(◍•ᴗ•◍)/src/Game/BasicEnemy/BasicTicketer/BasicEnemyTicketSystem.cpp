#include "../../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui.h"

#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Graphics/Renderable/Renderable.h"
#include "../../../Game/ShotReceiver/ShotReceiver.h"
#include "../../../Animation/SM_Animation.h"
#include "../../../Collisions/ICollider.h"
#include "../../../RayCast/RayCastTest.h"
#include "../../../RayCast/RayCaster.h"
#include "../../../Factory/Factory.h"
#include "../../../Input/Input.h"
#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Time/Time.h"


#include "../../PlayerMovement/PlayerMovement.h"
#include "../../Purification/Purification.h"
#include "../../Hitbox/Hitbox.h"
#include "../../Health/Health.h"

#include "BasicEnemyTicketSystem.h"
#include "BasicEnemyTicketStates.h"
#include "BasicTicketer.h"

BasicEnemyTicketMachine::BasicEnemyTicketMachine()
{
	SetName("BasicEnemyTicketMachine");

	mInitialState = new BasicEnemyTicketIdle("Idle");
	mCurrentState = mInitialState;
	AddState(mInitialState);
	AddState(new BasicEnemyTicketGoToPlayer("GoToPlayer"));
	AddState(new BasicEnemyTicketAttack("Attack"));
	AddState(new BasicEnemyTicketDoPath("DoPath"));
	AddState(new BasicEnemyTicketDie("Die"));
	AddState(new BasicEnemyTicketReset("Reset"));
	AddState(new BasicEnemyTicketStunned("Stunned"));
	AddState(new PurificationState("PurificationState"));
}

BasicEnemyTicketMachine::~BasicEnemyTicketMachine() { }

void BasicEnemyTicketMachine::Initialize()
{
	//Get the actor trans and collider
	mOwnerTransform = mActor->GetComp<TransformComponent>();
	mOwnerCollider = mActor->GetComp<Collider>();

	//Get the the players trans and collider
	_Target = SceneSys->GetMainSpace()->FindObjectByName("Player");
	if (_Target)
	{
		_TargetTransform = _Target->GetComp<TransformComponent>();
		_TargetCollider = _Target->GetComp<Collider>();
	}

	//Get the skeleton renderable, and if it doesn't exit, get the simple renderable
	mSkelRen = mActor->GetComp<SkeletonRenderable>();
	if (mSkelRen == nullptr)
		mRenderable = mActor->GetComp<Renderable>();
	else
		mRenderable = mSkelRen;

	mShot = mActor->GetComp<ShotReceiver>();

	mHealth = mActor->GetComp<Health>();

	//Get the rays, if they exist
	RayCaster* temp_ray_caster = mActor->GetComp<RayCaster>();
	if (temp_ray_caster != nullptr)
	{
		mLeftRay = temp_ray_caster->GetRay("LeftRay");
		mRightRay = temp_ray_caster->GetRay("RightRay");
	}

	//Finally, set the mix values of the animations
	if (mSkelRen != nullptr)
	{
		mSkelRen->SetMixValue("Basic_Idle", "Basic_Attack", 0.2);
		mSkelRen->SetMixValue("Basic_Walk", "Basic_Idle", 0.5);
		mSkelRen->SetMixValue("Basic_Damage", "Basic_Idle", 0.5);
	}
	StateMachine::Initialize();
}

void BasicEnemyTicketMachine::Update()
{
	//Check if we have been shot
	if (mShot && mShot->AmIShot())
	{
		mShot->SetShot(false);
		ChangeState("Stunned");
	}

	//Update
	StateMachine::Update();
}

void BasicEnemyTicketMachine::Shutdown()
{
	StateMachine::Clear();
}

void BasicEnemyTicketMachine::OnCollisionStarted(GameObject* other)
{
	//Check if we get hit
	if (other->CompareTag("Hitbox") && mCurrentState != GetState("PurificationState") && mCurrentState != GetState("Die"))
	{
		if (Hitbox* hb = other->GetComp<Hitbox>())
		{
			if (!hb->mEnemy)
			{
				mHDamage = hb->mDamage;
				if (hb->mKnockback)
				{
					mHKTime = hb->mKTime;
					mHKSPeed = hb->mKSpeed;

					receivedHitbox = other;

					//check relative position player and enemy for knockback direction
					if (mOwnerTransform->GetWorldPosition().x > _TargetTransform->GetWorldPosition().x)
						KnockbackDirectionRight = true;
					else
						KnockbackDirectionRight = false;
				}
			}
		}
		ChangeState("Reset");
	}

	//Check if we just collided with a platform
	if (other->CompareTag("Platform"))
	{
		AddPlatform(other);
	}
	StateMachine::OnCollisionStarted(other);
}

BasicEnemyTicketMachine& BasicEnemyTicketMachine::operator=(const BasicEnemyTicketMachine& rhs)
{
	//Idle
	mSleepDetectionRange	= rhs.mSleepDetectionRange;

	//DoPath
	mMoveSpeed				= rhs.mMoveSpeed;

	//GoToPlayer
	mChaseSpeed				= rhs.mChaseSpeed;

	//Attack
	mBackwardsDistance		= rhs.mBackwardsDistance;
	mJumpAttackSpeed		= rhs.mJumpAttackSpeed;
	mBackAttackSpeed		= rhs.mBackAttackSpeed;
	mAttackDistance			= rhs.mAttackDistance;
	mAttackCooldown			= rhs.mAttackCooldown;
	mAttackRange			= rhs.mAttackRange;

	//Others
	mStunDuration			= rhs.mStunDuration;
	mFacingRight			= rhs.mFacingRight;
	purif_prob				= rhs.purif_prob;
	ArenaMode				= rhs.ArenaMode;

	return *this;
}

BasicEnemyTicketMachine* BasicEnemyTicketMachine::Clone()
{
	BasicEnemyTicketMachine* temp = FactorySys->Create<BasicEnemyTicketMachine>();
	*temp = *this;
	return temp;
}

bool BasicEnemyTicketMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		//Idle
		ImGui::DragFloat("mSleepDetectionRange", &mSleepDetectionRange);

		//DoPath
		ImGui::DragFloat("MoveSpeed", &mMoveSpeed);

		//GoToPlayer
		ImGui::DragFloat("ChaseSpeed", &mChaseSpeed);

		//Attack
		ImGui::DragFloat("mBackwardsDistance", &mBackwardsDistance);
		if (mBackwardsDistance < 0)
			mBackwardsDistance = 0;
		ImGui::DragFloat("JumpAttackSpeed", &mJumpAttackSpeed);
		ImGui::DragFloat("BackAttackSpeed", &mBackAttackSpeed);
		ImGui::DragFloat("mAttackDistance", &mAttackDistance);
		if (mAttackDistance < 0)
			mAttackDistance = 0;
		ImGui::DragFloat("mAttackCooldown", &mAttackCooldown);
		mAttackRange.x = abs(mAttackRange.x);
		mAttackRange.y = abs(mAttackRange.y);
		ImGui::DragFloat("mAttackRange.x", &mAttackRange.x);
		if (mAttackRange.x < 0)
			mAttackRange.x = 0;
		ImGui::DragFloat("mAttackRange.y", &mAttackRange.y);
		if (mAttackRange.y < 0)
			mAttackRange.y = 0;
		if (mFacingRight == false)
			mAttackRange.x = -mAttackRange.x;

		//Others
		ImGui::DragFloat("mStunDuration", &mStunDuration);
		mFacingRight = mActor->GetComp<Renderable>()->IsFlipX();
		if (ImGui::Checkbox("FacingRight", &mFacingRight))
			mActor->GetComp<Renderable>()->SetFlipX(!mActor->GetComp<Renderable>()->IsFlipX());
		ImGui::Checkbox("ArenaMode", &ArenaMode);
		ImGui::DragInt("Purification probability", &purif_prob);

		//Draw debug lines
		if (mActor)
		{
			Space* _Space = mActor->GetParentSpace();
			glm::vec3 _Pos{ mActor->GetComp<Collider>()->GetCollideBox().GetWorldPosition() };

			_Space->Drawrectangle(_Pos + glm::vec3(mAttackRange.x / 2, 0, 0), mAttackRange.x, mAttackRange.y, Colors::yellow);
			_Space->DrawCircle(_Pos, mSleepDetectionRange, Colors::red);
		}
		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	return false;
}

void BasicEnemyTicketMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	//Idle
	j["mSleepDetectionRange"] = mSleepDetectionRange;

	//DoPath
	j["mMoveSpeed"] = mMoveSpeed;

	//GoToPlayer
	j["mChaseSpeed"] = mChaseSpeed;

	//Attack
	j["mBackwardsDistance"] = mBackwardsDistance;
	j["mJumpAttackSpeed"] = mJumpAttackSpeed;
	j["mBackAttackSpeed"] = mBackAttackSpeed;
	j["mAttackDistance"] = mAttackDistance;
	j["mAttackCooldown"] = mAttackCooldown;
	j["mAttackRange.x"] = mAttackRange.x;
	j["mAttackRange.y"] = mAttackRange.y;

	//Others
	j["mStunDuration"] = mStunDuration;
	j["mFacingRight"] = mFacingRight;
	j["ArenaMode"] = ArenaMode;
	j["purif_prob"] = purif_prob;
}

void BasicEnemyTicketMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	//Idle
	if (j.find("mSleepDetectionRange") != j.end())
		mSleepDetectionRange = j["mSleepDetectionRange"];

	//DoPath
	if (j.find("mMoveSpeed") != j.end())
		mMoveSpeed = j["mMoveSpeed"];

	//GoToPlayer
	if (j.find("mChaseSpeed") != j.end())
		mChaseSpeed = j["mChaseSpeed"];

	//Attack
	if (j.find("mBackwardsDistance") != j.end())
		mBackwardsDistance = j["mBackwardsDistance"];
	if (j.find("mJumpAttackSpeed") != j.end())
		mJumpAttackSpeed = j["mJumpAttackSpeed"];
	if (j.find("mBackAttackSpeed") != j.end())
		mBackAttackSpeed = j["mBackAttackSpeed"];
	if (j.find("mAttackDistance") != j.end())
		mAttackDistance = j["mAttackDistance"];
	if (j.find("mAttackCooldown") != j.end())
		mAttackCooldown = j["mAttackCooldown"];
	if (j.find("mAttackRange.x") != j.end())
		mAttackRange.x = j["mAttackRange.x"];
	if (j.find("mAttackRange.y") != j.end())
		mAttackRange.y = j["mAttackRange.y"];

	//Others
	if (j.find("mFacingRight") != j.end())
		mFacingRight = j["mFacingRight"];
	if (j.find("ArenaMode") != j.end())
		ArenaMode = j["ArenaMode"];
	if (j.find("mStunDuration") != j.end())
		mStunDuration = j["mStunDuration"];
	if (j.find("purif_prob") != j.end())
		purif_prob = j["purif_prob"];

}

void BasicEnemyTicketMachine::AddPlatform(GameObject* _Platform)
{
	if (std::find(mPlatforms.begin(), mPlatforms.end(), _Platform) == mPlatforms.end())
		mPlatforms.push_back(_Platform);
}

bool BasicEnemyTicketMachine::CheckIfSolapation()
{
	if (mTicketer)
	{
		auto enemies = mTicketer->GetEnemies();
		int size_x = mOwnerCollider->GetCollideBox().GetWorldScale().x;

		for (auto enemy : *enemies)
		{
			if (enemy != this)
			{
				float direction = enemy->mOwnerTransform->GetWorldPosition().x - mOwnerTransform->GetWorldPosition().x;
				if (abs(direction) < size_x && ((direction > 0 && mFacingRight) || (direction < 0 && !mFacingRight)))
					return true;
			}
		}
	}
	return false;
}

bool BasicEnemyTicketMachine::CheckOutOfBounds()
{
	float _LeftDistance = 0.0f;
	float _RightDistance = 0.0f;
	GameObject* _AnObject = nullptr;
	_LeftDistance = CheckRayWithVector(mLeftRay, &_AnObject, nullptr, mPlatforms);
	_RightDistance = CheckRayWithVector(mRightRay, &_AnObject, nullptr, mPlatforms);

	// The ray did not collide with any platform
	if (_LeftDistance == -1.0f && _RightDistance == -1.0f)
		return false;
	else if (_LeftDistance == -1.0f || _RightDistance == -1.0f)
		return true;
	return false;
}

void BasicEnemyTicketMachine::Flip()
{
	mFacingRight = !mFacingRight;
	if (mRenderable)
		mRenderable->SetFlipX(mFacingRight);
	mAttackRange.x = -mAttackRange.x;
}
