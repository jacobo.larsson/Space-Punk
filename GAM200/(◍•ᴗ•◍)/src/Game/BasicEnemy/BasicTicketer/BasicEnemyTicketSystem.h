#pragma once

#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../RTTI/SM_RTTI.h"

class TransformComponent;
class SkeletonRenderable;
class BasicTicketer;
class ShotReceiver;
class Renderable;
class Collider;
class Health;
class Ray;

class BasicEnemyTicketMachine : public StateMachine
{
	SM_RTTI_DECL(BasicEnemyTicketMachine, StateMachine);

public:

	BasicEnemyTicketMachine();
	~BasicEnemyTicketMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void OnCollisionStarted(GameObject* other) override;

	BasicEnemyTicketMachine& operator=(const BasicEnemyTicketMachine& rhs);
	BasicEnemyTicketMachine* Clone()override;

	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void AddPlatform(GameObject* _Platform);
	bool CheckIfSolapation();
	bool CheckOutOfBounds();
	void Flip();

	//External Pointers
	TransformComponent* _TargetTransform = nullptr;
	TransformComponent* mOwnerTransform = nullptr;
	SkeletonRenderable* mSkelRen = nullptr;
	std::vector<GameObject*> mPlatforms;
	Collider* _TargetCollider = nullptr;
	Collider* mOwnerCollider = nullptr;
	BasicTicketer* mTicketer = nullptr;
	Renderable* mRenderable = nullptr;
	GameObject* _Target = nullptr;
	ShotReceiver* mShot = nullptr;
	Health* mHealth = nullptr;
	Ray* mRightRay = nullptr;
	Ray* mLeftRay = nullptr;


	//Idle
	float mSleepDetectionRange = 0.0f;

	//DoPath
	glm::vec3 mOriginalPosition;
	float mMoveSpeed = 0.0f;

	//GoToPlayer
	float mChaseSpeed = 0.0f;

	//Attack
	float mBackwardsDistance = 0.0f;
	float mJumpAttackSpeed = 0.0f;
	float mBackAttackSpeed = 0.0f;
	float mAttackDistance = 0.0f;
	float mAttackCooldown = 0.0f;
	glm::vec2 mAttackRange;
	bool mTicket = false;

	//Reset
	bool KnockbackDirectionRight;
	GameObject* receivedHitbox;
	float mHDamage;
	float mHKSPeed;
	float mHKTime;

	//Others
	float mStunDuration = 0.0f;
	bool mFacingRight = false;
	float mDirection = 1.0f;
	bool ArenaMode = false;
	int purif_prob = 50;
	bool Shot = false;	
};