#include "../../../Input/Input.h" 
#include "../../../Time/Time.h" 
				
#include "../../../Space/Space.h"
#include "../../../Scene/Scene.h"
#include "../../../Time/Time.h"

#include "../../../Graphics/Renderable/Renderable.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Collisions/ICollider.h"
#include "../../../Collisions/Collisions.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Physics/PhysicSystem.h"
#include "../../../Collisions/ContactCollisions.h"

#include "../../../Animation/SM_Animation.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"

#include "../../Health/Health.h"
#include "../../PlayerMovement/PlayerMovement.h"
#include "../../Hitbox/Hitbox.h"



#include "BasicTicketer.h"
#include "BasicEnemyTicketSystem.h"
#include "BasicEnemyTicketStates.h"


#pragma region BasicEnemyTicketIdle

BasicEnemyTicketIdle::BasicEnemyTicketIdle(const char* name) : State(name)
{}
void BasicEnemyTicketIdle::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyTicketIdle::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Idle")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
	}
	mTimeInState = 0.0f;
}
void BasicEnemyTicketIdle::LogicUpdate()
{
	//Draw detection range
	if (SettingsSys->debug_draw)
	{
		glm::vec3 _Pos{ castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
		mActor->GetParentSpace()->DrawCircle(_Pos, castedMachine->mSleepDetectionRange, Colors::red);
	}

	glm::vec2 _TargetPos = { castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
	glm::vec2 _EnemyPos = { castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
	glm::vec2 Direction = glm::normalize(_TargetPos - _EnemyPos);
	if (glm::distance(_TargetPos, _EnemyPos) < castedMachine->mSleepDetectionRange)
		castedMachine->mTicketer->Activate(castedMachine);
}
void BasicEnemyTicketIdle::LogicExit()
{

}

#pragma endregion

#pragma region BasicEnemyTicketDoPath

BasicEnemyTicketDoPath::BasicEnemyTicketDoPath(const char* name) : State(name)
	{}
void BasicEnemyTicketDoPath::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyTicketDoPath::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Walk")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Walk", true);
	}

	float temp_dir = castedMachine->mOriginalPosition.x - castedMachine->mOwnerTransform->GetWorldPosition().x;

	if (temp_dir < 0 && castedMachine->mFacingRight)
	{
		castedMachine->mDirection = -1.0f;
		castedMachine->Flip();
	}
	else if (temp_dir > 0 && !castedMachine->mFacingRight)
	{
		castedMachine->mDirection = 1.0f;
		castedMachine->Flip();
	}

	if(castedMachine->mRenderable)
		castedMachine->mRenderable->SetFlipX(castedMachine->mFacingRight);
}
void BasicEnemyTicketDoPath::LogicUpdate()
{
	glm::vec3 _NewPos{ castedMachine->mOwnerTransform->GetWorldPosition() };
	_NewPos.x += castedMachine->mMoveSpeed * TimeSys->GetFrameTime() * castedMachine->mDirection;
	castedMachine->mOwnerTransform->SetWorldPosition(_NewPos);

	if (abs(castedMachine->mOwnerTransform->GetWorldPosition().x - castedMachine->mOriginalPosition.x) < 50)
		castedMachine->ChangeState("Idle");
}
void BasicEnemyTicketDoPath::LogicExit()
{
}


#pragma endregion

#pragma region BasicEnemyTicketGoToPlayer

BasicEnemyTicketGoToPlayer::BasicEnemyTicketGoToPlayer(const char* name) : State(name)
{}
void BasicEnemyTicketGoToPlayer::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyTicketGoToPlayer::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Walk")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Walk", true);
	}
}
void BasicEnemyTicketGoToPlayer::LogicUpdate()
{

	//Draw detection range
	glm::vec3 _Pos{ castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
	if (SettingsSys->debug_draw)
		mActor->GetParentSpace()->Drawrectangle(_Pos + glm::vec3(castedMachine->mAttackRange.x / 2, 0, 0), castedMachine->mAttackRange.x, castedMachine->mAttackRange.y, Colors::yellow);

	glm::vec3 Target_Pos{ castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
	glm::vec3 Target_Scale{ castedMachine->_TargetCollider->GetCollideBox().GetWorldScale() };
	glm::vec2 attack_range{ castedMachine->mAttackRange };
	//See if we are in range to attack
	bool can_attack = StaticRectToStaticRect(_Pos + glm::vec3(attack_range.x / 2, 0, 0), abs(attack_range.x), attack_range.y, Target_Pos, Target_Scale.x, Target_Scale.y);


	glm::vec2 _TargetPos{ Target_Pos };
	glm::vec2 _EnemyPos = { castedMachine->mOwnerTransform->GetWorldPosition() };
	glm::vec2 Direction = glm::normalize(_TargetPos - _EnemyPos);
	//Turn the enemy
	if (Direction.x < 0 && castedMachine->mFacingRight == true ||
		Direction.x > 0 && castedMachine->mFacingRight == false)
		castedMachine->Flip();


	//Follow until I am in range to attack
	if (!can_attack)
	{
		if (!castedMachine->CheckIfSolapation())
		{
			_EnemyPos.x += castedMachine->mChaseSpeed * TimeSys->GetFrameTime() * Direction.x;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
		}
	}
	//If I have a ticket to attack
	else if (castedMachine->mTicketer->mEnemyAttacking == false)
	{
		castedMachine->mTicketer->SetNewAttacker(castedMachine);
		mOwnerStateMachine->ChangeState("Attack");
	}
	else if (castedMachine->mTicket)
		mOwnerStateMachine->ChangeState("Attack");

	//Check if we got out the last frame
	if (castedMachine->CheckOutOfBounds())
	{
		_EnemyPos.x -= castedMachine->mChaseSpeed * TimeSys->GetFrameTime() * Direction.x;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
	}
}
void BasicEnemyTicketGoToPlayer::LogicExit()
{
}

#pragma endregion

#pragma region BasicEnemyTicketAttack

BasicEnemyTicketAttack::BasicEnemyTicketAttack(const char* name) : State(name)
{}

void BasicEnemyTicketAttack::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
	if(castedMachine->mSkelRen)
		AttackAnimDur = castedMachine->mSkelRen->GetAnimDuration();
	State::Initialize();
}

void BasicEnemyTicketAttack::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Idle")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
	}

	Attacking = false;
	GoingBackwards = true;
	AttackaAnim = false;

	AttackAnimTimer = 0.0;
	distance_covered = 0.0f;
	mTimeInState = 0.0f;
	if (castedMachine->mFacingRight)
		direction = -1.0f;
	else
		direction = 1.0f;
}

void BasicEnemyTicketAttack::LogicUpdate()
{
	_EnemyPos = castedMachine->mOwnerTransform->GetWorldPosition();
	if (GoingBackwards)
	{
		_EnemyPos.x += castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime() * direction;
		distance_covered += castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));

		//Check if we got out the last frame
		if (castedMachine->CheckOutOfBounds() || distance_covered >= castedMachine->mBackwardsDistance)
		{
			_EnemyPos.x -= castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime() * direction;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
			GoingBackwards = false;
			Attacking = true;
			distance_covered = 0.0f;
		}
	}
	else if (Attacking)
	{
		AttackAnimTimer += (float)TimeSys->GetFrameTime();
		if (AttackaAnim == false)
		{
			if (castedMachine->mSkelRen)
				castedMachine->mSkelRen->SetCurrentAnimation("Basic_Attack", true);
			AttackaAnim = true;
		}
		_EnemyPos.x += castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime() * -direction;
		distance_covered += castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));

		//Check if we got out the last frame
		if (castedMachine->CheckOutOfBounds() || distance_covered >= castedMachine->mAttackDistance + castedMachine->mBackwardsDistance)
		{
			_EnemyPos.x -= castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime() * -direction;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
			if (AttackAnimTimer >= AttackAnimDur)
			{
				Attacking = false;
				AttackaAnim = false;
				if (castedMachine->mSkelRen)
					castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
				distance_covered = 0.0f;
				AttackAnimTimer = 0.0f;
				mTimeInState = 0;
			}
		}
	}
	else if (mTimeInState >= castedMachine->mAttackCooldown)
	{
		GoingBackwards = true;
		castedMachine->ChangeState("GoToPlayer");
	}
	else
	{
		if (castedMachine->mTicket)
			castedMachine->mTicketer->RemoveActualAttacker(false);
	}
}

void BasicEnemyTicketAttack::LogicExit()
{
}
#pragma endregion

#pragma region BasicEnemyTicketDie

BasicEnemyTicketDie::BasicEnemyTicketDie(const char* name) : State(name)
{}

void BasicEnemyTicketDie::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
}

void BasicEnemyTicketDie::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Death")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Death");
	}

	if (castedMachine->mSkelRen)
		DieAnimDur = castedMachine->mSkelRen->GetAnimDuration();
	mTimeInState = 0;
}

void BasicEnemyTicketDie::LogicUpdate()
{
	DieAnimTimer += (float)TimeSys->GetFrameTime();
	if (DieAnimTimer >= DieAnimDur)
	{
		mActor->GetParentSpace()->DestroyObject(mActor);
		if (castedMachine->mTicket)
			castedMachine->mTicketer->RemoveActualAttacker(true);
	}
}

void BasicEnemyTicketDie::LogicExit()
{
}

#pragma endregion

#pragma region BasicEnemyTicketReset

BasicEnemyTicketReset::BasicEnemyTicketReset(const char* name) : State(name)
{
}

void BasicEnemyTicketReset::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
}

void BasicEnemyTicketReset::LogicEnter()
{
	if (castedMachine->mRenderable)
	{
		real_color = castedMachine->mRenderable->GetModulationColor();
		castedMachine->mRenderable->SetModulationColor(Colors::red);
	}
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Damage")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Damage");
	}

	mTimeInState = 0;
	if (castedMachine->mHealth)
	{
		castedMachine->mHealth->Damage(castedMachine->mHDamage);

		if (float hlth = castedMachine->mHealth->GetHealth() <= 0.0f)
		{
			mActor->RemoveComp(mActor->GetComp<Hitbox>());
			if ((rand() % 101) > (100 - castedMachine->purif_prob))
				castedMachine->ChangeState("PurificationState");
			else
				castedMachine->ChangeState("Die");
		}
	}

	
	bool pFacingRight = reinterpret_cast<MovementMachine*>(castedMachine->_Target->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"))->FacingRight;
	if (castedMachine->receivedHitbox) 
	{
		float pHitboxWidth = castedMachine->receivedHitbox->GetComp<TransformComponent>()->GetWorldScale().x;
		//Knockback to the end of the hitbox

		float pTransform = castedMachine->_TargetTransform->GetWorldPosition().x;
		float FinalPosX = pTransform + (pFacingRight ? pHitboxWidth + 100 : -pHitboxWidth - 100);

		float mPos = castedMachine->mOwnerTransform->GetWorldPosition().x;
		float DistanceToTravel = FinalPosX - mPos ;
		//distance that has to travel over time it has to travel it
		mKnockbackSpeed = (DistanceToTravel) / castedMachine->mHKTime;
	}
}

void BasicEnemyTicketReset::LogicUpdate()
{
	if (mTimeInState < castedMachine->mHKTime || mTimeInState < mResetTime)
	{
		glm::vec3 pos = castedMachine->mOwnerTransform->GetWorldPosition();
		pos.x += mKnockbackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(pos);
	}
	else if (float hlth = castedMachine->mHealth->GetHealth() >0.0f)
	{
		if (!castedMachine->mTicket)
		{
			castedMachine->mTicketer->RemoveActualAttacker(false);
			castedMachine->mTicketer->SetNewAttacker(castedMachine);
		}
		castedMachine->ChangeState("GoToPlayer");
	}
}

void BasicEnemyTicketReset::LogicExit()
{
	if (castedMachine->mRenderable)
		castedMachine->mRenderable->SetModulationColor(real_color);
}

#pragma endregion

#pragma region BasicEnemyTicketStunned

BasicEnemyTicketStunned::BasicEnemyTicketStunned(const char* name) : State(name)
{
}

void BasicEnemyTicketStunned::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyTicketMachine*>(mOwnerStateMachine);
}

void BasicEnemyTicketStunned::LogicEnter()
{
	mTimeInState = 0;
	mShakeDir = 1;
}

void BasicEnemyTicketStunned::LogicUpdate()
{
	if (mTimeInState >= castedMachine->mStunDuration)
		castedMachine->ChangeState("GoToPlayer");
	glm::vec3 temp_pos = castedMachine->mOwnerTransform->GetWorldPosition();
	temp_pos.x += 100 * TimeSys->GetFrameTime() * mShakeDir;
	castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));
	mShakeDir = -mShakeDir;
}

void BasicEnemyTicketStunned::LogicExit()
{
}

#pragma endregion