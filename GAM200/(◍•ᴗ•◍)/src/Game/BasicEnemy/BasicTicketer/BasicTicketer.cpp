#include "../../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui.h"


#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Archetype/ArchetypeFunctions.h"
#include "../../../GameObject/GameObject.h"
#include "../../../Collisions/ICollider.h"
#include "../../../Graphics/Color/Color.h"
#include "../../../Factory/Factory.h"
#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"

#include "BasicEnemyTicketSystem.h"
#include "BasicTicketer.h"


BasicTicketer::BasicTicketer()
{
	mName = "BasicTicketer";
}
BasicTicketer::~BasicTicketer()
{

}
BasicTicketer& BasicTicketer::operator=(const BasicTicketer& rhs)
{
	mPositions			= rhs.mPositions;
	mNumberOfEnemies	= rhs.mNumberOfEnemies;
	mScapeRange			= rhs.mScapeRange;

	return *this;
}
BasicTicketer* BasicTicketer::Clone()
{
	BasicTicketer* temp = FactorySys->Create<BasicTicketer>();
	(*temp) = (*this);
	return temp;
}

void BasicTicketer::Initialize()
{
	LogicComponent::Initialize();
	GameObject* temp_enemy = nullptr;

	for (auto pos : mPositions)
	{
		temp_enemy = CreateFromArchetype("BasicEnemyTicket", mOwner->GetParentSpace(), glm::vec3(pos, 0));

		BrainComponent* brain = temp_enemy->GetComp<BrainComponent>();
		if (brain)
		{
			StateMachine* machine = brain->GetStateMachine("BasicEnemyTicketMachine");
			if (machine)
			{
				BasicEnemyTicketMachine* final_machine = reinterpret_cast<BasicEnemyTicketMachine*>(machine);
				mEnemies.push_back(reinterpret_cast<BasicEnemyTicketMachine*>(machine));
				final_machine->mTicketer = this;
				final_machine->mTicket = false;
				final_machine->mOriginalPosition = glm::vec3(pos, 0);
			}
		}
	}

	_TargetCollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>();
	mOwnerTransform = mOwner->GetComp<TransformComponent>();

}
void BasicTicketer::Update()
{
	if (mActive)
	{
		if (SettingsSys->debug_draw)
		{
			//Draw original positions
			for (int i = 0; i < mNumberOfEnemies; i++)
				mOwner->GetParentSpace()->Drawrectangle(mPositions[i], 50.0f, 50.0f, Colors::orange, 0.0f);

			//Draw scape range
			glm::vec3 pt1(mOwnerTransform->GetWorldPosition().x - 200, mOwnerTransform->GetWorldPosition().y + mScapeRange, 0);
			glm::vec3 pt2(pt1.x + 400, pt1.y, 0);
			mOwner->GetParentSpace()->DrawLine(pt1, pt2, Colors::yellow);
			pt1.y = mOwnerTransform->GetWorldPosition().y - mScapeRange;
			pt2.y = pt1.y;
			mOwner->GetParentSpace()->DrawLine(pt1, pt2, Colors::yellow);
		}

		if (_TargetCollider && abs(_TargetCollider->GetCollideBox().GetWorldPosition().y - mOwnerTransform->GetWorldPosition().y) > mScapeRange)
			Deactivate();
	}
}
void BasicTicketer::Shutdown()
{

	LogicComponent::Shutdown();
}

void BasicTicketer::ToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);

	json& positionsJson = j["positions"];
	for (auto& pos : mPositions)
	{
		json posJson;
		posJson["X"] = pos.x;
		posJson["Y"] = pos.y;
		positionsJson.push_back(posJson);
	}

	j["mNumberOfEnemies"] = mNumberOfEnemies;
	j["mScapeRange"] = mScapeRange;
}
void BasicTicketer::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	auto positionsJson = j.find("positions");
	if (positionsJson != j.end())
	{
		int i = 0;
		for (const auto& posJson : *positionsJson)
		{
			mPositions.push_back(glm::vec2());
			mPositions[i].x = posJson["X"];
			mPositions[i].y = posJson["Y"];
			i++;
		}
	}
	if (j.find("mNumberOfEnemies") != j.end())
		mNumberOfEnemies = j["mNumberOfEnemies"];
	if (j.find("mScapeRange") != j.end())
		mScapeRange = j["mScapeRange"];

}

bool BasicTicketer::Edit()
{
	bool changed = false;
	int prev_count = mNumberOfEnemies;

	ImGui::DragFloat("Scape Range", &mScapeRange);
	ImGui::DragInt("Number Of Enemies", &mNumberOfEnemies, 1, 0, 10);
	if (ImGui::IsItemDeactivatedAfterEdit())
	{
		//Clamp the count
		if (mNumberOfEnemies > 10)
			mNumberOfEnemies = 10;
		else if(mNumberOfEnemies < 0)
			mNumberOfEnemies = 0;

		
		if (mNumberOfEnemies < mPositions.size())
		{
			while (mNumberOfEnemies != mPositions.size())
				mPositions.pop_back();
		}
		else if (mNumberOfEnemies > mPositions.size())
		{
			while (mNumberOfEnemies != mPositions.size())
				mPositions.push_back(mOwner->GetComp<TransformComponent>()->GetWorldPosition());
		}
		changed = true;
	}


	
	if (ImGui::TreeNode("My Enemies:"))
	{
		//mPositions.resize(mNumberOfEnemies);
		for (int i = 0; i < mPositions.size(); i++)
		{
			if (ImGui::CollapsingHeader(std::to_string(i+1).c_str()))
			{
				ImGui::PushID(i);
				ImGui::DragFloat("X:", &mPositions[i].x);
				ImGui::DragFloat("Y:", &mPositions[i].y);
				ImGui::PopID();
			}
			mOwner->GetParentSpace()->Drawrectangle(mPositions[i], 50.0f, 50.0f, Colors::orange, 0.0f);
		}
		ImGui::TreePop();
	}

	glm::vec3 pt1(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x - 200, mOwner->GetComp<TransformComponent>()->GetWorldPosition().y + mScapeRange, 0);
	glm::vec3 pt2(pt1.x + 400, pt1.y, 0);
	mOwner->GetParentSpace()->DrawLine(pt1, pt2, Colors::yellow);
	pt1.y = mOwner->GetComp<TransformComponent>()->GetWorldPosition().y - mScapeRange;
	pt2.y = pt1.y;
	mOwner->GetParentSpace()->DrawLine(pt1, pt2, Colors::yellow);
	return changed;
}

void BasicTicketer::Activate(BasicEnemyTicketMachine* first_one)
{
	if (mActive == false)
	{
		mActive = true;
		mActiveBasic = first_one;
		mEnemyAttacking = true;
		mActiveBasic->mTicket = true;

		for (auto basic : mEnemies)
			basic->ChangeState("GoToPlayer");
	}
}

void BasicTicketer::Deactivate()
{
	if (mActive == true)
	{
		mActive = false;
		mActiveBasic = nullptr;
		mEnemyAttacking = false;

		for (auto basic : mEnemies)
			basic->ChangeState("DoPath");
	}
}

void BasicTicketer::SetNewAttacker(BasicEnemyTicketMachine* enemy)
{
	if (!mEnemyAttacking)
	{
		mActiveBasic = enemy;
		mEnemyAttacking = true;
		mActiveBasic->mTicket = true;
	}
}

void BasicTicketer::RemoveActualAttacker(bool is_dead)
{
	if (is_dead)
		mEnemies.erase(std::find(mEnemies.begin(), mEnemies.end(), mActiveBasic));
	if(mActiveBasic)
		mActiveBasic->mTicket = false;
	mEnemyAttacking = false;
	mActiveBasic = nullptr;
}