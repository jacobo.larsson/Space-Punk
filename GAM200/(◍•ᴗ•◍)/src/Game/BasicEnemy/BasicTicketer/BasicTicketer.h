#pragma once
#include <vector>
#include <queue>

#include "../../../LogicSystem/LogicComponent.h"
#include "../../../Extern/json/json.hpp"
#include "../../../RTTI/SM_RTTI.h"

class BasicEnemyTicketMachine;
class StateMachine;
class GameObject;

class BasicTicketer : public LogicComponent
{
	SM_RTTI_DECL(BasicTicketer, LogicComponent);

public:
	BasicTicketer();
	~BasicTicketer();
	BasicTicketer& operator=(const BasicTicketer& rhs);
	BasicTicketer* Clone() override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	std::vector<BasicEnemyTicketMachine*>* GetEnemies() { return &mEnemies; }

	void Activate(BasicEnemyTicketMachine* first_one);
	void Deactivate();
	void SetNewAttacker(BasicEnemyTicketMachine* enemy);
	void RemoveActualAttacker(bool is_dead);
	bool Edit() override;

	bool mEnemyAttacking = false;


private:

	//External Pointers
	TransformComponent* mOwnerTransform = nullptr;
	Collider* _TargetCollider = nullptr;

	//Editor
	std::vector<glm::vec2> mPositions;
	int mNumberOfEnemies = 0;
	float mScapeRange = 0.0f;

	//In-game
	BasicEnemyTicketMachine* mActiveBasic = nullptr;
	std::vector<BasicEnemyTicketMachine*> mEnemies;
	bool mActive = false;
};