#include "../../Input/Input.h" 
#include "../../Time/Time.h" 

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"

#include "../../Graphics/Renderable/Renderable.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"
#include "../../Collisions/Collisions.h"
#include "../Health/Health.h"
#include "../../Animation/SM_Animation.h"

#include "BasicEnemySystem.h"
#include "BasicEnemyStates.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../Hitbox/Hitbox.h"

#pragma region BasicEnemyIdle

BasicEnemyIdle::BasicEnemyIdle(const char* name) : State(name)
{}
void BasicEnemyIdle::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyIdle::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Idle")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
	}
	mTimeInState = 0.0f;
}
void BasicEnemyIdle::LogicUpdate()
{
	if (mTimeInState >= 1.5)
	{
		glm::vec3 _Pos{ castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
		glm::vec2 attack_range{ castedMachine->mAttackRange };
		glm::vec2 view_range{ castedMachine->mViewRange };
		glm::vec3 Target_Pos{ castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
		glm::vec3 Target_Scale{ castedMachine->_TargetCollider->GetCollideBox().GetWorldScale() };
		bool CanAttack = StaticRectToStaticRect(_Pos + glm::vec3(attack_range.x / 2, 0, 0), abs(attack_range.x), attack_range.y, Target_Pos, Target_Scale.x, Target_Scale.y);
		bool CanFollow = StaticRectToStaticRect(_Pos + glm::vec3(view_range.x / 2, 0, 0), abs(view_range.x), view_range.y, Target_Pos, Target_Scale.x, Target_Scale.y);

		if (CanAttack)
			mOwnerStateMachine->ChangeState("Attack");
		else if (!CanAttack && CanFollow)
			mOwnerStateMachine->ChangeState("GoToPlayer");
		else if (!CanAttack && !CanFollow)
		{
			if(rand()%2 == 0)
				castedMachine->Flip();
			castedMachine->ChangeState("DoPath");
		}
	}
}
void BasicEnemyIdle::LogicExit()
{

}

#pragma endregion

#pragma region BasicEnemyDoPath

BasicEnemyDoPath::BasicEnemyDoPath(const char* name) : State(name)
	{}
void BasicEnemyDoPath::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyDoPath::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Walk")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Walk", true);
	}

	getting_out_edge = false;
	mLeftMargin = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
	mLeftMargin.x -= castedMachine->mLeftRange;
	mRightMargin = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
	mRightMargin.x += castedMachine->mRightRange;
	if (castedMachine->mFacingRight == false)
		castedMachine->mDirection = -1.0f;
	else
		castedMachine->mDirection = 1.0f;

	if (castedMachine->mRenderable)
		castedMachine->mRenderable->SetFlipX(castedMachine->mFacingRight);
}
void BasicEnemyDoPath::LogicUpdate()
{
	// Movement
	if (castedMachine->ArenaMode == false)
	{

		if (castedMachine->mMoveInRange)
		{
			if (castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition().x <= mLeftMargin.x ||
				castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition().x >= mRightMargin.x)
			{
				castedMachine->mDirection *= -1.0f;
				castedMachine->Flip();
			}
		}

		if (castedMachine->CheckOutOfBounds() && getting_out_edge == false)
		{
			getting_out_edge = true;
			castedMachine->mDirection *= -1.0f;
			castedMachine->Flip();

		}
		else if (getting_out_edge == true && !castedMachine->CheckOutOfBounds())
			getting_out_edge = false;

		glm::vec3 _NewPos{ castedMachine->mOwnerTransform->GetWorldPosition() };
		_NewPos.x += castedMachine->mMoveSpeed * TimeSys->GetFrameTime() * castedMachine->mDirection;
		castedMachine->mOwnerTransform->SetWorldPosition(_NewPos);
	}


	if (castedMachine->_Target)
	{
		glm::vec3 _Pos{ mActor->GetComp<Collider>()->GetCollideBox().GetWorldPosition() };
		glm::vec2 view_range{ castedMachine->mViewRange };
		glm::vec3 Target_Pos{ castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
		glm::vec3 Target_Scale{ castedMachine->_TargetCollider->GetCollideBox().GetWorldScale() };

		if (StaticRectToStaticRect(_Pos + glm::vec3(view_range.x / 2, 0, 0), abs(view_range.x), view_range.y, Target_Pos, Target_Scale.x, Target_Scale.y))
			castedMachine->ChangeState("GoToPlayer");
	}
}
void BasicEnemyDoPath::LogicExit()
{

}
void BasicEnemyDoPath::OnCollisionStarted(GameObject* _Other)
{
	if (_Other->CompareTag("Wall"))
	{
		castedMachine->mDirection = -(castedMachine->mDirection);
		castedMachine->Flip();
	}
}


#pragma endregion

#pragma region BasicEnemyGoToPlayer

BasicEnemyGoToPlayer::BasicEnemyGoToPlayer(const char* name) : State(name)
{}
void BasicEnemyGoToPlayer::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}
void BasicEnemyGoToPlayer::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Walk")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Walk", true);
	}
}
void BasicEnemyGoToPlayer::LogicUpdate()
{
	glm::vec2 _TargetPos = { castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
	glm::vec2 _EnemyPos = { castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
	glm::vec2 Direction = glm::normalize(_TargetPos - _EnemyPos);

	if(glm::distance(_TargetPos, _EnemyPos) > castedMachine->mFollowingRange)
		mOwnerStateMachine->ChangeState("Idle");
	else
	{
		//Turn the enemy
		if (Direction.x < 0 && castedMachine->mFacingRight == true ||
			Direction.x > 0 && castedMachine->mFacingRight == false)
			castedMachine->Flip();

		_EnemyPos.x += castedMachine->mChaseSpeed * TimeSys->GetFrameTime() * Direction.x;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));

		//Check if we got out the last frame
		if (castedMachine->CheckOutOfBounds())
		{
			_EnemyPos.x -= castedMachine->mChaseSpeed * TimeSys->GetFrameTime() * Direction.x;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
		}

		glm::vec3 _Pos{ castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition() };
		glm::vec2 attack_range{ castedMachine->mAttackRange };
		glm::vec3 Target_Pos{ castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition() };
		glm::vec3 Target_Scale{ castedMachine->_TargetCollider->GetCollideBox().GetWorldScale() };
		if (StaticRectToStaticRect(_Pos + glm::vec3(attack_range.x / 2, 0, 0), abs(attack_range.x), attack_range.y, Target_Pos, Target_Scale.x, Target_Scale.y))
			mOwnerStateMachine->ChangeState("Attack");
	}
}
void BasicEnemyGoToPlayer::LogicExit()
	{
	}
void BasicEnemyGoToPlayer::OnCollisionStarted(GameObject* _Other)
{
	if (_Other->CompareTag("Wall"))
	{
		castedMachine->Flip();
		castedMachine->ChangeState("DoPath");
	}
}

#pragma endregion

#pragma region BasicEnemyAttack

BasicEnemyAttack::BasicEnemyAttack(const char* name) : State(name)
{}

void BasicEnemyAttack::Initialize()
{
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
	if(castedMachine->mSkelRen)
		AttackAnimDur = castedMachine->mSkelRen->GetAnimDuration();
	State::Initialize();
}

void BasicEnemyAttack::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Idle")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
	}

	Attacking = false;
	GoingBackwards = true;
	AttackaAnim = false;

	AttackAnimTimer = 0.0;
	distance_covered = 0.0f;
	mTimeInState = 0.0f;
	if (castedMachine->mFacingRight)
		direction = -1.0f;
	else
		direction = 1.0f;
}

void BasicEnemyAttack::LogicUpdate()
{
	_EnemyPos = castedMachine->mOwnerTransform->GetWorldPosition();
	if (GoingBackwards)
	{
		_EnemyPos.x += castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime() * direction;
		distance_covered += castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));

		//Check if we got out the last frame
		if (castedMachine->CheckOutOfBounds() || distance_covered >= castedMachine->mBackwardsDistance)
		{
			_EnemyPos.x -= castedMachine->mBackAttackSpeed * TimeSys->GetFrameTime() * direction;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
			GoingBackwards = false;
			Attacking = true;
			distance_covered = 0.0f;
		}
	}
	else if (Attacking)
	{
		AttackAnimTimer += (float)TimeSys->GetFrameTime();
		if (AttackaAnim == false)
		{
			if (castedMachine->mSkelRen)
				castedMachine->mSkelRen->SetCurrentAnimation("Basic_Attack", true);
			AttackaAnim = true;
		}
		_EnemyPos.x += castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime() * -direction;
		distance_covered += castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));

		//Check if we got out the last frame
		if (castedMachine->CheckOutOfBounds() || distance_covered >= castedMachine->mAttackDistance + castedMachine->mBackwardsDistance)
		{
			_EnemyPos.x -= castedMachine->mJumpAttackSpeed * TimeSys->GetFrameTime() * -direction;
			castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(_EnemyPos.x, _EnemyPos.y, 0));
			if (AttackAnimTimer >= AttackAnimDur)
			{
				Attacking = false;
				AttackaAnim = false;
				if (castedMachine->mSkelRen)
					castedMachine->mSkelRen->SetCurrentAnimation("Basic_Idle", true);
				distance_covered = 0.0f;
				AttackAnimTimer = 0.0f;
				mTimeInState = 0;
			}
		}
	}
	else if (mTimeInState >= castedMachine->mAttackCooldown)
	{
		GoingBackwards = true;

		float following_range = castedMachine->mFollowingRange;
		glm::vec2 view_range = castedMachine->mViewRange;
		glm::vec2 attack_range = castedMachine->mAttackRange;
		glm::vec3 _Pos = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
		glm::vec3 Target_Pos = castedMachine->_TargetCollider->GetCollideBox().GetWorldPosition();
		glm::vec3 Target_Scale = castedMachine->_TargetCollider->GetCollideBox().GetWorldScale();
		bool CanAttack = StaticRectToStaticRect(_Pos + glm::vec3(attack_range.x / 2, 0, 0), abs(attack_range.x), attack_range.y, Target_Pos, Target_Scale.x, Target_Scale.y);
		bool CanFollow = StaticPointToStaticCircle(Target_Pos, _Pos, following_range);

		if (!CanAttack && CanFollow)
			mOwnerStateMachine->ChangeState("GoToPlayer");
		else if (!CanAttack && !CanFollow)
			castedMachine->ChangeState("Idle");
	}
}

void BasicEnemyAttack::LogicExit()
{
}

#pragma endregion

#pragma region BasicEnemyDie

BasicEnemyDie::BasicEnemyDie(const char* name) : State(name)
{}

void BasicEnemyDie::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
}

void BasicEnemyDie::LogicEnter()
{
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Death")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Death");
	}

	if (castedMachine->mSkelRen)
		DieAnimDur = castedMachine->mSkelRen->GetAnimDuration();
	mTimeInState = 0;
}

void BasicEnemyDie::LogicUpdate()
{
	DieAnimTimer += (float)TimeSys->GetFrameTime();
	if(DieAnimTimer >= DieAnimDur)
		mActor->GetParentSpace()->DestroyObject(mActor);
}

void BasicEnemyDie::LogicExit()
{
}

#pragma endregion

#pragma region BasicEnemyReset

BasicEnemyReset::BasicEnemyReset(const char* name) : State(name)
{
}

void BasicEnemyReset::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
}

void BasicEnemyReset::LogicEnter()
{
	if (castedMachine->mRenderable)
	{
		real_color = castedMachine->mRenderable->GetModulationColor();
		castedMachine->mRenderable->SetModulationColor(Colors::red);
	}
	if (castedMachine->mSkelRen)
	{
		if (castedMachine->mSkelRen->GetCurrentAnimationName() != "Basic_Damage")
			castedMachine->mSkelRen->SetCurrentAnimation("Basic_Damage");
	}

	mTimeInState = 0;
	if (castedMachine->mHealth)
	{
		castedMachine->mHealth->Damage(castedMachine->mHDamage);

		if (float hlth = castedMachine->mHealth->GetHealth() <= 0.0f)
		{
			mActor->RemoveComp(mActor->GetComp<Hitbox>());
			if ((rand() % 101) > (100 - castedMachine->purif_prob))
				castedMachine->ChangeState("PurificationState");
			else
				castedMachine->ChangeState("Die");
		}
	}

	
	bool pFacingRight = reinterpret_cast<MovementMachine*>(castedMachine->_Target->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"))->FacingRight;
	if (castedMachine->receivedHitbox) 
	{
		float pHitboxWidth = castedMachine->receivedHitbox->GetComp<TransformComponent>()->GetWorldScale().x;
		//Knockback to the end of the hitbox

		float pTransform = castedMachine->_TargetTransform->GetWorldPosition().x;
		float FinalPosX = pTransform + (pFacingRight ? pHitboxWidth + 100 : -pHitboxWidth - 100);

		float mPos = castedMachine->mOwnerTransform->GetWorldPosition().x;
		float DistanceToTravel = FinalPosX - mPos ;
		//distance that has to travel over time it has to travel it
		mKnockbackSpeed = (DistanceToTravel) / castedMachine->mHKTime;
	}
}

void BasicEnemyReset::LogicUpdate()
{
	if (mTimeInState < castedMachine->mHKTime || mTimeInState < mResetTime)
	{
		glm::vec3 pos = castedMachine->mOwnerTransform->GetWorldPosition();
		pos.x += mKnockbackSpeed * TimeSys->GetFrameTime();
		castedMachine->mOwnerTransform->SetWorldPosition(pos);
	}
	else if (float hlth = castedMachine->mHealth->GetHealth() >0.0f)
	{
		castedMachine->ChangeState("GoToPlayer");
	}
}

void BasicEnemyReset::LogicExit()
{
	if(castedMachine->mRenderable)
		castedMachine->mRenderable->SetModulationColor(real_color);
}

#pragma endregion

#pragma region BasicEnemyStunned

BasicEnemyStunned::BasicEnemyStunned(const char* name) : State(name)
{
}

void BasicEnemyStunned::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<BasicEnemyMachine*>(mOwnerStateMachine);
}

void BasicEnemyStunned::LogicEnter()
{
	mTimeInState = 0;
	mShakeDir = 1;
}

void BasicEnemyStunned::LogicUpdate()
{
	if (mTimeInState >= castedMachine->mStunDuration)
		castedMachine->ChangeState("GoToPlayer");
	glm::vec3 temp_pos = castedMachine->mOwnerTransform->GetWorldPosition();
	temp_pos.x += 100 * TimeSys->GetFrameTime() * mShakeDir;
	castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));
	mShakeDir = -mShakeDir;
}

void BasicEnemyStunned::LogicExit()
{
}

#pragma endregion