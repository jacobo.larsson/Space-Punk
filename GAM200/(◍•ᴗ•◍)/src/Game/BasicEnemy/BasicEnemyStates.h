#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"


class BasicEnemyMachine;
class Renderable;

class BasicEnemyIdle: public State
{
public:
	BasicEnemyIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicUpdate()override;
	void LogicEnter() override;
	void LogicExit()override;

	BasicEnemyMachine* castedMachine;
};

class BasicEnemyDoPath: public State
{
public:
	BasicEnemyDoPath(const char* name = nullptr);

	void Initialize() override;
	void LogicUpdate()override;
	void LogicEnter() override;
	void LogicExit()override;
	void OnCollisionStarted(GameObject* _Other)override;

	glm::vec2 mRightMargin = glm::vec2(0,0);
	glm::vec2 mLeftMargin = glm::vec2(0,0);
	BasicEnemyMachine* castedMachine;
	bool getting_out_edge = false;
};

class BasicEnemyGoToPlayer : public State
{
public:
	BasicEnemyGoToPlayer(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
	void OnCollisionStarted(GameObject* _Other)override;

	BasicEnemyMachine* castedMachine;
};

class BasicEnemyAttack : public State
{
public:
	BasicEnemyAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyMachine* castedMachine;
	
	float direction;

	bool GoingBackwards = true;
	bool AttackaAnim = false;
	bool Attacking = false;

	float distance_covered = 0.0f;
	float AttackAnimTimer = 0.0;
	float AttackAnimDur;

	glm::vec3 _EnemyPos;
};

class BasicEnemyDie : public State
{
public:
	BasicEnemyDie(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyMachine* castedMachine;

	float DieAnimTimer = 0.0f;
	float DieAnimDur;
};

class BasicEnemyReset : public State
{
public:
	BasicEnemyReset(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyMachine* castedMachine;
	glm::vec2 constantKnockback;
	float mKnockbackSpeed;
	float mResetTime = 0;
	Color real_color;
};

class BasicEnemyStunned : public State
{
public:
	BasicEnemyStunned(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	BasicEnemyMachine* castedMachine;
	float mShakeDir = 1;
};
