#pragma once
#include <array>
#include "../../Input/Input.h"
#include "../../StateMachines/StateMachine/StateMachine.h"



class PurificationState : public State
{
public:
	PurificationState(const char* name);
	~PurificationState();

	void Initialize()override;
	void Shutdown()override;
	void LogicEnter()override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionPersisted(GameObject* _Other)override;

private:
	std::array<std::pair<bool, bool (*)()>, 3> mCombo;
	GameObject* mButtons[3];
	GameObject* player;
	GameObject* initial_input;
	unsigned mLastTriggered;
	bool _Started = false;

	float purification_time = 0.f;
	int blink_time = 0;
	int index0;
	int index1;
	int index2;
	int indexes[3];
	std::string archetypeNames[8];
	std::vector<bool (*)()> allInputFunctions;

};