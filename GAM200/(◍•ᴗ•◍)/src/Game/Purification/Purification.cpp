#include <iostream>
#include <random>

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"

#include "../../Archetype/ArchetypeFunctions.h"

#include "../../GameObject/GameObject.h"

#include "../../Transform2D/TransformComponent.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"

#include "../GameInput/GameInput.h"
#include "../PlayerMovement/MovementStates.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../../Collisions/ICollider.h"
#include "Purification.h"

PurificationState::PurificationState(const char* name): State(name),mLastTriggered(0)
{
}

PurificationState::~PurificationState()
{
}

void PurificationState::Initialize()
{
	index0 = rand() % 8;
	index1 = rand() % 8;

	//make the index1 random from 0 to 3, but different to index0
	while(index1 == index0)
		index1 = rand() % 8;

	index2 = rand() % 8;

	while(index2 == index0 || index2 == index1)
		index2 = rand() % 8;
	
	//array of 3 different random numbers between 0 and 7
	indexes[0] = index0;
	indexes[1] = index1;
	indexes[2] = index2;

	//add the input functions to the all inputs vector
	allInputFunctions.push_back(GameInput::PurificationASpacePressed);
	allInputFunctions.push_back(GameInput::PurificationBShiftPressed);
	allInputFunctions.push_back(GameInput::PurificationXClickPressed);
	allInputFunctions.push_back(GameInput::PurificationYRightClickPressed);
	allInputFunctions.push_back(GameInput::PurificationUpWPressed);
	allInputFunctions.push_back(GameInput::PurificationDownSPressed);
	allInputFunctions.push_back(GameInput::PurificationLeftAPressed);
	allInputFunctions.push_back(GameInput::PurificationRigthDPressed);

	for (unsigned i = 0; i < 3; i++)
	{
		mCombo[i].first = false;
		mCombo[i].second = allInputFunctions[indexes[i]];
	}
}

void PurificationState::LogicEnter()
{
	//set the archetypes to create depending if the gamepad is being used
	if (GameInput::purificationUsingGamepad) {
		archetypeNames[0] = "Button_A";
		archetypeNames[1] = "Button_B";
		archetypeNames[2] = "Button_X";
		archetypeNames[3] = "Button_Y";
		archetypeNames[4] = "Joystick_Up";
		archetypeNames[5] = "Joystick_Down";
		archetypeNames[6] = "Joystick_Left";
		archetypeNames[7] = "Joystick_Right";
	}
	else {
		archetypeNames[0] = "Space";
		archetypeNames[1] = "Shift";
		archetypeNames[2] = "LeftClick";
		archetypeNames[3] = "RightClick";
		archetypeNames[4] = "W";
		archetypeNames[5] = "S";
		archetypeNames[6] = "A";
		archetypeNames[7] = "D";
	}
	
	auto transform = mActor->GetComp<TransformComponent>();
	mLastTriggered = 0;

	if (transform != nullptr)
	{
		TransformComponent& enemybox = mActor->GetComp<Collider>()->GetCollideBox();
		glm::vec3 pos = enemybox.GetWorldPosition() + glm::vec3(abs(enemybox.GetWorldScale().x), abs(enemybox.GetWorldScale().y), 0);
		glm::vec3 pos2 = enemybox.GetWorldPosition() + glm::vec3(0, abs(enemybox.GetWorldScale().y), 0);
		pos.z = 19;
		initial_input = CreateFromArchetype("E", mActor->GetParentSpace(), pos2);
		for (unsigned i = 0; i < 3; i++)
			mButtons[i] = CreateFromArchetype(archetypeNames[indexes[i]], mActor->GetParentSpace(), pos);
	}
	
	Renderable* renderable;
	renderable = mButtons[0]->GetComp<Renderable>();
	renderable->SetVisible(false);
	renderable->SetModulationColor(Colors::red);

	renderable = mButtons[1]->GetComp<Renderable>();
	renderable->SetVisible(false);
	renderable->SetModulationColor(Colors::yellow);

	renderable = mButtons[2]->GetComp<Renderable>();
	renderable->SetVisible(false);
	renderable->SetModulationColor(Colors::green);

	renderable = initial_input->GetComp<Renderable>();
	renderable->SetVisible(false);
	renderable->SetModulationColor(Colors::white);

	player = SceneSys->GetMainSpace()->FindObjectByName("Player");
	
}

void PurificationState::LogicUpdate()
{
	purification_time += TimeSys->GetFrameTime();
	blink_time++;
	if (mTimeInState > 4.0f && _Started ==false)
	{
			
		mActor->GetParentSpace()->DestroyObject(mActor);
		
	}


	if (_Started == false)
	{
		Renderable* render = mActor->GetComp<Renderable>();
		
		Renderable* renderable;
		renderable = initial_input->GetComp<Renderable>();
		renderable->SetVisible(true);
		TransformComponent* trs = initial_input->GetComp<TransformComponent>();

		if (blink_time > 20)
			blink_time = 0;

		if (blink_time > 10)
		{
			render->SetModulationColor(Colors::red);
			trs->SetScale(trs->GetScale() + glm::vec3(1, 1, 0));
		}
		else
		{
			render->SetModulationColor(Colors::white);
			trs->SetScale(trs->GetScale() - glm::vec3(1, 1, 0));
		}
		
	}
	else
	{
		Renderable* renderable;
		renderable = initial_input->GetComp<Renderable>();
		renderable->SetVisible(false);
		TransformComponent* trs = mButtons[mLastTriggered]->GetComp<TransformComponent>();
		
		if (mLastTriggered < 3)
		{
			if (blink_time > 20)
				blink_time = 0;

			if (blink_time > 10)
			{
				trs->SetScale(trs->GetScale() + glm::vec3(1,1,0));
			}
			else
			{
				trs->SetScale(trs->GetScale() - glm::vec3(1, 1, 0));
			}


			if (!(mCombo[0].first) && mCombo[0].second())
			{

				Renderable* renderable;
				renderable = mButtons[mLastTriggered]->GetComp<Renderable>();
				renderable->SetVisible(false);
				mCombo[mLastTriggered].first = true;
				mLastTriggered++;

				if (mLastTriggered < 3)
				{
					renderable = mButtons[mLastTriggered]->GetComp<Renderable>();
					renderable->SetVisible(true);
				}
			}
			else if ((mCombo[0].first) && mCombo[mLastTriggered].second())
			{
				Renderable* renderable;
				renderable = mButtons[mLastTriggered]->GetComp<Renderable>();
				renderable->SetVisible(false);
				mCombo[mLastTriggered].first = true;

				mLastTriggered++;

				if (mLastTriggered < 3)
				{
					renderable = mButtons[mLastTriggered]->GetComp<Renderable>();
					renderable->SetVisible(true);
				}
			}
		}
		
		if (mLastTriggered > 2 || purification_time > 4.0f)
		{
			auto movementMachine = player->GetComp<BrainComponent>()->GetStateMachine("MovementMachine");
			if (movementMachine)
			{
				auto playerIdle = reinterpret_cast<PlayerIdle*>(movementMachine->GetState("PlayerIdle"));
				movementMachine->ChangeState("PlayerIdle");
				playerIdle->locked = false;
				
			}

			mActor->GetParentSpace()->DestroyObject(mActor);
			return;
		}
		
	}
}
void PurificationState::OnCollisionPersisted(GameObject* _Other)
{
	if (_Other->CompareTag("Player") && GameInput::ShieldKeyPressed())
	{

		_Started = true;
		purification_time = 0.f;
		blink_time = 0;
		if (player)
		{
			Renderable* renderable;
			renderable = mButtons[0]->GetComp<Renderable>();
			renderable->SetVisible(true);
			auto movementMachine = player->GetComp<BrainComponent>()->GetStateMachine("MovementMachine");
			if (movementMachine)
			{
				auto playerpurf = reinterpret_cast<PlayerPurification*>(movementMachine->GetState("PlayerPurification"));

				if (playerpurf)
					movementMachine->ChangeState("PlayerPurification");

			}
		}
	}
}
void PurificationState::LogicExit()
{
}

void PurificationState::Shutdown()
{
	if (mActor)
	{
		mActor->GetParentSpace()->DestroyObject(mButtons[0]);
		mActor->GetParentSpace()->DestroyObject(mButtons[1]);
		mActor->GetParentSpace()->DestroyObject(mButtons[2]);
		mActor->GetParentSpace()->DestroyObject(initial_input);
		mButtons[0] = nullptr;
		mButtons[1] = nullptr;
		mButtons[2] = nullptr;
		initial_input = nullptr;
	}
}
