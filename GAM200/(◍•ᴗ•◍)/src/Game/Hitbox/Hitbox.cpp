#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../Factory/Factory.h"
#include "../CameraFollow/CameraEffect/CameraEffect.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../CameraFollow/CameraMachine.h"
#include "Hitbox.h"
#include "../../Space/Space.h"


Hitbox::Hitbox()
{
	SetName("Hitbox");
}

void Hitbox::Initialize()
{
	if (mTemplated)
	{
		SetValues(mTemplate);
	}
	mCameraMachine = reinterpret_cast<CameraMachine * >(mOwner->GetParentSpace()->FindObjectByName("Camera")->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));
	LogicComponent::Initialize();
}

void Hitbox::Update()
{
}

void Hitbox::LateUpdate()
{
}

void Hitbox::Shutdown()
{
	LogicComponent::Shutdown();
}
bool Hitbox::Edit()
{
	ImGui::Text("Hitbox Editor");
	if (ImGui::CollapsingHeader("Custom Hitbox")) 
	{
		ImGui::Checkbox("Is a enemy Hitbox?", &mEnemy);
		ImGui::DragFloat("Hitbox Damage", &mDamage);
		ImGui::Dummy(ImVec2(100, 0));
		ImGui::Checkbox("Does it make knockback?", &mKnockback);

		ImGui::DragFloat("Hitbox Knockback Time", &mKTime);
		ImGui::DragFloat("Hitbox Knockback Speed", &mKSpeed);
	}

	ImGui::Dummy(ImVec2(200, 0));

	if (ImGui::CollapsingHeader("Templated Hitboxes")) 
	{
		if (ImGui::IsItemClicked(ImGui::Button("Basic Enemy"))) {
			SetValues(Owner::Basic);
			mTemplated = true;
			mTemplate = Owner::Basic;
		}

		if (ImGui::IsItemClicked(ImGui::Button("Charger Enemy")))
		{
			SetValues(Owner::Charger);
			mTemplated = true;
			mTemplate = Owner::Charger;

		}

		if (ImGui::IsItemClicked(ImGui::Button("Bullet"))) {
			SetValues(Owner::Bullet);
			mTemplated = true;
			mTemplate = Owner::Bullet;

		}

		if (ImGui::IsItemClicked(ImGui::Button("Flying Enemy"))) {
			SetValues(Owner::Flying);
			mTemplated = true;
			mTemplate = Owner::Flying;

		}

		if (ImGui::IsItemClicked(ImGui::Button("Gas Enemy"))) {
			SetValues(Owner::GasEnemy);
			mTemplated = true;
			mTemplate = Owner::GasEnemy;

		}

		if (ImGui::IsItemClicked(ImGui::Button("Gas Cloud"))) {
			SetValues(Owner::GasCloud);
			mTemplated = true;
			mTemplate = Owner::GasCloud;

		}

		if (ImGui::Checkbox("Templated", &mTemplated)){
		}
		if (mTemplated)
			ImGui::Text(Default_Values::defNames[(int)mTemplate]);

		
	}

	ImGui::Dummy(ImVec2(300, 0));

	return true;
}

void Hitbox::ToJson(nlohmann::json& j)
{
	j["Damage"] = mDamage;
	j["Knocback"] = mKnockback;
	j["Enemy"] = mEnemy;
	j["KnockbackTime"] = mKTime;
	j["KnockbackSpeed"] = mKSpeed;
	j["mTemplated"] = mTemplated;
	j["mTemplate"] = mTemplate;
}

void Hitbox::FromJson(const nlohmann::json& j)
{
	if (j.find("Damage") != j.end())
		mDamage = j["Damage"];

	if (j.find("Knocback") != j.end())
		mKnockback = j["Knocback"];

	if (j.find("Enemy") != j.end())
		mEnemy = j["Enemy"];

	if (j.find("KnockbackTime") != j.end())
		mKTime = j["KnockbackTime"];

	if (j.find("KnockbackSpeed") != j.end())
		mKSpeed = j["KnockbackSpeed"];

	if (j.find("mTemplated") != j.end())
		mTemplated = j["mTemplated"];

	if (j.find("mTemplate") != j.end())
		mTemplate = j["mTemplate"];

	if (mTemplated) {
		SetValues(mTemplate);
	}
}

Hitbox* Hitbox::Clone()
{
	Hitbox* temp = FactorySys->Create<Hitbox>();
	(*temp) = (*this);
	return temp;
}

Hitbox& Hitbox::operator=(const Hitbox& rhs)
{
	mDamage = rhs.mDamage;
	mEnemy = rhs.mEnemy;
	mKnockback = rhs.mKnockback;
	mKTime = rhs.mKTime;
	mKSpeed = rhs.mKSpeed;

	mTemplated = rhs.mTemplated;
	mTemplate = rhs.mTemplate;
	return (*this);
}

void Hitbox::OnCollisionStarted(GameObject* other)
{
	//If we are a player hitbox and the other is an enemy hitbox
	if (!mEnemy && other->CompareTag("Enemy"))
	{
		if (mCameraMachine) 
		{
			CameraEffect _Effect;
			_Effect.mEffectType = CameraEffectLists::CameraShake;
			_Effect.mMagnitude = mDamage;
			_Effect.mDuration = mKTime;

			mCameraMachine->AddEffect(_Effect);
		}
	}
}

void Hitbox::SetValues(Owner owner_)
{
	mEnemy = true;
	switch (owner_) 
	{

	case Owner::Basic:
		mDamage =		Default_Values::Basic_Damage;
		mKnockback =	Default_Values::Basic_Knockback;
		mKSpeed =		Default_Values::Basic_Knockback_Speed;
		mKTime =		Default_Values::Basic_Knockback_Time;
		break;

	case Owner::Charger:
		mDamage =		Default_Values::Charger_Damage;
		mKnockback =	Default_Values::Charger_Knockback;
		mKSpeed =		Default_Values::Charger_Knockback_Speed;
		mKTime =		Default_Values::Charger_Knockback_Time;
		break;

	case Owner::Flying:
		mDamage =		Default_Values::Flying_Damage;
		mKnockback =	Default_Values::Flying_Knockback;
		mKSpeed =		Default_Values::Flying_Knockback_Speed;
		mKTime =		Default_Values::Flying_Knockback_Time;
		break;

	case Owner::Bullet:
		mDamage =		Default_Values::Bullet_Damage;
		mKnockback =	Default_Values::Bullet_Knockback;
		mKSpeed =		Default_Values::Bullet_Knockback_Speed;
		mKTime =		Default_Values::Bullet_Knockback_Time;
		break;

	case Owner::GasEnemy:
		mDamage =		Default_Values::GasEnemy_Damage;
		mKnockback =	Default_Values::GasEnemy_Knockback;
		mKSpeed =		Default_Values::GasEnemy_Knockback_Speed;
		mKTime =		Default_Values::GasEnemy_Knockback_Time;
		break;

	case Owner::GasCloud:
		mDamage =		Default_Values::GasCloud_Damage;
		mKnockback =	Default_Values::GasCloud_Knockback;
		mKSpeed =		Default_Values::GasCloud_Knockback_Speed;
		mKTime =		Default_Values::GasCloud_Knockback_Time;
		break;
	}
	mTemplated = true;
}
