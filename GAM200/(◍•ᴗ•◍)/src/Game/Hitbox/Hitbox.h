#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"


class CameraMachine;
enum class Owner {
	Player,
	Basic,
	Bullet,
	Charger,
	Flying,
	GasEnemy,
	GasCloud
};


namespace Default_Values
{
	static const char* defNames[7] = { "Player", "Basic", "Bullet","Charger", "Flying", "GasEnemy", "GasCloud" };

	static float Basic_Damage = 19;
	static bool Basic_Knockback = 0;
	static float Basic_Knockback_Time = 1;
	static float Basic_Knockback_Speed = 100;

	static float Charger_Damage = 20;
	static bool  Charger_Knockback = 0;
	static float Charger_Knockback_Time = 0;
	static float Charger_Knockback_Speed = 0;

	static float Bullet_Damage = 15;
	static bool  Bullet_Knockback = 0;
	static float Bullet_Knockback_Time = 0;
	static float Bullet_Knockback_Speed = 0;

	static float Flying_Damage = 10;
	static bool  Flying_Knockback = 0;
	static float Flying_Knockback_Time = 0;
	static float Flying_Knockback_Speed = 0;

	static float GasEnemy_Damage = 10;
	static bool  GasEnemy_Knockback = 0;
	static float GasEnemy_Knockback_Time = 0;
	static float GasEnemy_Knockback_Speed = 0;

	static float GasCloud_Damage = 25;
	static bool  GasCloud_Knockback = 0;
	static float GasCloud_Knockback_Time = 0;
	static float GasCloud_Knockback_Speed = 0;
}

class Hitbox : public LogicComponent
{
	SM_RTTI_DECL(Hitbox, LogicComponent)
public:
	Hitbox();

	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;
	Hitbox* Clone();
	Hitbox& operator=(const Hitbox& rhs);
	void OnCollisionStarted(GameObject* other)override;

	void SetValues(Owner owner_);

	bool mTemplated = false;
	Owner mTemplate;

	bool mEnemy; //Is the hitbox attached to an enemy?
	float mDamage; //Damage it should deal
	//knockback related
	bool mKnockback; //if it makes knockback at all
	float mKTime; //Time knockback affects target
	float mKSpeed; //Speed at which the target is launched

	CameraMachine* mCameraMachine;
};