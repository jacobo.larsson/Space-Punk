#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/Raycast.h"
#include "../../RayCast/Raycaster.h"


class ChargerEnemyMachine;

class ChargerEnemyIdle : public State
{
public:
	ChargerEnemyIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	void OnCollisionStarted(GameObject* _Other)override;

	ChargerEnemyMachine* castedMachine;
	bool mWalking = false;
	float mWalkingDistance = 0;
};

class ChargerEnemyCharge : public State
{
public:
	ChargerEnemyCharge(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	void OnCollisionStarted(GameObject* _Other)override;

	ChargerEnemyMachine* castedMachine;

	float mCurrentVelocity;
};

class ChargerEnemyStunned : public State
{
public:
	ChargerEnemyStunned(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit() override;

	ChargerEnemyMachine* castedMachine;

	float mDistanceTravelled = 0;
	float mShakeDir = 1;
	bool mGoingBackwards = true;
	bool Fliped = false;
};

class ChargerEnemyDie : public State
{
public:
	ChargerEnemyDie(const char* name = nullptr);


	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit() override;

	ChargerEnemyMachine* castedMachine;
};