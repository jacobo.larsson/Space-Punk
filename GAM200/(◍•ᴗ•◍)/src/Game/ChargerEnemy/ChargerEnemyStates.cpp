#include <iostream>
#include <math.h>

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"


#include "../../RayCast/RayCastTest.h"
#include "../../Graphics/Renderable/Renderable.h"

#include "../Hitbox/Hitbox.h"

#include "../Health/Health.h"
#include "ChargerEnemySystem.h"
#include "ChargerEnemyStates.h"



#pragma region ChargerEnemyIdle
ChargerEnemyIdle::ChargerEnemyIdle(const char* name) : State(name)
{
}

void ChargerEnemyIdle::Initialize()
{
	castedMachine = dynamic_cast<ChargerEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void ChargerEnemyIdle::LogicEnter()
{
	mWalking = false;
	mWalkingDistance = 0;
	mTimeInState = 0;
}

void ChargerEnemyIdle::LogicUpdate()
{
	if (castedMachine->LookForPlayer())
		mOwnerStateMachine->ChangeState("ChargerEnemyCharge");

	if (mWalking == false)
	{
		if (mTimeInState >= castedMachine->mWaitingTime)
		{
			if ((rand() % 11) > 5)
			{
				castedMachine->mDirection = 1;
				castedMachine->mLookingRight = true;
				castedMachine->mRenderable->SetFlipX(castedMachine->mLookingRight);
			}
			else
			{
				castedMachine->mDirection = -1;
				castedMachine->mLookingRight = false;
				castedMachine->mRenderable->SetFlipX(castedMachine->mLookingRight);
			}
			mWalking = true;
		}
	}
	else
	{
		glm::vec3 temp_pos = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
		temp_pos.x += castedMachine->mWanderingVelocity * TimeSys->GetFrameTime() * castedMachine->mDirection;
		mWalkingDistance += castedMachine->mWanderingVelocity * TimeSys->GetFrameTime() * castedMachine->mDirection;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));

		if (abs(mWalkingDistance) >= castedMachine->mWanderingDistance)
		{
			mWalking = false;
			mTimeInState = 0;
			mWalkingDistance = 0;
		}
	}


}

void ChargerEnemyIdle::LogicExit()
{
}

void ChargerEnemyIdle::OnCollisionStarted(GameObject* _Other)
{
	if (_Other->CompareTag("Wall"))
	{
		mWalking = false;
		mTimeInState = 0;
		mWalkingDistance = 0;
	}
}

#pragma endregion

#pragma region ChargerEnemyCharge
ChargerEnemyCharge::ChargerEnemyCharge(const char* name) : State(name)
{
}

void ChargerEnemyCharge::Initialize()
{
	castedMachine = dynamic_cast<ChargerEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void ChargerEnemyCharge::LogicEnter()
{
	mCurrentVelocity = castedMachine->mStartingVelocity;

	if (castedMachine->mLookingRight)
		castedMachine->mDirection = 1;
	else
		castedMachine->mDirection = -1;

	mTimeInState = 0;
}

void ChargerEnemyCharge::LogicUpdate()
{

	if (mTimeInState > 1)
	{
		glm::vec3 temp_pos = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
		temp_pos.x += mCurrentVelocity * TimeSys->GetFrameTime() * castedMachine->mDirection;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));

		if(mCurrentVelocity < castedMachine->mEndVelocity)
			mCurrentVelocity += castedMachine->mChargeAcceleration;
	}
}

void ChargerEnemyCharge::LogicExit()
{
}

void ChargerEnemyCharge::OnCollisionStarted(GameObject* _Other)
{
	if(_Other->CompareTag("Wall"))
		mOwnerStateMachine->ChangeState("ChargerEnemyStunned");
}

#pragma endregion

#pragma region ChargerEnemyStunned
ChargerEnemyStunned::ChargerEnemyStunned(const char* name) : State(name)
{
}

void ChargerEnemyStunned::Initialize()
{
	castedMachine = dynamic_cast<ChargerEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void ChargerEnemyStunned::LogicEnter()
{
	mTimeInState = 0;
	mDistanceTravelled = 0;
	mGoingBackwards = true;
	Fliped = false;
}

void ChargerEnemyStunned::LogicUpdate()
{
	if (mGoingBackwards)
	{
		glm::vec3 temp_pos = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
		temp_pos.x += 50 * TimeSys->GetFrameTime() * -castedMachine->mDirection;
		mDistanceTravelled += 50 * TimeSys->GetFrameTime() * -castedMachine->mDirection;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));
		if (abs(mDistanceTravelled) >= castedMachine->mBackwardsDistance)
		{
			mGoingBackwards = false;
			mTimeInState = 0;
		}
	}
	else
	{
		glm::vec3 temp_pos = castedMachine->mOwnerCollider->GetCollideBox().GetWorldPosition();
		temp_pos.x += castedMachine->mShakeFactor * TimeSys->GetFrameTime() * mShakeDir;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(temp_pos.x, temp_pos.y, 0));
		mShakeDir = -mShakeDir;
		if (mTimeInState >= castedMachine->mTimeStunned/2 && !Fliped)
		{
			castedMachine->mLookingRight = !castedMachine->mLookingRight;
			castedMachine->mRenderable->SetFlipX(castedMachine->mLookingRight);
			Fliped = true;
		}
		else if (mTimeInState >= castedMachine->mTimeStunned)
			mOwnerStateMachine->ChangeState("ChargerEnemyIdle");
	}
}

void ChargerEnemyStunned::LogicExit()
{
}

#pragma endregion

#pragma region ChargerEnemyDie
ChargerEnemyDie::ChargerEnemyDie(const char* name) : State(name)
{
}

void ChargerEnemyDie::Initialize()
{
	castedMachine = dynamic_cast<ChargerEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void ChargerEnemyDie::LogicEnter()
{
}
void ChargerEnemyDie::LogicUpdate()
{
	mActor->GetParentSpace()->DestroyObject(mActor);
	mOwnerStateMachine->Shutdown();
}

void ChargerEnemyDie::LogicExit()
{	
}

#pragma endregion

