#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/IComp.h"
#include "../../RTTI/SM_RTTI.h"


#include "../../Input/Input.h" //Input for movement

#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"
#include "../../RayCast/Raycast.h"

#include "../../StateMachines/StateMachine/StateMachine.h"

class Health;
class ChargerEnemyMachine : public StateMachine
{
	SM_RTTI_DECL(ChargerEnemyMachine, StateMachine);

public:

	ChargerEnemyMachine();
	~ChargerEnemyMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	ChargerEnemyMachine& operator=(ChargerEnemyMachine& rhs);
	ChargerEnemyMachine* Clone()override;

	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	bool LookForPlayer();

	void OnCollisionStarted(GameObject* other) override;

	//Variables
	GameObject* _Target;
	Collider* mOwnerCollider = nullptr;
	Collider* _TargetCollider = nullptr;
	TransformComponent* mOwnerTransform = nullptr;
	Renderable* mRenderable = nullptr;
	Health* mHealth = nullptr;

	std::vector<GameObject*> mRayPlatforms;
	std::vector<GameObject*> mRayWalls;

	float mViewRange = 300;
	bool mLookingRight = true;
	float mDirection = 1;

	//Wandering Around
	float mWanderingDistance = 0;
	float mWanderingVelocity = 0;
	float mWaitingTime = 0;

	//Charge
	float mChargeAcceleration = 0;
	float mStartingVelocity = 0;
	float mEndVelocity = 0;

	//Stunned
	float mBackwardsDistance = 0;
	float mTimeStunned = 0;
	float mShakeFactor = 0;

	int purif_prob = 50;
};