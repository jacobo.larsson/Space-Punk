#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Time/Time.h"
#include "../Health/Health.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Input/Input.h"
#include "../../Factory/Factory.h"
#include "../../RayCast/RayCastTest.h"
#include "../../Graphics/Color/Color.h"
#include "../Purification/Purification.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../Graphics/RenderManager/RenderManager.h"
#include "../../Collisions/Collisions.h"

//TODO:Testing. Might remove
#include "../ShotReceiver/ShotReceiver.h"

#include "../ChargerEnemy/ChargerEnemySystem.h"
#include "../ChargerEnemy/ChargerEnemyStates.h"
#include "../Hitbox/Hitbox.h"

ChargerEnemyMachine::ChargerEnemyMachine()
{
	mName = "ChargerEnemyMachine";

	mInitialState = new ChargerEnemyIdle("ChargerEnemyIdle");
	mCurrentState = mInitialState;
	AddState(mInitialState);
	AddState(new ChargerEnemyCharge("ChargerEnemyCharge"));
	AddState(new ChargerEnemyStunned("ChargerEnemyStunned"));
	AddState(new ChargerEnemyDie("ChargerEnemyDie"));
	AddState(new PurificationState("PurificationState"));
}

ChargerEnemyMachine::~ChargerEnemyMachine() { StateMachine::~StateMachine(); }

void ChargerEnemyMachine::Initialize()
{
	mOwnerCollider = mActor->GetComp<Collider>();
	mOwnerTransform = mActor->GetComp<TransformComponent>();
	_Target = SceneSys->GetMainSpace()->FindObjectByName("Player");

	if ((_TargetCollider = _Target->GetComp<Collider>()) == nullptr)
	{
		std::cout << "Target was not set" << std::endl;
		return;
	}

	if (mOwnerCollider == nullptr)
	{
		std::cout << mActor->GetName() << " missing  transform component" << std::endl;
		return;
	}

	if (mRenderable == nullptr)
		mRenderable = mActor->GetComp<Renderable>();

	if (mHealth == nullptr)
		mHealth = mActor->GetComp<Health>();

	std::for_each(mActor->GetParentSpace()->GetComponents<Collider>().begin(), mActor->GetParentSpace()->GetComponents<Collider>().end(), [&](auto& it)
	{
		Collider* _Other = reinterpret_cast<Collider*>(it);
		GameObject* _Owner = _Other->GetOwner();

		glm::vec3 ObbPos{ _Other->GetCollideBox().GetWorldPosition() };
		glm::vec3 ObbScale{ _Other->GetCollideBox().GetScale() };
		float ObbRot = _Other->GetCollideBox().GetOrientation();

		// TODO: Change the last parameter to avoid the enemy looking for the player in a circular area.
		if (OrientedRectToStaticCircle(ObbPos, ObbScale.x, ObbScale.y, ObbRot, mActor->GetComp<TransformComponent>()->GetWorldPosition(), 3 * mViewRange))
		{
			if (_Other->GetOwner()->CompareTag("Platform"))
				mRayPlatforms.push_back(_Owner);
			if (_Other->GetOwner()->CompareTag("Wall"))
				mRayWalls.push_back(_Owner);
		}
	});


	StateMachine::Initialize();
	mInitialState->LogicEnter();
}

void ChargerEnemyMachine::Update()
{
	StateMachine::Update();
}

void ChargerEnemyMachine::Shutdown()
{
	StateMachine::Clear();
}

void ChargerEnemyMachine::OnCollisionStarted(GameObject* other)
{

	if (other->CompareTag("Hitbox") && mCurrentState != GetState("PurificationState") && mCurrentState != GetState("ChargerEnemyDie"))
	{
		if (Hitbox* hb = other->GetComp<Hitbox>()) 
		{
			if (!hb->mEnemy && mHealth)
			{
				mHealth->Damage(hb->mDamage);
				mActor->RemoveComp(mActor->GetComp<Hitbox>());

				if (float hlth = mHealth->GetHealth() <= 0.0f)
				{
					if ((rand() % 101) > (100 - purif_prob))
						ChangeState("PurificationState");
					else
						ChangeState("ChargerEnemyDie");
				}
			}
		}
	}
	StateMachine::OnCollisionStarted(other);
}

ChargerEnemyMachine& ChargerEnemyMachine::operator=(ChargerEnemyMachine& rhs)
{
	mViewRange = rhs.mViewRange;
	mLookingRight = rhs.mLookingRight;
	
	mWanderingDistance = rhs.mWanderingDistance;
	mWanderingVelocity = rhs.mWanderingVelocity;
	mWaitingTime = rhs.mWaitingTime;

	mChargeAcceleration = rhs.mChargeAcceleration;
	mStartingVelocity = rhs.mStartingVelocity;
	mEndVelocity = rhs.mEndVelocity;

	mBackwardsDistance = rhs.mBackwardsDistance;
	mTimeStunned = rhs.mTimeStunned;
	mShakeFactor = rhs.mShakeFactor;

	purif_prob = rhs.purif_prob;
	return *this;
}

ChargerEnemyMachine* ChargerEnemyMachine::Clone()
{
	ChargerEnemyMachine* temp = FactorySys->Create<ChargerEnemyMachine>();
	*temp = *this;
	return temp;
}

bool ChargerEnemyMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		ImGui::Text("Target: ");
		if (_Target)
		{
			ImGui::SameLine();
			ImGui::Text(_Target->GetName().c_str());
		}
		if (ImGui::BeginDragDropTarget())
		{
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
			{
				IM_ASSERT(payload->DataSize == sizeof(GameObject*));
				GameObject* _Object = *((GameObject**)payload->Data);
				if (_Object->CompareTag("Player"))
				{
					_Target = _Object;
				}
			}
			ImGui::EndDragDropTarget();
		}

		for (auto state : mStates)
		{
			if (state->edit)
			{
				if (ImGui::CollapsingHeader(state->GetName().c_str()))
					state->Edit();
			}
		}

		ImGui::Separator();

		ImGui::DragFloat("ViewRange", &mViewRange);
		if (ImGui::Checkbox("mLookingRight", &mLookingRight))
			mActor->GetComp<Renderable>()->SetFlipX(mLookingRight);

		ImGui::DragFloat("mWanderingDistance", &mWanderingDistance);
		ImGui::DragFloat("mWanderingVelocity", &mWanderingVelocity);
		ImGui::DragFloat("mWaitingTime", &mWaitingTime);

		ImGui::DragFloat("mChargeAcceleration", &mChargeAcceleration);
		ImGui::DragFloat("mStartingVelocity", &mStartingVelocity);
		ImGui::DragFloat("mEndVelocity", &mEndVelocity);

		ImGui::DragFloat("mBackwardsDistance", &mBackwardsDistance);
		ImGui::DragFloat("mTimeStunned", &mTimeStunned);
		ImGui::DragFloat("mShakeFactor", &mShakeFactor);

		ImGui::DragInt("Purification probability", &purif_prob);
		mActor->GetParentSpace()->DrawCircle(mActor->GetComp<Collider>()->GetCollideBox().GetWorldPosition(), mViewRange, Colors::red);
		ImGui::End();
	}
	else
		ImGui::End();
	return false;
}

void ChargerEnemyMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["ViewRange"] = mViewRange;
	j["mLookingRight"] = mLookingRight;

	j["mWanderingDistance"] = mWanderingDistance;
	j["mWanderingVelocity"] = mWanderingVelocity;
	j["mWaitingTime"] = mWaitingTime;

	j["mChargeAcceleration"] = mChargeAcceleration;
	j["mStartingVelocity"] = mStartingVelocity;
	j["mEndVelocity"] = mEndVelocity;

	j["mBackwardsDistance"] = mBackwardsDistance;
	j["mTimeStunned"] = mTimeStunned;
	j["mShakeFactor"] = mShakeFactor;

	j["purif_prob"] = purif_prob;

	//if (_Target)
	//{
	//	j["_Target"]["Name"] = _Target->GetName();
	//	j["_Target"]["UID"] = _Target->GetUID();
	//}
}

void ChargerEnemyMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("ViewRange") != j.end())
		mViewRange = j["ViewRange"];
	if (j.find("mLookingRight") != j.end())
		mLookingRight = j["mLookingRight"];

	if (j.find("mWanderingDistance") != j.end())
		mWanderingDistance = j["mWanderingDistance"];
	if (j.find("mWanderingVelocity") != j.end())
		mWanderingVelocity = j["mWanderingVelocity"];
	if (j.find("mWaitingTime") != j.end())
		mWaitingTime = j["mWaitingTime"];

	if (j.find("mChargeAcceleration") != j.end())
		mChargeAcceleration = j["mChargeAcceleration"];
	if (j.find("mStartingVelocity") != j.end())
		mStartingVelocity = j["mStartingVelocity"];
	if (j.find("mEndVelocity") != j.end())
		mEndVelocity = j["mEndVelocity"];

	if (j.find("mBackwardsDistance") != j.end())
		mBackwardsDistance = j["mBackwardsDistance"];
	if (j.find("mTimeStunned") != j.end())
		mTimeStunned = j["mTimeStunned"];
	if (j.find("mShakeFactor") != j.end())
		mShakeFactor = j["mShakeFactor"];

	if (j.find("purif_prob") != j.end())
		purif_prob = j["purif_prob"];


	//if (j.find("_Target") != j.end())
	//{
	//	Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mActor->GetSpaceID()));
	//	if(_space)
	//		_space->AddPointerToLink(&_Target, j["_Target"]["UID"], j["_Target"]["Name"].get<std::string>());
	//}
}

bool ChargerEnemyMachine::LookForPlayer()
{	
	glm::vec2 Direction{ _TargetCollider->GetCollideBox().GetWorldPosition() - mOwnerCollider->GetCollideBox().GetWorldPosition() };
	float length = glm::length(Direction);

	if (SettingsSys->debug_draw && mOwnerCollider)
		mActor->GetParentSpace()->DrawCircle(mOwnerCollider->GetCollideBox().GetWorldPosition(), mViewRange, Colors::red);

	//Only if we are in range check if there is something blocking
	if (length <= mViewRange)
	{
		float angle = acos(Direction.x / length);
		if (Direction.y < 0)
			angle = 2 * 3.14159 - angle;
		Ray ray;
		ray.SetOrientation(angle);
		ray.SetOwnerTransform(&mOwnerCollider->GetCollideBox());


		if (SettingsSys->debug_draw)
			DrawRay(&ray);
		float _PlayerDistance = CheckRayWithObject(&ray, nullptr, _Target);
		float _WallDistance = CheckRayWithVector(&ray, nullptr, nullptr, mRayWalls);
		float _PlatformDistance = CheckRayWithVector(&ray, nullptr, nullptr, mRayPlatforms);
		float _MinWallPlatformDistance = 0;

		if (_WallDistance == -1.0f)
			_WallDistance = INFINITY;
		if (_PlatformDistance == -1.0f)
			_PlatformDistance = INFINITY;

		if(_PlayerDistance < _WallDistance && _PlayerDistance < _PlatformDistance)
		{
			//Flip to look the player
			if (Direction.x < 0)
				mLookingRight = false;
			else
				mLookingRight = true;
			mRenderable->SetFlipX(mLookingRight);

			return true;
		}
	}
	return false;
}
