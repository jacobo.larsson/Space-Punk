#pragma once
#include <glm/glm.hpp>

#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"

class GameObject;
class TransformComponent;
class PropertyMap;
class TriggerArena : public LogicComponent
{
	SM_RTTI_DECL(TriggerArena, LogicComponent)
public:
	TriggerArena();

	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionPersisted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

	void UnlockArena();

private:
	GameObject* mCamera;
	GameObject* mPlayer;
	GameObject* mLeftBlock;
	GameObject* mRightBlock;
	TransformComponent* mPlayerTransform;
	glm::vec3 mCenter;
	glm::vec2 mSize;
	glm::vec2 mHorizontalMargins;
	glm::vec2 mVerticalMargins;
	PropertyMap mProperties;
};