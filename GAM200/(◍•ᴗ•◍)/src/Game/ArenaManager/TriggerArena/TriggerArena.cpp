#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Scene/Scene.h"
#include "../../../AssertSystem/AssertSystem.h"
#include "../../../PropertySystem/PropertyMap.h"
#include "../../../GameObject/GameObject.h"
#include "../../../Space/Space.h"
#include "../../../Settings/Settings.h"
#include "../../../Archetype/ArchetypeFunctions.h"

#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../CameraFollow/CameraFollow.h"
#include "../../CameraFollow/CameraMachine.h"
#include "../../CameraFollow/CameraStates.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Graphics/Camera/Camera.h"

#include "TriggerArena.h"

TriggerArena::TriggerArena():mCenter{0.0f, 0.0f, 20.0f}, mSize(1.0f, 1.0f)
{
	SetName("TriggerArena");
	mProperties.AddProperty<float>("YOffSet");
	mProperties.AddProperty<float>("mRightLock");
	mProperties.AddProperty<float>("mLeftLock");
}

void TriggerArena::Initialize()
{
	LogicComponent::Initialize();
	mLeftBlock = CreateFromArchetype("ArenaBlock", mOwner->GetParentSpace(),glm::vec3(mCenter.x - mProperties(1.0f, "mLeftLock"), mCenter.y, 0.0f));
	mRightBlock = CreateFromArchetype("ArenaBlock", mOwner->GetParentSpace(),glm::vec3(mCenter.x + mProperties(1.0f, "mRightLock"), mCenter.y, 0.0f));
	mPlayer = SceneSys->GetMainSpace()->FindObjectByName("Player");
	mCamera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	mLeftBlock->SetEnabled(false);
	mRightBlock->SetEnabled(false);
}

void TriggerArena::Update()
{
	if (Space* _Space = mOwner->GetParentSpace() )
	{
		if (SettingsSys->debug_draw)
		{
			mPlayerTransform = mPlayer->GetComp<TransformComponent>();
			glm::vec3 botRightMargin(mCenter.x + mHorizontalMargins.y, mCenter.y - mVerticalMargins.x, 20.0f);
			glm::vec3 topRightMargin(mCenter.x + mHorizontalMargins.y, mCenter.y + mVerticalMargins.y, 20.0f);
			glm::vec3 botLeftMargin(mCenter.x - mHorizontalMargins.x, mCenter.y - mVerticalMargins.x, 20.0f);
			glm::vec3 topLeftMargin(mCenter.x - mHorizontalMargins.x, mCenter.y + mVerticalMargins.y, 20.0f);
			_Space->DrawLine(botRightMargin, topRightMargin, Colors::yellow);
			_Space->DrawLine(botLeftMargin, topLeftMargin, Colors::yellow);
			_Space->DrawLine(botLeftMargin, botRightMargin, Colors::yellow);
			_Space->DrawLine(topLeftMargin, topRightMargin, Colors::yellow);
			glm::vec3 abotRightMargin(mCenter.x + mHorizontalMargins.y + mSize.x / 2, mCenter.y - mVerticalMargins.x - mSize.y / 2, 20.0f);
			glm::vec3 atopRightMargin(mCenter.x + mHorizontalMargins.y + mSize.x / 2, mCenter.y + mVerticalMargins.y + mSize.y / 2 + mProperties(1.0f, "YOffSet"), 20.0f);
			glm::vec3 abotLeftMargin(mCenter.x - mHorizontalMargins.x - mSize.x / 2, mCenter.y - mVerticalMargins.x - mSize.y / 2, 20.0f);
			glm::vec3 atopLeftMargin(mCenter.x - mHorizontalMargins.x - mSize.x / 2, mCenter.y + mVerticalMargins.y + mSize.y / 2 + mProperties(1.0f, "YOffSet"), 20.0f);

			_Space->DrawLine(abotRightMargin, atopRightMargin, Colors::red);
			_Space->DrawLine(abotLeftMargin, atopLeftMargin, Colors::red);
			_Space->DrawLine(abotLeftMargin, abotRightMargin, Colors::red);
			_Space->DrawLine(atopLeftMargin, atopRightMargin, Colors::red);

			_Space->Drawrectangle(mCenter, mSize.x, mSize.y, Colors::green);
			_Space->DrawCircle(mCenter, 5.0f, Colors::red);
		}
	}
}

void TriggerArena::LateUpdate()
{
}

void TriggerArena::Shutdown()
{
	LogicComponent::Shutdown();
}

bool TriggerArena::Edit()
{
	bool changed = false;
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
		{
			IM_ASSERT(payload->DataSize == sizeof(GameObject*));
			GameObject* _Camera = *((GameObject**)payload->Data);
			if (_Camera->HasComp<CameraComponent>() )
			{
				mCamera = _Camera;
			}
			else if (_Camera->GetName() == "Player")
			{
				mPlayer = _Camera;
			}
		}
		ImGui::EndDragDropTarget();
	}
	ImGui::Text("Camera: ");
	if (mCamera)
	{
		ImGui::SameLine();
		ImGui::Text(mCamera->GetName().c_str());
	}
	ImGui::Text("Object that triggers the arena: ");
	if (mPlayer != nullptr)
	{
		ImGui::SameLine();
		ImGui::Text(mPlayer->GetName().c_str());
	}
	{
		ImGui::PushID("Center");
		ImGui::Columns(2, "Center", true);
		ImGui::DragFloat("X", &mCenter.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::NextColumn();
		ImGui::DragFloat("Y", &mCenter.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::Columns(1);
		ImGui::PopID();
	}
	{
		ImGui::Text("Margins");
		ImGui::PushID("HorizontalMargins");
		ImGui::Columns(2, "HorizontalMargins", true);
		ImGui::DragFloat("Left", &mHorizontalMargins.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::NextColumn();
		ImGui::DragFloat("Right", &mHorizontalMargins.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Columns(1);
		ImGui::PopID();

		ImGui::PushID("VerticalMargins");
		ImGui::Columns(2, "VerticalMargins", true);
		ImGui::DragFloat("Bot", &mVerticalMargins.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::NextColumn();
		ImGui::DragFloat("Top", &mVerticalMargins.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Columns(1);
		ImGui::PopID();

		ImGui::Text("Locks");
		ImGui::PushID("Lock");
		ImGui::Columns(2, "Locks", true);
		ImGui::DragFloat("mLeftLock", &mProperties(1.0f, "mLeftLock"));
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::NextColumn();
		ImGui::DragFloat("mRightLock", &mProperties(1.0f, "mRightLock"));
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Columns(1);
		ImGui::PopID();

		
		ImGui::DragFloat("YOffSet", &mProperties(1.0f, "YOffSet"));

		if (Space* _Space = mOwner->GetParentSpace())
		{
			glm::vec3 botRightMargin(mCenter.x + mHorizontalMargins.y, mCenter.y - mVerticalMargins.x, 5.0f);
			glm::vec3 topRightMargin(mCenter.x + mHorizontalMargins.y, mCenter.y + mVerticalMargins.y, 5.0f);
			glm::vec3 botLeftMargin(mCenter.x - mHorizontalMargins.x, mCenter.y - mVerticalMargins.x, 5.0f);
			glm::vec3 topLeftMargin(mCenter.x - mHorizontalMargins.x, mCenter.y + mVerticalMargins.y, 5.0f);
			_Space->DrawLine(botRightMargin, topRightMargin, Colors::yellow);
			_Space->DrawLine(botLeftMargin, topLeftMargin, Colors::yellow);
			_Space->DrawLine(botLeftMargin, botRightMargin, Colors::yellow);
			_Space->DrawLine(topLeftMargin, topRightMargin, Colors::yellow);

			glm::vec3 leftLock(mCenter.x - mProperties(1.0f, "mLeftLock"), mCenter.y, 0.0f);
			glm::vec3 rightLock(mCenter.x + mProperties(1.0f, "mRightLock"), mCenter.y, 0.0f);
			_Space->DrawCircle(leftLock, 5.0f, Colors::blue);
			_Space->DrawCircle(rightLock, 5.0f, Colors::blue);
		}
	}
	{
		ImGui::PushID("Size");
		ImGui::DragFloat("Width", &mSize.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			changed = true;
			mSize.y = mSize.x * 9 / 16;
		}
		ImGui::PopID();
	}
	if (Space* _Space = mOwner->GetParentSpace())
	{
		glm::vec3 botRightMargin(mCenter.x + mHorizontalMargins.y + mSize.x / 2, mCenter.y - mVerticalMargins.x - mSize.y / 2, 5.0f);
		glm::vec3 topRightMargin(mCenter.x + mHorizontalMargins.y + mSize.x / 2, mCenter.y + mVerticalMargins.y + mSize.y / 2 + mProperties(1.0f, "YOffSet"), 5.0f);
		glm::vec3 botLeftMargin(mCenter.x - mHorizontalMargins.x - mSize.x / 2, mCenter.y - mVerticalMargins.x - mSize.y / 2, 5.0f);
		glm::vec3 topLeftMargin(mCenter.x - mHorizontalMargins.x - mSize.x / 2, mCenter.y + mVerticalMargins.y + mSize.y / 2 + mProperties(1.0f, "YOffSet"), 5.0f);

		_Space->DrawLine(botRightMargin, topRightMargin, Colors::red);
		_Space->DrawLine(botLeftMargin, topLeftMargin, Colors::red);
		_Space->DrawLine(botLeftMargin, botRightMargin, Colors::red);
		_Space->DrawLine(topLeftMargin, topRightMargin, Colors::red);

		_Space->Drawrectangle(mCenter, mSize.x, mSize.y, Colors::green);
		_Space->DrawCircle(mCenter, 5.0f, Colors::red);
	}

	return changed;
}

void TriggerArena::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	mProperties.ToJson(j);
	j["mCenter"].push_back(mCenter.x);
	j["mCenter"].push_back(mCenter.y);
	j["mCenter"].push_back(mCenter.z);

	j["mSize"].push_back(mSize.x);
	j["mSize"].push_back(mSize.y);

	j["mHorizontalMargins"].push_back(mHorizontalMargins.x);
	j["mHorizontalMargins"].push_back(mHorizontalMargins.y);

	j["mVerticalMargins"].push_back(mVerticalMargins.x);
	j["mVerticalMargins"].push_back(mVerticalMargins.y);

	//if (mPlayer)
	//{
	//	j["Player"]["Name"] = mPlayer->GetName();
	//	j["Player"]["UID"] = mPlayer->GetUID();
	//}
	//if(mCamera)
	//{
	//	j["Camera"]["Name"] = mCamera->GetName();
	//	j["Camera"]["UID"] = mCamera->GetUID();
	//}
}

void TriggerArena::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	mProperties.FromJson(j);
	if (j.find("mCenter") != j.end())
	{
		mCenter.x = j["mCenter"][0];
		mCenter.y = j["mCenter"][1];
		mCenter.z = j["mCenter"][2];
	}
	if (j.find("mSize") != j.end())
	{
		mSize.x = j["mSize"][0];
		mSize.y = j["mSize"][1];
	}
	if (j.find("mHorizontalMargins") != j.end())
	{
		mHorizontalMargins.x = j["mHorizontalMargins"][0];
		mHorizontalMargins.y = j["mHorizontalMargins"][1];
	}
	if (j.find("mVerticalMargins") != j.end())
	{
		mVerticalMargins.x = j["mVerticalMargins"][0];
		mVerticalMargins.y = j["mVerticalMargins"][1];
	}
	//if (j.find("Player") != j.end())
	//{
	//	Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
	//	_space->AddPointerToLink(&mPlayer, j["Player"]["UID"], j["Player"]["Name"].get<std::string>());
	//}
	//if (j.find("Camera") != j.end())
	//{
	//	Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
	//	_space->AddPointerToLink(&mCamera, j["Camera"]["UID"], j["Camera"]["Name"].get<std::string>());
	//}
}

void TriggerArena::OnCollisionStarted(GameObject* other)
{
	if (other == mPlayer)
	{
		mLeftBlock->SetEnabled(true);
		mRightBlock->SetEnabled(true);
	}
}

void TriggerArena::OnCollisionPersisted(GameObject* other)
{
}

void TriggerArena::OnCollisionEnded(GameObject* other)
{
}

void TriggerArena::UnlockArena()
{
	auto _Machine = reinterpret_cast<CameraMachine*>( mCamera->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));
	auto _StateIdle = reinterpret_cast<CameraFollowStateIdle*>(_Machine->GetState("CameraFollowStateIdle"));
	_Machine->ChangeState("CameraFollowStateIdle");
	_StateIdle->mFocusSize = glm::vec2{1280, 720};
	_StateIdle->mMaxDistanceX = 125;
	_StateIdle->mMaxDistanceY = 125;

	_StateIdle->mYOffset = 0;
	mLeftBlock->SetEnabled(false);
	mRightBlock->SetEnabled(false);
}
