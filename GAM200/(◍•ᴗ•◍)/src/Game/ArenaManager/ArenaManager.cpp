#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../AssertSystem/AssertSystem.h"
#include "../../PropertySystem/PropertyMap.h"
#include "../../GameObject/GameObject.h"
#include "../../Debug/Console.h"
#include "../../Transform2D/TransformComponent.h"

#include "../Health/Health.h"
#include "TriggerArena/TriggerArena.h"

#include "ArenaManager.h"

ArenaManager::ArenaManager()
{
	SetName("ArenaManager");
}

void ArenaManager::Initialize()
{
	LogicComponent::Initialize();
}
void ArenaManager::Update()
{
	if (mEnemies.size() <= 0)
	{
		mArenaTriggerer->GetComp<TriggerArena>()->UnlockArena();
		mArenaTriggerer->GetParentSpace()->DestroyObject(mArenaTriggerer);
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}
	for (auto& _Enemy : mEnemies)
	{
		auto _Health = _Enemy->GetComp<Health>();
		ASSERT(_Health != nullptr);
		if (_Health->GetHealth() <= 0.0f)
		{
			ConsoleSys->DebugPrint("%s Enemy died",_Enemy->GetName().c_str());
			RemoveEnemy(_Enemy);
		}
	}
}

void ArenaManager::LateUpdate()
{
}

void ArenaManager::Shutdown()
{
	LogicComponent::Shutdown();
}

bool ArenaManager::Edit()
{
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
		{
			IM_ASSERT(payload->DataSize == sizeof(GameObject*));
			GameObject* _Enemy = *((GameObject**)payload->Data);
			if (_Enemy->HasComp<TriggerArena>())
			{
				mArenaTriggerer = _Enemy;
			}
			else if (_Enemy)
			{
				AddEnemy(_Enemy);
			}
		}
		ImGui::EndDragDropTarget();
	}

	ImGui::Text("Arena Triggerer: ");
	if (mArenaTriggerer != nullptr)
	{
		ImGui::SameLine();
		ImGui::Text(mArenaTriggerer->GetName().c_str());
		ASSERT(mArenaTriggerer->HasComp<TransformComponent>());
		Space* _Space = mOwner->GetParentSpace();
		ASSERT(_Space != nullptr);
		TransformComponent* _Transform = mArenaTriggerer->GetComp<TransformComponent>();
		_Space->Drawrectangle(_Transform->GetWorldPosition(), _Transform->GetWorldScale().x, _Transform->GetWorldScale().y, Colors::orange);
	}

	ImGui::Text("List of Enemies");
	for (auto& _enemy : mEnemies)
	{

		Space* _Space = mOwner->GetParentSpace();
		TransformComponent* _Transform = _enemy->GetComp<TransformComponent>();
		if (_Transform)
			_Space->Drawrectangle(_Transform->GetWorldPosition(), _Transform->GetWorldScale().x, _Transform->GetWorldScale().y, Colors::orange);
		ImGui::PushID(&_enemy);
		if (ImGui::TreeNode(_enemy->GetName().c_str()))
		{
			if (ImGui::Button("Remove"))
				RemoveEnemy(_enemy);
			ImGui::TreePop();
		}

		ImGui::PopID();
	}
	return false;
}

void ArenaManager::ToJson(nlohmann::json& j)
{
	nlohmann::json& enemies = j["Enemies"];
	enemies["Count"] = mEnemies.size();
	for (auto& _enemy : mEnemies)
	{
		enemies["EnemyName"].push_back(_enemy->GetName());
		enemies["EnemyUID"].push_back( _enemy->GetUID());
	}
	if (mArenaTriggerer != nullptr)
	{
		nlohmann::json& triggerer = j["Triggerer"];
		triggerer["Name"] = mArenaTriggerer->GetName().c_str();
		triggerer["UID"] = mArenaTriggerer->GetUID();
	}
}

void ArenaManager::FromJson(const nlohmann::json& j)
{
	if (j.find("Enemies") != j.end())
	{
		unsigned count = j["Enemies"]["Count"];
		mEnemies.clear();
		mEnemies.resize(count);
		for (unsigned i = 0; i < count; i++)
		{
			unsigned id = j["Enemies"]["EnemyUID"][i];
			std::string _name = j["Enemies"]["EnemyName"][i].get<std::string>();
			Space * _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
			_space->AddPointerToLink(&mEnemies[i], id, _name);
		}
	}
	if (j.find("Triggerer") != j.end())
	{
		const nlohmann::json& triggerer = j["Triggerer"];
		Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
		_space->AddPointerToLink(&mArenaTriggerer, triggerer["UID"], triggerer["Name"].get<std::string>());
	}
}

GameObject* ArenaManager::AddEnemy(GameObject* newEnemy)
{
	ASSERT(newEnemy != nullptr);
	mEnemies.push_back(newEnemy);
	return newEnemy;
}

void ArenaManager::RemoveEnemy(GameObject* _enemy)
{
	auto to_remove = std::find(mEnemies.begin(), mEnemies.end(), _enemy);
	if (to_remove != mEnemies.end())
	{
		mEnemies.erase(to_remove);
	}
}
