#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"

class GameObject;

class ArenaManager: public LogicComponent
{
	SM_RTTI_DECL(ArenaManager, LogicComponent)
public:
	ArenaManager();

	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	GameObject* AddEnemy(GameObject* newEnemy);
	void RemoveEnemy(GameObject* _enemy);

private:
	std::vector<GameObject*> mEnemies;
	GameObject* mArenaTriggerer;
};