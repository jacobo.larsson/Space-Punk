#include "../PlatformSystem/PlatformSystem.h"

#include "../../Time/Time.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Input/Input.h"
#include "../../Factory/Factory.h"

#include "../PlatformSystem/PlatformSystem.h"
#include "../PlatformSystem//PlatformStates.h"

PlatformMachine::PlatformMachine() {
	mName = "PlatformMachine";

	mInitialState = new PlatformNormal("PlatformNormal");
	mCurrentState = mInitialState;
	AddState(mInitialState);
	AddState(new PlatformGhosted("PlatformGhosted"));
}

PlatformMachine::~PlatformMachine() { StateMachine::~StateMachine(); }

void PlatformMachine::Initialize()
{
	myCollider = mActor->GetComp<Collider>();
	targetCollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>();

	if (myCollider == nullptr) {
		disabled = true;
	}
	StateMachine::Initialize();
}

void PlatformMachine::Update()
{

	if (!disabled)
		StateMachine::Update();
}

void PlatformMachine::Shutdown()
{
	StateMachine::Clear();
}

PlatformMachine& PlatformMachine::operator=(const PlatformMachine& rhs)
{

	return *this;
}

PlatformMachine* PlatformMachine::Clone()
{
	PlatformMachine* temp = FactorySys->Create<PlatformMachine>();
	*temp = *this;
	return temp;
}

void PlatformMachine::ToJson(nlohmann::json& j) { IBase::ToJson(j); }

void PlatformMachine::FromJson(const nlohmann::json& j) { IBase::FromJson(j); }