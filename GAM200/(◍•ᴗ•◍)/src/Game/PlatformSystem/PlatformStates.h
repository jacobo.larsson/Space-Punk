#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../../StateMachines/StateMachine/StateMachine.h"

class PlatformMachine;
class Collider;

class PlatformNormal : public State
{
public:
	PlatformNormal(const char* name = nullptr);

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	PlatformMachine* mCastedMachine = nullptr;
	MovementMachine* mPlayerMovement = nullptr;
};

class PlatformGhosted : public State
{
public:
	PlatformGhosted(const char* name = nullptr);

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
	
	PlatformMachine* mCastedMachine = nullptr;
};