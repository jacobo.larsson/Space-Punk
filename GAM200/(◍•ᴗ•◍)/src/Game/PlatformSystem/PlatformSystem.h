#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/IComp.h"
#include "../../RTTI/SM_RTTI.h"


#include "../../Input/Input.h" //Input for movement

#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"

#include "../../StateMachines/StateMachine/StateMachine.h"


class PlatformMachine : public StateMachine
{
	SM_RTTI_DECL(PlatformMachine, StateMachine);

public:

	PlatformMachine();
	~PlatformMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	PlatformMachine& operator=(const PlatformMachine& rhs);
	PlatformMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	Collider* myCollider;
	Collider* targetCollider;
	float timeSinceLastS = 0.0f;
	bool disabled = false;
};