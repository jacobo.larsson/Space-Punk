#include <iostream>

#include <glm/glm.hpp>

#include "../../Input/Input.h" //Input for movement

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"
#include "../../Collisions/CollisionTable.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../GameInput/GameInput.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "PlatformSystem.h"
#include "PlatformStates.h"



#pragma region PlatformNormal
PlatformNormal::PlatformNormal(const char* name) : State(name)
{
	
}

void PlatformNormal::Initialize()
{
	mCastedMachine = dynamic_cast<PlatformMachine*>(mOwnerStateMachine);
	GameObject* _Player = SceneSys->GetMainSpace()->FindObjectByName("Player");
	if (_Player)
		mPlayerMovement = reinterpret_cast<MovementMachine*>(_Player->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"));

	State::Initialize();
}
void PlatformNormal::LogicEnter()
{
	if(mCastedMachine->myCollider)
		mCastedMachine->myCollider->type_of_object = CollTable->GetCollisionGroupNumberByName("Platform");
}
void PlatformNormal::LogicUpdate()
{
	//we need to make the collider enabled a short delay after we enter the state
	if (mTimeInState > 1.0f && mActor->GetComp<Collider>()->type_of_object == CollTable->GetCollisionGroupNumberByName("PlatformGhost")) 
	{
		if (mCastedMachine->myCollider)
			mCastedMachine->myCollider->type_of_object = CollTable->GetCollisionGroupNumberByName("Platform");
	}
	mCastedMachine->timeSinceLastS += (float)TimeSys->GetFrameTime();

	//if the platform is below the player ignore collission
	if (mCastedMachine->targetCollider->GetCollideBox().GetWorldPosition().y < mCastedMachine->myCollider->GetCollideBox().GetWorldPosition().y)
	{
		mOwnerStateMachine->ChangeState("PlatformGhosted");
	}

	if (mPlayerMovement && mPlayerMovement->GetCurrentStateName() != "PlayerPurification")
	{
		if (GameInput::DownKeyPressed()) 
		{
			mCastedMachine->timeSinceLastS = 0.0f;
			mOwnerStateMachine->ChangeState("PlatformGhosted");
		}
	}
}
void PlatformNormal::LogicExit()
{
}
#pragma endregion


#pragma region PlatformGhosted
PlatformGhosted::PlatformGhosted(const char* name):State(name)
{
	
}

void PlatformGhosted::Initialize()
{
	mCastedMachine = dynamic_cast<PlatformMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void PlatformGhosted::LogicEnter()
{
	if(mCastedMachine->myCollider)
		mCastedMachine->myCollider->type_of_object = CollTable->GetCollisionGroupNumberByName("PlatformGhost");
}
void PlatformGhosted::LogicUpdate()
{
	if (!mCastedMachine->disabled)
	{
		mCastedMachine->timeSinceLastS += (float)TimeSys->GetFrameTime();

		if (mCastedMachine->targetCollider->GetCollideBox().GetWorldPosition().y - mCastedMachine->targetCollider->GetCollideBox().GetWorldScale().y / 2 > mCastedMachine->myCollider->GetCollideBox().GetWorldPosition().y)
		{
			if (mTimeInState > 0.2f && mCastedMachine->timeSinceLastS > 0.4f) 
			{
				mOwnerStateMachine->ChangeState("PlatformNormal");
			}
		}
	}
}
void PlatformGhosted::LogicExit()
{
}

#pragma endregion
