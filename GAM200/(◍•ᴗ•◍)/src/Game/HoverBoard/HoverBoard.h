#pragma once
#include "../../Input/Input.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../../StateMachines/StateMachine/StateMachine.h"


class HoverBoardState : public State
{
public:
	HoverBoardState(const char* name);
	~HoverBoardState();

	void Initialize()override;
	void LogicEnter()override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionPersisted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

	void IdleHoverboard();
	void Respawn();
	void Flicker();
	void RestartFlicker();


	glm::vec2 prevGravity = glm::vec2();

	// Collision variables
	float mForgivenessMaximum = 0.45f;
	float mForgivenessTime = 0.0f;

	// Movement variables
	float FlyingSpeed = 750.0f;
	float RotatingSpeed = 40.0f;
	float MaxRotatingSpeed = 250.0f;
	float RotatingAngle = 0.07f;
	float MaxRotatingAngle = 0.3f;

	bool GoingDown = false;
	bool GoingUp = false;

	// Flickering variables
	bool mRed = true;
	float mTimeSinceLastChange = 0.0f;
	float mChangeFrequency = 0.1f;
	float mMaxFlickerTime = 0.3f;

	// bouncing effect
	bool mRespawn = false;
	bool mBounce = false;
	float mPrevVelocityX = 0.0f;
	float mPrevVelocityY = 0.0f;
	float mBounceEnd = 0.3f;
	float mBounceTime = 0.0f;


	MovementMachine* mCastedMachine;

	GameObject* mHazard = nullptr;

	glm::vec3 mRespawnPosition = glm::vec3();	
	glm::vec2 prevVelocity = glm::vec2();
	glm::vec2 mOriginalColliderScale = glm::vec2{ 0 , 0 };
};
