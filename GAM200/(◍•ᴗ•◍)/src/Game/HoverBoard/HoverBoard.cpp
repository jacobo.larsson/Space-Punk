#include <iostream>
#include <random>

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"

#include "../../GameObject/GameObject.h"

#include "../../Transform2D/TransformComponent.h"
#include "../../Physics/RigidBody.h"
#include "../../Collisions/ICollider.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../PlayerMovement/MovementStates.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../GameInput/GameInput.h"
#include "../../Animation/SM_Animation.h"

#include "../../Archetype/ArchetypeFunctions.h"

#include "HoverBoard.h"

HoverBoardState::HoverBoardState(const char* name) : State(name)
{
}

HoverBoardState::~HoverBoardState()
{
}

void HoverBoardState::Initialize()
{
	mCastedMachine = dynamic_cast<MovementMachine*>(mOwnerStateMachine);
}

void HoverBoardState::LogicEnter()
{
	prevGravity = mCastedMachine->pRigidbody->gravity;
	mOriginalColliderScale = mCastedMachine->pCollider->GetCollideBox().GetWorldScale();
	mCastedMachine->pRigidbody->gravity.y = 0.0f;
	mCastedMachine->pTransform->SetOrientation(0.0f);
	mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard");
	mCastedMachine->mySkelRend->SetFlipX(!(mCastedMachine->FacingRight));
	mCastedMachine->pRigidbody->Velocity.x = mCastedMachine->FacingRight ? FlyingSpeed : -FlyingSpeed;
}


void HoverBoardState::LogicUpdate()
{
	if (mBounce)
	{
		mBounceTime += TimeSys->GetFrameTime();
		Flicker();
		if (mBounceTime > mBounceEnd)
		{
			mBounce = false;
			mBounceTime = 0.0f;

			RestartFlicker();

			mCastedMachine->pRigidbody->Velocity.x = FlyingSpeed;
			mCastedMachine->pRigidbody->Velocity.y = mPrevVelocityY;
			mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard");
			if (mRespawn)
				Respawn();
		}
	}
	// Normal movement
	else
	{
		float _Orientation = mCastedMachine->pTransform->GetWorldOrientation();
		// TODO: Fix rotation when going to the left
		float _ActualRotation = mCastedMachine->FacingRight ? RotatingAngle : -RotatingAngle;
		// The player is going up
		if (GameInput::LeftKeyPressed() || GameInput::UpKeyPressed())
		{
			if (!GoingUp)
				mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard/Nero_Hoverboard_Up_Anticipation");

			GoingUp = true;
			GoingDown = false;

			if (mCastedMachine->pRigidbody->Velocity.y < MaxRotatingSpeed)
				mCastedMachine->pRigidbody->Velocity.y += RotatingSpeed;
			if (_Orientation < MaxRotatingAngle)
			{
				mCastedMachine->pTransform->SetWorldOrientation(_Orientation + _ActualRotation);
				auto _Scale = mCastedMachine->pCollider->GetCollideBox().GetWorldScale();
				// TODO: Adjust this value with the final animation
				mCastedMachine->pCollider->GetCollideBox().SetWorldScale(glm::vec2{ _Scale.x, mOriginalColliderScale.y - 25.0f * sin(_Orientation + _ActualRotation) });
			}
		}
		// The player is going down
		else if (GameInput::RightKeyPressed() || GameInput::DownKeyPressed())
		{
			if (!GoingDown)
				mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard/Nero_Hoverboard_Down_Anticipation");

			GoingUp = false;
			GoingDown = true;

			if (mCastedMachine->pRigidbody->Velocity.y > -MaxRotatingSpeed)
				mCastedMachine->pRigidbody->Velocity.y -= RotatingSpeed;
			if (_Orientation > -MaxRotatingAngle)
			{
				mCastedMachine->pTransform->SetWorldOrientation(_Orientation - _ActualRotation);
				auto _Scale = mCastedMachine->pCollider->GetCollideBox().GetWorldScale();
				// TODO: Adjust this value with the final animation
				mCastedMachine->pCollider->GetCollideBox().SetWorldScale(glm::vec2{ _Scale.x, mOriginalColliderScale.y - 50.0f*sin(_Orientation + _ActualRotation) });
			}
		}
		// Idling
		else
		{
			mCastedMachine->pCollider->GetCollideBox().SetWorldScale(mOriginalColliderScale);
			if (GoingDown)
				mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard/Nero_Hoverboard_Down_Recuperation");
			if (GoingUp)
				mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard/Nero_Hoverboard_Up_Recuperation");

			GoingUp = false;
			GoingDown = false;

			IdleHoverboard();
		}
	}
}

void HoverBoardState::LogicExit()
{

}

void HoverBoardState::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Hazard"))
		mHazard = other;
	if (other->CompareTag("Wall") && !mBounce)
	{
		mBounce = true;
		RestartFlicker();
		mPrevVelocityX = mCastedMachine->pRigidbody->Velocity.x;
		mPrevVelocityY = mCastedMachine->pRigidbody->Velocity.y;
		
		if (mCastedMachine->pRigidbody->Velocity.x < -FLT_EPSILON)
			mCastedMachine->pRigidbody->Velocity.x = 300.0f;
		else
			mCastedMachine->pRigidbody->Velocity.x = -300.0f;

		if (mCastedMachine->pRigidbody->Velocity.y < -FLT_EPSILON)
			mCastedMachine->pRigidbody->Velocity.y = 300.0f;
		else if(mCastedMachine->pRigidbody->Velocity.y > FLT_EPSILON)
			mCastedMachine->pRigidbody->Velocity.y = -300.0f;

		mCastedMachine->mySkelRend->SetCurrentAnimation("Nero_Hoverboard/Nero_Hoverboard_Down_Recuperation");
	}
}

void HoverBoardState::OnCollisionPersisted(GameObject* other)
{
	if (other == mHazard)
	{
		mForgivenessTime += TimeSys->GetFrameTime();
		Flicker();
		if (mForgivenessTime > mForgivenessMaximum)
		{
			mBounce = true;
			mRespawn = true;
			RestartFlicker();
		}
	}
}

void HoverBoardState::OnCollisionEnded(GameObject* other)
{
	if (other == mHazard)
	{
		mHazard = nullptr;
		mForgivenessTime = 0.0f;
		mChangeFrequency = 0.1f;
		mCastedMachine->mySkelRend->SetVisible(true);
	}
}

void HoverBoardState::IdleHoverboard()
{
	if (abs(mCastedMachine->pRigidbody->Velocity.y) > 0.0f)
	{
		if (mCastedMachine->pRigidbody->Velocity.y > 0.0f)
			mCastedMachine->pRigidbody->Velocity.y -= 10;
		else
			mCastedMachine->pRigidbody->Velocity.y += 10;
	}
	float _Orientation = mCastedMachine->pTransform->GetWorldOrientation();

	if (abs(_Orientation) > 0.0f)
	{
		if (_Orientation > 0.0f)
			mCastedMachine->pTransform->SetWorldOrientation(_Orientation - 0.01f);
		else
			mCastedMachine->pTransform->SetWorldOrientation(_Orientation + 0.01f);
	}

}

void HoverBoardState::Respawn()
{
	mRespawn = false;
	mCastedMachine->pRigidbody->gravity = prevGravity;
	mCastedMachine->pRigidbody->Velocity.x = 0.0f;
	mCastedMachine->pTransform->SetWorldPosition(mRespawnPosition);
	mCastedMachine->pTransform->SetWorldOrientation(0.0f);
	mCastedMachine->ChangeState("PlayerIdle");
	mForgivenessTime = 0.0f;
}

void HoverBoardState::Flicker()
{
	mTimeSinceLastChange += TimeSys->GetFrameTime();
	if (mTimeSinceLastChange > mChangeFrequency)
	{
		mChangeFrequency /= 2.0f;
		mTimeSinceLastChange = 0.0f;
		if (mRed)
		{
			//mCastedMachine->mySkelRend->SetModulationColor(Colors::white);
			mCastedMachine->mySkelRend->SetVisible(true);
			mRed = false;
		}
		else
		{
			//mCastedMachine->mySkelRend->SetModulationColor(Colors::red);
			mCastedMachine->mySkelRend->SetVisible(false);
			mRed = true;
		}
	}

}

void HoverBoardState::RestartFlicker()
{
	mTimeSinceLastChange = 0.0f;
	mChangeFrequency = mMaxFlickerTime / 3.0f;
	mCastedMachine->mySkelRend->SetVisible(true);
}
