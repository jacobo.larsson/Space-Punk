#pragma once
#include <glm/glm.hpp>

#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"

class TransformComponent;
class MovementMachine;
class RigidBody;
class GameObject;
class RespawnHoverboard : public LogicComponent
{
	SM_RTTI_DECL(RespawnHoverboard, LogicComponent)
public:
	RespawnHoverboard() { SetName("RespawnHoverboard"); }

	void Initialize()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void OnCollisionStarted(GameObject* other)override;

private:

	glm::vec3 mRespawnPosition = glm::vec3{ 0, 0, 0};
	GameObject* mPlayer = nullptr;
	TransformComponent* mPlayerTransform = nullptr;
	RigidBody* mPlayerRigidBody = nullptr;
	MovementMachine* mMovementMachine = nullptr;
};