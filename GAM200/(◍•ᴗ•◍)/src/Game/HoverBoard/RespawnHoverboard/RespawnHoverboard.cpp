#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"

#include "../../../GameObject/GameObject.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Physics/RigidBody.h"

#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../PlayerMovement/PlayerMovement.h"
#include "../../PlayerMovement/MovementStates.h"
#include "../../Hoverboard/Hoverboard.h"

#include "RespawnHoverboard.h"

void RespawnHoverboard::Initialize()
{
    mPlayer = SceneSys->GetMainSpace()->FindObjectByName("Player");
    mPlayerTransform = mPlayer->GetComp<TransformComponent>();
    mPlayerRigidBody = mPlayer->GetComp<RigidBody>();
    mMovementMachine = reinterpret_cast<MovementMachine*>(mPlayer->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"));
    LogicComponent::Initialize();
}

bool RespawnHoverboard::Edit()
{
    bool changed = false;

    ImGui::Text("Respawn Position");
    ImGui::PushID("Respawn Position");
    ImGui::Columns(2, "Respawn Position", true);
    ImGui::DragFloat("X", &mRespawnPosition.x);

    if (ImGui::IsItemDeactivatedAfterEdit())
        changed = true;

    ImGui::NextColumn();
    ImGui::DragFloat("Y", &mRespawnPosition.y);

    if (ImGui::IsItemDeactivatedAfterEdit())
        changed = true;

    ImGui::Columns(1);
    ImGui::PopID();

    if (Space* _Space = mOwner->GetParentSpace())
    {
        auto _ObjectPosition = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
        _Space->DrawCircle(glm::vec2{ _ObjectPosition + mRespawnPosition }, 2.0f, Colors::green);
    }
    return changed;
}

void RespawnHoverboard::ToJson(nlohmann::json& j)
{
    j["mRespawnPosition"].push_back(mRespawnPosition.x);
    j["mRespawnPosition"].push_back(mRespawnPosition.y);
}

void RespawnHoverboard::FromJson(const nlohmann::json& j)
{
    if (j.find("mRespawnPosition") != j.end())
    {
        mRespawnPosition.x = j["mRespawnPosition"][0];
        mRespawnPosition.y = j["mRespawnPosition"][1];
    }
}

void RespawnHoverboard::OnCollisionStarted(GameObject* other)
{
    //if (other->CompareTag("Player"))
    //{
    //    auto _ObjectPosition = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
    //    auto _FocusPosition = _ObjectPosition + mRespawnPosition;
    //    HoverBoardState* _State = reinterpret_cast<HoverBoardState*>(mMovementMachine->GetState("HoverBoardState"));
    //
    //    mPlayerRigidBody->gravity = _State->prevGravity;
    //    mPlayerRigidBody->Velocity.x = 0.0f;
    //    mPlayerTransform->SetWorldPosition(glm::vec3{ _FocusPosition.x, _FocusPosition.y, 0.0f });
    //    mPlayerTransform->SetWorldOrientation(0.f);
    //    mMovementMachine->ChangeState("PlayerIdle");
    //}
}
