#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Scene/Scene.h"
#include "../../../AssertSystem/AssertSystem.h"
#include "../../../PropertySystem/PropertyMap.h"
#include "../../../GameObject/GameObject.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Collisions/ICollider.h"
#include "../../../Graphics/Renderable/Renderable.h"
#include "../../../Space/Space.h"
#include "../../../Archetype/ArchetypeFunctions.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../HoverBoard/HoverBoard.h"
#include "../../../Animation/SM_Animation.h"

#include "../../CameraFollow/CameraFollow.h"
#include "../../CameraFollow/CameraMachine.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Graphics/Camera/Camera.h"
#include "TriggerHoverBoard.h"

void TriggerHoverBoard::Initialize()
{
	mPlayer = SceneSys->GetMainSpace()->FindObjectByName("Player");
	mCamera = SceneSys->GetMainSpace()->FindObjectByName("Camera");

	//if (mCamera == nullptr || mPlayer == nullptr)
	//{
	//	mOwner->RemoveComp(this);
	//	std::cout << "Camera or player not set on TriggerHoverboard. Owner: " << mOwner->GetName() << " With ID: " << mOwner->GetUID() << std::endl;
	//	ConsoleSys->DebugPrint("Camera or player not set on TriggerHoverboard %s With ID: %i \n", mOwner->GetName(), mOwner->GetUID());
	//	return;
	//}
	mCollider = mOwner->GetComp<Collider>();
	mPlayerTransform = mPlayer->GetComp<TransformComponent>();
	mPlayerRigidBody = mPlayer->GetComp<RigidBody>();
	mMovementMachine = reinterpret_cast<MovementMachine*>( mPlayer->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"));
	LogicComponent::Initialize();
}
bool TriggerHoverBoard::Edit()
{
	bool changed = false;
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
		{
			IM_ASSERT(payload->DataSize == sizeof(GameObject*));
			GameObject* _Camera = *((GameObject**)payload->Data);
			if (_Camera->HasComp<CameraComponent>() && _Camera->HasComp<CameraFollow>())
			{
				mCamera = _Camera;
			}
		}
		ImGui::EndDragDropTarget();
	}
	ImGui::Text("Respawn Position");
	ImGui::PushID("Respawn Position");
	ImGui::Columns(2, "Respawn Position", true);
	ImGui::DragFloat("X", &mRespawnPosition.x);

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("Y", &mRespawnPosition.y);

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::Columns(1);
	ImGui::PopID();

	if (Space* _Space = mOwner->GetParentSpace())
	{
		auto _ObjectPosition = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
		_Space->DrawCircle(glm::vec2{ _ObjectPosition + mRespawnPosition }, 2.0f, Colors::green);
	}

	ImGui::DragFloat("Flying Velocity", &mFlyingSpeed, 1.0f, 0.0f, 1000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::DragFloat("Orientation", &mOrientation, 1.0f, 0.0f, 1000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Checkbox("Turn Hoverboard On", &mActivate);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::Text("Camera: ");
	if (mCamera)
	{
		ImGui::SameLine();
		ImGui::Text(mCamera->GetName().c_str());
	}
	return changed;
}

void TriggerHoverBoard::ToJson(nlohmann::json& j)
{
	if (mCamera)
	{
		j["Camera"]["Name"] = mCamera->GetName();
		j["Camera"]["UID"] = mCamera->GetUID();
	}
	j["mFlyingSpeed"] = mFlyingSpeed;
	j["mMode"] = mActivate;
	j["mOrientation"] = mOrientation;

	j["mRespawnPosition"].push_back(mRespawnPosition.x);
	j["mRespawnPosition"].push_back(mRespawnPosition.y);

}

void TriggerHoverBoard::FromJson(const nlohmann::json& j)
{

	if (j.find("mRespawnPosition") != j.end())
	{
		mRespawnPosition.x = j["mRespawnPosition"][0];
		mRespawnPosition.y = j["mRespawnPosition"][1];
	}

	if (j.find("mFlyingSpeed") != j.end())
		mFlyingSpeed = j["mFlyingSpeed"];
	if (j.find("mOrientation") != j.end())
		mOrientation = j["mOrientation"];
	if (j.find("mMode") != j.end())
		mActivate = j["mMode"];
	if (j.find("Camera") != j.end())
	{
		Space* _space = reinterpret_cast<Space*>(SceneSys->FindObjectById(mOwner->GetSpaceID()));
		_space->AddPointerToLink(&mCamera, j["Camera"]["UID"], j["Camera"]["Name"].get<std::string>());
	}
}

void TriggerHoverBoard::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Player"))
	{
		HoverBoardState* _State = reinterpret_cast<HoverBoardState*>(mMovementMachine->GetState("HoverBoardState"));
		// Activate
		if (mActivate)
		{
			mMovementMachine->ChangeState("PlayerDashingFast");
			mMovementMachine->mHoverboardTransition = true;
			_State->FlyingSpeed = mFlyingSpeed;
			auto _ObjectPosition = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
			_State->mRespawnPosition = _ObjectPosition + mRespawnPosition;
			mPlayerTransform->SetWorldOrientation(mOrientation);
		}
		else if (!mTriggered)
		{
			if (mPlayerRigidBody )
			{
				mPlayerRigidBody->gravity = _State->prevGravity;
				mPlayerRigidBody->Velocity.x = 0.0f;

				mCollider->type_of_object = CollTable->GetCollisionGroupNumberByName("Wall");

				glm::vec3 _Offset = mPlayerTransform->GetWorldPosition();

				if (_Offset.x < mOwner->GetComp<TransformComponent>()->GetWorldPosition().x)
					_Offset.x += 5 * mOwner->GetComp<TransformComponent>()->GetScale().x;
				else
					_Offset.x -= 5 * mOwner->GetComp<TransformComponent>()->GetScale().x;

				mPlayerTransform->SetWorldPosition(_Offset);
				mPlayerTransform->SetWorldOrientation(0.0f);
			}
			mMovementMachine->ChangeState("PlayerDashingFast");
		}
		mTriggered = true;
	}
}

void TriggerHoverBoard::OnCollisionPersisted(GameObject* other)
{
}

void TriggerHoverBoard::OnCollisionEnded(GameObject* other)
{
}
