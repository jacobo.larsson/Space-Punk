#pragma once
#include <glm/glm.hpp>

#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"

class GameObject;
class TransformComponent;
class Collider;
class RigidBody;
class CameraFollow;
class CameraMachine;
class MovementMachine;
class TriggerHoverBoard : public LogicComponent
{
	SM_RTTI_DECL(TriggerHoverBoard, LogicComponent)
public:
	TriggerHoverBoard() { SetName("TriggerHoverBoard"); }

	void Initialize()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionPersisted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

private:

	bool mActivate = false;
	bool mTriggered = false;

	float mFlyingSpeed = 0.0f;
	float mOrientation = 0.0f;

	glm::vec3 mRespawnPosition = glm::vec3{ 0, 0, 0 };

	Collider* mCollider = nullptr;
	GameObject* mCamera = nullptr;
	GameObject* mPlayer = nullptr;
	TransformComponent* mPlayerTransform = nullptr;
	RigidBody* mPlayerRigidBody = nullptr;
	MovementMachine* mMovementMachine = nullptr;
};