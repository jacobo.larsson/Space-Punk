#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../AssertSystem/AssertSystem.h"
		  
#include "../../Archetype/ArchetypeFunctions.h"
#include "../../Factory/Factory.h"
#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Collisions/Collisions.h"
#include "../../PropertySystem/PropertyMap.h"
#include "../../Changer/Changer.h"
#include "../../Input/Input.h"
#include "../../Time/Time.h"
#include "../../Editor/Menus/Menus.h"

#include "Dialog.h"

DialogComponent::DialogComponent()
{
	mName = "DialogComponent";


}
DialogComponent::DialogComponent(DialogComponent& rhs)
{
	timed = rhs.timed;
	time = rhs.time;
	dialogues = rhs.dialogues;

}
DialogComponent::~DialogComponent()
{
	dialogues.clear();
	dialogues_with_gamepad.clear();
}
DialogComponent& DialogComponent::operator=(const DialogComponent& rhs)
{
	timed = rhs.timed;
	time = rhs.time;
	dialogues = rhs.dialogues;
	return *this;

}
DialogComponent* DialogComponent::Clone()
{

	DialogComponent* temp = FactorySys->Create<DialogComponent>();
	(*temp) = (*this);
	return temp;
}

void DialogComponent::OnCreate()
{


}
void DialogComponent::Initialize()
{
	GameObject* object_owner = GetOwner();
	LogicComponent::Initialize();
	player = SceneSys->GetMainSpace()->FindObjectByName("Player");
	owner_transform = object_owner->GetComp<TransformComponent>();
	player_transform = player->GetComp<TransformComponent>();
	text_rend = object_owner->GetComp<TextRenderable>();
	
	
	

	text_rend->mText = "";
	text_rend->mMesh->Clear();
	ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

	// upload the mesh to GPU
	text_rend->mMesh->UploadToGPU();

	//dialog_square = CreateFromArchetype("Dialog_sq",
	//	mOwner->GetParentSpace(), mOwner->GetComp<TransformComponent>()->GetWorldPosition());
	if (dialog_square)
	{
		dialog_square->GetComp<Renderable>()->SetEnabled(false);
		dialog_square->GetComp<TransformComponent>()->SetWorldPosition(glm::vec3(dialog_square->GetComp<TransformComponent>()->GetWorldPosition().x, dialog_square->GetComp<TransformComponent>()->GetWorldPosition().y, 0.f));
	}

	for (int i = 0; i < dialogues.size(); i++)
	{
		if (dialogues[i].second == "")
		{
			dialogues[i].second = dialogues_with_gamepad[i].second;
		}
		else if (dialogues_with_gamepad[i].second == "")
		{
			dialogues_with_gamepad[i].second = dialogues[i].second;
		}
	}

}
void DialogComponent::Update()
{
	if (player)
	{
	
		if (started == true)
		{
			if (started_gamepad == false)
			{
				ShowCurrentDialog();
			}
			else
			{
				ShowCurrentDialogGamepad();
			}

			if (timed == false)
			{
				if ((InputSys->key_is_down(keys::F) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))&& next_dialog == false)
				{
					if (current_dialog < dialogues.size())
					{
						if (started_gamepad == false)
						{
							if (current_letter <= dialogues[current_dialog].second.size())
							{
								current_letter = dialogues[current_dialog].second.size();
							}
							else
							{
								current_dialog++;
								current_letter = 0;
							}
						}
						else
						{
							if (current_letter <= dialogues_with_gamepad[current_dialog].second.size())
							{
								current_letter = dialogues_with_gamepad[current_dialog].second.size();
							}
							else
							{
								current_dialog++;
								current_letter = 0;
							}
						}
					}
					else
					{
						started = false;
						current_dialog = 0;
						current_letter = 0;
						started_gamepad = false;
						text_rend->mText = "";
						text_rend->mMesh->Clear();
						ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

						// upload the mesh to GPU
						text_rend->mMesh->UploadToGPU();

						if (dialog_square)
							dialog_square->GetComp<Renderable>()->SetEnabled(false);
					}
				}
			}
			else
			{
				if (counter > time)
				{
					counter = 0.f;
					if (current_dialog < dialogues.size())
					{
						current_dialog++;
						current_letter = 0;
					}
					else
					{
						started = false;
						current_dialog = 0;
						current_letter = 0;
						started_gamepad = false;
						text_rend->mText = "";
						text_rend->mMesh->Clear();
						ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

						// upload the mesh to GPU
						text_rend->mMesh->UploadToGPU();

						if(dialog_square)
							dialog_square->GetComp<Renderable>()->SetEnabled(false);
					}
				}
			}

		}

		if (owner_transform && player_transform)
		{
			
			if (StaticRectToStaticRect(player_transform->GetWorldPosition(), player_transform->GetScale().x, player_transform->GetScale().y,
				owner_transform->GetWorldPosition(), owner_transform->GetScale().x+50, owner_transform->GetScale().y+50))
			{

				if (started == false)
				{
					if (InputSys->using_gamepad == false)
					{
						if (text_rend->mText != "Press F to talk")
						{
							text_rend->mText = "Press F to talk";
							text_rend->mMesh->Clear();
							ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);


							// upload the mesh to GPU
							text_rend->mMesh->UploadToGPU();

						}
					}
					else
					{
						if (text_rend->mText != "Press Left Shoulder to talk")
						{
							text_rend->mText = "Press Left Shoulder to talk";
							text_rend->mMesh->Clear();
							ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);


							// upload the mesh to GPU
							text_rend->mMesh->UploadToGPU();

						}
					}


					if (InputSys->key_is_down(keys::F))
					{
						started_gamepad = false;

						started = true;
						if (dialog_square)	
							dialog_square->GetComp<Renderable>()->SetEnabled(true);
					}
					else if (InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
					{
						started_gamepad = true;
						started = true;
						if (dialog_square)
							dialog_square->GetComp<Renderable>()->SetEnabled(true);
					}

				}
				

			}
			else
			{
				if (text_rend->mText != "")
				{
					text_rend->mText = "";
					text_rend->mMesh->Clear();
					ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

					// upload the mesh to GPU
					text_rend->mMesh->UploadToGPU();
					started = false;
					started_gamepad = false;
					current_dialog = 0;
					current_letter = 0;
					if (dialog_square)
						dialog_square->GetComp<Renderable>()->SetEnabled(false);
				}

			}
		}

	}

}
void DialogComponent::Shutdown()
{
	

}

void DialogComponent::ShowCurrentDialog()
{

	if (current_dialog < dialogues.size())
	{
		std::string current_str = dialogues[current_dialog].second;
		if (timed == true)
		{
			next_dialog = false;
			if (char_counter > 1.f && current_letter <= dialogues[current_dialog].second.size())
			{


				string_updated = current_str.substr(0, current_letter);
				// save text
				text_rend->mText = string_updated;

				// clear the model
				text_rend->mMesh->Clear();
				ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

				// upload the mesh to GPU
				text_rend->mMesh->UploadToGPU();
				char_counter = 0.f;
				current_letter++;
			}
			else
				char_counter += TimeSys->GetFrameTime() * dialogues[current_dialog].first;

			if (current_letter > dialogues[current_dialog].second.size())
			{
				counter += TimeSys->GetFrameTime();
			}



		}
		else
		{
			next_dialog = false;
			if (char_counter > 1.f && current_letter <= dialogues[current_dialog].second.size())
			{


				string_updated = current_str.substr(0, current_letter);
				// save text
				text_rend->mText = string_updated;

				// clear the model
				text_rend->mMesh->Clear();
				ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

				// upload the mesh to GPU
				text_rend->mMesh->UploadToGPU();
				char_counter = 0.f;
				current_letter++;
			}
			else
				char_counter += TimeSys->GetFrameTime() * dialogues[current_dialog].first;

			if (current_letter > dialogues[current_dialog].second.size())
			{
				if (InputSys->key_is_down(keys::F) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
				{
					current_dialog++;
					current_letter = 0;
					next_dialog = true;
				}
			}
		}
	}
	else
	{
		
			next_dialog = true;
			started = false;
			current_dialog = 0;
			current_letter = 0;
			// clear the model
			text_rend->mText = "";
			started_gamepad = false;
			text_rend->mMesh->Clear();
			ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);
			
			if (dialog_square)
				dialog_square->GetComp<Renderable>()->SetEnabled(false);
			// upload the mesh to GPU
			text_rend->mMesh->UploadToGPU();
		
	}
}

void DialogComponent::ShowCurrentDialogGamepad()
{
	if (current_dialog < dialogues.size())
	{
		std::string current_str = dialogues_with_gamepad[current_dialog].second;
		if (timed == true)
		{
			next_dialog = false;
			if (char_counter > 1.f && current_letter <= dialogues_with_gamepad[current_dialog].second.size())
			{


				string_updated = current_str.substr(0, current_letter);
				// save text
				text_rend->mText = string_updated;

				// clear the model
				text_rend->mMesh->Clear();
				ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

				// upload the mesh to GPU
				text_rend->mMesh->UploadToGPU();
				char_counter = 0.f;
				current_letter++;
			}
			else
				char_counter += TimeSys->GetFrameTime() * dialogues_with_gamepad[current_dialog].first;

			if (current_letter > dialogues_with_gamepad[current_dialog].second.size())
			{
				counter += TimeSys->GetFrameTime();
			}



		}
		else
		{
			next_dialog = false;
			if (char_counter > 1.f && current_letter <= dialogues_with_gamepad[current_dialog].second.size())
			{


				string_updated = current_str.substr(0, current_letter);
				// save text
				text_rend->mText = string_updated;

				// clear the model
				text_rend->mMesh->Clear();
				ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

				// upload the mesh to GPU
				text_rend->mMesh->UploadToGPU();
				char_counter = 0.f;
				current_letter++;
			}
			else
				char_counter += TimeSys->GetFrameTime() * dialogues_with_gamepad[current_dialog].first;

			if (current_letter > dialogues_with_gamepad[current_dialog].second.size())
			{
				if (InputSys->key_is_down(keys::F) || InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
				{
					current_dialog++;
					current_letter = 0;
					next_dialog = true;
				}
			}
		}
	}
	else
	{

		next_dialog = true;
		started = false;
		current_dialog = 0;
		current_letter = 0;
		// clear the model
		started_gamepad = false;
		text_rend->mText = "";
		text_rend->mMesh->Clear();
		ComputeTextMesh(text_rend->mText.c_str(), text_rend->pFontInfo, text_rend->mMesh);

		if (dialog_square)
			dialog_square->GetComp<Renderable>()->SetEnabled(false);
		// upload the mesh to GPU
		text_rend->mMesh->UploadToGPU();

	}
}


void DialogComponent::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	// Write the IBase data (name, enabled, guid)
	IBase::ToJson(j);
	
	j["Total dialogues: "] = dialogues.size();
	if (dialogues.size())
	{
		json& mdialogsJson = j["dialogues keyboard"];
		for (auto& dialogue : dialogues)
		{
			mdialogsJson["speed"].push_back(dialogue.first);
			mdialogsJson["text"].push_back(dialogue.second.c_str());
		}

	}

	if (dialogues_with_gamepad.size())
	{
		json& mdialogsJson = j["dialogues gamepad"];
		for (auto& dialogue : dialogues_with_gamepad)
		{
			mdialogsJson["speed"].push_back(dialogue.first);
			mdialogsJson["text"].push_back(dialogue.second.c_str());
		}

	}

	j["timed"] = timed;

	j["time"] = time;
}
void DialogComponent::FromJson(const nlohmann::json& j)
{
	using nlohmann::json;
	// read from json as IBase (name, uid, enabled)
	IBase::FromJson(j);
	auto dialoguesjson = j.find("dialogues keyboard");
	auto dialoguesjsongamepad = j.find("dialogues gamepad");

	unsigned total_dialogues;

	//Dont touch
	{
		if (j.find("Total dialogues: ") != j.end())
		{
			dialogues.clear();

			total_dialogues = j["Total dialogues: "];
			if (j.find("dialogues keyboard") != j.end())
			{
				for (unsigned i = 0; i < total_dialogues; i++)
				{
					std::pair<int, std::string> newpair;
					int newspeed = 0;
					std::string newstring = "\0";
					newspeed = j["dialogues keyboard"]["speed"][i];
					newstring = j["dialogues keyboard"]["text"][i].get<std::string>();
					newpair = std::make_pair(newspeed, newstring);
					dialogues.push_back(newpair);

				}
			}


			dialogues_with_gamepad.clear();
			if (j.find("dialogues gamepad") != j.end())
			{
				for (unsigned i = 0; i < total_dialogues; i++)
				{
					std::pair<int, std::string> newpair;
					int newspeed = 0;
					std::string newstring = "\0";
					newspeed = j["dialogues gamepad"]["speed"][i];
					newstring = j["dialogues gamepad"]["text"][i].get<std::string>();
					newpair = std::make_pair(newspeed, newstring);
					dialogues_with_gamepad.push_back(newpair);

				}
			}

		}

	}

	if (j.find("timed") != j.end())
		timed = j["timed"];

	if (j.find("time") != j.end())
		time = j["time"];


}


bool DialogComponent::Edit()
{
	bool changed = false;
	
	ImGui::Checkbox("Timed dialog", &timed);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	if (timed)
	{
		ImGui::SliderFloat("Dialog time", &time, 0.f, 100.f);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
	}



	if (ImGui::Button("Add Dialogue"))
	{
		AddDialogue();
		changed = true;
	}

	if (ImGui::Begin("Dialogues menu", nullptr, 0))
	{
		std::vector<std::pair<int, std::string>> to_erase;
		
		auto it = dialogues.begin();
		auto et = dialogues_with_gamepad.begin();
		while (it!= dialogues.end())
		{
			bool add = true;
			ImGui::PushID(&(it));
			ImGui::PushID(&(et));
			
			int distance = std::distance(dialogues.begin(), it);
			std::string to_convert = "Dialog number " + std::to_string(distance);
			
			
			if (ImGui::CollapsingHeader(to_convert.c_str()))
			{
				if (ImGui::Begin("Dialogues edit keyboard", nullptr, 0))
				{

					ImGui::Text(it->second.c_str());

					ImGui::Text("Edit it keyboard");

					ImGui::InputTextMultiline("Keyboard edit", temp, 500);
					if (ImGui::Button("Apply keyboard"))
					{

						it->second = temp;
						std::fill_n(temp, 500, 0);
						changed = true;
					}


					ImGui::Text(et->second.c_str());

					ImGui::Text("Edit it gamepad");

					ImGui::InputTextMultiline("Gamepad edit", temp2, 500);
					if (ImGui::Button("Apply gamepad"))
					{

						et->second = temp2;
						std::fill_n(temp2, 500, 0);
						changed = true;
					}


					ImGui::SliderInt("Dialog speed", &it->first, 0, 500);
					if (ImGui::IsItemDeactivatedAfterEdit())
						changed = true;

					if (ImGui::Button("Delete"))
					{
						auto ot = it;
						auto ut = et;
						ot++;
						ut++;
						if (ot == dialogues.end())
						{
							add = false;
							it = dialogues.erase(it);
							it = dialogues.end();
						}
						else
							it = dialogues.erase(it);

						if (ut == dialogues_with_gamepad.end())
						{
							add = false;
							et = dialogues_with_gamepad.erase(et);
							et = dialogues_with_gamepad.end();
						}
						else
							et = dialogues_with_gamepad.erase(et);

						changed = true;

					}

					ImGui::End();
				}
				else
					ImGui::End();
			}

			ImGui::PopID();
			ImGui::PopID();
			
			if (add)
			{
				it++;
				et++;
			}
		}

		ImGui::End();
	}
	else
		ImGui::End();
	return changed;

}

void DialogComponent::AddDialogue()
{
	int basic_speed = 5;
	std::pair<int, std::string> newpair = std::make_pair(basic_speed, "\0");
	dialogues.push_back(newpair);
	dialogues_with_gamepad.push_back(newpair);
}
