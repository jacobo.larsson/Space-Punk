#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IComp.h"
#include "../../EventSystem/EventHandler.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Graphics/Font/Font.h"

class DialogComponent : public LogicComponent
{
	SM_RTTI_DECL(DialogComponent, IComp);
public:

	DialogComponent();
	DialogComponent(DialogComponent& rhs);
	~DialogComponent();
	DialogComponent& operator=(const DialogComponent& rhs);
	DialogComponent* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ShowCurrentDialog();
	void ShowCurrentDialogGamepad();


	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;

private:
	void AddDialogue();
	std::vector<std::pair<int,std::string>> dialogues;
	std::vector<std::pair<int,std::string>> dialogues_with_gamepad;
	bool started_gamepad = false;
	char temp[500] ="\0";
	char temp2[500] ="\0";
	int current_dialog=0;
	bool timed = false;
	float time = 0.f;
	float counter = 0.f;
	float char_counter = 0.f;
	int current_letter = 0;
	bool started = false;
	bool next_dialog=false;
	std::string string_updated;
	GameObject* player;
	TransformComponent* owner_transform;
	TransformComponent* player_transform;
	TextRenderable* text_rend;
	GameObject* dialog_square;
};