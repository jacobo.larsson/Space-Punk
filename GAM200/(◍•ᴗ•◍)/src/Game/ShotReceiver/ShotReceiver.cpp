#include "ShotReceiver.h"

ShotReceiver::ShotReceiver()
{
	SetName("ShotReceiver");
}

void ShotReceiver::SetShot(bool _State)
{
	mImShot = _State;
}

bool ShotReceiver::AmIShot()
{
	return mImShot;
}
