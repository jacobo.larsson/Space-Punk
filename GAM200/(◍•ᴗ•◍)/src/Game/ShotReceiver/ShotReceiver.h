#pragma once
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"

class ShotReceiver : public LogicComponent
{
	SM_RTTI_DECL(ShotReceiver, LogicComponent);
public:
	ShotReceiver();

	void SetShot(bool _State);
	bool AmIShot();
private:
	bool mImShot = false;
};