#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"


class FlyingEnemyMachine;

class FlyingEnemyDie : public State
{
public:
	FlyingEnemyDie(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit() override;

	FlyingEnemyMachine* castedMachine;
	glm::vec3 death_target;
	glm::vec3 original_pos;
	bool mDeleted = false;
};

class FlyingEnemyReset : public State
{
public:
	FlyingEnemyReset(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	FlyingEnemyMachine* castedMachine;
	float ResetTime = 0.3f;

};

class FlyingEnemyFalling : public State
{
public:
	FlyingEnemyFalling(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
	float ResetTime = 0.3f;
};


//////////////NEW//////////////////////////////////////////////

class FlyingEnemyPursue : public State
{
public:
	FlyingEnemyPursue(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
};


class FlyingEnemyAim : public State
{
public:
	FlyingEnemyAim(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
};



class FlyingEnemyDart : public State
{
public:
	FlyingEnemyDart(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
	float angle_to_change = 0.f;
	bool to_add = false;
};


class FlyingEnemyNewIddle : public State
{
public:
	FlyingEnemyNewIddle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
};


class FlyingEnemyReturn : public State
{
public:
	FlyingEnemyReturn(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;

	void OnCollisionStarted(GameObject* other) override;
	FlyingEnemyMachine* castedMachine;
	bool to_add = false;
	bool already_damaged = false;
};