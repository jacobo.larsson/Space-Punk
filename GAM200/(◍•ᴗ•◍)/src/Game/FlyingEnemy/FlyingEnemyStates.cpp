#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Input/Input.h"
#include <cstdlib>
#include "../../Game/Health/Health.h"

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include "../Health/Health.h"
#include "../../Settings/Settings.h"
#include "../../Collisions/Collisions.h"
#include "../../Collisions/ICollider.h"
#include "../Hitbox/Hitbox.h"

#include "FlyingEnemySystem.h"
#include "FlyingEnemyStates.h"
#include "./FlockComp/FlockComp.h"
#include "../../Time/Time.h"

#pragma region FlyingEnemyDie

FlyingEnemyDie::FlyingEnemyDie(const char* name) : State(name)
{}

void FlyingEnemyDie::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}

void FlyingEnemyDie::LogicEnter()
{
	mTimeInState = 0;
	TransformComponent& pcollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();
	if (pcollider.GetWorldPosition().x < castedMachine->mOwnerTransform->GetWorldPosition().x)
	{
		float multiplier =250.f;
		int angle = rand() % (60 - 15 + 1) + 15;
		float rad_angle = (float)((angle * 3.141592653589793238f)/180.f);
		glm::vec3 initial_target = glm::vec3(1.f,1.f, castedMachine->mOwnerTransform->GetWorldPosition().z);
		initial_target.x = initial_target.x * cos(rad_angle) * multiplier;
		initial_target.y = initial_target.y * sin(rad_angle) * multiplier;
		death_target = castedMachine->mOwnerTransform->GetWorldPosition()+ initial_target;
	}
	else
	{
		float multiplier =250.f;
		int angle = rand() % (60 - 15 + 1) + 15;
		float rad_angle = (float)((angle * 3.141592653589793238f) / 180.f);
		glm::vec3 initial_target = glm::vec3(-1.f, 1.f, castedMachine->mOwnerTransform->GetWorldPosition().z);
		initial_target.x = initial_target.x * cos(rad_angle) * multiplier;
		initial_target.y = initial_target.y * sin(rad_angle) * multiplier;
		death_target = castedMachine->mOwnerTransform->GetWorldPosition()+ initial_target;
	}
}

void FlyingEnemyDie::LogicUpdate()
{
	if (mTimeInState > 0.5f && !mDeleted)
	{
		auto it = std::find(castedMachine->owner_flock->birds.begin(), castedMachine->owner_flock->birds.end(), mActor);

		if (it != castedMachine->owner_flock->birds.end())
		{

			castedMachine->owner_flock->give_turn_to_attack = true;
			castedMachine->owner_flock->death_bird = true;
			castedMachine->owner_flock->birds.erase(it);
			castedMachine->owner_flock->number_of_birds--;
			SceneSys->GetMainSpace()->DestroyObject(mActor);
			mDeleted = true;

		}
	}
	else
	{
		glm::vec3 newpos = glm::mix(castedMachine->mOwnerTransform->GetWorldPosition(), death_target,0.1f);
		castedMachine->mOwnerTransform->SetWorldPosition(newpos);
		castedMachine->mOwnerTransform->SetWorldOrientation(castedMachine->mOwnerTransform->GetWorldOrientation() + 100.f);
		
	}
}

void FlyingEnemyDie::LogicExit()
{
}


#pragma endregion

#pragma region FlyingEnemyReset

FlyingEnemyReset::FlyingEnemyReset(const char* name) : State(name)
{
}

void FlyingEnemyReset::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}

void FlyingEnemyReset::LogicEnter()
{
	mTimeInState = 0;
	castedMachine->mHealth->Damage(100);

	if (castedMachine->mHealth->GetHealth() <= 0)
		mOwnerStateMachine->ChangeState("FlyingEnemyFalling");
	else
		castedMachine->ChangeState("FlyingEnemyDecide");
}

void FlyingEnemyReset::LogicUpdate()
{
}

void FlyingEnemyReset::LogicExit()
{
}

#pragma endregion


#pragma region FlyingEnemyReturn

FlyingEnemyFalling::FlyingEnemyFalling(const char* name) : State(name)
{
}

void FlyingEnemyFalling::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}
void FlyingEnemyFalling::LogicEnter()
{
	mTimeInState = 0;
	RigidBody* temp = mActor->GetComp<RigidBody>();
	if (temp)
	{
		temp->InvMass = 1.0f;
		temp->gravity.y = -1200.0f;
	}
}
void FlyingEnemyFalling::LogicUpdate()
{}

void FlyingEnemyFalling::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Floor") || other->CompareTag("Platform"))
	{
		RigidBody* temp = mActor->GetComp<RigidBody>();
		if (temp)
		{
			temp->InvMass = 0.0f;
			temp->gravity.y = 0.0f;
		}
		int randFactor = rand() % 100;
		if (randFactor < 50)
			mOwnerStateMachine->ChangeState("FlyingEnemyDie");
		else
			mOwnerStateMachine->ChangeState("PurificationState");
	}
}

#pragma endregion


//////////////NEW//////////////////////////////////////////////

#pragma region FlyingEnemyPursue

FlyingEnemyPursue::FlyingEnemyPursue(const char* name) : State(name)
{
}

void FlyingEnemyPursue::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}
void FlyingEnemyPursue::LogicEnter()
{
	mTimeInState = 0;
}
void FlyingEnemyPursue::LogicUpdate()
{

	//Update to minimun distance

	glm::vec3 newpos = glm::mix(castedMachine->mOwnerTransform->GetWorldPosition(), castedMachine->pTransform->GetWorldPosition(), 0.5f);
	castedMachine->mOwnerTransform->SetWorldPosition(newpos);

	if (mTimeInState > 3.f)
	{

		mOwnerStateMachine->ChangeState("FlyingEnemyAim");

	}
}

void FlyingEnemyPursue::OnCollisionStarted(GameObject* other)
{

}

#pragma endregion


#pragma region FlyingEnemyAim

FlyingEnemyAim::FlyingEnemyAim(const char* name) : State(name)
{
}

void FlyingEnemyAim::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}
void FlyingEnemyAim::LogicEnter()
{
	mTimeInState = 0;
}
void FlyingEnemyAim::LogicUpdate()
{}

void FlyingEnemyAim::OnCollisionStarted(GameObject* other)
{

}

#pragma endregion


#pragma region FlyingEnemyDart

FlyingEnemyDart::FlyingEnemyDart(const char* name) : State(name)
{

}

void FlyingEnemyDart::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}
void FlyingEnemyDart::LogicEnter()
{
	mTimeInState = 0;
	TransformComponent& pcollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();
	castedMachine->initialpos = castedMachine->mOwnerTransform->GetWorldPosition();
	if (castedMachine->mOwnerTransform->GetWorldPosition().x < castedMachine->pTransform->GetWorldPosition().x)
	{
		if(mActor->GetComp<Renderable>()->IsFlipX()==false)
			mActor->GetComp<Renderable>()->SetFlipX(true);
		to_add = true;
		glm::vec3 dist = castedMachine->mOwnerTransform->GetWorldPosition() - pcollider.GetWorldPosition();
		castedMachine->radius = sqrt(dist.x * dist.x + dist.y * dist.y) / 2.f;
		castedMachine->circle_center = pcollider.GetWorldPosition() + glm::vec3(dist.x / 2.f, dist.y / 2.f, 0);
		dist.x = dist.x * -1.f;
		float angle = atan(dist.y / dist.x);
		castedMachine->initial_angle = angle + 2 * (3.14159265358979 / 2.f - angle);
		angle_to_change = castedMachine->initial_angle;

	}
	else
	{
		if (mActor->GetComp<Renderable>()->IsFlipX()==true)
			mActor->GetComp<Renderable>()->SetFlipX(false);
		to_add = false;


		glm::vec3 dist = castedMachine->mOwnerTransform->GetWorldPosition() - pcollider.GetWorldPosition();
		castedMachine->radius = sqrt(dist.x * dist.x + dist.y * dist.y) / 2.f;

		castedMachine->circle_center = pcollider.GetWorldPosition() + glm::vec3(dist.x / 2.f, dist.y / 2.f, 0);
		float angle = atan(dist.y / dist.x);
		castedMachine->initial_angle = angle;
		angle_to_change = castedMachine->initial_angle;
	}

	castedMachine->player_target = pcollider.GetWorldPosition();
	castedMachine->stepsize = (2.f * 3.14159265358979f) / 150.f;
}
void FlyingEnemyDart::LogicUpdate()
{
	SceneSys->GetMainSpace()->DrawLine(castedMachine->pTransform->GetWorldPosition(), castedMachine->circle_center, Colors::white);
	SceneSys->GetMainSpace()->DrawLine(castedMachine->pTransform->GetWorldPosition(), castedMachine->initialpos, Colors::red);
	float xpos = castedMachine->radius * cos(angle_to_change) + castedMachine->circle_center.x;
	float ypos = castedMachine->radius * sin(angle_to_change) + castedMachine->circle_center.y;
	castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(xpos, ypos, castedMachine->circle_center.z));

	if (to_add)
		angle_to_change += castedMachine->stepsize;
	else
		angle_to_change -= castedMachine->stepsize;

	glm::vec3 dist = castedMachine->mOwnerTransform->GetWorldPosition() - castedMachine->player_target;
	float rad = sqrt(dist.x * dist.x + dist.y * dist.y);

	if (rad <= 5.f)
	{
		castedMachine->final_angle = angle_to_change;
		castedMachine->damaged = false;
		castedMachine->ChangeState("FlyingEnemyReturn");
		mActor->GetComp<Renderable>()->SetFlipX(!mActor->GetComp<Renderable>()->IsFlipX());
	}

}

void FlyingEnemyDart::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Hitbox"))
	{
		castedMachine->final_angle = angle_to_change;
		castedMachine->damaged = true;
		castedMachine->ChangeState("FlyingEnemyReturn");

		if (Hitbox* hb = other->GetComp<Hitbox>())
		{
			if (!hb->mEnemy)
			{
				float to_do = hb->mDamage;
				mActor->GetComp<Health>()->Damage(to_do);

				if (mActor->GetComp<Health>()->GetHealth() <= 0.0f)
				{
					castedMachine->ChangeState("FlyingEnemyDie");

				}

			}

		}
	}
}

#pragma endregion


#pragma region FlyingEnemyNewIddle

FlyingEnemyNewIddle::FlyingEnemyNewIddle(const char* name) : State(name)
{
}

void FlyingEnemyNewIddle::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);



}
void FlyingEnemyNewIddle::LogicEnter()
{
	
}
void FlyingEnemyNewIddle::LogicUpdate()
{
	static float newcounter = 0.f;
	if (castedMachine->owner_flock)
	{
		glm::vec3 newpos = castedMachine->mOwnerTransform->GetWorldPosition();
		newpos.y = castedMachine->prevy;
		glm::vec3 movement = glm::mix(newpos, castedMachine->target_pos, castedMachine->speed);

		castedMachine->prevy = movement.y;
		movement.y = movement.y + sin(newcounter * 0.5) * 10;



		castedMachine->mOwnerTransform->SetWorldPosition(movement);

		float to_comp = abs(castedMachine->target_pos.x - castedMachine->mOwnerTransform->GetWorldPosition().x);
		if (to_comp <= 10.f)
		{

			TransformComponent* flock_trans = castedMachine->owner_flock->GetOwner()->GetComp<TransformComponent>();


			castedMachine->target_pos.z = flock_trans->GetWorldPosition().z;
			castedMachine->prevy = castedMachine->mOwnerTransform->GetWorldPosition().y;
			if (castedMachine->move_right)
			{
				mActor->GetComp<Renderable>()->SetFlipX(true);
				castedMachine->target_pos.x = flock_trans->GetWorldPosition().x + castedMachine->owner_flock->width_range / 2.f;
				castedMachine->move_right = false;
			}
			else
			{
				mActor->GetComp<Renderable>()->SetFlipX(false);
				castedMachine->target_pos.x = flock_trans->GetWorldPosition().x - castedMachine->owner_flock->width_range / 2.f;
				castedMachine->move_right = true;
			}

			float newheight = (float)(rand() % (int)castedMachine->owner_flock->height_range);
			newheight = flock_trans->GetWorldPosition().y + (newheight - castedMachine->owner_flock->height_range / 2.f);
			castedMachine->target_pos.y = newheight;

			castedMachine->amplitude = (float)(rand() % 9 + 1);

			castedMachine->travel_speed = (float)(rand() % 4 + 1);

			castedMachine->speed = ((float)rand() / RAND_MAX) * (0.04f - 0.01f) + 0.01f;

		}


		if (castedMachine->turn_to_attack)
		{
			castedMachine->ChangeState("FlyingEnemyDart");

		}
	}
	newcounter += 0.016f;
}

void FlyingEnemyNewIddle::OnCollisionStarted(GameObject* other)
{

}

#pragma endregion


#pragma region FlyingEnemyReturn
FlyingEnemyReturn::FlyingEnemyReturn(const char* name) : State(name)
{
}

void FlyingEnemyReturn::Initialize()
{
	State::Initialize();
	castedMachine = dynamic_cast<FlyingEnemyMachine*>(mOwnerStateMachine);
}

void FlyingEnemyReturn::LogicEnter()
{
	mTimeInState = 0;

	if (castedMachine->damaged)
	{
		TransformComponent& pcollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();

		if (pcollider.GetWorldPosition().x > castedMachine->mOwnerTransform->GetWorldPosition().x)
		{
			to_add = false;
		}
		else
		{
			to_add = true;
		}

		already_damaged = true;
	}
	else
	{
		if (castedMachine->initial_angle > 3.14159265358979f / 2.f)
		{
			to_add = true;

		}
		else
		{
			to_add = false;
		}
		already_damaged = false;
	}
}

void FlyingEnemyReturn::LogicUpdate()
{
	if (mTimeInState > 1.f || castedMachine->damaged == true)
	{
		float xpos = castedMachine->radius * cos(castedMachine->final_angle) + castedMachine->circle_center.x;
		float ypos = castedMachine->radius * sin(castedMachine->final_angle) + castedMachine->circle_center.y;
		castedMachine->mOwnerTransform->SetWorldPosition(glm::vec3(xpos, ypos, castedMachine->circle_center.z));

		if (to_add)
			castedMachine->final_angle += castedMachine->stepsize;
		else
			castedMachine->final_angle -= castedMachine->stepsize;

		glm::vec3 dist = castedMachine->mOwnerTransform->GetWorldPosition() - castedMachine->initialpos;
		float rad = sqrt(dist.x * dist.x + dist.y * dist.y);

		if (rad <= 5.f)
		{
			TransformComponent& pcollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();
			castedMachine->initialpos = castedMachine->mOwnerTransform->GetWorldPosition();
			if (castedMachine->target_pos.x < castedMachine->mOwnerTransform->GetWorldPosition().x)
			{
				if (mActor->GetComp<Renderable>()->IsFlipX() == true)
					mActor->GetComp<Renderable>()->SetFlipX(false);
			}
			else
			{
				if (mActor->GetComp<Renderable>()->IsFlipX() == false)
					mActor->GetComp<Renderable>()->SetFlipX(true);
			}

			castedMachine->ChangeState("FlyingEnemyNewIddle");
			castedMachine->damaged == false;
			castedMachine->owner_flock->give_turn_to_attack = true;
			castedMachine->turn_to_attack = false;
		}
	}
}

void FlyingEnemyReturn::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Hitbox"))
	{
		TransformComponent& pcollider = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();
		castedMachine->damaged = true;

		if (mTimeInState < 1.f && already_damaged == false)
		{
			if (pcollider.GetWorldPosition().x > castedMachine->mOwnerTransform->GetWorldPosition().x)
			{
				to_add = false;
			}
			else
			{
				to_add = true;
			}

			already_damaged = true;
		}

		if (Hitbox* hb = other->GetComp<Hitbox>())
		{
			if (!hb->mEnemy)
			{
				float to_do = hb->mDamage;
				mActor->GetComp<Health>()->Damage(to_do);

				
				if (mActor->GetComp<Health>()->GetHealth() <= 0.0f)
				{
					castedMachine->ChangeState("FlyingEnemyDie");
				}
			}

		}

	}

}
#pragma endregion