#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/IComp.h"
#include "../../RTTI/SM_RTTI.h"


#include "../../Input/Input.h" //Input for movement

#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../FlyingEnemy/FlockComp/FlockComp.h"

class Health;

class FlyingEnemyMachine : public StateMachine
{
	SM_RTTI_DECL(FlyingEnemyMachine, StateMachine);

public:
	friend class FlockComp;

	FlyingEnemyMachine();
	~FlyingEnemyMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	FlyingEnemyMachine& operator=(const FlyingEnemyMachine& rhs);
	FlyingEnemyMachine* Clone()override;

	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;


	bool SeesPlayer();


	bool CheckIfPlayerInside();
	void Decide();
	void Move();
	void Charge();
	void ReturnToPoint();

	TransformComponent* pTransform;
	TransformComponent* mOwnerTransform;
	RigidBody* mOwnerRigidBody;



	Health* mHealth = NULL;


	//New

	bool turn_to_move = false;
	bool turn_to_attack = false;
	bool move_right = false;
	glm::vec3 target_pos;
	float prevy = 0.f;
	FlockComp* owner_flock;
	float travel_speed = 0.5f;
	float final_angle = 0.f;
	float amplitude = 10.f;
	float speed = 1.f;
	float initial_angle;
	glm::vec3 player_target;
	float stepsize = 0.f;
	float radius = 0.f;
	glm::vec3 circle_center;
	glm::vec3 initialpos;
	bool damaged = false;
};