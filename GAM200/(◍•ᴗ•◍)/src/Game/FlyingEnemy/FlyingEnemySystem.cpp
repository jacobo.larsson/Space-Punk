#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Time/Time.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Input/Input.h"
#include "../../Factory/Factory.h" 
#include "../../Physics/RigidBody.h"
#include "../../Game/Purification/Purification.h"

#include "../Health/Health.h"

#include "../FlyingEnemy/FlyingEnemySystem.h"
#include "../FlyingEnemy/FlyingEnemyStates.h"



FlyingEnemyMachine::FlyingEnemyMachine()
{
	mName = "FlyingEnemyMachine";
	mInitialState = new FlyingEnemyNewIddle("FlyingEnemyNewIddle");
	mCurrentState = mInitialState;
	AddState(mInitialState);
	AddState(new FlyingEnemyDart("FlyingEnemyDart"));
	AddState(new FlyingEnemyReturn("FlyingEnemyReturn"));
	AddState(new FlyingEnemyDie("FlyingEnemyDie"));
}

FlyingEnemyMachine::~FlyingEnemyMachine() {/* StateMachine::~StateMachine(); */}

void FlyingEnemyMachine::Initialize()
{
	mOwnerTransform = mActor->GetComp<TransformComponent>();
	if (mOwnerTransform == nullptr)
	{
		std::cout << mActor->GetName() << " missing  transform component" << std::endl;
		return;
	}
	//mOwnerTransform->GetWorldPosition().z = 4.0f;

	glm::vec3 new_pos(mOwnerTransform->GetWorldPosition().x, mOwnerTransform->GetWorldPosition().y, 0);

	mOwnerTransform->SetWorldPosition(new_pos);
	mOwnerRigidBody = mActor->GetComp<RigidBody>();

	if (mOwnerTransform == nullptr)
	{
		std::cout << mActor->GetName() << " missing  transform component" << std::endl;
		return;
	}
	if (SceneSys->GetMainSpace()->FindObjectByName("Player") != nullptr)
		pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	if (pTransform == nullptr)
	{
		std::cout << "Player missing  transform component" << std::endl;
		return;
	}

	mActor->GetComp<Collider>()->SetIsIgnored(true);
	mHealth = mActor->GetComp<Health>();

	StateMachine::Initialize();
}

void FlyingEnemyMachine::Update()
{
	StateMachine::Update();
}

void FlyingEnemyMachine::Shutdown()
{
	StateMachine::Shutdown();
}

void FlyingEnemyMachine::OnCollisionStarted(GameObject* other)
{
	/*if (other->CompareTag("Hitbox"))
	{
		if(mCurrentState->GetName()!="PurificationState" && mCurrentState->GetName() != "FlyingEnemyFalling")
			ChangeState("FlyingEnemyReset");
	}*/
	StateMachine::OnCollisionStarted(other);
}
bool FlyingEnemyMachine::SeesPlayer()
{
	//return (glm::distance(mOwnerTransform->GetWorldPosition(), pTransform->GetWorldPosition()) <= RangeOfSight);
	return false;
}
FlyingEnemyMachine& FlyingEnemyMachine::operator=(const FlyingEnemyMachine& rhs)
{
	//To be done

	return *this;
}

FlyingEnemyMachine* FlyingEnemyMachine::Clone()
{
	FlyingEnemyMachine* temp = FactorySys->Create<FlyingEnemyMachine>();
	*temp = *this;
	return temp;
}

bool FlyingEnemyMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		/*ImGui::DragFloat("MaxSpeed", &MaxSpeed);
		ImGui::DragFloat("Acceleration", &Acceleration);
		ImGui::DragFloat("Flying Area(X)", &FlyingXArea);
		ImGui::DragFloat("Flying Area(Y)", &FlyingYArea);
		ImGui::DragFloat("Range Of Sight", &RangeOfSight);*/
		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	return false;
}

void FlyingEnemyMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	/*j["MaxSpeed"] = MaxSpeed;
	j["Acceleration"] = Acceleration;
	j["Flying Area(X)"] = FlyingXArea;
	j["Flying Area(Y)"] = FlyingYArea;*/
}

void FlyingEnemyMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);


	/*if (j.find("MaxSpeed") != j.end())
		MaxSpeed = j["MaxSpeed"];
	if (j.find("Acceleration") != j.end())
		Acceleration = j["Acceleration"];
	if (j.find("Flying Area(X)") != j.end())
		FlyingXArea = j["Flying Area(X)"];
	if (j.find("Flying Area(Y)") != j.end())
		FlyingYArea = j["Flying Area(Y)"];*/
}

bool FlyingEnemyMachine::CheckIfPlayerInside()
{
	//Check if the player is inside the area
	/*if (pTransform->GetWorldPosition().x < MaxXPoint && pTransform->GetWorldPosition().x > MinXPoint)
	{
		if (pTransform->GetWorldPosition().y < MaxYPoint && pTransform->GetWorldPosition().y > MinYPoint)
		{
			return true;
		}
	}*/
	return false;
}

void FlyingEnemyMachine::Decide()
{
	//decide the next and nextx2 points to go
	//Target.x = rand() % (int)(MaxXPoint - MinXPoint + 1) + MinXPoint;
	//Target.y = rand() % (int)(MaxYPoint - MinYPoint + 1) + MinYPoint;
}

void FlyingEnemyMachine::Move()
{
	//normalize the direction
	/*Direction = glm::normalize(Direction);
	mOwnerTransform->SetWorldPosition(mOwnerTransform->GetWorldPosition() + glm::vec3(Direction.x * Velocity * TimeSys->GetFrameTime(),
																					  Direction.y * Velocity * TimeSys->GetFrameTime(),
																					  0)) ;*/
}

void FlyingEnemyMachine::Charge()
{
	//normalize the direction
	/*Direction = glm::normalize(Direction);
	mOwnerTransform->SetWorldPosition(mOwnerTransform->GetWorldPosition() + glm::vec3(Direction.x * chargeVelocity * TimeSys->GetFrameTime(),
		Direction.y * chargeVelocity * TimeSys->GetFrameTime(),
		0));*/
}

void FlyingEnemyMachine::ReturnToPoint()
{
	/*Direction = glm::normalize(Direction);
	mOwnerTransform->SetWorldPosition(mOwnerTransform->GetWorldPosition() - glm::vec3(Direction.x * Velocity * 2 * TimeSys->GetFrameTime(),
		Direction.y * Velocity * 2 * TimeSys->GetFrameTime(),
		0));*/
}

