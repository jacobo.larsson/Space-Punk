#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../../AssertSystem/AssertSystem.h"

#include "../../../Factory/Factory.h"
#include "../../../PropertySystem/PropertyMap.h"
#include "../../../Changer/Changer.h"
#include "../../../Input/Input.h"
#include "../../../Graphics/RenderManager/RenderManager.h"
#include "../../../Audio/SoundEmitter.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Collisions/Collisions.h"
#include "../../../Scene/Scene.h"
#include "../../../MainEngine/MainEngine.h"
#include "FlockComp.h"


FlockComp::FlockComp()
{
	mName = "FlockComp";
	transform = nullptr;
	width_range = 0.f;;
	height_range = 0.f;
	number_of_birds = 0;
	current_turn_to_attack = 0;
	give_turn_to_attack = true;
	current_turn_to_move = 0;
	give_turn_to_move =true ;
	detect_radius =0.f;
	minimun_distance =0.f;
}
FlockComp::FlockComp(FlockComp& rhs)
{
	width_range = rhs.width_range;
	height_range = rhs.height_range;
	number_of_birds = rhs.number_of_birds;
	current_turn_to_attack = rhs.current_turn_to_attack;
	give_turn_to_attack = rhs.give_turn_to_attack;
	current_turn_to_move = rhs.current_turn_to_move;
	give_turn_to_move = rhs.give_turn_to_move;
	detect_radius = rhs.detect_radius;
	minimun_distance = rhs.minimun_distance;
}
FlockComp::~FlockComp()
{
	birds.clear();
}
FlockComp& FlockComp::operator=(const FlockComp& rhs)
{
	width_range = rhs.width_range;
	height_range = rhs.height_range;
	number_of_birds = rhs.number_of_birds;
	current_turn_to_attack = rhs.current_turn_to_attack;
	give_turn_to_attack = rhs.give_turn_to_attack;
	current_turn_to_move = rhs.current_turn_to_move;
	give_turn_to_move = rhs.give_turn_to_attack;
	detect_radius = rhs.detect_radius;
	minimun_distance = rhs.minimun_distance;
	return *this;
}
FlockComp* FlockComp::Clone()
{
	FlockComp* temp = FactorySys->Create<FlockComp>();
	(*temp) = (*this);
	return temp;
}

void FlockComp::OnCreate()
{

}
void FlockComp::Initialize()
{
	LogicComponent::Initialize();
	
	transform = mOwner->GetComp<TransformComponent>();
	
	if (transform)
	{
		for (unsigned i = 0; i < number_of_birds; i++)
		{
			GameObject* newbird = CreateFromArchetype("FlyingEnemy", mOwner->GetParentSpace(), transform->GetWorldPosition());
			
			TransformComponent* birdpos = newbird->GetComp<TransformComponent>();
			FlyingEnemyMachine* bird_machine = reinterpret_cast<FlyingEnemyMachine*>(newbird->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));

			if (bird_machine)
				bird_machine->owner_flock = this;
			

			if (rand() % 2 == 0)
			{
				//At left
				birdpos->SetWorldPosition(glm::vec3(birdpos->GetWorldPosition().x - width_range / 2.f, birdpos->GetWorldPosition().y, birdpos->GetWorldPosition().z));
				bird_machine->move_right = true;
			}
			else
			{
				//At right
				birdpos->SetWorldPosition(glm::vec3(birdpos->GetWorldPosition().x + width_range / 2.f, birdpos->GetWorldPosition().y, birdpos->GetWorldPosition().z));
				bird_machine->move_right = false;
			}
			
			float newheight = (float)(rand() % (int)height_range);
			newheight = birdpos->GetWorldPosition().y + (newheight - height_range / 2.f);

			birdpos->SetWorldPosition(glm::vec3(birdpos->GetWorldPosition().x, newheight, birdpos->GetWorldPosition().z));

			prev_pos = transform->GetWorldPosition();
			bird_machine->target_pos.z = transform->GetWorldPosition().z;
			bird_machine->prevy = birdpos->GetWorldPosition().y;
			if (bird_machine->move_right)
			{
				bird_machine->target_pos.x = transform->GetWorldPosition().x + width_range / 2.f;
				bird_machine->move_right = false;
			}
			else
			{
				bird_machine->target_pos.x = transform->GetWorldPosition().x - width_range / 2.f;
			}

			float newheight2 = (float)(rand() % (int)height_range);
			newheight2 = transform->GetWorldPosition().y + (newheight2 - height_range / 2.f);
			bird_machine->target_pos.y = newheight2;

			bird_machine->amplitude = (float)(rand() %30  + 1);

			bird_machine->travel_speed = (float)(rand() % 4 + 1);
			bird_machine->speed= ((float)rand() / RAND_MAX) * (0.04f - 0.01f) + 0.01f;

			birds.push_back(newbird);
		}
	}
}
void FlockComp::Update()
{
	if (birds.size() == 0)
	{
		SceneSys->GetMainSpace()->DestroyObject(mOwner);
	}
	else
	{
		SceneSys->GetMainSpace()->Drawrectangle(glm::vec2(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x, mOwner->GetComp<TransformComponent>()->GetWorldPosition().y), width_range, height_range, 0.f);
		SceneSys->GetMainSpace()->DrawCircle(glm::vec2(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x, mOwner->GetComp<TransformComponent>()->GetWorldPosition().y), detect_radius, Colors::blue);

		for (int i = 0; i < birds.size(); i++)
		{
			FlyingEnemyMachine* bird_machine2 = reinterpret_cast<FlyingEnemyMachine*>(birds[i]->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));
			SceneSys->GetMainSpace()->DrawCircle(glm::vec2(bird_machine2->target_pos.x, bird_machine2->target_pos.y), 10.f, Colors::green);
		}

		if (detected == false)
		{
			TransformComponent& playercoll = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();
			if (StaticRectToStaticCirlce(playercoll.GetWorldPosition(), playercoll.GetScale().x, playercoll.GetScale().y, transform->GetWorldPosition(), detect_radius))
			{
				detected = true;
				go_to_pos = true;
			}
		}
		else
		{
			if (go_to_pos && start_attacking == false)
			{
				TransformComponent& playercoll = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();

				glm::vec3 pos_to_achieve = playercoll.GetWorldPosition() + glm::vec3(0, minimun_distance, 0);


				glm::vec3 new_pos = glm::mix(mOwner->GetComp<TransformComponent>()->GetWorldPosition(), pos_to_achieve, 0.1f);
				glm::vec3 to_add = new_pos - mOwner->GetComp<TransformComponent>()->GetWorldPosition();
				for (int i = 0; i < birds.size(); i++)
				{
					FlyingEnemyMachine* bird_machine2 = reinterpret_cast<FlyingEnemyMachine*>(birds[i]->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));
					if (bird_machine2->turn_to_attack == false)
					{
						birds[i]->GetComp<TransformComponent>()->SetWorldPosition(birds[i]->GetComp<TransformComponent>()->GetWorldPosition() + to_add);
						bird_machine2->target_pos = bird_machine2->target_pos + to_add;

					}
				}

				mOwner->GetComp<TransformComponent>()->SetWorldPosition(new_pos);

				if (abs(new_pos.x - pos_to_achieve.x) < 4.f && abs(new_pos.y - pos_to_achieve.y) < 4.f)
				{
					start_attacking = true;
				}

			}

			if (start_attacking)
			{

				TransformComponent& playercoll = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<Collider>()->GetCollideBox();

				glm::vec3 pos_to_achieve = playercoll.GetWorldPosition() + glm::vec3(0, minimun_distance, 0);
				float xmov = glm::mix(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x, pos_to_achieve.x, 0.1f);
				float ymov = glm::mix(mOwner->GetComp<TransformComponent>()->GetWorldPosition().y, pos_to_achieve.y, 1.f);

				
				float to_add = xmov - mOwner->GetComp<TransformComponent>()->GetWorldPosition().x;
				float to_add_y = pos_to_achieve.y - mOwner->GetComp<TransformComponent>()->GetWorldPosition().y;
				glm::vec3 new_pos = glm::vec3(xmov, pos_to_achieve.y, mOwner->GetComp<TransformComponent>()->GetWorldPosition().z);

				for (int i = 0; i < birds.size(); i++)
				{
					FlyingEnemyMachine* bird_machine2 = reinterpret_cast<FlyingEnemyMachine*>(birds[i]->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));
					if (bird_machine2->turn_to_attack == false)
					{
						bird_machine2->prevy = bird_machine2->prevy +to_add_y;
						birds[i]->GetComp<TransformComponent>()->SetWorldPosition(glm::vec3(birds[i]->GetComp<TransformComponent>()->GetWorldPosition().x + to_add, birds[i]->GetComp<TransformComponent>()->GetWorldPosition().y + to_add_y,new_pos.z));
						bird_machine2->target_pos = glm::vec3(bird_machine2->target_pos.x + to_add, bird_machine2->target_pos.y + to_add_y, new_pos.z);

					}
				}

				mOwner->GetComp<TransformComponent>()->SetWorldPosition(new_pos);





				if (give_turn_to_attack == true)
				{

					int bird_to_attack = rand() % birds.size();

					FlyingEnemyMachine* bird_machine = reinterpret_cast<FlyingEnemyMachine*>(birds[bird_to_attack]->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));
					
					if (birds.size() != 1)
					{
						while (bird_to_attack == prev_attack && death_bird == false)
						{
							bird_to_attack = rand() % birds.size();
							bird_machine = reinterpret_cast<FlyingEnemyMachine*>(birds[bird_to_attack]->GetComp<BrainComponent>()->GetStateMachine("FlyingEnemyMachine"));
						}
					}
					bird_machine->turn_to_attack = true;
					prev_attack = bird_to_attack;
					give_turn_to_attack = false;
					death_bird = false;
				}
			}
		}
	}
}
void FlockComp::Shutdown()
{
	LogicComponent::Shutdown();
}


void FlockComp::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	j["width_range"] = width_range;
	j["height_range"] = height_range;
	j["number_of_birds"] = number_of_birds;
	j["detect_radius"] = detect_radius;
	j["minimun_distance"] = minimun_distance;
}
void FlockComp::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("width_range") != j.end())
		width_range = j["width_range"];

	if (j.find("height_range") != j.end())
		height_range = j["height_range"];

	if (j.find("number_of_birds") != j.end())
		number_of_birds = j["number_of_birds"];

	if (j.find("detect_radius") != j.end())
		detect_radius = j["detect_radius"];

	if (j.find("minimun_distance") != j.end())
		minimun_distance = j["minimun_distance"];
}


bool FlockComp::Edit()
{
	bool changed = false;

	SceneSys->GetMainSpace()->Drawrectangle(glm::vec2(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x, mOwner->GetComp<TransformComponent>()->GetWorldPosition().y), width_range, height_range, 0.f);
	SceneSys->GetMainSpace()->DrawCircle(glm::vec2(mOwner->GetComp<TransformComponent>()->GetWorldPosition().x, mOwner->GetComp<TransformComponent>()->GetWorldPosition().y),detect_radius,Colors::blue);

	ImGui::PushID(&number_of_birds);
	ImGui::DragInt("Number of birds", &number_of_birds, 1.0f, 1.0f, 10.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::PopID();

	ImGui::PushID(&width_range);
	ImGui::DragFloat("Width range", &width_range, 1.0f, 1.0f, 1000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::PopID();

	ImGui::PushID(&height_range);
	ImGui::DragFloat("Height range", &height_range, 1.0f, 1000.f, 10.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::PopID();


	ImGui::PushID(&detect_radius);
	ImGui::DragFloat("Detect radius", &detect_radius, 1.0f, 1000.f, 10.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::PopID();

	ImGui::PushID(&minimun_distance);
	ImGui::DragFloat("Minimun distance", &minimun_distance, 1.0f, 1000.f, 10.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::PopID();


	return changed;
}