#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../RTTI/IComp.h"
#include "../../../Graphics/Renderable/Renderable.h"
#include "../../../Graphics/Font/Font.h"
#include "../../../EventSystem/EventHandler.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../FlyingEnemySystem.h"
class FlockComp : public LogicComponent
{
	SM_RTTI_DECL(FlockComp, LogicComponent);
public:

	FlockComp();
	FlockComp(FlockComp& rhs);
	~FlockComp();
	FlockComp& operator=(const FlockComp& rhs);
	FlockComp* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;


	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;



	TransformComponent* transform;
	float width_range=0.f;
	float height_range=0.f;
	std::vector<GameObject*> birds;
	int number_of_birds=0;
	int current_turn_to_attack =0;
	int current_turn_to_move =0;
	bool give_turn_to_attack =true;
	bool give_turn_to_move =true;
	int prev_attack = 0;
	float detect_radius;
	bool detected = false;
	float minimun_distance = 0.f;
	glm::vec3 prev_pos;
	bool go_to_pos = false;
	bool start_attacking =false;
	bool death_bird = false;
};