#include <iostream>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Factory/Factory.h"
#include "../../Time/Time.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../Health/Health.h"
#include "GasCloud.h"


GasCloud::GasCloud()
{
	SetName("GasCloud");
	mTimeExisting = 0.0f;
}

GasCloud::~GasCloud()
{
	LogicComponent::Shutdown();
}

void GasCloud::Initialize()
{
	pMovementMachine = reinterpret_cast<MovementMachine*>(SceneSys->GetSpace("Main")->FindObjectByName("Player")->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"));
	LogicComponent::Initialize();
}

void GasCloud::Update()
{
	mTimeExisting += TimeSys->GetFrameTime();

	if (mTimeExisting >= mLifeTime)
	{
	//	SceneSys->GetMainSpace()->DestroyObject(GetOwner());
		mOwner->SetEnabled(false);
		mTimeExisting = 0.0f;
	}
}

void GasCloud::Shutdown()
{
	LogicComponent::Shutdown();
}

bool GasCloud::Edit()
{
	ImGui::DragFloat("mDamage", &mDamage,0.005f,0.0f,0.04f);
	ImGui::DragFloat("mLifeTime", &mLifeTime,1.0f,0.0f,10.0f);
	return false;
}

void GasCloud::OnCollisionPersisted(GameObject* other)
{
	if (other->CompareTag("Player"))
	{
		other->GetComp<Health>()->Damage(mDamage);
	}
}

GasCloud* GasCloud::Clone()
{
	GasCloud* temp = FactorySys->Create<GasCloud>();
	(*temp) = (*this);
	return temp;
}

GasCloud& GasCloud::operator=(const GasCloud& rhs)
{
	mTimeExisting = rhs.mTimeExisting;
	mDamage = rhs.mDamage;
	mLifeTime = rhs.mLifeTime;

	return (*this);
}

void GasCloud::ToJson(nlohmann::json& j)
{
	j["mDamage"] = mDamage;
	j["mLifeTime"] = mLifeTime;
}

void GasCloud::FromJson(const nlohmann::json& j)
{
	if (j.find("mDamage") != j.end())
		mDamage = j["mDamage"];
	if (j.find("mLifeTime") != j.end())
		mLifeTime = j["mLifeTime"];
}
