#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"

class GasEnemyMachine;
class GameObject;

class GasEnemyIdle : public State
{
public:
	GasEnemyIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	GasEnemyMachine* mCastedMachine = nullptr;
};

class GasEnemyJump : public State
{
public:
	GasEnemyJump(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionStarted(GameObject* _Other)override;

	glm::vec2 mDesiredPosition;
	GasEnemyMachine* mCastedMachine = nullptr;
};

class GasEnemyRunFromPlayer : public State
{
public:
	GasEnemyRunFromPlayer(const char* name = nullptr);
	
	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	void OnCollisionStarted(GameObject* _Other)override;

	GasEnemyMachine* mCastedMachine = nullptr;

	glm::vec2 mDesiredPosition;
	float mDirection = 1.0f;
	bool mAvoidedPlatform = false;
	int mJumpAfterAvoid = 0;
	bool mOnFloor = false;
};

class GasEnemyReset : public State
{
public:
	GasEnemyReset(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;

	void OnCollisionStarted(GameObject* _Other)override;

	GasEnemyMachine* mCastedMachine = nullptr;
};

class GasEnemyDie : public State
{
public:
	GasEnemyDie(const char* name = nullptr);
	
	void Initialize() override;
	void LogicEnter() override;
	void LogicUpdate();
	void LogicExit();

	GasEnemyMachine* mCastedMachine = nullptr;
};