#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Game/PlayerMovement/PlayerMovement.h"
#pragma once

class GasCloud : public LogicComponent
{
	SM_RTTI_DECL(GasCloud, LogicComponent);

public:
	GasCloud();
	~GasCloud();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	void OnCollisionPersisted(GameObject* other)override;

	GasCloud* Clone() override;
	GasCloud& operator=(const GasCloud& rhs);

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	MovementMachine* pMovementMachine;

private:
	float mTimeExisting;
	float mLifeTime;
	float mDamage;
};