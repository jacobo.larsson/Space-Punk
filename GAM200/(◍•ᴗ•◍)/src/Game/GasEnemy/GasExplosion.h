#pragma once
#include <glm/glm.hpp>
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"

class TransformComponent;
class GameObject;

class GasExplosion : public LogicComponent
{
	SM_RTTI_DECL(GasExplosion, LogicComponent);
public:
	GasExplosion();
	~GasExplosion();

	bool SetEnabled(bool _Enabled)override;
	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	GasExplosion* Clone() override;
	GasExplosion& operator=(const GasExplosion& rhs);

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void OnCollisionPersisted(GameObject* other)override;

private:
	TransformComponent* mOwnerTransform;
	glm::vec2 mDirection;
	float mTimeExisting;
	float mLifeTime;
	float mDamage;
	float mVelocity;
};