#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Factory/Factory.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../RayCast/RayCast.h"
#include "../../RayCast/RayCaster.h"
#include "../../RayCast/RayCastTest.h"
#include "../../Game/ShotReceiver/ShotReceiver.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Physics/RigidBody.h"
#include "../../Collisions/ICollider.h"

#include "../Health/Health.h"
#include "../Hitbox/Hitbox.h"
#include "../Purification/Purification.h"

#include "GasTicketer/GasTicketer.h"

#include "../GasEnemy/GasEnemyStates.h"
#include "../GasEnemy/GasEnemySystem.h"

GasEnemyMachine::GasEnemyMachine()
	:mJumpTimer(),
	mGasCloudsTimer(),
	mGasExplosionTimer(),
	mFlickerTimerAbsolute(),
	mFlickerTimerInternal()
{
	mName = "GasEnemyMachine";

	mInitialState = new GasEnemyIdle("GasEnemyIdle");
	mCurrentState = mInitialState;

	AddState(mInitialState);
	AddState(new GasEnemyRunFromPlayer	("GasEnemyRunFromPlayer"));
	AddState(new GasEnemyJump			("GasEnemyJump"));
	AddState(new GasEnemyDie			("GasEnemyDie"));
	AddState(new GasEnemyReset			("GasEnemyReset"));
	AddState(new PurificationState		("PurificationState"));
}

GasEnemyMachine::~GasEnemyMachine() { StateMachine::~StateMachine(); GasEnemyMachine::Shutdown(); }

void GasEnemyMachine::Initialize()
{
	mOwnerTransform = mActor->GetComp<TransformComponent>();
	mHealth = mActor->GetComp<Health>();
	mShotReceiver = mActor->GetComp<ShotReceiver>();
	mRaycaster = mActor->GetComp<RayCaster>();
	mRigidBody = mActor->GetComp<RigidBody>();
	mRenderable = mActor->GetComp<Renderable>();
	if (mRaycaster != nullptr)
	{
		mLeftRay = mRaycaster->GetRay("LeftRay");
		mRightRay = mRaycaster->GetRay("RightRay");
	}


	mPlayer = SceneSys->GetMainSpace()->FindObjectByName("Player");
	mPlayerTransform = mPlayer->GetComp<TransformComponent>();


	for (unsigned i = 0; i < mMaxClouds; i++)
	{
		mGasClouds[i] = CreateFromArchetype("GasCloudHitbox",
			mActor->GetParentSpace(), mActor->GetComp<TransformComponent>()->GetWorldPosition());
		mGasClouds[i]->SetEnabled(false);
	}
	for (unsigned i = 0; i < 5; i++)
	{
		mGasExplosion[i] = CreateFromArchetype("GasExplosion",
			mActor->GetParentSpace(), mActor->GetComp<TransformComponent>()->GetWorldPosition());
		mGasExplosion[i]->SetEnabled(false);
	}
	mGasCloudsTimer.SetMaximum(0.3f);
	mGasExplosionTimer.SetMaximum(((float)rand() / RAND_MAX) * 7.0f + 1.0f);
	mFlickerTimerAbsolute.SetMaximum(1.5f);
	mFlickerTimerInternal.SetMaximum(0.75f);
		
	StateMachine::Initialize();
}

void GasEnemyMachine::Update()
{
	mGasExplosionTimer.Update();
	if (mGasExplosionTimer.HasReachedMaximum())
	{
		mGasExplosionTimer.Reset();
		float _RandomNumber = (((float)rand() / RAND_MAX) * 7.0f + 1.0f);
		mGasExplosionTimer.SetMaximum(_RandomNumber);
		GasExplosionAttack();
	}
	if (mFlicker)
	{
		mFlickerTimerAbsolute.Update();
		mFlickerTimerInternal.Update();
		if (mFlickerTimerAbsolute.HasReachedMaximum())
		{
			mFlicker = false;
			mRenderable->SetVisible(true);
			mFlickerTimerAbsolute.Reset();
			mFlickerTimerInternal.Reset();
			mFlickerTimerInternal.SetMaximum(mOriginalFlickerTime);
		}
		else
		{
			Flicker();
			if (mFlickerTimerInternal.HasReachedMaximum())
			{
				mFlickerTimerInternal.Reset();
				mFlickerTimerInternal.SetMaximum(mFlickerTimerInternal.GetMaximumTime() / 2.0f);
			}
		}
	}

	if(mTicket)
		StateMachine::Update();
	else if(!mGasTicketer)
		StateMachine::Update();
}

void GasEnemyMachine::Shutdown()
{
	for (unsigned i = 0; i < mMaxClouds; i++)
	{
		if(mGasClouds[i])
			mGasClouds[i]->GetParentSpace()->DestroyObject(mGasClouds[i]);
	}
	for (unsigned i = 0; i < 5; i++)
	{
		if(mGasExplosion[i])
			mGasExplosion[i]->GetParentSpace()->DestroyObject(mGasExplosion[i]);
	}
	StateMachine::Shutdown();
}

void GasEnemyMachine::OnCollisionStarted(GameObject* other)
{
	if (other->CompareTag("Hitbox"))
	{
		if (Hitbox* _Hitbox = other->GetComp<Hitbox>())
		{
			if (!_Hitbox->mEnemy)
			{
				mFlicker = true;
				GasExplosionAttack();
				if (mGasTicketer)
					mGasTicketer->AskPriority(this);
				mHealth->Damage(_Hitbox->mDamage);
				if (mHealth->GetHealth() <= 0.0f)
				{
					if (mGasTicketer)
						mGasTicketer->RemoveToad(mActor);
					if ((rand() % 50) + 1 > 50)
						ChangeState("PurificationState");
					else
						ChangeState("GasEnemyDie");
					return;
				}
				/*if (_Hitbox->mKnockback) 
				{
					mHKTime = _Hitbox->mKTime;
					mHKSPeed = _Hitbox->mKSpeed;

					receivedHitbox = other;
				}*/
			}
		}
		if(GetCurrentStateName() != "GasEnemyRunFromPlayer")
			ChangeState("GasEnemyRunFromPlayer");
	}
	if (other->CompareTag("Platform"))
		mPlatforms.push_back(other);

	StateMachine::OnCollisionStarted(other);
}

GasEnemyMachine& GasEnemyMachine::operator=(const GasEnemyMachine& rhs)
{
	mMinJumpVelocity = rhs.mMinJumpVelocity;
	mMaxJumpVelocity = rhs.mMaxJumpVelocity;
	mJumpTimer = rhs.mJumpTimer;
	return *this;
}

GasEnemyMachine* GasEnemyMachine::Clone()
{
	GasEnemyMachine* temp = FactorySys->Create<GasEnemyMachine>();
	*temp = *this;
	return temp;
}

bool GasEnemyMachine::Edit()
{
	ImGui::DragFloat("Min Jump Velocity", &mMinJumpVelocity, 1.0f, 0.0f);
	ImGui::DragFloat("Max Jump Velocity", &mMaxJumpVelocity, 1.0f, 0.0f);
	mJumpTimer.Edit("Jump Timer");
	return false;
}

void GasEnemyMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["mMinJumpVelocity"] = mMinJumpVelocity;
	j["mMaxJumpVelocity"] = mMaxJumpVelocity;

	mJumpTimer.ToJson(j["mJumpTimer"]);
}

void GasEnemyMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	if (j.find("mMinJumpVelocity") != j.end())
		mMinJumpVelocity = j["mMinJumpVelocity"];
	if (j.find("mMaxJumpVelocity") != j.end())
		mMaxJumpVelocity = j["mMaxJumpVelocity"];
	if(j.find("mJumpTimer") != j.end())
		mJumpTimer.FromJson(j["mJumpTimer"]);

}

void GasEnemyMachine::AddPlatform(GameObject* _Platform)
{
	if (std::find(mPlatforms.begin(), mPlatforms.end(), _Platform) == mPlatforms.end())
		mPlatforms.push_back(_Platform);
}

void GasEnemyMachine::GasExplosionAttack()
{
	for (unsigned i = 0; i < 5; i++)
	{
		mGasExplosion[i]->GetComp<TransformComponent>()->SetWorldPosition(mActor->GetComp<TransformComponent>()->GetWorldPosition());
		mGasExplosion[i]->SetEnabled(true);
	}
}

void GasEnemyMachine::TriggerGasCloud()
{
	mGasCloudsTimer.Reset();
	mGasClouds[mLastUsedCloud]->SetEnabled(true);
	mGasClouds[mLastUsedCloud++]->GetComp<TransformComponent>()->SetWorldPosition(mOwnerTransform->GetWorldPosition());
	if (mLastUsedCloud >= 25)
		mLastUsedCloud = 0;
}

void GasEnemyMachine::Flicker()
{
	if (mRenderable->IsVisible())
		mRenderable->SetVisible(false);
	else
		mRenderable->SetVisible(true);
}

