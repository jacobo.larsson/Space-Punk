#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../GameObject/GameObject.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Physics/RigidBody.h"
#include "../Health/Health.h"

#include "../../RayCast/RayCast.h"
#include "../../RayCast/RayCastTest.h"
#include "../PlayerMovement/PlayerMovement.h"
#include "../../GameObject/GameObject.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"

#include "GasTicketer/GasTicketer.h"

#include "GasEnemySystem.h"
#include "GasEnemyStates.h"

GasEnemyIdle::GasEnemyIdle(const char* name) :State(name)
{

}

void GasEnemyIdle::Initialize()
{
	mCastedMachine = reinterpret_cast<GasEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void GasEnemyIdle::LogicEnter()
{
	mCastedMachine->mJumpTimer.Reset();
	mCastedMachine->mJumpTimer.Start();
}

void GasEnemyIdle::LogicUpdate()
{
	mCastedMachine->mJumpTimer.Update();
	if (mCastedMachine->mJumpTimer.HasReachedMaximum())
		mCastedMachine->ChangeState("GasEnemyJump");
}

void GasEnemyIdle::LogicExit()
{
}

GasEnemyJump::GasEnemyJump(const char* name) :State(name)
{
}

void GasEnemyJump::Initialize()
{
	mCastedMachine = reinterpret_cast<GasEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void GasEnemyJump::LogicEnter()
{
	float _RandomNumber = (float)rand() / RAND_MAX;
	float _JumpVelocity = glm::mix(mCastedMachine->mMinJumpVelocity, mCastedMachine->mMaxJumpVelocity, _RandomNumber);

	mCastedMachine->mRigidBody->Velocity.y = _JumpVelocity;
	auto _Position = mCastedMachine->mOwnerTransform->GetWorldPosition();
	mDesiredPosition = _Position;

	// It should jump to the right
	if (_Position.x < mCastedMachine->mMovingOffset.x )
	{
		mCastedMachine->mRigidBody->Velocity.x = _JumpVelocity;
		while (_Position.x + _JumpVelocity > mCastedMachine->mMovingOffset.x + mCastedMachine->mMovingArea.x)
		{
			mCastedMachine->mRigidBody->Velocity.x /= 2.0f;
			_JumpVelocity /= 2.0f;
		}
		mDesiredPosition.x += _JumpVelocity;
	}
	else
	{
		mCastedMachine->mRigidBody->Velocity.x = -_JumpVelocity;
		while (_Position.x - _JumpVelocity < mCastedMachine->mMovingOffset.x - mCastedMachine->mMovingArea.x)
		{
			mCastedMachine->mRigidBody->Velocity.x /= 2.0f;
			_JumpVelocity /= 2.0f;
		}
		mDesiredPosition.x -= _JumpVelocity;
	}
}

void GasEnemyJump::LogicUpdate()
{
	SceneSys->GetMainSpace()->DrawCircle(mDesiredPosition, 5.0f, Colors::green);
}

void GasEnemyJump::LogicExit()
{
	mCastedMachine->mRigidBody->Velocity.x = 0.0f;
	mCastedMachine->mRigidBody->Velocity.y = 0.0f;
	mCastedMachine->mTicket = false;
}

void GasEnemyJump::OnCollisionStarted(GameObject* _Other)
{
	if (_Other->CompareTag("Platform"))
		mCastedMachine->ChangeState("GasEnemyIdle");
}

GasEnemyRunFromPlayer::GasEnemyRunFromPlayer(const char* name) : State(name)
{}

void GasEnemyRunFromPlayer::Initialize()
{
	mCastedMachine = reinterpret_cast<GasEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void GasEnemyRunFromPlayer::LogicEnter()
{
	auto _PlayerPos = mCastedMachine->mPlayerTransform->GetWorldPosition();
	auto _ToadPos = mCastedMachine->mOwnerTransform->GetWorldPosition();
	if (_ToadPos.x > _PlayerPos.x)
		mDirection = 1.0f;
	else
		mDirection = -1.0f;
	mCastedMachine->mRigidBody->Velocity.x = mDirection * 300.0f;
	mCastedMachine->mRigidBody->Velocity.y = 100.0f;

	mCastedMachine->mGasCloudsTimer.Reset();
	mCastedMachine->mGasCloudsTimer.Start();
}

void GasEnemyRunFromPlayer::LogicUpdate()
{
	mCastedMachine->mGasCloudsTimer.Update();
	if (mCastedMachine->mGasCloudsTimer.HasReachedMaximum())
	{
		mCastedMachine->TriggerGasCloud();
		float _RandomNumber = (((float)rand() / RAND_MAX) * 0.2f + 0.1f);
		mCastedMachine->mGasCloudsTimer.SetMaximum(_RandomNumber);
	}

	if (mOnFloor)
	{
		glm::vec2 _PlayerPos{ mCastedMachine->mPlayerTransform->GetWorldPosition() };
		glm::vec2 _ToadPos{ mCastedMachine->mOwnerTransform->GetWorldPosition() };
		float _Velocity = 300.0f;

		if (!mAvoidedPlatform)
		{
			if (_ToadPos.x > _PlayerPos.x)
				mDirection = 1.0f;
			else
				mDirection = -1.0f;
		}

		if (glm::distance(_PlayerPos.x, _ToadPos.x) < 300.0f)
		{
			mCastedMachine->mRigidBody->Velocity.x = mDirection * _Velocity;
			mCastedMachine->mRigidBody->Velocity.y = 100.0f;
			mOnFloor = false;
		}
		else
			mCastedMachine->ChangeState("GasEnemyIdle");


		if (_ToadPos.x > mCastedMachine->mMovingOffset.x - mCastedMachine->mMovingArea.x && _ToadPos.x < mCastedMachine->mMovingOffset.x + mCastedMachine->mMovingArea.x)
			mAvoidedPlatform = false;

		glm::vec2 _Position{ mDirection * 100.0f, 0.0f };
		mDesiredPosition = mCastedMachine->mOwnerTransform->GetWorldPosition();
		Ray _Ray;
		_Ray.SetOrigin(_Position);
		_Ray.SetOrientation(3.0f * 3.1415f / 2.0f);
		_Ray.SetLength(1000);
		_Ray.SetOwnerTransform(mCastedMachine->mOwnerTransform);
		DrawRay(&_Ray);

		if (CheckRayWithVector(&_Ray, nullptr, nullptr, mCastedMachine->mPlatforms) == -1.0f)
		{
			mCastedMachine->mRigidBody->Velocity.x = -mDirection * _Velocity;
			mDirection *= -1.0f;
			mAvoidedPlatform = true;
		}
	}
}

void GasEnemyRunFromPlayer::LogicExit()
{
	mCastedMachine->mRigidBody->Velocity.x = 0.0f;
	mCastedMachine->mRigidBody->Velocity.y = 0.0f;
	mCastedMachine->mTicket = false;
}

void GasEnemyRunFromPlayer::OnCollisionStarted(GameObject* _Other)
{
	if (_Other->CompareTag("Platform"))
		mOnFloor = true;
}

GasEnemyReset::GasEnemyReset(const char* name) : State(name)
{
}

void GasEnemyReset::Initialize()
{
	mCastedMachine = reinterpret_cast<GasEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void GasEnemyReset::LogicEnter()
{
	//mCastedMachine->GasExplosionAttack();
	//
	//if (mCastedMachine->mHealth)
	//{
	//	mCastedMachine->mHealth->Damage(mCastedMachine->mHDamage);
	//
	//	if (mCastedMachine->mHealth->GetHealth() <= 0.0f)
	//	{
	//		if ((rand() % 50) + 1 > 50)
	//			mCastedMachine->ChangeState("PurificationState");
	//		else
	//			mCastedMachine->ChangeState("GasEnemyDie");
	//	}
	//}
	//bool pFacingRight = reinterpret_cast<MovementMachine*>(mCastedMachine->mPlayer->GetComp<BrainComponent>()->GetStateMachine("MovementMachine"))->FacingRight;
	//if (mCastedMachine->receivedHitbox) 
	//{
	//	float pHitboxWidth = mCastedMachine->receivedHitbox->GetComp<TransformComponent>()->GetWorldScale().x;
	//	//Knockback to the end of the hitbox
	//
	//	float pTransform = mCastedMachine->mPlayerTransform->GetWorldPosition().x;
	//	float FinalPosX = pTransform + (pFacingRight ? pHitboxWidth + 100 : -pHitboxWidth - 100);
	//
	//	float mPos = mCastedMachine->mOwnerTransform->GetWorldPosition().x;
	//	float DistanceToTravel = FinalPosX - mPos;
	//	//distance that has to travel over time it has to travel it
	//	mKnockbackSpeed = (DistanceToTravel) / mCastedMachine->mHKTime;
	//}

}

void GasEnemyReset::LogicUpdate()
{/*
	if (mTimeInState < mCastedMachine->mHKTime || mTimeInState < ResetTime)
	{
		glm::vec3 pos = mCastedMachine->mOwnerTransform->GetWorldPosition();
		pos.x += mKnockbackSpeed * TimeSys->GetFrameTime();
		mCastedMachine->mOwnerTransform->SetWorldPosition(pos);
	}
	else if (mCastedMachine->mHealth->GetHealth() > 0.0f)
	{
		mCastedMachine->ChangeState("GasEnemyRunFromPlayer");
	}*/
}

void GasEnemyReset::LogicExit()
{
}

void GasEnemyReset::OnCollisionStarted(GameObject* _Other)
{
}

GasEnemyDie::GasEnemyDie(const char* name) : State(name)
{}

void GasEnemyDie::Initialize()
{
	mCastedMachine = reinterpret_cast<GasEnemyMachine*>(mOwnerStateMachine);
	State::Initialize();
}

void GasEnemyDie::LogicEnter()
{
}

void GasEnemyDie::LogicUpdate()
{
	mActor->GetParentSpace()->DestroyObject(mActor);
}

void GasEnemyDie::LogicExit()
{
}

