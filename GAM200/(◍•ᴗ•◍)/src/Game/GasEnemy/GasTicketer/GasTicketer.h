#pragma once
#include <glm/glm.hpp>
#include <queue>

#include "../../../LogicSystem/LogicComponent.h"

#include "../../../Time/Timer.h"

class GameObject;
class GasEnemyMachine;

class GasTicketer :public LogicComponent
{
	SM_RTTI_DECL(GasTicketer, LogicComponent);

public:

	GasTicketer();
	~GasTicketer();
	GasTicketer* Clone() override;
	GasTicketer& operator=(const GasTicketer& rhs);

	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void EnQueue(GasEnemyMachine* _Machine);
	void AskPriority(GasEnemyMachine* _Machine);
	void RemoveToad(GameObject* _Toad);

	void ShuffleQueue();
	void GiveTicket();

	std::vector<GameObject*> mToads;
	std::deque<GasEnemyMachine*> mQueue;

	int mEventProbability = 0;
	int mShuffleProbability = 0;
	int mMultiTicketProbability = 0;
	int mMaxTickets = 0;

	Timer mTicketsTimer;
	Timer mEventTimer;

	int mNumOfToads = 0;
	glm::vec2 mOffSet = glm::vec2{ 0.0f, 0.0f };
	glm::vec2 mArea = glm::vec2{ 0.0f, 0.0f };

};