#include <random>

#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Archetype/ArchetypeFunctions.h"
#include "../../../Transform2D/TransformComponent.h"

#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../RayCast/RayCaster.h"

#include "../../../Space/Space.h"
#include "../../../GameObject/GameObject.h"

#include "../GasEnemySystem.h"
#include "GasTicketer.h"

GasTicketer::GasTicketer()
{
	SetName("GasTicketer");
}

GasTicketer::~GasTicketer()
{
}

GasTicketer* GasTicketer::Clone()
{
	return nullptr;
}

GasTicketer& GasTicketer::operator=(const GasTicketer& rhs)
{
	// TODO: insert return statement here
	return *this;
}

void GasTicketer::Initialize()
{
	auto _Position = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
	for (unsigned i = 0; i < mNumOfToads; i++)
	{
		if(i % 2 == 0)
			mToads.push_back( CreateFromArchetype("GasEnemy", mOwner->GetParentSpace(), glm::vec3{_Position.x + glm::mix(mOffSet.x, mOffSet.x + mArea.x / 2.0f, (float)rand() / RAND_MAX), _Position.y , _Position.z}));
		else
			mToads.push_back( CreateFromArchetype("GasEnemy", mOwner->GetParentSpace(), glm::vec3{ _Position.x + glm::mix(mOffSet.x, mOffSet.x - mArea.x / 2.0f, (float)rand() / RAND_MAX), _Position.y , _Position.z }));
		auto _Machine = reinterpret_cast<GasEnemyMachine*>( mToads[i]->GetComp<BrainComponent>()->GetStateMachine("GasEnemyMachine"));
		_Machine->mMovingOffset = glm::vec2{ _Position } + mOffSet;
		_Machine->mMovingArea = mArea;
		_Machine->mGasTicketer = this;
		EnQueue(_Machine);
	}
	LogicComponent::Initialize();
}

void GasTicketer::Update()
{
	if (mToads.size() <= 0)
		mOwner->GetParentSpace()->DestroyObject(mOwner);

	mTicketsTimer.Update();
	mEventTimer.Update();

	if (mTicketsTimer.HasReachedMaximum())
		GiveTicket();
	if (mEventTimer.HasReachedMaximum())
	{
		mEventTimer.Reset();
		std::cout << "Event" << std::endl;
		int _RandomNumber = rand() % 100 + 1;
		if (_RandomNumber <= mShuffleProbability)
		{
			ShuffleQueue();
			std::cout << "Shuffle" << std::endl;
		}
		else if (_RandomNumber > mShuffleProbability && _RandomNumber <= mShuffleProbability + mMultiTicketProbability)
		{
			int _AnotherRandomNumber = rand() % mMaxTickets + 1;
			for (int i = 0; i < mMaxTickets; i++)
				GiveTicket();
			std::cout << "MultiTicket" << std::endl;
		}
		//else if (_RandomNumber >= 25 && _RandomNumber <= 30)
		//{
		//	int _AnotherRandomNumber = rand() % mNumOfToads + 1;
		//	for (int i = 0; i < _AnotherRandomNumber / 2; i++)
		//		GiveTicket();
		//}
	}
	Space* _Space = mOwner->GetParentSpace();

	if (_Space)
	{
		auto _Position = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
		_Space->DrawCircle(glm::vec2{ _Position } + mOffSet, 5.0f, Colors::green);
		_Space->Drawrectangle(glm::vec2{ _Position } + mOffSet, mArea.x, mArea.y, Colors::yellow);
	}

}

void GasTicketer::LateUpdate()
{
}

void GasTicketer::Shutdown()
{
	LogicComponent::Shutdown();
}

bool GasTicketer::Edit()
{
	bool changed = false;

	ImGui::PushID(&mNumOfToads);
	ImGui::InputInt("Number of toads", &mNumOfToads);
	ImGui::PopID();

	ImGui::PushID(&mEventProbability);
	ImGui::InputInt("Event probability", &mEventProbability);
	ImGui::PopID();

	ImGui::PushID(&mShuffleProbability);
	ImGui::InputInt("Shuffle probability", &mShuffleProbability);
	ImGui::PopID();

	ImGui::PushID(&mMultiTicketProbability);
	ImGui::InputInt("Multiple ticket probability", &mMultiTicketProbability);
	ImGui::PopID();

	ImGui::PushID(&mMaxTickets);
	ImGui::SliderInt("Multiple ticket amount", &mMaxTickets, 0, mNumOfToads - 1);
	ImGui::PopID();

	ImGui::Text("Offset");
	ImGui::PushID("Offset");
	ImGui::Columns(2, "Offset", true);

	ImGui::DragFloat("Offset X", &mOffSet.x);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("Offset Y", &mOffSet.y);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();

	ImGui::Text("mArea");
	ImGui::PushID("mArea");
	ImGui::Columns(2, "mArea", true);

	ImGui::DragFloat("mArea X", &mArea.x);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("mArea Y", &mArea.y);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();

	
	mTicketsTimer.Edit("Tickets Timer");
	mEventTimer.Edit("EventTimer");

	Space* _Space = mOwner->GetParentSpace();

	if (_Space)
	{
		auto _Position = mOwner->GetComp<TransformComponent>()->GetWorldPosition();
		_Space->DrawCircle(glm::vec2{ _Position } + mOffSet, 5.0f, Colors::green);
		_Space->Drawrectangle(glm::vec2{ _Position } + mOffSet, mArea.x, mArea.y, Colors::yellow);
	}

	return changed;
}

void GasTicketer::ToJson(nlohmann::json& j)
{
	j["mNumOfToads"] = mNumOfToads;
	j["mEventProbability"] = mEventProbability;
	j["mShuffleProbability"] = mShuffleProbability;
	j["mMultiTicketProbability"] = mMultiTicketProbability;
	j["mMaxTickets"] = mMaxTickets;
	j["mOffSet"].push_back(mOffSet.x);
	j["mOffSet"].push_back(mOffSet.y);
	j["mArea"].push_back(mArea.x);
	j["mArea"].push_back(mArea.y);
	mTicketsTimer.ToJson(j["TicketsTimer"]);
	mEventTimer.ToJson(j["EventTimer"]);
}

void GasTicketer::FromJson(const nlohmann::json& j)
{
	if (j.find("mNumOfToads") != j.end())
		mNumOfToads = j["mNumOfToads"];
	if (j.find("mEventProbability") != j.end())
		mEventProbability = j["mEventProbability"];
	if (j.find("mShuffleProbability") != j.end())
		mShuffleProbability = j["mShuffleProbability"];
	if (j.find("mMultiTicketProbability") != j.end())
		mMultiTicketProbability = j["mMultiTicketProbability"];
	if (j.find("mMaxTickets") != j.end())
		mMaxTickets = j["mMaxTickets"];

	if (j.find("mOffSet") != j.end())
		mOffSet.x = j["mOffSet"][0];
	if (j.find("mOffSet") != j.end())
		mOffSet.y = j["mOffSet"][1];
	if (j.find("mArea") != j.end())
		mArea.x = j["mArea"][0];
	if (j.find("mArea") != j.end())
		mArea.y = j["mArea"][1];
	if(j.find("TicketsTimer") != j.end())
		mTicketsTimer.FromJson(j["TicketsTimer"]);
	if(j.find("EventTimer") != j.end())
		mEventTimer.FromJson(j["EventTimer"]);
}

void GasTicketer::EnQueue(GasEnemyMachine* _Machine)
{
	mQueue.push_back(_Machine);
}

void GasTicketer::AskPriority(GasEnemyMachine* _Machine)
{
	mTicketsTimer.Reset();
	auto _ToReinsert = std::find(mQueue.begin(), mQueue.end(), _Machine);
	if (_ToReinsert != mQueue.end())
		mQueue.erase(_ToReinsert);
	EnQueue(_Machine);
	_Machine->mTicket = true;
}

void GasTicketer::RemoveToad(GameObject* _Toad)
{
	auto _ToDelete = std::find(mToads.begin(), mToads.end(), _Toad);
	if (_ToDelete != mToads.end())
		mToads.erase(_ToDelete);
}

void GasTicketer::ShuffleQueue()
{
	std::shuffle(mQueue.begin(), mQueue.end(), std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()));
}

void GasTicketer::GiveTicket()
{
	auto _ToadMachine = mQueue.front();
	mQueue.pop_front();
	mQueue.push_back(_ToadMachine);
	_ToadMachine->mTicket = true;
	mTicketsTimer.Reset();
}
