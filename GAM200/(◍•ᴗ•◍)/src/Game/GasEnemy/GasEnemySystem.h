#pragma once
#include "../../LogicSystem/LogicComponent.h"

#include "../../Time/Timer.h"
#include "../../StateMachines/StateMachine/StateMachine.h"

class Ray;
class Health;
class ShotReceiver;
class RigidBody;
class Renderable;
class GasTicketer;

class GasEnemyMachine : public StateMachine
{
	SM_RTTI_DECL(GasEnemyMachine, StateMachine);

public:

	GasEnemyMachine();
	~GasEnemyMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void OnCollisionStarted(GameObject* other) override;

	GasEnemyMachine& operator=(const GasEnemyMachine& rhs);
	GasEnemyMachine* Clone()override;

	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void AddPlatform(GameObject* _Platform);
	void GasExplosionAttack();
	void TriggerGasCloud();
	void Flicker();

	bool mTicket = false;

	// Player related
	GameObject* mPlayer = nullptr;
	TransformComponent* mPlayerTransform = nullptr;

	GasTicketer* mGasTicketer = nullptr;

	// Toad related
	TransformComponent* mOwnerTransform = nullptr;
	RayCaster* mRaycaster = nullptr;
	Ray* mLeftRay = nullptr;
	Ray* mRightRay = nullptr;
	ShotReceiver* mShotReceiver = nullptr;
	Health* mHealth = nullptr;
	RigidBody* mRigidBody = nullptr;
	Renderable* mRenderable = nullptr;
	Timer mFlickerTimerAbsolute;
	Timer mFlickerTimerInternal;
	float mOriginalFlickerTime = 0.75f;
	bool mFlicker = false;
	
	// Platforms related
	std::vector<GameObject*> mPlatforms;
	GameObject* mCurrentPlatform = nullptr;
	
	// Gas clouds
	const unsigned mMaxClouds = 25;
	unsigned mLastUsedCloud = 0;
	std::array<GameObject*, 25> mGasClouds;
	Timer mGasCloudsTimer;

	// Gas explosion
	std::array<GameObject*, 5 > mGasExplosion;
	Timer mGasExplosionTimer;
	
	// Idle movement
	glm::vec2 mMovingOffset = glm::vec2{ 0, 0 };
	glm::vec2 mMovingArea = glm::vec2{ 0, 0 };
	float mMaxJumpVelocity = 0.0f;
	float mMinJumpVelocity = 0.0f;
	Timer mJumpTimer;

	//Damage receiving
	float mHDamage;
	float mHKSPeed;
	float mHKTime;
	bool KnockbackDirectionRight;
	GameObject* receivedHitbox;

};
