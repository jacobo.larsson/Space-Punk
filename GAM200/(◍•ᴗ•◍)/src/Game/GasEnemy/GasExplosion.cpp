#include <stdlib.h>
#include <iostream>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Factory/Factory.h"
#include "../../Time/Time.h"

#include "../../GameObject/GameObject.h"

#include "../Health/Health.h"
#include "../../Transform2D/TransformComponent.h"

#include "GasExplosion.h"

GasExplosion::GasExplosion():mTimeExisting(0),mLifeTime(0),mDamage(0)
{
	SetName("GasExplosion");
	// Random numbers between -1 and 1
	mDirection.x = std::rand() % (3) - 1;
	mDirection.y = std::rand() % (3) - 1;
	mDirection = glm::normalize(mDirection);
}

GasExplosion::~GasExplosion()
{
	LogicComponent::Shutdown();
}

bool GasExplosion::SetEnabled(bool _Enabled)
{
	mDirection.x = -1.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.0f + 1.0f)));
	mDirection.y = -1.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.0f + 1.0f)));
	mDirection = glm::normalize(mDirection);
	IBase::SetEnabled(_Enabled);
	return _Enabled;
}

void GasExplosion::ToJson(nlohmann::json& j)
{
	j["mLifeTime"] = mLifeTime;
	j["mDamage"] = mDamage;
	j["mVelocity"] = mVelocity;
}

void GasExplosion::FromJson(const nlohmann::json& j)
{
	if (j.find("mLifeTime") != j.end())
		mLifeTime = j["mLifeTime"];
	if (j.find("mDamage") != j.end())
		mDamage = j["mDamage"];
	if (j.find("mVelocity") != j.end())
		mVelocity = j["mVelocity"];
}

GasExplosion* GasExplosion::Clone()
{
	GasExplosion* temp = FactorySys->Create<GasExplosion>();
	(*temp) = (*this);
	return temp;
}

GasExplosion& GasExplosion::operator=(const GasExplosion& rhs)
{
	mLifeTime = rhs.mLifeTime;
	mDamage = rhs.mDamage;
	mVelocity = rhs.mVelocity;
	return *this;
}

void GasExplosion::Initialize()
{
	mOwnerTransform = mOwner->GetComp<TransformComponent>();
	LogicComponent::Initialize();
}

void GasExplosion::Update()
{
	mTimeExisting += TimeSys->GetFrameTime();
	
	if (mOwnerTransform != nullptr)
	{
		glm::vec3 newPos = mOwnerTransform->GetWorldPosition();
		newPos.x += mDirection.x * mVelocity * (float)TimeSys->GetFrameTime();
		newPos.y += mDirection.y * mVelocity * (float)TimeSys->GetFrameTime();
		mOwnerTransform->SetWorldPosition(newPos );
	}

	if (mTimeExisting >= mLifeTime)
	{
		mOwner->SetEnabled(false);
		mTimeExisting = 0.0f;
	}
}

void GasExplosion::Shutdown()
{
	LogicComponent::Shutdown();
}

bool GasExplosion::Edit()
{
	ImGui::DragFloat("mLifeTime", &mLifeTime, 1.0f, 0.0f, 5.0f);
	ImGui::DragFloat("mDamage", &mDamage, 0.005f, 0.0f, 0.04f);
	ImGui::DragFloat("mVelocity", &mVelocity, 0.5f, 0.0f, 10.0f);
	return false;
}

void GasExplosion::OnCollisionPersisted(GameObject* other)
{
	if (other->CompareTag("Player"))
	{
		other->GetComp<Health>()->Damage(mDamage);
	}
}
