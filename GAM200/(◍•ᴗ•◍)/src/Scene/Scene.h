/**********************************************************************************/
/*
\file   Scene.h
\author Juan Pacheco & David Miranda
\par    email: pacheco.j@digipen.edu & m.david@digipen.edu
\par    DigiPen login: pacheco.j & m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that create an space and create/load/change a level in it.
*/
/*********************************************************************************/
#pragma once

#include <fstream>
#include <vector>

#include "../Extern/json/json.hpp"

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "../ResourceManager/ResourceManager.h"

class GameObject;
class Space;
class Scene : public IBase
{
	friend class Menus;
	friend class Editor;
	friend class Checkpoint;
	SM_RTTI_DECL(Scene, IBase);
	Make_Singleton(Scene)

public:
	// State functions
	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();

	// space management
	Space*	GetSpace(const char* spaceName);
	Space*	GetMainSpace();
	const std::vector<Space*>& GetAllSpaces();
	std::string GetLevelName();

	Space*	NewSpace(const char* spaceName);
	void	AddSpace(Space * toadd);
	void	DestroySpace(const char* spaceName);
	void	DestroySpace(Space* space);
	void	Reset();

	// wrappers for object creation (always call space)
	GameObject* NewObject(Space* space = NULL, const char* name = NULL);

	void ToJson(nlohmann::json& j) override;
	void CheckPointToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j) override;

	void NewLevel(std::string file_name);
	void ChangeLevelGame(std::string levelName);
	void ChangeLevelGameCompleteName(std::string levelName);
	void ResetLevelInGame();
	void SaveLevel();

	// Tags management
	void RegisterTag(const std::string& tag);
	void RemoveTag(const std::string& tag);
	const std::vector<std::string>& GetAllTags();
	void RemoveTags();
	
	GameObject* FindObjectById(unsigned int id);
	GameObject* FindObjectInAnySpace(const char* _Name);
	unsigned int FindLastMaxId();

	void	InitializeSpacesComponents();
	bool to_change = false;
private:
	void	DestroyAllSpaces();
	void	CreateMainSpace();
	void ChangeLevel(std::string file_name);

	std::vector<Space*> mSpaces;
	std::vector<std::string> allTags;
	Resource<Level>* mCurrentLevel = nullptr;
	Resource<Level>* mNextLevel = nullptr;
};
#define SceneSys Scene::Get()