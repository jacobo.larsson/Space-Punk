/**********************************************************************************/
/*
\file   Scene.cpp
\author Juan Pacheco & David Miranda
\par    email: pacheco.j@digipen.edu & m.david@digipen.edu
\par    DigiPen login: pacheco.j & m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions in Scene.h
*/
/*********************************************************************************/
#include <sys/stat.h>
#include "../MainEngine/MainEngine.h"
#include "../Factory/Factory.h"
#include "../GameObject/GameObject.h"
#include "../Space/Space.h"
#include "../Collisions/CollisionTable.h"
#include "../Settings/Settings.h"
#include "Scene.h"


Scene* Scene::instance = nullptr;

using nlohmann::json;

bool Scene::Initialize()
{
	
	// In here we will pass the level saved in the settings class in the future
	mNextLevel = mCurrentLevel = RsrcMan->GetResourceType<Level>(SettingsSys->initial_level);
	if (mCurrentLevel == nullptr)
	{
		SettingsSys->initial_level = "./data/Levels/MainMenu.lvl";
		RsrcMan->Load("./data/Levels/MainMenu.lvl");
		mNextLevel = mCurrentLevel = RsrcMan->GetResourceType<Level>("./data/Levels/MainMenu.lvl");
	}
	CreateMainSpace();

	std::ifstream fp1(SettingsSys->initial_level, std::ios_base::out | std::ios_base::in);
	json j;
	if (fp1.is_open() && fp1.good())
	{
		fp1 >> j;
		fp1.close();
		SceneSys->FromJson(j);
	}

	SettingsSys->current_max_id = SceneSys->FindLastMaxId();
	FactorySys->uId = SceneSys->FindLastMaxId()+1;
	return true;
}
void Scene::Update()
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
			(*it)->Update();
}
void Scene::LateUpdate()
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
	{
		(*it)->LateUpdate();
	}
	if (to_change)
	{
		to_change = false;
		ChangeLevel(mNextLevel->get()->level_name);
		// If we are ingame all the systems must be restarted
		if (!SettingsSys->inEditor)
			Engine->ChangeLevel();

		
		mCurrentLevel = mNextLevel;
	}
}
void Scene::Shutdown()
{
	RsrcMan->SaveLevel(mCurrentLevel->get()->level_name);
	DestroyAllSpaces();
	CollTable->Save();
}

// space management
Space* Scene::GetSpace(const char* spaceName)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			IBase* ibase_ = dynamic_cast<IBase*>(*it);
			if (ibase_->GetName() == spaceName)
				return *it;
		}
	return nullptr;
}
Space* Scene::GetMainSpace()
{
	return mSpaces[0];
}
const std::vector<Space*>& Scene::GetAllSpaces()
{
	return mSpaces;
}
std::string Scene::GetLevelName()
{
	if (mCurrentLevel)
		return mCurrentLevel->RelPath;
	else
		return std::string();
}
Space* Scene::NewSpace(const char* spaceName)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			if ((*it)->GetName() == spaceName)
				return *it;
		}
	Space* newSpace = FactorySys->Create<Space>();
	newSpace->SetName(spaceName);
	
	//Forgot to add push_back
	mSpaces.push_back(newSpace);
	return newSpace;

}
void Scene::AddSpace(Space* toadd)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			if ((*it) == toadd)
				return ;
		}
	

	//Forgot to add push_back
	mSpaces.push_back(toadd);
}
void Scene::DestroySpace(const char* spaceName)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			if ((*it)->GetName() == spaceName)
			{
				delete *it;
				mSpaces.erase(it);
				return;
			}
		}
}
void	Scene::DestroySpace(Space* space)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			if (*it == space)
			{
				space->Shutdown();
				mSpaces.erase(it);
				delete space;
				return;
			}
		}
}
void Scene::Reset()
{
	DestroyAllSpaces();

	std::ifstream fpmiputamadre(mCurrentLevel->get()->level_name, std::ios_base::out | std::ios_base::in);
	if (fpmiputamadre.is_open() && fpmiputamadre.good())
		mCurrentLevel = RsrcMan->GetResourceType<Level>(mCurrentLevel->get()->level_name);
	else
	{
		RsrcMan->Load("./data/Levels/Default.lvl");
		mNextLevel = mCurrentLevel = RsrcMan->GetResourceType<Level>("./data/Levels/Default.lvl");

	}
	std::ifstream fp1(mCurrentLevel->get()->level_name, std::ios_base::out | std::ios_base::in);
	json j;
	if (fp1.is_open() && fp1.good())
		{
			fp1 >> j;
			fp1.close();
			SceneSys->FromJson(j);
		}
}

// wrappers for object creation (always call space)
GameObject* Scene::NewObject(Space* space, const char* name)
{
	if(space)
		return space->NewGameObject(name);
	return nullptr;
}

void Scene::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	using nlohmann::json;
	if (mSpaces.size())
		{
			json& spacesJson = j["spaces"];
			for (auto& space : mSpaces)
			{
				json spaceJson;
				space->ToJson(spaceJson);
				spacesJson.push_back(spaceJson);
			}
		}


	if (allTags.size())
		{
			json& alltagsJson = j["AllTags"];
			for (auto& tag : allTags)
			{
				json tagJson;
				alltagsJson.push_back(tag);
			}
		}

	
}
void Scene::CheckPointToJson(nlohmann::json& j)
{

	IBase::ToJson(j);

	using nlohmann::json;
	if (mSpaces.size())
	{
		json& spacesJson = j["spaces"];
		for (auto& space : mSpaces)
		{
			json spaceJson;
			space->CheckPointToJson(spaceJson);
			spacesJson.push_back(spaceJson);
		}
	}
}
void Scene::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	auto spacesJson = j.find("spaces");
	if (spacesJson != j.end())
		{
			for (const auto& spaceJson : *spacesJson)
			{
				if (spaceJson.find("name") != spaceJson.end())
				{
					std::string spaceName = spaceJson["name"];

					if (Space* newSpace = NewSpace(spaceName.c_str()))
					{
						newSpace->FromJson(spaceJson);
					}
				}
			}
		}


	auto alltagsJson = j.find("AllTags");
	if (alltagsJson != j.end())
		{
			for (const auto& alltagJson : *alltagsJson)
			{
				RegisterTag(alltagJson);
			}
		}
}

void Scene::NewLevel(std::string file_name)
{
	if (file_name.size() == 0)
		{
			std::cout << "Please provide a level name.\n";
			return;
		}
	std::string outString("./data/Levels/");
	outString += file_name;
	outString += ".lvl";

	// Check if the file already existed
	struct stat buf;
	if (stat(outString.c_str(), &buf) == -1)
		{
			std::ofstream fp_out(outString);

			if (fp_out.is_open() && fp_out.good())
			{
				fp_out.close();
			}
			SaveLevel();
			RsrcMan->Load(outString);
			mCurrentLevel = RsrcMan->GetResourceType<Level>(outString);
			DestroyAllSpaces();
			CreateMainSpace();
		}
	else
		std::cout << "Level " << file_name << " already existed" << std::endl;

}
void Scene::ChangeLevel(std::string file_name)
{
	std::string outString("./data/Levels/");
	outString += file_name;
	outString += ".lvl";

	json j;
	std::ifstream fp_in;
	fp_in.open(file_name, std::ios_base::out | std::ios_base::in);

	if (fp_in.is_open() && fp_in.good())
	{
		RsrcMan->Load(file_name);
		mNextLevel = mCurrentLevel = RsrcMan->GetResourceType<Level>(file_name);
		DestroyAllSpaces();
		fp_in >> j;
		FromJson(j);
	}
	else
	{
		std::cout << "No level with name " << outString << " exists.\n";
	}

}
void Scene::ChangeLevelGame(std::string levelName)
{
	std::string outString("./data/Levels/");
	outString += levelName;
	outString += ".lvl";

	json j;
	std::ifstream fp_in;
	fp_in.open(outString, std::ios_base::out | std::ios_base::in);

	if (fp_in.is_open() && fp_in.good())
	{
		RsrcMan->Load(outString);
		mNextLevel = RsrcMan->GetResourceType<Level>(outString);
	}
	else
	{
		std::cout << "No level with name " << outString << " exists.\n";
	}
}
void Scene::SaveLevel()
{
	json h;
	SceneSys->ToJson(h);

	std::ofstream fp_out(mCurrentLevel->get()->level_name);
	if (fp_out.is_open() && fp_out.good())
		{
			fp_out << std::setw(4) << h;
			fp_out.close();
		}
}

void Scene::RegisterTag(const std::string& tag)
{
	auto _tag = std::find(allTags.begin(), allTags.end(), tag);
	if (_tag == allTags.end())
		allTags.push_back(tag);
}
void Scene::RemoveTag(const std::string& tag)
{
	std::for_each(mSpaces.begin(), mSpaces.end(), [&](auto& space)
		{
			// Sanity check when the space does not have any object with that tag
			if (space->ObjectsWithTags.find(tag) != space->ObjectsWithTags.end())
			{
				std::for_each(space->ObjectsWithTags.find(tag)->second.begin(), space->ObjectsWithTags.find(tag)->second.end(), [&](auto& obj)
				{
					obj->RemoveTag(tag);
				});
				space->ObjectsWithTags.find(tag)->second.clear();
				space->ObjectsWithTags.erase(tag);
			}
		});
	
	auto to_delete = std::find(allTags.begin(), allTags.end(), tag);
	if (to_delete != allTags.end())
		allTags.erase(to_delete);
}
const std::vector<std::string>& Scene::GetAllTags()
{
	return allTags;
}
void Scene::RemoveTags()
{
	allTags.clear();
}

GameObject* Scene::FindObjectById(unsigned int id)
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
		{
			if ((*it)->GetUID() == id)
				return *it;
			else
			{
				GameObject* test = (*it)->FindObjectById(id);
				if (test)
					return test;
			}

		}
	return nullptr;
}

GameObject* Scene::FindObjectInAnySpace(const char* _Name)
{
	GameObject* _Object = nullptr;
	for (auto& _Space : mSpaces)
	{
		_Object = _Space->FindObjectByName(_Name);
		if (_Object != nullptr)
			return _Object;
	}
	return _Object;
}

unsigned int Scene::FindLastMaxId()
{
	int max_id = 0;
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
	{
		if ((*it)->GetUID() > (unsigned int)max_id)
			max_id = (*it)->GetUID();
		
			int temporal_id = (*it)->FindObjectMaxId();
			if (temporal_id > max_id)
				max_id = temporal_id;
	}
	return max_id;
}

void Scene::InitializeSpacesComponents()
{
	std::for_each(mSpaces.begin(), mSpaces.end(), [&](auto & _Space)
	{
		_Space->InitializeComponents();
		_Space->InsertAllObjects();
	});
}
void	Scene::DestroyAllSpaces()
{
	std::vector<Space*>::iterator it;
	for (it = mSpaces.begin(); it != mSpaces.end(); it++)
	{
		(*it)->Shutdown();
		delete* it;
	}
	mSpaces.clear();
}
void	Scene::CreateMainSpace()
{
	NewSpace("Main");
}
void Scene::ChangeLevelGameCompleteName(std::string levelName)
{
	json j;
	std::ifstream fp_in;
	fp_in.open(levelName, std::ios_base::out | std::ios_base::in);

	if (fp_in.is_open() && fp_in.good())
	{
		RsrcMan->Load(levelName);
		mNextLevel = RsrcMan->GetResourceType<Level>(levelName);
	}
	else
	{
		std::cout << "No level with name " << levelName << " exists.\n";
	}
}
void Scene::ResetLevelInGame()
{
	mNextLevel = mCurrentLevel;
	mCurrentLevel = nullptr;}