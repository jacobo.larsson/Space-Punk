#pragma once
#include <thread>
#include <mutex>
#include <list>
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "../ResourceManager/ResourceManager.h"


class ThreadPool;

class Worker : public IBase
{
	SM_RTTI_DECL(Worker, IBase);
public:
	Worker(ThreadPool& to_assign) : pool_owner(to_assign) { }
	void operator()();

	bool WorkerWorking() const { return working; }

private:
	ThreadPool& pool_owner;
	bool working=false;
};


class ThreadPool : IBase
{
	SM_RTTI_DECL(ThreadPool, IBase);
	Make_Singleton(ThreadPool)

public:
	bool Initialize(); 
	~ThreadPool()
	{
		// stop all threads11
		stop_threads = true;
		condition.notify_all();

		// join them
		for (size_t i = 0; i < actual_threads.size(); ++i)
		{
			if(actual_threads[i].joinable())
				actual_threads[i].join();
		}
		for (Worker* worker : actual_workers)
		{
			delete worker;
		}
		actual_workers.clear();
	}

	template<class FUNCTION>
	void enqueue(FUNCTION _func)
	{
		{ // acquire lock
			std::unique_lock<std::mutex> lock(queue_mutex);

			// add the task
			actual_tasks.push_back(std::function<void()>(_func));
		} // release lock

		// wake up one thread
		condition.notify_one();
	}

	void WaitAll();

	std::list< std::function<void()> > actual_tasks;


	std::vector< std::thread >	actual_threads;
	std::vector< Worker* >	actual_workers;

	std::condition_variable condition;
	std::condition_variable condition_finish;
	std::mutex queue_mutex;
	std::thread::id mainID;
	bool stop_threads;
	int estimated_tasks=0;
	int counter_tasks = 0;
	int counter_dependant = 0;
	int estimated_dependant = 0;
};

#define ThreadP ThreadPool::Get()