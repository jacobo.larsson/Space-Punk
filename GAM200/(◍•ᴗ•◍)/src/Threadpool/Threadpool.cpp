#include "../Graphics/Window/Window.h"
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

#include "Threadpool.h"

ThreadPool* ThreadPool::instance = nullptr;
bool ThreadPool::Initialize() 
{
	//Get the main ID for the loading screen
	mainID = std::this_thread::get_id();
	stop_threads = false;
	
	//Get the maximun amount of threads (8 usually)
	int thread_count = std::thread::hardware_concurrency() ;
	
	//Add threads and workers (threads too) to the vectors
	for (int i = thread_count; i > 0; --i)
	{
		actual_workers.push_back(new Worker(*this));
		actual_threads.push_back(std::thread(*actual_workers.back()));
	}
	return true;
}

void Worker::operator()()
{
	std::function<void()> task;
	while (true)
	{
		{   // acquire lock
			std::unique_lock<std::mutex>
				lock(pool_owner.queue_mutex);

			working = false;	// Need to know if a task is being executed

			// look for a work item
			while (!pool_owner.stop_threads && pool_owner.actual_tasks.empty())
			{ // if there are none wait for notification
				pool_owner.condition.wait(lock);
			}

			if (pool_owner.stop_threads) // exit if the pool is stopped
				return;

			// get the task from the queue
			task = pool_owner.actual_tasks.front();
			pool_owner.actual_tasks.pop_front();

			working = true;
		}   // release lock

		// execute the task
		task();
	}
}



void ThreadPool::WaitAll()
{
	//Wait until the assets are loaded
	while (counter_tasks < estimated_tasks)
	//while (RsrcMan->dependant==false)
	{
		//Render the loading screen
		if (std::this_thread::get_id() == mainID)
		{
			SDL_SetRenderDrawColor(WindowSys->_renderer, 255, 255, 255, 255);
			SDL_RenderClear(WindowSys->_renderer);
			
			SDL_Rect rect;
			SDL_Rect rect2;

			float widthnew = 1.f - ((float)(estimated_tasks - counter_tasks) / (float)estimated_tasks);
			widthnew = 500.f * widthnew;
			rect.w = (int)widthnew;
			rect.h = 120;
			rect.x = (WindowSys->mWidth/2) - (250);
			rect.y = (WindowSys->mHeight / 2)  -(rect.h / 2);

			
			
			rect2.w =500;
			rect2.h = 120;
			rect2.x = (WindowSys->mWidth / 2) - (rect2.w / 2) ;
			rect2.y = (WindowSys->mHeight / 2) - (rect2.h / 2);


			SDL_SetRenderDrawColor(WindowSys->_renderer, 0, 0, 0, 255);
			SDL_RenderFillRect(WindowSys->_renderer, &rect2);

			SDL_SetRenderDrawColor(WindowSys->_renderer, 0, 255, 0, 255);
			SDL_RenderFillRect(WindowSys->_renderer, &rect);


			SDL_RenderPresent(WindowSys->_renderer);
		}

	}


	
}
