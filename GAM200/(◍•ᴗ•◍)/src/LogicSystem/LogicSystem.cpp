/**********************************************************************************/
/*
\file   LogicSystem.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarartion of the functions in LogicSystem.h
*/
/*********************************************************************************/
#include "../Space/Space.h"
#include "../Scene/Scene.h"

#include "../GameObject/GameObject.h"
#include "../Settings/Settings.h"
#include "LogicSystem.h"


LogicSystem* LogicSystem::instance = nullptr;

bool LogicSystem::Initialize()
{
	return true;
}
void LogicSystem::Update()
{
	if (SettingsSys->SpacePartitioning)
	{
		mActiveComponents.clear();
		for (auto& _Space : SceneSys->GetAllSpaces())
		{
			_Space->GetActiveComponents<LogicComponent>(mActiveComponents);
		}
		for (auto& _Comp : mActiveComponents)
		{

			GameObject* _Owner = _Comp->GetOwner();
			if (_Owner->Enabled() && _Comp->Enabled())
				_Comp->Update();
		}
	}
	else
	{
		for (auto& _Comp : mComponents)
		{
			GameObject* _Owner = _Comp->GetOwner();
			if (_Owner->Enabled() && !_Owner->IsDeleted() && _Comp->Enabled())
				_Comp->Update();
		}
	}
}
void LogicSystem::LateUpdate()
{
	if (SettingsSys->SpacePartitioning)
	{
		for (auto& _Comp : mActiveComponents)
		{
			if (_Comp->Enabled() == false)
			{
				GameObject* _Owner = _Comp->GetOwner();
				if (_Comp->Enabled() && !_Owner->IsDeleted() && _Owner->Enabled())
					_Comp->LateUpdate();
			}
		}
	}
	else
	{
		for (auto& _Comp : mComponents)
		{
			if (_Comp->Enabled() == false)
			{
				GameObject* _Owner = _Comp->GetOwner();

				if (_Owner->Enabled()&& !_Owner->IsDeleted() && _Comp->Enabled())
					_Comp->LateUpdate();
			}
		}
	}
}
void LogicSystem::Shutdown()
{
	RemoveAllBehaviors();
}

const LogicComponent* LogicSystem::AddBehavior(LogicComponent* new_behavior)
{
	auto it = std::find(mComponents.begin(), mComponents.end(), new_behavior);
	if (it == mComponents.end())
		mComponents.push_back(new_behavior);
	return new_behavior;
}
void LogicSystem::RemoveBehavior(LogicComponent* to_delete)
{
	auto it = std::find(mComponents.begin(), mComponents.end(), to_delete);
	if (it != mComponents.end())
		mComponents.erase(it);

	it = std::find(mActiveComponents.begin(), mActiveComponents.end(), to_delete);
	if (it != mActiveComponents.end())
		mActiveComponents.erase(it);
}
void LogicSystem::RemoveAllBehaviors()
{
	mComponents.clear();
}