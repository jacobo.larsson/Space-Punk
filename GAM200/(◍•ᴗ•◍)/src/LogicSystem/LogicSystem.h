/**********************************************************************************/
/*
\file   LogicSystem.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions to add behaviour 
*/
/*********************************************************************************/
#pragma once
#include <algorithm>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"

#include "../Singleton/Singleton.h"

#include "LogicComponent.h"


class LogicSystem :public IBase
{
	SM_RTTI_DECL(LogicSystem, IBase);
	Make_Singleton(LogicSystem)
public:
	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();

	const LogicComponent* AddBehavior(LogicComponent* new_behavior);
	void RemoveBehavior(LogicComponent* to_delete);
	void RemoveAllBehaviors();
private:
	std::vector<LogicComponent*> mComponents;
	std::vector<LogicComponent*> mActiveComponents;
};
#define LogicSys LogicSystem::Get()