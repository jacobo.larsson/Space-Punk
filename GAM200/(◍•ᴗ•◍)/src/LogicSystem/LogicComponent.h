#pragma once
#include "../RTTI/IBase.h"
#include "../RTTI/IComp.h"
#include "../RTTI/SM_RTTI.h"


class  LogicComponent : public IComp
{
	SM_RTTI_DECL(LogicComponent, IComp);
public:
	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	virtual bool Edit();
};