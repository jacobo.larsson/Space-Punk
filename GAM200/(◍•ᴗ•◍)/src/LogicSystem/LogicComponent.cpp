#include "LogicComponent.h"
#include "LogicSystem.h"

void LogicComponent::Initialize()
{
	LogicSys->AddBehavior(this);
}
void LogicComponent::Update()
{
}
void LogicComponent::LateUpdate()
{
}
void LogicComponent::Shutdown()
{
	LogicSys->RemoveBehavior(this);
}
bool LogicComponent::Edit()
{
	return false;
}
