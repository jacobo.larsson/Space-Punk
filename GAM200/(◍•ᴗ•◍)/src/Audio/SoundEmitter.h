#pragma once
#include <string>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../ResourceManager/ResourceManager.h"
#include "../EventSystem/Listener/Listener.h"
#include "../EventSystem/EventHandler.h"
struct Sound;
class Voice;
class TransformComponent;
class DumpResource;
class SoundEmiter :public IComp
{
	SM_RTTI_DECL(SoundEmiter, IComp);
public:
	friend class SelectableComp;
	SoundEmiter();
	SoundEmiter(SoundEmiter& rhs);
	~SoundEmiter();
	SoundEmiter& operator=(const SoundEmiter& rhs);
	SoundEmiter* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void Play();
	float ComputeVolume(glm ::vec3 emitter_pos, GameObject* listener);
	float ComputeAngle(glm ::vec3 emitter_pos, GameObject* listener);
	glm::vec2 ComputeMixes(glm ::vec3 emitter_pos, GameObject* listener);
	void UpdateVoice();
	void UpdateEchoes();
	void UpdateReverb();
	void NextOnQueue(bool play=false);
	void PrevOnQueue(bool play = false);
	void AddToQueue(const std::string& to_add);
	void RemoveFromQueue(const std::string& to_add);
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	Resource<Sound>* GetSound();
	Voice * GetVoice();
	void SetSound(std::string file_name);

	bool Edit()override;

#pragma region Events
	//void handle_event(const Event& _event)override;
	//EVENTHANDLER_DECL
		virtual void OnDumpResource(const DumpResource& _event);

private:
	void CheckToFree();
	void CheckToFreeEchoes();
	void CheckToFreeReverb();
	void CheckQueueChange();
	std::string mCurrentSound;
	float counter = 0;
	float reverb_counter = 0;
	Resource<Sound>* mSound = NULL;
	//Sound* mSound = NULL;
	bool echo_effect=false;
	bool reverb_effect = false;
	float time_of_delay=0.f;
	float echo_reducer = 0.f;
	float reverb_reducer = 0.f;
	TransformComponent* associated_tansform = nullptr;
	bool positional = false;
	float volume = 0.f;
	Voice* mVoice = NULL;
	Voice* reverb_mVoice = NULL;
	bool played = false;
	std::vector<std::pair<float,Voice*>> echo_voices;
	std::vector<std::string> sound_queue;
	bool loop_queue = false;
	int current_queue_pos = 0;
};