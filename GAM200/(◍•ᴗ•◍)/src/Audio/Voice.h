#pragma once

namespace FMOD
{
	class Channel;
}


class Voice
{
	friend class AudioManager;
public:

	Voice();
	~Voice();

	void SetVolume(float volume);
	void SetPause(bool paused);
	void SetLoop(bool isLoop);

	bool IsPaused();
	bool IsValid();
	bool IsLooping();
	float  GetVolume();
	FMOD::Channel* GetChannel() { return pChannel; }
private:
	FMOD::Channel* pChannel;
	float			volume;
	bool			isLooping;
	bool			isPaused;
};