/**********************************************************************************/
/*!
\file   Shader.cpp
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on AudioManager.h
*/
/*********************************************************************************/
#include <FMOD/fmod.hpp>

#include"Sound.h"
#include "Voice.h"
#include "SoundEmitter.h"
//#include "../Space/Space.h"
#include "../Scene/Scene.h"
#include "../Space/Space.h"

#include "AudioManager.h"

AudioManager* AudioManager::instance = nullptr;

AudioManager::~AudioManager()
{
	Shutdown();
}

bool AudioManager::Initialize()
{
	FMOD_RESULT result;

	result = FMOD::System_Create(&pFMOD);
	if (result != FMOD_OK)
	{
		return false;
	}

	// Initialize FMOD
	result = pFMOD->init(MAX_VOICE_COUNT, FMOD_INIT_NORMAL, 0);
	if (result != FMOD_OK)
	{
		pFMOD->release();
		pFMOD = NULL;
		return false;
	}

	AllocVoices();
	return true;
}
void AudioManager::Update()
{
	if (NULL == pFMOD)
		return;
	Space* space_to_check = SceneSys->GetMainSpace();
	if (space_to_check)
	{
		std::vector<GameObject*>audio_emitters = space_to_check->GetAllObjectsWithComponent<SoundEmiter>();
		for (auto it = audio_emitters.begin(); it != audio_emitters.end(); it++)
		{
			SoundEmiter* emitter = (*it)->GetComp<SoundEmiter>();
			emitter->Update();
		
		}
	}

	// Update FMOD -> play audio
	pFMOD->update();

	// loop through the voices
	for (auto it = usedVoices.begin(); it != usedVoices.end(); ++it)
	{
		// this voice is no longer playing
		if (!(*it)->IsValid())
		{
			// erase and push to free list
			freeVoices.push_back(*it);
			it = usedVoices.erase(it);
			if (it == usedVoices.end())
				break;
		}
	}
}

void AudioManager::LateUpdate()
{
	
}

void AudioManager::Shutdown()
{
	if (NULL == pFMOD)
		return;

	pFMOD->release();
	pFMOD = NULL;
	FreeVoices();
}

Sound* AudioManager::CreateSound(const char* filename)
{
	if (NULL == pFMOD)
		return NULL;

	// check if the sound doesn't exits already
	SoundMap::iterator it = soundResources.find(filename);
	if (it != soundResources.end())
	{
		return it->second;
	}

	// Allocate new memory for the sound
	Sound* Temp = new Sound();
	pFMOD->createSound(filename, FMOD_LOOP_NORMAL | FMOD_2D, 0, &Temp->pSound);


	// save the name of the 
	Temp->filename = filename;

	// error check
	if (Temp->pSound == NULL)
	{
		// make sure to delete the sound pointer
		delete Temp;
		return NULL;
	}

	// All is good

	// add to sound map
	soundResources[filename] = Temp;

	++soundCount;	// Stats update

	return Temp;
}
void AudioManager::FreeSound(Sound* pSound)
{
	if (NULL == pFMOD)
		return;
	if (!pSound)
		return;

	if (pSound->pSound)
	{
		pSound->pSound->release();
		pSound->pSound = 0;
	}

	// remove from sound resources
	soundResources.erase(pSound->filename);

	// Stats update
	--soundCount;
	delete pSound;
}
Voice* AudioManager::Play(Sound* pSound,float volume, bool paused)
{
	// make sure we can actually play the sound
	if (pFMOD == NULL || pSound == NULL)
		return NULL;

	// look for a free voice
	Voice* pVoice = GetFreeVoice();
	// this voice is valid
	if (pVoice)
	{
		// we found an available voice
		FMOD_RESULT res = pFMOD->playSound(pSound->pSound, 0, paused, &pVoice->pChannel);
	
	}

	// Return the voice (either NULL or correct)
	return pVoice;
}
Voice* AudioManager::Loop(Sound* pSound, bool paused)
{
	Voice* pv = Play(pSound, paused);
	if (pv)
		pv->SetLoop(true);
	return pv;
}
void AudioManager::AddSoundFile(std::string file_name)
{
	//Check for duplicates
	for (auto sound : mSoundNames)
	{
		if (sound == file_name)
			return;
	}
	mSoundNames.push_back(file_name);
}

void AudioManager::StopAll()
{
	if (NULL == pFMOD)
		return;

	// loop through the voices
	while (!usedVoices.empty())
	{
		// erase and push to free list
		usedVoices.back()->pChannel->stop();
		freeVoices.push_back(usedVoices.back());
		usedVoices.pop_back();
	}
}
void AudioManager::FreeThisVoice(Voice* pVoice)
{
	if (NULL == pFMOD)
		return;
	if (pVoice == NULL)
		return;

	pVoice->pChannel->stop();
	usedVoices.remove(pVoice);
	freeVoices.push_back(pVoice);
}

FMOD::System* AudioManager::GetFMOD()
{
	return this->pFMOD;
}
unsigned AudioManager::GetFreeVoiceCount() const
{
	return (unsigned)freeVoices.size();
}
unsigned AudioManager::GetUsedVoiceCount() const
{
	return (unsigned)usedVoices.size();
}
unsigned AudioManager::GetTotalVoiceCount() const
{
	return voiceCount;
}
const AudioManager::SoundMap& AudioManager::GetAllLoadedSounds() const
{
	return soundResources;
}
unsigned int AudioManager::GetSoundCount()
{
	return soundCount;
}

void AudioManager::AllocVoices()
{
	if (NULL == pFMOD)
		return;

	// free the voices if necessary
	FreeVoices();

	// allocate new array of voices
	voiceCount = MAX_VOICE_COUNT;
	voices = new Voice[voiceCount];

	// push all onto the free voices
	for (unsigned int i = 0; i < voiceCount; ++i)
		freeVoices.push_back(voices + i);
}
void AudioManager::FreeVoices()
{
	if (NULL == pFMOD)
		return;
	if (voices)
	{
		delete[] voices;
		voiceCount = 0;
		freeVoices.clear();
		usedVoices.clear();
	}
}
Voice* AudioManager::GetFreeVoice()
{
	if (NULL == pFMOD)
		return NULL;
	if (freeVoices.empty())
		return NULL;

	Voice* pv = freeVoices.back();
	freeVoices.pop_back();
	usedVoices.push_back(pv);

	return pv;
}