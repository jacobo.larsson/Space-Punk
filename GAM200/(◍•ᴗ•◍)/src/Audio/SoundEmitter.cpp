
#include <map>
#include <math.h>
#include <FMOD/fmod.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/constants.hpp >
#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"
#include "../OpenSaveFile/OpenSaveFile.h"
#include "../Time/Time.h"
#include "../Physics/PhysicSystem.h"

#include "Voice.h"
#include "Sound.h"

#include "AudioManager.h"
#include "AudioListener.h"
#include "../Factory/Factory.h"
#include "../Space/Space.h"
#include "../Scene/Scene.h"
#include "../Input/Input.h"

#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/ResourcesEvents/ResourcesEvents.h"

#include "SoundEmitter.h"


SoundEmiter::SoundEmiter() :mSound(nullptr)
{
	mCurrentSound = "./data/Sounds/short_sound.mp3";
	sound_queue.push_back("./data/Sounds/short_sound.mp3");
	mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
	SetName("SoundEmiter");
	EventSys->subscribe(this, DumpResource());
	mEventHandler.register_handler(*this, &SoundEmiter::OnDumpResource);
}
SoundEmiter::SoundEmiter(SoundEmiter& rhs)
{
	mCurrentSound = rhs.mCurrentSound;
	mSound->mRawData = nullptr;//AudioSys->CreateSound(rhs.sound_name.c_str());
	mVoice = nullptr;
}
SoundEmiter::~SoundEmiter()
{
	EventSys->unsubscribe(this, DumpResource());
}
SoundEmiter& SoundEmiter::operator=(const SoundEmiter& rhs)
{
	mCurrentSound = rhs.mCurrentSound;
	mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
	mVoice = nullptr;
	volume = rhs.volume;
	positional = rhs.positional;
	reverb_effect = rhs.reverb_effect;
	loop_queue = rhs.loop_queue;
	echo_reducer = rhs.echo_reducer;
	reverb_reducer = rhs.reverb_reducer;
	echo_effect = rhs.echo_effect;
	current_queue_pos = rhs.current_queue_pos;
	counter = rhs.counter;
	reverb_counter = rhs.reverb_counter;
	sound_queue.clear();
	for (auto it = rhs.sound_queue.begin(); it != rhs.sound_queue.end(); it++)
	{
		sound_queue.push_back(*it);
	}
	return *this;
}
SoundEmiter* SoundEmiter::Clone()
{
	SoundEmiter* temp = FactorySys->Create<SoundEmiter>();
	(*temp) = (*this);
	return temp;
}

void SoundEmiter::OnCreate()
{
}
void SoundEmiter::Initialize()
{
	if (mOwner != nullptr)
	{
		TransformComponent* parent_transform2 = mOwner->GetComp<TransformComponent>();
		if (parent_transform2)
		{
			associated_tansform = parent_transform2;
		}
		else
			associated_tansform = nullptr;
	}	
}
void SoundEmiter::Update()
{
	if (InputSys->key_is_down(keys::L))
	{
		Play();
		played = true;
	}

	if (InputSys->key_is_down(keys::P))
	{
		NextOnQueue(true);
	}

	if (InputSys->key_is_down(keys::Z))
	{
		PrevOnQueue(true);
	}

	if (mVoice)
	{


		UpdateVoice();
		
		
		CheckToFree();
	}

	if (reverb_effect)
	{
		UpdateReverb();
		CheckToFreeReverb();
	}

	if (echo_effect)
	{
		UpdateEchoes();
		CheckToFreeEchoes();
	}

	CheckQueueChange();

	
}
void SoundEmiter::Shutdown()
{
	mSound = nullptr;
	
	if (mVoice)
		mVoice->SetPause(true);
	mVoice = nullptr;

	echo_voices.clear();
	sound_queue.clear();
}

void SoundEmiter::Play()
{

	mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
	if (mSound->get())
	{
		
		AudioSys->FreeThisVoice(mVoice);
		mVoice = AudioSys->Play(mSound->get());
		reverb_counter = 0.f;
	}
}

float SoundEmiter::ComputeVolume(glm::vec3 emitter_pos, GameObject* listener)
{
	AudioListener* to_check = listener->GetComp<AudioListener>();
	TransformComponent* transform_to_check = listener->GetComp<TransformComponent>();
	if (to_check)
	{
		glm::vec3 distancewithoutz = transform_to_check->GetWorldPosition();
		glm::vec3 distancewithoutz2 = emitter_pos;
		distancewithoutz2.z = 0;
		distancewithoutz.z = 0;
		glm::vec3 distance = distancewithoutz - distancewithoutz2;
		glm::vec2 distance2 =glm::vec2(distance.x,distance.y);
		float length = glm::length(distance2);
		if (length <= to_check->min_range)
			return 1.f;
		else if (length >= to_check->max_range)
			return 0.f;
		else
		{
			float tparam = ( to_check->max_range - length) / (to_check->max_range - to_check->min_range);
			return tparam;
		}
	}
	else
	{
		return 1.f;
	}

}

float SoundEmiter::ComputeAngle(glm::vec3 emitter_pos, GameObject* listener)
{
	float angle = 0.f;
	AudioListener* to_check = listener->GetComp<AudioListener>();
	TransformComponent* transform_to_check = listener->GetComp<TransformComponent>();
	if (to_check)
	{
		glm::vec3 distancewithoutz = transform_to_check->GetWorldPosition();
		glm::vec3 distancewithoutz2 = emitter_pos;
		distancewithoutz2.z = 0;
		distancewithoutz.z = 0;
		glm::vec3 distance = distancewithoutz2- distancewithoutz;
		glm::vec2 distance2 = glm::vec2(distance.x, distance.y);
		angle = glm::orientedAngle(glm::normalize(distance2), glm::vec2(1.f, 0));
		float deg = angle * (180.f / 3.14f);
		
	}

	return angle;
}

glm::vec2 SoundEmiter::ComputeMixes(glm::vec3 emitter_pos, GameObject* listener)
{
	glm::vec2 mixes(1.f);
	AudioListener* to_check = listener->GetComp<AudioListener>();
	TransformComponent* transform_to_check = listener->GetComp<TransformComponent>();
	if (to_check)
	{
		glm::vec3 distancewithoutz = transform_to_check->GetWorldPosition();
		glm::vec3 distancewithoutz2 = emitter_pos;
		distancewithoutz2.z = 0;
		distancewithoutz.z = 0;
		glm::vec3 distance = distancewithoutz2 - distancewithoutz;
		glm::vec2 distance2 = glm::vec2(distance.x, distance.y);


		float angle = glm::orientedAngle(glm::normalize(distance2), glm::vec2(1.f, 0));
		float angledeg = angle * (180.f / glm::pi<float>());
		glm::vec2 norm = glm::normalize(distance2);


		float test_ang = acos(glm::dot(norm, glm::vec2(1.f, 0)));
		float angledeg2 = test_ang * (180.f / glm::pi<float>());




		if (distance2.y < 0.f && distance2.x < 0.f && angledeg2 >180.f)
		{
			angledeg2 += 90.f;
		}
		else if (distance2.y < 0.f && distance2.x > 0.f && angledeg2 > 180.f) //correct
		{
			angledeg2 = 360.f - angledeg2;
		}
		float mix_leftx;
		float mix_rightx;

		//Emitter at left

		if(angledeg2>=45.f && angledeg2 <= 135.f || angledeg2 >= 225.f && angledeg2 <= 315.f)
		{ 
			mix_leftx = cos(glm::pi<float>() / 4);
			mix_rightx = cos(glm::pi<float>() / 4);
		}
		else
		{
			if (distancewithoutz2.x <= distancewithoutz.x)
			{
				mix_leftx = cos(angle) * -1;
				//mix_rightx = 1 - cos(angle) * -1;
				mix_rightx = abs(sin(angle));

			}
			else //Emitter at right
			{
				mix_leftx = abs(sin(angle));
				mix_rightx = cos(angle);
			}
		}
		mixes = glm::vec2(mix_leftx, mix_rightx);

	}

	return mixes;
}

void SoundEmiter::UpdateVoice()
{
	mVoice->SetPause(false);
	mVoice->SetLoop(false);

	if (Physys->slowmo == true)
	{

		GetVoice()->GetChannel()->setFrequency(36000);
	}
	else
	{
		GetVoice()->GetChannel()->setFrequency(48000);
		
	}

	if (positional)
	{
		
		Space* space_to_check = SceneSys->GetMainSpace();
		if (space_to_check)
		{
			std::vector<GameObject*>audio_listeners = space_to_check->GetAllObjectsWithComponent<AudioListener>();
			for (auto it = audio_listeners.begin(); it != audio_listeners.end(); it++)
			{
				volume = ComputeVolume(associated_tansform->GetWorldPosition(), *it);

				mVoice->SetVolume(volume);
				float angle = ComputeAngle(associated_tansform->GetWorldPosition(), *it);

				
				glm::vec2 test = ComputeMixes(associated_tansform->GetWorldPosition(), *it);

			
				mVoice->GetChannel()->setMixLevelsOutput(test.x, test.y, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
			}

		}
	}
	else
	{
		mVoice->SetVolume(volume);
		mVoice->GetChannel()->setMixLevelsOutput(1.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f);
		
	}
	
}

void SoundEmiter::UpdateEchoes()
{

	if(  mVoice || echo_voices.size() != 0)
	counter += (float)TimeSys->GetFrameTime();

	if (counter >= time_of_delay)
	{
		float prevvol;
		if (echo_voices.size() != 0)
			prevvol = echo_voices.back().first;
		else
			prevvol = volume;
		float newvolume = prevvol * (1-echo_reducer);

		if (prevvol >= 0.08)
		{
			mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
			if (mSound->get())
			{
				Voice * to_add =AudioSys->Play(mSound->get());

				echo_voices.push_back(std::make_pair(newvolume, to_add));
			}
		}
		counter = 0.f;

	}

	for (auto it = echo_voices.begin(); it != echo_voices.end(); it++)
	{

		it->second->SetPause(false);
		it->second->SetLoop(false);


		if (positional)
		{

			Space* space_to_check = SceneSys->GetMainSpace();
			if (space_to_check)
			{
				std::vector<GameObject*>audio_listeners = space_to_check->GetAllObjectsWithComponent<AudioListener>();
				for (auto et = audio_listeners.begin(); et != audio_listeners.end(); et++)
				{
					
					float newvol = ComputeVolume(associated_tansform->GetWorldPosition(), *et);

					it->second->SetVolume(newvol * it->first);
					float angle = ComputeAngle(associated_tansform->GetWorldPosition(), *et);


					glm::vec2 test = ComputeMixes(associated_tansform->GetWorldPosition(), *et);


					it->second->GetChannel()->setMixLevelsOutput(test.x, test.y, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
				}

			}
		}
		else
		{
			it->second->SetVolume(volume * it->first);
			it->second->GetChannel()->setMixLevelsOutput(1.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f);

		}
	}

}

void SoundEmiter::UpdateReverb()
{


	if (mVoice && reverb_mVoice == nullptr)
		reverb_counter += (float)TimeSys->GetFrameTime();

	if (reverb_counter >= 0.16f)
	{
		mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
		if (mSound->get())
		{
			reverb_mVoice = AudioSys->Play(mSound->get());
		}
		
		reverb_counter = 0.f;
	}

	if (reverb_mVoice)
	{
		reverb_mVoice->SetPause(false);
		reverb_mVoice->SetLoop(false);
		if (positional)
		{

			Space* space_to_check = SceneSys->GetMainSpace();
			if (space_to_check)
			{
				std::vector<GameObject*>audio_listeners = space_to_check->GetAllObjectsWithComponent<AudioListener>();
				for (auto it = audio_listeners.begin(); it != audio_listeners.end(); it++)
				{

					float newvolume = ComputeVolume(associated_tansform->GetWorldPosition(), *it);

					newvolume = volume * reverb_reducer;
					reverb_mVoice->SetVolume(newvolume);



					glm::vec2 test = ComputeMixes(associated_tansform->GetWorldPosition(), *it);


					reverb_mVoice->GetChannel()->setMixLevelsOutput(test.x, test.y, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
				}

			}
		}
		else
		{

			float newvolume = volume * reverb_reducer;
			reverb_mVoice->SetVolume(newvolume);
			reverb_mVoice->GetChannel()->setMixLevelsOutput(1.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f);

		}
	}
}

void SoundEmiter::NextOnQueue(bool play)
{
	if (played)
	{


		if (mVoice)
		{
			AudioSys->FreeThisVoice(mVoice);
			mVoice = nullptr;
		}
		for (auto it = echo_voices.begin(); it != echo_voices.end(); it++)
		{
			AudioSys->FreeThisVoice(it->second);
		}
		echo_voices.clear();
		if (reverb_mVoice)
		{

			AudioSys->FreeThisVoice(reverb_mVoice);
			reverb_mVoice = nullptr;

		}

		auto it = std::find(sound_queue.begin(), sound_queue.end(), mSound->RelPath);
		auto et = it;
		et++;
		if (et == sound_queue.end())
		{
			mCurrentSound = *sound_queue.begin();

			mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
			if (play)
				Play();
		}
		else
		{
			mCurrentSound = *et;

			mSound = RsrcMan->GetResourceType<Sound>(*et);

			if (play)
				Play();
		}
		counter = 0.f;
		reverb_counter = 0.f;
	}
}

void SoundEmiter::PrevOnQueue(bool play)
{
	if (played)
	{
		if (mVoice)
		{
			AudioSys->FreeThisVoice(mVoice);
			mVoice = nullptr;
		}
		for (auto it = echo_voices.begin(); it != echo_voices.end(); it++)
		{
			AudioSys->FreeThisVoice(it->second);
		}
		echo_voices.clear();
		if (reverb_mVoice)
		{

			AudioSys->FreeThisVoice(reverb_mVoice);
			reverb_mVoice = nullptr;

		}

		auto it = std::find(sound_queue.begin(), sound_queue.end(), mSound->RelPath);
		auto et = it;

		if (et == sound_queue.begin())
		{
			auto ot = sound_queue.end();
			ot--;
			mCurrentSound = *ot;

			mSound = RsrcMan->GetResourceType<Sound>(*ot);
			if (play)
				Play();
		}
		else
		{
			et--;
			mCurrentSound = *et;

			mSound = RsrcMan->GetResourceType<Sound>(*et);

			if (play)
				Play();
		}
	}
}

void SoundEmiter::AddToQueue(const std::string& to_add)
{
	auto it = std::find(sound_queue.begin(), sound_queue.end(), to_add);
	if (it == sound_queue.end())
	{
		sound_queue.push_back(to_add);
	}
}

void SoundEmiter::RemoveFromQueue(const std::string& to_add)
{
	if (to_add == mSound->RelPath)
		return;

	auto it = std::find(sound_queue.begin(), sound_queue.end(), to_add);
	if (it != sound_queue.end())
	{
		sound_queue.erase(it);
	}
}

void SoundEmiter::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["sound_name"] = mCurrentSound;
	if (mSound)
		j["CurrentSound"] = mSound->RelPath;
	j["volume"] = volume;
	j["positional"] = positional;
	j["echo_effect"] = echo_effect;
	j["time_of_delay"] = time_of_delay;
	j["echo_reducer"] = echo_reducer;
	j["reverb_effect"] = reverb_effect;
	j["reverb_reducer"] = reverb_reducer;
	j["loop_queue"] = loop_queue;
	

	if (sound_queue.size())
	{
		json& msoundsJson = j["queue_sounds"];
		for (auto& sound : sound_queue)
		{
		
			msoundsJson.push_back(sound);
		}

	}
}
void SoundEmiter::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	mCurrentSound = j["sound_name"].get<std::string>();

	if (j.find("CurrentSound") != j.end())
		SetSound(j["CurrentSound"].get<std::string>());

	if (j.find("volume") != j.end())
		volume = j["volume"];

	if (j.find("positional") != j.end())
		positional = j["positional"];



	if (j.find("echo_effect") != j.end())
		echo_effect = j["echo_effect"];

	if (j.find("time_of_delay") != j.end())
		time_of_delay = j["time_of_delay"];

	if (j.find("echo_reducer") != j.end())
		echo_reducer = j["echo_reducer"];

	if (j.find("reverb_effect") != j.end())
		reverb_effect = j["reverb_effect"];

	if (j.find("reverb_reducer") != j.end())
		reverb_reducer = j["reverb_reducer"];

	if (j.find("loop_queue") != j.end())
		loop_queue = j["loop_queue"];

	sound_queue.clear();
	auto sounds = j.find("queue_sounds");
	if (sounds != j.end())
	{
		for (auto& sound : *sounds)
		{
			sound_queue.push_back(sound);
			
		}
	}

	if(sound_queue.empty())
	{
		if (j.find("CurrentSound") != j.end())
			sound_queue.push_back(j["CurrentSound"].get<std::string>());
		
	}

}

Resource<Sound>* SoundEmiter::GetSound()
{
	return mSound;
}

Voice* SoundEmiter::GetVoice()
{
	return mVoice;
}

void SoundEmiter::SetSound(std::string file_name)
{
	mCurrentSound = file_name;
	mSound = RsrcMan->GetResourceType<Sound>(mCurrentSound);
}

bool SoundEmiter::Edit()
{
	bool changed = false;
	if (ImGui::Button("Add Sound"))
	{
		std::string filename; // default;
		OpenSaveFileDlg dlg;
		if (dlg.Open("Open Sound")) {
			filename = dlg.GetFiles().front();

		}
		size_t found = filename.find("data");
		if (found != std::string::npos)
		{
			std::string slash = filename.substr(found);
			filename = std::string("./") + filename.substr(found, 4) + std::string("/") + filename.substr(found + 5);
			auto extension = RsrcMan->GetExtensionOfFile(filename);
			if (extension == TypeSound)
				RsrcMan->Load(filename);
		}
	}

	auto ot = RsrcMan->Resourcesmap[TypeSound].begin();
	auto ut = RsrcMan->Resourcesmap[TypeSound].find(mCurrentSound);
	static int soundIndex = (int)std::distance(RsrcMan->Resourcesmap[TypeSound].begin(), RsrcMan->Resourcesmap[TypeSound].find(mCurrentSound));
	static std::string active_str_sound = RsrcMan->Resourcesmap[TypeSound].find(mCurrentSound)->first.c_str();

	auto it = RsrcMan->Resourcesmap[TypeSound].begin();

	ImGui::PushID("CurrentSound");
	if (ImGui::BeginCombo("Sound", active_str_sound.c_str(), 0))
	{
		for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeSound].size(); i++, it++)
		{

			if (ImGui::Selectable(it->first.c_str(), soundIndex == i))
			{
				soundIndex = i;
				active_str_sound = it->first.c_str();
				SetSound(it->first.c_str());
				changed = true;

				auto ot = std::find(sound_queue.begin(), sound_queue.end(), it->first);
				if (ot != sound_queue.end())
					sound_queue.erase(ot);
				
				sound_queue[0] = it->first;

			}

			if (soundIndex == i)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}

	ImGui::PopID();
	ImGui::Checkbox("Positional sound", &positional);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;


	ImGui::DragFloat("Volume", &volume,0.01f,0.f,1.f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;


	ImGui::Checkbox("Echo effect", &echo_effect);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	if (echo_effect)
	{
		ImGui::DragFloat("Echo time of delay", &time_of_delay, 0.01f, 0.f, 0.f);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::DragFloat("Echo reducer", &echo_reducer, 0.01f, 0.f, 1.f);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
	}

	ImGui::Checkbox("Reverberation effect", &reverb_effect);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	if (reverb_effect)
	{

		ImGui::DragFloat("Reverberation reducer", &reverb_reducer, 0.01f, 0.f, 1.f);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
	}
	
	ImGui::Checkbox("Loop queue", &loop_queue);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	
	if (ImGui::CollapsingHeader("Add To Queue"))
	{
		ImGui::PushID("Sounds to add");
		static unsigned sound_to_add_index = 0;
		auto it = RsrcMan->Resourcesmap[TypeSound].begin();
		if (ImGui::BeginCombo("Sounds to add", RsrcMan->Resourcesmap[TypeSound].begin()->first.c_str(), 0))
		{
			for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeSound].size(); i++,it++)
			{
				if (ImGui::Selectable(it->first.c_str(), sound_to_add_index == i))
				{
					changed = true;
					sound_to_add_index = i;
					AddToQueue(it->first.c_str());

				}
			}
			ImGui::EndCombo();

		}
		ImGui::PopID();
	}

	if (ImGui::CollapsingHeader("Remove From Queue"))
	{
		ImGui::PushID("Sounds to remove");
		static unsigned sound_to_remove_index = 0;
		auto it = RsrcMan->Resourcesmap[TypeSound].begin();
		if (ImGui::BeginCombo("Sounds to remove", RsrcMan->Resourcesmap[TypeSound].begin()->first.c_str(), 0))
		{
			for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeSound].size(); i++, it++)
			{
				if (ImGui::Selectable(it->first.c_str(), sound_to_remove_index == i))
				{
					changed = true;
					sound_to_remove_index = i;
					RemoveFromQueue(it->first.c_str());

				}
			}
			ImGui::EndCombo();

		}
		ImGui::PopID();
	}

	ImGui::Text("Sounds on queue");
	for (auto it = sound_queue.begin(); it != sound_queue.end(); it++)
	{
		ImGui::Text(it->c_str());
	}




	return changed;
}

void SoundEmiter::OnDumpResource(const DumpResource& _event)
{
	std::string a = mSound->RelPath;
	auto it = std::find(RsrcMan->current_level_resources.begin(), RsrcMan->current_level_resources.end(), a);
	if (it == RsrcMan->current_level_resources.end())
		RsrcMan->current_level_resources.push_back(mSound->RelPath);
}

void SoundEmiter::CheckToFree()
{
	
	if (mVoice->IsValid() == false)
	{
		AudioSys->FreeThisVoice(mVoice);
		mVoice = nullptr;
	}
	

}

void SoundEmiter::CheckToFreeEchoes()
{
	for (auto it = echo_voices.begin();it!=echo_voices.end();it++)
	{
		if (it->second->IsValid() == false)
		{
			AudioSys->FreeThisVoice(it->second);
			it->second = nullptr;

			echo_voices.erase(it);
			break;
		}
	}

}

void SoundEmiter::CheckToFreeReverb()
{
	if (reverb_mVoice)
	{
		if (reverb_mVoice->IsValid() == false)
		{
			AudioSys->FreeThisVoice(reverb_mVoice);
			reverb_mVoice = nullptr;
		}
	}
}

void SoundEmiter::CheckQueueChange()
{
	if (played)
	{
		if (mVoice == nullptr && reverb_mVoice == nullptr)
		{
			
			for(auto it = echo_voices.begin();it!= echo_voices.end();it++)
			{ 
				if (it->second != nullptr)
				{
					return;
				}
			}

			

			auto ot = std::find(sound_queue.begin(), sound_queue.end(), mSound->RelPath);
			auto mt =ot;

			mt++;
			if (mt == sound_queue.end())
			{
				if (loop_queue)
				{
					NextOnQueue();
					Play();

				}
				else
				{
					NextOnQueue();
					played = false;
				}
				
			}
			else
			{
					NextOnQueue();
					Play();
			}

		}
		
	}
}
