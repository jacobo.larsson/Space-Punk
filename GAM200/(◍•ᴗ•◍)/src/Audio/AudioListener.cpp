#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include <glm/gtc/matrix_transform.hpp>

#include "../Time/Time.h"
#include "../Factory/Factory.h"

#include "../Transform2D/TransformComponent.h"
#include "../GameObject/GameObject.h"
#include "AudioListener.h"

int AudioListener::listener_counters = 0;
void AudioListener::Initialize()
{
	if (mOwner != nullptr)
	{
		if (listener_counters==1)
		{
			mOwner->RemoveComp(this);
			std::cout << "Error, only 1 possible listener " << mOwner->GetName() << std::endl;
			return;
			
		}
		listener_counters++;
		IComp* parent_transform2 = mOwner->GetComp<TransformComponent>();
		if (parent_transform2)
		{
			associated_tansform = dynamic_cast<TransformComponent*>(parent_transform2);

		}
		else
			associated_tansform = nullptr;
	}


}

void AudioListener::Update()
{
}

void AudioListener::Shutdown()
{
	listener_counters--;
}

bool AudioListener::Edit()
{
	bool changed = false;
	ImGui::PushID("Ranges");
	ImGui::Columns(2, "Range", true);
	ImGui::DragFloat("Min range", &min_range);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("Max range", &max_range);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();
	return changed;
}

AudioListener::AudioListener()
{
	SetName("AudioListener");
	
	max_range = 0.f;
	min_range = 0.f;
}

AudioListener::~AudioListener()
{
}

AudioListener& AudioListener::operator=(const AudioListener& rhs)
{
	return *this;
}

void AudioListener::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	using nlohmann::json;
	j["max_range"] = max_range;
	j["min_range"] = min_range;
}

void AudioListener::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	if (j.find("max_range") != j.end())
		max_range = j["max_range"];

	if (j.find("min_range") != j.end())
		min_range = j["min_range"];
}

AudioListener* AudioListener::Clone()
{
	AudioListener* temp = FactorySys->Create<AudioListener>();
	(*temp) = (*this);
	return temp;
}
