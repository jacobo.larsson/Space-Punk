#include <FMOD/fmod.hpp>
#include "Voice.h"
Voice::Voice() : pChannel(nullptr), isLooping(false), isPaused(true), volume(1.0f)
{}
Voice::~Voice()
{}

void Voice::SetVolume(float vol)
{
	if (pChannel == nullptr)
		return;

	volume = vol;

	if (volume < 0.0f)
		volume = 0.0f;
	else if (volume > 1.0f)
		volume = 1.0f;

	pChannel->setVolume(volume);
}
void Voice::SetPause(bool paused)
{
	if (pChannel == nullptr)
		return;
	isPaused = paused;
	pChannel->setPaused(paused);
}
void Voice::SetLoop(bool isLoop)
{
	if (pChannel == nullptr)
		return;

	isLooping = isLoop;
	if (isLooping)
		pChannel->setLoopCount(-1);
	else
		pChannel->setLoopCount(0);
}

bool Voice::IsPaused()
{
	return isPaused;
}
bool Voice::IsValid()
{
	if (pChannel == nullptr)
		return false;

	bool pl;
	pChannel->isPlaying(&pl);
	return pl;
}
bool Voice::IsLooping()
{
	return isLooping;
}
float Voice::GetVolume()
{
	return volume;
}