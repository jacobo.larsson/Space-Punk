#pragma once
#include <string>

#include <FMOD/fmod.hpp>

namespace FMOD
{
	class Sound;
}

struct Sound
{
	FMOD::Sound* pSound = nullptr;
	std::string  filename;
};