/**********************************************************************************/
/*!
\file   AudioManager.h
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the necessary functions to have a playing sound
*/
/*********************************************************************************/
#pragma once
// STL includes
#include <list>
#include <map>
//

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "../ResourceManager/ResourceManager.h"




//#include "SoundEmitter.h"

// Forward declare the FMOD classes
// so that I don't have to include fmod.hpp 
// in Audio.h, and therefore create an unnecessary 
// dependency. 
namespace FMOD
{
	class System;
}
struct Sound;
class Voice;
class SoundEmiter;
//class Space;

class AudioManager : public IBase
{
	friend class Editor;
	friend class Menus;
	friend class SoundEmiter;

	SM_RTTI_DECL(AudioManager, IBase);
	Make_Singleton(AudioManager)
	typedef std::map<std::string, Sound*> SoundMap;
public:

	~AudioManager();

	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();

	Sound* CreateSound(const char* filename);
	void FreeSound(Sound* pSound);
	Voice* Play(Sound* pSound,float volume=1.f, bool paused = false);
	Voice* Loop(Sound* pSound, bool paused = false);
	void AddSoundFile(std::string file_name);

	void StopAll();
	void FreeThisVoice(Voice* pVoice);

	FMOD::System* GetFMOD();
	unsigned GetFreeVoiceCount() const;
	unsigned GetUsedVoiceCount() const;
	unsigned GetTotalVoiceCount() const;
	const SoundMap& GetAllLoadedSounds() const;
	unsigned int GetSoundCount();


	unsigned int soundCount = 0;
private:
	void AllocVoices();
	void FreeVoices();
	Voice* GetFreeVoice();

	FMOD::System* pFMOD = NULL;
	std::vector<std::string> mSoundNames;
	std::list<Voice*> freeVoices;
	std::list<Voice*> usedVoices;
	Voice* voices = 0;
	SoundMap soundResources;
	const unsigned MAX_VOICE_COUNT = 100;
	unsigned int voiceCount = 100;
};

// GLOBAL Audio Manager
#define AudioSys AudioManager::Get()