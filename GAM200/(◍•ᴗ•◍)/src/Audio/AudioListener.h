/**********************************************************************************/
/*
\file   AudioListener.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   26/11/2020
\brief
Audio Listener class.
*/
/*********************************************************************************/
#pragma once

#include <glm/glm.hpp>

#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"

#include "../Transform2D/TransformComponent.h"

class AudioListener : public IComp
{
	SM_RTTI_DECL(AudioListener, IComp);
public:

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	AudioListener();
	~AudioListener();

	AudioListener& operator=(const AudioListener& rhs);

	static int listener_counters;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	AudioListener* Clone()override;

	TransformComponent* associated_tansform = nullptr;
	float max_range = 0.f;
	float min_range = 0.f;
};