/**********************************************************************************/
/*
\file   IComp.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the initialize, update and shutdown of the components
*/
/*********************************************************************************/
#pragma once
#include "IBase.h"
#include "SM_RTTI.h"
#include "../EventSystem/Listener/Listener.h"
#include "../EventSystem/EventHandler.h"
class GameObject;
class Transform2D;
class DumpResource;

class IComp : public IBase, public Listener
{
	friend class GameObject;
	SM_RTTI_DECL(IComp, IBase);
public:

	//IComp() :mOwner{ nullptr }, IBase() {}
	IComp();
	virtual ~IComp();

	virtual void OnCreate(){}
	virtual void Initialize(){}
	virtual void Update(){}
	virtual void LateUpdate(){}
	virtual void Shutdown(){}
	virtual IComp* Clone() { return new IComp(*this); }

	virtual bool Edit() { return false; }

	virtual void OnCollisionStarted(GameObject* other) {}
	virtual void OnCollisionPersisted(GameObject* other) {}
	virtual void OnCollisionEnded(GameObject* other) {}


	bool SetEnabled(bool enabled) { return mEnabled = enabled; }
	unsigned GetOrderKey();
	unsigned SetOrderKey(unsigned _Key);


	GameObject* SetOwner(GameObject* owner) { return mOwner = owner; }
	 	GameObject* GetOwner() { return mOwner; }



#pragma region Events
	//void handle_event(const Event& _event)override;
	EVENTHANDLER_DECL
		virtual void OnDumpResource(const DumpResource& _event);
protected:
	GameObject* mOwner;
	unsigned mOrderKey;
};