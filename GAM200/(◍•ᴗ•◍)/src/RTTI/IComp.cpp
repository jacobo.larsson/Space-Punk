#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/ResourcesEvents/ResourcesEvents.h"
#include "../Settings/Settings.h"
#include "IComp.h"
IComp::IComp() :mOwner{ nullptr }, IBase() 
{
	SetName("IComp");
	EventSys->subscribe(this, DumpResource());
	mEventHandler.register_handler(*this, &IComp::OnDumpResource);
}
IComp::~IComp()
{
	EventSys->unsubscribe(this, DumpResource());
}
unsigned IComp::GetOrderKey()
{
	return mOrderKey;
}
unsigned IComp::SetOrderKey(unsigned _Key)
{
	return mOrderKey = _Key;
}
void IComp::OnDumpResource(const DumpResource& _event)
{
}
