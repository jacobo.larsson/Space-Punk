/**********************************************************************************/
/*
\file   SM_RTTI.h
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the RTTI with the parent and child relation functions.
*/
/*********************************************************************************/
#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
class SM_Rtti
{
private:
    std::string mTypeName;
    SM_Rtti* mParentRtti = nullptr;
    std::map<std::string, SM_Rtti*> mChildren;
public:
    const std::string& GetName() const;
    const std::string& GetParentTypeName()const;
    bool IsDerivedFrom(const SM_Rtti& otherType)const;

    // Static interface
private:
    static std::map<std::string, SM_Rtti> sAllTypes; // main storage for Rtti
    static std::vector<SM_Rtti*> sRootTypes;
public:
    static const SM_Rtti& AddRtti(const char* thisTypeName, const char* parentTypeName = nullptr);
    static void PrintAllRtti();

    template<typename T> static std::string ParseTypeName(bool removeNamespaces = true)
    {
        std::string tName = typeid(T).name();
        std::size_t pos = tName.find_last_of(' ');
        tName = tName.substr(pos + 1);
        if (removeNamespaces) {
            pos = tName.find_last_of(':');
            if (pos != std::string::npos) {
                tName = tName.substr(pos + 1);
            }
        }
        return tName;
    }
};

// Maccros used to declare RTTI types within class declarations. 
#define RTTI_DECL_BASE(classType)\
public:\
virtual const SM_Rtti& GetType() {\
    return SM_Rtti::AddRtti(SM_Rtti::ParseTypeName<classType>().c_str());\
}\
static const SM_Rtti& TYPE() {\
    return SM_Rtti::AddRtti(SM_Rtti::ParseTypeName<classType>().c_str());\
}

#define SM_RTTI_DECL(classType, parentType)\
public:\
virtual const SM_Rtti& GetType() {\
return SM_Rtti::AddRtti(SM_Rtti::ParseTypeName<classType>().c_str(), SM_Rtti::ParseTypeName<parentType>().c_str());\
}\
static const SM_Rtti& TYPE() {\
    return SM_Rtti::AddRtti(SM_Rtti::ParseTypeName<classType>().c_str(), SM_Rtti::ParseTypeName<parentType>().c_str());\
}