/**********************************************************************************/
/*
\file   RTTI.cpp
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions of SM_RTTI.h
*/
/*********************************************************************************/
#include "SM_RTTI.h"

// STATIC INTERFACE
std::map<std::string, SM_Rtti> SM_Rtti::sAllTypes; // main storage for Rtti
std::vector<SM_Rtti*> SM_Rtti::sRootTypes;

const SM_Rtti& SM_Rtti::AddRtti(const char* thisTypeName, const char* parentTypeName)
{
    bool typeExists = sAllTypes.find(thisTypeName) != sAllTypes.end();

    SM_Rtti& newRtti = sAllTypes[thisTypeName];
    newRtti.mTypeName = thisTypeName;
    // base class
    if (parentTypeName == nullptr)
    {
        if (!typeExists)
            sRootTypes.push_back(&newRtti);
    }
    // a derived class
    else
    {
        // find the parent
        auto it = sAllTypes.find(parentTypeName);

        // parent not yet created
        if (it == sAllTypes.end())
        {
            // add the parent to the list
            SM_Rtti& parentRtti = sAllTypes[parentTypeName];
            parentRtti.mTypeName = parentTypeName;

            // update the iterator
            it = sAllTypes.find(parentTypeName);
        }

        // add to the parent the new rtti
        newRtti.mParentRtti = &it->second;
        it->second.mChildren[thisTypeName] = &newRtti;
    }

    return newRtti;
}
void SM_Rtti::PrintAllRtti()
{
    std::vector<std::pair<SM_Rtti*, std::string>> stack;

    // add the root types to the stack
    for (auto rtti : sRootTypes)
        stack.push_back({ rtti, "" });

    // traverse all the Rtti tree
    // recursion in iterative form
    while (stack.size())
    {
        SM_Rtti* rtti = stack.back().first;
        std::string indent = stack.back().second;
        stack.pop_back();

        std::cout << indent << rtti->mTypeName << "\n";
        indent += "->";
        for (auto c : rtti->mChildren)
            stack.push_back({ c.second, indent });
    }
}

// INSTANCE INTERFACE 
const std::string& SM_Rtti::GetName() const
{
    return mTypeName;
}
const std::string& SM_Rtti::GetParentTypeName()const
{
    static std::string nullStr = "null";
    if (mParentRtti)return mParentRtti->GetName(); return nullStr;
}
bool SM_Rtti::IsDerivedFrom(const SM_Rtti& otherType)const
{
    SM_Rtti* parent = mParentRtti;
    while (parent) {
        if (parent == &otherType)
            return true;
        // go to next parent
        parent = parent->mParentRtti;
    }
    return false;
}