/**********************************************************************************/
/*
\file   IBase.cpp
\author Jaime Sanchez & David Miranda & Juan Pacheco & Maria Bola�os
\par    email: jaime.s@digipen.edu & m.david@digipen.edu & pacheco.j@digipen.edu & maria.bolanos@digipen.edu
\par    DigiPen login: jaime.s & m.david & pacheco.j & maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of  the functions in IBase.h
*/
/*********************************************************************************/
#include "IBase.h"

const std::string& IBase::GetName()
{
	return mName;
}

void IBase::SetName(const std::string& name)
{
	mName = name;
}

unsigned IBase::GetUID()
{
	return mUID;
}

bool &IBase::Enabled()
{
	return mEnabled;
}

bool IBase::SetEnabled(bool _enabled)
{
	return mEnabled = _enabled;
}


void IBase::ToJson(nlohmann::json& j)
{
	j["name"] = mName;
	j["enabled"] = mEnabled;
	j["mUID"] = mUID;
}
void IBase::FromJson(const nlohmann::json& j)
{
	if(j.find("name") != j.end())
		mName = j["name"].get<std::string>();
	if (j.find("enabled") != j.end())
		mEnabled = j["enabled"];

	if (j.find("mUID") != j.end())
		mUID = j["mUID"];
}

IBase* IBase::Clone() const
{
	return new IBase(*this);
}