/**********************************************************************************/
/*
\file   IBase.h
\author Jaime Sanchez & David Miranda & Juan Pacheco & Maria Bola�os
\par    email: jaime.s@digipen.edu & m.david@digipen.edu & pacheco.j@digipen.edu & maria.bolanos@digipen.edu
\par    DigiPen login: jaime.s & m.david & pacheco.j & maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the Base class with its json read and write.
*/
/*********************************************************************************/
#pragma once
#include <string>
#include "SM_RTTI.h"
#include "../Extern/json/json.hpp"
class ISerializer;
class IBase
{
    friend class Editor;
    friend class Factory;
    RTTI_DECL_BASE(IBase);

public:
    virtual ~IBase() {}

    void SetName(const std::string& name);
    virtual bool SetEnabled(bool _enabled);
    const std::string& GetName();
    bool& Enabled();
    unsigned GetUID();

    virtual void ToJson(nlohmann::json& j);
    virtual void FromJson(const nlohmann::json& j);

    virtual IBase* Clone() const;

    unsigned         mUID = -1;
protected:
    IBase() :mEnabled{ true } {}

    std::string mName;
    bool mEnabled;
    
};