#pragma once 


#include "../RTTI/SM_RTTI.h"
#include "../Extern/json/json.hpp"
#include "../LogicSystem/LogicComponent.h"

class Health;

class Cheats : public LogicComponent
{
	SM_RTTI_DECL(Cheats, LogicComponent);

public:

	Cheats();
	~Cheats();
	Cheats& operator=(const Cheats& rhs);
	Cheats* Clone() override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	bool Edit() override;

private:

	Health* health = nullptr;
	bool Inmortal = false;
	float actual_health = 0.0f;
};