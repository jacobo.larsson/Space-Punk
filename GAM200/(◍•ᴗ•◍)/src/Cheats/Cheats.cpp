#include "../Extern/ImGui/imgui.h"

#include "../Factory/Factory.h"
#include "../Game/Health/Health.h"
#include "../RTTI/IComp.h"
#include "../GameObject/GameObject.h"
#include "../Input/Input.h"

#include "Cheats.h"


Cheats::Cheats()
{
	mName = "Cheats";
}
Cheats::~Cheats(){}
Cheats& Cheats::operator=(const Cheats& rhs)
{
	Inmortal = rhs.Inmortal;
	return *this;
}
Cheats* Cheats::Clone()
{
	Cheats* temp = FactorySys->Create<Cheats>();
	(*temp) = (*this);
	return temp;
}

void Cheats::Initialize()
{
	if (mOwner)
	{
		health = mOwner->GetComp<Health>();
		if(health)
			health->can_get_damaged = !Inmortal;
	}

	LogicComponent::Initialize();
}
void  Cheats::Update()
{
	if (InputSys->key_is_down(keys::I) && InputSys->key_is_triggered(keys::LeftControl))
	{
		Inmortal = !Inmortal;
		if (health)
			health->can_get_damaged = !Inmortal;
	}
}
void  Cheats::Shutdown()
{
	LogicComponent::Shutdown();
}

void  Cheats::ToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);
	j["Inmortal"] = Inmortal;
}
void  Cheats::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	if (j.find("Inmortal") != j.end())
		Inmortal = j["Inmortal"];
}

bool  Cheats::Edit()
{
	bool changed = false;
	ImGui::Checkbox("Inmotal", &Inmortal);
	if (ImGui::IsItemDeactivatedAfterEdit())
	{
		changed = true;
		if (health)
			health->can_get_damaged = !Inmortal;
	}
	return changed;
}