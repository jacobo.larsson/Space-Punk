/**********************************************************************************/
/*
\file   RigidBody.cpp
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions declared in RigidBody.h
*/
/*********************************************************************************/
#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include <glm/gtc/matrix_transform.hpp>

#include "../Time/Time.h"
#include "../Factory/Factory.h"
#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/RigidBodyEvents/RigidBodyEvents.h"

#include "../Transform2D/TransformComponent.h"
#include "../GameObject/GameObject.h"
#include "../Collisions/ICollider.h"


#include "RigidBody.h"
#include "PhysicSystem.h"

RigidBody::RigidBody()
	: InvMass(1.0f),
	gravity(0, -0.2f),
	mDirection(0.0f),
	drag(0.990f),rotationlocked(true)
{
	mName = "RigidBody";
	
}

RigidBody::RigidBody(RigidBody& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;

	InvMass = rhs.InvMass;
	Velocity = rhs.Velocity;
	drag = rhs.drag;

	gravity = rhs.gravity;
	Acceleration = rhs.gravity;
	PrevPosition = rhs.gravity;
	SumForces = rhs.gravity;
	rotationlocked = rhs.rotationlocked;
	Transform = rhs.Transform->Clone();
}

RigidBody& RigidBody::operator=(const RigidBody& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;

	InvMass = rhs.InvMass;
	Velocity = rhs.Velocity;
	drag = rhs.drag;

	gravity = rhs.gravity;
	Acceleration = rhs.gravity;
	PrevPosition = rhs.gravity;
	SumForces = rhs.gravity;
	rotationlocked = rhs.rotationlocked;

	return *this;
}

RigidBody::~RigidBody()
{
	EventSys->unsubscribe(this, CollisionGroupUpdated());
}
void RigidBody::Initialize()
{
	if (mOwner != nullptr)
	{	
		IComp* parent_transform2 = mOwner->GetComp<TransformComponent>();
		if (parent_transform2)
		{
			Transform = dynamic_cast<TransformComponent*>(parent_transform2);
			PrevOrientation = Transform->GetWorldOrientation();
		}
		else
			Transform = nullptr;
	}
	

}

void RigidBody::Update()
{
	// add gravity
	if (InvMass)
		AddForce(gravity / InvMass);

	// Integrate physics
	Integrate((float)TimeSys->GetFrameTime());

}

void RigidBody::Shutdown()
{
}

bool RigidBody::Edit()
{
	bool changed = false;
	if (ImGui::TreeNode("RigidBody"))
	{
		ImGui::Text("InvMass");
		ImGui::DragFloat("InvMass", &InvMass, 1.0f, 0.0f, NPhysicsSystem::max_mass);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Text("Drag");
		ImGui::SliderFloat("Drag", &drag, 0.0f, 1.0f, "%.3f");
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::Text("Angular Velocity");
		ImGui::DragFloat("Angular Velocity", &angularVelocity);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Text("Torque");
		ImGui::DragFloat("Torque", &torque);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;


		ImGui::Text("Velocity");
		ImGui::PushID("Velocity");
		ImGui::Columns(2, "Velocity", true);
		ImGui::DragFloat("X", &Velocity.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::NextColumn();
		ImGui::DragFloat("Y", &Velocity.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Columns(1);
		ImGui::PopID();

		ImGui::Separator();

		ImGui::Text("Gravity");
		ImGui::PushID("Gravity");
		ImGui::Columns(2, "Gravity", true);
		ImGui::DragFloat("X", &gravity.x);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::NextColumn();
		ImGui::DragFloat("Y", &gravity.y);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;
		ImGui::Columns(1);
		ImGui::PopID();

		ImGui::Separator();

		
		ImGui::PushID("Rotation Locked");
		ImGui::Checkbox("Rotation Locked", &rotationlocked);
		ImGui::PopID();

		ImGui::Separator();

		
		


		ImGui::TreePop();

	}
	return changed;
}

void RigidBody::Integrate(float dt)
{

	//Execute if the inverse mass is not 0
	if (InvMass != 0)
	{
		//Add to the velocity the acceleration times the inverse mass and the timestep
		Velocity += Acceleration * InvMass * dt;

	
		Velocity = Velocity * drag;
		//Update the previous position
		PrevPosition = Transform->GetWorldPosition();

		//Add to the position the velocity times timestep
		//Position += Velocity * dt;
		glm::vec3 newvec(Velocity.x, Velocity.y, 0);
		glm::vec3 newpos(PrevPosition.x, PrevPosition.y, 0);

		Transform->SetWorldPosition(newpos + newvec * dt);

		angularVelocity += torque * 1 * dt;
		angularVelocity = angularVelocity * drag;
		
		if (rotationlocked == false)
		{
			float newvec2 = angularVelocity;
			float newor = PrevOrientation;
			//float test = newor + newvec2 * dt * dt;
			Transform->SetWorldOrientation(newor + newvec2 * dt);
			PrevOrientation = Transform->GetWorldOrientation();
		}
		//Set the acceleration to 0
		Acceleration = glm::vec2(0, 0);
		torque = 0;

	}

}

void RigidBody::AddForce(glm::vec2 force)
{
	Acceleration += force;
}

RigidBody* RigidBody::AddCollidingObject(RigidBody* obj)
{
	if (obj == nullptr)
		return nullptr;

	auto duplicated = std::find_if(mCollidedWith.begin(), mCollidedWith.end(), [&](auto& it)
	{
		return obj == it;
	});

	if (duplicated == mCollidedWith.end())
	{
		mCollidedWith.push_back(obj);
		return obj;
	}
	else
		return nullptr;
}

void RigidBody::RemoveCollidingObject(RigidBody* obj)
{
	auto to_delete = std::find(mCollidedWith.begin(), mCollidedWith.end(), obj);

	if (to_delete != mCollidedWith.end())
	{
		mCollidedWith.erase(to_delete);
	}

}

bool RigidBody::HasCollidedBefore(RigidBody* obj)
{
	if (obj == nullptr)
		return false;

	auto duplicated = std::find_if(mCollidedWith.begin(), mCollidedWith.end(), [&](auto& it)
		{
			return obj == it;
		});

	if (duplicated == mCollidedWith.end())
		return false;
	else
		return true;
}




void RigidBody::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	using nlohmann::json;
	j["InvMass"] = InvMass;
	j["drag"] = drag;
	json& velocityJson = j["Velocity"];
	velocityJson.push_back(Velocity.x);
	velocityJson.push_back(Velocity.y);

	json& gravityjs = j["gravity"];
	gravityjs.push_back(gravity.x);
	gravityjs.push_back(gravity.y);

	json& Accelerationjs = j["Acceleration"];
	Accelerationjs.push_back(Acceleration.x);
	Accelerationjs.push_back(Acceleration.y);
	
	
	json& PrevPositionjs = j["PrevPosition"];
	PrevPositionjs.push_back(PrevPosition.x);
	PrevPositionjs.push_back(PrevPosition.y);
	
	json& SumForcesjs = j["SumForces"];
	SumForcesjs.push_back(SumForces.x);
	SumForcesjs.push_back(SumForces.y);

	j["RotationLock"] = rotationlocked;
}

void RigidBody::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	using nlohmann::json;
	if (j.find("InvMass") != j.end())
		InvMass= j["InvMass"];

	if (j.find("drag") != j.end())
		drag= j["drag"];
	
	if (j.find("Velocity") != j.end() && j["Velocity"].size() >= 2)
	{
		Velocity.x = j["Velocity"][0];
		Velocity.y = j["Velocity"][1];
	}

	if (j.find("gravity") != j.end() && j["gravity"].size() >= 2)
	{
		gravity.x = j["gravity"][0];
		gravity.y = j["gravity"][1];
	}

	if (j.find("Acceleration") != j.end() && j["Acceleration"].size() >= 2)
	{
		Acceleration.x = j["Acceleration"][0];
		Acceleration.y = j["Acceleration"][1];
	}

	if (j.find("PrevPosition") != j.end() && j["PrevPosition"].size() >= 2)
	{
		PrevPosition.x = j["PrevPosition"][0];
		PrevPosition.y = j["PrevPosition"][1];
	}

	if (j.find("SumForces") != j.end() && j["SumForces"].size() >= 2)
	{
		SumForces.x = j["SumForces"][0];
		SumForces.y = j["SumForces"][1];
	}


	if (j.find("RotationLock") != j.end())
	{
		rotationlocked = j["RotationLock"];
	}
}
void RigidBody::SetDirection(float _Direction)
{
	mDirection = _Direction;
}
float RigidBody::GetDirection()
{
	return mDirection;
}

RigidBody* RigidBody::Clone()
{
	RigidBody* temp = FactorySys->Create<RigidBody>();
	(*temp) = (*this);
	return temp;
}


