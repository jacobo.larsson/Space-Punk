/**********************************************************************************/
/*
\file   RigidBody.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions make an object have a rigid body with velocity, gravity
and inverse mass. A dorce can also be added to the object.
*/
/*********************************************************************************/
#pragma once 
#include <glm/glm.hpp>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"

#include "../EventSystem/Listener/Listener.h"
#include "../EventSystem/EventHandler.h"
#include "../Collisions/CollisionTable.h"

class CollisionGroupUpdated;
class TransformComponent;

class RigidBody : public IComp
{
	friend class Editor;
	SM_RTTI_DECL(RigidBody, IComp);

public:
	RigidBody();
	RigidBody(RigidBody& rhs);
	~RigidBody();
	RigidBody& operator=(const RigidBody& rhs);
	RigidBody* Clone()override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void Integrate(float dt);
	void AddForce(glm::vec2 force);


	RigidBody* AddCollidingObject(RigidBody* obj);
	void RemoveCollidingObject(RigidBody* obj);
	bool HasCollidedBefore(RigidBody* obj);

	void ToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j);

	void SetDirection(float _Direction);
	float GetDirection();


	TransformComponent* Transform = nullptr;
	TransformComponent* ColliderTrans = nullptr;
	glm::vec2 Velocity = glm::vec2(0,0);
	glm::vec2 gravity;
	float InvMass;
	float drag;
	float angularVelocity = 0;
	float torque = 0;

	bool rotationlocked = false;
private:
	
	float mDirection;
	glm::vec2 Acceleration = glm::vec2(0, 0);
	glm::vec2 PrevPosition = glm::vec2(0, 0);
	float PrevOrientation = 0;
	glm::vec2 SumForces = glm::vec2(0, 0);

	std::vector<RigidBody*> mCollidedWith;
	

};