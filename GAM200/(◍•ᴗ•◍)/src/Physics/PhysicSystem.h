/**********************************************************************************/
/*
\file   PhysicSystem.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that resolve the collision between two bodies and the
addition of rigid bodies
*/
/*********************************************************************************/
#pragma once
#include <list>

#include <glm/glm.hpp>

#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"

class RigidBody;
struct Contact;

namespace NPhysicsSystem
{
	const float DFLT_RESTITUTION = 0;

	const float max_mass = 1000.0f;
}

class PhysicSystem : public IBase
{
	SM_RTTI_DECL(PhysicSystem, IBase);
	Make_Singleton(PhysicSystem)
public:
	bool Initialize(void);
	void Update(void);
	void LateUpdate(void);
	void Shutdown(void);

	void GetActiveBodies();
	void ClearBodies(void);

	// Exposed solver
	void ResolveContactPenetration(RigidBody* obj1, RigidBody* obj2, Contact* contact);
	void ResolveContactVelocity(RigidBody* obj1, RigidBody* obj2, Contact* contact);
	
	std::map<unsigned, std::vector<RigidBody*>*> mAllBodies;
	std::vector<RigidBody*> mActiveBodies;
	bool slowmo=false;
};

#define Physys PhysicSystem::Get()