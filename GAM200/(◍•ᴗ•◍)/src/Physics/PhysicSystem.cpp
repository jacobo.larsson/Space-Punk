/**********************************************************************************/
/*
\file   PhysicSystem.cpp
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions in PhysicSystem.h
*/
/*********************************************************************************/

#include <glm/gtc/matrix_transform.hpp>
#include <glm\gtx\matrix_transform_2d.hpp>

#include "../Space/Space.h"
#include "../Scene/Scene.h"

#include "../GameObject/GameObject.h"
#include "../Collisions/ContactCollisions.h"

#include "../Transform2D/TransformComponent.h"
#include "RigidBody.h"

#include "PhysicSystem.h"

PhysicSystem* PhysicSystem::instance = nullptr;

bool PhysicSystem::Initialize(void)
{
	ClearBodies();
	for (auto& _Space : SceneSys->GetAllSpaces())
	{
		mAllBodies[_Space->GetUID()] = reinterpret_cast<std::vector<RigidBody*>*>(&_Space->GetComponents<RigidBody>());
	}
	return true;
}

void PhysicSystem::Update(void)
{
	if (SettingsSys->SpacePartitioning)
		GetActiveBodies();
	else
	{
		std::for_each(mAllBodies.begin(), mAllBodies.end(), [&](auto& _Bodies)
		{
			std::for_each(_Bodies.second->begin(), _Bodies.second->end(), [&](auto& _Body)
			{
				if (_Body->GetOwner()->Enabled() && _Body->Enabled())
					_Body->Update();
			});
		});
	}
}

void PhysicSystem::LateUpdate(void)
{
	//std::for_each(AllRigidBodies.begin(), AllRigidBodies.end(), [&](auto& _Body)
	//{
	//	_Body->LateUpdate();
	//});
	//std::for_each(mRigidBodies.begin(), mRigidBodies.end(), [&](auto& _Bodies)
	//{
	//	std::for_each(_Bodies.second->begin(), _Bodies.second->end(), [&](auto& _Body)
	//	{
	//		if (_Body->GetOwner()->Enabled() && _Body->Enabled())
	//			_Body->LateUpdate();
	//	});
	//}
	//);
}

void PhysicSystem::Shutdown(void)
{
	 ClearBodies(); 
}

void PhysicSystem::GetActiveBodies()
{
	mActiveBodies.clear();
	for (auto& _Space : SceneSys->GetAllSpaces())
	{
		_Space->GetActiveComponents(mActiveBodies, "RigidBody");
	}
	std::for_each(mActiveBodies.begin(), mActiveBodies.end(), [&](auto& _Body)
	{
		_Body->Update();
	});
}

void PhysicSystem::ClearBodies(void)
{
	mAllBodies.clear();
	mActiveBodies.clear();
}

void PhysicSystem::ResolveContactPenetration(RigidBody* obj1, RigidBody* obj2, Contact* contact)
{
	//Compute the total inverse mass
	float TotalInvMass = obj1->InvMass + obj2->InvMass;

	//Compute the mass influence of the body 1
	float massInfluence1 = obj1->InvMass / TotalInvMass;

	//Compute the mass influence of the body 2
	float massInfluence2 = obj2->InvMass / TotalInvMass;

	//Update the body 1 position taking into account the normal, the penetration and the mass
	//obj1->mPosition = obj1->mPosition - contact->mNormal * contact->mPenetration * massInfluence1;
	//glm::vec3 newaxis = contact->mNormal * contact->mPenetration * massInfluence1;
	//glm::mat3 rotmat = glm::rotate(glm::mat3(1.f),(obj1->Transform->world_transform.mOrientation));
	//newaxis.z = 0;
	//newaxis = rotmat * newaxis;
	glm::vec3 Positiontoset(obj1->Transform->GetWorldPosition() - (contact->mNormal * contact->mPenetration * massInfluence1));
	obj1->Transform->SetWorldPosition(Positiontoset);

	//glm::vec3 newaxis2 = contact->mNormal * contact->mPenetration * massInfluence2;
	//glm::mat3 rotmat2 = glm::rotate(glm::mat3(),(obj2->Transform->world_transform.mOrientation));
	//newaxis2.z = 0;
	//newaxis2 = rotmat2 * newaxis2;

	glm::vec3 Positiontoset2(obj2->Transform->GetWorldPosition()+ (contact->mNormal * contact->mPenetration * massInfluence2));
	//Update the body 2 position taking into account the normal, the penetration and the mass 
	obj2->Transform->SetWorldPosition(Positiontoset2);
}

void PhysicSystem::ResolveContactVelocity(RigidBody* obj1, RigidBody* obj2, Contact* contact)
{
	//Comput the relative velocity
	glm::vec2 relativevel = obj2->Velocity - obj1->Velocity;

	//Compute the separating velocity using the relative velocity and the normal
	float separatingvel = glm::dot(relativevel,glm::vec2(contact->mNormal.x, contact->mNormal.y));\

	//Execute if separating velocity is bigger than 0
	if (separatingvel > 0)
	{
		//Exit the function
		return;
	}

	//Else execute this
	else
	{
		//Compute the total inverse mass
		float TotalInvMass = obj1->InvMass + obj2->InvMass;

		//Compute the mass influence of the body 1
		float massInfluence1 = obj1->InvMass / TotalInvMass;

		//Compute the mass influence of the body 2
		float massInfluence2 = obj2->InvMass / TotalInvMass;

		//Store in separatingvelprime the result of multiplying the negation of separatingvel and the restitution
		float separatingvelprime = -separatingvel * NPhysicsSystem::DFLT_RESTITUTION;

		//Compute the separating velocity difference
		float separatingdiff = separatingvelprime - separatingvel;

		//Update the body 1 position taking into account the normal, the separating velocity difference and the mass
		obj1->Velocity = obj1->Velocity - glm::vec2(contact->mNormal.x, contact->mNormal.y) * separatingdiff * (massInfluence1 * massInfluence1);


		//Update the body 2 position taking into account the normal, the separating velocity difference and the mass 
		obj2->Velocity = obj2->Velocity + glm::vec2(contact->mNormal.x, contact->mNormal.y) * separatingdiff * (massInfluence2 * massInfluence2);



		glm::vec3 relpoint1= contact->mPi - obj1->Transform->GetWorldPosition();
		glm::vec3 relpoint2 = contact->mPi - obj2->Transform->GetWorldPosition();
		
		float torqueperunit1 = relpoint1.x * contact->mNormal.y - contact->mNormal.x * relpoint1.y;
		float torqueperunit2 = relpoint2.x * contact->mNormal.y - contact->mNormal.x * relpoint2.y;

		
		float invinertia1 =1.f/ 0.3f;
		float invinertia2 =1.f/ 0.3f;

		if (obj1->Transform->GetWorldPosition().x < obj2->Transform->GetWorldPosition().x)
		{
			
			if (obj1->Transform->GetWorldPosition().x < (obj2->Transform->GetWorldPosition().x - obj2->Transform->GetWorldScale().x / 2))
				obj1->torque = -1 * torqueperunit1 * invinertia1;

			if (obj2->Transform->GetWorldPosition().x < (obj1->Transform->GetWorldPosition().x + obj1->Transform->GetWorldScale().x / 2))
				obj2->torque = -1 * torqueperunit2 * invinertia2;
		}
		else
		{
			if (obj1->Transform->GetWorldPosition().x > (obj2->Transform->GetWorldPosition().x + obj2->Transform->GetWorldScale().x / 2))
				obj1->torque = -1 * torqueperunit1 * invinertia1;

			if (obj2->Transform->GetWorldPosition().x > (obj1->Transform->GetWorldPosition().x - obj1->Transform->GetWorldScale().x / 2))
				obj2->torque = -1 * torqueperunit2 * invinertia2;
		}
	}
}
