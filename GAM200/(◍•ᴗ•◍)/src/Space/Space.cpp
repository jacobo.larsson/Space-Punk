/**********************************************************************************/
/*
\file   Space.cpp
\author Juan Pacheco & David Miranda
\par    email: pacheco.j@digipen.edu & m.david@digipen.edu
\par    DigiPen login: pacheco.j & m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions of Space.h
*/
/*********************************************************************************/
#include <glm/gtc/constants.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include "../AssertSystem/AssertSystem.h"
#include "../Factory/Factory.h"

#include "../Input/Input.h"

#include "../Transform2D/TransformComponent.h"
#include "../EventSystem/EventDispatcher.h" 
#include "../EventSystem/Events/ArchetypesEvents/ArchetypesEvents.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Graphics/Shaders/Shader.h"
#include "../Graphics/Camera/Camera.h"
#include "../Collisions/CollisionTable.h"
#include "../Collisions/ICollider.h"

#include "Space.h"

static int MAX_LINE_VTX = 48000;
static int CirclePrecision = 20;

Space::Space()
{
	EventSys->unsubscribe(this, ArchetypeUpdated());
	mMesh = RndMngr->GetMesh("line.mesh"); 
	mMesh->GetVertices().resize(MAX_LINE_VTX);
	mMesh->GetIndices().resize(MAX_LINE_VTX);
	mMesh->UploadToGPU();
	CreateGrid(1500, 840);
}

 void Space::Update()
 {
	 FreeDestroyedObjects();
	 FreeDestroyedComponents();
	 if(SettingsSys->inEditor)
		UpdateTransformHierarchy();
	 
	 // Space Partitioning
	 if (SettingsSys->SpacePartitioning)
	 {
		 //if (SettingsSys->inEditor)
		 //{
		//	 ResetPartitions();
		//	 InsertAllObjects();
		 //}
		 if (SettingsSys->DrawPartitions)
			 DrawPartitions();
		 UpdateActiveObjects();
	 }
}
 void Space::LateUpdate()
 {
 }
 void Space::Shutdown()
 {
	 for (auto& it : AllObjects)
	 {
		 it->Shutdown();
		 delete it;
	 }
	 AllObjects.clear();
	 AllParents.clear();
	 ActiveObjects.clear();
	 AllComponents.clear();
	 ObjectsWithTags.clear();
	 ClearPartitions();
 }

// Object creation/deletion
GameObject* Space::NewGameObject(const char* name)
{
	GameObject* newobj = FactorySys->Create<GameObject>();
	newobj->mParentSpace = this;
	newobj->SetName(name);
	newobj->spaceId = this->mUID;
	// Forgot to add it to the object map
	// Still to be discussed if we are going to have the objects stored this way
	
	AllParents.push_back(newobj);
	AllObjects.push_back(newobj);
	
	return newobj;

}
GameObject* Space::AddGameObject(GameObject* obj)
{
	auto duplicated = std::find(AllObjects.begin(), AllObjects.end(), obj);
	if (duplicated == AllObjects.end())
	{
		obj->mParentSpace = this;
		obj->spaceId = this->mUID;
		
		for (auto comp : obj->mComponents)
			RegisterComponent(comp);
		for (auto tag : obj->mTags)
			AddObjectToTag(tag, obj);

		mPartitions->Insert(obj);

		AllParents.push_back(obj);
		AllObjects.push_back(obj);
		return obj;
	}
	return nullptr;
}
void	Space::DestroyObject(GameObject* obj)
{
	if (obj != nullptr)
	{
		if (obj->mChildren.size())
		{
			for (auto child : obj->mChildren)
			{
				DestroyObject(child);
			}
		}
		obj->mbDeleted = true;
		DestroyedObjects.push_back(obj);
		obj->RemoveFromPartitions();
	}
}

// object searching
std::vector<GameObject*>& Space::GetAllObjects()
{
	return AllObjects;
}
std::vector<GameObject*>& Space::GetAllParents()
{
	return AllParents;
}

std::map<unsigned, GameObject*>& Space::GetActiveObjects()
{
	return ActiveObjects;
}

bool Space::SpacePartitioningEnabled()
{
	return mSpacePartitioningEnabled;
}


void Space::InsertObject(GameObject* _Object)
{
	mPartitions->Insert(_Object);
	//Point _TopLeft{ _Object->mPos.x - _Object->mMaxScale.x, _Object->mPos.y + _Object->mMaxScale.y };
	//Point _TopRight{ _Object->mPos.x + _Object->mMaxScale.x, _Object->mPos.y + _Object->mMaxScale.y };
	//Point _BotLeft{ _Object->mPos.x - _Object->mMaxScale.x, _Object->mPos.y - _Object->mMaxScale.y };
	//Point _BotRight{ _Object->mPos.x + _Object->mMaxScale.x, _Object->mPos.y - _Object->mMaxScale.y };
	//mPartitions->Insert(_TopLeft, _Object);
	//mPartitions->Insert(_TopRight, _Object);
	//mPartitions->Insert(_BotLeft, _Object);
	//mPartitions->Insert(_BotRight, _Object);
}

GameObject* Space::FindObjectByName(const char* name)
{
	// see if we have a registered list of objects for the specified name
	auto it = std::find_if(AllObjects.begin(), AllObjects.end(), [&](auto& object)
	{
		return object->GetName() == std::string(name);
	});

	if (it != AllObjects.end() )
		return *it;
	else
		return nullptr;
}
unsigned int Space::FindAllObjectsByName(const char* name, std::vector<GameObject*>& outObjects)
{
	auto previousSize = outObjects.size();
	// see if we have a registered list of objects for the specified name
	auto it = std::for_each(AllObjects.begin(), AllObjects.end(), [&](auto& object)
	{
		if (object->GetName() == std::string(name))
			outObjects.push_back(object);
	});

	return (unsigned)(outObjects.size() - previousSize);
}

unsigned int Space::FindAllObjectsWithComponent(const char* compTypeName, std::vector<GameObject*>& outObjects)
{
	auto ComponentsVector = GetComponents(compTypeName);
	// record the size of the output list
	unsigned int previousSize = (unsigned)outObjects.size();

	std::for_each(ComponentsVector.begin(), ComponentsVector.end(), [&](auto& comp)
	{
		outObjects.push_back(comp->GetOwner());
	});

	// return the number of objects that were added
	return (unsigned)outObjects.size() - previousSize;
}

GameObject* Space::FindObjectByTag(const char* name)
{
	// see if we have a registered list of objects for the specified name
	auto it = std::find_if(AllObjects.begin(), AllObjects.end(), [&](auto& object)
	{
		return object->CompareTag(name);
	});

	if (it != AllObjects.end())
		return *it;
	else
		return nullptr;
}
std::vector<GameObject*>* Space::FindAllObjectsByTag(const char* name)
{
	auto objectVector = ObjectsWithTags.find(name);

	if (objectVector != ObjectsWithTags.end())
		return &objectVector->second;
	else
		return nullptr;
}

std::vector<GameObject*> Space::FindAllObjectsByArchetype(const char* name)
{
	std::vector<GameObject*> temp;
	if (name)
	{
		for (auto& obj : AllObjects)
		{
			if (obj->GetArchetypeName() == name)
				temp.push_back(obj);
		}
	}
	return temp;
}

std::vector<GameObject*> Space::FindAllObjectsByCollisionGroup(const char* name)
{
	std::vector<GameObject*> temp;
	if (name)
	{
		auto coll_group_map = CollTable->CollisionTypesVector;

		for (auto& obj : AllObjects)
		{
			Collider* coll = obj->GetComp<Collider>();
			if (coll)
			{
				if (coll_group_map[coll->type_of_object] == name)
					temp.push_back(obj);
			}
		}
	}
	return temp;
}

void Space::FindAllObjectsByUID(unsigned _ID, std::vector<GameObject*>& _OutVector)
{
	for (auto& _Object : AllObjects)
	{
		if (_Object->mUID == _ID)
			_OutVector.push_back(_Object);
	}
}

GameObject* Space::FindObjectById(unsigned int id)
{
	for (auto it = AllObjects.begin(); it != AllObjects.end(); it++)
	{
		if ((*it)->GetUID() == id)
			return *it;
	}
	return nullptr;
}

int Space::FindObjectMaxId()
{
	int max_id = 0;
	for (auto it = AllObjects.begin(); it != AllObjects.end(); it++)
	{
		if ((*it)->GetUID() > (unsigned int)max_id)
			max_id = (*it)->GetUID();

		int temporal_id = (*it)->FindComponentMaxId();
		if (temporal_id > max_id)
			max_id = temporal_id;

	}
	return max_id;
}

void Space::InitializeComponents()
{
	std::for_each(AllComponents.begin(), AllComponents.end(), [&](auto& _components)
	{
		std::for_each(_components.second.begin(), _components.second.end(), [&](auto& comp)
		{
			comp->Initialize();
		});
	});
}
IComp* Space::RegisterComponent(IComp* comp)
{
	AllComponents[comp->GetName()].push_back(comp);
	return comp;
}
void Space::DeleteComponent(IComp * comp)
{
	auto& vector = AllComponents[comp->GetName()];
	if (comp != nullptr)
	{
		auto to_delete = std::find(vector.begin(), vector.end(), comp);
		if (to_delete != vector.end())
		{
			DestroyedComponents.push_back(*to_delete);
			vector.erase(to_delete);
		}
	}
}
std::vector<IComp*>& Space::GetComponents(std::string type)
{
	return AllComponents[type];
}

void Space::ToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);

	j["Order"] = Order;
	j["mSpacePartitioningEnabled"] = mSpacePartitioningEnabled;
	if (AllObjects.size())
	{
		json& objectsJson = j["objects"];
		for (auto& obj : AllParents)
		{
			if (obj->to_serialize == true)
			{
				json objJson;
				objJson["_type"] = obj->GetType().GetName();
				obj->ToJson(objJson);
				objectsJson.push_back(objJson);
			}
		}
	}
}
void Space::CheckPointToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);

	j["Order"] = Order;
	if (AllObjects.size())
	{
		json& objectsJson = j["objects"];
		for (auto& obj : AllParents)
		{
			if (obj->to_serialize == true)
			{
				json objJson;
				objJson["_type"] = obj->GetType().GetName();
				obj->ToJson(objJson);
				objectsJson.push_back(objJson);
			}
		}
	}
}
void Space::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("Order") != j.end())
		Order = j["Order"];
	if (j.find("mSpacePartitioningEnabled") != j.end())
		mSpacePartitioningEnabled = j["mSpacePartitioningEnabled"];
	auto objectsJson = j.find("objects");
	if (objectsJson != j.end())
	{
		for (const auto& objectJson : *objectsJson)
		{
			if (objectJson.find("name") != objectJson.end())
			{
				std::string objectName = objectJson["name"];
				if (GameObject* newObj = NewGameObject(objectName.c_str()))
				{
					newObj->FromJson(objectJson);
				}
			}
		}
	}
	LinkPointers();
}

void Space::AddObjectToTag(const std::string& tag, GameObject* thisobj)
{
	auto obj = std::find(ObjectsWithTags[tag].begin(), ObjectsWithTags[tag].end(), thisobj);
	if (obj == ObjectsWithTags[tag].end())
	{
		ObjectsWithTags[tag].push_back(thisobj);
	}


}
void Space::RemoveObjectFromTag(const std::string& tag, GameObject* thisobj)
{
	auto obj = std::find(ObjectsWithTags[tag].begin(), ObjectsWithTags[tag].end(), thisobj);
	if (obj != ObjectsWithTags[tag].end())
	{
		ObjectsWithTags[tag].erase(obj);
	}
}

void Space::SetCurrentLineVtx(int num)
{
	mCurrentLineVtx = num;
}

void Space::RenderLines(glm::mat4 mat)
{
	auto* myShader = RndMngr->GetShader("line.shader");
	//glm::mat4 mtxViewProj = editor_camera->GetViewProjMatrix();
	
	myShader->Use();
	myShader->SetUniform("Model2World", glm::mat4(1));
	myShader->SetUniform("CamToProj", mat);

	mMesh->ReUploadToGPU();
	mMesh->Draw(mCurrentLineVtx);
	glBindVertexArray(0);
}
void Space::DrawLine(glm::vec3 v0, glm::vec3 v1, Color col)
{
	if (mCurrentLineVtx + 2 >= MAX_LINE_VTX)
		return; // todo: print error
	//v0.z = v1.z = 19.0f;
	mMesh->GetVertices()[mCurrentLineVtx].position = v0;
	mMesh->GetVertices()[mCurrentLineVtx].color = col;
	mMesh->GetIndices()[mCurrentLineVtx] = mCurrentLineVtx++;

	mMesh->GetVertices()[mCurrentLineVtx].position = v1;
	mMesh->GetVertices()[mCurrentLineVtx].color = col;
	mMesh->GetIndices()[mCurrentLineVtx] = mCurrentLineVtx++;
}
void Space::DrawLine(glm::vec2 v0, glm::vec2 v1, Color col)
{
	if (mCurrentLineVtx + 2 >= MAX_LINE_VTX)
		return; // todo: print error

	mMesh->GetVertices()[mCurrentLineVtx].position = glm::vec3(v0, 19.0f);
	mMesh->GetVertices()[mCurrentLineVtx].color = col;
	mMesh->GetIndices()[mCurrentLineVtx] = mCurrentLineVtx++;

	mMesh->GetVertices()[mCurrentLineVtx].position = glm::vec3(v1, 19.0f);
	mMesh->GetVertices()[mCurrentLineVtx].color = col;
	mMesh->GetIndices()[mCurrentLineVtx] = mCurrentLineVtx++;
}
void Space::DrawHorizontalLine(glm::vec3 v0, int width, Color col)
{
	glm::vec3 v1{ v0.x + width / 2, v0.y, v0.z };
	glm::vec3 v2{ v0.x - width / 2, v0.y, v0.z };
	DrawLine(v1, v2, col);
}
void Space::DrawHorizontalLine(glm::vec2 v0, int width, Color col)
{
	DrawHorizontalLine(glm::vec3{ v0.x, v0.y, 0.0f }, width, col);
}
void Space::DrawVerticalLine(glm::vec3 v0, int height, Color col)
{
	glm::vec3 v1{ v0.x, v0.y - height/2, v0.z };
	glm::vec3 v2{ v0.x, v0.y + height/2, v0.z };
	DrawLine(v1, v2, col);
}
void Space::DrawVerticalLine(glm::vec2 v0, int height, Color col)
{
	DrawVerticalLine(glm::vec3{ v0.x, v0.y, 0.0f }, height, col);
}
void Space::Drawrectangle(glm::vec3 pos, int width, int height, Color color, float rot)
{
	if (mCurrentLineVtx + 2 >= MAX_LINE_VTX)
		return; // todo: print error
	if (rot != 0.0f)
	{
		glm::mat3 rotMtx = glm::rotate(glm::mat3(1.0), rot);
		glm::vec2 vertices[4];
		vertices[0] = rotMtx * glm::vec3( width / 2,  height / 2.0f, 0.0f) + pos;
		vertices[1] = rotMtx * glm::vec3(-width / 2,  height / 2.0f, 0.0f) + pos;
		vertices[2] = rotMtx * glm::vec3(-width / 2, -height / 2.0f, 0.0f) + pos;
		vertices[3] = rotMtx * glm::vec3( width / 2, -height / 2.0f, 0.0f) + pos;

		//Top line
		DrawLine(glm::vec3(vertices[0].x, vertices[0].y, pos.z),
				 glm::vec3(vertices[1].x, vertices[1].y, pos.z), color);
		// Bot line
		DrawLine(glm::vec3(vertices[1].x, vertices[1].y, pos.z),
				 glm::vec3(vertices[2].x, vertices[2].y, pos.z), color);
		// Left line
		DrawLine(glm::vec3(vertices[2].x, vertices[2].y, pos.z),
				 glm::vec3(vertices[3].x, vertices[3].y, pos.z), color);
		// Right line
		DrawLine(glm::vec3(vertices[3].x, vertices[3].y, pos.z),
				 glm::vec3(vertices[0].x, vertices[0].y, pos.z), color);
	}
	else
	{
		//Top line
		DrawLine(glm::vec3(pos.x - width / 2, pos.y + height / 2, pos.z),
			glm::vec3(pos.x + width / 2, pos.y + height / 2, pos.z), color);
		// Bot line
		DrawLine(glm::vec3(pos.x - width / 2, pos.y - height / 2, pos.z),
			glm::vec3(pos.x + width / 2, pos.y - height / 2, pos.z), color);
		// Left line
		DrawLine(glm::vec3(pos.x - width / 2, pos.y + height / 2, pos.z),
			glm::vec3(pos.x - width / 2, pos.y - height / 2, pos.z), color);
		// Right line
		DrawLine(glm::vec3(pos.x + width / 2, pos.y + height / 2, pos.z),
			glm::vec3(pos.x + width / 2, pos.y - height / 2, pos.z), color);
	}
}
void Space::Drawrectangle(glm::vec2 pos, int width, int height, Color color, float rot)
{
	Drawrectangle(glm::vec3{ pos.x, pos.y, 0.0f }, width, height, color, rot);
}
void Space::DrawCircle(glm::vec3 pos, float radius, Color color, float precision)
{
	float twoPi = glm::two_pi<float>();
	float angle_step = twoPi / CirclePrecision;
	glm::vec3 p1 = { radius, 0.0f, 19.0f }, p2 = { 0.0f, 0.0f, 19.0f };
	for (float theta = angle_step; theta <= twoPi + angle_step; theta += angle_step)
	{
		p2.x = glm::cos(theta) * radius;
		p2.y = glm::sin(theta) * radius;
		glm::vec3 temp1 = p1 + pos;
		glm::vec3 temp2 = p2 + pos;
		DrawLine(p1 + pos, p2 + pos, color);
		p1 = p2;
	}
}

void Space::DrawCircle(glm::vec2 pos, float radius, Color color, float precision)
{
	DrawCircle(glm::vec3{ pos.x, pos.y, 0.0f }, radius, color, precision);
}

void Space::AddPointerToLink(GameObject** pObj, unsigned uID, std::string name)
{
	ASSERT(pObj != nullptr);
	mPointerToLink.push_back(std::make_pair(pObj, std::make_pair(uID, name)));
}

void Space::LinkPointers()
{
	for (auto pointer : mPointerToLink)
	{
		*pointer.first = FindObjectByUIDAndName(pointer.second.first, pointer.second.second);
	}
	mPointerToLink.clear();
}

int& Space::GetOrder()
{
	return Order;
}

int Space::GetconstOrder() const
{
	return Order;
}


void Space::FreeDestroyedObjects()
{

	std::list<GameObject*>::iterator it;
	for (it = DestroyedObjects.begin(); it != DestroyedObjects.end(); it++)
	{
		// disable it along with all of its components
		(*it)->SetEnabled(false);

		// shut it down
		(*it)->Shutdown();

		//Remove from both lists
		AllObjects.erase(std::find(AllObjects.begin(), AllObjects.end(), *it));

		auto temp = std::find(AllParents.begin(), AllParents.end(), *it);
		if (temp != AllParents.end())
			AllParents.erase(temp);
		

		if ((*it)->mParent)
		{
			auto child = std::find((*it)->mParent->mChildren.begin(), (*it)->mParent->mChildren.end(), *it);

			if (child != (*it)->mParent->mChildren.end())
				(*it)->mParent->mChildren.erase(child);

		}
		delete (*it);
	}

	DestroyedObjects.clear();

}
void Space::FreeDestroyedComponents()
{
	std::for_each(DestroyedComponents.begin(), DestroyedComponents.end(), [&](auto& it)
	{
		delete (it);
	});

	DestroyedComponents.clear();
}
void Space::UpdateTransformHierarchy()
{
	// use a local stack to simulate recursion iteratively.
	std::list<GameObject*> stack;

	// add all children objects of the space
	// these objects are considered root objects 
	// so their world transform is the same as their local transform

	for (auto c : AllParents)
		stack.push_back(c);

	// process the entire hierachy 
	while (stack.size()) {
		GameObject* obj = stack.back();
		stack.pop_back();

		// if the object has a transform, update it
		if (auto tr = obj->GetComp<TransformComponent>())
		{
			TransformComponent* toupdate = dynamic_cast<TransformComponent*>(tr);
			if(toupdate->GetOwner()->Enabled() && toupdate->Enabled())
				toupdate->UpdateWorld();	// update the world transform
		}
		// add children to the stack for processing
		for (auto c : obj->mChildren)
			stack.push_back(c);
	}

}

void Space::CreateGrid(int _Width, int _Height)
{
	if (mPartitions)
		ClearPartitions();
	mPartitions = new Quad(Point(0, 0), Point(_Width, _Height));
	mPartitions->mRoot = mPartitions;
}

void Space::UpdateActiveObjects()
{
	UpdateTransformComponents();
	ActiveObjects.clear();
	for (auto& _Partition : mActivePartitions)
		_Partition->SetEnabled(false);
	mActivePartitions.clear();

	std::for_each(AllComponents["CameraComponent"].begin(), AllComponents["CameraComponent"].end(),[&](auto & _Camera)
	{
		CameraComponent* _Cam = reinterpret_cast<CameraComponent*>(_Camera);
		GameObject* _Owner = _Cam->GetOwner();
		mPartitions->GetActiveObjects(Point{ _Owner->mPos.x - _Cam->mCamera.mViewRectangle.x / 2, _Owner->mPos.y - _Cam->mCamera.mViewRectangle.y / 2 },
									  Point{ _Owner->mPos.x + _Cam->mCamera.mViewRectangle.x / 2, _Owner->mPos.y + _Cam->mCamera.mViewRectangle.y / 2 },
										ActiveObjects, mActivePartitions);
		//_Min = Point{ _Cam->GetOwner()->mPos.x - _Cam->mCamera.mViewRectangle.x / 2, _Cam->GetOwner()->mPos.y - _Cam->mCamera.mViewRectangle.y / 2 };
		//_Max = Point{ _Cam->GetOwner()->mPos.x + _Cam->mCamera.mViewRectangle.x / 2, _Cam->GetOwner()->mPos.y + _Cam->mCamera.mViewRectangle.y / 2 };
	});
}

void Space::UpdateTransformComponents()
{
	for (auto& _Comp : AllComponents["TransformComponent"])
		_Comp->Update();
}

void Space::InsertAllObjects()
{ 
	// Update all the transform components to update SpacePartitioning's variables
	for (auto& _Comp : AllComponents["TransformComponent"])
		_Comp->Update();
	// Insert all the objects in the grid
	for (auto& _Object : AllObjects)
	{
			mPartitions->Insert(_Object);
			if (_Object->mDisplacedCollider)
				mPartitions->Insert(_Object, true);
	}
}

void Space::ResetPartitions()
{
	mPartitions->ClearedThisFrame = 0;
	mPartitions->DrawnThisFrame = 0;
	mPartitions->Reset(DrawDirection::DrawLeft);
	mPartitions->Reset(DrawDirection::DrawRight);
}

void Space::DrawPartitions()
{
	mPartitions->DrawQuad(this, DrawDirection::DrawLeft);
	mPartitions->DrawQuad(this, DrawDirection::DrawRight);
}

void Space::ClearPartitions()
{
	if (mPartitions != nullptr)
	{
		mPartitions->Destroy(DrawDirection::DrawLeft);
		mPartitions->Destroy(DrawDirection::DrawRight);
		delete mPartitions;
		mPartitions = nullptr;
	}
}

GameObject* Space::FindObjectByUIDAndName(unsigned uID, std::string name)
{
	auto found = std::find_if(AllObjects.begin(), AllObjects.end(), [&](auto& obj)
	{
		if (obj->GetUID() == uID && obj->GetName() == name)
			return true;
		else
			return false;
	}
	);
	if (found != AllObjects.end())
	{
		return *found;
	}
	else
		return nullptr;
}

bool Space::operator<(const Space& rhs)
{
	return Order < rhs.GetconstOrder();
}