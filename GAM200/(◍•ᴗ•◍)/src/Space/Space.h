/**********************************************************************************/
/*
\file   Space.h
\author Juan Pacheco & David Miranda
\par    email: pacheco.j@digipen.edu & m.david@digipen.edu
\par    DigiPen login: pacheco.j & m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that make an space and all the components that are 
inside it. It can create and delete components. It also has Tags.
*/
/*********************************************************************************/
#pragma once
#include <list>
#include <vector>

#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../Singleton/Singleton.h"

#include "../GameObject/GameObject.h"
#include "../SpacePartitioning/Quadtree.h"
#include "../Graphics/Mesh/Mesh.h"



class Space : public GameObject
{
	friend class Scene;
	friend class Menus;
	friend class Editor;
	SM_RTTI_DECL(Space, GameObject);
public:

	Space();

	// update: 
	// deletes object flagged as destroyed
	// updates the world transform of all objects in the hierarchy
	virtual void Update();
	virtual void LateUpdate();
	void Shutdown()override;
	bool operator<(const Space& rhs);

	// Object creation/deletion
	GameObject* NewGameObject(const char* name = "Game Object");
	GameObject* AddGameObject(GameObject * obj);
	void		DestroyObject(GameObject* obj);

	// object searching
	std::vector<GameObject*>& GetAllObjects();
	std::vector<GameObject*>& GetAllParents();
	//std::vector<GameObject*>& GetActiveObjects();
	std::map<unsigned, GameObject*>& GetActiveObjects();
	bool SpacePartitioningEnabled();
	void InsertObject(GameObject* _Object);

	GameObject* FindObjectByName(const char* name);
	unsigned int FindAllObjectsByName(const char* name, std::vector<GameObject*>& outObjects);

	template<typename T>
	std::vector<GameObject*> GetAllObjectsWithComponent()
	{
		std::vector<GameObject*> OutVector;
		for (auto& object : GetAllObjects())
		{
			if (object->HasComp<T>())
				OutVector.push_back(object);
		}
		return OutVector;
	}
	unsigned int FindAllObjectsWithComponent(const char* compTypeName, std::vector<GameObject*>& outObjects);
	template <typename T>
	unsigned int FindAllObjectsWithComponent(std::vector<GameObject*>& outObjects) {
		return FindAllObjectsWithComponent(T::TYPE().GetName(), outObjects);
	}

	GameObject* FindObjectByTag(const char* name);
	std::vector<GameObject*>* FindAllObjectsByTag(const char* name);
	std::vector<GameObject*> FindAllObjectsByArchetype(const char* name);
	std::vector<GameObject*> FindAllObjectsByCollisionGroup(const char* name);
	void FindAllObjectsByUID(unsigned _ID, std::vector<GameObject*>& _OutVector);

	GameObject* FindObjectById(unsigned int id);
	int FindObjectMaxId();


	void InitializeComponents();
	IComp* RegisterComponent(IComp* comp);
	void DeleteComponent(IComp* comp);
	std::vector<IComp*>& GetComponents(std::string type);
	template<typename T>
	std::vector<IComp*>& GetComponents();
	template<typename T>
	void GetActiveComponents(std::vector<T*>& _OutVector);

	template<typename T>
	void GetActiveComponents(std::vector<T*>& _OutVector, const char * _CompName);
	
	void ToJson(nlohmann::json& j) override;
	void CheckPointToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j) override;
	
	void AddObjectToTag(const std::string& tag, GameObject* thisobj);
	void RemoveObjectFromTag(const std::string& tag,GameObject* thisobj);

	void SetCurrentLineVtx(int num);

	void RenderLines(glm::mat4 mat = glm::mat4(1));
	void DrawLine(glm::vec3 v0, glm::vec3 v1, Color col);
	void DrawLine(glm::vec2 v0, glm::vec2 v1, Color col);
	void DrawHorizontalLine(glm::vec3 v0, int width , Color col);
	void DrawHorizontalLine(glm::vec2 v0, int width , Color col);
	void DrawVerticalLine(glm::vec3 v0, int height , Color col);
	void DrawVerticalLine(glm::vec2 v0, int height, Color col);
	void Drawrectangle(glm::vec3 pos, int width, int height, Color color, float rot = 0.0f);
	void Drawrectangle(glm::vec2 pos, int width, int height, Color color, float rot = 0.0f);
	void DrawCircle(glm::vec3 pos, float radius, Color color, float precision = 10.0f);
	void DrawCircle(glm::vec2 pos, float radius, Color color, float precision = 10.0f);

	void AddPointerToLink(GameObject** pObj, unsigned uID, std::string name);
	void LinkPointers();

	int& GetOrder();
	int GetconstOrder() const;

	
private:

	void FreeDestroyedObjects();
	void FreeDestroyedComponents();
	void UpdateTransformHierarchy();

	// Space Partitioning
	void CreateGrid(int _Width, int _Height);
	void UpdateActiveObjects();
	void UpdateTransformComponents();
	void InsertAllObjects();
	void ResetPartitions();
	void DrawPartitions();
	void ClearPartitions();
	GameObject* FindObjectByUIDAndName(unsigned uID, std::string name);

	std::vector<GameObject*> AllObjects;
	std::vector<GameObject*> AllParents;
	//std::vector<GameObject*> ActiveObjects;
	std::map<unsigned,GameObject*> ActiveObjects;
	std::vector<std::pair<GameObject**, std::pair<unsigned, std::string>>> mPointerToLink;
	std::map<std::string, std::vector<IComp*>> AllComponents;
	std::map<std::string, std::vector<GameObject*>> ObjectsWithTags;

	std::list<GameObject*> DestroyedObjects;
	std::vector<IComp*> DestroyedComponents;

	Mesh* mMesh = 0;
	int mCurrentLineVtx = 0;
	
	int Order;
	unsigned mMaxObjects = 0;
	Point _Min;
	Point _Max;

	Quad* mPartitions = nullptr;
	std::vector<Quad*> mActivePartitions;
	bool mInserted = false;
	bool mSpacePartitioningEnabled = false;
};


template<typename T>
inline std::vector<IComp*>& Space::GetComponents()
{
	auto name = T::TYPE().GetName().c_str();
	return AllComponents[T::TYPE().GetName().c_str()];
}

template<typename T>
inline void Space::GetActiveComponents(std::vector<T*>& _OutVector)
{
	for (auto& _Object : ActiveObjects)
	{
		for (auto& _Comp : _Object.second->mComponents)
		{
			T* _CastedComp = dynamic_cast<T*>(_Comp);
			if(_CastedComp != nullptr)
				_OutVector.push_back(_CastedComp);
		}
	}
}

template<typename T>
inline void Space::GetActiveComponents(std::vector<T*>& _OutVector, const char* _CompName)
{
	for (auto& _Object : ActiveObjects)
	{
		auto _Comp = reinterpret_cast<T*>(_Object.second->GetComp(_CompName));
		if (_Comp != nullptr)
			_OutVector.push_back(_Comp);
	}
}
