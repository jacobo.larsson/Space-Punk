/**********************************************************************************/
/*!
\file   MainEngine.cpp
\author David Miranda 
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the initialize, update and shutdown off all the systems in the engine.
*/
/*********************************************************************************/
#include "../Scene/Scene.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Graphics/Window/Window.h"
#include "../Input/Input.h"
#include "../LogicSystem/LogicSystem.h"
#include "../ResourceManager/ResourceManager.h"
#include "../Physics/PhysicSystem.h"
#include "../Collisions/CollisionSystem.h"
#include "../Collisions/CollisionTable.h"
#include "../Time/Time.h"
#include "../Audio/AudioManager.h"
#include "../Factory/Factory.h"
#include "../Editor/Editor.h"
#include "../Animation/SM_Animation.h"
#include "../Changer/Changer.h"
#include "../Settings/Settings.h"
#include "../Debug/Console.h"
#include "../Settings/Settings.h"
#include "../ThreadPool/ThreadPool.h"
#include "../Profiler/Profiler.h"
#include "../MenuManager/MenuManager.h"

#include "../Graphics/Color/Color.h"

#include"MainEngine.h"


using nlohmann::json;

MainEngine* MainEngine::instance = nullptr;

MainEngine::MainEngine() :running{ true }
{}

bool MainEngine::Initialize()
{
	ProfSys->StartTimer();
	FactorySys->Initialize();
	ProfSys->EndTimer("FactorySys");

	ProfSys->StartTimer();
	if (!ThreadP->Initialize())return false;
	ProfSys->EndTimer("ThreadP");

	ProfSys->StartTimer();
	if (!InputSys->Initialize())return false;
	ProfSys->EndTimer("InputSys");

	ProfSys->StartTimer();
	if (!WindowSys->Initialize())return false;
	ProfSys->EndTimer("WindowSys");

	ProfSys->StartTimer();
	if (!RndMngr->Initialize())return false;
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	if (!AudioSys->Initialize())return false;
	ProfSys->EndTimer("AudioSys");

	ProfSys->StartTimer();
	if (!SettingsSys->Initialize())return false;
	ProfSys->EndTimer("SettingsSys");

	ProfSys->StartTimer();
	if (!RsrcMan->Initialize())return false;
	ProfSys->EndTimer("RsrcMan");

	ProfSys->StartTimer();
	if (!SceneSys->Initialize())return false;
	ProfSys->EndTimer("SceneSys");

	ProfSys->StartTimer();
	if (!GameMenuSys->Initialize())return false;
	ProfSys->EndTimer("GameMenuSys");

	ProfSys->StartTimer();
	if (!LogicSys->Initialize())return false;
	ProfSys->EndTimer("LogicSys");

	ProfSys->StartTimer();
	if (!EditorSys->Initialize())return false;
	ProfSys->EndTimer("EditorSys");

	ProfSys->StartTimer();
	if (!CollTable->Initialize())return false;
	ProfSys->EndTimer("CollTable");

	ProfSys->StartTimer();
	if (!TimeSys->Initialize())return false;
	ProfSys->EndTimer("TimeSys");

	ProfSys->StartTimer();
	if (!ChangerSys->Initialize())return false;
	ProfSys->EndTimer("ChangerSys");

	if (!ProfSys->Initialize())return false;

	// If we are going to start ingame,
	// initialize all the systems related to the game
	if (SettingsSys->mStartInGame)
		StartGame();
	
	return true;
}
void MainEngine::Run()
{
	TimeSys->Reset();
	while (running)
	{
		TimeSys->StartFrame();

		if (SettingsSys->inEditor)
			RunEditor();
		else
			RunGame();

		ProfSys->Update();
		TimeSys->EndFrame();
	}
}
void MainEngine::Shutdown()
{
	WindowSys->Shutdown();
	RndMngr->Shutdown();
	InputSys->Shutdown();
	GameMenuSys->Shutdown();
	LogicSys->Shutdown();
	if(SettingsSys->inEditor)
		SceneSys->Shutdown();
	EditorSys->Shutdown();
	Physys->Shutdown();
	Collsys->Shutdown();
	AudioSys->Shutdown();
	RsrcMan->Shutdown();
	CollTable->Shutdown();
	ChangerSys->Shutdown();
	ConsoleSys->Shutdown();
	SettingsSys->Shutdown();
	ProfSys->Shutdown();
	Singletons::ReleaseAll();
}

void MainEngine::OpenEditor()
{
	std::experimental::filesystem::remove_all("./data/Saves");
	SettingsSys->inEditor = true;
	CloseLevel();
}
void MainEngine::CloseEditor()
{
	SettingsSys->inEditor = false;
	StartGame();
}

void MainEngine::Quit()
{
	running = false;
	if(std::experimental::filesystem::exists("./data/Saves"))
		std::experimental::filesystem::remove_all("./data/Saves");
}

void MainEngine::StartGame()
{
	std::experimental::filesystem::create_directory("./data/Saves");
	SceneSys->InitializeSpacesComponents();
	Physys->Initialize();
	Collsys->Initialize();
	LogicSys->Initialize();
}

void MainEngine::CloseLevel()
{
	RndMngr->Clear();
	Physys->Shutdown();
	Collsys->Shutdown();
	LogicSys->Shutdown();
}

/*! \fn     ChangeLevel
	\brief  Makes shutdown in all the systems related to gameplay and initializes them again.
			It is expected that the scene has already loaded the new level.
*/
void MainEngine::ChangeLevel()
{
	CloseLevel();
	StartGame();
}

void MainEngine::RunEditor()
{
	ProfSys->StartTimer();
	WindowSys->Update();
	ProfSys->EndTimer("WindowSys");

	ProfSys->StartTimer();
	InputSys->Update();
	ProfSys->EndTimer("InputSys");

	ProfSys->StartTimer();
	SceneSys->Update();
	ProfSys->EndTimer("SceneSys");

	ProfSys->StartTimer();
	EditorSys->Update();
	ProfSys->EndTimer("EditorSys");

	ProfSys->StartTimer();
	SettingsSys->Update();
	ProfSys->EndTimer("SettingsSys");

	ProfSys->StartTimer();
	RsrcMan->Update();
	ProfSys->EndTimer("RsrcMan");

	ProfSys->StartTimer();
	RndMngr->Update();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	RndMngr->RenderEditor();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	RndMngr->LateUpdate();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	EditorSys->LateUpdate();
	ProfSys->EndTimer("EditorSys");

	ProfSys->StartTimer();
	InputSys->LateUpdate();
	ProfSys->EndTimer("InputSys");
}

void MainEngine::RunGame()
{
	ProfSys->StartTimer();
	WindowSys->Update();
	ProfSys->EndTimer("WindowSys");

	ProfSys->StartTimer();
	InputSys->Update();
	ProfSys->EndTimer("InputSys");

	if (SettingsSys->paused == false)
	{
		ProfSys->StartTimer();
		SceneSys->Update();
		ProfSys->EndTimer("SceneSys");
	}

	ProfSys->StartTimer();
	EditorSys->Update();
	ProfSys->EndTimer("EditorSys");

	ProfSys->StartTimer();
	SettingsSys->Update();
	ProfSys->EndTimer("SettingsSys");

	ProfSys->StartTimer();
	GameMenuSys->Update();
	ProfSys->EndTimer("GameMenuSys");

	if (SettingsSys->paused == false)
	{
		ProfSys->StartTimer();
		LogicSys->Update();
		ProfSys->EndTimer("LogicSys");

		ProfSys->StartTimer();
		Physys->Update();
		ProfSys->EndTimer("Physys");

		ProfSys->StartTimer();
		Collsys->Update();
		ProfSys->EndTimer("Collsys");

		ProfSys->StartTimer();
		SceneSys->Update();
		ProfSys->EndTimer("SceneSys");

		ProfSys->StartTimer();
		AudioSys->Update();
		ProfSys->EndTimer("AudioSys");
	}

	ProfSys->StartTimer();
	RndMngr->Update();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	RndMngr->Render();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	GameMenuSys->LateUpdate();
	ProfSys->EndTimer("GameMenuSys");

	if (SettingsSys->paused == false)
	{
		ProfSys->StartTimer();
		LogicSys->LateUpdate();
		ProfSys->EndTimer("LogicSys");

		ProfSys->StartTimer();
		Physys->LateUpdate();
		ProfSys->EndTimer("Physys");

		ProfSys->StartTimer();
		Collsys->LateUpdate();
		ProfSys->EndTimer("Collsys");

		ProfSys->StartTimer();
		AudioSys->LateUpdate();
		ProfSys->EndTimer("AudioSys");
	}

	ProfSys->StartTimer();
	RndMngr->LateUpdate();
	ProfSys->EndTimer("RndMngr");

	ProfSys->StartTimer();
	EditorSys->LateUpdate();
	ProfSys->EndTimer("EditorSys");

	ProfSys->StartTimer();
	InputSys->LateUpdate();
	ProfSys->EndTimer("InputSys");

	// It will be called the last one when being ingame to handle changing level
	if (SettingsSys->paused == false)
	{
		ProfSys->StartTimer();
		SceneSys->LateUpdate();
		ProfSys->EndTimer("SceneSys");
	}
}

