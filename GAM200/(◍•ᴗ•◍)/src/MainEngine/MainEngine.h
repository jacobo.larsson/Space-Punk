/**********************************************************************************/
/*
\file   MainEngine.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the initialize, run and shutdown of the engine
*/
/*********************************************************************************/
#pragma once


#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IBase.h"
#include "../Singleton/Singleton.h"

#include "../Raycast/Raycast.h"

class MainEngine : public IBase
{
	friend class Editor;
	SM_RTTI_DECL(MainEngine, IBase);
	Make_Singleton_Decl(MainEngine)

public:

	virtual bool Initialize();
	void Run();
	void Shutdown();

	void OpenEditor();
	void CloseEditor();
	void ChangeLevel();

	void Quit();
private:

	void StartGame();
	void CloseLevel();
	void RunEditor();
	void RunGame();
	bool running;
	//bool inEditor;
};
#define Engine  MainEngine::Get()
