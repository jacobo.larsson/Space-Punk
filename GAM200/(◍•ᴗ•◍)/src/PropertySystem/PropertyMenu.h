#pragma once
#include <string>

namespace PropertyMenus
{
	namespace Sliders
	{
		void CreateSlider(std::string name, float&  data , float min = 0.0f, float max = 100.0f);
		//void CreateSlider(std::string name, int*  data, int min = 0 , int max = 100);
	}

}