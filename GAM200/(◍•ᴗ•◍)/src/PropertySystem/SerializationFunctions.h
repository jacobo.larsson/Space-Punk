#pragma once

#include "../Extern/json/json.hpp"

#include "../Extern/json/json.hpp"

enum AtomicTypes
{
	IntType,
	FloatType,
	StringType,
	BoolType
};

namespace PropertySerialization
{

	void ToJson(nlohmann::json& j, const int data);
	void ToJson(nlohmann::json& j, const float data );
	void ToJson(nlohmann::json& j, const std::string & data);
	void ToJson(nlohmann::json& j, const bool data);

	void FromJson(const nlohmann::json& j, int& data);
	void FromJson(const nlohmann::json& j, float& data);
	void FromJson(const nlohmann::json& j, std::string &data);
	void FromJson(const nlohmann::json& j, bool& data);
}