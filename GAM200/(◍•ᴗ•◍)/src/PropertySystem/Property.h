#pragma once
#include <string>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"

#include "PropertyMenu.h"

#include "../Extern/json/json.hpp"

#include "SerializationFunctions.h"

using nlohmann::json;

enum MenuType
{
	Slider
};

class Serializable :public IBase
{
public:
	SM_RTTI_DECL(Serializable, IBase);

	void ToJson(nlohmann::json& j)override {}
	void FromJson(const nlohmann::json& j)override {}
	//virtual bool Edit(std::string name) {	return false;}
};

template<typename T>
class Property :public Serializable
{
	SM_RTTI_DECL(Property<T>, Serializable);
public:
	Property();
	Property(const T& rhs);

	T& operator=(const T& rhs);
	T* operator&();
	T& operator*();
	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override; 

	T mData;
};

template<typename T>
inline Property<T>::Property()
{
}

template<typename T>
inline Property<T>::Property(const T& rhs)
	:mData{ rhs }
{}

template<typename T>
inline T& Property<T>::operator=(const T& rhs)
{
	mData = rhs;
	return mData;
}

template<typename T>
inline T* Property<T>::operator&()
{
	return &mData;
}

template<typename T>
inline T& Property<T>::operator*()
{
	return mData;
}

template<typename T>
inline void Property<T>::ToJson(nlohmann::json& j)
{
	PropertySerialization::ToJson(j, mData);
}


template<typename T>
inline void Property<T>::FromJson(const nlohmann::json& j)
{
}
