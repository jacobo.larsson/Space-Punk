#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "PropertyMap.h"

PropertyMap::PropertyMap()
{

}
PropertyMap::~PropertyMap()
{
	Clear();
}
PropertyMap& PropertyMap::operator=(const PropertyMap& rhs)
{
	Clear();
	//std::for_each(rhs.mProperties.begin(), rhs.mProperties.begin(), [&](auto& _Property)
	//	{
	//		operator()(*_Property.second, _Property.first)
	//		*mProperties[_Property.first] = *_Property.second;
	//	});
	return *this;
}
void PropertyMap::ToJson(nlohmann::json& j)
{
	json& properties = j["Properties"];
	for (auto& _property : mProperties)
	{
		nlohmann::json _j;
		// Serializa name of the property
		_property.second->ToJson(_j[_property.first]);
		properties.push_back(_j);
	}
}

void PropertyMap::FromJson(const nlohmann::json& j)
{
	auto properties = j.find("Properties");
	if (properties != j.end())
	{
		std::string key;
		AtomicTypes value = AtomicTypes::FloatType;
		for(auto & _property : *properties)
		{ 
			key = _property.begin().key();
			value = _property[key]["Type"];
			switch (value)
			{
			case AtomicTypes::IntType:
			{
				Property<int>* temp_property = operator()(key, static_cast<int>(_property[key]["Data"]));
			}
				break;
			case AtomicTypes::FloatType:
			{
				Property<float>* temp_property = operator()(key, static_cast<float>(_property[key]["Data"]));
			}
				break;
			case AtomicTypes::StringType:
			{
				Property<std::string>* temp_property = operator()(key, static_cast<std::string>(_property[key]["Data"].get<std::string>()));
			}
				break;
			case AtomicTypes::BoolType:
			{
				Property<bool>* temp_property = operator()(key, static_cast<bool>(_property[key]["Data"]));
			}
				break;
			default:
				break;
			}
		}
	}

}

void PropertyMap::Clear()
{
	std::for_each(mProperties.begin(), mProperties.end(), [&](auto& _Property) 
		{
			delete _Property.second;
		});
	mProperties.clear();
}
