#include "SerializationFunctions.h"

namespace PropertySerialization
{
	void ToJson(nlohmann::json& j, const int data)
	{
		j["Data"] = data;
		j["Type"] = IntType;
	}
	void ToJson(nlohmann::json& j, const float data)
	{
		j["Data"] = data;
		j["Type"] = FloatType;
	}
	void ToJson(nlohmann::json& j, const std::string& data)
	{
		j["Data"] = data;
		j["Type"] = StringType;
	}
	void ToJson(nlohmann::json& j, const bool data)
	{
		j["Data"] = data;
		j["Type"] = BoolType;
	}

#pragma region FromJson

	void FromJson(const nlohmann::json& j, int& data)
	{
		data = j;
	}
	void FromJson(const nlohmann::json& j, float& data)
	{
		data = j;
	}
	void FromJson(const nlohmann::json& j, std::string& data)
	{
		data = j.get<std::string>();
	}
	void FromJson(const nlohmann::json& j, bool& data)
	{
		data = j;
	}

#pragma endregion
}