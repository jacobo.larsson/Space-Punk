#pragma once
#include <map>
#include <string>
#include "../Factory/Factory.h"
#include "../Debug/Console.h"
#include "Property.h"

//class Serializable;
//
//template<typename T>
//class Property;

class PropertyMap
{
public:
	PropertyMap();
	~PropertyMap();
	Serializable* operator[](const std::string& key);
	const Serializable* operator[](const std::string& key)const;

	PropertyMap& operator=(const PropertyMap& rhs);
	template <typename T>
	Property<T> * operator()(const std::string& key, T data);
	template <typename T>
	T& operator()(T _Type, const std::string& key);
	template <typename T>
	const T& operator()(const T _Type, const std::string& key)const;
	Property<std::string>* operator()(const std::string& key, std::string data)
	{
		auto duplicated = mProperties.find(key);

		// Check for duplicates
		if (duplicated != mProperties.find(key))
		{
			Property<std::string>* _Property = reinterpret_cast<Property<std::string>*>(duplicated->second);
			*_Property = data;
			return _Property;
		}
		else
			return reinterpret_cast<Property<std::string>*>(mProperties[key] = new Property<std::string>(data));
	}
	Property<std::string>* operator()(const std::string& key, const char *data)
	{
		return operator()(key, std::string(data));
	}

	template<typename T>
	void AddProperty( const std::string& name);

	void ToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j);

private:
	void Clear();

	std::map<std::string, Serializable*> mProperties;
};


inline Serializable* PropertyMap::operator[](const std::string& key)
{
	auto found = mProperties.find(key);
	if (found != mProperties.end())
	{
		// The property was alrady generated
		return found->second;
	}
	return nullptr;
}

inline const Serializable* PropertyMap::operator[](const std::string& key)const
{
	auto found = mProperties.find(key);
	if (found != mProperties.find(key))
		return found->second;
	return nullptr;
}


template<typename T>
inline Property<T>* PropertyMap::operator()(const std::string& key, T data)
{
	auto duplicated = mProperties.find(key);

	// Check for duplicates
	if (duplicated != mProperties.end())
	{
		Property<T>* _Property = reinterpret_cast<Property<T>*>(duplicated->second);
		*_Property = data;
		return _Property;
	}
	return nullptr;
}

template<typename T>
inline T& PropertyMap::operator()(T _Type, const std::string& key)
{
	auto duplicated = mProperties.find(key);

	// Check for duplicates
	if (duplicated != mProperties.end())
	{
		Property<T>* _Property = reinterpret_cast<Property<T>*>(duplicated->second);
		return **_Property;
	}
	else
	{
		ConsoleSys->DebugPrint("Could not find %s property.\n", key.c_str());
		AddProperty<T>(key);
		return operator()(_Type, key);
	}
}

template<typename T>
inline const T& PropertyMap::operator()(const T _Type, const std::string& key) const
{
	auto duplicated = mProperties.find(key);

	// Check for duplicates
	if (duplicated != mProperties.end())
	{
		Property<T>* _Property = reinterpret_cast<Property<T>*>(duplicated->second);
		return **_Property;
	}
	else
	{
		ConsoleSys->DebugPrint("Could not find %s property.\n", key.c_str());
		exit(-1);
	}
}

template<typename T>
inline void PropertyMap::AddProperty(const std::string& name)
{
	auto found = mProperties.find(name);
	// TODO: AssertSystem
	if (found == mProperties.end())
		mProperties[name]  = FactorySys->Create<Property<T>>();
}
