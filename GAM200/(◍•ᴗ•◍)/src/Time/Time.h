/**********************************************************************************/
/*
\file   Time.h
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that use and make time.
*/
/*********************************************************************************/
#pragma once

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"


class  FRC : public IBase
{
	SM_RTTI_DECL(FRC, IBase);
	Make_Singleton(FRC)

public:
	virtual bool Initialize();
	virtual void Update();

	// call between each frame for frame statistics
	void StartFrame();
	void EndFrame();
	void Reset();

	double GetMaxFrameRate();
	double GetFrameRate();
	double GetFrameTime();
	double GetFrameCounter();

	// set max frame rate
	void LockFrameRate(bool enabled) { bFrameRateLocked = enabled; }
	inline bool FrameRateLocked() { return bFrameRateLocked; }
	void SetMaxFrameRate(double fps);

	// uses the CPU clock to return a time in seconds.
	static double GetCPUTime();

private:

	bool bFrameRateLocked = true;
	double	sFrameRateMax = 60.0;	// clamp the frame rate to at most this number
	double	sFrameRate = 0.0 ;		// the frame rate based on the last frame
	double	sFrameTime = 0.0 ;		// time taken to process the last frame(in seconds)
	double	sFrameCounter = 0.0 ;	// number of frame since the last reset
	double	sFrameTimeMin = 0.0;
	double	sFrameTimeMax = 0.0;
	double	sFrameTimeStart = 0.0;
	double	sFrameTimeEnd = 0.0;
};
// Easy access to singleton
#define TimeSys FRC::Get()
