#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "Timer.h"
#include "Time.h"

Timer::Timer()
	: mMaxTime(0.0)
	, mCurrentTime(0.0)
	, mPause(false)
{
	Reset();
}
void Timer::ToJson(nlohmann::json& j)
{
	j["MaximumTime"] = mMaxTime;
}
void Timer::FromJson(const nlohmann::json& j)
{
	if(j.find("MaximumTime") != j.end())
		mMaxTime = j["MaximumTime"];
}
void Timer::Reset()
{
	// reset the time stamps to the current time
	mCurrentTime = 0.0f;
}
bool Timer::Edit(std::string _Name)
{
	bool changed = false;
	ImGui::DragFloat(_Name.c_str(), &mMaxTime, 0.1f, 0.0f);
	return changed;
}
void Timer::SetMaximum(float _Maximum)
{
	mMaxTime = _Maximum;
}
float Timer::GetCurrentTime()
{
	return mCurrentTime;
}
float Timer::GetMaximumTime()
{
	return mMaxTime;
}
bool Timer::HasReachedMaximum()
{
	return mCurrentTime > mMaxTime;
}
void Timer::Start()
{
	mPause = false;
}
void Timer::Pause()
{
	mPause = true;
}

void Timer::Update()
{
	if(!mPause)
		mCurrentTime += (float)TimeSys->GetFrameTime();
}
