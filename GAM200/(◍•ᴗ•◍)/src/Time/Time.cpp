/**********************************************************************************/
/*
\file   Time.cpp
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declarations of the functions of Time.h
*/
/*********************************************************************************/
#include <Windows.h>

#include "../Factory/Factory.h"
#include "../Physics/PhysicSystem.h"

#include "Time.h"

#define FRAME_RATE_SYNC_TO_RETRACE 0


FRC* FRC::instance = nullptr;

bool FRC::Initialize()
{
	FRC::sFrameCounter = 0;
	FRC::sFrameRateMax = 60;
	FRC::sFrameRate = FRC::sFrameRateMax;
	FRC::sFrameTime = 1.0 / FRC::sFrameRate;
	FRC::sFrameTimeMin = 1.0 / FRC::sFrameRateMax;
	FRC::sFrameTimeMax = 1.0 / FRC::sFrameRate;
	return true;
}
void FRC::Update()
{
	if (sFrameCounter)
		EndFrame();
	else
		sFrameCounter++;
	StartFrame();
}

void FRC::StartFrame()
{
	FRC::sFrameTimeStart = FRC::GetCPUTime();
}
void FRC::EndFrame()
{

	// if the total time spent is less than the minimum required time to 
	// maintain the maximum frame rate, wait
	do
	{
		FRC::sFrameTimeEnd = GetCPUTime();
		// calculate the amount of time spend this frame
		sFrameTime = sFrameTimeEnd - sFrameTimeStart;
	} while (bFrameRateLocked && (sFrameTime) < FRC::sFrameTimeMin);

	if (sFrameTime > sFrameTimeMax && Physys->slowmo==false)
		sFrameTime = sFrameTimeMax;
	else if (sFrameTime > sFrameTimeMax && Physys->slowmo == true)
		sFrameTime = sFrameTimeMax/3;

	//@FIXED - Reset the frame rate variable
	sFrameRate = 1.0 / sFrameTime;

	// increment the total number of counter
	sFrameCounter++;
}
void FRC::Reset()
{
	//AE_ASSERT_MESG(gAEFrameRateMax > 0.0, "maximum frame rate MUST be greater than 0");

	FRC::sFrameCounter = 0;
	FRC::sFrameRate = sFrameRateMax;
	FRC::sFrameTime = 1.0 / FRC::sFrameRate;
	FRC::sFrameTimeMin = 1.0 / FRC::sFrameRateMax;
}

double FRC::GetMaxFrameRate()
{
	return sFrameRateMax;
}
double FRC::GetFrameRate()
{
	return sFrameRate;
}
/* It returns dt. 
*/
double FRC::GetFrameTime()
{
	return sFrameTime;
}
double FRC::GetFrameCounter()
{
	return sFrameCounter;
}

void FRC::SetMaxFrameRate(double fps)
{
	sFrameRateMax = fps;
	FRC::sFrameTimeMin = 1.0 / FRC::sFrameRateMax;
}

double FRC::GetCPUTime()
{
	signed long long f, t;
	double r, r0, r1;

	QueryPerformanceFrequency((LARGE_INTEGER*)(&f));
	QueryPerformanceCounter((LARGE_INTEGER*)(&t));

	//@FIXED - precision warning
	r0 = double(t / f);
	r1 = (t - ((t / f) * f)) / (double)(f);
	r = r0 + r1;

	return r;//r0 + r1;
}

