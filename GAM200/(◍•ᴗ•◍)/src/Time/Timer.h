#pragma once
#include "../RTTI/IBase.h"

struct Timer: public IBase
{
	Timer();

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void Start();	// Sets paused to false
	void Pause();	// Sets paused to true
	void Update();
	void Reset();	// Resets the timer values
	bool Edit(std::string  _Name);

	void SetMaximum(float _Maximum);

	float GetCurrentTime();
	float GetMaximumTime();
	bool HasReachedMaximum();
private:
	float mMaxTime;
	float mCurrentTime;
	bool mPause;
};
