#pragma once
#include <string>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"


class Level : public IBase
{
	SM_RTTI_DECL(Level, IBase);
public:
	Level() {}
	Level(std::string name) : level_name(name) {}
	std::string level_name;
private:
};