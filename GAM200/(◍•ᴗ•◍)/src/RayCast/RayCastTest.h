#include <glm/glm.hpp>

class Space;
class LineSegment2D;
class Transform2D;
class Ray;
class GameObject;


float RayCastLine(const glm::vec2& origin, const glm::vec2& dir, const LineSegment2D& line, glm::vec2* outPi);
float RayCastRect(const glm::vec2& origin, const glm::vec2& dir, const Transform2D& rect, glm::vec2* outPi);
float RayCastCircle(const glm::vec2& origin, const glm::vec2& dir, const glm::vec2& circle, float radius, glm::vec2* outPi);

float RayCastLine(const Ray& ray, const LineSegment2D& line, glm::vec2* outPi);
float RayCastRect(const Ray& ray, const Transform2D& rect, glm::vec2* outPi);
float RayCastCircle(const Ray& ray, const glm::vec2& circle, float radius, glm::vec2* outPi);

float CheckRayCollisions(Ray* ray, Space* space, GameObject** outObj, glm::vec2* outPi, GameObject* owner);
float CheckRayWithVector(Ray* _Ray, GameObject** outObj, glm::vec2* outPi, std::vector<GameObject*> & _Objects);
float CheckRayWithVector(glm::vec2 _Origin, glm::vec2 _Direction, float _Length, GameObject** outObj, glm::vec2* outPi, std::vector<GameObject*> & _Objects);
float CheckRayWithObject(Ray* _Ray, glm::vec2* outPi, GameObject* _Other);
void DrawRay(Ray* ray);