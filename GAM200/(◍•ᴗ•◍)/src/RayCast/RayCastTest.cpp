#define GLM_FORCE_RADIANS

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <algorithm>
#include <iostream>

#include "../Space/Space.h"
#include "../Scene/Scene.h"

#include "LineSegment2D.h"
#include "../Transform2D/Transform2D.h"
#include "../RayCast/Raycast.h"
#include "../Graphics/Color/Color.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../GameObject/GameObject.h"
#include "../Space/Space.h"
#include "../Collisions/ICollider.h"
#include "RayCastTest.h"


float RayCastLine(const glm::vec2& origin, const glm::vec2& dir, const LineSegment2D& line, glm::vec2* outPi)
{
	//Compute the denominator of the time of intersection fraction
	float denominator = glm::dot(dir, line.mN);

	//Compute the numerator of the time of intersection fraction
	float numerator = glm::dot(line.mP0, line.mN) - glm::dot(origin, line.mN);

	//Execute if the denominator is equal to 0 (lines are parallel)
	if (denominator == 0)
	{
		//Return -1 (not valid time)
		return -1;
	}

	//Compute the time of intersection
	float t = numerator / denominator;

	//Compute the point of intersection
	glm::vec2 intersection = origin + dir * t;

	//Compute the edge vector of the segment line
	glm::vec2 edge = line.mP1 - line.mP0;

	//Compute the vector that goes from the initial point of the line segment
	//to the point of intersection
	glm::vec2	origintointersect = intersection - line.mP0;

	//Compute the vector that goes from the ending point of the line segment
	//to the point of intersection
	glm::vec2 endtointersect = intersection - line.mP1;

	//Execute if the point is behind the line segment
	if (glm::dot(edge, origintointersect) < 0)
		return -1;

	//Execute if the point is in front of the line segment
	if (glm::dot(-edge, endtointersect) < 0)
		return -1;

	//Execute if outpi is valid
	if (outPi)
		*outPi = intersection;

	//Return the intersection time
	return t;
}
float RayCastRect(const glm::vec2& origin, const glm::vec2& dir, const Transform2D& rect, glm::vec2* outPi)
{
	//Create temporal times of intersections
	float t1 = -1;
	float t2 = -1;
	float t3 = -1;
	float t4 = -1;

	//Create a boolean to know if a _Ray intersects
	bool collide = false;

	glm::mat3 vertexrot1 = glm::rotate(glm::mat3(1.0), rect.mOrientation);

	glm::vec3 _OBBVertices[4] = { { rect.mScale.x / 2, rect.mScale.y / 2, 0 }, { -rect.mScale.x / 2, rect.mScale.y / 2, 0 },
								  { -rect.mScale.x / 2, -rect.mScale.y / 2, 0 }, { rect.mScale.x / 2, -rect.mScale.y / 2, 0 } };
	for (unsigned i = 0; i < 4; i++)
	{
		_OBBVertices[i] = vertexrot1 * _OBBVertices[i] + rect.mPosition;
	}

	//Create an array to store the line segments of the OBB
	LineSegment2D _OBBLines[4];
	_OBBLines[0] = LineSegment2D(_OBBVertices[0], _OBBVertices[1]);
	_OBBLines[1] = LineSegment2D(_OBBVertices[1], _OBBVertices[2]);
	_OBBLines[2] = LineSegment2D(_OBBVertices[2], _OBBVertices[3]);
	_OBBLines[3] = LineSegment2D(_OBBVertices[3], _OBBVertices[0]);

	//Compute a time of intersection with one line segment
	t1 = RayCastLine(origin, dir, _OBBLines[0], outPi);
	t2 = RayCastLine(origin, dir, _OBBLines[1], outPi);
	t3 = RayCastLine(origin, dir, _OBBLines[2], outPi);
	t4 = RayCastLine(origin, dir, _OBBLines[3], outPi);

	//Execute if collide is true
	if (t1 > -1 || t2 > -1 || t3 > -1 || t4 > -1)
	{
		//Execute if t1 is equal to -1 (is invalid) 
		if (t1 <= -1)
			t1 = INFINITY;
		if (t2 <= -1)
			t2 = INFINITY;
		if (t3 <= -1)
			t3 = INFINITY;
		if (t4 <= -1)
			t4 = INFINITY;

		//Store in t1 the minimun time between t1 and t2
		t1 = std::min(t1, t2);

		//Store in t3 the minimun time between t3 and t4
		t3 = std::min(t3, t4);

		//Store in t1 the minimun time between t1 and t3
		t1 = std::min(t1, t3);
	}

	//Execute if outpi is valid
	if (outPi)
		*outPi = origin + dir * t1;

	//Return the minimun time of intersection
	return t1;
}
float RayCastCircle(const glm::vec2& origin, const glm::vec2& dir, const glm::vec2& circle, float radius, glm::vec2* outPi)
{
	//Compute the "A" part of the quadratic formula
	float a = glm::dot(dir, dir);

	//Compute the "B" part of the quadratic formula
	float b = -2 * (glm::dot((circle - origin), dir));

	//Compute the "C" part of the quadratic formula
	float c = (glm::dot((circle - origin), (circle - origin)) - glm::dot(radius, radius));

	//Compute the delta part of the quadratic formula
	float delta = b * b - 4 * a * c;

	//Create temporal times of intersections?
	float t1 = -1;
	float t2 = -1;
	float t3 = -1;

	//Create a temporal point of intersection
	glm::vec2 intersection1 = glm::vec2(0, 0);

	//Execute if delta is less than 0 (no solution for the quadratic equation)
	if (delta < 0)
	{
		//Return -1 (not valid time)
		return -1;
	}

	//Else execute if delta is equal to 0 (one solution for the quadratic equation)
	else if (delta == 0)
	{
		//Compute the intersection time
		t3 = (-b) / (2 * a);

		//Execute if t3 is negative
		if (t3 < 0)
		{
			//Return -1 (not valid time)
			return -1;
		}

	}

	//Else execute if delta is bigger than 0 (two solutions for the quadratic equation)
	else if (delta > 0)
	{
		//Compute the first intersection time
		t1 = (-b + sqrt(delta)) / (2 * a);

		//Compute the second intersection time
		t2 = (-b - sqrt(delta)) / (2 * a);

		//Execute if the two times are negative
		if (t1 < 0 && t2 < 0)
		{
			//Return -1 (not valid times)
			return -1;

		}

		//Else execute if t2 is negative
		else if (t2 < 0)
		{
			//Store in t3 the minimun valid time (t1)
			t3 = t1;
		}

		//Else execute if t1 is negative
		else if (t1 < 0)
		{
			//Store in t3 the minimun valid time (t2)
			t3 = t2;
		}
		else
		{
			//Store in t3 the minimun between t1 and t2
			t3 = std::min(t1, t2);
		}

	}

	//Compute the point of intersection using the minimun time
	intersection1 = origin + dir * t3;

	//Execute of outpi is valid
	if (outPi)
	{
		//Store in outpi the point of intersection
		*outPi = intersection1;

	}

	//Return the minimun time of intersection
	return t3;
}

float RayCastCircle(const Ray& ray, const glm::vec2& circle, float radius, glm::vec2* outPi)
{
	glm::vec2 RealPos = glm::vec2{ ray.owner_transform->GetWorldPosition() }
	+ ray.mOrigin;
	float RealOrient = ray.owner_transform->GetWorldOrientation() + ray.mOrientation;

	return RayCastCircle(RealPos, glm::rotate(glm::vec2(1, 0), RealOrient), circle, radius, outPi);
}
float RayCastRect(const Ray& ray, const Transform2D& rect, glm::vec2* outPi)
{
	//glm::vec2 RealPos = glm::vec2{ ray.owner_transform->GetWorldPosition() }
	//+ ray.mOrigin;
	//float RealOrient = ray.owner_transform->GetWorldOrientation() + ray.mOrientation;

	return RayCastRect(ray.mOrigin, glm::rotate(glm::vec2(1, 0), ray.mOrientation), rect, outPi);

	//glm::vec2 RealPos = glm::vec2{ ray.owner_transform->GetWorldPosition() }
	//+ ray.mOrigin;
	//float RealOrient = ray.owner_transform->GetWorldOrientation() + ray.mOrientation;
	//
	//return RayCastRect(RealPos, glm::rotate(glm::vec2(1, 0), RealOrient), rect, outPi);
}
float RayCastLine(const Ray& ray, const LineSegment2D& line, glm::vec2* outPi)
{
	glm::vec2 RealPos = glm::vec2{ ray.owner_transform->GetWorldPosition() }
	+ ray.mOrigin;
	float RealOrient = ray.owner_transform->GetWorldOrientation() + ray.mOrientation;

	return RayCastLine(RealPos, glm::rotate(glm::vec2(1, 0), RealOrient), line, outPi);
}

float CheckRayCollisions(Ray* _Ray, Space* _Space, GameObject** _OutObj, glm::vec2* _OutPI, GameObject* _Other)
{
	if (_Ray == nullptr)
		return -1;

	float _FinalTime = INFINITY;

	for (auto &_Collider : reinterpret_cast<std::vector<Collider*>&>(_Space->GetComponents<Collider>()))
	{
		if (_Collider != nullptr && _Collider->GetOwner() != _Other)
		{
			float temp_time = 0;
			switch (_Collider->GetShape())
			{
			case CollisionShape::CSHAPE_AABB:
			case CollisionShape::CSHAPE_OBB:
				temp_time = RayCastRect(*_Ray, _Collider->GetCollideBox().GetWorldTransform(), _OutPI);
				break;
			case CollisionShape::CSHAPE_CIRCLE:
				temp_time = RayCastCircle(*_Ray, _Collider->GetCollideBox().GetWorldPosition(), _Collider->GetCollideBox().GetWorldScale().x, _OutPI);
				break;
			default:
				break;
			}

			if (temp_time > 0 && temp_time < _FinalTime)
			{
				if (_OutObj != nullptr)
					*_OutObj = _Collider->GetOwner();

				_FinalTime = temp_time;
			}
		}
	}

	if (_FinalTime == INFINITY)
		_FinalTime = -1;

	return _FinalTime;
}
float CheckRayWithVector(Ray* _Ray, GameObject** _OutObj, glm::vec2* _OutPI, std::vector<GameObject*>& _Objects)
{
	if (_Ray == nullptr )
		return -1;

	float _FinalTime = INFINITY;

	if (_Objects.size())
	{
		Collider* _Collider;
		for (auto _Object : _Objects)
		{
			_Collider = _Object->GetComp<Collider>();

			if (_Collider != nullptr)
			{
				float temp_time = 0;
				switch (_Collider->GetShape())
				{
				case CollisionShape::CSHAPE_AABB:
				case CollisionShape::CSHAPE_OBB:
					temp_time = RayCastRect(*_Ray, _Collider->GetCollideBox().GetWorldTransform(), _OutPI);
					break;
				case CollisionShape::CSHAPE_CIRCLE:
					temp_time = RayCastCircle(*_Ray, _Collider->GetCollideBox().GetWorldPosition(), _Collider->GetCollideBox().GetWorldScale().x, _OutPI);
					break;
				default:
					break;
				}

				if (temp_time > 0 && temp_time < _FinalTime)
				{
					if (_OutObj != nullptr)
						*_OutObj = _Object;

					_FinalTime = temp_time;
				}
			}
		}
	}

	if (_FinalTime == INFINITY)
		_FinalTime = -1;

	return _FinalTime;
}
float CheckRayWithVector(glm::vec2 _Origin, glm::vec2 _Direction, float _Length, GameObject** outObj, glm::vec2* outPi, std::vector<GameObject*>& _Objects)
{
	float _FinalTime = INFINITY;

	if (_Objects.size())
	{
		Collider* _Collider;
		for (auto _Object : _Objects)
		{
			_Collider = _Object->GetComp<Collider>();

			if (_Collider != nullptr)
			{
				float temp_time = 0;
				switch (_Collider->GetShape())
				{
				case CollisionShape::CSHAPE_AABB:
				case CollisionShape::CSHAPE_OBB:
					temp_time = RayCastRect(_Origin, _Direction, _Collider->GetCollideBox().GetWorldTransform(), outPi);
					break;
				case CollisionShape::CSHAPE_CIRCLE:
					temp_time = RayCastCircle(_Origin, _Direction, _Collider->GetCollideBox().GetWorldPosition(), _Collider->GetCollideBox().GetWorldScale().x, outPi);
					break;
				default:
					break;
				}

				if (temp_time > 0 && temp_time < _FinalTime)
				{
					if (outObj != nullptr)
						*outObj = _Object;

					_FinalTime = temp_time;
				}
			}
		}
	}

	if (_FinalTime == INFINITY)
		_FinalTime = -1;

	return _FinalTime;
}
float CheckRayWithObject(Ray* _Ray, glm::vec2* _OutPI, GameObject* _Other)
{
	if (_Ray == nullptr || _Other == nullptr)
		return -1;

	float _FinalTime = INFINITY;

	Collider* _Collider = _Other->GetComp<Collider>();

	if (_Collider != nullptr)
	{
		float temp_time = 0;
		switch (_Collider->GetShape())
		{
		case CollisionShape::CSHAPE_AABB:
		case CollisionShape::CSHAPE_OBB:
			temp_time = RayCastRect(*_Ray, _Collider->GetCollideBox().GetWorldTransform(), _OutPI);
			break;
		case CollisionShape::CSHAPE_CIRCLE:
			temp_time = RayCastCircle(*_Ray, _Collider->GetCollideBox().GetWorldPosition(), _Collider->GetCollideBox().GetWorldScale().x, _OutPI);
			break;
		default:
			break;
		}

		if (temp_time > 0 && temp_time < _FinalTime)
			_FinalTime = temp_time;
	}

	if (_FinalTime == INFINITY)
		_FinalTime = -1;

	return _FinalTime;
}

void DrawRay(Ray* ray)
{
	if (ray)
	{
		if (ray->owner_transform)
		{
			glm::vec2 RealPos = glm::vec2{ ray->owner_transform->GetWorldPosition()} + ray->mOrigin;
			float RealOrient = ray->owner_transform->GetWorldOrientation() + ray->mOrientation;

			SceneSys->GetMainSpace()->DrawLine(RealPos, RealPos + (glm::rotate(glm::vec2(1, 0), RealOrient) * ray->mLength), Colors::red);
		}
	}
}