#pragma once

#include <vector>

#include "../RTTI/SM_RTTI.h"
#include "../Extern/json/json.hpp"

#include "../LogicSystem/LogicComponent.h"

class Ray;

class RayCaster : public LogicComponent
{
	SM_RTTI_DECL(RayCaster, LogicComponent);

public:

	RayCaster();
	~RayCaster();
	RayCaster& operator=(const RayCaster& rhs);
	RayCaster* Clone() override;

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	Ray* AddRay(Ray* ray);
	void RemoveRay(Ray* ray);

	Ray* GetRay(std::string name);
	std::vector<Ray*> GetAllRays(std::string name);

	bool Edit() override;

private:
	std::vector<Ray*> mMyRays;
};