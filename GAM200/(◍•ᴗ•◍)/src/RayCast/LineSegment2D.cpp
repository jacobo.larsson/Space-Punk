// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior 
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		LineSegment2D.cpp
//
//	Purpose:		Implementation of the creation of line segments.					
//
//	Project:		State Machines  
//			
//	Author:			Juan Pacheco Larrucea, pacheco.j, 540000219
// ----------------------------------------------------------------------------


#include "LineSegment2D.h"
// ---------------------------------------------------------------------------


// @TODO
/*!
\brief		Constructs a dummy line segment.
\details	Sets all values to zero
*/
LineSegment2D::LineSegment2D()
{
	//Set the initial point to 0,0
	mP0 = glm::vec2(0, 0);

	//Set the endpoint to 0,0
	mP1 = glm::vec2(0, 0);

	//Set the normal vector to 0,0
	mN = glm::vec2(0, 0);

	//Set the dot product between the normal and the initial point to 0
	mNdotP0 = 0;
}

// @TODO
/*! ---------------------------------------------------------------------------

\brief	Constructs a 2D line segment's data using 2 points

\details:
- Edge is defined as E = P1-P0
- mN is the outward normal and is defined as mN = -E.Perp().Normalize();
- mNdotP0 = the dot product of the normal with p0.

---------------------------------------------------------------------------*/
LineSegment2D::LineSegment2D(const glm::vec2 & p0, const glm::vec2 &p1)
{
	//Set the initial point to the given initial point
	mP0 = p0;

	//Set the endpoint to the given endpoint
	mP1 = p1;

	//Compute the edge vector as the vector from intitial to ending point
	glm::vec2 edgevector = p1 - p0;

	//Compute the normal vector as the perpendicular of the edgvector and 
	//then normalize (the vector is inverted by convention)
	
	edgevector = -edgevector;
	float temp = edgevector.y;
	edgevector.y = -edgevector.x;
	edgevector.x = temp;

	mN = glm::normalize(edgevector);
	//mN = -edgevector.Perp().Normalize();

	//Compute the dot product between the normal and the initial point 
	mNdotP0 = glm::dot(p0 , mN);

}


// @TODO
/*!
 \brief	This function determines the distance separating a point from a line

 \return	The distance. Note that the returned value should be:
			- Negative if the point is in the line's inside half plane
			- Positive if the point is in the line's outside half plane
			- Zero if the point is on the line
*/
float StaticPointToStaticLineSegment(glm::vec2 *P, LineSegment2D *LS)
{

	//Return the distance between a point and a line
	return (glm::dot(*P, LS->mN) - LS->mNdotP0);
}

// @TODO
/*!
 \brief	Given a point P and an array of LineSegment2D, determines if a point
			is contained by all line segments (ie. inside half plane of all segments).

 \return	true if the point is inside all line segments, false otherwise.
*/
bool PointInLineSegments(glm::vec2 *P, LineSegment2D *LS, unsigned int count)
{
	//Do a loop to check all the line segments
	for (unsigned int i = 0; i < count; i++)
	{
		//Check if the the given point is outside the half plane of a line
		if (StaticPointToStaticLineSegment(P, (LS + i)) >= 0)
		{
			//Return false if it is outside
			return false;
		}
	}

	//Return true if it is inside the half plane of all the line segments
	return true;
}