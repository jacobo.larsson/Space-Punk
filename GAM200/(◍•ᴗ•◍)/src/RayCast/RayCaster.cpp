#define GLM_FORCE_RADIANS

#include <glm/gtx/rotate_vector.hpp>
#include <iostream>

#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include "../Factory/Factory.h"
#include "../Settings/Settings.h"
#include "../Editor/Menus/Menus.h"
#include "../GameObject/GameObject.h"
#include "../Transform2D/Transform2D.h"

#include "Raycast.h"
#include "RayCaster.h"
#include "RayCastTest.h"

RayCaster::RayCaster()
{
	mName = "RayCaster";
}
RayCaster::~RayCaster() {}
RayCaster& RayCaster::operator=(const RayCaster& rhs)
{
	for (int i = 0; i < rhs.mMyRays.size(); i++)
	{
		Ray* newray = reinterpret_cast<Ray*>(FactorySys->Create<Ray>());
		*newray = *rhs.mMyRays[i];
		AddRay(newray);

	}

	return *this;
}
RayCaster* RayCaster::Clone()
{
	RayCaster* temp = FactorySys->Create<RayCaster>();
	(*temp) = (*this);
	return temp;
}

void RayCaster::Initialize()
{
	LogicComponent::Initialize();
	GameObject* object_owner = GetOwner();
	if (object_owner)
	{
		IComp* parent_transform2 = object_owner->GetComp<TransformComponent>();
		if (parent_transform2)
		{
			TransformComponent* parent_transform_casted = dynamic_cast<TransformComponent*>(parent_transform2);

			if (parent_transform_casted)
			{
				for (auto it = mMyRays.begin(); it != mMyRays.end(); it++)
				{
					(*it)->SetOwnerTransform(parent_transform_casted);
				}
			}
		}
	}
}
void RayCaster::Update()
{
	for (auto it = mMyRays.begin(); it != mMyRays.end(); it++)
	{
		if (SettingsSys->debug_draw)
		{
			if ((*it)->DrawInGame)
				DrawRay(*it);
		}
	}
}
void RayCaster::Shutdown()
{
	for (auto it = mMyRays.begin(); it != mMyRays.end(); it++)
		delete (*it);

	mMyRays.clear();
	LogicComponent::Shutdown();
}

void RayCaster::ToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);

	if (mMyRays.size())
	{
		json& mRays = j["rays"];
		for (auto it = mMyRays.begin(); it != mMyRays.end(); it++)
		{
			json ray;
			(*it)->ToJson(ray);
			mRays.push_back(ray);
		}
	}
}
void RayCaster::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	auto raysJson = j.find("rays");
	if (raysJson != j.end())
	{
		if (mMyRays.empty())
		{
			for (const auto& rayJson : *raysJson)
			{
				if (rayJson.find("name") != rayJson.end())
				{
					std::string name = rayJson["name"];
					AddRay(reinterpret_cast<Ray*>(FactorySys->Create<Ray>()))->FromJson(rayJson);
				}
			}
		}
	}
}

Ray* RayCaster::AddRay(Ray* ray)
{
	if (ray == nullptr)
		return nullptr;

	auto duplicated = std::find_if(mMyRays.begin(), mMyRays.end(), [&](auto& it)
	{
		return ray == it;
	});

	if (duplicated == mMyRays.end())
	{
		//ray->SetOwner(*this);
		ray->SetEnabled(mEnabled);

		GameObject* object_owner = GetOwner();
		if (object_owner)
		{
			IComp* parent_transform2 = object_owner->GetComp<TransformComponent>();
			if (parent_transform2)
			{
				TransformComponent* parent_transform_casted = dynamic_cast<TransformComponent*>(parent_transform2);

				if (parent_transform_casted)
				{
					ray->SetOwnerTransform(parent_transform_casted);
				}
			}
		}


		mMyRays.push_back(ray);
	}

	return ray;
}
void RayCaster::RemoveRay(Ray* ray)
{
	if (ray == nullptr)
		return;
	auto to_delete = std::find(mMyRays.begin(), mMyRays.end(), ray);

	if (to_delete != mMyRays.end())
	{
		mMyRays.erase(to_delete);
		delete ray;
	}
}

Ray* RayCaster::GetRay(std::string name)
{
	for (auto& ray : mMyRays) {
		if (ray->GetName() == name) {
			return ray;
		}
	}
	return nullptr;
}
std::vector<Ray*> RayCaster::GetAllRays(std::string name)
{
	std::vector<Ray*> temp;

	for (auto& ray : mMyRays) 
	{
		if (ray && ray->GetName() == name) 
			temp.push_back(ray);
	}

	return temp;
}

bool RayCaster::Edit()
{
	bool changed = false;
	if (ImGui::Button("AddRay"))
	{
		AddRay(reinterpret_cast<Ray*>(FactorySys->Create("Ray")));
	}

	if (ImGui::Begin("RaysMenu", nullptr, 0))
	{
		static char RenameBuffer[64];
		for (auto& it : mMyRays)
		{
			if (it->GetOwnerTransform() == nullptr)
			{
				GameObject* object_owner = GetOwner();
				if (object_owner)
				{
					IComp* parent_transform2 = object_owner->GetComp<TransformComponent>();
					if (parent_transform2)
					{
						TransformComponent* parent_transform_casted = dynamic_cast<TransformComponent*>(parent_transform2);

						if (parent_transform_casted)
						{
							it->SetOwnerTransform(parent_transform_casted);
						}
					}
				}
			}

			if(SettingsSys->debug_draw)
				DrawRay(it);

			ImGui::PushID(&(*it));
			if (ImGui::CollapsingHeader(it->GetName().c_str()))
			{
				changed = it->Edit();

				if (MenusSys->ShowInputText("RenameRay", RenameBuffer, 64))
				{
					it->SetName(RenameBuffer);
					MenusSys->ResetBuffer(RenameBuffer, 64);
				}
				if (ImGui::Button("Delete"))
				{
					RemoveRay(it);
					changed = true;
				}
			}
			ImGui::PopID();
		}
		ImGui::End();
	}
	else
		ImGui::End();
	return changed;
}