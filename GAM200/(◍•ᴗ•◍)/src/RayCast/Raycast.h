#pragma once

#include <glm/glm.hpp>

#include "../RTTI/SM_RTTI.h"

#include "../LogicSystem/LogicComponent.h"
#include "../Transform2D/TransformComponent.h"



class LineSegment2D;
class Space;

/*!
\struct Ray
\brief	 Represents a 2-dimensional ray with an origina and direction.
\remark Direction is not assumed to be normalized.
*/
class Ray : public IBase
{
	friend class Editor;
	friend class Menus;
	friend float RayCastLine(const glm::vec2& origin, const glm::vec2& dir, const LineSegment2D& line, glm::vec2* outPi);
	friend float RayCastRect(const glm::vec2& origin, const glm::vec2& dir, const Transform2D& rect, glm::vec2* outPi);
	friend float RayCastCircle(const glm::vec2& origin, const glm::vec2& dir, const glm::vec2& circle, float radius, glm::vec2* outPi);
	friend float RayCastLine(const Ray& ray, const LineSegment2D& line, glm::vec2* outPi);
	friend float RayCastRect(const Ray& ray, const Transform2D& rect, glm::vec2* outPi);
	friend float RayCastCircle(const Ray& ray, const glm::vec2& circle, float radius, glm::vec2* outPi);
	friend void DrawRay(Ray* ray);
	friend float CheckRayCollisions(Ray* ray, Space* space, GameObject** outObj, glm::vec2* outPi, GameObject* owner);

	SM_RTTI_DECL(Ray, LogicComponent);
public:


	Ray();
	~Ray();
	Ray& operator==(const Ray& rhs);
	Ray* Clone();

	void ToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j);

	void SetOrigin(glm::vec2 origin);
	void SetOrientation(float orientation);
	void SetOwnerTransform(TransformComponent* trans);
	void SetLength(float length);

	glm::vec2 GetOrigin();
	float  GetOrientation();
	TransformComponent* GetOwnerTransform();
	float GetLength();

	bool Edit();

	bool DrawInGame = false;
private:
	glm::vec2 mOrigin;
	float mLength;
	float mOrientation;


	TransformComponent* owner_transform;
};

