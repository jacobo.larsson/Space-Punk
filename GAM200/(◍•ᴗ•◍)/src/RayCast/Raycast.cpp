#define GLM_FORCE_RADIANS

#include <glm/gtx/rotate_vector.hpp>

#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include "../Graphics/RenderManager/RenderManager.h"
#include "../Graphics/Color/Color.h"
#include "../Factory/Factory.h"

#include "../GameObject/GameObject.h"
#include "LineSegment2D.h"

#include "Raycast.h"


Ray::Ray()
{
	mName = "Ray";
	mOrigin = { 0, 0 };

	mLength = 1000.0f;
	mOrientation = 0.0f;

	owner_transform = nullptr;
}
Ray::~Ray()
{}
Ray& Ray::operator==(const Ray& rhs)
{
	mOrigin = rhs.mOrigin;
	mLength = rhs.mLength;
	mOrientation = rhs.mOrientation;

	return *this;
}
Ray* Ray::Clone()
{
	Ray* temp = FactorySys->Create<Ray>();
	(*temp) = (*this);
	return temp;
}

void Ray::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	IBase::ToJson(j);

	json& temp_trans = j["mOrigin"];

	temp_trans.push_back(mOrigin.x);
	temp_trans.push_back(mOrigin.y);

	j["mLength"] = mLength;
	j["mOrientation"] = mOrientation;
	j["DrawInGame"] = DrawInGame;


}
void Ray::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("mOrigin") != j.end())
	{
		if (j["mOrigin"].size() >= 2)
		{
			mOrigin.x = j["mOrigin"][0];
			mOrigin.y = j["mOrigin"][1];
		}
	}

	if (j.find("mLength") != j.end())
		mLength = j["mLength"];

	if (j.find("mOrientation") != j.end())
		mOrientation = j["mOrientation"];

	if (j.find("DrawInGame") != j.end())
		DrawInGame = j["DrawInGame"];
}

void Ray::SetOrigin(glm::vec2 origin)
{
	mOrigin = origin;

}
void Ray::SetOrientation(float orientation)
{
	mOrientation = orientation;


}
void Ray::SetOwnerTransform(TransformComponent* trans)
{
	owner_transform = trans;
}
void Ray::SetLength(float length)
{
	mLength = length;
}

glm::vec2 Ray::GetOrigin()
{
	return mOrigin;
}
float  Ray::GetOrientation()
{
	return mOrientation;
}
TransformComponent* Ray::GetOwnerTransform()
{
	return owner_transform;
}
float Ray::GetLength()
{
	return mLength;
}

bool Ray::Edit()
{
	bool changed = false;

	ImGui::Checkbox("DrawInGame", &DrawInGame);

	ImGui::Text("mOrigin");
	ImGui::PushID("mOrigin");
	ImGui::Columns(2, "mOrigin", true);
	ImGui::DragFloat("X", &mOrigin.x);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::NextColumn();
	ImGui::DragFloat("Y", &mOrigin.y);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();

	ImGui::Separator();

	ImGui::Text("mDirection");
	ImGui::PushID("mDirection");
	ImGui::SliderFloat("Rotation", &mOrientation, 0.0f, 6.28f, "%.3f");
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	
	ImGui::PopID();

	ImGui::Separator();

	ImGui::DragFloat("mLength", &mLength);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	return changed;
}
