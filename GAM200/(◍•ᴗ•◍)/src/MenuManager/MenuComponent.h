#pragma once
#include "../RTTI/IBase.h"
#include "../RTTI/IComp.h"
#include "../RTTI/SM_RTTI.h"


class  MenuComponent : public IComp
{
	SM_RTTI_DECL(MenuComponent, IComp);
public:
	void Initialize()override;
	void Update()override;
	void LateUpdate()override;
	void Shutdown()override;
	virtual bool Edit();
};