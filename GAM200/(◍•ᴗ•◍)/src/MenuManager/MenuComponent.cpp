#include "MenuComponent.h"
#include "MenuManager.h"

void MenuComponent::Initialize()
{
	GameMenuSys->AddButton(this);
}
void MenuComponent::Update()
{
}
void MenuComponent::LateUpdate()
{
}
void MenuComponent::Shutdown()
{
	GameMenuSys->RemoveButton(this);
}
bool MenuComponent::Edit()
{
	return false;
}