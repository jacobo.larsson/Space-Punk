#pragma once
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"

#include "MenuComponent.h"

class GameMenuManager : public IBase
{
	friend class Menus;
	friend class Editor;
	SM_RTTI_DECL(GameMenuManager, IBase);
	Make_Singleton(GameMenuManager)

public:
	// State functions
	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();
	void ChangeButtonVisibility();

	const MenuComponent* AddButton(MenuComponent* new_behavior);
	void RemoveButton(MenuComponent* to_delete);
	void RemoveAllButtons();
private:
	bool ButtonsVisible = true;
	std::vector<MenuComponent*> mButtons;

};
#define GameMenuSys GameMenuManager::Get()