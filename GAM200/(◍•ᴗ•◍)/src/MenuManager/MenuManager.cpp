#include "../Scene/Scene.h"
#include "../Settings/Settings.h"
#include "../GameObject/GameObject.h"
#include "../Graphics/Renderable/Renderable.h"

#include "MenuManager.h"

GameMenuManager* GameMenuManager::instance = nullptr;

bool GameMenuManager::Initialize()
{
	return true;
}
void GameMenuManager::Update()
{
	std::string test = SceneSys->GetLevelName();
	if (test == "./data/Levels/MainMenu.lvl" || test == "./data/Levels/GameOverMenu.lvl" || SettingsSys->paused)
	{
		for (auto& button : mButtons)
		{
			GameObject* owner = button->GetOwner();
			if (owner)
			{
				if (owner->Enabled() && button->Enabled())
					button->Update();
			}
		}
	}
}
void GameMenuManager::LateUpdate()
{
	std::string test = SceneSys->GetLevelName();
	if (test == "./data/Levels/MainMenu.lvl" || test == "./data/Levels/GameOverMenu.lvl" || SettingsSys->paused)
	{
		for (auto& button : mButtons)
		{
			GameObject* owner = button->GetOwner();
			if (owner)
			{
				if (owner->Enabled() && button->Enabled())
					button->Update();
			}
		}
	}
}
void GameMenuManager::Shutdown()
{
	RemoveAllButtons();
}
void GameMenuManager::ChangeButtonVisibility()
{
	std::string test = SceneSys->GetLevelName();
	if (test == "./data/Levels/MainMenu.lvl" || test == "./data/Levels/GameOverMenu.lvl")
		return;
	for (auto& button : mButtons)
		button->GetOwner()->GetComp<Renderable>()->SetVisible(SettingsSys->paused);
}

const MenuComponent* GameMenuManager::AddButton(MenuComponent* new_button)
{
	mButtons.push_back(new_button);
	return new_button;
}
void GameMenuManager::RemoveButton(MenuComponent* to_delete)
{
	auto it = std::find(mButtons.begin(), mButtons.end(), to_delete);
	if (it != mButtons.end())
	{
		mButtons.erase(it);
	}
}
void GameMenuManager::RemoveAllButtons()
{
	mButtons.clear();
}