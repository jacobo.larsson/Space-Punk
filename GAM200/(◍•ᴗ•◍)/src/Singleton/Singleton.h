/**********************************************************************************/
/*
\file   Singleton.h
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that get a singleton and delete it.
*/
/*********************************************************************************/
#pragma once
#include <map>
#include "..\RTTI\IBase.h"
#include "..\RTTI\SM_RTTI.h"

class Singletons
{
private:
	static std::vector<IBase*> sSingletons;
public:
	static void RegisterSingleton(IBase* newSingleton);
	static void ReleaseAll();
};
#define AddSingleton(to_add) Singletons::RegisterSingleton(to_add);

#define Make_Singleton(class_type)\
private:\
class_type(){}\
static class_type * instance;\
public:\
static class_type* Get()\
{\
if (class_type::instance == nullptr)\
{\
    class_type::instance = new class_type;\
    AddSingleton(class_type::instance)\
}\
return class_type::instance; \
}\
private:

#define Make_Singleton_Decl(class_type)\
private:\
class_type();\
static class_type * instance;\
public:\
static class_type* Get()\
{\
if (class_type::instance == nullptr)\
{\
    class_type::instance = new class_type;\
    AddSingleton(class_type::instance)\
}\
return class_type::instance; \
}\
private: