#include "../Graphics/Window/Window.h"
#include "../Input/Input.h"
#include "../Scene/Scene.h"
#include "../Editor/Editor.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../LogicSystem/LogicSystem.h"
#include "../Physics/PhysicSystem.h"
#include "../Collisions/CollisionSystem.h"
#include "../Audio/AudioManager.h"
#include "../Time/Time.h"
#include "../Factory/Factory.h"
#include "../EventSystem/EventDispatcher.h"
#include "../Collisions/CollisionTable.h"
#include "../ResourceManager/ResourceManager.h"
#include "../Editor/Menus/Menus.h"

#include "Singleton.h"

std::vector<IBase*> Singletons::sSingletons;

void Singletons::RegisterSingleton(IBase* newSingleton)
{
	if (newSingleton)
	{
		auto duplicated = std::find(sSingletons.begin(), sSingletons.end(), newSingleton);
		if(duplicated == sSingletons.end())
		{ 
			sSingletons.push_back(newSingleton);
		}
	}
}

void Singletons::ReleaseAll()
{	
	for (auto entry : sSingletons)
	{
		delete entry;
	}
	sSingletons.clear();
}