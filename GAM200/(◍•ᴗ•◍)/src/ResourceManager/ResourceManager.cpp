#include <fstream>
#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/ArchetypesEvents/ArchetypesEvents.h"
#include "../EventSystem/Events/ResourcesEvents/ResourcesEvents.h"
#include "../Scene/Scene.h"
#include "../GameObject/GameObject.h"
#include "../Factory/Factory.h"
#include "../Scene/Scene.h"
#include "../GameObject/GameObject.h"
#include "../Space/Space.h"
#include "../Animation/SM_Animation.h"
#include "../ThreadPool/ThreadPool.h"
#include "ResourceManager.h"

ResourceManager* ResourceManager::instance = nullptr;
static spine::CustomTextureLoader spine_loader;
MultithreadedLoader* MultithreadedLoader::instance = nullptr;
bool ResourceManager::Initialize(void)
{
	ImportersMap[TypeTexture] = new TextureImporter();
	ImportersMap[TypeSound] = new SoundImporter();
	ImportersMap[TypeArchetype] = new ArchetypeImporter();
	ImportersMap[TypeLevel] = new LevelImporter();
	ImportersMap[TypeSpine] = new SpineImporter();
	ImportersMap[NotFound] = new NotFoundImporter();

	ThreadPool::Get()->estimated_tasks = FirstParser();
	auto start = std::chrono::high_resolution_clock::now();
	LoadResourcesFromMeta();

	if (SettingsSys->fast_load)
		ThreadPool::Get()->WaitAll();

	if (SettingsSys->fast_load)
	{
		SetAllTextures();
		SetAllSpineTextures();
	}

	auto end = std::chrono::high_resolution_clock::now();
	auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
	std::cout << std::endl << diff.count() * 1e-9 << std::endl;


	std::cout << std::endl << "Fast load was " << SettingsSys->fast_load << std::endl;
	first_load = false;
	return true;
}

void ResourceManager::Update(void)
{
	if (SettingsSys->EnableHotReload)
	{
		
		ThreadP->enqueue([&]
			{
				FileStatus data_to_check;
				for (auto it = Resourcesmap.begin(); it != Resourcesmap.end(); it++)
				{
					for (auto et = it->second.begin(); et != it->second.end(); et++)
					{
						stat(et->first.data(), &data_to_check);

						if (data_to_check.st_mtime != et->second->modifytime.st_mtime)
						{

							//ReloadResource(et->first);
							MultithreadedLoader::Get()->mLock.lock();
							objects_to_reload.push_back(et->first);
							et->second->modifytime.st_mtime = data_to_check.st_mtime;
							MultithreadedLoader::Get()->mLock.unlock();
						}
					}

				}

				FileStatus skeleton_data_to_check;
				FileStatus atlas_data_to_check;
				for (auto it = spine_names.begin(); it != spine_names.end(); it++)
				{
					stat(it->second.skeleton_name_.data(), &skeleton_data_to_check);
					stat(it->second.atlas_name_.data(), &atlas_data_to_check);

					if (skeleton_data_to_check.st_mtime != it->second.skeleton_stats.st_mtime)
					{
						MultithreadedLoader::Get()->mLock.lock();
						it->second.skeleton_changed = true;
						it->second.skeleton_stats = skeleton_data_to_check;
						MultithreadedLoader::Get()->mLock.unlock();
					}

					if (atlas_data_to_check.st_mtime != it->second.atlas_stats.st_mtime)
					{
						MultithreadedLoader::Get()->mLock.lock();
						it->second.atlas_changed = true;
						it->second.atlas_stats = atlas_data_to_check;
						MultithreadedLoader::Get()->mLock.unlock();

					}

					if (it->second.skeleton_changed == true && it->second.atlas_changed == true)
					{
						//ReloadResource(it->first->RelPath);
						MultithreadedLoader::Get()->mLock.lock();
						objects_to_reload.push_back(it->first->RelPath);
						it->second.skeleton_changed = false;
						it->second.atlas_changed = false;
						MultithreadedLoader::Get()->mLock.unlock();

					}

				}
			}
		);

		for(auto object : objects_to_reload)
			ReloadResource(object);
		
		if (objects_to_reload.size() != 0)
			objects_to_reload.clear();
	}
	
}

void ResourceManager::Shutdown(void)
{
	SaveResourcesToMeta();
	//ParseResourcesMeta();
}

IResource* ResourceManager::GetResource(const std::string& filename)
{
	ExtensionTypes type_of_file = GetExtensionOfFile(filename);
	auto resource_found = Resourcesmap[type_of_file].find(filename);
	if (resource_found != Resourcesmap[type_of_file].end())
		return resource_found->second;
	return nullptr;
}

IResource* ResourceManager::Load(const std::string& name_extension)
{
	ExtensionTypes current_type = GetExtensionOfFile(name_extension);
	IResourceImporter* loader = GetImporterFromExtension(current_type);

	// Check if the resource is already loaded
	// Load
	if (Resourcesmap[current_type].find(name_extension) == Resourcesmap[current_type].end())
	{
		IResource* added_resource = loader->Load(name_extension);
		if (added_resource != nullptr)
			AddResource(added_resource, current_type, name_extension);
		else
			added_resource = loader->LoadDummy();

		return added_resource;
	}
	// Reload
	else
	{
		IResource* added_resource = loader->ReLoad(name_extension);

		return added_resource;
	}

}

IResource* ResourceManager::ReloadResource(std::string filename)
{
	ExtensionTypes extension_to_check = GetExtensionOfFile(filename);
	if (extension_to_check != TypeLevel && extension_to_check != TypeArchetype)
	{
		std::string check = filename;

		auto importer_to_load = GetImporterFromExtension(extension_to_check);


		importer_to_load->Unload(check);



		return importer_to_load->ReLoad(check);
	}
	return GetResource(filename);
}

void ResourceManager::SetAllTextures()
{
	//What this function basically does is to set all the textures in the main thread
	for (TexturesInfos info : infos)
	{
		Texture* newtex = new Texture;


		for (auto it = Resourcesmap.begin(); it != Resourcesmap.end(); it++)
		{
			auto et = it->second.find(info.name_file);
			if (et != it->second.end())
			{
				Resource<Texture>* textres = reinterpret_cast<Resource<Texture>*>(et->second);
				if (textres)
				{
					if (info.GLHandle_) // already created
						return;

					// if we have a texture associated already -> kill it
					if (info.GLHandle_)
						glDeleteTextures(1, (GLuint*)&info.GLHandle_);

					// Create texture and bind it
					glGenTextures(1, (GLuint*)&info.GLHandle_);
					glBindTexture(GL_TEXTURE_2D, info.GLHandle_);
					//check_gl_error();

					// set texture look-up parameters
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					//check_gl_error();
					if (info.GLHandle_ && (info.width_ * info.height_) != 0)
					{
						/*if(info.GLHandle_)
							glBindTexture(GL_TEXTURE_2D, info.GLHandle_)*/
						glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, info.width_, info.height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, reinterpret_cast<void*>(info.pixels_));

						newtex->GetHeight() = info.height_;
						newtex->GetWidth() = info.width_;
						newtex->GetGLHandle() = info.GLHandle_;
						newtex->SetName(info.name_file);
						newtex->mPixels = info.pixels_;
					}

					textres->SetResource(newtex);

				}
				break;
			}

		}
	}


}

void ResourceManager::SetAllSpineTextures()
{
	for (auto it = spineinfos.begin(); it != spineinfos.end(); it++)
	{
		// here you should create a texture resource (assuming you have a resource manager). 
		//Texture* newTex = RndMngr->LoadTexture("name", path.buffer());
	
		Resource<Texture>* newTex =RsrcMan->GetResourceType<Texture>(it->second.buffer());
		// sanity check and error message
		if (!newTex) {
			printf("ERROR! Couldn't load Spine atlas page %s \n", it->second.buffer());
			return;
		}

		// set texture filter based on atlas page setting
		{

			//newTex->get()->Bind();
			//newTex->Bind();
			switch (it->first->magFilter) {
			case spine::TextureFilter::TextureFilter_Linear:

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			case spine::TextureFilter::TextureFilter_MipMapNearestNearest:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				break;
			}
			switch (it->first->minFilter) {
			case spine::TextureFilter::TextureFilter_Linear:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				break;
			case spine::TextureFilter::TextureFilter_MipMapNearestNearest:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				break;
			}
			//newTex->get()->Unbind();
			//newTex->Unbind();
		}

		// pass the texture pointer to the page. Spine doesn't actually do anything with it, 
		// it will be passed back to us when drawing the skeleton. 
		it->first->setRendererObject(newTex->get());

		// set the texture width and height
		it->first->width = newTex->get()->GetWidth();
		//page.width = newTex->GetWidth();
		it->first->height = newTex->get()->GetHeight();
		//page.height = newTex->GetHeight();
		
	}
}

void ResourceManager::UnloadResource(const std::string& name_extension)
{
	ExtensionTypes current_type = GetExtensionOfFile(name_extension);

	auto it = Resourcesmap[current_type].find(name_extension);

	if (it != Resourcesmap[current_type].end())
	{
		auto file_it = std::find(name_files[current_type].begin(), name_files[current_type].end(), name_extension);
		if (file_it != name_files[current_type].end())
			name_files[current_type].erase(file_it);

		Resource<Texture>* TextResource = nullptr;
		Resource<Sound>* SoundResource = nullptr;
		Resource<Level>* LevelResource = nullptr;
		Resource<SpineData>* SpineResource = nullptr;

		switch (current_type)
		{
		case TypeTexture:
			TextResource = dynamic_cast<Resource<Texture>*>(it->second);
			delete TextResource;
			Resourcesmap[current_type].erase(it);
			break;

		case TypeSpine:
			SpineResource = dynamic_cast<Resource<SpineData>*>(it->second);
			delete SpineResource;
			Resourcesmap[current_type].erase(it);
			break;

		case TypeSound:
			SoundResource = dynamic_cast<Resource<Sound>*>(it->second);
			delete SoundResource;
			Resourcesmap[current_type].erase(it);
			break;

		case TypeLevel:
			LevelResource = dynamic_cast<Resource<Level>*>(it->second);
			delete LevelResource;
			Resourcesmap[current_type].erase(it);
			break;

		case NotFound:
			delete it->second;
			Resourcesmap[current_type].erase(it);
			break;
		}
	}
}

void ResourceManager::UnloadAllResources()
{
	for (auto i : Resourcesmap)
	{
		for (auto j : i.second)
		{
			UnloadResource(j.first);
		}
	}
}

void ResourceManager::AddResource(IResource* to_add, const ExtensionTypes& file_extension, const std::string& filename)
{

	Resourcesmap[file_extension].insert(std::pair<std::string, IResource*>(filename, to_add));
	if (std::find(name_files[file_extension].begin(), name_files[file_extension].end(), filename) == name_files[file_extension].end())
	{
		name_files[file_extension].push_back(filename);
	}

}

void ResourceManager::LoadResourcesFromMeta()
{
	int i = 0;
	FILE* fp;
	FILE* to_check;
	fopen_s(&fp, "./data/EditorResources.meta", "rt");
	if (fp)
	{
		std::string toload;
		while (!feof(fp))
		{
			char current_char = fgetc(fp);
			if (current_char == EOF)
				break;
			if (current_char == '\0')
				continue;
			if (current_char == '\n')
			{
				bool check = true;
				fopen_s(&to_check, toload.c_str(), "rt");
				if (to_check)
				{
					fclose(to_check);
				}
				else
				{
					check = false;
					ThreadP->estimated_tasks--;
				}

				if(check)
					Load(toload);
				toload.clear();
				i++;
				continue;
			}
			toload += current_char;
		}
		fclose(fp);
	}
	else
	{
		printf("Could not open the meta file\n");
		Load("./data/Levels/Default.lvl");
		Load("./data/Textures/bakugo.png");
		Load("./data/Sounds/short_sound.mp3");
	}
}

void ResourceManager::SaveResourcesToMeta(std::string to_save)
{
	FILE* fp;
	fopen_s(&fp, to_save.c_str(), "wt");
	if (fp)
	{
		for (auto name_file_ext : name_files)
		{
			for (auto name_file : name_file_ext.second)
			{
				fputs(name_file.c_str(), fp);
				fputc('\n', fp);
			}
		}
		fclose(fp);
	}
	else
		printf("Could not open the meta file\n");
}

void ResourceManager::ParseResourcesMeta()
{
	std::fstream fs("./data/EditorResources.meta", std::fstream::in | std::fstream::out);
	if (fs.is_open()) 
	{
		while (!fs.eof()) 
		{
			char _Buffer = fs.get();
			if (!fs.eof())
			{
				if (_Buffer == '\\')
				{
					fs.seekp((fs.tellp() - static_cast<std::streampos>(1)));
					fs.put('/');
					fs.seekp(fs.tellp());
				}
			}
		}
		fs.close();
	}
	else {
		std::cout << "Faild to open" << '\n';
	}
}

IResourceImporter* ResourceManager::GetImporterFromExtension(const ExtensionTypes& file_extension)
{
	switch (file_extension)
	{
	case TypeTexture:
		return ImportersMap[TypeTexture];

	case TypeSound:
		return ImportersMap[TypeSound];


	case TypeArchetype:
		return ImportersMap[TypeArchetype];

	case TypeLevel:
		return ImportersMap[TypeLevel];

	case TypeSpine:
		return ImportersMap[TypeSpine];
	};

	return ImportersMap[NotFound];

}



ExtensionTypes ResourceManager::GetExtensionOfFile(const std::string& filename)
{
	std::string file_extension;
	size_t prevfound;
	size_t found = filename.find('.');

	while (found != std::string::npos)
	{
		prevfound = found;
		found = filename.find('.', found + 1);
	}
	file_extension = filename.substr(prevfound + 1);
	if (file_extension == "png" || file_extension == "jpg")
		return TypeTexture;
	if (file_extension == "wav" || file_extension == "mp3")
		return TypeSound;
	if (file_extension == "archetype")
		return TypeArchetype;
	if (file_extension == "lvl")
		return TypeLevel;
	if (file_extension == "anim")
		return TypeSpine;
	return NotFound;
}

void ResourceManager::RegisterArchetypeFromLoad(std::string object_name)
{
	//check for duplicates
	auto archetype = std::find(allArchetypes.begin(), allArchetypes.end(), object_name);

	if (archetype == allArchetypes.end())
	{
		allArchetypes.push_back(object_name);
	}
}

void ResourceManager::RegisterArchetype(GameObject& to_register)
{
	std::string register_name = "./data/Archetypes/" + to_register.GetName() + ".archetype";
	auto archetype = std::find(allArchetypes.begin(), allArchetypes.end(), register_name);

	// Archetype did not exist
	if (archetype == allArchetypes.end())
	{
		to_register.SetArchetypeName(to_register.GetName());
		to_register.CreateArchetype(register_name);
		RegisterArchetypeFromLoad(register_name);
		Load(register_name);
	}
	else
	{
		to_register.SetArchetypeName(to_register.GetName());
		to_register.CreateArchetype(register_name);
		auto archetype = Load(register_name);
		EventSys->trigger_event(ArchetypeUpdated(reinterpret_cast<Archetype*>(archetype->mRawData)->GetObject()));
	}
}

void ResourceManager::SaveLevel(std::string level_)
{
}

std::vector<std::string>& ResourceManager::GetAllArchetypes()
{
	return allArchetypes;
}

void ResourceManager::FindDumpResourcesInLevel()
{
	//EventSys->trigger_event(DumpResource());
	//std::string name_meta = SceneSys->GetLevelName();
	//int pos = name_meta.find_last_of(".lvl");
	//name_meta = name_meta.substr(0, pos - 3);
	//name_meta = name_meta + ".meta";
	//FILE* fp;
	//fopen_s(&fp, name_meta.c_str(), "wt");
	//if (fp)
	//{
	//	for (auto current : current_level_resources)
	//	{
	//
	//		fputs(current.c_str(), fp);
	//		fputc('\n', fp);
	//
	//	}
	//
	//	fclose(fp);
	//}
	//else
	//{
	//	printf("Could not open the meta file\n");
	//
	//}
	//current_level_resources.clear();
}

int ResourceManager::FirstParser()
{
	//Check how many textures and sounds do we have to load
	int i = 0;
	FILE* fp;
	fopen_s(&fp, "./data/EditorResources.meta", "rt");
	if (fp)
	{
		std::string toload;
		while (!feof(fp))
		{
			char current_char = fgetc(fp);
			if (current_char == EOF)
				break;
			if (current_char == '\0')
				continue;
			if (current_char == '\n')
			{
				ExtensionTypes current_type = GetExtensionOfFile(toload);
				//if(current_type == TypeTexture || current_type == TypeSound || current_type == TypeArchetype)
				//if(current_type != TypeSpine)
				i++;
				toload.clear();
				continue;
			}
			toload += current_char;
		}
		fclose(fp);
	}

	return i;
}


IResource* NotFoundImporter::Load(std::string filename)
{
	return nullptr;
}

IResource* NotFoundImporter::ReLoad(std::string filename)
{
	return nullptr;
}

void NotFoundImporter::Unload(const std::string& name_extension)
{
}

IResource* SoundImporter::Load(std::string filename)
{
	if (SettingsSys->fast_load != true || RsrcMan->first_load==false)
	{
		AudioSys->AddSoundFile(filename);
		Resource<Sound>* soundResource = new Resource<Sound>(filename);

		Sound* newsound = new Sound();

		soundResource->SetResource(newsound);

		if (NULL == AudioSys->GetFMOD())
			return NULL;

		AudioSys->GetFMOD()->createSound(filename.c_str(), FMOD_LOOP_NORMAL | FMOD_2D, 0, &soundResource->get()->pSound);

		// save the name of the 
		soundResource->get()->filename = filename;

		// error check
		if (soundResource->get()->pSound == NULL)
		{
			// make sure to delete the sound pointer
			delete soundResource->get();
			return nullptr;

		}

		auto temp = AudioSys->GetAllLoadedSounds();
		temp[filename] = soundResource->get();

		AudioSys->soundCount++;

		return soundResource;
	}
	else
	{
		Resource<Sound>* soundResource = new Resource<Sound>(filename);

		Sound* newsound = new Sound();

		AudioSys->AddSoundFile(filename);
		soundResource->SetResource(newsound);

		if (NULL == AudioSys->GetFMOD())
			return NULL;

		ThreadP->enqueue([filename, soundResource]
			{
				MultithreadedLoader::Get()->AddTask();
				AudioSys->GetFMOD()->createSound(filename.c_str(), FMOD_LOOP_NORMAL | FMOD_2D, 0, &soundResource->get()->pSound);
				// save the name of the 
				soundResource->get()->filename = filename;

				// error check
				if (soundResource->get()->pSound == NULL)
				{
					// make sure to delete the sound pointer
					delete soundResource->get();
					return nullptr;

				}

				MultithreadedLoader::Get()->mLock.lock();
				auto temp = AudioSys->GetAllLoadedSounds();
				temp[filename] = soundResource->get();
				MultithreadedLoader::Get()->mLock.unlock();

				MultithreadedLoader::Get()->mLock.lock();
				std::cout << std::endl << filename << " loaded by " << std::this_thread::get_id() << std::endl;
				MultithreadedLoader::Get()->mLock.unlock();

				MultithreadedLoader::Get()->mLock.lock();
				AudioSys->soundCount++;
				MultithreadedLoader::Get()->mLock.unlock();

				MultithreadedLoader::Get()->FinishTask();

			}
		);

		return soundResource;

	}
}

IResource* SoundImporter::ReLoad(std::string filename)
{
	AudioSys->AddSoundFile(filename);
	Resource<Sound>* soundResource = RsrcMan->GetResourceType<Sound>(filename);;

	Sound* newsound = new Sound();

	soundResource->SetResource(newsound);

	if (NULL == AudioSys->GetFMOD())
		return NULL;

	AudioSys->GetFMOD()->createSound(filename.c_str(), FMOD_LOOP_NORMAL | FMOD_2D, 0, &soundResource->get()->pSound);

	// save the name of the 
	soundResource->get()->filename = filename;

	// error check
	if (soundResource->get()->pSound == NULL)
	{
		// make sure to delete the sound pointer
		delete soundResource->get();
		return nullptr;

	}

	auto temp = AudioSys->GetAllLoadedSounds();
	temp[filename] = soundResource->get();

	AudioSys->soundCount++;

	return soundResource;
}

IResource* SoundImporter::LoadDummy()
{
	return Load(dummytext);
}

void SoundImporter::Unload(const std::string& name_extension)
{
	Resource<Sound>* soundResource = reinterpret_cast<Resource<Sound>*>(RsrcMan->GetResource(name_extension));


	if (NULL == AudioSys->GetFMOD())
		return;
	if (!soundResource->get()->pSound)
		return;

	if (soundResource->get()->pSound)
	{
		soundResource->get()->pSound->release();
		soundResource->get()->pSound = 0;
	}

	// remove from sound resources
	auto temp = AudioSys->GetAllLoadedSounds();
	temp.erase(soundResource->get()->filename);

	// Stats update
	AudioSys->soundCount--;
	delete soundResource->get();
	soundResource->mRawData = nullptr;
}

IResource* TextureImporter::Load(std::string filename)
{
	if (SettingsSys->fast_load != true || RsrcMan->first_load == false)
	{
		Resource<Texture>* texture_resource;

		texture_resource = new Resource<Texture>(filename);

		unsigned Width = 0;
		unsigned Height = 0;
		char* mPixels = NULL;
		GLuint GLHandle = 0;
		Texture* newtex = new Texture(filename.c_str());
		texture_resource->SetResource(newtex);
		bool res = LoadImageFromFile(filename.c_str(), texture_resource->get()->mPixels, Width, Height);
		if (res)// success
		{


			texture_resource->get()->GetHeight() = Height;
			texture_resource->get()->GetWidth() = Width;
			texture_resource->get()->GetGLHandle() = GLHandle;

			if (texture_resource->get()->GetGLHandle()) // already created
				return texture_resource;

			// if we have a texture associated already -> kill it
			if (texture_resource->get()->GetGLHandle())
				glDeleteTextures(1, (GLuint*)&texture_resource->get()->GetGLHandle());

			// Create texture and bind it
			glGenTextures(1, (GLuint*)&texture_resource->get()->GetGLHandle());
			glBindTexture(GL_TEXTURE_2D, texture_resource->get()->GetGLHandle());
			//check_gl_error();

			// set texture look-up parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			//check_gl_error();

			if (texture_resource->get()->GetGLHandle() && (texture_resource->get()->GetWidth() * texture_resource->get()->GetHeight()) != 0)
			{
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_resource->get()->GetWidth(), texture_resource->get()->GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, reinterpret_cast<void*>(texture_resource->get()->GetPixels()));
			}
		}
		else
		{
			std::cout << "COULD NOT LOAD TEXTURE" << std::string(filename) << std::endl;
			return nullptr;

		}


		return texture_resource;
	}
	else
	{
		Resource<Texture>* texture_resource = new Resource<Texture>(filename);
		Texture* newtex = new Texture(filename.c_str());
		texture_resource->SetResource(newtex);
		ThreadP->enqueue([filename, texture_resource]
			{
				//Load the image data and push it to the info texture

				TexturesInfos info;
				char* pixels = nullptr;

				info.height_ = 0;
				info.pixels_ = nullptr;
				info.GLHandle_ = 0;
				info.name_file = filename;
				info.resource = texture_resource;
				int width = 0;
				int height = 0;
				stbi_set_flip_vertically_on_load(1);


				MultithreadedLoader::Get()->AddTask();



				pixels = (char*)stbi_load(filename.c_str(), &width, &height, 0, 4);

				if (pixels)// success
				{
					info.width_ = width;
					info.height_ = height;
					info.pixels_ = pixels;

				}
				else
				{
					std::cout << "COULD NOT LOAD TEXTURE" << std::string(filename) << std::endl;


				}

				MultithreadedLoader::Get()->mLock.lock();
				RsrcMan->infos.push_back(info);
				MultithreadedLoader::Get()->mLock.unlock();

				MultithreadedLoader::Get()->mLock.lock();
				std::cout << std::endl << filename << " loaded by " << std::this_thread::get_id() << std::endl;
				MultithreadedLoader::Get()->mLock.unlock();


				MultithreadedLoader::Get()->FinishTask();
			}
		);

		return texture_resource;

	}
}

IResource* TextureImporter::ReLoad(std::string filename)
{
	Resource<Texture>* _TexResource = RsrcMan->GetResourceType<Texture>(filename);

	unsigned _Width = 0;
	unsigned _Height = 0;

	Texture* _NewTexture = new Texture();
	_TexResource->SetResource(_NewTexture);

	bool res = LoadImageFromFile(filename.c_str(), _NewTexture->mPixels, _Width, _Height);
	if (res)// success
	{
		_NewTexture->SetHeight(_Height);
		_NewTexture->SetWidth(_Width);

		_NewTexture->CreateOpenGLTexture();
		_NewTexture->UploadToGPU();
	}
	else
	{
		std::cout << "COULD NOT LOAD TEXTURE" << std::string(filename) << std::endl;
		return nullptr;
	}

	return _TexResource;
}

IResource* TextureImporter::LoadDummy()
{
	return RsrcMan->Load(dummytext);
}

void TextureImporter::Unload(const std::string& name_extension)
{
	Resource<Texture>* texture_resource = RsrcMan->GetResourceType<Texture>(name_extension);

	texture_resource->get()->DeleteFromGPU();
	texture_resource->get()->FreeData();

	delete texture_resource->get();
	texture_resource->mRawData = nullptr;
}



// External Image loading // works with PNG, JPG and TGA
bool TextureImporter::LoadImageFromFile(const char* filename, char*& outPixels, GLuint& outWidth, GLuint& outHeight)
{
	// Load the data
	int x = 0, y = 0, n = 0, reqComp = 4;	// out parameters
	stbi_set_flip_vertically_on_load(1);
	outPixels = (char*)stbi_load(filename, &x, &y, &n, reqComp);	// 4 - forces output data to be of the form RGBA
	if (outPixels == NULL)
	{
		// TODO: provide error message
		return false;
	}
	// store width and height
	outWidth = (GLuint)x;
	outHeight = (GLuint)y;
	//
	return true;
}

void TextureImporter::CreateOpenGLTexture(GLuint to_check)
{
	if (to_check) // already created
		return;

	// if we have a texture associated already -> kill it
	if (to_check)
		glDeleteTextures(1, &to_check);

	// Create texture and bind it
	glGenTextures(1, &to_check);
	glBindTexture(GL_TEXTURE_2D, to_check);
	//check_gl_error();

	// set texture look-up parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//check_gl_error();
}

IResource* ArchetypeImporter::Load(std::string filename)
{
	if (SettingsSys->fast_load != true || RsrcMan->first_load == false)
	{
		Resource<Archetype>* archetype_resource;

		archetype_resource = new Resource<Archetype>(filename);

		Archetype* new_arch = FactorySys->Create<Archetype>();

		new_arch->Load(filename);

		archetype_resource->SetResource(new_arch);

		RsrcMan->RegisterArchetypeFromLoad(filename);

		return archetype_resource;
	}
	else
	{
		Resource<Archetype>* archetype_resource;

		archetype_resource = new Resource<Archetype>(filename);
		Archetype* new_arch = FactorySys->Create<Archetype>();
		archetype_resource->SetResource(new_arch);
		ThreadP->enqueue([filename, archetype_resource, new_arch]
			{

				MultithreadedLoader::Get()->AddTask();

				if (new_arch->mObject)
					delete new_arch->mObject;
				MultithreadedLoader::Get()->mLock.lock();
				new_arch->mObject = FactorySys->Create<GameObject>();
				MultithreadedLoader::Get()->mLock.unlock();
				using nlohmann::json;

				std::ifstream fp1(filename);
				json j;

				if (fp1.is_open() && fp1.good())
				{
					fp1 >> j;
					fp1.close();
				}
				MultithreadedLoader::Get()->mLock.lock();
				new_arch->mObject->FromJson(j);
				MultithreadedLoader::Get()->mLock.unlock();
				//EventSys->unsubscribe(new_arch->mObject, ArchetypeUpdated());


				MultithreadedLoader::Get()->mLock.lock();
				std::cout << std::endl << filename << " loaded by " << std::this_thread::get_id() << std::endl;
				MultithreadedLoader::Get()->mLock.unlock();


				MultithreadedLoader::Get()->FinishTask();
			}
		);

		return archetype_resource;

	}
}

IResource* ArchetypeImporter::ReLoad(std::string filename)
{
	Archetype* archetype = reinterpret_cast<Archetype*>(RsrcMan->GetResource(filename)->mRawData);
	archetype->ReLoad(filename);
	return RsrcMan->GetResource(filename);
}

IResource* ArchetypeImporter::LoadDummy()
{
	return nullptr;
}

void ArchetypeImporter::Unload(const std::string& name_extension)
{
	/*Resource<Archetype>* arch_resource = Singletons::Get<ResourceManager>()->GetResourceType<Archetype>(name_extension);*/
	Resource<Archetype>* arch_resource = RsrcMan->GetResourceType<Archetype>(name_extension);

	if (arch_resource->get()->GetObject())
		delete arch_resource->get()->GetObject();
	delete arch_resource->get();

}

IResource* LevelImporter::Load(std::string filename)
{
	if (SettingsSys->fast_load != true || RsrcMan->first_load == false)
	{
		Resource<Level>* level_resource;

		level_resource = new Resource<Level>(filename);

		Level* newlevel = new Level(filename);

		using nlohmann::json;
		std::ifstream fp1(filename, std::ios_base::out | std::ios_base::in);
		json j;
		if (fp1.is_open() && fp1.good())
		{
			if (fp1.peek() != std::ifstream::traits_type::eof())
				fp1 >> j;
			fp1.close();
			newlevel->level_name = filename;
			level_resource->SetResource(newlevel);
		}
		else
		{
			return LoadDummy();
			std::cout << "No level with name " << filename << "exists.\n";
		}
		return level_resource;
	}
	else
	{
		Resource<Level>* level_resource;

		level_resource = new Resource<Level>(filename);

		Level* newlevel = new Level(filename);
		level_resource->SetResource(newlevel);
		ThreadP->enqueue([filename, level_resource, newlevel]
			{
				MultithreadedLoader::Get()->AddTask();
				using nlohmann::json;
				std::ifstream fp1(filename, std::ios_base::out | std::ios_base::in);
				json j;
				if (fp1.is_open() && fp1.good())
				{
					if (fp1.peek() != std::ifstream::traits_type::eof())
						fp1 >> j;
					fp1.close();
					newlevel->level_name = filename;


				}
				else
				{
					return nullptr;
					std::cout << "No level with name " << filename << "exists.\n";
				}

				MultithreadedLoader::Get()->mLock.lock();
				std::cout << std::endl << filename << " loaded by " << std::this_thread::get_id() << std::endl;
				MultithreadedLoader::Get()->mLock.unlock();


				MultithreadedLoader::Get()->FinishTask();
			}
		);

		return level_resource;


	}
}

IResource* LevelImporter::ReLoad(std::string filename)
{
	return RsrcMan->GetResource(filename);
}

IResource* LevelImporter::LoadDummy()
{
	Resource<Level>* level_resource;

	level_resource = new Resource<Level>("./data/Levels/Default.lvl");

	Level* newlevel = new Level("./data/Levels/Default.lvl");
	level_resource->SetResource(newlevel);

	using nlohmann::json;
	std::ifstream fp1("./data/Levels/Default.lvl");
	json j;
	if (fp1.is_open() && fp1.good())
	{
		fp1 >> j;
		fp1.close();
		newlevel->level_name = "./data/Levels/Default.lvl";
		level_resource->SetResource(newlevel);
	}

	FromJson(j);

	return level_resource;
}

void LevelImporter::Unload(const std::string& name_extension)
{
	Resource<Level>* levelResource = reinterpret_cast<Resource<Level>*>(RsrcMan->GetResource(name_extension));
	using nlohmann::json;
	json h;
	std::ofstream fp_out(name_extension, std::ios_base::out | std::ios_base::in);
	if (fp_out.is_open() && fp_out.good())
	{
		fp_out << std::setw(4) << h;
		fp_out.close();
	}
	delete levelResource->get();
}


IResource* SpineImporter::Load(std::string filename)
{
	if (SettingsSys->fast_load != true || RsrcMan->first_load == false)
	{
		Resource<SpineData>* animation_resource;

		animation_resource = new Resource<SpineData>(filename);
		animation_resource->SetResource(new SpineData);


		std::string skeleton_name;
		std::string atlas_name;
		std::ifstream fp_in(filename);
		if (fp_in.is_open() && fp_in.good())
		{
			std::getline(fp_in, skeleton_name);
			std::getline(fp_in, atlas_name);
		}



		SpineFile newspine;

		newspine.skeleton_name_ = skeleton_name;
		newspine.atlas_name_ = atlas_name;

		stat(atlas_name.data(), &newspine.atlas_stats);
		stat(skeleton_name.data(), &newspine.skeleton_stats);


		animation_resource->get()->jsonScale = 0.01f; // should be given as a parameter or read from a meta file. 
		animation_resource->get()->animStateData = nullptr;
		animation_resource->get()->atlas = new spine::Atlas(atlas_name.c_str(), &spine_loader);
		// load json again
		// json variable
		spine::SkeletonJson json(animation_resource->get()->atlas);

		// set the scale at which the skeleton should be loaded
		json.setScale(animation_resource->get()->jsonScale);

		animation_resource->get()->skelData = json.readSkeletonDataFile(skeleton_name.c_str());

		// loading failed
		if (!animation_resource->get()->skelData) {
			printf("ERROR! Loading skel data Json: %s\n", json.getError().buffer());
			animation_resource->get()->FreeData();
			return nullptr;
		}
		// create a new animation state data
		animation_resource->get()->animStateData = new spine::AnimationStateData(animation_resource->get()->skelData);

		RsrcMan->spine_names[animation_resource] = newspine;
		return animation_resource;
	}
	else
	{
	
		Resource<SpineData>* animation_resource;
		animation_resource = new Resource<SpineData>(filename);
	
		SpineData* newspinedata = new SpineData;
		
		animation_resource->SetResource(new SpineData);
	
		ThreadP->enqueue([filename, animation_resource]
			{
				std::string skeleton_name;
				std::string atlas_name;
				std::ifstream fp_in(filename);
				if (fp_in.is_open() && fp_in.good())
				{
					std::getline(fp_in, skeleton_name);
					std::getline(fp_in, atlas_name);
				}
	
				SpineFile newspine;
	
				newspine.skeleton_name_ = skeleton_name;
				newspine.atlas_name_ = atlas_name;
	
				stat(atlas_name.data(), &newspine.atlas_stats);
				stat(skeleton_name.data(), &newspine.skeleton_stats);
	
	
				animation_resource->get()->jsonScale = 0.01f; // should be given as a parameter or read from a meta file. 
				animation_resource->get()->animStateData = nullptr;
	
				MultithreadedLoader::Get()->AddTask();
				animation_resource->get()->atlas = new spine::Atlas(atlas_name.c_str(), &spine_loader);
				// load json again
				// json variable
				spine::SkeletonJson json(animation_resource->get()->atlas);
	
	
				
				// set the scale at which the skeleton should be loaded
				json.setScale(animation_resource->get()->jsonScale);
				
	
				
				animation_resource->get()->skelData = json.readSkeletonDataFile(skeleton_name.c_str());
				
	
				// loading failed
				if (!animation_resource->get()->skelData) {
					printf("ERROR! Loading skel data Json: %s\n", json.getError().buffer());
					animation_resource->get()->FreeData();
					return nullptr;
				}
	
				
				// create a new animation state data
				animation_resource->get()->animStateData = new spine::AnimationStateData(animation_resource->get()->skelData);
				
				MultithreadedLoader::Get()->mLock.lock();
				int index = atlas_name.find("atlas");
				std::string pngfile = atlas_name.replace(index, 5, "png");
	
				RsrcMan->spineinfos[animation_resource->get()->atlas->getPages()[0]] = pngfile.c_str();
				MultithreadedLoader::Get()->mLock.unlock();


				MultithreadedLoader::Get()->mLock.lock();
				RsrcMan->spine_names[animation_resource] = newspine;
				MultithreadedLoader::Get()->mLock.unlock();
	
				MultithreadedLoader::Get()->FinishTask();
	
			}
		);
		return animation_resource;
	}
}

IResource* SpineImporter::ReLoad(std::string filename)
{
	Resource<SpineData>* animation_resource;

	animation_resource = RsrcMan->GetResourceType<SpineData>(filename);
	animation_resource->SetResource(new SpineData);

	std::string skeleton_name;
	std::string atlas_name;
	std::ifstream fp_in(filename);
	if (fp_in.is_open() && fp_in.good())
	{
		std::getline(fp_in, skeleton_name);
		std::getline(fp_in, atlas_name);
	}


	auto it = RsrcMan->spine_names.find(animation_resource);
	if (it != RsrcMan->spine_names.end())
	{
		if (it->second.atlas_name_ != atlas_name)
		{
			FileStatus atlas_data_to_check;
			stat(atlas_name.data(), &atlas_data_to_check);
			it->second.atlas_name_ = atlas_name;
			it->second.atlas_stats = atlas_data_to_check;

		}

		if (it->second.skeleton_name_ != skeleton_name)
		{
			FileStatus skeleton_data_to_check;
			stat(skeleton_name.data(), &skeleton_data_to_check);
			it->second.skeleton_name_ = skeleton_name;
			it->second.skeleton_stats = skeleton_data_to_check;
		}

	}
	


	animation_resource->get()->jsonScale = 0.23f; // should be given as a parameter or read from a meta file. 
	animation_resource->get()->animStateData = nullptr;
	animation_resource->get()->atlas = new spine::Atlas(atlas_name.c_str(), &spine_loader);
	// load json again
	// json variable
	spine::SkeletonJson json(animation_resource->get()->atlas);

	// set the scale at which the skeleton should be loaded
	json.setScale(animation_resource->get()->jsonScale);

	animation_resource->get()->skelData = json.readSkeletonDataFile(skeleton_name.c_str());

	// loading failed
	if (!animation_resource->get()->skelData) {
		printf("ERROR! Loading skel data Json: %s\n", json.getError().buffer());
		animation_resource->get()->FreeData();
		return nullptr;
	}
	// create a new animation state data
	animation_resource->get()->animStateData = new spine::AnimationStateData(animation_resource->get()->skelData);

	
	return animation_resource;
}

IResource* SpineImporter::LoadDummy()
{
	return nullptr;
}

void SpineImporter::Unload(const std::string& name_extension)
{
	Resource<SpineData>* spine_resource = RsrcMan->GetResourceType<SpineData>(name_extension);
	spine_resource->get()->FreeData();
	delete spine_resource->get();
	spine_resource->mRawData = nullptr;
}

void MultithreadedLoader::AddTask()
{
	mLock.lock();

	mToLoad++;

	mLock.unlock();
}


void MultithreadedLoader::FinishTask()
{
	mLock.lock();
	ThreadPool::Get()->counter_tasks++;
	mToLoad--;

	mLock.unlock();
}


void MultithreadedLoader::PushInfo(TexturesInfos& _info)
{
	mLock.lock();
	loaded_textures.push_back(_info);
	mLock.unlock();
}
