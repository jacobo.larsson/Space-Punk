#pragma once
#include <map>
#include <mutex>
#include <unordered_map>
#include <spine/spine.h>
#include "../Extern/json/json.hpp"
#include "../Extern/STB/stb_image.h"
#include "../Extern/STB/stb_image_write.h"

#include <GL/glew.h>
#include <sys/stat.h>
#include "../RTTI/SM_RTTI.h"
//#include "../Animation/SM_Animation.h"
#include "../Singleton/Singleton.h"
#include "../Graphics/Texture/Texture.h"
#include "../Audio/AudioManager.h"
#include "../Audio/Sound.h"
#include "../Archetype/Archetype.h"
#include "../Level/Level.h"

//class Texture;
//struct Sound;

typedef struct stat FileStatus;
class IResourceImporter;
class TexturesInfos;
class SpineInfos;

class SpineFile
{
public:
	std::string skeleton_name_;
	std::string atlas_name_;
	bool atlas_changed = false;
	bool skeleton_changed = false;
	FileStatus skeleton_stats;
	FileStatus atlas_stats;

};

enum ExtensionTypes
{
	TypeTexture,
	TypeSound,
	TypeArchetype,
	TypeLevel,
	TypeSpine,
	NotFound
};


class IResource : public IBase
{
	SM_RTTI_DECL(IResource, IBase);
public:
	void* mRawData;
	FileStatus modifytime;
	std::string RelPath;

};

template <typename T>
class Resource : public IResource
{
	SM_RTTI_DECL(Resource, IResource);
public:
	Resource(const std::string& filename);
	~Resource();
	T* get() { return (T*)mRawData; };
	void SetResource(T* resource_to_set);


	//std::string AbsPath;
};

class ResourceManager : public IBase
{
	SM_RTTI_DECL(ResourceManager, IBase);
	Make_Singleton(ResourceManager)
public:

	bool Initialize(void);
	void Update(void);
	void Shutdown(void);


	IResource* GetResource(const std::string& filename);
	IResource* Load(const std::string& name_extension);
	IResource* ReloadResource(std::string filename);
	void SetAllTextures();
	void SetAllSpineTextures();
	void UnloadResource(const std::string& name_extension);
	void UnloadAllResources();

	void AddResource(IResource* to_add, const ExtensionTypes& file_extension, const std::string& filename);

	void LoadResourcesFromMeta();
	void SaveResourcesToMeta(std::string to_save = "./data/EditorResources.meta");
	void ParseResourcesMeta();


	IResourceImporter* GetImporterFromExtension(const ExtensionTypes& file_extension);
	ExtensionTypes GetExtensionOfFile(const std::string& filename);

	void RegisterArchetypeFromLoad(std::string object_name);
	void RegisterArchetype(GameObject& to_register);
	void SaveLevel(std::string level_);
	std::vector<std::string>& GetAllArchetypes();
	void FindDumpResourcesInLevel();
	int FirstParser();
	template <typename T>
	Resource<T>* GetResourceType(const std::string& name_extension)
	{

		auto current_res = GetResource(name_extension);
		if (current_res == nullptr)
		{
			return nullptr;
			ExtensionTypes current_type = GetExtensionOfFile(name_extension);
			switch (current_type)
			{
			case TypeTexture:
				current_res = GetResource("./data/Textures/bakugo.png");
				break;


			case TypeSound:
				current_res = GetResource("./data/Sounds/short_sound.mp3");
				break;

			case TypeLevel:
				current_res = GetResource("./data/Level/Default.lvl");
				break;
			}
		}
		return dynamic_cast<Resource<T>*>(current_res);
	}

	std::unordered_map<ExtensionTypes, std::vector<std::string>> name_files;
	std::unordered_map<ExtensionTypes, IResourceImporter*>  ImportersMap;
	std::unordered_map<ExtensionTypes, std::map<std::string, IResource*>>  Resourcesmap;
	std::vector<std::string> allArchetypes;
	std::vector<std::string> current_level_resources;
	std::map<IResource*,SpineFile> spine_names;
	std::map<spine::AtlasPage*,spine::String> spineinfos;
	std::vector<TexturesInfos> infos;
	std::vector<std::string> objects_to_reload;
	bool first_load = true;
	
};

#define RsrcMan ResourceManager::Get()


class IResourceImporter : public IBase
{

	SM_RTTI_DECL(IResourceImporter, IBase)
public:
	IResourceImporter() {}
	virtual ~IResourceImporter() {}
	virtual IResource* Load(std::string filename) = 0;
	virtual IResource* ReLoad(std::string filename) = 0;
	virtual IResource* LoadDummy() = 0;
	virtual void Unload(const std::string& name_extension) = 0;

};



class NotFoundImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename)override;
	IResource* LoadDummy() { return nullptr; }
	void Unload(const std::string& name_extension);
	std::string dummytext = "Dummy";

};

class SoundImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename) override;
	IResource* LoadDummy()override;
	void Unload(const std::string& name_extension)override;
	std::string dummytext = "./data/Sounds/short_sound.mp3";
};

class TextureImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename)override;
	IResource* LoadDummy()override;
	void Unload(const std::string& name_extension);
	bool LoadImageFromFile(const char* filename, char*& outPixels, GLuint& outWidth, GLuint& outHeight);
	void CreateOpenGLTexture(GLuint to_check);
	std::string dummytext = "./data/Textures/bakugo.png";
};


class ArchetypeImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename)override;
	IResource* LoadDummy()override;
	void Unload(const std::string& name_extension)override;
	std::string dummytext = "Dummy";

};


class SpineImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename)override;
	IResource* LoadDummy()override;
	void Unload(const std::string& name_extension)override;
	std::string dummytext = "Dummy";

};


class LevelImporter : public IResourceImporter
{
private:
	IResource* Load(std::string filename)override;
	IResource* ReLoad(std::string filename)override;
	IResource* LoadDummy()override;
	void Unload(const std::string& name_extension)override;
	std::string dummytext = "./data/Levels/Default.lvl";

};



template<typename T>
inline Resource<T>::Resource(const std::string& filename)
{
	mName = filename;
	RelPath = filename;
	stat(filename.data(), &modifytime);
}

template<typename T>
inline Resource<T>::~Resource()
{
	ExtensionTypes extension = RsrcMan->GetExtensionOfFile(RelPath);
	IResourceImporter* myImporter = RsrcMan->GetImporterFromExtension(extension);

	myImporter->Unload(RelPath);
	T* got = get();
	got = nullptr;

}

template<typename T>
inline void Resource<T>::SetResource(T* resource_to_set)
{
	if (resource_to_set)
	{
		mRawData = resource_to_set;
	}

}



class TexturesInfos
{
public:
	std::string name_file;
	IResource* resource;
	unsigned int  width_;
	unsigned int  height_;
	char* pixels_;
	std::string pixels2;
	GLuint GLHandle_;

};

class MultithreadedLoader : public IBase
{
	SM_RTTI_DECL(MultithreadedLoader, IBase);
	Make_Singleton(MultithreadedLoader)

public:

	void AddTask();
	void FinishTask();

	int	mToLoad = 0;

	void PushInfo(TexturesInfos& _info);
	std::vector<TexturesInfos> loaded_textures;
	std::mutex	mLock;

};