#define ASSERTS_ENABLED

namespace Assert
{
	enum FailBehavior
	{
		Halt,
		Continue,
	};

	typedef FailBehavior(*Handler)(const char* condition,
		const char* msg,
		const char* file,
		int line);

	Handler GetHandler();
	void SetHandler(Handler newHandler);

	FailBehavior ReportFailure(const char* condition,
		const char* file,
		int line,
		const char* msg, ...);
}

#define HALT() __debugbreak()
#define UNUSED(x) do { (void)sizeof(x); } while(0)

#ifdef ASSERTS_ENABLED
#define ASSERT(cond) \
		do \
		{ \
			if (!(cond)) \
			{ \
				if (Assert::ReportFailure(#cond, __FILE__, __LINE__, 0) == \
					Assert::Halt) \
					HALT(); \
			} \
		} while(0)

#define ASSERT_MSG(cond, msg, ...) \
		do \
		{ \
			if (!(cond)) \
			{ \
				if (Assert::ReportFailure(#cond, __FILE__, __LINE__, (msg), __VA_ARGS__) == \
					Assert::Halt) \
					HALT(); \
			} \
		} while(0)

#define ASSERT_FAIL(msg, ...) \
		do \
		{ \
			if (Assert::ReportFailure(0, __FILE__, __LINE__, (msg), __VA_ARGS__) == \
				Assert::Halt) \
			HALT(); \
		} while(0)

#else
#define ASSERT(condition) \
		do { UNUSED(condition); } while(0)
#define ASSERT_MSG(condition, msg, ...) \
		do { UNUSED(condition); UNUSED(msg); } while(0)
#define ASSERT_FAIL(msg, ...) \
		do { UNUSED(msg); } while(0)
#endif