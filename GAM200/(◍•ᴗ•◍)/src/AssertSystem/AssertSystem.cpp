#include <iostream>
#include <cstdarg>
#include <string>

#include "AssertSystem.h"

namespace Assert
{
	Assert::FailBehavior DefaultHandler(const char* condition,
		const char* msg,
		const char* file,
		const int line)
	{
		//std::printf("%s(%d): Assert Failure: ", file, line);
		std::cout << file << "(" << line << ")" << ": Assert Failure:";

		if (condition != NULL)
			//std::printf("'%s' ", condition);
			std::cout << "'" << condition << "'";

		if (msg != NULL)
			//std::printf("%s", msg);
			std::cout << msg;
		//std::printf("\n");
		std::cout << std::endl;

		return Assert::Halt;
	}


	Assert::Handler& GetAssertHandlerInstance()
	{
		static Assert::Handler s_handler = &DefaultHandler;
		return s_handler;
	}

	void Assert::SetHandler(Assert::Handler newHandler)
	{
		GetAssertHandlerInstance() = newHandler;
	}

	Assert::FailBehavior Assert::ReportFailure(const char* condition,
		const char* file,
		const int line,
		const char* msg, ...)
	{
		const char* message = NULL;
		if (msg != NULL)
		{
			char messageBuffer[1024];
			{
				va_list args;
				va_start(args, msg);
				vsnprintf(messageBuffer, 1024, msg, args);
				va_end(args);
			}

			message = messageBuffer;
		}

		return GetAssertHandlerInstance()(condition, message, file, line);
	}
}



