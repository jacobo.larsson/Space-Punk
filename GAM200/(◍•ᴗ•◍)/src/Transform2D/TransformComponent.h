/**********************************************************************************/
/*
\file   TransformComponent.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that give the position, scale and orientation. 
It also sets the world position, scale and orientation.
*/
/*********************************************************************************/
#pragma once

#include <glm/glm.hpp>

#include "../RTTI/SM_RTTI.h"
#include "../LogicSystem/LogicComponent.h"

#include "../PropertySystem/PropertyMap.h"

#include "Transform2D.h"

class Collider;

class TransformComponent : public IComp
{
	friend Collider;

	SM_RTTI_DECL(TransformComponent, IComp);
public:
	TransformComponent();
	~TransformComponent();
	TransformComponent& operator=(const TransformComponent& rhs);
	TransformComponent* Clone()override;

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	Transform2D& GetWorldTransform();
	Transform2D& GetLocalTransform();

	void SetPosition(const glm::vec3& pos3D);
	void SetScale(const glm::vec2& scale);
	void SetOrientation(float rotZ);

	glm::vec3 GetPosition() const;
	glm::vec3 GetScale() const;
	float GetOrientation() const;

	void SetWorldPosition(const glm::vec3& pos3D);
	void SetWorldScale(const glm::vec2& scale);
	void SetWorldOrientation(float rotZ);

	glm::vec3 GetWorldPosition() const;
	glm::vec3 GetWorldScale() const;
	float GetWorldOrientation() const;

	void SetParentTransform(TransformComponent* parent);
	void SetParentTransformNoChange(TransformComponent* parent);
	TransformComponent* GetParentTransform();

	void UpdateWorld();

	bool Edit()override;

private:
	bool moving = false;
	TransformComponent* parent_transform = nullptr;
	Transform2D local_transform, world_transform;
};