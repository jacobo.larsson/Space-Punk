/**********************************************************************************/
/*
\file   Singleton.h
\author Thomas Komair
\par    email: tkomair@digipen.edu
\par    DigiPen login: tkomair
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that make a transform in 2d, plus making matrix operations
*/
/*********************************************************************************/
#pragma once
#include <glm/glm.hpp>

class Transform2D 
{
	friend class GameObject;
	
public:

	// ------------------------------------------------------------------------
	// Default constructor. Sets the transform to identity
	Transform2D();

	// ------------------------------------------------------------------------
	// Custom constructors
	Transform2D(const glm::vec3& pos, const glm::vec3& scale, const float& rot);
	Transform2D(float tx, float ty, float sx, float sy, float rot);

	// ------------------------------------------------------------------------
	// SetIdentity - Sets this transform to the identity transform such that
	// Position = (0,0), Scale = (1,1), Orientation = 0.0f;
	void SetIdentity();

	// ------------------------------------------------------------------------
	// GetMatrix - returns a 3x3 matrix representing the sequence of transforms
	// mat = T*R*S
	glm::mat4 GetMatrix()const;

	// ------------------------------------------------------------------------
	// GetInvMatrix - returns a 3x3 matrix representing the inverse sequence of
	// transforms: mat = S_inv * R_inv * T_inv
	glm::mat4 GetInvMatrix()const;

	// ------------------------------------------------------------------------
	// Transform concatenation: T = this * rhs;
	Transform2D Concat(const Transform2D& rhs)const;
	Transform2D operator *(const Transform2D& rhs)const;
	Transform2D& operator *=(const Transform2D& rhs);

	// ------------------------------------------------------------------------
	// MultPoint - Transforms a vector rhs by the T*R*S matrix.
	glm::vec3 MultPoint(const glm::vec3& rhs)const;
	glm::vec3 operator *(const glm::vec3& rhs)const;

public:
	glm::vec3	mPosition;		// Position
	glm::vec3	mScale;			// Scale
	float	mOrientation;	// Orientation in radians!
};