/**********************************************************************************/
/*
\file   Transform2D.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declaration of the functions of Transform2D.h
*/
/*********************************************************************************/

#include <glm/gtc/matrix_transform.hpp>
#include"Transform2D.h"
/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*/
Transform2D::Transform2D()
{
	mPosition = { 0.0f,0.0f, 0.0f};	// set all the values to the basic ones
	mScale = { 1.0f,1.0f, 1.0f };		// set all the values to the basic ones
	mOrientation = 0.0f;		// set all the values to the basic ones
}
/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*	\param const glm::vec2&: The position
*	\param const glm::vec2&: The scale
*	\param const float &:  The angle
*/
Transform2D::Transform2D(const glm::vec3& pos, const glm::vec3& scale, const float& rot)
	: mPosition{pos}, mScale{scale}, mOrientation{rot}
{}
/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*	\param	float tx: The position x
*	\param	float ty: The position y
*	\param	float sx: The scale x
*	\param	float sy: The scale y
*	\param  float rot:  The angle
*/
Transform2D::Transform2D(float tx, float ty, float sx, float sy, float rot)
	:mPosition{tx, ty, 0},mScale{sx, sy, 0}, mOrientation{rot}
{}
/*!
*	\fn		SetIdentity
*	\brief	Sets the transform 2D to the most basic values.
*/
void Transform2D::SetIdentity()
{
	// set all the values to the basic ones
	mPosition = { 0.0f,0.0f, 0.0f };
	mScale = { 1.0f,1.0f, 0.0f };	   
	mOrientation = 0.0f;	   
}
/*!
*	\fn		GetMatrix
*	\brief	Gets the matrix of the transform
*	\return The matrix
*/
glm::mat4 Transform2D::GetMatrix() const
{
	glm::mat4 translation = glm::translate(glm::mat4(1.0f), mPosition); // Translation matrix
	glm::mat4 rotation = glm::rotate(glm::mat4(1.0f),mOrientation, glm::vec3(0.0f,0.0f,1.0f)); // rotation matrix
	glm::mat4 scale = glm::scale(glm::mat4(1.0f),glm::vec3(mScale.x, mScale.y,1.0f)); // scale matrix

	return translation * rotation * scale;	// return the multiplied matrix using the other three
}
/*!
*	\fn		GetInvMatrix
*	\brief	Gets the inverse matrix of the transform
*	\return The inverse matrix
*/
glm::mat4 Transform2D::GetInvMatrix() const
{
	glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3(-mPosition.x, -mPosition.y, 0.0f)); // Translation matrix
	glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), -mOrientation, glm::vec3(mPosition.x, mPosition.y, 1.0f)); // rotation matrix
	float scalex = mScale.x != 0 ? 1 / mScale.x : 0;
	float scaley = mScale.y != 0 ? 1 / mScale.y : 0;
	glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(scalex, scaley, 1.0f)); // scale matrix

	return scale * rotation * translation;	// return the multiplied matrix using the other three
}
/*!
*	\fn		Concat
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D Transform2D::Concat(const Transform2D& rhs) const
{
	Transform2D temp; // holder transform

	temp.mPosition.x = mPosition.x + cos(mOrientation) * mScale.x * rhs.mPosition.x - sin(mOrientation) * mScale.y * rhs.mPosition.y; // position x
	temp.mPosition.y = mPosition.y + sin(mOrientation) * mScale.x * rhs.mPosition.x + cos(mOrientation) * mScale.y * rhs.mPosition.y; // position y

	temp.mScale.x = (mScale.x * rhs.mScale.x); // scale x
	temp.mScale.y = (mScale.y * rhs.mScale.y);	// scale y

	temp.mOrientation = (mOrientation + rhs.mOrientation); // rotation addition
	return temp; // return holder transform
}
/*!
*	\fn		operator*
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D Transform2D::operator*(const Transform2D& rhs) const
{
	Transform2D temp = Concat(rhs); // holder transform concatenated with the passed one

	return temp; // return the holder transform
}
/*!
*	\fn		operator*=
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D& Transform2D::operator*=(const Transform2D& rhs)
{
	*this = Concat(rhs); // concatenate the original transform with the passed one

	return *this; // return the transform
}
/*!
*	\fn		MultPoint
*	\brief	Transforms one point
*	\param	const glm::vec2 & rhs: The vector to transform
*	\return The transformed vector
*/
glm::vec3 Transform2D::MultPoint(const glm::vec3& rhs) const
{
	glm::vec4 temp(rhs, 1.0f); // holder vector
	temp = GetMatrix() * temp; // multiply the point with the transformation matrix
	return glm::vec3(temp); // return the holder vector
}
/*!
*	\fn		operator*
*	\brief	Transforms one point
*	\param	const glm::vec2 & rhs: The vector to transform
*	\return The transformed vector
*/
glm::vec3 Transform2D::operator*(const glm::vec3& rhs) const
{
	glm::vec4 temp{ rhs,1.0f }; // holder vector
	temp = GetMatrix() * temp; // multiply the point with the transformation matrix
	return glm::vec3(temp); // return the holder vector
}
