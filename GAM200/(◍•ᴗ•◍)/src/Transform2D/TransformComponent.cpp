/**********************************************************************************/
/*
\file   TransformComponent.cpp
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the declaration of the functions of TransformComponent.h
*/
/*********************************************************************************/

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"
#include "../AssertSystem/AssertSystem.h"

#include "../Factory/Factory.h"
#include "../PropertySystem/PropertyMap.h"
#include "../Changer/Changer.h"
#include "../Input/Input.h"
#include "../Editor/ObjectEdition/ObjectEdition.h"
#include "../Collisions/ICollider.h"

#include "../GameObject/GameObject.h"

#include "TransformComponent.h"

TransformComponent::TransformComponent()
{
	mName = "TransformComponent";
}
TransformComponent::~TransformComponent()
{}
TransformComponent& TransformComponent::operator=(const TransformComponent& rhs)
{
	SetScale(rhs.GetScale());
	SetOrientation(rhs.GetOrientation());

	SetWorldScale(rhs.GetWorldScale());
	SetWorldOrientation(rhs.GetWorldOrientation());

	return (*this);
}
TransformComponent* TransformComponent::Clone()
{
	TransformComponent* temp = FactorySys->Create<TransformComponent>();
	(*temp) = (*this);
	return temp;
}

void TransformComponent::OnCreate()
{
}
void TransformComponent::Initialize()
{
	GameObject* object_owner = GetOwner();

	glm::ivec2 _MaxScale = world_transform.mScale;

	mOwner->mPos.x = world_transform.mPosition.x;
	mOwner->mPos.y = world_transform.mPosition.y;

	if (auto _Collider = mOwner->GetComp<Collider>())
	{
		_Collider->GetCollideBox().SetParentTransformNoChange(this);
		_Collider->GetCollideBox().UpdateWorld();

		auto _ColliderScale = _Collider->GetCollideBox().GetWorldScale();
		auto _ColliderPosition = _Collider->GetCollideBox().GetWorldPosition();

		// Set the maximum scale of the object
		if (_ColliderScale.x > world_transform.mScale.x)
			_MaxScale.x = _ColliderScale.x;
		if (_ColliderScale.y > world_transform.mScale.y)
			_MaxScale.y = _ColliderScale.y;

		// Check if the collider is displaced
		if (_ColliderPosition.x != world_transform.mPosition.x || _ColliderPosition.y != world_transform.mPosition.y)
		{
			mOwner->mDisplacedCollider = true;
			mOwner->mDisplacedPos.x = _ColliderPosition.x;
			mOwner->mDisplacedPos.y = _ColliderPosition.y;
		}
	}
	mOwner->mScale.x = _MaxScale.x / 2;
	mOwner->mScale.y = _MaxScale.y / 2;
}
void TransformComponent::Update()
{
	if (mOwner->GetParentSpace())
	{
		if (mOwner->GetParentSpace()->SpacePartitioningEnabled()&&mOwner->mQuad.size()&& !mOwner->mbDeleted)
		{
			mOwner->mPos.x = world_transform.mPosition.x;
			mOwner->mPos.y = world_transform.mPosition.y;

			if (mOwner->mQuad.size())
			{
				if (mOwner->HasChangedPartition())
				{
					mOwner->RemoveFromPartitions();
					mOwner->GetParentSpace()->InsertObject(mOwner);
				}
			}
		}
	}
	
}
void TransformComponent::Shutdown()
{
}

void TransformComponent::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	IBase::ToJson(j);

	json& temp_trans2 = j["world_transform"];

	temp_trans2["Position"].push_back(world_transform.mPosition.x);
	temp_trans2["Position"].push_back(world_transform.mPosition.y);
	temp_trans2["Position"].push_back(world_transform.mPosition.z);

	temp_trans2["Scale"].push_back(world_transform.mScale.x);
	temp_trans2["Scale"].push_back(world_transform.mScale.y);

	temp_trans2["Orientation"] = world_transform.mOrientation;

}
void TransformComponent::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("world_transform") != j.end())
	{
		if (j["world_transform"].find("Position") != j["world_transform"].end() && j["world_transform"]["Position"].size() >= 3)
		{
			local_transform.mPosition.x = j["world_transform"]["Position"][0];
			local_transform.mPosition.y = j["world_transform"]["Position"][1];
			local_transform.mPosition.z = j["world_transform"]["Position"][2];


			world_transform.mPosition.x = j["world_transform"]["Position"][0];
			world_transform.mPosition.y = j["world_transform"]["Position"][1];
			world_transform.mPosition.z = j["world_transform"]["Position"][2];
		}

		if (j["world_transform"].find("Scale") != j["world_transform"].end() && j["world_transform"]["Scale"].size() >= 2)
		{
			local_transform.mScale.x = j["world_transform"]["Scale"][0];
			local_transform.mScale.y = j["world_transform"]["Scale"][1];

			world_transform.mScale.x = j["world_transform"]["Scale"][0];
			world_transform.mScale.y = j["world_transform"]["Scale"][1];
		}

		if (j["world_transform"].find("Orientation") != j["world_transform"].end())
		{
			local_transform.mOrientation = j["world_transform"]["Orientation"];

			world_transform.mOrientation = j["world_transform"]["Orientation"];
		}
	}
}

Transform2D& TransformComponent::GetWorldTransform()
{
	return world_transform;
}
Transform2D& TransformComponent::GetLocalTransform()
{
	return local_transform;
}

void TransformComponent::SetPosition(const glm::vec3& pos3D)
{
	if (parent_transform)
	{
		float parent_scale_x = parent_transform->world_transform.mScale.x != 0 ? pos3D.x / parent_transform->world_transform.mScale.x : 0;
		float parent_scale_y = parent_transform->world_transform.mScale.y != 0 ? pos3D.y / parent_transform->world_transform.mScale.y : 0;
		float parent_scale_z = parent_transform->world_transform.mScale.z != 0 ? pos3D.z / parent_transform->world_transform.mScale.z : 0;
		local_transform.mPosition.x = parent_scale_x;
		local_transform.mPosition.y = parent_scale_y;
		local_transform.mPosition.z = parent_scale_z;
	}
	else
	{
		local_transform.mPosition.x = pos3D.x;
		local_transform.mPosition.y = pos3D.y;
		local_transform.mPosition.z = pos3D.z;
	}

}
void TransformComponent::SetScale(const glm::vec2& scale)
{
	if (parent_transform)
	{
		if (scale.x >= 0)
		{
			float scale_x = parent_transform->world_transform.mScale.x != 0 ? scale.x / parent_transform->world_transform.mScale.x : 0;
			local_transform.mScale.x = scale_x;
		}
		if (scale.y >= 0)
		{
			float scale_y = parent_transform->world_transform.mScale.y != 0 ? scale.y / parent_transform->world_transform.mScale.y : 0;
			local_transform.mScale.y = scale_y;
		}
		local_transform.mScale.z = 1;
	}
	else
	{
		if (scale.x >= 0)
			local_transform.mScale.x = scale.x;
		if (scale.y >= 0)
			local_transform.mScale.y = scale.y;
		local_transform.mScale.z = 1;
	}
}
void TransformComponent::SetOrientation(float rotZ)
{
	local_transform.mOrientation = rotZ;
}

glm::vec3 TransformComponent::GetPosition() const
{
	if (parent_transform)
	{
		glm::vec3 local_pos = local_transform.mPosition;
		glm::vec3 parent_scale = parent_transform->world_transform.mScale;
		return glm::vec3(local_pos.x*parent_scale.x, local_pos.y*parent_scale.y, local_pos.z);
	}
	else
		return local_transform.mPosition;
}
glm::vec3 TransformComponent::GetScale() const
{
	if (parent_transform)
	{
		glm::vec2 local_scale = local_transform.mScale;
		glm::vec3 parent_scale = parent_transform->world_transform.mScale;
		return glm::vec3(local_scale.x * parent_scale.x, local_scale.y * parent_scale.y, 1);
	}
	else
		return glm::vec3(local_transform.mScale.x, local_transform.mScale.y, 1);
}
float TransformComponent::GetOrientation() const
{
	if (parent_transform)
		return local_transform.mOrientation + parent_transform->world_transform.mOrientation;
	else
		return local_transform.mOrientation;
}

void TransformComponent::SetWorldPosition(const glm::vec3& pos3D)
{
	world_transform.mPosition = pos3D;

	if (parent_transform)
	{
		glm::vec4 newvec(pos3D, 1);
		local_transform.mPosition = parent_transform->world_transform.GetInvMatrix() * newvec;
	}
	else
		local_transform.mPosition = world_transform.mPosition;
}
void TransformComponent::SetWorldScale(const glm::vec2& scale)
{
	if (scale.x >= 0)
		world_transform.mScale.x = scale.x;
	if (scale.y >= 0)
		world_transform.mScale.y = scale.y;
	world_transform.mScale.z = 1;

	if (parent_transform)
	{
		float parent_scale_x = parent_transform->world_transform.mScale.x != 0 ? world_transform.mScale.x / parent_transform->world_transform.mScale.x : 0;
		float parent_scale_y = parent_transform->world_transform.mScale.y != 0 ? world_transform.mScale.y / parent_transform->world_transform.mScale.y : 0;
		local_transform.mScale = glm::vec3(parent_scale_x, parent_scale_y, 1);
	}
	else
		local_transform.mScale = world_transform.mScale;
}
void TransformComponent::SetWorldOrientation(float rotZ)
{
	world_transform.mOrientation = rotZ;
	if (parent_transform)
		local_transform.mOrientation = world_transform.mOrientation - parent_transform->world_transform.mOrientation;
	else
		local_transform.mOrientation = world_transform.mOrientation;
}

glm::vec3 TransformComponent::GetWorldPosition() const
{
	return world_transform.mPosition;
}
glm::vec3 TransformComponent::GetWorldScale() const
{
	return glm::vec3(world_transform.mScale.x, world_transform.mScale.y, 1);
}
float TransformComponent::GetWorldOrientation() const
{
	return world_transform.mOrientation;
}

void TransformComponent::SetParentTransform(TransformComponent* parent)
{
	parent_transform = parent;

	SetWorldPosition(GetWorldPosition());
	SetWorldScale(GetWorldScale());
	SetWorldOrientation(GetWorldOrientation());
}
void TransformComponent::SetParentTransformNoChange(TransformComponent* parent)
{
	parent_transform = parent;
}
TransformComponent* TransformComponent::GetParentTransform()
{
	return parent_transform;
}

void TransformComponent::UpdateWorld()
{
	if (parent_transform)
		world_transform = parent_transform->world_transform * local_transform;
	else
		world_transform = local_transform;

	//Update the position of the collider
	GameObject* owner = GetOwner();
	if (owner)
	{
		Collider* coll = owner->GetComp<Collider>();
		if (coll)
		{
			if (coll->GetCollideBox().GetParentTransform() == nullptr)
				coll->GetCollideBox().SetParentTransformNoChange(this);
			coll->GetCollideBox().UpdateWorld();
		}
	}
}

bool TransformComponent::Edit()
{
	bool changed = false;
	glm::vec3 temp_vec;

	// Position
	temp_vec = GetPosition();
	ImGui::Text("Position");
	ImGui::PushID("Position");
	ImGui::Columns(3, "Position", true);

	ImGui::DragFloat("X", &temp_vec.x);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("Y", &temp_vec.y);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	ImGui::DragFloat("Z", &temp_vec.z);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	SetPosition(temp_vec);

	ImGui::Columns(1);
	ImGui::PopID();

	// Scale
	temp_vec = GetScale();
	ImGui::Text("Scale");
	ImGui::PushID("Scale");
	ImGui::Columns(2, "Scale", true);
	ImGui::DragFloat("X", &temp_vec.x, 1.0f, 0.0f, 100000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;


	ImGui::NextColumn();
	ImGui::DragFloat("Y", &temp_vec.y, 1.0f, 0.0f, 100000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	SetScale(temp_vec);
	ImGui::NextColumn();
	ImGui::Columns(1);
	ImGui::PopID();

	// Rotation
	float orientation = local_transform.mOrientation;
	ImGui::SliderFloat("Rotation", &orientation, -3.1415f, 3.1415f, "%.3f");
	SetOrientation(orientation);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;


	//Gizmo Editing
	if (ObjectEdition::editing_gizmo == ObjectEdition::None && (ObjectEdition::prev_editing_gizmo == ObjectEdition::None || ObjectEdition::prev_editing_gizmo == ObjectEdition::Transform))
	{
		ObjectEdition::editing_gizmo = ObjectEdition::Transform;
		moving |= ObjectEdition::EditSelectedObject(this);

		if (moving && InputSys->mouse_is_released(mouse_buttons::Right_click))
		{
			ChangerSys->AddChange(mOwner, ObjectChanged);
			moving = false;
		}
	}
	return changed;
}