#pragma once
// ----------------------------------------------------------------------------
#include <glm/glm.hpp>
#include <vector>
#include "../RTTI/SM_RTTI.h"
#include "../LogicSystem/LogicComponent.h"


struct PathPoint2D 
{
	friend struct Path2D;
public:
	PathPoint2D();
	PathPoint2D(float t);
	PathPoint2D(float t, const glm::vec3& pt);
private:
	float mTime;
	glm::vec3 mPoint = { 0,0,0 };
};

struct Path2D : public LogicComponent
{
	SM_RTTI_DECL(Path2D, LogicComponent);

	Path2D();
	~Path2D();
	Path2D& operator=(const Path2D& rhs);
	Path2D* Clone()override;
	
	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;

	void ToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j);

	glm::vec3 SampleTime(float t) const;
	glm::vec3 SampleDistance(float dist) const;
	glm::vec3 SampleParameter(float u)const;

	void Push(float t, const glm::vec3 &val);
	void Clear();

	unsigned int Size() const;
	float Duration() const;
	float GetTotalLength() const;
	float GetLengthFromParam(float u)const;

private:
	float mCummulatedTime;
	float mLastRegisteredTime = 0.0f;
	bool loopDone;
	std::vector<PathPoint2D> mPoints;
	std::vector<float> mCummulatedLengths;
};