
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>


#include <GL/glew.h>

#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "../Graphics/RenderManager/RenderManager.h"
#include "../Graphics/Mesh/Mesh.h"
#include "../Graphics/Texture/Texture.h"
#include "../Graphics/Color/Color.h"
#include "../Graphics/Shaders/Shader.h"
#include "../Graphics/Camera/Camera.h"

#include "../Space/Space.h"
#include "../Scene/Scene.h"

#include "../Transform2D/TransformComponent.h"
#include "../Space/Space.h"
#include "../Factory/Factory.h"
#include "../Scene/Scene.h"
#include "../GameObject/GameObject.h"
#include "../Time/Time.h"
#include "SM_Animation.h"
#include "../Editor/Editor.h"

/*
	The SpineExtension class is a mechanism for the engine to
	control how spine allocates memory, reads data from file.
	This is essentially spine-cpp's platform-independent layer.

	Fortunately, spine-cpp comes with a default extension that work
	with the default memory allocation functions of the C/C++ runtime
	(malloc, alloc, file I/O, etc...).

	All we have to do is to return the default extension.
*/
static int animNum = 0;
spine::CustomTextureLoader sTextureLoader;

SpineData::SpineData()
{
	skelData = nullptr;
	atlas = nullptr;
	animStateData = nullptr;
}
SpineData::~SpineData()
{
	SpineData::FreeData();
}
void SpineData::FreeData()
{
	if (skelData)delete skelData;
	if (atlas) delete atlas;
	if (animStateData) delete animStateData;

	skelData = 0;
	atlas = 0;
	animStateData = 0;
}
void SpineData::Load(const char* jsonPath, const char* atlasFile) {
	//if (FilePath::Exists(jsonPath) && FilePath::Exists(atlasFile))
	if (jsonPath && atlasFile)
	{
		FreeData();

		// load atlas firts
		atlas = new spine::Atlas(atlasFile, &spine::sTextureLoader);

		// load json again
		// json variable
		spine::SkeletonJson json(atlas);

		// set the scale at which the skeleton should be loaded
		json.setScale(jsonScale);

		skelData = json.readSkeletonDataFile(jsonPath);

		// loading failed
		if (!skelData) {
			printf("ERROR! Loading skel data Json: %s\n", json.getError().buffer());
			FreeData();
			return;
		}
		// create a new animation state data
		animStateData = new spine::AnimationStateData(skelData);
	}
}
void SkeletonRenderable::ApplyAnimation(float dt)
{
	// update skeleton animation
	mSkeleton->update(dt);

	// update anim
	mAnimState->update(dt * mTimeScale);

	// apply anim
	mAnimState->apply(*mSkeleton);

	// update transform
	mSkeleton->updateWorldTransform();
}

void SkeletonRenderable::ReloadSpineInstances() {

	// sanity check
	if (!mSpineData)
		return;

	if (mSkeleton)delete mSkeleton;
	if (mAnimState) delete mAnimState;

	// create the skeleton
	mSkeleton = new spine::Skeleton(mSpineData->get()->skelData);

	// create the animation state
	mAnimState = new spine::AnimationState(mSpineData->get()->animStateData);
}

void SkeletonRenderable::DeleteSpineInstances()
{
	if (mSkeleton)delete mSkeleton;
	if (mAnimState) delete mAnimState;

	mSkeleton = 0;
	mAnimState = 0;
}

spine::Animation* SkeletonRenderable::GetAnimationByName(const char* name) const
{
	return mSpineData->get()->skelData->findAnimation(name);
}
spine::Animation* SkeletonRenderable::GetCurrentAnimation() const
{
	return mAnimState->getCurrent(0)->getAnimation();
}
std::string SkeletonRenderable::GetCurrentAnimationName()
{
	std::string animName = mAnimState->getCurrent(0)->getAnimation()->getName().buffer();
	return animName;
}
void  SkeletonRenderable::SetCurrentAnimation(const char* animName, bool loop)
{
	if (AnimationExists(animName))
		mAnimState->setAnimation(0, GetAnimationByName(animName), loop);
}
bool  SkeletonRenderable::AnimationExists(const char* animName) const
{
	return GetAnimationByName(animName) != nullptr;
}
void  SkeletonRenderable::SetToSetupPose()
{
	mSkeleton->setToSetupPose();
}
void  SkeletonRenderable::SetMixValue(const char* fromAnimName, const char* toAnimName, float mixDuration)
{
	mSpineData->get()->animStateData->setMix(spine::String(fromAnimName), spine::String(toAnimName), mixDuration);
}
float  SkeletonRenderable::GetMixValue(const char* fromAnimName, const char* toAnimName)
{
	// get the animation 
	spine::Animation* fromAnim = GetAnimationByName(fromAnimName);
	spine::Animation* toAnim = GetAnimationByName(toAnimName);
	if (fromAnim && toAnim)
		return mSpineData->get()->animStateData->getMix(fromAnim, toAnim);
	return 0.0f;
}

void SkeletonRenderable::SetSkin(const char* skinName)
{
	mSkeleton->setSkin(skinName);
}

void SkeletonRenderable::DebugDraw()
{
	// check what is ik and what isn't	
	glm::vec3 ownerScale = mTransform->mScale;

	// try to get the whole bone hierarchy
	for (int i = 0; i < mSkeleton->getBones().size(); ++i)
	{
		spine::Bone* bone = mSkeleton->getBones()[i];
		glm::vec3 pos = { bone->getWorldX() * ownerScale.x, bone->getWorldY() * ownerScale.y, 0.0f };
		pos = pos + mTransform->mPosition.z;
		SceneSys->GetMainSpace()->DrawCircle(glm::vec3(pos.x, pos.y, 0.0f), 5, Colors::blue);
		// draw line to parent
		if (bone->getParent())
		{
			glm::vec3 parentPos =
			{ bone->getParent()->getWorldX() * ownerScale.x, bone->getParent()->getWorldY() * ownerScale.y, 0.0f };
			parentPos += mTransform->mPosition.z;
			SceneSys->GetMainSpace()->DrawLine(glm::vec2(pos), glm::vec2(parentPos), Colors::blue);
		}
	}

	static float bboxVertices[128];
	for (int i = 0; i < mSkeleton->getSlots().size(); ++i)
	{
		spine::Slot* slot = mSkeleton->getDrawOrder()[i];
		if (slot->getAttachment() && slot->getAttachment()->getRTTI().isExactly(spine::BoundingBoxAttachment::rtti))
		{
			spine::BoundingBoxAttachment* bbox = (spine::BoundingBoxAttachment*)slot->getAttachment();
			int vtxCount = bbox->getWorldVerticesLength();
			bbox->computeWorldVertices(*slot, 0, vtxCount, bboxVertices, 0, 2);
			for (int v = 0; v < vtxCount - 1; v += 2)
			{
				// draw a line between each of them
				glm::vec3 bboxPt;
				glm::vec3 bboxPt2;

				if (v == vtxCount - 2)
				{
					bboxPt = glm::vec3(bboxVertices[v] * ownerScale.x, bboxVertices[v + 1] * ownerScale.y, 0);
					bboxPt2 = glm::vec3(bboxVertices[0] * ownerScale.x, bboxVertices[1] * ownerScale.y, 0);
				}
				else
				{
					bboxPt = glm::vec3(bboxVertices[v] * ownerScale.x, bboxVertices[v + 1] * ownerScale.y, 0);
					bboxPt2 = glm::vec3(bboxVertices[v + 2] * ownerScale.x, bboxVertices[v + 3] * ownerScale.y, 0);
				}
				bboxPt += mTransform->mPosition.z;
				bboxPt2 += mTransform->mPosition.z;

				SceneSys->GetMainSpace()->DrawLine(glm::vec2(bboxPt), glm::vec2(bboxPt2), Colors::blue);
			}

		}
	}
}

void SkeletonRenderable::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	j["mSpineData"] = mSpineData->RelPath;

}

void SkeletonRenderable::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	if (j.find("mSpineData") != j.end())
	{
		mSpineData = RsrcMan->GetResourceType<SpineData>(j["mSpineData"].get<std::string>());

		if (mSpineData)
		{
			// create the skeleton and animstate
			ReloadSpineInstances();

			// intitilaize some data
			mSkeleton->setToSetupPose();
			mSkeleton->setPosition(0, 0);

			// set default animation
			if (mSpineData->get()->skelData->getAnimations().size())
				mAnimState->setAnimation(0, mSpineData->get()->skelData->getAnimations()[animNum], true);

			// set default skin
			if (mSpineData->get()->skelData->getSkins().size())
				mSkeleton->setSkin(mSpineData->get()->skelData->getSkins()[0]);
		}
	}
}

float SkeletonRenderable::GetAnimTime(int trackIdx)
{
	// TODO: actually take choose the correct track.
	if (mAnimState && trackIdx < mAnimState->getTracks().size())
		return mAnimState->getTracks()[trackIdx]->getAnimationTime();
	return -1.0f;
}

void SkeletonRenderable::SetAnimTime(float time, int trackIdx)
{
	if (mAnimState && trackIdx < mAnimState->getTracks().size())
		mAnimState->getTracks()[trackIdx]->setTrackTime(time);
}

float SkeletonRenderable::GetAnimDuration(int trackIdx)
{
	if (mAnimState && trackIdx < mAnimState->getTracks().size()) {
		float e = mAnimState->getTracks()[trackIdx]->getAnimationEnd();
		float s = mAnimState->getTracks()[trackIdx]->getAnimationStart();
		return e - s;
	}
	return -1.0f;
}

SkeletonRenderable::SkeletonRenderable() : Renderable(), mTransform()
{
	SetName("SkeletonRenderable");
	mShader = RndMngr->GetShader("spine.shader");
	mSpineData = RsrcMan->GetResourceType<SpineData>("./data/Animations/Spine-boy/spineboy.anim");
	if (mSpineData)
	{
		// create the skeleton and animstate
		ReloadSpineInstances();

		// intitilaize some data
		mSkeleton->setToSetupPose();
		mSkeleton->setPosition(0, 0);

		// set default animation
		if (mSpineData->get()->skelData->getAnimations().size())
			mAnimState->setAnimation(0, mSpineData->get()->skelData->getAnimations()[animNum], true);

		// set default skin
		if (mSpineData->get()->skelData->getSkins().size())
			mSkeleton->setSkin(mSpineData->get()->skelData->getSkins()[0]);

	}
	ModulColor.a = 1.0f;

	// manage our own model
	mMesh = new Mesh();
	mMesh->GetVertices().resize(2048);
	mMesh->GetIndices().resize(4096);

	mMesh->UploadToGPU();
}

void SkeletonRenderable::Initialize()
{
	Renderable::Initialize();
	mTransform = &mOwner->GetComp<TransformComponent>()->GetWorldTransform();
}

void SkeletonRenderable::Update()
{
	if (SettingsSys->inEditor == true && mPause == false)
		ApplyAnimation(TimeSys->GetFrameTime());
	else if (SettingsSys->inEditor == false)
		ApplyAnimation(TimeSys->GetFrameTime());
}

void SkeletonRenderable::Shutdown()
{
	mMesh = 0;
	DeleteSpineInstances();
	Renderable::Shutdown();
}

bool SkeletonRenderable::Edit()
{
	Update();

	bool changed = Renderable::Edit();

	static int spineindex = std::distance(RsrcMan->Resourcesmap[TypeSpine].begin(), RsrcMan->Resourcesmap[TypeSpine].find(mSpineData->RelPath));

	static std::string spine_active_str = RsrcMan->Resourcesmap[TypeSpine].find(mSpineData->RelPath)->first.c_str();

	auto it = RsrcMan->Resourcesmap[TypeSpine].begin();
	if (ImGui::Begin("Player Animation2"))
	{
		// show nodes
		{
			if (ImGui::CollapsingHeader((std::string("Nodes: ") + std::to_string(mSpineData->get()->skelData->getBones().size())).c_str())) {
				for (int i = 0; i < mSpineData->get()->skelData->getBones().size(); ++i) {
					spine::BoneData* bone = mSpineData->get()->skelData->getBones()[i];
					ImGui::Text("%s", bone->getName().buffer());
				}
			}
		}

		// show animations 
		{
			if (ImGui::CollapsingHeader((std::string("Animations: ") + std::to_string(mSpineData->get()->skelData->getAnimations().size())).c_str()))
			{
				for (unsigned i = 0; i < mSpineData->get()->skelData->getAnimations().size(); i++)
				{
					spine::Animation* anim = mSpineData->get()->skelData->getAnimations()[i];
					if (ImGui::Selectable(anim->getName().buffer()))
					{
						SetCurrentAnimation(anim->getName().buffer(), true);
					}
				}
			}
		}

		// show skins
		{
			if (ImGui::CollapsingHeader((std::string("Skins: ") + std::to_string(mSpineData->get()->skelData->getSkins().size())).c_str())) {
				for (int i = 0; i < mSpineData->get()->skelData->getSkins().size(); ++i) {
					spine::Skin* skin = mSpineData->get()->skelData->getSkins()[i];
					ImGui::Text("%s", skin->getName().buffer());
				}
			}
		}

		ImGui::Checkbox("FlipX", &flipX);
		ImGui::Checkbox("FlipY", &flipY);

		float* col = &ModulColor.r;
		ImGui::ColorPicker4("Color Modulation", col);
		ImGui::SliderFloat("Alpha", &ModulColor.a, 0.f, 1.f);

		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	ImGui::PushID("Spine Texture");
	if (ImGui::BeginCombo("Spine Texture", spine_active_str.c_str(), 0))
	{
		for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeSpine].size(); i++, it++)
		{

			if (ImGui::Selectable(it->first.c_str(), spineindex == i))
			{
				spineindex = i;
				spine_active_str = it->first.c_str();
				//SetTexture(it->first.c_str());
				mSpineData = RsrcMan->GetResourceType<SpineData>(spine_active_str);
				if (mSpineData)
				{
					// create the skeleton and animstate
					ReloadSpineInstances();

					// intitilaize some data
					mSkeleton->setToSetupPose();
					mSkeleton->setPosition(0, 0);

					// set default animation
					if (mSpineData->get()->skelData->getAnimations().size())
						mAnimState->setAnimation(0, mSpineData->get()->skelData->getAnimations()[animNum], true);

					// set default skin
					if (mSpineData->get()->skelData->getSkins().size())
						mSkeleton->setSkin(mSpineData->get()->skelData->getSkins()[0]);
				}
				changed = true;
			}

			if (spineindex == i)
				ImGui::SetItemDefaultFocus();

			if (ImGui::IsItemHovered())
			{
				/*std::string text_name = it->first;
				Resource<Texture>* texture_to_get = RsrcMan->GetResourceType<Texture>(text_name);
				ImGui::BeginTooltip();
				float my_tex_w = texture_to_get->get()->GetWidth();
				float my_tex_h = texture_to_get->get()->GetHeight();
				ImVec2 uv_min = ImVec2(-1.0f, 1.0f);
				ImVec2 uv_max = ImVec2(0.0f, 0.0f);
				ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
				ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f);
				ImGui::Image((void*)(intptr_t)texture_to_get->get()->GetGLHandle(), ImVec2(my_tex_w / 15, my_tex_h / 15), uv_min, uv_max, tint_col, border_col);
				ImGui::EndTooltip();*/

			}
		}
		ImGui::EndCombo();
	}
	ImGui::PopID();

	return changed;
}

void SkeletonRenderable::Render()
{

	//if (mpSkelAnimComp)
	{
		if (mSkeleton)
		{
			// get ptr to model current resource
			Mesh* model = mMesh;
			if (model == nullptr)
				printf("spine render: model is null\n");

			if (auto comp = mOwner->GetComp<TransformComponent>()) {
				glm::mat4 Model2World = comp->GetWorldTransform().GetMatrix();
				Transform2D FlipMatrix;
				if (flipX)
					FlipMatrix.mScale = glm::vec3(-1, 1, 1);
				else
					FlipMatrix.mScale = glm::vec3(1, 1, 1);
				mShader->SetUniform("Model2World", Model2World);
				mShader->SetUniform("ModulColor", ModulColor);
				mShader->SetUniform("FlipMatrix", FlipMatrix.GetMatrix());
				mShader->SetUniform("texture1", 0);
			}

			// vertex array used below to hold the attachment vertices when copying to the model.
			int vtx_counter = 0, idx_counter = 0;
			Texture* previous_tex = nullptr;
			Color skelColor = Color(mSkeleton->getColor().r, mSkeleton->getColor().g, mSkeleton->getColor().b, mSkeleton->getColor().a);

			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);

			// draw the skeleton 
			//for (int i = 0; i < skeleton->getSlots().size(); ++i)
			for (int i = mSkeleton->getSlots().size() - 1; i >= 0; --i)
			{
				spine::Slot* slot = mSkeleton->getDrawOrder()[i];
				if (!slot)continue;

				spine::Attachment* attachment = slot->getAttachment();
				if (!attachment)continue;

				// compute the attachment color
				Color color = skelColor;
				color = Color(slot->getColor().r, slot->getColor().g, slot->getColor().b, slot->getColor().a);

				// REGION ATTACHMENT
				if (attachment)
				{
					if (attachment->getRTTI().isExactly(spine::RegionAttachment::rtti))
					{
						// cast in region attachment
						spine::RegionAttachment* regionAttachment = (spine::RegionAttachment*)attachment;

						// retrieve texture
						Texture* pTex = (Texture*)((spine::AtlasRegion*)regionAttachment->getRendererObject())->page->getRendererObject();

						// first texture?
						{
							if (!previous_tex) previous_tex = pTex;
							// new texture (different atlas page)?
							else if (previous_tex != pTex)
							{
								// save previous texture
								previous_tex = pTex;

								// draw mesh
								DrawTempMesh(vtx_counter, idx_counter);
							}
						}

						// bind the texture
						pTex->Bind();

						// set the vertex positions
						{
							//ensure that we have enough vertices left 
							if (vtx_counter + 4 > model->GetVertexCount())
								printf("not enough vertex data\n");
							if (vtx_counter + 4 < model->GetVertexCount())
							{
								// compute vertices of the region in world (i.e. the skeleton root space)
								// all the vertex data is indexed, so here we are going to receive only 
								// 4 vertex, when 6 are needed to form a quad. 
								{
									regionAttachment->computeWorldVertices(
										slot->getBone(),			// bone
										(float*)(model->GetVertices().data() + vtx_counter),				// output
										0,							// offset
										sizeof(Vertex) / sizeof(float) // stride
									);

									// copy uvs and colors one as is
									for (int k = 0, l = 0; k < 4; ++k, l += 2) {
										model->SetVertexTex(vtx_counter + k, { regionAttachment->getUVs()[l], 1.0f - regionAttachment->getUVs()[l + 1] });
										model->SetVertexColor(vtx_counter + k, color);
									}
								}

								// copy the data to the vertex buffer
								{
									// simple idx buffer to copy easily the quad data. 
									int idxBuff[6] = { 0,1,2,0,2,3 };
									Vertex v;
									// set data and increment couinter
									for (int idx = 0; idx < 6; ++idx) {
										model->GetIndices()[idx_counter++] = (vtx_counter + idxBuff[idx]);
									}
								}

								// increment vtx_counter
								vtx_counter += 4;
							}
						}
					}

					// MESH ATTACHMENT
					else if (attachment->getRTTI().isExactly(spine::MeshAttachment::rtti))
					{
						spine::MeshAttachment* meshAttachment = (spine::MeshAttachment*)attachment;

						// retrieve texture
						Texture* pTex = (Texture*)((spine::AtlasRegion*)meshAttachment->getRendererObject())->page->getRendererObject();

						// first texture?
						{
							if (!previous_tex) previous_tex = pTex;
							// new texture (different atlas page)?
							else if (previous_tex != pTex)
							{
								// save previous texture
								previous_tex = pTex;

								// draw mesh
								DrawTempMesh(vtx_counter, idx_counter);
							}
						}

						// bind the texture
						pTex->Bind();

						// set the vertex positions
						{
							// get the number of indices in the mesh (each corresponds to a vertex index). 
							int numIndices = meshAttachment->getTriangles().size();

							// get the number of vertices in the attachment
							// division by zero is because spine uses float arrays for storing positions. so 2 float equal on position. 
							int numVertices = meshAttachment->getWorldVerticesLength() / 2;

							//ensure that we have enough vertices left 
							if (idx_counter + numIndices >= model->GetIndicesCount())
								printf("not enough index data");
							if (vtx_counter + numVertices >= model->GetVertexCount())
								printf("not enough vertex data");

							if (idx_counter + numIndices < model->GetIndicesCount() &&
								vtx_counter + numVertices < model->GetVertexCount())
							{

								// compute world vertices and copy ito vtx_buffer
								meshAttachment->computeWorldVertices(
									*slot,									// slot with the mesh attachment
									0,										// offset from the slot vertices to start transforming
									numVertices * 2,							// number of vertices to copy
									(float*)(model->GetVertices().data() + vtx_counter),	// pointer to the start of the vertex 
									0,										// offset to the first available vertex
									sizeof(Vertex) / sizeof(float));						// stride is the size of an entire vertex

								// set the vertex uv and color
								float* meshUVs = meshAttachment->getUVs().buffer();
								for (int k = 0; k < numVertices; ++k) {

									int fidx = k * 2;	// note: multiply by 2  because there are 2 values per uv
									model->SetVertexTex(vtx_counter + k, { meshUVs[fidx], 1.0f - meshUVs[fidx + 1] }); // flip y
									model->SetVertexColor(vtx_counter + k, color);
								}

								// Copy indices as is. 
								for (int k = 0; k < numIndices; ++k) {

									// get the index of the uv entry 
									int idx = meshAttachment->getTriangles()[k];
									model->GetIndices()[idx_counter++] = vtx_counter + idx;
								}

								// advance counter
								vtx_counter += numVertices;
							}
						}
					}
				}
			}

			// draw the remainder of the accumulated mesh.
			DrawTempMesh(vtx_counter, idx_counter);
			//glDisable(GL_BLEND);

		}

	}
}

void SkeletonRenderable::DrawTempMesh(int& vtx_counter, int& idx_counter)
{
	// draw and flush
	mMesh->ReUploadToGPU(/*0, vtx_counter - 1*/);
	mMesh->Draw(idx_counter);

	// flush
	vtx_counter = 0;
	idx_counter = 0;
}


void SkeletonRenderable::SkelEditor()
{
	using std::string;
	using std::to_string;
	if (mSkeleton)
	{

		if (ImGui::Begin("Player Animation"))
		{
			// show nodes
			{
				if (ImGui::CollapsingHeader((string("Nodes: ") + to_string(mSpineData->get()->skelData->getBones().size())).c_str())) {
					for (int i = 0; i < mSpineData->get()->skelData->getBones().size(); ++i) {
						spine::BoneData* bone = mSpineData->get()->skelData->getBones()[i];
						ImGui::Text("%s", bone->getName().buffer());
					}
				}
			}

			// show animations 
			{
				if (ImGui::CollapsingHeader((string("Animations: ") + to_string(mSpineData->get()->skelData->getAnimations().size())).c_str()))
				{
					for (unsigned i = 0; i < mSpineData->get()->skelData->getAnimations().size(); i++)
					{
						spine::Animation* anim = mSpineData->get()->skelData->getAnimations()[i];
						if (ImGui::Selectable(anim->getName().buffer()))
						{
							SetCurrentAnimation(anim->getName().buffer(), true);
						}
					}
				}
			}

			// show skins
			{
				if (ImGui::CollapsingHeader((string("Skins: ") + to_string(mSpineData->get()->skelData->getSkins().size())).c_str())) {
					for (int i = 0; i < mSpineData->get()->skelData->getSkins().size(); ++i) {
						spine::Skin* skin = mSpineData->get()->skelData->getSkins()[i];
						ImGui::Text("%s", skin->getName().buffer());
					}
				}
			}

			ImGui::Checkbox("FlipX", &flipX);
			ImGui::Checkbox("FlipY", &flipY);

			float* col = &ModulColor.r;
			ImGui::ColorPicker4("Color Modulation", col);
			ImGui::SliderFloat("Alpha", &ModulColor.a, 0.f, 1.f);

			ImGui::End();
		}
		else
		{
			ImGui::End();
		}

	}
}

void spine::CustomTextureLoader::load(spine::AtlasPage& page, const spine::String& path)
{
	// here you should create a texture resource (assuming you have a resource manager). 
	//Texture* newTex = RndMngr->LoadTexture("name", path.buffer());
	Resource<Texture>* newTex = dynamic_cast<Resource<Texture>*>(RsrcMan->Load(path.buffer()));
	// sanity check and error message
	if (!newTex) {
		printf("ERROR! Couldn't load Spine atlas page %s \n", path.buffer());
		return;
	}

	// set texture filter based on atlas page setting
	{

		newTex->get()->Bind();
		//newTex->Bind();
		switch (page.magFilter) {
		case spine::TextureFilter::TextureFilter_Linear:

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			break;
		case spine::TextureFilter::TextureFilter_MipMapNearestNearest:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			break;
		}
		switch (page.minFilter) {
		case spine::TextureFilter::TextureFilter_Linear:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			break;
		case spine::TextureFilter::TextureFilter_MipMapNearestNearest:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			break;
		}
		newTex->get()->Unbind();
		//newTex->Unbind();
	}

	// pass the texture pointer to the page. Spine doesn't actually do anything with it, 
	// it will be passed back to us when drawing the skeleton. 
	page.setRendererObject(newTex->get());

	// set the texture width and height
	page.width = newTex->get()->GetWidth();
	//page.width = newTex->GetWidth();
	page.height = newTex->get()->GetHeight();
	//page.height = newTex->GetHeight();
}

void spine::CustomTextureLoader::unload(void* texture)
{


	// without resource management:
	//std::string texName = ((Texture*)texture)->GetName();
	//RndMngr->DeleteTexture(texName.c_str());

	// with resource management
	// ignore this call, let the resource manager take over. 
	// also this will cause issues if the resource is reloaded. 
}

namespace spine
{
	SpineExtension* getDefaultExtension()
	{
		return new spine::DefaultSpineExtension();

	}
}
