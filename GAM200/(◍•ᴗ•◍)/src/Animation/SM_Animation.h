#pragma once
#include <spine/spine.h>
#include <spine/Extension.h>
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../LogicSystem/LogicComponent.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Graphics/Color/Color.h"
#include "../ResourceManager/ResourceManager.h"



namespace spine
{
	class SkeletonData;
	class AnimationStateData;
	class Skeleton;
	class AnimationState;
	class Atlas;
	class Animation;
}

// new Resource
class SpineData : public IBase
{
public:
	SM_RTTI_DECL(SpineData, IBase);
	spine::Atlas* atlas = 0;
	spine::SkeletonData* skelData = 0;
	spine::AnimationStateData* animStateData = 0;

	float jsonScale = 0.001f;

	SpineData();
	~SpineData();

	void FreeData();
	void Load(const char* jsonPath, const char* atlasFile);
};

// skeleton renderable
class SkeletonRenderable : public Renderable
{
	SM_RTTI_DECL(SkeletonRenderable, Renderable)

public:
	SkeletonRenderable();

	void Initialize() override;
	void Update() override;
	void Render() override;
	void Shutdown() override;
	bool Edit() override;

	void ToJson(nlohmann::json& j)override;
	void FromJson(const nlohmann::json& j)override;

	void ReloadSpineInstances();
	void DeleteSpineInstances();

	void ApplyAnimation(float dt = 0.0f);

	// Helpers and wrappers around Spine functionality
	void  SetCurrentAnimation(const char* animName, bool loop = false);
	std::string GetCurrentAnimationName();

	// Get Spine animation
	spine::Animation* GetAnimationByName(const char* name) const;
	spine::Animation* GetCurrentAnimation() const;
	bool  AnimationExists(const char* animName)const;
	void  SetToSetupPose();

	// Mix setting
	void SetMixValue(const char* fromAnimName, const char* toAnimName, float mixDuration);
	float  GetMixValue(const char* fromAnimName, const char* toAnimName);

	// Skin
	void SetSkin(const char* skinName);

	void DebugDraw();
	// Get and set anim time
	void SetAnimTime(float time, int trackIdx = 0);
	float GetAnimTime(int trackIdx = 0);
	float GetAnimDuration(int trackIdx = 0);

	void DrawTempMesh(int& vtx_counter, int& idx_counter);
	void SkelEditor();
public:

	Transform2D* mTransform;
	spine::Skeleton* mSkeleton = nullptr;
	spine::AnimationState* mAnimState = nullptr;
	Resource<SpineData>* mSpineData = nullptr;
	float	mTimeScale = 1.0f;

	std::string mCurrentAnimation;

	bool mPause = false;
	float* col = &ModulColor.r;
};

namespace spine
{
	SpineExtension* getDefaultExtension();

	/*
	spine-cpp's Atlas class expects to be passed an instance of TextureLoader to load
	and create an engine specific texture representation for a single atlas page.
	The TextureLoader class has two methods, load to load a method for an atlas page
	given a path, and unload to dispose of the texture.

	The load function may store the texture in the atlas page via a call to AtlasPage::setRendererObject().
	This makes it easy later on to get the texture an attachment references via a region in an atlas page.

	The load method is also supposed to set the width and height of the AtlasPage in pixels according to the
	texture file loaded by the engine. This data is required to compute texture coordinates by spine-c.

	The load function's path parameter is the path to the page image file, relative to the .atlas file
	path passed to the Atlas constructor, or relative to the dir parameter of the second Atlas constructor
	that loads the atlas from meory.
	*/
	class CustomTextureLoader : public spine::TextureLoader
	{

		// load the texture
		virtual void load(spine::AtlasPage& page, const spine::String& path);

		virtual void unload(void* texture);
	};
	static CustomTextureLoader sTextureLoader;
}
