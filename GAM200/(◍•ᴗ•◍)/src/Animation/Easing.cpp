// ----------------------------------------------------------------------------
// Copyright (C)DigiPen Institute of Technology.
// Reproduction or disclosure of this file or its contents without the prior
// written consent of DigiPen Institute of Technology is prohibited.
//
// File Name:		Easing.cpp
// Purpose:			This file implemets the several functions incharged of
//					easing the time passed.
// Project:			CS230_jaime.s_4_3
// Author:			Jaime Sanchez
// Student ID:		540001319
// Login:			jaime.s@digipen.edu
// ----------------------------------------------------------------------------

#include "Easing.h"

float EaseLinear(float tn)
{
	return tn;
}
float EaseInQuad(float tn)
{
	return tn*tn;
}
float EaseOutQuad(float tn)
{
	return tn*(2.0f-tn);
}
float EaseInOutQuad(float tn)
{
	//Use the formula seen in class
	if (2.0f * tn < 1.0f)
	{
		return EaseInQuad(2.0f * tn)*0.5f;
		
	}
	else
	{
		return EaseOutQuad(2.0f * tn - 1.0f)*0.5f + 0.5f;
	}
	return 0.0f;
}
