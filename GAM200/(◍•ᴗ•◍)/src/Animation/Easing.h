#pragma once

typedef float(*EaseFunc)(float);

float EaseLinear(float tn);
float EaseInQuad(float tn);
float EaseOutQuad(float tn);
float EaseInOutQuad(float tn);