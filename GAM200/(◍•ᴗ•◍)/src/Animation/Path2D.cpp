// ----------------------------------------------------------------------------
// Copyright (C)DigiPen Institute of Technology.
// Reproduction or disclosure of this file or its contents without the prior
// written consent of DigiPen Institute of Technology is prohibited.
//
// File Name:		Path2D.cpp
// Purpose:			This file implemets two new structers: PathPpint2D and 
//					Path2D.				
// Project:			CS230_jaime.s_4_3
// Author:			Jaime Sanchez
// Student ID:		540001319
// Login:			jaime.s@digipen.edu
// ----------------------------------------------------------------------------

#include <iostream>

#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "../Input/Input.h"
#include "../Factory/Factory.h"
#include "../Graphics/Color/Color.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Time/Time.h"

#include "../Scene/Scene.h"
#include "../Space/Space.h"

#include "../GameObject/GameObject.h"

#include "../Transform2D/TransformComponent.h"


#include "Path2D.h"

glm::vec3 Lerp(const glm::vec3& start, const glm::vec3& end, float tn)
{
	float temp_x = start.x + (end.x - start.x) * tn; //interpolation of x
	float temp_y = start.y + (end.y - start.y) * tn; //interpolation of y

	glm::vec3 vector = { temp_x, temp_y, 0 }; //set the vector

	return vector; //return the vector
}

float Distance(const glm::vec3& p1, const glm::vec3& p2)
{
	//calculate the distance
	float distance = sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));

	//return the distance
	return distance; 
}

PathPoint2D::PathPoint2D()
{
	mTime = 0.0f;
	mPoint = glm::vec3{ 0, 0,0 };
}
PathPoint2D::PathPoint2D(float t)
{
	mTime = t;
	mPoint = glm::vec3{ 0, 0,0 };
}
PathPoint2D::PathPoint2D(float t, const glm::vec3& pt)
{
	mTime = t;
	mPoint = pt;
}

Path2D::Path2D()
{
	mName = "Path2D";
	mCummulatedTime = 0;
}
Path2D::~Path2D()
{
}
Path2D& Path2D::operator=(const Path2D& rhs)
{
	return *this;
}
Path2D* Path2D::Clone()
{
	Path2D* temp = FactorySys->Create<Path2D>();
	(*temp) = (*this);
	return temp;
}

void Path2D::Initialize()
{
	LogicComponent::Initialize();

	mCummulatedTime = 0;

	loopDone = false;
}
void Path2D::Update()
{
	if (mPoints.size() > 1)
	{
		if (!loopDone)
			mCummulatedTime += (float)TimeSys->GetFrameTime();
		else
			mCummulatedTime -= (float)TimeSys->GetFrameTime();

		std::cout << mCummulatedTime << std::endl;

		mOwner->GetComp<TransformComponent>()->SetWorldPosition(SampleTime(mCummulatedTime));
		if (mCummulatedTime >= mPoints[mPoints.size() - 1].mTime && loopDone == false)
		{
			loopDone = true;
		}

		if (mCummulatedTime <= 0.0f)
		{
			mCummulatedTime = 0;
			loopDone = false;
		}
	}
}
void Path2D::Shutdown()
{
	LogicComponent::Shutdown();
}
bool Path2D::Edit()
{
	bool changed = false;


	for (int i = 0; i < (int)mPoints.size() - 1; i++)
	{
		if (mPoints.size() > 1)
			SceneSys->GetMainSpace()->DrawLine(mPoints[i].mPoint, mPoints[i + 1].mPoint, Colors::blue);
	}
	for (auto& point : mPoints)
	{
		SceneSys->GetMainSpace()->DrawCircle(point.mPoint, 5.0f, Colors::blue);
	}

	auto mouse_pos = InputSys->get_current_mouse_position();

	mouse_pos = RndMngr->ProjToWindowInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->EditorCamToProjInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->WorldToEditorCamInv(glm::vec3(mouse_pos, 0.0f));

	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted("Press left_shit + left_click to create a new point.");
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
	ImGui::SameLine();
	if (ImGui::CollapsingHeader("Add Points"))
	{
		if (InputSys->key_is_triggered(keys::LeftShift) && InputSys->mouse_is_down(mouse_buttons::Left_click))
		{
			Push(mLastRegisteredTime, glm::vec3(mouse_pos, 0.0f));
			mLastRegisteredTime += 1.0f;
			changed = true;
		}
	}
	if (ImGui::Begin("List of points", nullptr, 0))
	{


		for (unsigned i = 0; i < mPoints.size(); i++)
		{
			ImGui::PushID(&mPoints[i]);
			if (ImGui::CollapsingHeader(std::to_string(i).c_str(), 0))
			{
				ImGui::DragFloat("X", &(mPoints[i].mPoint.x));
				if (ImGui::IsItemDeactivatedAfterEdit())
					changed = true;
				ImGui::DragFloat("Y", &(mPoints[i].mPoint.y));
				if (ImGui::IsItemDeactivatedAfterEdit())
					changed = true;
				if (i > 0)
				{
					ImGui::SliderFloat("Time", &(mPoints[i].mTime), mPoints[i - 1].mTime, 100.0f);
					if (ImGui::IsItemDeactivatedAfterEdit())
						changed = true;
				}
			}
			ImGui::PopID();
		}
		ImGui::End();
	}
	else
		ImGui::End();
	return changed;
}

void Path2D::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	IBase::ToJson(j);

	j["Total Points: "] = mPoints.size();

	json& temp_points = j["Points"];
	for (auto& point : mPoints)
	{
		temp_points["Point"].push_back(point.mPoint.x);
		temp_points["Point"].push_back(point.mPoint.y);
		temp_points["Point"].push_back(point.mPoint.z);
		temp_points["time"].push_back(point.mTime);
	}
}
void Path2D::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	unsigned TotalPoints;
	if (j.find("Total Points: ") != j.end())
	{
		mPoints.clear();
		TotalPoints = j["Total Points: "];
		if (j.find("Points") != j.end())
		{
			for (unsigned i = 0; i < TotalPoints * 3; i += 3)
			{
				mLastRegisteredTime = j["Points"]["time"][i / 3];
				Push(j["Points"]["time"][i / 3], glm::vec3(j["Points"]["Point"][i], j["Points"]["Point"][i + 1], j["Points"]["Point"][i + 2]));
			}
		}
	}
}
glm::vec3 Path2D::SampleTime(float t) const
{
	float NormParam; //normalized parameter

	glm::vec3 Segment; //segment

	float Ti; //determined time in a position in mPoints
	float NextTi; //the next of Ti

	glm::vec3 Pi; //determined point in mPoints
	glm::vec3 NextPi; //the next of Pi

	//if the size is 0, return (0,0) vector
	if (mPoints.size() == 0)
		return glm::vec3{ 0,0,0 };

	//if the size is 1, return the only point in the array
	if (mPoints.size() == 1)
		return mPoints[0].mPoint;

	//if the time is negative, return the first point
	if (t <= 0)
		return mPoints[0].mPoint;

	//if the times is grater than the time of the last point, return the last point
	if (t > mPoints[mPoints.size() - 1].mTime)
		return mPoints[mPoints.size() - 1].mPoint;

	//go along the mPoints array
	for (auto it = mPoints.begin(); it != mPoints.end(); it++)
	{
		//if the given time is in the interval
		if (it->mTime <= t && (it + 1)->mTime >= t)
		{
			Ti = it->mTime; //set Ti
			NextTi = (it + 1)->mTime; //set NextTi

			Pi = it->mPoint; //set Pi
			NextPi = (it + 1)->mPoint; //set NextPi

			Segment = NextPi - Pi; //set the segment

			break; //get out of the loop
		}
	}

	NormParam = (t - Ti) / (NextTi - Ti); //comput the normalized parameter

	Segment = Lerp(Pi, NextPi, NormParam);

	return Segment; //return the linear interpolation
}
glm::vec3 Path2D::SampleDistance(float dist) const
{
	//If there are no points, just pass a dummt point
	if (mPoints.size() == 0)
		return glm::vec3{ 0,0,0 };

	//If there is only one point, just pass that point
	else if (mPoints.size() == 1)
		return (*mPoints.begin()).mPoint;

	//If the distance is 0 or less, we are in the first point
	else if (dist <= 0.0f)
		return (*(mPoints.begin())).mPoint;

	else
	{
		std::vector<PathPoint2D>::const_iterator it1 = mPoints.begin();
		std::vector<float>::const_iterator it2 = mCummulatedLengths.begin();
		//Iterate through the lengths list
		for (; it2 != mCummulatedLengths.end(); it2++, it1++)
		{
			//If we find the exact distance, return the point corresponding to that distance
			if (dist == *it2)
				return (*(it1 + 1)).mPoint;
			//If we find a bigger time, we are in the segment
			if (dist < *it2)
			{
				//Check if we are taking the first component of the vector
				if (it2 == mCummulatedLengths.begin())
				{
					//Calculate the normalized parameter for the lerping function
					float u = dist / *it2;
					return Lerp((*it1).mPoint, (*(it1 + 1)).mPoint, u);
				}
				else
				{
					//Take a step backwords to be at the stating point of the segment
					it2--;
					//Calculate the normalized parameter for the lerping function
					float u = (dist - *it2) / (*(it2 + 1) - *it2);
					return Lerp((*it1).mPoint, (*(it1 + 1)).mPoint, u);
				}
			}
		}
		//If we didn't find the segment in the vector, just return the last point
		return (*(--mPoints.end())).mPoint;
	}
}
glm::vec3 Path2D::SampleParameter(float u) const
{
	return SampleDistance(GetLengthFromParam(u));
}


void Path2D::Push(float t, const glm::vec3 &val)
{
	//Push the point passed to the end of the vector
	mPoints.push_back(PathPoint2D(t,val));
	//Get the iterators to the start of the lists
	std::vector<PathPoint2D>::iterator it1 = mPoints.begin();
	std::vector<float>::iterator it2 = mCummulatedLengths.begin();
	//If there are only two points, just add at the beginning of the lengths list the distance between the points
	if (mPoints.size() == 2)
	{
		glm::vec3 point = (*(it1)).mPoint;
		mCummulatedLengths.push_back(Distance(point, (*(it1)).mPoint));
	}
	//If the are more than one points
	else if (mPoints.size() > 2)
	{
		//Go to the end of the vector
		while (it2 != mCummulatedLengths.end())
		{
			it2++;
			it1++;
		}

		glm::vec3 point = (*(it1)).mPoint;

		//Push to the back of the lengths list the distance between the two last points, plus the previous total distance
		mCummulatedLengths.push_back(Distance(point, (*(it1 + 1)).mPoint) + *(--it2));
	}
}
void Path2D::Clear()
{
	mPoints.clear();
	mCummulatedLengths.clear();
}

unsigned int Path2D::Size()const
{
	return (unsigned)mPoints.size();
}
float Path2D::Duration()const
{
	if (mPoints.empty())
		return 0.0f;
	else
	{
		std::vector<PathPoint2D>::const_iterator it = mPoints.end();
		it--;
		return (*it).mTime;
	}
}
float Path2D::GetTotalLength()const
{
	if (mPoints.size() < 2)
		return 0.0f;
	else
	{
		std::vector<float>::const_iterator it = mCummulatedLengths.end();
		it--;
		return *it;
	}

}
float Path2D::GetLengthFromParam(float u)const
{
	return GetTotalLength()*u;
}