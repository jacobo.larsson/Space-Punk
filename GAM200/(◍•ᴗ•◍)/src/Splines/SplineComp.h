#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Graphics/Font/Font.h"
#include "../EventSystem/EventHandler.h"
#include "../StateMachines/StateMachine/StateMachine.h"
#include "../StateMachines/StateMachine/StateMachine.h"
#include "../StateMachines/BrainComponent/BrainComponent.h"

struct SplinePoint
{
	SplinePoint(glm::vec3 to_point)
	{
		point = to_point;
	}

	SplinePoint(glm::vec3 to_point, float distance_to_check)
	{
		point = to_point;
		distance = distance_to_check;
	}
	glm::vec3 point;
	float distance;
};

class SplineComp : public LogicComponent
{
	SM_RTTI_DECL(SplineComp, LogicComponent);
public:

	SplineComp();
	SplineComp(SplineComp& rhs);
	~SplineComp();
	SplineComp& operator=(const SplineComp& rhs);
	SplineComp* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;


	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;

	glm::vec3 ComputeSplinePath(float t);
	float SplineSegmentLength(int node_to_compute);
	float GetNormalizedOffSet(float p);

	std::vector<SplinePoint> spline_points;
	int selected_point = -1;
	float total_length;
};


