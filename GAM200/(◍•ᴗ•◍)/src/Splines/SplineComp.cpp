#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"
#include "../AssertSystem/AssertSystem.h"

#include "../Factory/Factory.h"
#include "../PropertySystem/PropertyMap.h"
#include "../Changer/Changer.h"
#include "../Input/Input.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Audio/SoundEmitter.h"
#include "../Transform2D/TransformComponent.h"
#include "../Collisions/Collisions.h"
#include "../Scene/Scene.h"
#include "../MainEngine/MainEngine.h"
#include "../Game/CameraFollow/CameraMachine.h"
#include "SplineComp.h"

SplineComp::SplineComp()
{
	mName = "SplineComp";

}
SplineComp::SplineComp(SplineComp& rhs)
{


}
SplineComp::~SplineComp()
{
	spline_points.clear();

}
SplineComp& SplineComp::operator=(const SplineComp& rhs)
{

	return *this;
}
SplineComp* SplineComp::Clone()
{
	SplineComp* temp = FactorySys->Create<SplineComp>();
	(*temp) = (*this);
	return temp;

}

void SplineComp::OnCreate()
{

}
void SplineComp::Initialize()
{
	LogicComponent::Initialize();

}
void SplineComp::Update()
{
	static int frame_counter = 0;
	static float objectpos = 0.0f;
	static bool start = false;
	if (InputSys->key_is_down(keys::M))
	{
		start = true;
	}
	if (start)
	{
		if (spline_points.size() > 3)
		{
			int check = 0;
			for (float t = 0.0f; t < (float)spline_points.size() - 3.01f;)
			{
				glm::vec3 initi_point = ComputeSplinePath(t);
				t += 0.01f;

				glm::vec3 final_point = ComputeSplinePath(t);
				t += 0.01f;
				SceneSys->GetMainSpace()->DrawLine(glm::vec2(initi_point.x, initi_point.y), glm::vec2(final_point.x, final_point.y), Colors::red);
				check++;
			}
			

		}

		if (objectpos <total_length)
		{
			/*glm::vec3 spp= ComputeSplinePath(objectpos);
			SceneSys->GetMainSpace()->DrawCircle(spp, 5.0f, Colors::red);
			spp.z = 20.f;
			CameraMachine* mach = reinterpret_cast<CameraMachine*>(SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));
			mach->mCameraTransform->SetWorldPosition(spp);
			objectpos += 0.01f;*/
			float off = GetNormalizedOffSet(objectpos)-1;
			if (off < (float)(spline_points.size()-3))
			{
				glm::vec3 spp = ComputeSplinePath(off);
				SceneSys->GetMainSpace()->DrawCircle(spp, 5.0f, Colors::red);
				spp.z = 20.f;
				CameraMachine* mach = reinterpret_cast<CameraMachine*>(SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<BrainComponent>()->GetStateMachine("CameraMachine"));
				mach->mCameraTransform->SetWorldPosition(spp);
				objectpos += 5.f;
			}

		}
		else
		{
			objectpos =0.f;
		}
		
	}
}
void SplineComp::Shutdown()
{
	LogicComponent::Shutdown();

}


void SplineComp::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	IBase::ToJson(j);

	j["Total Length: "] = total_length;
	j["Total Points: "] = spline_points.size();

	json& temp_points = j["Points"];
	for (auto& to_point : spline_points)
	{
		temp_points["Point"].push_back(to_point.point.x);
		temp_points["Point"].push_back(to_point.point.y);
		temp_points["Point"].push_back(to_point.point.z);
		temp_points["Distance"].push_back(to_point.distance);
	}

}
void SplineComp::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	unsigned TotalPoints;

	if (j.find("Total Length: ") != j.end())
	{
		total_length = j["Total Length: "];
	}

	if (j.find("Total Points: ") != j.end())
	{
		spline_points.clear();
		TotalPoints = j["Total Points: "];
		if (j.find("Points") != j.end())
		{
			for (unsigned i = 0; i < TotalPoints * 3; i += 3)
			{
				glm::vec3 newpos;
				newpos.x = j["Points"]["Point"][i];
				newpos.y = j["Points"]["Point"][i+1];
				newpos.z = j["Points"]["Point"][i+2];
				float dis = j["Points"]["Distance"][i/3];
				SplinePoint to_add  = SplinePoint(newpos, dis);
				spline_points.push_back(to_add);
				
			}
		}
	}
}


bool SplineComp::Edit()
{
	bool changed = false;
	auto mouse_pos = InputSys->get_current_mouse_position();

	mouse_pos = RndMngr->ProjToWindowInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->EditorCamToProjInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->WorldToEditorCamInv(glm::vec3(mouse_pos, 0.0f));

	for (auto it = spline_points.begin(); it != spline_points.end(); it++)
	{
		int distance = std::distance(spline_points.begin(), it);
		
		if (distance == selected_point)
		{
			SceneSys->GetMainSpace()->DrawCircle(it->point, 5.0f, Colors::red);
		}
		else
		{
			SceneSys->GetMainSpace()->DrawCircle(it->point, 5.0f, Colors::blue);
		}
		if (StaticPointToStaticCircle(glm::vec3(mouse_pos,0.f),it->point,5.f))
		{
			if (InputSys->mouse_is_triggered(mouse_buttons::Left_click))
				selected_point = distance;
		}

	}


	if (selected_point >= 0)
	{
		if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->mouse_is_down(mouse_buttons::Right_click))
		{

			spline_points[selected_point] = glm::vec3(mouse_pos, 0.f);

		}

	}
	

	if (InputSys->key_is_triggered(keys::LeftShift) && InputSys->mouse_is_triggered(mouse_buttons::Left_click))
	{
		selected_point = spline_points.size();
		spline_points.push_back(glm::vec3(mouse_pos, 0.0f));
		changed = true;
	}
	

	if (spline_points.size() > 3)
	{
		float length_total_computation = 0.f;
		for (int i = 1; i < spline_points.size()-2; i++)
		{
			
			spline_points[i].distance = SplineSegmentLength(i-1);
			length_total_computation += spline_points[i].distance;
		}
		total_length = length_total_computation;

		for (float t = 0.0f; t < (float)spline_points.size() - 3.001f;)
		{
			glm::vec3 initi_point = ComputeSplinePath(t);
			t += 0.001f;

			glm::vec3 final_point = ComputeSplinePath(t);
			t += 0.001f;

			SceneSys->GetMainSpace()->DrawLine(glm::vec2(initi_point.x, initi_point.y), glm::vec2(final_point.x, final_point.y), Colors::red);
		}
	}

	if (ImGui::Begin("List of points", nullptr, 0))
	{


		for (unsigned i = 0; i < spline_points.size(); i++)
		{
			ImGui::PushID(&spline_points[i]);
			if (ImGui::CollapsingHeader(std::to_string(i).c_str(), 0))
			{
				selected_point = i;
				ImGui::DragFloat("X", &(spline_points[i].point.x));
				if (ImGui::IsItemDeactivatedAfterEdit())
					changed = true;
				ImGui::DragFloat("Y", &(spline_points[i].point.y));
				if (ImGui::IsItemDeactivatedAfterEdit())
					changed = true;
				
				ImGui::Text("Length is %f", spline_points[i].distance);

			}
			ImGui::PopID();
		}
		ImGui::End();
	}

	

	return changed;

}

glm::vec3 SplineComp::ComputeSplinePath(float t)
{
	glm::vec3 to_return = glm::vec3();
	if (spline_points.size() > 3)
	{
		int p1 = (float)glm::floor(t) + 1;
		int p2 = p1 + 1;
		int p3 = p2 + 1;
		int p0 = p1 - 1;

		t = t - (float)glm::floor(t);

		float tsq = t * t;
		float tcubic = tsq * t;


		float q1 = -tcubic + 2.0f * tsq - t;
		float q2 = 3.0f * tcubic - 5.0f * tsq + 2.0f;
		float q3 = -3.0f * tcubic + 4.0f * tsq + t;
		float q4 = tcubic - tsq;


		float pointx = 0.5f * (spline_points[p0].point.x * q1 + spline_points[p1].point.x * q2 + spline_points[p2].point.x * q3 + spline_points[p3].point.x * q4);
		float pointy = 0.5f * (spline_points[p0].point.y * q1 + spline_points[p1].point.y * q2 + spline_points[p2].point.y * q3 + spline_points[p3].point.y * q4);

		to_return.x = pointx;
		to_return.y = pointy;
	}


	return to_return;
}

float SplineComp::SplineSegmentLength(int node_to_compute)
{
	float total_length = 0.f;
	float to_add = 0.01f;

	glm::vec3 first_point = ComputeSplinePath((float)node_to_compute);
	glm::vec3 last_point;

	for (float t = 0.f; t < 1.0f; t += to_add)
	{
		last_point = ComputeSplinePath((float)node_to_compute + t);
		glm::vec2 to_length = glm::vec2(last_point.x - first_point.x, last_point.y - first_point.y);

		total_length = total_length + sqrtf(to_length.x * to_length.x + to_length.y * to_length.y);
		first_point = last_point;
	}

	return total_length;
}

float SplineComp::GetNormalizedOffSet(float p)
{
	if (p < total_length)
	{
		int i = 1;
		while (p > spline_points[i].distance)
		{
			p -= spline_points[i].distance;
			i++;
		}


		return (float)i + (p / spline_points[i].distance);
	}
}
