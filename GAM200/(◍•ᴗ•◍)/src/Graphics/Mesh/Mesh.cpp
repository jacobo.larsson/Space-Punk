/**********************************************************************************/
/*!
\file   Mesh.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Mesh.h
*/
/*********************************************************************************/

#include "../Color/Color.h"
#include "Mesh.h"

Mesh::Mesh()
{
	CreateOpenglMesh();
}
Mesh::Mesh(EPrimitiveType prim) : mPrimitiveType(prim)
{
	CreateOpenglMesh();
}

const GLuint Mesh::GetVAO()
{
	return VAO;
}
const GLuint Mesh::GetVBO()
{
	return VBO;
}
std::vector<unsigned>& Mesh::GetIndices()
{
	return indices;
}
std::vector<Vertex>& Mesh::GetVertices()
{
	return vertices;
}

void Mesh::UploadVertexFormat()
{
	GLenum error = glGetError();

	// bind our buffer
	Bind();
	//glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	error = glGetError();
	// vertex texture coords
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, UV));
	error = glGetError();
	// vertex colors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	error = glGetError();

	Unbind();
}
void Mesh::UploadToGPU()
{
	GLenum error = glGetError();
	Bind();

	// Tell to openGL the layout, size of our buffer and how it must be draw
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
	error = glGetError();


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
		indices.data(), GL_STATIC_DRAW);
	error = glGetError();

	Unbind();
}
void Mesh::ReUploadToGPU()
{
	Bind();
	// Tell to openGL the layout, size of our buffer and how it must be draw
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), vertices.data());


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices.size() * sizeof(unsigned int),
		indices.data());

	Unbind();
}

void Mesh::Bind() {
	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
}
void Mesh::Unbind() {
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Mesh::Draw(int count)
{
	Bind();

	if (count == -1)
		count = int(indices.size());

	GLenum primType = GL_TRIANGLES;
	switch (mPrimitiveType) {
	case EPrimitiveType::eLineList: primType = GL_LINES; break;
	}

	glDrawElements(primType, (GLsizei)count, GL_UNSIGNED_INT, 0);

	Unbind();
}
void Mesh::DrawInstanced(int instance_count, int count)
{
	GLenum error = glGetError();
	Bind();

	if (count == -1)
		count = int(indices.size());

	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLenum primType = GL_TRIANGLES;
	switch (mPrimitiveType) {
	case EPrimitiveType::eLineList: primType = GL_LINES; break;
	}
	error = glGetError();

	glDrawElementsInstanced(primType, count, GL_UNSIGNED_INT, 0, instance_count);
	//glDrawElements(primType, (GLsizei)count, GL_UNSIGNED_INT, 0);
	error = glGetError();
	glDepthMask(GL_TRUE);
	Unbind();
}

void Mesh::Clear()
{
	vertices.clear();
	indices.clear();
}

void Mesh::create_quad(Mesh& outMesh)
{
	glm::vec3 corners[4] = { glm::vec3(-0.5f, 0.5f, 0.0f), glm::vec3(0.5f, 0.5f, 0.0f),
						 glm::vec3(-0.5f, -0.5f, 0.0f),glm::vec3(0.5f, -0.5f, 0.0f) };


	// Unused texture coordinates
	glm::vec2 UV[4] = { glm::vec2{0.0f,1.0f}, glm::vec2{1.0f,1.0f},
						glm::vec2{0.0f,0.0f}, glm::vec2{1.0f,0.0f} };

	// Color for all the vertices
	Color colors[4] = { Color(1.0f, 0.0f, 0.0f, 1.0f), Color(0.0f, 1.0f, 0.0f, 1.0f),
						Color(0.0f, 0.0f, 1.0f, 1.0f), Color(1.0f, 1.0f, 1.0f, 1.0f) };

	outMesh.vertices.clear();
	outMesh.indices.clear();

	// First triangle
	outMesh.AddIndex(1);
	outMesh.AddIndex(0);
	outMesh.AddIndex(2);
	// Second triangle
	outMesh.AddIndex(1);
	outMesh.AddIndex(2);
	outMesh.AddIndex(3);

	// Add the vertices of the plane
	for (unsigned i = 0; i < 4; i++)
		outMesh.AddVertex(Vertex(corners[i], UV[i], colors[i]));

	outMesh.UploadToGPU();
}

const Vertex& Mesh::AddVertex(const Vertex& new_ver)
{
	vertices.push_back(new_ver);
	return new_ver;
}
const unsigned Mesh::AddIndex(const unsigned new_index)
{
	indices.push_back(new_index);
	return new_index;
}

Vertex* Mesh::GetVertex(int offset)
{
	// Sanity Check
	if (offset >= GetVertexCount())
		return NULL;

	// return vertex
	return (vertices.data() + offset);
}
unsigned Mesh::GetIndicesCount()
{
	return unsigned(indices.size());
}

void Mesh::SetVertexPos(int offset, glm::vec3 pos)
{
	if (Vertex* vtx = GetVertex(offset))
		vtx->position = pos;
}
void Mesh::SetVertexTex(int offset, glm::vec2 tex)
{
	if (Vertex* vtx = GetVertex(offset))
		vtx->UV = tex;
}
void Mesh::SetVertexColor(int offset, glm::vec4 col)
{
	if (Vertex* vtx = GetVertex(offset))
		vtx->color = col;
}
int Mesh::GetVertexCount()
{
	return int(vertices.size());
}

void Mesh::CreateOpenglMesh()
{
	if (VAO) {
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
	}

	// Generate buffers for all the variables in the mesh
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	UploadVertexFormat();
}