/**********************************************************************************/
/*!
\file   Mesh.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing functions necessary to create a mesh depending on the specified type
(triangle, line and quad)
*/
/*********************************************************************************/
#pragma once
#include <vector>
#include <GL/glew.h>

#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IBase.h"

#include "../Vertex/Vertex.h"

using GLuint = unsigned;

class Mesh: public IBase
{
	friend class RenderManager;
	SM_RTTI_DECL(Mesh, IBase)
public:
	enum class EPrimitiveType
	{
		eLineList,
		eTriangleList,
		eQuadList,
		eFontList
	};

	// Primitive types
	EPrimitiveType GetPrimitiveType() const { return mPrimitiveType; }
	EPrimitiveType GetPrimitiveType() { return mPrimitiveType; }
	void SetPrimitiveType(EPrimitiveType primType) { mPrimitiveType = primType; }

	Mesh();
	Mesh(EPrimitiveType prim);


	const GLuint GetVAO();
	const GLuint GetVBO();
	std::vector<unsigned>& GetIndices();
	std::vector<Vertex>& GetVertices();

	
	void UploadVertexFormat();
	void UploadToGPU();
	void ReUploadToGPU();

	void Bind();
	void Unbind();
	void Draw(int count = -1);
	void DrawInstanced(int instance_count, int element_count = -1);

	void Clear();

	static void create_quad(Mesh& outMesh);

	const Vertex& AddVertex(const Vertex& new_ver);
	const unsigned AddIndex(const unsigned new_index);

	Vertex* GetVertex(int offset);
	unsigned GetIndicesCount();

	void SetVertexPos(int offset, glm::vec3 pos);
	void SetVertexTex(int offset, glm::vec2 tex);
	void SetVertexColor(int offset, glm::vec4 col);

	int GetVertexCount();

	//static void create_triangle(Mesh& outMesh);
	//static void create_line(Mesh& outMesh);

private:

	// Primitive type
	EPrimitiveType		mPrimitiveType = EPrimitiveType::eTriangleList;

	// Vector editors
	void CreateOpenglMesh();

	GLuint VAO = 0;
	GLuint VBO = 0;
	GLuint EBO = 0;

	std::vector<Vertex> vertices;
	std::vector<unsigned> indices;
};