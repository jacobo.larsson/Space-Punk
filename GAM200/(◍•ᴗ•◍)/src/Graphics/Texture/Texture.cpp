﻿/**********************************************************************************/
/*!
\file   Texture.cpp
\author Maria Bolaños
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Texture.h
*/
/*********************************************************************************/

#define STB_IMAGE_IMPLEMENTATION
#include "../../Extern/STB/stb_image.h"
#include "../../Extern/STB/stb_image_write.h"

#include "Texture.h"

Texture::Texture()
{
	mWidth = 0;
	mHeight = 0;
	mPixels = 0;
	mGLHandle = 0;
}

Texture::Texture(const char* _Filename)
{
	file_path = _Filename;
}

// Destructor
Texture::~Texture()
{
}

// Upload the texture data to openGL
void Texture::UploadToGPU()
{
	if (mGLHandle && (mWidth * mHeight) != 0)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, reinterpret_cast<void*>(mPixels));
	}
}
void Texture::DeleteFromGPU()
{
	if (mGLHandle)
	{
		Bind();
		glDeleteTextures(1, &mGLHandle);
		Unbind();
	}
}
void Texture::Bind()
{
	if (mGLHandle)
		glBindTexture(GL_TEXTURE_2D, mGLHandle);
}
void Texture::Unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint & Texture::GetWidth()
{
	return mWidth;
}

GLuint & Texture::GetHeight()
{
	return mHeight;
}

char* Texture::GetPixel(unsigned x, unsigned y)
{
	if (x < mWidth && y < mHeight)
	{
		return mPixels + ((mWidth * y + x) * 4u);
	}
	return nullptr;
}

char* Texture::GetPixels()
{
	return mPixels;
}

GLuint & Texture::GetGLHandle()
{
	return mGLHandle;
}

void Texture::SetWidth(unsigned _Width)
{
	mWidth = _Width;
}

void Texture::SetHeight(unsigned _Height)
{
	mHeight = _Height;
}

// Private member functions
// Creates the texture in OpenGL
void Texture::CreateOpenGLTexture()
{
	DeleteFromGPU();

	// Create texture and bind it
	glGenTextures(1, &mGLHandle);
	glBindTexture(GL_TEXTURE_2D, mGLHandle);
	//check_gl_error();

	// set texture look-up parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//check_gl_error();
}
void Texture::FreeData()
{
	if (mPixels) {
		delete[] mPixels;
		mPixels = NULL;
	}
}
