/**********************************************************************************/
/*!
\file   Texture.h
\author Maria Bola�os
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the ability to load textures from files, bind and upload them.
*/
/*********************************************************************************/
#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"


class Texture : public IBase
{
	SM_RTTI_DECL(Texture, IBase)

public:

	// constructor
	Texture();
	Texture(const char* _Filename);

	// Destructor
	virtual ~Texture();

	void CreateOpenGLTexture();
	void UploadToGPU();

	void FreeData();
	void DeleteFromGPU();

	void Bind();
	void Unbind();

	// gettors 
	GLuint & GetWidth(); 
	GLuint & GetHeight(); 
	char* GetPixel(unsigned x = 0, unsigned y = 0); 
	char* GetPixels();
	GLuint & GetGLHandle();

	// Settors
	void SetWidth(unsigned _Width);
	void SetHeight(unsigned _Height);


	char* mPixels; // pixels in RGBA format

private:
	unsigned mWidth;
	unsigned mHeight;
	GLuint	mGLHandle;// openGL handle
	std::string file_path;

	// Creates the texture in OpenGL
};