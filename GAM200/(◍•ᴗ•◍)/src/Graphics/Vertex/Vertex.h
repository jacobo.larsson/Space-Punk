/**********************************************************************************/
/*!
\file   Vertex.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing a Vertex struct with position, uv and color
*/
/*********************************************************************************/
#pragma once
#include <glm/glm.hpp>
#include "../Color/Color.h"

// Convenient struct to work with OpenGL.
// Later it might get more attributes which will have to be
// properly declarated in Mesh.cpp/UploadVertexFormat
struct Vertex
{
	Vertex()
			:position(glm::vec3(0)), UV(glm::vec2(0)), color(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)){}
	
	Vertex(glm::vec3 pos, glm::vec2 tex, glm::vec4 col)
		:position(pos), UV(tex), color(col) {}
	
	Vertex(glm::vec3 pos, glm::vec2 tex, Color col)
		:position(pos), UV(tex), color(col) {}

	glm::vec3 position;
	glm::vec2 UV;
	glm::vec4 color;
};