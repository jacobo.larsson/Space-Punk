/**********************************************************************************/
/*!
\file   Window.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions necessary for the creation of the window
*/
/*********************************************************************************/

#include <Windows.h>
#include <iostream>
#include <GL/glew.h>
#include "../OGLDebug/OGLDebug.h"

#include "../../Time/Time.h"
#include "../../EventSystem/EventDispatcher.h"
#include "../../EventSystem/Events/WindowEvents/WindowEvents.h"
#include "../../Settings/Settings.h"
#include "Window.h"


Window* Window::instance = nullptr;

Window::Window() :mWindow{ nullptr }, mContext{nullptr}
{}
/*! \fn     Initialize
	\brief  
*/
bool Window::Initialize()
{
	return CreateNewWindow(mWidth, mHeight);
}

/*! \fn     Shutdown
	\brief  
*/
void Window::Shutdown()
{
	SDL_GL_DeleteContext(mContext);
	SDL_DestroyWindow(mWindow);
}
/*! \fn     Update
	\brief  This function just updates the title of the window
*/
void Window::Update()
{
	SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
	std::string str = "Laser Soul - FrameRate: ";
	str += std::to_string( TimeSys->GetFrameRate());
	SDL_SetWindowTitle(mWindow, str.c_str());
}

SDL_Window* Window::GetWindow()
{
	return mWindow;
}

SDL_GLContext& Window::GetContext()
{
	return mContext;
}

int Window::GetWidth()
{
	return mWidth;
}

int Window::GetHeight()
{
	return mHeight;
}

glm::uvec2 Window::GetSize()
{
	return glm::uvec2(GetWidth(), GetHeight());
}


glm::mat4 Window::NDCToWindow()
{
	return glm::mat4{ GetWidth() / 2,0,0,0,
					  0, GetHeight() / 2,0,0,
					  0,0,1,0,
					  0,0,0,1 };
}

glm::mat4 Window::NDCToWindowInv()
{
	return glm::inverse(NDCToWindow());
}

void Window::ToggleFullScreen(bool b)
{
	if (b)
	{
		SDL_SetWindowFullscreen(mWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
		SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
		Sleep(500);
	}
	else
	{
		SDL_SetWindowFullscreen(mWindow, 0);
		SDL_SetWindowSize(mWindow, (int)SettingsSys->current_res.x, (int)SettingsSys->current_res.y);
		SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
		Sleep(500);
	}
	EventSys->trigger_event(FullScreenToggled(b));
}

void Window::ChangeResolution(glm::vec2 resolution)
{
	if (SettingsSys->fullscreen)
	{
		ToggleFullScreen(false);
		SettingsSys->fullscreen = false;
	}
	else
	{
		SDL_SetWindowSize(mWindow, (int)resolution.x, (int)resolution.y);
		SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
		Sleep(500);
	}
	EventSys->trigger_event(ResolutionChanged((int)resolution.x, (int)resolution.y));
}

/*! \fn     CreateNewWindow
	\brief  
*/
bool Window::CreateNewWindow(unsigned _width, unsigned _height)
{
	// Check if there is already a window created

	if (mWindow != nullptr)
		return false;
	mWidth = _width;
	mHeight = _height;

	// Initializing SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Failure initializing the window" << std::endl;
		return false;
	}
	mWindow = SDL_CreateWindow("GAM200", 100, 100, _width, _height, SDL_WINDOW_OPENGL);
	if (mWindow == nullptr)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	_renderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_PRESENTVSYNC);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	
	SDL_SetWindowResizable(mWindow, SDL_TRUE);

	mContext = SDL_GL_CreateContext(mWindow);

	// create a context for OpenGL
	if (mContext == nullptr)
	{
		SDL_DestroyWindow(mWindow);
		std::cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}

	// Initializing Glew
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		SDL_GL_DeleteContext(mContext);
		SDL_DestroyWindow(mWindow);
		std::cout << "GLEW Error: Failed to init" << std::endl;
		SDL_Quit();
		return false;
	}

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);

	return true;
}
