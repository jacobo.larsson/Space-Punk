/**********************************************************************************/
/*!
\file   Window.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Window.h
*/
/*********************************************************************************/
#pragma once

#include <SDL2/SDL.h>
#include <glm/glm.hpp>

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../Singleton/Singleton.h"

class ResolutionChanged;

class Window : public IBase
{
	SM_RTTI_DECL(Window, IBase);
	Make_Singleton_Decl(Window)
public:

	bool Initialize();
	void Update();
	void Shutdown();

	SDL_Window* GetWindow();
	SDL_GLContext& GetContext();
	int GetWidth();
	int GetHeight();
	glm::uvec2 GetSize();

	glm::mat4 NDCToWindow();
	glm::mat4 NDCToWindowInv();

	void ToggleFullScreen(bool b);
	int mWidth = 1280;
	int mHeight = 720;

	void ChangeResolution(glm::vec2 resolution);
	SDL_Renderer* _renderer;
private:
	SDL_Window * mWindow;
	SDL_GLContext mContext;

	bool CreateNewWindow(unsigned _width, unsigned _height);
};
#define WindowSys Window::Get()
