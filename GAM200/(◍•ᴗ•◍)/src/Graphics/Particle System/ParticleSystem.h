#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <list>

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../Texture/Texture.h"
#include "../../Transform2D/Transform2D.h"
#include "../Renderable/Renderable.h"

#include <list>


class TransformComponent;

struct ParticleProperties
{
	glm::vec3 mPosition;
	glm::vec2 mOffset;
	glm::vec3 mVelocity, mVelVariation;
	glm::vec2 CircularRot;
	Color mColorBegin, mColorEnd;
	glm::vec2 mSizeBegin, mSizeEnd, mSizeVariation;
	float mRotSpeed;
	float mLifeTime = 1.0f;
	float mRadius = 0.0f; 

	bool mBlending = false;
	bool mFill = false;
	bool mCircular = false;

	glm::vec2 mInitialOffset = { 0.0f, 0.0f };

	// NEW VARIABLES
	enum EEmitterShape { ES_POINT, ES_RECT, ES_CIRCLE, ES_LINE, ES_COUNT };
	EEmitterShape mEmitShapeType;
	bool mbEmitShapeFill;
	// rect/fill-rect data
	glm::vec2 mEmitShapeOffset;
	glm::vec2 mEmitShapeSize;
	// circle data: Emit Angle limits
	float mEmitShapeAngle[2];
	// line data
	glm::vec2 mEmitShapeLineStart;	// Start position of the line emitter (with respect to the emitter transform).
	glm::vec2 mEmitShapeLineEnd;		// End position of the line emitter (with respect to the emitter transform).

};

struct Particle
{
	bool Update(float dt, glm::vec2 _Pos);
	glm::vec3 mPosition; // TODO(Maria): Change this to a vec3

	glm::vec3 mVelocity;
	glm::vec2 mOffset;
	Color mColorBegin, mColorEnd;
	Color mColor;
	glm::vec2 CircularRot;
	float mRotation = 0.0f;
	glm::vec2 mSizeBegin = glm::vec2(50);
	glm::vec2 mSizeEnd = glm::vec2(0);
	glm::vec2 mSize = glm::vec2(0);
	float mRotSpeed = 0.01f;
	float mRadius = 0.0f;

	float mLifeTime = 1.0f;
	float mLifeRemaining = 0.0f;
	float mLife = 0.0f;

	bool mAcitve = true;
	bool mPrevActive = false;
	bool mBlending = false;
	bool mFill = false;
	bool mCircular = false;
};

class ParticleEmitter : public Renderable
{
	
	RTTI_DECL_BASE(ParticleEmitter, Renderable);

public:
	ParticleEmitter();
	~ParticleEmitter() {}

	void Initialize()override;

	void Update()override;
	void Render()override;
	void Shutdown()override;

	void Emit(const ParticleProperties& ParticleProperties);

	void SetParticles(unsigned int numb) { mNumberOfParticles = numb;  mIndex = mNumberOfParticles - 1; }
	void InitializeProps();

	ParticleEmitter* Clone();

	bool Edit() override;

	void operator=(ParticleEmitter& part);

	void ToJson(nlohmann::json& j);
	void FromJson(const nlohmann::json& j);

	void Save(std::string to_save);
	void Load(std::string to_save);

	ParticleProperties props;

	int mNumberOfParticles = 500;
	int mIndex = mNumberOfParticles-1;

	float time = 0.1;
	bool mActiveSystem = false;
	bool looping = false;
	Transform2D mTransform;

#pragma region Instance Rendering

	//std::list<TransformComponent*> mInstanceTransforms;

	//void AddInstance(TransformComponent* transform);
	//void RemoveInstance(TransformComponent* transform);
	//void ClearInstances();
	int InstanceCount();

	void CreateOpenGLData();
	void AttachTransformBufferToModel();
	void DetachTransformBufferFromModel();
	void UploadToGPUFromTransformComponents();
	void UploadToGPUFromTransformData();

	void PreDrawEvent();

	virtual void BindResources();
	virtual void DrawModel();
	virtual void UnBindResources();

	// Specify whether the renderable should upload on render. 
	bool mbUploadOnRender = true;
	// Specifies whether we should upload the data
	bool mbUploadTransforms = true;

	GLuint	mGLPositionBuffer = 0;	// per-instance position
	GLuint	mGLScaleRotBuffer = 0;	// per-instance scale/rot
	GLuint	mGLColorBuffer = 0;	// per-instance scale/rot
	GLuint mGLTexCoordBuffer;	// per-instance texture transforms

	std::vector< glm::vec3> mWorldPositions; // xyz-> position
	std::vector < glm::vec3> mWorldScaleRot; // x,y -> scale, z -> rot
	std::vector < glm::vec4> mWorldColor; // x,y -> scale, z -> rot

#pragma endregion


private:
	TransformComponent* mOwnerTransform;

	std::list<Particle*> mFreeList, mAliveList;
	std::vector<Particle> mParticlePool;

};