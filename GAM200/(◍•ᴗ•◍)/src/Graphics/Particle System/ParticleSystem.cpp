#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtc/constants.hpp>
#include <iostream>  
#include<string>  

#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../AssertSystem/AssertSystem.h"
#include "../../Time/Time.h"
#include "../Shaders/Shader.h"
#include "../Mesh/Mesh.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../GameObject/GameObject.h"
#include "../../OpenSaveFile/OpenSaveFile.h"
#include "../Window/Window.h"
#include "../RenderManager/RenderManager.h"
#include "../../ResourceManager/ResourceManager.h"

#include <stdlib.h>

#include "ParticleSystem.h"

#define LERP(_min, _max, _tn) ((_min) + ((_max)-(_min)) * (_tn))

// HELPER FUNCTIONS
Color Lerp(Color left, Color right, float tn)
{
	Color ret;
	ret.r = LERP(left.r, right.r, tn);
	ret.g = LERP(left.g, right.g, tn);
	ret.b = LERP(left.b, right.b, tn);
	ret.a = 1.0f;

	return ret;
}

float Random(float max = 1.0f, float min = 0.0f)
{
	return (min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min))));
}

glm::vec2 Random(float maxX, float minX, float maxY, float minY)
{
	glm::vec2 ret;
	ret.x = (minX + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (maxX - minX))));
	ret.y = (minY + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (maxY - minY))));
	return ret;
}

glm::vec2 RandomPosLine(glm::vec2 start, glm::vec2 end)
{
	//Create a temporal vector
	glm::vec2 randompos;

	//Store in that vector a random position between the starting point and the
	//ending point using linear interpolation
	randompos.x = LERP(start.x, end.x, Random());
	randompos.y = LERP(start.y, end.y, Random());

	//Return that random position
	return 	randompos;
}

glm::vec2 RandomPosRect(glm::vec2 rectOffset, glm::vec2 rectSize, bool filled)
{
	//Create a temporal vector
	glm::vec2 randompos;

	//Execute if filled is false (if we want to generate particles only in the perimeter)
	if (filled == false)
	{
		//Generate a random point in the area of the rectangle/square
		randompos = rectOffset + Random(-rectSize.x / 2, +rectSize.x / 2, -rectSize.y / 2, +rectSize.y / 2);

		//Create a vector that goes from the center of the rectangle/square to the random position in the area
		glm::vec2 origin_to_point = randompos - rectOffset;


		//Execute if the reaminder of a random number and 2 is 1
		if (rand() % 2 == 1)
		{

			//Execute if origin to point x coordinate is less or equal than zero
			if (origin_to_point.x <= 0)
			{
				//Set the random position x coordinate as the left limit of the rectangle/square
				randompos.x = rectOffset.x - (rectSize.x / 2);
			}

			//Else execute this
			else
			{
				//Set the random position x coordinate as the right limit of the rectangle/square
				randompos.x = rectOffset.x + (rectSize.x / 2);

			}

		}

		//Else execute this
		else
		{
			//Execute if origin to point y coordinate is less or equal than zero
			if (origin_to_point.y <= 0)
			{
				//Set the random position y coordinate as the bottom limit of the rectangle/square
				randompos.y = rectOffset.y - (rectSize.y / 2);
			}

			//Else execute this
			else
			{
				//Set the random position y coordinate as the top limit of the rectangle/square
				randompos.y = rectOffset.y + (rectSize.y / 2);

			}
		}
	}

	//Else execute this (if we want particles in the entire area)
	else
	{

		//Generate a random point in the area of the rectangle/square
		randompos = rectOffset + Random(-rectSize.x / 2, +rectSize.x / 2, ((-rectSize.y) / 2), +rectSize.y / 2);

	}

	//Return that random position
	return randompos;
}

glm::vec2 RandomPosCircle(glm::vec2 circlePos, glm::vec2 circleSize, float minAngle, float maxAngle, bool filled)
{
	//Create a temporal vector
	glm::vec2 randompos = glm::vec2();

	//Create a temporal angle
	float randomangle = 0;

	//Execute if filled is false (if we want to generate particles only in the perimeter)
	if (filled == false)
	{
		//Generate a random angle between the minimun and the maximun angle
		randomangle = Random(minAngle, maxAngle);

		//Generate a random position in the perimeter of the circle/ellipse
		randompos.x = circlePos.x + circleSize.x * cos(randomangle);
		randompos.y = circlePos.y + circleSize.y * sin(randomangle);
	}

	//Else execute this (if we want particles in the entire area)
	else
	{
		//Generate a random angle between the minimun and the maximun angle
		randomangle = Random(minAngle, maxAngle);

		//Generate a random position in the perimeter of the circle/ellipse
		randompos.x = circlePos.x + circleSize.x * cos(randomangle);
		randompos.y = circlePos.y + circleSize.y * sin(randomangle);

		//Call RandomPosLine using the circle/ellipse centre and the perimeter point
		//to generate a point in the circle/ellipse area
		randompos = RandomPosLine(circlePos, randompos);

	}

	//Return that random position
	return randompos;
}

/***********************************************************************/

ParticleEmitter::ParticleEmitter()
{
	SetName("ParticleEmitter");

	InitializeProps();

	mParticlePool.resize(mNumberOfParticles);
	mWorldPositions.resize(mNumberOfParticles);
	mWorldScaleRot.resize(mNumberOfParticles);
	mWorldColor.resize(mNumberOfParticles);

	mMesh = RndMngr->GetMesh("quad.mesh");
	mShader = RndMngr->GetShader("particle.shader");
	mTexture = reinterpret_cast<Resource<Texture>*>( RsrcMan->GetResource("./data/Textures/bakugo.png"));
}

void ParticleEmitter::Initialize()
{
	SetName("ParticleEmitter");

	mParticlePool.resize(mNumberOfParticles);
	mWorldPositions.resize(mNumberOfParticles);
	mWorldScaleRot.resize(mNumberOfParticles);
	mWorldColor.resize(mNumberOfParticles);
	mOwnerTransform = mOwner->GetComp<TransformComponent>();
	
	for (std::size_t i = 0; i < mParticlePool.size(); ++i)
	{
		mFreeList.push_back(mParticlePool.data() + i);
	}
	mAliveList.clear();
}

void ParticleEmitter::Update()
{
	// first thing is to emit new particles. 
	//Emit(this->props);

	//if (!mActiveSystem) return;
	glm::vec2 _Pos{ mOwner->GetComp<TransformComponent>()->GetWorldPosition() };
	float _dt = (float)TimeSys->GetFrameTime();
	for(auto it = mAliveList.begin(); it != mAliveList.end(); ++it)
	{
		bool alive = (*it)->Update(_dt, _Pos);
		if (!alive) {

			// remove from alive list and push to free list
			it = mAliveList.erase(it);

			// easy looping -> don't add to the freelist. 
			if(looping && mFreeList.size() < mParticlePool.size())
				mFreeList.push_back(*it);

			// special case for last particle erased.
			if (it == mAliveList.end())
				break;
		}
	}

	/*for (int i = 0; i < mParticlePool.size(); i++)
	{
		if (!mParticlePool[i].mAcitve)
			continue;

		float _dt = (float)TimeSys->GetFrameTime();


		// particle update
		mParticlePool[i].mLifeRemaining -= _dt;
		mParticlePool[i].mRotation = mParticlePool[i].mRotSpeed * _dt;

		if (mParticlePool[i].mCircular == 0)
		{
			mParticlePool[i].mPosition += (mParticlePool[i].mVelocity * _dt);
		}
		else
		{
			if (mParticlePool[i].mAcitve && !mParticlePool[i].mPrevActive)
			{
				mParticlePool[i].mPrevActive = true;
				mParticlePool[i].mPosition = _Pos + RandomPosCircle(mParticlePool[i].mOffset, glm::vec2{ mParticlePool[i].mRadius, mParticlePool[i].mRadius },
					mParticlePool[i].CircularRot.x, mParticlePool[i].CircularRot.y, mParticlePool[i].mFill);
			}
			mParticlePool[i].mPosition += (mParticlePool[i].mVelocity * _dt);
		}

		if (mParticlePool[i].mLifeRemaining <= 0.0f)
		{
			mParticlePool[i].mAcitve = false;
			mParticlePool[i].mPrevActive = false;
			continue;
		}

	}*/
	
}

void ParticleEmitter::Render()
{
	/*//if (!mActiveSystem) return;
	TransformComponent* trans = mOwner->GetComp<TransformComponent>();
	//trans->SetWorldPosition(trans->GetWorldTransform().mPosition + mTransform.mPosition);
	glm::vec3 translation = trans->GetWorldTransform().mPosition;
	float rot = trans->GetWorldTransform().mOrientation;
	*/
	/*if (auto transform = trans)
	{
		for (auto& particle : mParticlePool)
		{
	
			if (!particle.mAcitve)
				continue;
			

			float life = particle.mLifeRemaining / particle.mLifeTime;
			Color color = Lerp(particle.mColorEnd, particle.mColorBegin, life);
			color.w = color.w * life;
			
			// store and compute the lerp in update of particle
			glm::vec2 size = LERP(particle.mSizeEnd, particle.mSizeBegin, life);
			
			glm::vec3 transl = glm::vec3(particle.mPosition.x + mTransform.mPosition.x,
				particle.mPosition.y + mTransform.mPosition.y, mTransform.mPosition.z); // Translation matrix
			float rotation = particle.mRotation; // rotation matrix
			glm::vec3 scale = glm::vec3(size.x, size.y, 0.0f); // scale matrix
			
			mShader->SetUniform("Identity", glm::mat4(1.0f));
			mShader->SetUniform("Pos", transl);
			mShader->SetUniform("Scale", scale);
			mShader->SetUniform("Rotation", rotation);

			glEnable(GL_DEPTH_BUFFER);
			int blendEnabled = 0;
			int prevBlendSrcF, prevBlendDstF, prevBlendSrcA, prevBlendDstA;
			glGetIntegerv(GL_BLEND, &blendEnabled);
			if (blendEnabled) {
				glGetIntegerv(GL_BLEND_SRC_RGB, &prevBlendSrcF);
				glGetIntegerv(GL_BLEND_DST_RGB, &prevBlendDstF);
				glGetIntegerv(GL_BLEND_SRC_ALPHA, &prevBlendSrcA);
				glGetIntegerv(GL_BLEND_DST_ALPHA, &prevBlendDstA);
			}
			//glEnable(GL_BLEND);
			if (particle.mBlending == true)
			{
				glBlendFuncSeparate(GL_ONE, GL_ONE, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}
			else
			{
				glEnable(GL_BLEND);
				glDepthMask(GL_FALSE);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				//glBlendFuncSeparate(GL_ONE, GL_ONE, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}
			
			glActiveTexture(GL_TEXTURE0);
			if(mTexture)
			mTexture->get()->Bind();

			mShader->SetUniform("texture1", 0);
			mMesh->Draw();
			
			// restor the state
			if (blendEnabled) {
				glEnable(GL_BLEND);
				glBlendFuncSeparate(prevBlendSrcF, prevBlendDstF, prevBlendSrcA, prevBlendDstA);
			
			}
			else
				glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);

		}
	}*/

	BindResources();
	//PreDrawEvent();

	if (mbUploadOnRender || mbUploadTransforms)
		//mMesh->UploadToGPU();
		UploadToGPUFromTransformComponents();
	DrawModel();

	UnBindResources();

}

void ParticleEmitter::Shutdown()
{
	RndMngr->RemoveActiveEmitter(this);
}

void ParticleEmitter::Emit(const ParticleProperties& ParticleProperties)
{
	if (!mActiveSystem) return;
	time -= TimeSys->GetFrameTime();
	if (mIndex == 0) {
		if (looping) {
			mIndex = mNumberOfParticles - 1;
		}
		else
			return;
	}

	//Particle& particle = mParticlePool[mIndex - 1];
	if (mFreeList.size() != 0)
	{

		Particle* particle = mFreeList.back(); mFreeList.pop_back();

		particle->mAcitve = true;
		particle->mPosition.x = ParticleProperties.mPosition.x + mOwner->GetComp<TransformComponent>()->GetWorldPosition().x + mTransform.mPosition.x;
		particle->mPosition.y = ParticleProperties.mPosition.y + mOwner->GetComp<TransformComponent>()->GetWorldPosition().y + mTransform.mPosition.y;
		particle->mPosition.z = mTransform.mPosition.z;

		particle->mPosition.x += (Random(-1, 1) * ParticleProperties.mInitialOffset.x * particle->mSizeBegin.x);
		particle->mPosition.y += (Random(-1, 1) * ParticleProperties.mInitialOffset.y);

		particle->mRotation = Random() * 2.0f * 3.1415923 + mOwner->GetComp<TransformComponent>()->GetWorldOrientation();

		particle->mVelocity = ParticleProperties.mVelocity;
		particle->mVelocity.x += ParticleProperties.mVelVariation.x * (Random() - 0.5f);
		particle->mVelocity.y += ParticleProperties.mVelVariation.y * (Random() - 0.5f);

		particle->mOffset = ParticleProperties.mOffset;

		particle->mRotSpeed = ParticleProperties.mRotSpeed;

		particle->mRadius = ParticleProperties.mRadius;

		particle->mColorBegin = ParticleProperties.mColorBegin;
		particle->mColorEnd = ParticleProperties.mColorEnd;

		particle->mFill = ParticleProperties.mFill;

		particle->CircularRot = ParticleProperties.CircularRot;

		particle->mLifeTime = ParticleProperties.mLifeTime;
		particle->mLifeRemaining = ParticleProperties.mLifeTime;
		particle->mSizeBegin = ParticleProperties.mSizeBegin + ParticleProperties.mSizeVariation * (Random() - 0.5f);
		particle->mSizeEnd = ParticleProperties.mSizeEnd;

		particle->mBlending = props.mBlending;
		particle->mCircular = props.mCircular;

		mAliveList.push_back(particle);

	}
	--mIndex;//% mParticlePool.size();

}

void ParticleEmitter::InitializeProps()
{
	props.mColorBegin = { 0, 0, 0, 1.0f };
	props.mOffset = { 0,0 };
	props.mColorEnd = { 1, 1, 1, 1.0f };
	props.mSizeBegin = glm::vec2(20.0f);
	props.mSizeVariation = glm::vec2(0.3f);
	props.mSizeEnd = glm::vec2(0.0f);
	props.mLifeTime = 1.0f;
	props.mVelocity = { 0.0f, 100.0f , 0.0f };
	props.mVelVariation = { 3.0f, 1.0f, 0.0f};
	props.mPosition = { 0.0f, 0.0f, 0.0f};
	props.mRotSpeed = 0.1f;
	props.mBlending = false;
}

bool ParticleEmitter::Edit()
{
	Emit(props);
	Update();
	if (ImGui::Begin("Particle System"))
	{
		if (ImGui::BeginTabBar("Options"))
		{
			if (ImGui::BeginTabItem("Properties"))
			{
				ImGui::DragFloat3("Position", &mTransform.mPosition.x, 1.0f, -500.0f, 500.0f);

				if (ImGui::SliderInt("NumberOfParticles", &mNumberOfParticles, 5, 5000))
				{
					mIndex = mNumberOfParticles - 1;
					mParticlePool.resize(mNumberOfParticles);
					mWorldPositions.resize(mNumberOfParticles);
					mWorldScaleRot.resize(mNumberOfParticles);
					mWorldColor.resize(mNumberOfParticles);
				}

				ImGui::Checkbox("ActiveSystem", &mActiveSystem);

				if (ImGui::CollapsingHeader("Birth Color"))
					ImGui::ColorPicker4("Birth Color", &props.mColorBegin.r);
				if (ImGui::CollapsingHeader("Death Color"))
					ImGui::ColorPicker4("Death Color", &props.mColorEnd.r);

				ImGui::NewLine();
				//ImGui::ColorEdit4("Birth Color", &props.mColorBegin.r);
				//ImGui::ColorEdit4("Death Color", &props.mColorEnd.r);
				ImGui::DragFloat("Life Time", &props.mLifeTime, 0.1f, 0.0f, 1000.0f);

				ImGui::NewLine();

				ImGui::DragFloat2("InitialOffset", &props.mInitialOffset.x, 0.01f);

				ImGui::NewLine();

				ImGui::DragFloat2("Velocity_xy", &props.mVelocity.x);
				ImGui::DragFloat2("Velocity Variation_xy", &props.mVelVariation.x);

				ImGui::NewLine();

				ImGui::DragFloat("Rotation Speed", &props.mRotSpeed, 0.1f, 0.0f, 10000.0f);

				ImGui::NewLine();

				ImGui::DragFloat2("Birth Size", &props.mSizeBegin.x, 0.1f);
				ImGui::DragFloat2("Death Size", &props.mSizeEnd.x, 0.1f);
				ImGui::DragFloat2("Size Variation", &props.mSizeVariation.x, 0.1f);

				ImGui::NewLine();

				ImGui::DragFloat2("Circular Rotation", &props.CircularRot.x, 0.1f);

				ImGui::NewLine();

				ImGui::DragFloat("Angular Radius", &props.mRadius, 0.1f, 0.0f, 500.0f);

				ImGui::NewLine();

				ImGui::DragFloat2("CircularOffset", &props.mOffset.x, 0.01f);

				ImGui::NewLine();

				ImGui::Checkbox("Circules", &props.mCircular);
				ImGui::Checkbox("Blend", &props.mBlending);
				ImGui::Checkbox("Looping", &looping);
				ImGui::Checkbox("FillCircle", &props.mFill);

				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("Texture"))
			{
				if (ImGui::CollapsingHeader("Add Textures"))
				{
					bool changed = false;
					if (ImGui::Button("Add Texture"))
					{
						std::string filename; // default;
						OpenSaveFileDlg dlg;
						if (dlg.Open("Open Texture")) {
							filename = dlg.GetFiles().front();
						}

						size_t found = filename.find("data");
						if (found != std::string::npos)
						{

							std::string slash = filename.substr(found);
							filename = std::string("./") + filename.substr(found, 4) + std::string("/") + filename.substr(found + 5);
							auto extension = RsrcMan->GetExtensionOfFile(filename);
							if (extension == TypeTexture)
								RsrcMan->Load(filename);
						}
					}

					static int texIndex = std::distance(RsrcMan->Resourcesmap[TypeTexture].begin(), RsrcMan->Resourcesmap[TypeTexture].find(mTexture->RelPath));

					static std::string active_str = RsrcMan->Resourcesmap[TypeTexture].find(mTexture->RelPath)->first.c_str();

					auto it = RsrcMan->Resourcesmap[TypeTexture].begin();

					if (ImGui::BeginCombo("Texture", active_str.c_str(), 0))
					{

						for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeTexture].size(); i++, it++)
						{

							if (ImGui::Selectable(it->first.c_str(), texIndex == i))
							{
								texIndex = i;
								active_str = it->first.c_str();
								SetTexture(it->first.c_str());
								changed = true;
							}

							if (texIndex == i)
								ImGui::SetItemDefaultFocus();

							if (ImGui::IsItemHovered())
							{
								std::string text_name = it->first;
								Resource<Texture>* texture_to_get = RsrcMan->GetResourceType<Texture>(text_name);
								ImGui::BeginTooltip();
								float my_tex_w = texture_to_get->get()->GetWidth();
								float my_tex_h = texture_to_get->get()->GetHeight();
								ImVec2 uv_min = ImVec2(-1.0f, 1.0f);
								ImVec2 uv_max = ImVec2(0.0f, 0.0f);
								ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
								ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f);
								ImGui::Image((void*)(intptr_t)texture_to_get->get()->GetGLHandle(), ImVec2(my_tex_w / 15, my_tex_h / 15), uv_min, uv_max, tint_col, border_col);
								ImGui::EndTooltip();

							}
						}
						ImGui::EndCombo();
					}
				}

				if (ImGui::CollapsingHeader("Save/Load"))
				{

					static char str1[500] = "";
					static char str2[500] = "";
					static char str3[500] = "./data/Particles/";
					std::string str4;

					ImGui::PushID("Load");
					ImGui::InputText("Load", str1, 500);
					if (ImGui::Button("Apply"))
					{
						str4 += str3; str4 += str1; str4 += ".json";
						Load(str4);
						str4.clear();
					}
					ImGui::PopID();

					ImGui::PushID("Save");
					ImGui::InputText("Save", str2, 500);
					if (ImGui::Button("Apply"))
					{
						str4 += str3; str4 += str2; str4 += ".json";
						Save(str4);
						str4.clear();
					}
					ImGui::PopID();
				}

				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("EmitterShapes"))
			{
				// emitter shape
				if (ImGui::TreeNodeEx("Emitter Shape:", ImGuiTreeNodeFlags_DefaultOpen))
				{
					// Shape type
					{
						ImGui::SameLine();
						ImGui::Checkbox("Filled", &props.mbEmitShapeFill);
						static int current_item = props.mEmitShapeType;
						static const char* items[] = {
							"POINT",
							"RECT",
							"CIRCLE",
							"LINE"
						};
						if (ImGui::ListBox("", &current_item, items, 4))
							props.mEmitShapeType = (ParticleProperties::EEmitterShape)current_item;
					}

					// Emitter offset
					{
						ImGui::DragFloat2("pos offset", &props.mEmitShapeOffset.x);
						ImGui::DragFloat2("shape size", &props.mEmitShapeSize.x);

						ImGui::Separator();
						ImGui::Text("Emitter Specific:");
						ImGui::Separator();

						switch (props.mEmitShapeType)
						{
						case ParticleProperties::ES_CIRCLE: {
							ImGui::Text("shape angle (min/max)");
							glm::vec2 angle_deg = { glm::degrees(props.mEmitShapeAngle[0]), glm::degrees(props.mEmitShapeAngle[1]) };
							glm::vec2 tmp = angle_deg;

							if (ImGui::DragFloat2("", &angle_deg.x, 1.0f, 0.0f, 360.0f))
							{
								// check which has been dragged
								bool moving_x = angle_deg.x != tmp.x;

								// make sure that min is always less than max
								if (moving_x && angle_deg.x > angle_deg.y)
									angle_deg.y = angle_deg.x;

								if (!moving_x && angle_deg.y < angle_deg.x)
									angle_deg.x = angle_deg.y;

								props.mEmitShapeAngle[0] = glm::radians(angle_deg.x);
								props.mEmitShapeAngle[1] = glm::radians(angle_deg.y);
							}
						}break;
						case ParticleProperties::ES_LINE: {
							ImGui::Text("Line Endpoints (start/end)\nYou can also press CTRL + Left/Right\nmouse buttons to set in the scene\nNote:position offset is ignored");
							ImGui::DragFloat2("start", &props.mEmitShapeLineStart.x);
							ImGui::DragFloat2("end", &props.mEmitShapeLineEnd.x);
						} break;
						default:
							ImGui::Text("NONE");

						}
					}

					ImGui::TreePop();
				}
				ImGui::EndTabItem();
			}
			ImGui::EndTabBar();
		}
		else
			ImGui::EndTabBar();
		ImGui::End();
	}

	return true;
}

ParticleEmitter* ParticleEmitter::Clone()
{
	ParticleEmitter* temp = FactorySys->Create<ParticleEmitter>();
	(*temp) = (*this);
	return temp;
}

void ParticleEmitter::operator=(ParticleEmitter& part)
{
	mTexture = part.mTexture;
	props.mColorBegin = part.props.mColorBegin;
	props.mOffset = part.props.mOffset;
	props.mColorEnd = part.props.mColorEnd;
	props.mSizeBegin = part.props.mSizeBegin;
	props.mSizeVariation = part.props.mSizeVariation;
	props.mSizeEnd = part.props.mSizeEnd;
	props.mLifeTime = part.props.mLifeTime;
	props.mVelocity = part.props.mVelocity;
	props.mVelVariation = part.props.mVelVariation;
	props.mPosition = part.props.mPosition;
	props.mRotSpeed = part.props.mRotSpeed;
	props.mBlending = part.props.mBlending;
	mTransform = part.mTransform;
	mNumberOfParticles = part.mNumberOfParticles;
	mIndex = part.mIndex;
	looping = part.looping;
	props.mRotSpeed = part.props.mRotSpeed;
	props.CircularRot = part.props.CircularRot;
	props.mRadius = part.props.mRadius;
	props.mFill = part.props.mFill;
	props.mCircular = part.props.mCircular;
	props.mInitialOffset = part.props.mInitialOffset;
}

void ParticleEmitter::ToJson(nlohmann::json& j)
{

	IBase::ToJson(j);

	j["Name"] = "ParticleEmitter";
	j["NumberOfParticles"] = mNumberOfParticles;
	j["PositionX"] = props.mPosition.x;
	j["PositionY"] = props.mPosition.y;

	json& transform = j["Text_Transform"];
	transform["Translation"].push_back(mTransform.mPosition.x);
	transform["Translation"].push_back(mTransform.mPosition.y);
	transform["Translation"].push_back(mTransform.mPosition.z);

	j["ActiveSystem"] = mActiveSystem;

	j["VelocityX"] = props.mVelocity.x;
	j["VelocityY"] = props.mVelocity.y;

	j["VelocityVarX"] = props.mVelVariation.x;
	j["VelocityVarY"] = props.mVelVariation.y;

	j["InitialOffsetX"] = props.mInitialOffset.x;
	j["InitialOffsetY"] = props.mInitialOffset.y;

	j["CircularRotX"] = props.CircularRot.x;
	j["CircularRotY"] = props.CircularRot.y;

	j["CircularOffsetX"] = props.mOffset.x;
	j["CircularOffsetY"] = props.mOffset.y;

	j["Radius"] = props.mRadius;

	nlohmann::json BeginCol = nlohmann::json::array();

	BeginCol.push_back(props.mColorBegin.x);
	BeginCol.push_back(props.mColorBegin.y);
	BeginCol.push_back(props.mColorBegin.z);
	BeginCol.push_back(props.mColorBegin.w);

	j["BeginColors"] = BeginCol;

	nlohmann::json EndCol = nlohmann::json::array();

	EndCol.push_back(props.mColorEnd.x);
	EndCol.push_back(props.mColorEnd.y);
	EndCol.push_back(props.mColorEnd.z);
	EndCol.push_back(props.mColorEnd.w);

	j["EndColors"] = EndCol;

	j["SizeBeginX"] = props.mSizeBegin.x;
	j["SizeBeginY"] = props.mSizeBegin.y;
	j["SizeEndX"] = props.mSizeEnd.x;
	j["SizeEndY"] = props.mSizeEnd.y;
	j["SizeVariationX"] = props.mSizeVariation.x;
	j["SizeVariationY"] = props.mSizeVariation.y;

	j["LifeTime"] = props.mLifeTime;

	j["RotationSpeed"] = props.mRotSpeed;

	j["Radius"] = props.mRadius;

	j["FillCircle"] = props.mFill;

	j["UseBlending"] = props.mBlending;

	j["Looping"] = looping;

	j["Circular"] = props.mCircular;

	j["CurrentTex"] = mTexture->RelPath;

	//j["EmitShapeOffset"] = mEmitShapeOffset;
	json& emit = j["Emitter"];
	emit["EmitShapeOffset"].push_back(props.mEmitShapeOffset.x);
	emit["EmitShapeOffset"].push_back(props.mEmitShapeOffset.y);

	//j["EmitShapeSize"] = mEmitShapeSize;
	emit["EmitShapeSize"].push_back(props.mEmitShapeSize.x);
	emit["EmitShapeSize"].push_back(props.mEmitShapeSize.y);

	emit["EmitShapeAngle"].push_back(props.mEmitShapeAngle[0]);
	emit["EmitShapeAngle"].push_back(props.mEmitShapeAngle[1]);

	emit["EmitShapeLineStart"].push_back(props.mEmitShapeLineStart.x);
	emit["EmitShapeLineStart"].push_back(props.mEmitShapeLineStart.y);
	emit["EmitShapeLineEnd"].push_back(props.mEmitShapeLineEnd.x);
	emit["EmitShapeLineEnd"].push_back(props.mEmitShapeLineEnd.y);


}

void ParticleEmitter::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if (j.find("Name") != j.end())
		mName = j["Name"].get<std::string>();

	if (j.find("NumberOfParticles") != j.end()) {
		mNumberOfParticles = j["NumberOfParticles"];
		mIndex = mNumberOfParticles - 1;
	}

	if (j.find("PositionX") != j.end())
		props.mPosition.x = j["PositionX"];
	if (j.find("PositionY") != j.end())
		props.mPosition.y = j["PositionY"];

	if (j.find("Text_Transform") != j.end())
	{
		if (j["Text_Transform"].find("Translation") != j["Text_Transform"].end() && j["Text_Transform"]["Translation"].size() >= 3)
		{
			mTransform.mPosition.x = j["Text_Transform"]["Translation"][0];
			mTransform.mPosition.y = j["Text_Transform"]["Translation"][1];
			mTransform.mPosition.z = j["Text_Transform"]["Translation"][2];
		}
	}

	if (j.find("ActiveSystem") != j.end())
		mActiveSystem = j["ActiveSystem"];

	if (j.find("CircularRotX") != j.end())
		props.CircularRot.x = j["CircularRotX"];
	if (j.find("CircularRotY") != j.end())
		props.CircularRot.y = j["CircularRotY"];

	if (j.find("VelocityX") != j.end())
		props.mVelocity.x = j["VelocityX"];
	if (j.find("VelocityY") != j.end())
		props.mVelocity.y = j["VelocityY"];

	if (j.find("VelocityVarX") != j.end())
		props.mVelVariation.x = j["VelocityVarX"];
	if (j.find("VelocityVarY") != j.end())
		props.mVelVariation.y = j["VelocityVarY"];

	if (j.find("InitialOffsetX") != j.end())
		props.mInitialOffset.x = j["InitialOffsetX"];
	if (j.find("InitialOffsetY") != j.end())
		props.mInitialOffset.y = j["InitialOffsetY"];

	if (j.find("BeginColors") != j.end() && j["BeginColors"].size() >= 4)
	{
		props.mColorBegin.x = j["BeginColors"][0];
		props.mColorBegin.y = j["BeginColors"][1];
		props.mColorBegin.z = j["BeginColors"][2];
		props.mColorBegin.w = j["BeginColors"][3];
	}

	if (j.find("EndColors") != j.end() && j["EndColors"].size() >= 4)
	{
		props.mColorEnd.x = j["EndColors"][0];
		props.mColorEnd.y = j["EndColors"][1];
		props.mColorEnd.z = j["EndColors"][2];
		props.mColorEnd.w = j["EndColors"][3];
	}

	if (j.find("SizeBeginX") != j.end())
		props.mSizeBegin.x = j["SizeBeginX"];
	if (j.find("SizeBeginY") != j.end())
		props.mSizeBegin.y = j["SizeBeginY"];
	if (j.find("SizeEndX") != j.end())
		props.mSizeEnd.x = j["SizeEndX"];
	if (j.find("SizeEndY") != j.end())
		props.mSizeEnd.y = j["SizeEndY"];
	if (j.find("SizeVariationX") != j.end())
		props.mSizeVariation.x = j["SizeVariationX"];
	if (j.find("SizeVariationY") != j.end())
		props.mSizeVariation.y = j["SizeVariationY"];

	if (j.find("CircularOffsetX") != j.end())
		props.mOffset.x = j["CircularOffsetX"];
	if (j.find("CircularOffsetY") != j.end())
		props.mOffset.y = j["CircularOffsetY"];

	if (j.find("Radius") != j.end())
		props.mRadius = j["Radius"];

	if (j.find("FillCircle") != j.end())
		props.mFill = j["FillCircle"];

	props.mLifeTime = j["LifeTime"];
	props.mRotSpeed = j["RotationSpeed"];

	if (j.find("UseBlending") != j.end())
		props.mBlending = j["UseBlending"];
	if (j.find("Circular") != j.end())
		props.mCircular = j["Circular"];

	props.mBlending = j["UseBlending"];

	looping = j["Looping"];

	if (j.find("CurrentTex") != j.end())
		SetTexture(j["CurrentTex"].get<std::string>());

	if (j.find("Emitter") != j.end())
	{
		props.mEmitShapeOffset.x = j["Emitter"]["EmitShapeOffset"][0];
		props.mEmitShapeOffset.y = j["Emitter"]["EmitShapeOffset"][1];

		props.mEmitShapeSize.x = j["Emitter"]["EmitShapeSize"][0];
		props.mEmitShapeSize.y = j["Emitter"]["EmitShapeSize"][1];

		props.mEmitShapeAngle[0] = j["Emitter"]["EmitShapeAngle"][0];
		props.mEmitShapeAngle[1] = j["Emitter"]["EmitShapeAngle"][1];

		props.mEmitShapeLineStart.x = j["Emitter"]["EmitShapeLineStart"][0];
		props.mEmitShapeOffset.y = j["Emitter"]["EmitShapeLineStart"][1];
		props.mEmitShapeLineEnd.x = j["Emitter"]["EmitShapeLineEnd"][0];
		props.mEmitShapeLineEnd.y = j["Emitter"]["EmitShapeLineEnd"][1];
	}


	Initialize();
}

void ParticleEmitter::Save(std::string to_save)
{
	using nlohmann::json;
	json j;

	j["Name"] = "ParticleEmitter";
	j["NumberOfParticles"] = mNumberOfParticles;
	j["PositionX"] = props.mPosition.x;
	j["PositionY"] = props.mPosition.y;

	json& transform = j["Text_Transform"];
	transform["Translation"].push_back(mTransform.mPosition.x);
	transform["Translation"].push_back(mTransform.mPosition.y);
	transform["Translation"].push_back(mTransform.mPosition.z);

	j["ActiveSystem"] = mActiveSystem;

	j["VelocityX"] = props.mVelocity.x;
	j["VelocityY"] = props.mVelocity.y;

	j["VelocityVarX"] = props.mVelVariation.x;
	j["VelocityVarY"] = props.mVelVariation.y;

	j["InitialOffsetX"] = props.mInitialOffset.x;
	j["InitialOffsetY"] = props.mInitialOffset.y;

	j["CircularRotX"] = props.CircularRot.x;
	j["CircularRotY"] = props.CircularRot.y;

	j["CircularOffsetX"] = props.mOffset.x;
	j["CircularOffsetY"] = props.mOffset.y;

	j["Radius"] = props.mRadius;

	nlohmann::json BeginCol = nlohmann::json::array();

	BeginCol.push_back(props.mColorBegin.x);
	BeginCol.push_back(props.mColorBegin.y);
	BeginCol.push_back(props.mColorBegin.z);
	BeginCol.push_back(props.mColorBegin.w);

	j["BeginColors"] = BeginCol;

	nlohmann::json EndCol = nlohmann::json::array();

	EndCol.push_back(props.mColorEnd.x);
	EndCol.push_back(props.mColorEnd.y);
	EndCol.push_back(props.mColorEnd.z);
	EndCol.push_back(props.mColorEnd.w);

	j["EndColors"] = EndCol;

	j["SizeBeginX"] = props.mSizeBegin.x;
	j["SizeBeginY"] = props.mSizeBegin.y;
	j["SizeEndX"] = props.mSizeEnd.x;
	j["SizeEndY"] = props.mSizeEnd.y;
	j["SizeVariationX"] = props.mSizeVariation.x;
	j["SizeVariationY"] = props.mSizeVariation.y;

	j["LifeTime"] = props.mLifeTime;

	j["RotationSpeed"] = props.mRotSpeed;

	j["Radius"] = props.mRadius;

	j["FillCircle"] = props.mFill;

	j["UseBlending"] = props.mBlending;

	j["Looping"] = looping;

	j["Circular"] = props.mCircular;

	j["CurrentTex"] = mTexture->RelPath;

	//j["EmitShapeOffset"] = mEmitShapeOffset;
	json& emit = j["Emitter"];
	emit["EmitShapeOffset"].push_back(props.mEmitShapeOffset.x);
	emit["EmitShapeOffset"].push_back(props.mEmitShapeOffset.y);

	//j["EmitShapeSize"] = mEmitShapeSize;
	emit["EmitShapeSize"].push_back(props.mEmitShapeSize.x);
	emit["EmitShapeSize"].push_back(props.mEmitShapeSize.y);

	emit["EmitShapeAngle"].push_back(props.mEmitShapeAngle[0]);
	emit["EmitShapeAngle"].push_back(props.mEmitShapeAngle[1]);

	emit["EmitShapeLineStart"].push_back(props.mEmitShapeLineStart.x);
	emit["EmitShapeLineStart"].push_back(props.mEmitShapeLineStart.y);
	emit["EmitShapeLineEnd"].push_back(props.mEmitShapeLineEnd.x);
	emit["EmitShapeLineEnd"].push_back(props.mEmitShapeLineEnd.y);

	j["CurrentTex"] = mTexture->RelPath;

	std::ofstream fp_outspace(to_save);
	if (fp_outspace.is_open())
	{
		fp_outspace << std::setw(4) << j;
		fp_outspace.close();
	}
}
void ParticleEmitter::Load(std::string to_save)
{
	mParticlePool.clear();
	using nlohmann::json;
	json j;

	std::ifstream fp_outspace(to_save);
	if (fp_outspace.is_open() && fp_outspace.good())
	{
		fp_outspace >> j;
		fp_outspace.close();
	}
	else return;

	if (j.find("Name") != j.end())
		mName = j["Name"].get<std::string>();

	if (j.find("NumberOfParticles") != j.end()) {
		mNumberOfParticles = j["NumberOfParticles"];
		mIndex = mNumberOfParticles - 1;
	}
	mParticlePool.resize(mNumberOfParticles);
	mWorldPositions.resize(mNumberOfParticles);
	mWorldScaleRot.resize(mNumberOfParticles);
	mWorldColor.resize(mNumberOfParticles);
	if (j.find("PositionX") != j.end())
		props.mPosition.x = j["PositionX"];
	if (j.find("PositionY") != j.end())
		props.mPosition.y = j["PositionY"];

	if (j.find("Text_Transform") != j.end())
	{
		if (j["Text_Transform"].find("Translation") != j["Text_Transform"].end() && j["Text_Transform"]["Translation"].size() >= 3)
		{
			mTransform.mPosition.x = j["Text_Transform"]["Translation"][0];
			mTransform.mPosition.y = j["Text_Transform"]["Translation"][1];
			mTransform.mPosition.z = j["Text_Transform"]["Translation"][2];
		}
	}

	if (j.find("ActiveSystem") != j.end())
		mActiveSystem = j["ActiveSystem"];

	if (j.find("CircularRotX") != j.end())
		props.CircularRot.x = j["CircularRotX"];
	if (j.find("CircularRotY") != j.end())
		props.CircularRot.y = j["CircularRotY"];

	if (j.find("VelocityX") != j.end())
		props.mVelocity.x = j["VelocityX"];
	if (j.find("VelocityY") != j.end())
		props.mVelocity.y = j["VelocityY"];

	if (j.find("VelocityVarX") != j.end())
		props.mVelVariation.x = j["VelocityVarX"];
	if (j.find("VelocityVarY") != j.end())
		props.mVelVariation.y = j["VelocityVarY"];

	if (j.find("InitialOffsetX") != j.end())
		props.mInitialOffset.x = j["InitialOffsetX"];
	if (j.find("InitialOffsetY") != j.end())
		props.mInitialOffset.y = j["InitialOffsetY"];

	if (j.find("BeginColors") != j.end() && j["BeginColors"].size() >= 4)
	{
		props.mColorBegin.x = j["BeginColors"][0];
		props.mColorBegin.y = j["BeginColors"][1];
		props.mColorBegin.z = j["BeginColors"][2];
		props.mColorBegin.w = j["BeginColors"][3];
	}

	if (j.find("EndColors") != j.end() && j["EndColors"].size() >= 4)
	{
		props.mColorEnd.x = j["EndColors"][0];
		props.mColorEnd.y = j["EndColors"][1];
		props.mColorEnd.z = j["EndColors"][2];
		props.mColorEnd.w = j["EndColors"][3];
	}

	if (j.find("SizeBeginX") != j.end())
		props.mSizeBegin.x = j["SizeBeginX"];
	if (j.find("SizeBeginY") != j.end())
		props.mSizeBegin.y = j["SizeBeginY"];
	if (j.find("SizeEndX") != j.end())
		props.mSizeEnd.x = j["SizeEndX"];
	if (j.find("SizeEndY") != j.end())
		props.mSizeEnd.y = j["SizeEndY"];
	if (j.find("SizeVariationX") != j.end())
		props.mSizeVariation.x = j["SizeVariationX"];
	if (j.find("SizeVariationY") != j.end())
		props.mSizeVariation.y = j["SizeVariationY"];

	if (j.find("CircularOffsetX") != j.end())
		props.mOffset.x = j["CircularOffsetX"];
	if (j.find("CircularOffsetY") != j.end())
		props.mOffset.y = j["CircularOffsetY"];

	if (j.find("Radius") != j.end())
		props.mRadius = j["Radius"];

	if (j.find("FillCircle") != j.end())
		props.mFill = j["FillCircle"];

	props.mLifeTime = j["LifeTime"];
	props.mRotSpeed = j["RotationSpeed"];

	if (j.find("UseBlending") != j.end())
		props.mBlending = j["UseBlending"];
	if (j.find("Circular") != j.end())
		props.mCircular = j["Circular"];

	props.mBlending = j["UseBlending"];

	looping = j["Looping"];

	if (j.find("CurrentTex") != j.end())
		SetTexture(j["CurrentTex"].get<std::string>());

	if (j.find("Emitter") != j.end())
	{
		props.mEmitShapeOffset.x = j["Emitter"]["EmitShapeOffset"][0];
		props.mEmitShapeOffset.y = j["Emitter"]["EmitShapeOffset"][1];

		props.mEmitShapeSize.x = j["Emitter"]["EmitShapeSize"][0];
		props.mEmitShapeSize.y = j["Emitter"]["EmitShapeSize"][1];

		props.mEmitShapeAngle[0] = j["Emitter"]["EmitShapeAngle"][0];
		props.mEmitShapeAngle[1] = j["Emitter"]["EmitShapeAngle"][1];

		props.mEmitShapeLineStart.x = j["Emitter"]["EmitShapeLineStart"][0];
		props.mEmitShapeOffset.y = j["Emitter"]["EmitShapeLineStart"][1];
		props.mEmitShapeLineEnd.x = j["Emitter"]["EmitShapeLineEnd"][0];
		props.mEmitShapeLineEnd.y = j["Emitter"]["EmitShapeLineEnd"][1];
	}

	if (j.find("CurrentTex") != j.end())
	{
		mTexture = RsrcMan->GetResourceType<Texture>(j["CurrentTex"].get<std::string>());
	}

	Initialize();
}

#pragma region InstanceRendering

int ParticleEmitter::InstanceCount()
{
	// return only the number of alive particles
	return mAliveList.size();
}

/*    RENDER()
BindResources();
	PreDrawEvent();

	if (mbUploadOnRender || mbUploadTransforms)
		UploadToGPU();
	DrawModel();

	UnBindResources();*/

void ParticleEmitter::CreateOpenGLData()
{
	// TODO(Maria): Add a function to free the GL buffers in the destructor. 
	if (mGLPositionBuffer) {
		glDeleteBuffers(1, &mGLPositionBuffer);
		mGLPositionBuffer = 0;
	}
	if (mGLScaleRotBuffer) {
		glDeleteBuffers(1, &mGLScaleRotBuffer);
		mGLScaleRotBuffer = 0;
	}
	if (mGLColorBuffer) {
		glDeleteBuffers(1, &mGLColorBuffer);
		mGLColorBuffer = 0;
	}

	// now generate and bind the buffer for world position
	glGenBuffers(1, &mGLPositionBuffer);
	// now generate and bind the buffer for world scale/rot
	glGenBuffers(1, &mGLScaleRotBuffer);
	// now generate and bind the buffer for world scale/rot
	glGenBuffers(1, &mGLColorBuffer);
}

void ParticleEmitter::AttachTransformBufferToModel()
{
	// LAZY INIT - create the open gl data for instanced rendering. 
	if(mGLPositionBuffer == 0)
		CreateOpenGLData();

	GLenum error = glGetError();
	assert(mMesh != NULL, "");

	glBindVertexArray(mMesh->GetVAO());
	error = glGetError();

	// Locations for the worldposition attribute
	GLuint world_pos_attr_loc = 3; // 3 because we know that 0 is pos, 1 is tex coord, 2 is color (check Model::UploadVertexFormatToGPU() for more details)
	GLuint world_scale_rot_attr_loc = world_pos_attr_loc + 1;
	GLuint world_color_loc = world_scale_rot_attr_loc + 1;

	// now generate and bind the buffer for world position
	glBindBuffer(GL_ARRAY_BUFFER, mGLPositionBuffer); // TODO(Thomas): Test if I can do without this. technically, the vertex definition is not related to the buffer itself. i.e. it's just a mapping. 
	glEnableVertexAttribArray(world_pos_attr_loc);
	glVertexAttribPointer(world_pos_attr_loc, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);
	glVertexAttribDivisor(world_pos_attr_loc, 1);
	error = glGetError();

	// now generate and bind the buffer for world scale/rot
	glBindBuffer(GL_ARRAY_BUFFER, mGLScaleRotBuffer);
	glEnableVertexAttribArray(world_scale_rot_attr_loc);
	glVertexAttribPointer(world_scale_rot_attr_loc, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);
	glVertexAttribDivisor(world_scale_rot_attr_loc, 1);
	error = glGetError();

	// now generate and bind the buffer for world color
	glBindBuffer(GL_ARRAY_BUFFER, mGLColorBuffer);
	glEnableVertexAttribArray(world_color_loc);
	glVertexAttribPointer(world_color_loc, 4, GL_FLOAT, GL_FALSE, sizeof(Color), 0);
	glVertexAttribDivisor(world_color_loc, 1);
	error = glGetError();

	// unbind all
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	error = glGetError();
}

void ParticleEmitter::DetachTransformBufferFromModel()
{
	GLenum error = glGetError();
	assert(mMesh != NULL, "");

	// Locations for the worldposition attribute
	GLuint world_pos_attr_loc = 3; // 3 because we know that 0 is pos, 1 is tex coord, 2 is color (check Model::UploadVertexFormatToGPU() for more details)
	GLuint world_scale_rot_attr_loc = world_pos_attr_loc + 1;
	GLuint world_color_loc = world_scale_rot_attr_loc + 1;

	// disable the vertex attributes
	glBindVertexArray(mMesh->GetVAO());	// bind the new model's vertex array object - TODO(Thomas): Is this needed?
	glDisableVertexAttribArray(world_pos_attr_loc);
	glDisableVertexAttribArray(world_scale_rot_attr_loc);
	glDisableVertexAttribArray(world_color_loc);
	//glDisableVertexArrayAttrib(model->GetGLHandleVAO(), world_pos_attr_loc);
	//glDisableVertexArrayAttrib(model->GetGLHandleVAO(), world_scale_rot_attr_loc);
	error = glGetError();
}

void ParticleEmitter::UploadToGPUFromTransformComponents()
{
	assert(mGLPositionBuffer != 0);
	assert(mGLScaleRotBuffer != 0);
	GLenum error = glGetError();
	// get instance coutn
	int instanceCount = mAliveList.size();// InstanceCount();
	if (instanceCount)
	{
		int i = 0;
		//float z = mOwnerTransform->GetWorldPosition().z; // TODO(maria): figure this out!
		for (auto& part : mAliveList) {
			if (i == mWorldPositions.size()) break;
			mWorldPositions[i] = glm::vec3(part->mPosition);
			mWorldScaleRot[i].x = part->mSize.x;
			mWorldScaleRot[i].y = part->mSize.y;
			mWorldScaleRot[i].z = part->mRotation;
			mWorldColor[i++] = part->mColor;
		}

		// pass these data to the buffers. 
		// Note(Thomas): This will re-allocate the data everytime, so there's room for optimization
		glBindBuffer(GL_ARRAY_BUFFER, mGLPositionBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * instanceCount, mWorldPositions.data(), GL_DYNAMIC_DRAW);
		// glBufferSubData(....) 
		error = glGetError();

		glBindBuffer(GL_ARRAY_BUFFER, mGLScaleRotBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * instanceCount, mWorldScaleRot.data(), GL_DYNAMIC_DRAW);
		error = glGetError();

		glBindBuffer(GL_ARRAY_BUFFER, mGLColorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * instanceCount, mWorldColor.data(), GL_DYNAMIC_DRAW);
		error = glGetError();

		// clean up
		//delete[] worldPos;
		//delete[] worldScaleRot;
	}
	// flag the transfomrs up to date
	mbUploadTransforms = false;
}

void ParticleEmitter::BindResources()
{
	// Set Texture to current
	if (mTexture && mTexture->get())
	{
		mTexture->get()->Bind();
	}

	// bind the sampler if one is set
	//if (pSamplerRes && pSamplerRes->get())
	//	pSamplerRes->get()->BindToTextureUnit(0);

	// Send data to shader
	if (mShader)
	{
		//mShader->Bind();
		mShader->SetUniform("texture1", 0);
		//mShader->SetUniform("ModulColor", mParticlePool.at(1).mColorBegin);
		// Texture unit
		//int texUnit = 0;
		//pShaderRes->get()->SetShaderUniform("ts_diffuse", &texUnit);
		//check_gl_error();
	}

	AttachTransformBufferToModel();
}

void ParticleEmitter::DrawModel()
{
	assert(mMesh != NULL);
	mMesh->DrawInstanced(InstanceCount());
}

void ParticleEmitter::UnBindResources()
{
	DetachTransformBufferFromModel();
}

#pragma endregion 

bool Particle::Update(float dt, glm::vec2 _Pos )
{
	float _dt = (float)TimeSys->GetFrameTime();

	
	// particle update
	
	mLifeRemaining -= _dt;
	mRotation = mRotSpeed * _dt;

	if (mCircular == 0)
	{
		mPosition += (mVelocity * _dt);
	}
	else
	{
		if (mAcitve && !mPrevActive)
		{
			mPrevActive = true;
			mPosition = glm::vec3(_Pos + RandomPosCircle(mOffset, glm::vec2{ mRadius, mRadius },
				CircularRot.x, CircularRot.y, mFill), 0);
		}
		mPosition += (mVelocity * _dt);
	}

	mLife = mLifeRemaining / mLifeTime;
	mSize = LERP(mSizeEnd, mSizeBegin, mLife);
	mColor = Lerp(mColorEnd, mColorBegin, mLife);
	mColor.w = mColor.w * mLife;
	

	if (mLifeRemaining <= 0.0f)
	{
		mAcitve = false;
		mPrevActive = false;
		return false;
	}
	return true;
}
