/**********************************************************************************/
/*!
\file   Camera.h
\author Maria Bola�os
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the classes Camera, CameraComponent & EditorCamera.
Functions contain getting the viewport matrix, camera matrix, view rectangle.
Camera can move with WASD keys, rotate with KL and zoom in/out with IO.
*/
/*********************************************************************************/
// CAMERA.H
#pragma once
#include <glm/glm.hpp>


#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IComp.h"

#include "../Color/Color.h"

#include "../../EventSystem/Listener/Listener.h"
#include "../../EventSystem/EventHandler.h"

#include "../../Transform2D/Transform2D.h"

class FullScreenToggled;
class ResolutionChanged;

struct Viewport
{
	float left, bottom, right, top;
};


class Camera : public IBase
{
	SM_RTTI_DECL(Camera, IBase)

public:
	Camera();
	virtual ~Camera();

	Viewport& GetViewport();
	bool GetShouldClear();
	Color& GetClearColor();
	glm::vec2& GetViewRectangle();
	void SetViewport(glm::vec2 viewport);
	void SetViewRectangle(glm::vec2 _viewRectangle);

public:

	Viewport mViewport = { 0, 0,  1, 1 };
	bool mbShouldClear = true;
	Color mClearColor;// = Color(0, 0, 0, 0);
	glm::vec2 mViewRectangle = { 1280, 720 };
	// no gettor or settor as they are only called by class functions
	glm::vec2 normalizeRec;
	glm::vec2 constRectanle = mViewRectangle;
};

class CameraComponent : public IComp
{
	friend class Editor;
	friend class Menus;

	SM_RTTI_DECL(CameraComponent, IComp)


public:
	CameraComponent();

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	CameraComponent* Clone()override;

	void Update()override;
	bool Edit()override;
	void DefaultValue();
	CameraComponent& operator=(const CameraComponent& rhs);

	glm::mat4 GetViewMatrix();
	glm::mat4 GetInvViewMatrix();
	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetViewProjMatrix();
	glm::mat4 GetModelToView();
	glm::mat4 GetWorld2Cam();
	glm::mat4 GetInvWorld2Cam();
	glm::mat4 GetCam2Proj();
	glm::mat4 GetInvCam2Proj();
	glm::mat4 GetProj2Window();
	glm::mat4 GetInvProj2Window();

	Camera* GetCamera();
	Viewport& GetViewport();
	float GetRatio();
	void SetZOrder(float z);
	float GetZOrder();
	
	// TODO: Make private
public:

	void ComputeViewMatrix();
	void ComputeProjectionMatrix();
	void UpdateMatrices();
	Camera mCamera;
	float ZOrder = 0;
	// no settor or gettor as they are only caled inside class 
	float mCustomOrientation = 0;
	glm::vec2 mCustomPosition{ 0,0 };

	glm::mat4 mViewMatrix;
	glm::mat4 mProjectionMatrix;
	glm::mat4 mModel;

};

class EditorCamera : public Camera
{
	SM_RTTI_DECL(EditorCamera, Camera)

public:
	EditorCamera();
	void Update();
	void DefaultValue();

	glm::mat4 GetViewMatrix();
	glm::mat4 GetInvViewMatrix();
	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetViewProjMatrix();
	glm::mat4 GetModelToView();
	glm::mat4 GetWorld2Cam();
	glm::mat4 GetInvWorld2Cam();
	glm::mat4 GetCam2Proj();
	glm::mat4 GetInvCam2Proj();
	glm::mat4 GetProj2Window();
	glm::mat4 GetInvProj2Window();
	glm::mat4 GetProj2Viewport();
	glm::mat4 GetInvProj2Viewport();

	Transform2D GetTransform();
	void SetTranform(Transform2D trans);

public:
	Transform2D mTransform;
	glm::vec2 prevResolution = mViewRectangle;
	float CameraVelocity = 5;
	bool ResolutionWindowed = false;
	bool ResolutionFullWindow = false;
};
