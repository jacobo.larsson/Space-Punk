/**********************************************************************************/
/*!
\file   Camera.cpp
\author Maria Bola�os & David Miranda
\par    email: maria.bolanos@digipen.edu m.david@digipen.edu
\par    DigiPen login: maria.bolanos  m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Camera.h
*/
/*********************************************************************************/

#include <glm/gtc/matrix_transform.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h"
#include "../../Factory/Factory.h"
#include "../Window/Window.h"
#include "../../EventSystem/EventDispatcher.h"
#include "../../Editor/Editor.h"
#include "../../Editor/Menus/Menus.h"
#include "../../EventSystem/Events/WindowEvents/WindowEvents.h"


#include "../../GameObject/GameObject.h"
#include "../../Space/Space.h"

#include "../../Transform2D/TransformComponent.h"

#include "Camera.h"

#pragma region Camera
Camera::Camera():mClearColor(Colors::black)
{
	SetName("Camera");	
	normalizeRec = mViewRectangle / (glm::vec2)WindowSys->GetSize();
}

Camera::~Camera()
{
}

Viewport& Camera::GetViewport()
{
	return mViewport;
}

bool Camera::GetShouldClear()
{
	return mbShouldClear;
}

Color& Camera::GetClearColor()
{
	return mClearColor;
}

glm::vec2& Camera::GetViewRectangle()
{
	return mViewRectangle;
}

void Camera::SetViewport(glm::vec2 viewRectangle)
{
	mViewRectangle = viewRectangle;
	normalizeRec = mViewRectangle / (glm::vec2)WindowSys->GetSize();
	constRectanle = mViewRectangle;
}
void Camera::SetViewRectangle(glm::vec2 _viewRectangle)
{
	mViewRectangle = _viewRectangle;
}
#pragma endregion 

#pragma region CameraComponent
// Camera component functions
CameraComponent::CameraComponent()
{
	SetName("CameraComponent");
}

void CameraComponent::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	IBase::ToJson(j);

	json& camera = j["Camera"];
	camera["Viewport"].push_back(mCamera.mViewport.top);
	camera["Viewport"].push_back(mCamera.mViewport.left);
	camera["Viewport"].push_back(mCamera.mViewport.bottom);
	camera["Viewport"].push_back(mCamera.mViewport.right);

	camera["ShouldClear"] = mCamera.mbShouldClear;

	camera["ClearColor"].push_back(mCamera.mClearColor.r);
	camera["ClearColor"].push_back(mCamera.mClearColor.g);
	camera["ClearColor"].push_back(mCamera.mClearColor.b);
	camera["ClearColor"].push_back(mCamera.mClearColor.a);

	camera["ViewRectangle"].push_back(mCamera.mViewRectangle.x);
	camera["ViewRectangle"].push_back(mCamera.mViewRectangle.y);

}
void CameraComponent::FromJson(const nlohmann::json& j)
{
	using nlohmann::json;

	IBase::FromJson(j);
	if (j.find("Camera") != j.end())
	{
		if (j["Camera"].find("Viewport") != j["Camera"].end() && j["Camera"]["Viewport"].size() >= 4)
		{
			mCamera.mViewport.top = j["Camera"]["Viewport"][0];
			mCamera.mViewport.left = j["Camera"]["Viewport"][1];
			mCamera.mViewport.bottom = j["Camera"]["Viewport"][2];
			mCamera.mViewport.right = j["Camera"]["Viewport"][3];
		}

		if (j["Camera"].find("ShouldClear") != j["Camera"].end())
			mCamera.mbShouldClear = j["Camera"]["ShouldClear"];

		if (j["Camera"].find("ClearColor") != j["Camera"].end() && j["Camera"]["ClearColor"].size() >= 4)
		{
			mCamera.mClearColor.r = j["Camera"]["ClearColor"][0];
			mCamera.mClearColor.g = j["Camera"]["ClearColor"][1];
			mCamera.mClearColor.b = j["Camera"]["ClearColor"][2];
			mCamera.mClearColor.a = j["Camera"]["ClearColor"][3];
		}

		if (j["Camera"].find("ViewRectangle") != j["Camera"].end() && j["Camera"]["ViewRectangle"].size() >= 2)
		{
			mCamera.mViewRectangle.x = j["Camera"]["ViewRectangle"][0];
			mCamera.mViewRectangle.y = j["Camera"]["ViewRectangle"][1];
		}
	}
}

CameraComponent* CameraComponent::Clone()
{
	CameraComponent* temp = FactorySys->Create<CameraComponent>();
	(*temp) = (*this);
	return temp;
}

void CameraComponent::Update()
{
	UpdateMatrices();
}
bool CameraComponent::Edit()
{
	bool changed = false;

	auto transform = mOwner->GetComp<TransformComponent>();
	if (transform != nullptr)
	{
		mOwner->GetParentSpace()->Drawrectangle(glm::vec3{ transform->GetWorldPosition().x, transform->GetWorldPosition().y, 0} , (int)mCamera.mViewRectangle.x, (int)mCamera.mViewRectangle.y, Colors::orange);
	}

	// Position
	ImGui::Text("Viewport");
	ImGui::PushID("Viewport");
	ImGui::Columns(2, "Viewport", true);
	ImGui::DragFloat("Left", &mCamera.mViewport.left);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::NextColumn();
	ImGui::DragFloat("Right", &mCamera.mViewport.right);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::NextColumn();
	ImGui::DragFloat("Bottom", &mCamera.mViewport.bottom);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::NextColumn();
	ImGui::DragFloat("Top", &mCamera.mViewport.top);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();

	ImGui::Text("ViewRectangle");
	ImGui::PushID("ViewRectangle");
	ImGui::Columns(2, "ViewRectangle", true);
	ImGui::DragFloat("X", &mCamera.mViewRectangle.x);
	mCamera.mViewRectangle.y = mCamera.mViewRectangle.x * 9 / 16;
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::NextColumn();
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Columns(1);
	ImGui::PopID();

	return changed;
}
void CameraComponent::DefaultValue()
{
	mCustomOrientation = 0;
	mCamera.mViewRectangle = { 1280, 720 };
}
CameraComponent& CameraComponent::operator=(const CameraComponent& rhs)
{
	mName = rhs.mName;
	mCamera = rhs.mCamera;
	ZOrder = rhs.ZOrder;
	mCustomOrientation = rhs.mCustomOrientation;
	mCustomPosition = rhs.mCustomPosition;
	return *this;

}

glm::mat4 CameraComponent::GetViewMatrix()
{
	return mViewMatrix;
}
glm::mat4 CameraComponent::GetInvViewMatrix()
{
	return glm::inverse(mViewMatrix);
}
glm::mat4 CameraComponent::GetProjectionMatrix()
{
	return mProjectionMatrix;
}
glm::mat4 CameraComponent::GetViewProjMatrix()
{
	return mProjectionMatrix * mViewMatrix;
}
glm::mat4 CameraComponent::GetModelToView()
{
	glm::mat4 view = glm::ortho(mCamera.mViewport.left * (float)WindowSys->GetWidth(),
								mCamera.mViewport.right * (float)WindowSys->GetWidth(),
								mCamera.mViewport.bottom * (float)WindowSys->GetHeight(), 
								mCamera.mViewport.top * (float)WindowSys->GetHeight(), 0.01f, 1000.0f);
	glm::mat4 cam = GetViewMatrix();
	glm::mat4 ret = view * mViewMatrix;
	return ret;
}
glm::mat4 CameraComponent::GetWorld2Cam()
{
	const auto& _Transform = mOwner->GetComp<TransformComponent>()->GetWorldTransform();
	glm::mat4 mat = glm::rotate(glm::mat4(1.0f), _Transform.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f))
		* glm::translate(glm::mat4(1.0f), glm::vec3(_Transform.mPosition.x, _Transform.mPosition.y, 0.0f));
	return glm::inverse(mat);

}
glm::mat4 CameraComponent::GetInvWorld2Cam()
{
	return glm::inverse(GetWorld2Cam());
}
glm::mat4 CameraComponent::GetCam2Proj()
{
	return GetWorld2Cam() * GetProjectionMatrix();
}
glm::mat4 CameraComponent::GetInvCam2Proj()
{
	return glm::inverse(mProjectionMatrix);
}
glm::mat4 CameraComponent::GetProj2Window()
{
	return WindowSys->NDCToWindow();
}
glm::mat4 CameraComponent::GetInvProj2Window()
{
	return WindowSys->NDCToWindowInv();
}

Camera* CameraComponent::GetCamera()
{
	return &mCamera;
}
Viewport& CameraComponent::GetViewport()
{
	return mCamera.GetViewport();
}
float CameraComponent::GetRatio()
{
	return mCamera.mViewRectangle.x / mCamera.mViewRectangle.y;
}
void CameraComponent::SetZOrder(float z)
{
	ZOrder = z;
}
float CameraComponent::GetZOrder()
{
	return ZOrder;
}

void CameraComponent::ComputeViewMatrix()
{
	Transform2D* world_transform = &(mOwner->GetComp<TransformComponent>()->GetWorldTransform());

	if (world_transform == nullptr)
	{
		std::cout << "Could not find transform of the camera: " << mOwner->GetName().c_str() << std::endl;
	}
	else
		mViewMatrix = glm::rotate(glm::mat4(1.0f), -world_transform->mOrientation + mCustomOrientation, glm::vec3(0.0f, 0.0f, 1.0f)) *
			glm::translate(glm::mat4(1.0f), -world_transform->mPosition + glm::vec3(mCustomPosition, 0.0f));
	
}

void CameraComponent::ComputeProjectionMatrix()
{
	float left = -mCamera.mViewRectangle.x * 0.5f;
	float right = +mCamera.mViewRectangle.x * 0.5f;
	float bottom = -mCamera.mViewRectangle.y * 0.5f;
	float top = +mCamera.mViewRectangle.y * 0.5f;

	mProjectionMatrix = glm::ortho(left, right, bottom, top, 0.01f, 1000.0f);
}

void CameraComponent::UpdateMatrices()
{
	ComputeViewMatrix();
	ComputeProjectionMatrix();
}


#pragma endregion

#pragma region Editor Camera
// Editor Camera functions
EditorCamera::EditorCamera():mTransform { glm::vec3{ 0,0,20 }, glm::vec3{ 1,1,0 }, 0 }
{
	mViewport = { 0, 0,  1, 1 };
	mbShouldClear = true;
	mClearColor;// = Color(0, 0, 0, 0);
	mViewRectangle = { 1280, 720 };
	normalizeRec = mViewRectangle / (glm::vec2)WindowSys->GetSize();
}

void EditorCamera::Update()
{

}

void EditorCamera::DefaultValue()
{
	mViewRectangle = { 1280, 720 };
	mTransform.mPosition = { 0,0,20 };
	mTransform.mOrientation = 0.0f;
}
glm::mat4 EditorCamera::GetViewMatrix()
{
	glm::mat4 view = glm::rotate(glm::mat4(1.0f), -mTransform.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f)) *
		glm::translate(glm::mat4(1.0f), -mTransform.mPosition);
	return view;
}
glm::mat4 EditorCamera::GetInvViewMatrix()
{
	glm::mat4 view = glm::rotate(glm::mat4(1.0f), mTransform.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f)) *
		glm::translate(glm::mat4(1.0f), mTransform.mPosition);
	return view;
}
glm::mat4 EditorCamera::GetProjectionMatrix()
{
	float left = -mViewRectangle.x * 0.5f;
	float right = +mViewRectangle.x * 0.5f;
	float bottom = -mViewRectangle.y * 0.5f;
	float top = +mViewRectangle.y * 0.5f;

	return glm::ortho(left, right, bottom, top, 0.01f, 1000.0f);
}
glm::mat4 EditorCamera::GetViewProjMatrix()
{
	return GetProjectionMatrix() * GetViewMatrix();
}
glm::mat4 EditorCamera::GetModelToView()
{
	glm::mat4 view = glm::ortho(mViewport.left * (float)WindowSys->GetWidth(), 
		mViewport.right * (float)WindowSys->GetWidth(),
		mViewport.bottom * (float)WindowSys->GetHeight(),
		mViewport.top * (float)WindowSys->GetHeight(), 0.01f, 1000.0f);
	glm::mat4 cam = GetViewMatrix();
	glm::mat4 ret = view * cam;
	return ret;
}
glm::mat4 EditorCamera::GetWorld2Cam()
{
	glm::mat4 mat = glm::rotate(glm::mat4(1.0f), mTransform.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f)) *
		glm::translate(glm::mat4(1.0f), mTransform.mPosition);
	return glm::inverse(mat);
}
glm::mat4 EditorCamera::GetInvWorld2Cam()
{
	return glm::inverse(GetWorld2Cam());
}
glm::mat4 EditorCamera::GetCam2Proj()
{
	return GetProjectionMatrix();
}
glm::mat4 EditorCamera::GetInvCam2Proj()
{
	return glm::inverse(GetCam2Proj());
}

glm::mat4 EditorCamera::GetProj2Window()
{
	return WindowSys->NDCToWindow();
}

glm::mat4 EditorCamera::GetInvProj2Window()
{
	return WindowSys->NDCToWindowInv();
}

glm::mat4 EditorCamera::GetProj2Viewport()
{
	return GetViewProjMatrix();
}

glm::mat4 EditorCamera::GetInvProj2Viewport()
{
	return glm::inverse(GetProj2Viewport());
}

Transform2D EditorCamera::GetTransform()
{
	return mTransform;
}

void EditorCamera::SetTranform(Transform2D trans)
{
	mTransform = trans;
}

#pragma endregion