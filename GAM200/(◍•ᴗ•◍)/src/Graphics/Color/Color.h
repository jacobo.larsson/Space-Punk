/**********************************************************************************/
/*!
\file   Color.h
\author David Miranda 
\par    email: m.david@digipen.edu 
\par    DigiPen login: m.david 
\par    Course: GAM200
\date   16/10/2020
\brief
File containing a Color struct with rgba
*/
/*********************************************************************************/
#pragma once
#include <glm/glm.hpp>

//typedef glm::vec4 Color;
class Color : public glm::vec4
{
public:
	Color(float _r = 1, float _g = 1, float _b = 1, float _a = 1) : glm::vec4(_r, _g, _b, _a){}

};

namespace Colors
{
	static Color red = Color(1, 0, 0, 1);
	static Color green = Color(0, 1, 0, 1);
	static Color blue = Color(0, 0, 1, 1);
	static Color white = Color(1, 1, 1, 1);
	static Color black = Color(0, 0, 0, 1);
	static Color yellow = Color(1, 1, 0, 1);
	static Color orange = Color(1, 0.5, 0, 1);
}