/**********************************************************************************/
/*!
\file   Shader.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Shader.h
*/
/*********************************************************************************/


#include <fstream>
#include <sstream>
#include <iostream>
#include <GL/glew.h>

#include "Shader.h"

Shader::Shader(const char* name, const char* vertexPath, const char* fragmentPath) : ID { 0 }
{
    mName = name;

	std::string vertexCode;
	std::string fragmentCode;
    std::string vertex_path(vertexPath);
    std::string fragment_path(fragmentPath);
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;

	vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open(vertex_path);
        fShaderFile.open(fragment_path);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (std::ifstream::failure &)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();
    // 2. compile shaders
    unsigned int vertex, fragment;
    // vertex shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    checkCompileErrors(vertex, "VERTEX");
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");
    // shader Program
    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);
    checkCompileErrors(ID, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);

}

Shader::~Shader()
{
    if (ID > 0)
        glDeleteProgram(ID);
}

void Shader::Use()
{
    glUseProgram(ID);
}

void Shader::SetUniform(const std::string & name, bool value) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniform1i(loc, value);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string & name, int value) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniform1i(loc, value);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string & name, float value) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniform1f(loc, value);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string& name, float x, float y, float z) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniform3f(loc, x, y, z);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string& name, const glm::vec4& v) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniform4f(loc, v.x, v.y, v.z, v.w);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string& name, const glm::mat3& m) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniformMatrix3fv(loc, 1, GL_FALSE, &m[0][0]);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string& name, const glm::mat4& m) const
{
    int loc = GetUniformLocation(name);

    if (loc >= 0)
    {
        glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
    }
    else
    {
        std::cout << "Uniform: " << name << " not found." << std::endl;
    }
}

void Shader::SetUniform(const std::string& name, const glm::vec3 & value) const
{
    SetUniform(name, value.x, value.y, value.z);
}

void Shader::checkCompileErrors(unsigned int shader, std::string type)
{
    int success;
    char infoLog[1024];
    if (type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
}

int Shader::GetUniformLocation(const std::string& name) const
{
    return glGetUniformLocation(ID,name.c_str());
}
