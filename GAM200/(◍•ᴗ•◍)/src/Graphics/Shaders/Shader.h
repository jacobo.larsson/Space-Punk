/**********************************************************************************/
/*!
\file   Shader.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing functions to create a shader and set the shader uniforms
*/
/*********************************************************************************/
#pragma once
#include <string>
#include <glm/glm.hpp>


#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IBase.h"

class Shader: public IBase
{
	SM_RTTI_DECL(Shader, IBase)
public:
	unsigned ID;
	
	Shader(const char* name = NULL, const char * vertexPath = NULL, const char * fragmentPath = NULL);
	~Shader();

	void Use();

	void SetUniform(const std::string& name, bool value)const;
	void SetUniform(const std::string& name, int value)const;
	void SetUniform(const std::string& name, float value)const;
	void SetUniform(const std::string& name, float x, float y, float z)const;
	void SetUniform(const std::string& name, const glm::vec3& value)const;
	void SetUniform(const std::string& name, const glm::vec4& v) const;
	void SetUniform(const std::string& name, const glm::mat3& m) const;
	void SetUniform(const std::string& name, const glm::mat4& m) const;
private:
	void checkCompileErrors(unsigned int shader, std::string type);
	int GetUniformLocation(const std::string& name)const;
	std::string log;
};