﻿/**********************************************************************************/
/*!
\file   RenderManager.cpp
\author Maria Bolaños & David Miranda
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on RenderManager.h
*/
/*********************************************************************************/
#include <glm/gtc/constants.hpp>
#include <spine/spine.h>
#include <spine/Extension.h>

#include <glm/gtc/matrix_transform.hpp>

#include "../Font/Font.h"

#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Animation/SM_Animation.h"

#include "../Window/Window.h"
#include "../../Input/Input.h"
#include "../../ResourceManager/ResourceManager.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"

#include "../Vertex/Vertex.h"
#include "../Color/Color.h"
#include "../Shaders/Shader.h"
#include "../Texture/Texture.h"
#include "../Mesh/Mesh.h"
#include "../Particle System/ParticleSystem.h"

#include "../Renderable/Renderable.h"
#include "../Camera/Camera.h"
#include "../../Time/Time.h"
#include "../../Editor/Editor.h"

#include "../../Transform2D/TransformComponent.h"

#include "RenderManager.h"

RenderManager* RenderManager::instance = nullptr;

bool RenderManager::Initialize()
{
	LoadDefaultResources();
	editor_camera = new EditorCamera();
	//editor_camera->mViewRectangle = { 500,500 };
	mMeshes["line.mesh"] = new Mesh(Mesh::EPrimitiveType::eLineList);
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);
	//mParticle->FromJson("./data/Particles/fire.json");

	return true;
}
void RenderManager::Update()
{
	for (auto& _Space : SceneSys->GetAllSpaces())
	{
		if (SettingsSys->SpacePartitioning && _Space->SpacePartitioningEnabled())
		{
			mActiveRenderables.clear();
			mActiveEmitters.clear();
			mActiveSkeletons.clear();
			_Space->GetActiveComponents<Renderable>(mActiveRenderables, "Renderable");
			_Space->GetActiveComponents<ParticleEmitter>(mActiveEmitters, "ParticleEmitter");
			_Space->GetActiveComponents<SkeletonRenderable>(mActiveSkeletons, "SkeletonRenderable");

			for (auto& _Camera : _Space->GetComponents<CameraComponent>())
				_Camera->Update();
			if (SettingsSys->inEditor == false)
			{
				for (auto& _Emiter : mActiveEmitters)
				{
					_Emiter->Emit(_Emiter->props);
					_Emiter->Update();
				}
				for (auto& _Animation : mActiveSkeletons)
				{
					_Animation->Update();
				}
			}
		}
		else
		{
			for (auto& _Camera : _Space->GetComponents<CameraComponent>())
				_Camera->Update();
			if (SettingsSys->inEditor == false)
			{
				for (auto& _EmiterIT : _Space->GetComponents<ParticleEmitter>())
				{
					ParticleEmitter* _Emiter = reinterpret_cast<ParticleEmitter*>(_EmiterIT);
					_Emiter->Emit(_Emiter->props);
					_Emiter->Update();
				}
				for (auto& _Animation : _Space->GetComponents<SkeletonRenderable>())
				{
					_Animation->Update();
				}
			}
		}
	}
	if (InputSys->key_is_down(keys::R) && InputSys->key_is_triggered(keys::LeftControl))
	{
		editor_camera->DefaultValue();
		WireFrame = false;
	}
	if (InputSys->key_is_down(keys::M) && InputSys->key_is_triggered(keys::LeftControl))
	{
		WireFrame = !WireFrame;
	}
}
void RenderManager::LateUpdate()
{
}
void RenderManager::Render()
{
	// Clear the frame buffer to black color.
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);


	// Render all the spaces
	// But first order them
	SortSpaces();
	for (auto& _Space : mSpaces)
	{
		if (_Space->Enabled())
		{
			
			//else
			//	SortRenderableZ(_Space);

			// Iterate over the cameras to render the scene
			for (auto& _CamIT : _Space->GetComponents<CameraComponent>())
			{
				CameraComponent* _Camera = reinterpret_cast<CameraComponent*>(_CamIT);
				auto _CamMatrix = _Camera->GetViewProjMatrix();

				SetViewport(_Camera->GetViewport());

				RenderRenderables(_Camera, _Space);
				RenderSkeletons(_Camera, _Space);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				RenderComponents<TextRenderable>(_CamMatrix, _Space);
				RenderParticles(_Camera, _Space);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				RenderTranslucentComponents(_Camera->GetViewProjMatrix());

				_Space->RenderLines(_CamMatrix);
				_Space->SetCurrentLineVtx(0);
			}
		}
		glClear(GL_DEPTH_BUFFER_BIT);
	}
	RenderGui();
	//mCurrentLineVtx = 0;
	glUseProgram(0);

	SDL_GL_SwapWindow(WindowSys->GetWindow());
}

void RenderManager::RenderEditor()
{
	// Clear the frame buffer to black color.
	SetViewport(editor_camera->GetViewport());
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);
	editor_camera->Update();

	// Render all the spaces
	// sorted
	SortSpaces();
	for (auto& space : mSpaces)
	{
		if (space->Enabled())
		{
			// Render all the objects in the space
			glm::mat4 CamToProj = editor_camera->GetViewProjMatrix();

			RenderComponents<Renderable>(CamToProj, space);
			RenderComponents<SkeletonRenderable>(CamToProj, space);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			RenderComponents<TextRenderable>(CamToProj, space);
			GameObject*  obj = EditorSys->mStateMachine.GetPickedObject();
			if (obj)
			{
				ParticleEmitter* p = obj->GetComp<ParticleEmitter>();
				if (p)
				{
					p->GetShader()->Use();
					p->GetShader()->SetUniform("CamToProj", CamToProj);
					p->Render();
				}
					
			}
			//RenderComponents<ParticleEmitter>(CamToProj, space);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			RenderTranslucentComponents(CamToProj);
			space->RenderLines(CamToProj);
			space->SetCurrentLineVtx(0);
		}
		mActiveRenderables.clear();
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	if (SettingsSys->EditorViewport)
	{
		// Render all the objects in the space
		// Render in game camera to a little viewport
		SortSpaces();
		SetViewport(0.0f, 0.0f, 0.2f, 0.2f);
		for (auto& space : mSpaces)
		{
			if (space->Enabled())
			{
				if (SettingsSys->SpacePartitioning && space->SpacePartitioningEnabled())
				{
					mActiveRenderables.clear();
					mActiveEmitters.clear();
					space->GetActiveComponents<Renderable>(mActiveRenderables, "Renderable");
					space->GetActiveComponents<ParticleEmitter>(mActiveEmitters, "ParticleEmitter");
					//SortActiveRenderables();
				}

				for (auto& cam : space->GetComponents<CameraComponent>())
				{
					CameraComponent* camera = dynamic_cast<CameraComponent*>(cam);
					glm::mat4 CamToProj = camera->GetViewProjMatrix();

					//SortRenderableZ(space);
					RenderRenderables(camera, space);
					RenderComponents<SkeletonRenderable>(CamToProj, space);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					RenderComponents<TextRenderable>(CamToProj, space);
					//RenderComponents<ParticleEmitter>(CamToProj, space);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					RenderTranslucentComponents(CamToProj);
					space->RenderLines(CamToProj);
					space->SetCurrentLineVtx(0);
				}
			}
			glClear(GL_DEPTH_BUFFER_BIT);
		}
	}
	
	RenderGui();
	glUseProgram(0);
	SDL_GL_SwapWindow(WindowSys->GetWindow());
}
void RenderManager::Shutdown()
{
	FreeResources();
}

void RenderManager::SortRenderableZ(Space* sp)
{
	// differenciate between translucent and non translucent objects
	auto& Renderables = sp->GetComponents<Renderable>();
	std::sort(Renderables.begin(), Renderables.end(), [&](auto& render1, auto &render2)
	{
		return *reinterpret_cast<Renderable*>(render1) < *reinterpret_cast<Renderable*>(render2);
	});
}

void RenderManager::SortActiveRenderables()
{
	//std::sort(mActiveRenderables.begin(), mActiveRenderables.end(), [&](auto& render1, auto& render2)
	//{
	//	return *render1 < *render2;
	//});
}

void RenderManager::SortSpaces()
{
	//if(mSpaces.empty())
		mSpaces = SceneSys->GetAllSpaces();
	std::sort(mSpaces.begin(), mSpaces.end(), [&](auto& space1, auto& space2)
	{
		return *reinterpret_cast<Space*>(space1) < *reinterpret_cast<Space*>(space2);
	});
}

#pragma region Resource Management
Shader* RenderManager::LoadShader(const char* shaderName, const char* vtxShaderFile, const char* fragShaderFile)
{
	// avoid duplicates
	if (auto shader = GetShader(shaderName))
		return shader;

	// create a new shader 
	Shader* newShader = new Shader(shaderName, vtxShaderFile, fragShaderFile);
	mShaders[shaderName] = newShader;
	
	return newShader;
}

Texture* RenderManager::LoadTexture(const char* textureName, const char* filePath)
{
	// avoid duplicates
	if (auto tex = GetTexture(textureName))
		return tex;

	// create texture and set it in the map
	Texture* newTex = new Texture(filePath);
	int x = 0, y = 0, n = 0, reqComp = 4;	// out parameters
	stbi_set_flip_vertically_on_load(1);
	newTex->mPixels = (char*)stbi_load(filePath, &x, &y, &n, reqComp);	// 4 - forces output data to be of the form RGBA
	newTex->SetWidth(x);
	newTex->SetHeight(y);
	newTex->CreateOpenGLTexture();
	newTex->UploadToGPU();
	if (newTex->mPixels == NULL)
	{
		// TODO: provide error message
		return nullptr;
	}
	mTexturePaths.push_back(filePath);
	mTextures[textureName] = newTex;

	return newTex;
}

Mesh* RenderManager::LoadMesh(const char* filePath)
{
	// avoid duplicates
	if (auto mesh = GetMesh(filePath))
		return mesh;

	// constructor calls CreateOpenglMesh, which makes a quad mesh
	// so nothing else is needed.
	Mesh* newMesh = new Mesh;
	mMeshes[filePath] = newMesh;

	// load a quad to it
	Mesh::create_quad(*newMesh);

	return newMesh;
}

void RenderManager::LoadDefaultResources()
{
	LoadMesh("quad.mesh");

	LoadShader("gpu_basic.shader", "./data/shaders/gpu_basic.vtx", "./data/shaders/basic.frag");
	LoadShader("basic.shader", "./data/shaders/basic.vtx", "./data/shaders/basic.frag");
	LoadShader("particle.shader", "./data/shaders/InstancedSprite.vtx", "./data/shaders/basic.frag");
	LoadShader("line.shader", "./data/shaders/basic.vtx", "./data/shaders/Line.frag");
	LoadShader("spine.shader", "./data/shaders/spine.vtx", "./data/shaders/spine.frag");
	LoadShader("text.shader", "./data/shaders/text.vtx", "./data/shaders/text.frag");

	gFontInfo = new FontInfo();
	{
		LoadTTF("data/Fonts/edo.ttf", 32, 197, 512, 256, 48, gFontInfo, true, "data/Fonts/edo.png");
		// load the newly saved font texture into the engine. 
		LoadTexture("edo.png", "data/Fonts/edo.png");
	}
}

SpineData* RenderManager::LoadSpineData(const char* jsonPath, const char* atlasPath)
{
	if (auto spineData = GetSpineData(jsonPath))
		return spineData;

	// create new and load
	SpineData* spineData = new SpineData();
	spineData->jsonScale = 0.23f; // should be given as a parameter or read from a meta file. 
	spineData->animStateData = nullptr;
	spineData->Load(jsonPath, atlasPath);

	// add to resources
	mSpineDatas[jsonPath] = spineData;

	// return
	return spineData;
}

void RenderManager::DeleteTexture(std::string texName)
{
	for (auto& it : mTextures)
	{
		if (it.second == GetTexture(texName.c_str()))
		{
			delete it.second;
		}
	}
}

void RenderManager::FreeResources()
{
	for (auto& it : mShaders)
		delete it.second;

	for (auto& it : mMeshes)
		delete it.second;

	for (auto& it : mSpineDatas)
		delete it.second;

	for (auto& it : mTextures)
		delete it.second;
}

Shader* RenderManager::GetShader(const char* shaderName)
{
	auto it = mShaders.find(shaderName);
	if (it != mShaders.end())
		return it->second;
	return nullptr;
}

Texture* RenderManager::GetTexture(const char* textureName)
{
	auto it = mTextures.find(textureName);
	if (it != mTextures.end())
		return it->second;
	return nullptr;
}

Mesh* RenderManager::GetMesh(const char* meshName)
{
	auto it = mMeshes.find(meshName);
	if (it != mMeshes.end())
		return it->second;
	return nullptr;
}

SpineData* RenderManager::GetSpineData(const char* spineResName)
{
	auto it = mSpineDatas.find(spineResName);
	if (it == mSpineDatas.end())
		return nullptr;
	return it->second;
}

std::vector<Renderable*>& RenderManager::GetRenderables()
{
	return mActiveRenderables;
}

void RenderManager::SetRenderable(Renderable* rend)
{
	mActiveRenderables.push_back(rend);
	std::sort(mActiveRenderables.begin(), mActiveRenderables.end(), [&](auto& render1, auto& render2)
	{
		return *reinterpret_cast<Renderable*>(render1) < *reinterpret_cast<Renderable*>(render2);
	});
}

float RenderManager::GetEditorRatio()
{
	return editor_camera->mViewRectangle.x / WindowSys->GetWidth();
}

void RenderManager::SetViewport(float left, float bottom, float right, float top)
{
	float width = (float)WindowSys->GetWidth();
	float height = (float)WindowSys->GetHeight();
	glViewport(left * width, bottom * height,
		(right - left) * width,
		(top - bottom) * height);
	glScissor(left * width, bottom * height, GLsizei((right - left) * width), GLsizei((top - bottom) * height));
}

void RenderManager::SetViewport(Viewport& view)
{
	SetViewport(view.left, view.bottom, view.right, view.top);
}

void RenderManager::ClearViewport()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderManager::SetClearColor(Color& col)
{
	glClearColor(col.r, col.g, col.b, col.a);
}

void RenderManager::SetClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

Viewport& RenderManager::GetViewport()
{
	return editor_camera->mViewport;
}

FontInfo* RenderManager::GetFontInfo()
{
	return gFontInfo;
}

void RenderManager::RemoveActiveEmitter(ParticleEmitter* _Emitter)
{
	auto _ToRemove = std::find(mActiveEmitters.begin(), mActiveEmitters.end(), _Emitter);
	if (_ToRemove != mActiveEmitters.end())
		mActiveEmitters.erase(_ToRemove);
}

void RenderManager::Clear()
{
	mActiveRenderables.clear();
	mActiveSkeletons.clear();
	mActiveEmitters.clear();
	mTranslucentObj.clear();
}

glm::vec3 RenderManager::WorldToEditorCam(const glm::vec3& point)
{
	return editor_camera->GetViewMatrix() * glm::vec4(point, 1.0f);
}

glm::vec3 RenderManager::WorldToCam(const glm::vec3& point)
{
	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	CameraComponent* mCamera = nullptr;
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
	}

	if (mCamera)
	{
		return mCamera->GetViewMatrix() * glm::vec4(point, 1.0f);

	}

	return glm::vec3();
}

glm::vec3 RenderManager::CamToProj(const glm::vec3& point)
{
	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	CameraComponent* mCamera = nullptr;
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
	}

	if (mCamera)
	{
		return mCamera->GetProjectionMatrix() * glm::vec4(point, 1.0f);

	}

	return glm::vec3();
}

glm::vec3 RenderManager::EditorCamToProj(const glm::vec3& point)
{
	return editor_camera->GetProjectionMatrix() * glm::vec4(point, 1.0f);
}

glm::vec3 RenderManager::ProjToWindow(const glm::vec3& point)
{
	return WindowSys->NDCToWindow() * glm::vec4(point, 1.0f);
}

glm::vec3 RenderManager::WorldToEditorCamInv(const glm::vec3& point)
{
	return glm::inverse(editor_camera->GetViewMatrix()) * glm::vec4(point, 1.0f);
}

glm::vec3 RenderManager::WorldToCamInv(const glm::vec3& point)
{
	//return glm::vec3();
	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	CameraComponent* mCamera=nullptr;
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
	}

	if (mCamera)
	{
		return glm::inverse(mCamera->GetViewMatrix())* glm::vec4(point, 1.0f);
		
	}

	return glm::vec3();
}

glm::vec3 RenderManager::CamToProjInv(const glm::vec3& point)
{
	GameObject* camera = SceneSys->GetMainSpace()->FindObjectByName("Camera");
	CameraComponent* mCamera = nullptr;
	if (camera)
	{
		mCamera = camera->GetComp<CameraComponent>();
	}

	if (mCamera)
	{
		return glm::inverse(mCamera->GetProjectionMatrix()) * glm::vec4(point, 1.0f);

	}

	return glm::vec3();
}

glm::vec3 RenderManager::EditorCamToProjInv(const glm::vec3& point)
{
	return glm::inverse(editor_camera->GetProjectionMatrix()) * glm::vec4(point, 1.0f);
}

glm::vec3 RenderManager::ProjToWindowInv(const glm::vec3& point)
{
	return WindowSys->NDCToWindowInv () * glm::vec4(point, 1.0f);
}

void RenderManager::RenderGui()
{
	ImGui::Render();

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		SDL_Window* backup_current_window = SDL_GL_GetCurrentWindow();
		SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
	}
}

void RenderManager::RenderRenderables(CameraComponent* _Camera, Space* _Space)
{
	// If Space Partitioning is enabled iterate over the active renderables
	// Not the most elegant solution
	// TODO: Clean this
	if (SettingsSys->SpacePartitioning&&_Space->SpacePartitioningEnabled())
	{
		for (auto _Renderable : mActiveRenderables)// space->GetComponents<Renderable>())
		{
			if (_Renderable->IsVisible() && _Renderable->GetOwner()->Enabled() && _Renderable->Enabled() && !_Renderable->IsTranslucent())
			{
				_Renderable->GetShader()->Use();
				_Renderable->GetShader()->SetUniform("CamToProj", _Camera->GetViewProjMatrix());
				_Renderable->Render();
			}
			else if (_Renderable->IsTranslucent())
			{
				mTranslucentObj.push_back(reinterpret_cast<Renderable*>(_Renderable));
			}
		}
	}
	else
		RenderComponents<Renderable>(_Camera->GetViewProjMatrix(), _Space);
}

void RenderManager::RenderParticles(CameraComponent* _Camera, Space* _Space)
{
	if (SettingsSys->SpacePartitioning)
	{
		for (auto _Renderable : mActiveEmitters)// space->GetComponents<Renderable>())
		{
			if (_Renderable->IsVisible() && _Renderable->GetOwner()->Enabled() && _Renderable->Enabled())
			{
				_Renderable->GetShader()->Use();
				_Renderable->GetShader()->SetUniform("CamToProj", _Camera->GetViewProjMatrix());
				_Renderable->Render();
			}
		}
	}
	else
		RenderComponents<ParticleEmitter>(_Camera->GetViewProjMatrix(), _Space);
}

void RenderManager::RenderSkeletons(CameraComponent* _Camera, Space* _Space)
{
	// If Space Partitioning is enabled iterate over the active renderables
	// Not the most elegant solution
	// TODO: Clean this
	if (SettingsSys->SpacePartitioning && _Space->SpacePartitioningEnabled())
	{
		for (auto _Renderable : mActiveSkeletons)// space->GetComponents<Renderable>())
		{
			if (_Renderable->IsVisible() && _Renderable->GetOwner()->Enabled() && _Renderable->Enabled())
			{
				_Renderable->GetShader()->Use();
				_Renderable->GetShader()->SetUniform("CamToProj", _Camera->GetViewProjMatrix());
				_Renderable->Render();
			}
		}
	}
	else
		RenderComponents<SkeletonRenderable>(_Camera->GetViewProjMatrix(), _Space);
}

void RenderManager::RenderTranslucentComponents(const glm::mat4& _Camera)
{
	
	std::sort(mTranslucentObj.begin(), mTranslucentObj.end(), [&](auto& render1, auto& render2)
	{
		return *render1 < *render2;
	});
	for (auto& _ToRender : mTranslucentObj)
	{
		Renderable* _Renderable = _ToRender;
		// Render all the objects
		if (_Renderable->IsVisible() && _Renderable->GetOwner()->Enabled() && _Renderable->Enabled())
		{
			_Renderable->GetShader()->Use();
			_Renderable->GetShader()->SetUniform("CamToProj", _Camera);
			_Renderable->Render();
		}
	}
	mTranslucentObj.clear();
}

void RenderManager::ChangeWireframeMode()
{
	WireFrame = !WireFrame;
	if(WireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

#pragma endregion