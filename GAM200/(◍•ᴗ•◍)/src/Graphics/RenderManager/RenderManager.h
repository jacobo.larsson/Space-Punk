/**********************************************************************************/
/*!
\file   RenderManager.h
\author Maria Bola�os & David Miranda
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions, vectors, maps and pointers necessary to render throught
scenes and spaces. Works as gfxSystem.
*/
/*********************************************************************************/
#pragma once
#include <glm/glm.hpp>
//
#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IBase.h"
#include "../../Singleton/Singleton.h"

#include "../../Space/Space.h"
#include "../Camera/Camera.h"

class Shader;
class Texture;
class Mesh;
class Color;

class Renderable;
class SkeletonRenderable;
class Camera;
class Space;
class EditorCamera;
struct Viewport;
class CameraComponent;
class SpineData;
class ParticleEmitter;

struct FontInfo;

class RenderManager: public IBase
{ 
	friend class Editor;
	friend class Menus;
	SM_RTTI_DECL(RenderManager, IBase);
	friend class Renderable;
	Make_Singleton(RenderManager)
public:

#pragma region GameLoop
	bool Initialize();
	void Update();
	void LateUpdate();
	void Render();
	void RenderEditor();
	void Shutdown();
#pragma endregion

	// resource management
	Shader* LoadShader(const char * shaderName, const char* vtxShaderFile, const char * fragShaderFile);
	Texture* LoadTexture(const char* textureName, const char* filePath);
	Mesh* LoadMesh(const char* filePath);
	SpineData* LoadSpineData(const char* jsonPath, const char* atlasPath);

	// Getters
	Shader* GetShader(const char* shaderName);
	Texture* GetTexture(const char* textureName);
	Mesh* GetMesh(const char* meshName);
	SpineData* GetSpineData(const char* spineResName);
	std::vector<Renderable*>& GetRenderables();
	void SetRenderable(Renderable* rend);

	// debuggin
	void LoadDefaultResources();

	//void RenderLines(glm::mat4 mat = glm::mat4(1));
	//void DrawLine(glm::vec3 v0, glm::vec3 v1, Color col);
	//void DrawLine(glm::vec2 v0, glm::vec2 v1, Color col);
	//void Drawrectangle(glm::vec3 pos, int width, int height, Color color);
	//void DrawCircle(glm::vec3 pos, float radius, Color color, float precision = 10.0f);

	void DeleteTexture(std::string texName);
	void FreeResources();

	void SortRenderableZ(Space* space);
	void SortActiveRenderables();
	void SortSpaces();

	float GetEditorRatio();
	// Removes
	void SetViewport(float left, float bottom, float right, float top);
	void SetViewport(Viewport& view);
	void ClearViewport();
	void SetClearColor(Color& col);
	void SetClearColor(float r, float g, float b, float a);
	Viewport& GetViewport();
	FontInfo* GetFontInfo();
	void RemoveActiveEmitter(ParticleEmitter* _Emitter);

	void Clear();

#pragma region SpaceConversion
	// Forward direction
	glm::vec3 WorldToEditorCam(const glm::vec3& point);
	glm::vec3 WorldToCam(const glm::vec3& point);
	glm::vec3 CamToProj(const glm::vec3 & point);
	glm::vec3 EditorCamToProj(const glm::vec3& point);
	glm::vec3 ProjToWindow(const glm::vec3& point);

	// Inverse direction
	glm::vec3 WorldToEditorCamInv(const glm::vec3& point);
	glm::vec3 WorldToCamInv(const glm::vec3& point);
	glm::vec3 CamToProjInv(const glm::vec3& point);
	glm::vec3 EditorCamToProjInv(const glm::vec3& point);
	glm::vec3 ProjToWindowInv(const glm::vec3& point);

#pragma endregion
	

	EditorCamera* editor_camera = nullptr;
private:

	void RenderGui();
	void RenderRenderables(CameraComponent * _Camera, Space * _Space);
	void RenderParticles(CameraComponent* _Camera, Space* _Space);
	void RenderSkeletons(CameraComponent* _Camera, Space* _Space);
	template<typename T>
	void RenderComponents(const glm::mat4 & _Camera, Space* _Space);
	void RenderTranslucentComponents(const glm::mat4& TransObj);
	void ChangeWireframeMode();
	// In the first version of the render manager it will also handle the game objects
	// just to draw onto screen until composition is done
	// active components
	std::string active_shader;

	//EditorCamera* mini_viewport;
	// toggle wireframe
	bool WireFrame = false;
	// Resource management
	std::map<std::string, Shader*> mShaders;
	std::map<std::string, Texture*> mTextures;
	std::map<std::string,Mesh*> mMeshes;
	std::map<std::string, SpineData*> mSpineDatas;
	std::vector<std::string> mTexturePaths;
	std::vector<Renderable*> mTranslucentObj;

	std::vector<Renderable*> mActiveRenderables;
	std::vector<SkeletonRenderable*> mActiveSkeletons;
	std::vector<ParticleEmitter*> mActiveEmitters;
	std::vector<Space*> mSpaces;

	FontInfo* gFontInfo = nullptr;

	Mesh * mMesh = nullptr;
	Mesh* mLineMesh = nullptr;
	Mesh* mFontMesh = nullptr;
	int mCurrentLineVtx = 0;
};
#define RndMngr RenderManager::Get()

template<typename T>
inline void RenderManager::RenderComponents(const glm::mat4 & _Camera, Space* _Space)
{
	for (auto& _ToRender : _Space->GetComponents<T>())
	{
		T* _Renderable = reinterpret_cast<T*>(_ToRender);
		// Render all the objects
		if (_Renderable->IsVisible() && _Renderable->GetOwner()->Enabled() && _Renderable->Enabled() && !_Renderable->IsTranslucent())
		{
			_Renderable->GetShader()->Use();
			_Renderable->GetShader()->SetUniform("CamToProj", _Camera);
			_Renderable->Render();
		}
		else if(_Renderable->IsTranslucent())
		{
			mTranslucentObj.push_back(reinterpret_cast<Renderable*>(_Renderable));
		}
	}
}

