/**********************************************************************************/
/*!
\file   Renderable.h
\author Maria Bola�os
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions to render each Game object. It is serialized.
*/
/*********************************************************************************/
#pragma once
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"
#include "../../RTTI/SM_RTTI.h"
#include "../Color/Color.h"
#include "../../RTTI/IComp.h"
#include "../../ResourceManager/ResourceManager.h"
#include "../../EventSystem/Listener/Listener.h"
#include "../../EventSystem/EventHandler.h"

class Shader;
class Mesh;
class Texture;
class DumpResource;

class Renderable : public IComp
{
	SM_RTTI_DECL(Renderable, IComp);
	friend class Editor;
	friend class Menus;
public:
	Renderable();
	virtual ~Renderable();

	Renderable& operator=(const Renderable& rhs);
	bool operator<(const Renderable & rhs);
	Renderable* Clone()override;
	void Initialize() override;
	virtual void Render();
	void Shutdown()override;
	bool Edit()override;

	// GETTORS AND SETTORS
	bool IsVisible();
	void SetVisible(bool visible);
	Resource<Texture>* GetTextures(); 
	void SetTexture(Resource<Texture>* tex);
	Shader* GetShader();
	void SetShader(Shader* shader);
	Mesh* GetMesh();
	void SetMesh(Mesh* mesh);
	bool IsFlipX();
	void SetFlipX(bool flip);
	bool IsFlipY();
	void SetTranslucent(bool translu);
	bool IsTranslucent();
	void SetFlipY(bool flip);
	Color GetModulationColor();
	void SetModulationColor(Color col);

	void SetTexture(std::string filename);

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


#pragma region Events
	//void handle_event(const Event& _event)override;
	//EVENTHANDLER_DECL
		virtual void OnDumpResource(const DumpResource& _event);

protected:
	bool mIsVisible = true;
	Resource<Texture>* mTexture;
	Shader* mShader = nullptr;
	Mesh* mMesh = nullptr;
	bool flipX = false;
	bool flipY = false;
	Color ModulColor;
	bool Translucent = false;
};