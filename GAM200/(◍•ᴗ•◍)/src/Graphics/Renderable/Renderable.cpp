/**********************************************************************************/
/*!
\file   Renderable.cpp
\author Maria Bola�os
\par    email: maria.bolanos@digipen.edu
\par    DigiPen login: maria.bolanos
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Renderable.h
*/
/*********************************************************************************/
#include <map>
#include "../Shaders/Shader.h"
#include "../Mesh/Mesh.h"
#include "../Texture/Texture.h"

#include "../RenderManager/RenderManager.h"
#include "../Color/Color.h"
#include "../../ResourceManager/ResourceManager.h"
#include "../../Factory/Factory.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../GameObject/GameObject.h"
#include "../../OpenSaveFile/OpenSaveFile.h"

#include "../../EventSystem/EventDispatcher.h"
#include "../../EventSystem/Events/ResourcesEvents/ResourcesEvents.h"

#include "Renderable.h"

Renderable::Renderable():ModulColor()
{
	SetName("Renderable");
	mMesh = RndMngr->GetMesh("quad.mesh");
	mTexture = reinterpret_cast<Resource<Texture>*>( RsrcMan->GetResource("./data/Textures/bakugo.png"));
	mShader = RndMngr->GetShader("gpu_basic.shader");
	ModulColor.a = 1.0f;
	EventSys->subscribe(this, DumpResource());
	mEventHandler.register_handler(*this, &Renderable::OnDumpResource);
	
}
Renderable::~Renderable() 
{
	EventSys->unsubscribe(this, DumpResource());
}

Renderable& Renderable::operator=(const Renderable& rhs)
{
	mEnabled =  rhs.mEnabled;

	mShader = rhs.mShader;
	mIsVisible = rhs.mIsVisible;
	mTexture = RsrcMan->GetResourceType<Texture>(rhs.mTexture->RelPath);
	ModulColor = rhs.ModulColor;
	flipX = rhs.flipX;
	flipY = rhs.flipY;

	return *this;
}
bool Renderable::operator<(const Renderable& rhs)
{
	if (mOwner->HasComp<TransformComponent>() && rhs.mOwner->HasComp<TransformComponent>())
	{
		return mOwner->GetComp<TransformComponent>()->GetWorldPosition().z < rhs.mOwner->GetComp<TransformComponent>()->GetWorldPosition().z;
	}
	std::cout << "Failed sorting Renderables, one of them does not have Transform " << std::endl;
	return false;
}
Renderable* Renderable::Clone()
{
	Renderable* temp = FactorySys->Create<Renderable>();
	(*temp) = (*this);
	return temp;
}

void Renderable::Initialize()
{
	//RndMngr->AddRenderable(this);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
void Renderable::Render()
{
	if (auto transform = mOwner->GetComp<TransformComponent>())
	{

		mShader->SetUniform("Identity", glm::mat4(1.0f));
		mShader->SetUniform("Pos", transform->GetWorldPosition());
		mShader->SetUniform("Scale", transform->GetWorldScale());
		mShader->SetUniform("Rotation", transform->GetWorldOrientation());
		// Set the matrixes for the shaders
		//glm::mat4 Model2World = transform->GetWorldTransform().GetMatrix();
		//mShader->SetUniform("Model2World", Model2World);

		mShader->SetUniform("flipX", flipX);
		mShader->SetUniform("flipY", flipY);

		mShader->SetUniform("ModulColor", ModulColor);
		// set the texture
		glActiveTexture(GL_TEXTURE0);
		//mTexture->Bind();
		mTexture->get()->Bind();
		mShader->SetUniform("texture1", 0);
		mMesh->Draw();

	}
	else
	{
		std::cout << "DEBUG::RENDERABLE: " << mOwner->GetName().c_str() << " does not have transform component" << std::endl;
	}
}
void Renderable::Shutdown()
{
	//RndMngr->RemoveRenderable(this);
}
bool Renderable::Edit()
{
	bool changed = false;
	ImGui::Checkbox("Visible", &mIsVisible);

	if (ImGui::Button("Add Texture"))
	{
		std::string filename; // default;
		OpenSaveFileDlg dlg;
		if (dlg.Open("Open Texture")) {
			filename = dlg.GetFiles().front();
		}

		size_t found = filename.find("Textures");
		if (found != std::string::npos)
		{

			std::string slash = filename.substr(found);
			filename = std::string("./") + filename.substr(found,8) + std::string("/") + filename.substr(found + 9);
			auto extension = RsrcMan->GetExtensionOfFile(filename);
			if (extension == TypeTexture)
				RsrcMan->Load(filename);
		}
	}

	static int texIndex = int(std::distance(RsrcMan->Resourcesmap[TypeTexture].begin(), RsrcMan->Resourcesmap[TypeTexture].find(mTexture->RelPath)));

	static std::string active_str = RsrcMan->Resourcesmap[TypeTexture].find(mTexture->RelPath)->first.c_str();

	auto it = RsrcMan->Resourcesmap[TypeTexture].begin();

	if (ImGui::BeginCombo("Texture", active_str.c_str(), 0))
	{
		for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeTexture].size(); i++, it++)
		{

			if (ImGui::Selectable(it->first.c_str(), texIndex == i))
			{
				texIndex = i;
				active_str = it->first.c_str();
				SetTexture(it->first.c_str());
				changed = true;
				std::string text_name = it->first;
				Resource<Texture>* texture_to_get = RsrcMan->GetResourceType<Texture>(text_name);
				TransformComponent* to_transform = mOwner->GetComp<TransformComponent>();

				if (to_transform)
				{
					float format = (float)texture_to_get->get()->GetWidth()/ (float)texture_to_get->get()->GetHeight();

					float actual_x = to_transform->GetScale().x;
					float actual_y = to_transform->GetScale().y;

					float prev_format = actual_x / actual_y;

					float to_multiply = format / prev_format;



					to_transform->SetScale(glm::vec2(actual_x* to_multiply, actual_y));


				}

			}

			if (texIndex == i)
				ImGui::SetItemDefaultFocus();

			if (ImGui::IsItemHovered())
			{
				std::string text_name = it->first;
				Resource<Texture>* texture_to_get = RsrcMan->GetResourceType<Texture>(text_name);
				ImGui::BeginTooltip();
				float my_tex_w = float(texture_to_get->get()->GetWidth());
				float my_tex_h = float(texture_to_get->get()->GetHeight());
				ImVec2 uv_min = ImVec2(-1.0f, 1.0f);
				ImVec2 uv_max = ImVec2(0.0f, 0.0f);
				ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
				ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f);
				ImGui::Image((void*)(intptr_t)texture_to_get->get()->GetGLHandle(), ImVec2(my_tex_w / 15, my_tex_h / 15), uv_min, uv_max, tint_col, border_col);
				ImGui::EndTooltip();

			}
		}
		ImGui::EndCombo();
	}

	ImGui::ColorPicker4("Color Modulation", &ModulColor.r);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::SliderFloat("Alpha", &ModulColor.a, 0.f, 1.f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::Checkbox("FlipX", &flipX);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	ImGui::Checkbox("FlipY", &flipY);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::Checkbox("Translucent", &Translucent);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

bool Renderable::IsVisible()
{
	return mIsVisible;
}
void Renderable::SetVisible(bool visible)
{
	mIsVisible = visible;
}
Resource<Texture>* Renderable::GetTextures()
{
	return mTexture;
}
void Renderable::SetTexture(Resource<Texture>* tex)
{
	mTexture = tex;
}
Shader* Renderable::GetShader()
{
	return mShader;
}
void Renderable::SetShader(Shader* shader)
{
	mShader = shader;
}
Mesh* Renderable::GetMesh()
{
	return mMesh;
}
void Renderable::SetMesh(Mesh* mesh)
{
	mMesh = mesh;
}
bool Renderable::IsFlipX()
{
	return flipX;
}
void Renderable::SetFlipX(bool flip)
{
	flipX = flip;
}
bool Renderable::IsFlipY()
{
	return flipY;
}
void Renderable::SetTranslucent(bool translu)
{
	Translucent = translu;
}
bool Renderable::IsTranslucent()
{
	return Translucent;
}
void Renderable::SetFlipY(bool flip)
{
	flipY = flip;
}
Color Renderable::GetModulationColor()
{
	return ModulColor;
}
void Renderable::SetModulationColor(Color col)
{
	ModulColor = col;
}
void Renderable::SetTexture(std::string filename)
{
	mTexture = RsrcMan->GetResourceType<Texture>(filename.c_str());
	if (mTexture == nullptr)
		std::cout << "FAILED LOADING TEXTURE "<< filename << std::endl;
}

void Renderable::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["IsVisible"] = mIsVisible;
	j["CurrentTex"] = mTexture->RelPath;
	json& Col = j["Color_Modulation"];
	Col["Color"].push_back(ModulColor.r);
	Col["Color"].push_back(ModulColor.g);
	Col["Color"].push_back(ModulColor.b);
	Col["Color"].push_back(ModulColor.a);
	j["FlipX"] = flipX;
	j["FlipY"] = flipY;
	j["Translucent"] = Translucent;
}
void Renderable::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);

	if(j.find("IsVisible") != j.end())
		mIsVisible = j["IsVisible"];
	if (j.find("CurrentTex") != j.end())
	{
		SetTexture(j["CurrentTex"].get<std::string>());
	}
	if (j.find("Color_Modulation") != j.end())
	{
		if (j["Color_Modulation"].find("Color") != j["Color_Modulation"].end() && j["Color_Modulation"]["Color"].size() >= 3)
		{
			ModulColor.r = j["Color_Modulation"]["Color"][0];
			ModulColor.g = j["Color_Modulation"]["Color"][1];
			ModulColor.b = j["Color_Modulation"]["Color"][2];
			ModulColor.a = j["Color_Modulation"]["Color"][3];
		}
	}
	if (j.find("FlipX") != j.end())
		flipX = j["FlipX"];
	if (j.find("FlipY") != j.end())
		flipY = j["FlipY"];
	if (j.find("Translucent") != j.end())
		Translucent = j["Translucent"];
}

void Renderable::OnDumpResource(const DumpResource& _event)
{
	std::string a = mTexture->RelPath;
	auto it = std::find(RsrcMan->current_level_resources.begin(), RsrcMan->current_level_resources.end(), a);
	if(it== RsrcMan->current_level_resources.end())
		RsrcMan->current_level_resources.push_back(mTexture->RelPath);
}
