#include "Font.h"

// include STB
#pragma warning (disable:4996) //  fopen': This function or variable may be unsafe. 
#define PACK_OVERSAMPLE 1
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_RECT_PACK_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "../../Extern/STB/stb_image.h"
#include "../../Extern/STB/stb_image_write.h"
#include "../../Extern/STB/stb_rect_pack.h"
#include "../../Extern/STB/stb_truetype.h"

#include "../Mesh/Mesh.h"
#include "../Renderable/Renderable.h"
#include "../Shaders/Shader.h"
#include "../RenderManager/RenderManager.h"

#include "../../Transform2D/TransformComponent.h"
#include "../../GameObject/GameObject.h"


#pragma warning (default:4996) //  fopen': This function or variable may be unsafe. 

#pragma region// CORE API
int AEPackFontRange(stbtt_fontinfo* STBFontInfo, stbtt_pack_range* range, int bitmap_width, int bitmap_height, char** pixels)
{
	// output buffer containing the packed characters rectangles
	range->chardata_for_range = (stbtt_packedchar*)malloc(sizeof(stbtt_packedchar) * range->num_chars);

	// flag all characters as NOT packed
	for (int i = 0; i < range->num_chars; ++i)
		range->chardata_for_range[i].x0 =
		range->chardata_for_range[i].y0 =
		range->chardata_for_range[i].x1 =
		range->chardata_for_range[i].y1 = unsigned short(0);

	// begin packing process
	stbtt_pack_context pc;
	stbtt_PackBegin(&pc, NULL, bitmap_width, bitmap_height, 0, 1, NULL);
	stbtt_PackSetOversampling(&pc, PACK_OVERSAMPLE, PACK_OVERSAMPLE); // OPTIONAL

	stbrp_rect* rects;


	// Alloc packed rectangles
	rects = (stbrp_rect*)std::malloc(sizeof(*rects) * range->num_chars);
	assert(rects != NULL);// failure , "ERROR WHILE LOADING FONT"
	//DebugAssert(rects != NULL, "ERROR WHILE LOADING FONT"); 

	// gather the number of rectangles from the ranges.
	int n = stbtt_PackFontRangesGatherRects(&pc, STBFontInfo, range, 1, rects);

	// pack them...
	stbtt_PackFontRangesPackRects(&pc, rects, n);

	// render if necessary to write to PNG file
	if (pixels)
	{
		// bitmap to render to
		int bmsize = sizeof(unsigned char) * bitmap_width * bitmap_height;
		pc.pixels = (unsigned char*)malloc(bmsize);
		std::memset(pc.pixels, 0, bmsize);

		// render packed font ranges
		int res = stbtt_PackFontRangesRenderIntoRects(&pc, STBFontInfo, range, 1, rects);
		assert(res != 0); // , "problem when packing font"// failure
		//DebugAssert(res != 0, "problem when packing font");

		// transfer the pixels.
		*pixels = (char*)pc.pixels;
		pc.pixels = 0;
	}

	// done packing
	stbtt_PackEnd(&pc);

	// success
	return 1;
}

void LoadTTF(const char* fileName,
	int start_char, int end_char,
	int bitmap_width, int bitmap_height,
	float font_size, FontInfo* outFontInfo,
	bool saveToTexture,
	const char* textureFilename)
{
	if (saveToTexture && textureFilename != NULL) {
		char* pixels;
		LoadTTF(fileName, start_char, end_char, bitmap_width, bitmap_height, font_size, outFontInfo, &pixels);
		// no texture filename, just save to same dir and name
		stbi_write_png(textureFilename, bitmap_width, bitmap_height, 1, pixels, 0);
		free(pixels);
		return;
	}
	LoadTTF(fileName, start_char, end_char, bitmap_width, bitmap_height, font_size, outFontInfo, (char**)NULL);
}

void LoadTTF(const char* fileName,
	int start_char, int end_char, 
	int bitmap_width, int bitmap_height, 
	float font_size, FontInfo* outFontInfo,
	char** outPixels)
{
	// Sanity Checks
	assert(fileName != NULL); //, "No Font file name provided"
	assert(outFontInfo != NULL); //, "No Font Info provided when loading the font file"

#pragma region LOAD FONT FILE

	// font info from stbtt
	stbtt_fontinfo STBFontInfo;

	// open font file and load ttf file in binary
	FILE* fontFilePtr = NULL;
	fopen_s(&fontFilePtr, fileName, "rb");

	// get how big is the file
	fseek(fontFilePtr, 0, SEEK_END);	// set the current position to end of file (note: this technique is not portable)
	int size = ftell(fontFilePtr);		// get the current position to get the size of the file.
	fseek(fontFilePtr, 0, SEEK_SET);	// reset the current position to the beginning.

	// buffer to hold the ttf file data
	unsigned char* ttf_buffer = (unsigned char*)malloc(size);
	fread(ttf_buffer, 1, size, fontFilePtr);

	// close the file
	fclose(fontFilePtr);

	// Initialize the font info structure
	int font_index = 0; // most files only have one font in them... this is here for completeness
	int font_start = stbtt_GetFontOffsetForIndex(ttf_buffer, font_index);
	stbtt_InitFont(&STBFontInfo, ttf_buffer, font_start);
#pragma endregion

#pragma region PACK FONTS AND SAVE FONT TEXTURE
	//
	// Pack the font data into a  bitmap
	stbtt_pack_range range;
	range.first_unicode_codepoint_in_range = start_char;
	range.array_of_unicode_codepoints = NULL;
	range.num_chars = end_char - start_char;
	range.font_size = font_size;
	AEPackFontRange(&STBFontInfo, &range, bitmap_width, bitmap_height, outPixels);

	// Note: At this point, out pixel is a single alpha channel, convert it to RGBA, or use a 
	// custom shader. This example chooses the second. option. 

#pragma endregion

#pragma region GENERATE FONTINFO GLYPH CONTAINER

		// Store basic information
	outFontInfo->mFirstCharacterCode = start_char;
	outFontInfo->mLastCharacterCode = end_char;
	outFontInfo->mCharacterCount = range.num_chars;
	outFontInfo->mFontSize = font_size;

	int num_char = range.num_chars;
	stbtt_packedchar* pdata = range.chardata_for_range;

	// allocate memory for the glyph and kerning table
	outFontInfo->mGlyphContainer = (Glyph*)(malloc(sizeof(Glyph) * num_char));
	outFontInfo->mKerningTable = (float**)(malloc(sizeof(float*) * num_char));
	for (int i = 0; i < num_char; ++i)
	{
		outFontInfo->mKerningTable[i] = (float*)(malloc(sizeof(float) * num_char));
		for (int j = 0; j < num_char; ++j)
			outFontInfo->mKerningTable[i][j] = 0.0f;
	}

	//
	// Compute font metrics and glyph data

	// Compute scale for a specific font_size. 
	float scale;
	scale = stbtt_ScaleForPixelHeight(&STBFontInfo, font_size);

	// compute line gap
	int ascent, descent, lineGap;
	stbtt_GetFontVMetrics(&STBFontInfo, &ascent, &descent, &lineGap);
	float ascentf = scale * (float)ascent;
	float descentf = scale * (float)descent;
	float lineGapf = scale * (float)lineGap;
	outFontInfo->mLineGap = ascentf - descentf + lineGapf;


	// 
	// Extract glyph specific metrics such as texture coordinates in bitmap from packed data and
	// bounding box of the glyph character

	// Retrieve the coordinates in the texture
	for (int char_index = start_char; char_index < end_char; ++char_index)
	{
		// get the index of the character in the array
		int array_index = char_index - start_char; // this is by the way how to retrieve it with AEX::FontInfo (TODO(Thomas) provide method to do this)

		// get the glyph index from the font (this will increase performance according to STB_TT)
		int glyph_index = stbtt_FindGlyphIndex(&STBFontInfo, char_index);

		// get the glyph metrics
		int x0 = 0, y0 = 0, x1 = 0, y1 = 0; // unscaled glyph bounding box values
		stbtt_GetGlyphBox(&STBFontInfo, glyph_index, &x0, &y0, &x1, &y1);

		// Texture coodinates
		float xpos = 0.0f, ypos = 0.0f; // dummy values
		stbtt_aligned_quad aquad;
		stbtt_GetPackedQuad(pdata, bitmap_width, bitmap_height, array_index,
			&xpos, &ypos, &aquad, 0);

		// convert to positive y axis up
		aquad.t0 = 1.0f - aquad.t0;
		aquad.t1 = 1.0f - aquad.t1;
		float temp = aquad.t0;
		aquad.t0 = aquad.t1;
		aquad.t1 = temp;

		// store data into Glyph array
		// note: The glyph array stores the character in an array whose 
		// index doesn't relate to the character code in any way. 
		// use AEX::FontInfo to retrieve the glyph index based on a character
		// code.

		// Get the glyph
		Glyph& glyph = outFontInfo->mGlyphContainer[array_index];

		// Get advance width
		int advanceWidth;
		stbtt_GetGlyphHMetrics(&STBFontInfo, glyph_index, &advanceWidth, 0); // last param is for left side bearing which is not used in our case
		glyph.mAdvanceWidth = scale * (float)advanceWidth;

		// store the character code
		glyph.mCharacterCode = char_index;

		// hack: if the character code is space and it doesn't exist in the font, then set it to dummy values. 
		if (char_index == 32 && x0 == 0 && x1 == 0)
			x1 = int(2.0f * glyph.mAdvanceWidth);

		// store the bounding box
		glyph.mX0 = scale * (float)x0;
		glyph.mX1 = scale * (float)x1;
		glyph.mY0 = scale * (float)y0;
		glyph.mY1 = scale * (float)y1;

		// store the texture coordinates
		glyph.mU0 = aquad.s0;
		glyph.mU1 = aquad.s1;
		glyph.mV0 = aquad.t0;
		glyph.mV1 = aquad.t1;

		// get the kerning info for all the remaining characters. 
		for (int other_char_index = start_char;
			other_char_index < end_char;
			++other_char_index)
		{
			int other_glyph_index = stbtt_FindGlyphIndex(&STBFontInfo, other_char_index);
			int kern_advance = stbtt_GetGlyphKernAdvance(&STBFontInfo, glyph_index, other_glyph_index);
			int other_array_index = other_char_index - start_char;
			outFontInfo->mKerningTable[array_index][other_array_index] = float(scale * (float)kern_advance);
		}
	}
#pragma endregion

	// clean up
	free(range.chardata_for_range);
	free(ttf_buffer);
}

void ComputeTextMesh(const char* text, FontInfo* fontInfo, Mesh* outMesh)
{

	// Sanity Checks
	assert(fontInfo); //, "ComputeTextMesh: Invalid AEGfxFont structure"
	assert(text != NULL); //, "ComputeTextMesh: You must provide a valid text"
	assert(outMesh != NULL); //, "ComputeTextMesh: You must provide a valid Model class"

	// return if any of the assert above continues
	if (NULL == fontInfo || NULL == text || NULL == outMesh)
		return;

	// start position - bottom left
	glm::vec2 startPos = { 0, 0 };

	// we will create a quad for each letter
	int len = int(strlen(text));
	int loop_counter = 0; int i = 0;
	while (loop_counter < len)
	{
		// get the character
		int char_index = *(text + loop_counter);

		// new line
		if (char_index == '\n')
		{
			startPos.y -= fontInfo->mLineGap;
			startPos.x = 0;
			loop_counter++;
			continue;
		}

		// get the glyph index in the font info
		int glyph_index = char_index - fontInfo->mFirstCharacterCode;

		// get the glyp
		Glyph& glyph = fontInfo->mGlyphContainer[glyph_index];

		// make a quad
		Vertex vtx[4];

		vtx[0].position = { startPos + glm::vec2(glyph.mX0, glyph.mY1), 19.0f };
		vtx[1].position = { startPos + glm::vec2(glyph.mX0, glyph.mY0), 19.0f };
		vtx[2].position = { startPos + glm::vec2(glyph.mX1, glyph.mY0), 19.0f };
		vtx[3].position = { startPos + glm::vec2(glyph.mX1, glyph.mY1), 19.0f };

		// texture coordinates
		vtx[0].UV = glm::vec2(glyph.mU0, glyph.mV1);
		vtx[1].UV = glm::vec2(glyph.mU0, glyph.mV0);
		vtx[2].UV = glm::vec2(glyph.mU1, glyph.mV0);
		vtx[3].UV = glm::vec2(glyph.mU1, glyph.mV1);

		// color (always white)
		vtx[0].color = Color(1, 1, 1, 1);
		vtx[1].color = Color(1, 1, 1, 1);
		vtx[2].color = Color(1, 1, 1, 1);
		vtx[3].color = Color(1, 1, 1, 1);

		// add to the model as a quad
		outMesh->AddVertex(vtx[0]);
		outMesh->AddVertex(vtx[1]);
		outMesh->AddVertex(vtx[2]);
		outMesh->AddVertex(vtx[0]);
		outMesh->AddVertex(vtx[2]);
		outMesh->AddVertex(vtx[3]);

		outMesh->AddIndex(i++);
		outMesh->AddIndex(i++);
		outMesh->AddIndex(i++);
		outMesh->AddIndex(i++);
		outMesh->AddIndex(i++);
		outMesh->AddIndex(i++);

		// advance 
		startPos.x += glyph.mAdvanceWidth;

		// get kerning
		if (text + loop_counter + 1)
		{
			int next_char_index = *(text + loop_counter + 1);
			int next_glyph_index = next_char_index - fontInfo->mFirstCharacterCode;

			float kern_advance = fontInfo->mKerningTable[glyph_index][next_glyph_index];

			startPos.x += kern_advance;
		}

		// next character
		loop_counter++;
	}//	loop over each character in text
}

#pragma endregion

#pragma region// TEXT RENDERABLE

TextRenderable::TextRenderable()
{
	SetName("TextRenderable");
	this->mMesh = new Mesh();

	mShader = RndMngr->GetShader("text.shader");
	mFontTexture = RndMngr->GetTexture("edo.png");
	pFontInfo = RndMngr->GetFontInfo();
}

void TextRenderable::Initialize()
{
	Renderable::Initialize();
	// allocate memory for a custom text mehs
	//this->mMesh = new Mesh();
	
}
void TextRenderable::Shutdown()
{
	Renderable::Shutdown();
}
void TextRenderable::Render()
{
	if (auto transform = mOwner->GetComp<TransformComponent>())
	{
		glm::mat4 Model2World = transform->GetWorldTransform().GetMatrix() / mTransform.GetMatrix();

		// Set the matrixes for the shaders
		glm::mat4 scale(1.0f);
	
		mShader->SetUniform("Model2World", Model2World);
		
		mShader->SetUniform("textColor", col);

		// set the texture
		glActiveTexture(GL_TEXTURE0);
		//mTexture->Bind();
		mFontTexture->Bind();
		//mTexture->get()->Bind();
		mShader->SetUniform("text", 0);
		mMesh->Draw();
	}
	else
	{
		std::cout << "DEBUG::RENDERABLE: " << mOwner->GetName().c_str() << " does not have transform component" << std::endl;
	}
}
bool TextRenderable::Edit()
{
	ImGui::Checkbox("Visible", &mIsVisible);
	static char temp[500] = "";
	ImGui::InputTextMultiline("FontText", temp, 500);
	if(ImGui::Button("Apply"))
		SetText(temp);
	//ImGui::DragFloat("Scale", &this->pFontInfo->mFontSize);
	if(ImGui::CollapsingHeader("Text Color"))
		ImGui::ColorPicker3("Color", &col.r);
	
	ImGui::DragFloat2("Scale X", &mTransform.mScale.x, 1.0f, -500.0f, 500.0f);
	
	ImGui::DragFloat3("Position", &mTransform.mPosition.x, 1.0f, -500.0f, 500.0f);
	
	return true;
}
const std::string& TextRenderable::GetText() const
{
	// return the text as is. 
	return mText;
}
void TextRenderable::SetText(const std::string& newText)
{
	// sanity check (avoid unnecessary work).
	if (mText == newText) return;

	// save text
	mText = newText;

	// clear the model
	mMesh->Clear();
	ComputeTextMesh(mText.c_str(), pFontInfo, mMesh);

	// upload the mesh to GPU
	mMesh->UploadToGPU();
}
void TextRenderable::AppendText(const std::string& addText)
{
	// todo, implement me
	if (mText.empty())
		mText = addText;
	else
		mText.append(addText);

	// clear the model
	mMesh->Clear();
	ComputeTextMesh(mText.c_str(), pFontInfo, mMesh);

	// upload the mesh to GPU
	mMesh->UploadToGPU();
}
void TextRenderable::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);

	j["IsVisible"] = mIsVisible;
	j["Text"] = mText;
	//j["CurrentTex"] = mTexture->RelPath;
	json& Col = j["Text_Color"];
	Col["Color"].push_back(col.r);
	Col["Color"].push_back(col.g);
	Col["Color"].push_back(col.b);

	json& transform = j["Text_Transform"];
	transform["Scale"].push_back(mTransform.mScale.x);
	transform["Scale"].push_back(mTransform.mScale.y);
	transform["Scale"].push_back(mTransform.mScale.z);

	transform["Translation"].push_back(mTransform.mPosition.x);
	transform["Translation"].push_back(mTransform.mPosition.y);
	transform["Translation"].push_back(mTransform.mPosition.z);
}
void TextRenderable::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	if (j.find("IsVisible") != j.end())
		mIsVisible = j["IsVisible"];

	if (j.find("Text") != j.end())
	{
		SetText(j["Text"].get<std::string>());
	}
	if (j.find("Text_Color") != j.end())
	{
		if (j["Text_Color"].find("Color") != j["Text_Color"].end() && j["Text_Color"]["Color"].size() >= 3)
		{
			col.r = j["Text_Color"]["Color"][0];
			col.g = j["Text_Color"]["Color"][1];
			col.b = j["Text_Color"]["Color"][2];
		}
	}
	if (j.find("Text_Transform") != j.end())
	{
		if (j["Text_Transform"].find("Scale") != j["Text_Transform"].end() && j["Text_Transform"]["Scale"].size() >= 3)
		{
			mTransform.mScale.x = j["Text_Transform"]["Scale"][0];
			mTransform.mScale.y = j["Text_Transform"]["Scale"][1];
			mTransform.mScale.z = j["Text_Transform"]["Scale"][2];
		}
		if (j["Text_Transform"].find("Translation") != j["Text_Transform"].end() && j["Text_Transform"]["Translation"].size() >= 3)
		{
			mTransform.mPosition.x = j["Text_Transform"]["Translation"][0];
			mTransform.mPosition.y = j["Text_Transform"]["Translation"][1];
			mTransform.mPosition.z = j["Text_Transform"]["Translation"][2];
		}
	}
}
#pragma endregion