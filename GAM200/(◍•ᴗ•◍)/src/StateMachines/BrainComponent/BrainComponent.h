#pragma once

#include <vector>
#include <string>

#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IComp.h"
#include "../../Extern/json/json.hpp"

#include "../../LogicSystem/LogicComponent.h"

class StateMachine;

class BrainComponent : public LogicComponent
{
	friend class Editor;
	friend class Menus;
	friend class State;
	SM_RTTI_DECL(BrainComponent, IComp);
public:

	BrainComponent();
	~BrainComponent();
	BrainComponent& operator=(const BrainComponent& rhs);
	BrainComponent* Clone()override;

	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionPersisted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	StateMachine* GetStateMachine(const char* name);
	StateMachine* AddStateMachine(StateMachine* state);
	void RemoveStateMachine(StateMachine* state);

	bool Edit()override;

private:
	std::vector<StateMachine*> mMyStateMachines;
};