#include <iostream>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Editor/Editor.h"
#include "../../Editor/Menus/Menus.h"
#include "../../GameObject/GameObject.h"

#include "../StateMachine/StateMachine.h"
#include "../../Factory/Factory.h"
#include "../../Editor/Menus/Menus.h"

#include "BrainComponent.h"

BrainComponent::BrainComponent() 
{
	mName = "BrainComponent";
}
BrainComponent::~BrainComponent() 
{
	Shutdown();
}
BrainComponent& BrainComponent::operator=(const BrainComponent& rhs)
{
	mMyStateMachines.clear();
	for (int i = 0; i < rhs.mMyStateMachines.size(); i++)
	{
		StateMachine* newstatem = rhs.mMyStateMachines[i]->Clone();//reinterpret_cast<StateMachine*>(FactorySys->Create<StateMachine>());

		AddStateMachine(newstatem);
	}

	return *this;
}
BrainComponent* BrainComponent::Clone()
{
	BrainComponent* temp = FactorySys->Create<BrainComponent>();
	(*temp) = (*this);
	return temp;
}

void BrainComponent::OnCollisionStarted(GameObject* other)
{
	for (auto& state : mMyStateMachines) {

		state->OnCollisionStarted(other);
	}
}
void BrainComponent::OnCollisionPersisted(GameObject* other)
{
	for (auto& state : mMyStateMachines) {

		state->OnCollisionPersisted(other);
	}
}
void BrainComponent::OnCollisionEnded(GameObject* other)
{
	for (auto& state : mMyStateMachines) {

		state->OnCollisionEnded(other);
	}
}

void BrainComponent::OnCreate()
{}
void BrainComponent::Initialize()
{
	LogicComponent::Initialize();
	for (auto it = mMyStateMachines.begin(); it != mMyStateMachines.end(); it++)
	{
		(*it)->mActor = mOwner;
		(*it)->Initialize();
	}
}
void BrainComponent::Update()
{
	for (auto it = mMyStateMachines.begin(); it != mMyStateMachines.end(); it++)
		(*it)->Update();
}
void BrainComponent::Shutdown()
{
	for (auto it = mMyStateMachines.begin(); it != mMyStateMachines.end(); it++)
	{
		delete *it;
	}
	mMyStateMachines.clear();
	LogicComponent::Shutdown();
}

void BrainComponent::ToJson(nlohmann::json& j)
{
	using nlohmann::json;
	IBase::ToJson(j);

	if (mMyStateMachines.size())
	{
		json& mStates = j["states"];
		for (auto it = mMyStateMachines.begin(); it != mMyStateMachines.end(); it++)
		{
			json state;
			(*it)->ToJson(state);
			mStates.push_back(state);
		}
	}
}
void BrainComponent::FromJson(const nlohmann::json& j)
{
	auto statesJson = j.find("states");
	if (statesJson != j.end())
	{
		for (const auto& stateJson : *statesJson)
		{
			if (stateJson.find("name") != stateJson.end())
			{
				std::string name = stateJson["name"];
				auto tusmuertos = AddStateMachine(reinterpret_cast<StateMachine*>(FactorySys->Create(name.c_str())));
				if (tusmuertos != nullptr)
					tusmuertos->FromJson(stateJson);
			}
		}
	}

}

StateMachine* BrainComponent::GetStateMachine(const char* name)
{
	for (auto& machine : mMyStateMachines) {
		if (machine->mName == name) {
			return machine;
		}
	}
	return nullptr;
}
StateMachine* BrainComponent::AddStateMachine(StateMachine* state)
{
	if (state == nullptr)
		return nullptr;

	auto duplicated = std::find_if(mMyStateMachines.begin(), mMyStateMachines.end(), [&](auto& it)
		{
			return state->GetName() == it->GetName();
		});

	if (duplicated == mMyStateMachines.end())
	{
		state->mBrainOwner = this;
		state->mActor = mOwner;
		state->SetEnabled(mEnabled);
		mMyStateMachines.push_back(state);
		return state;
	}
	return nullptr;
}
void BrainComponent::RemoveStateMachine(StateMachine* state)
{

	if (state->mBrainOwner != this)
		return;

	auto to_delete = std::find(mMyStateMachines.begin(), mMyStateMachines.end(), state);

	if (to_delete != mMyStateMachines.end())
	{
		// Delete the component from both the vector and memory.
		state->Shutdown();
		mMyStateMachines.erase(to_delete);
		delete state;
	}
}

bool BrainComponent::Edit()
{
	bool changed = false;
	static unsigned index = 0;
	const char* label = MenusSys->statesBuffer[index];

	if (EditorSys->AllStateMachines.size() && ImGui::BeginCombo("Add State", MenusSys->statesBuffer[index], 0))
	{
		for (unsigned i = 0; i < EditorSys->AllStateMachines.size(); i++)
		{
			if (ImGui::Selectable(MenusSys->statesBuffer[i], index == i))
			{
				/*Editor* debugEditor = EditorSys;
				debugEditor->AllStateMachines;*/
				index = i;
				AddStateMachine(reinterpret_cast<StateMachine*>(FactorySys->Create(MenusSys->statesBuffer[i])));
				changed = true;
			}
			if (index == i)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}

	if (ImGui::TreeNode("RemoveState"))
	{
		for (unsigned i = 0; i < mMyStateMachines.size(); i++)
		{
			if (ImGui::Selectable(mMyStateMachines[i]->GetName().c_str()))
			{
				RemoveStateMachine(mMyStateMachines[i]);
				changed = true;
			}
		}
		ImGui::TreePop();
	}
	for (auto stateMachines : mMyStateMachines)
	{
		if(ImGui::CollapsingHeader(stateMachines->GetName().c_str()))
			stateMachines->Edit();
	}
	return changed;

}