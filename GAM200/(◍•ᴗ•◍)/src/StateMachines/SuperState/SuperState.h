#pragma once


#include "../State/State.h"
#include "../../GameObject/GameObject.h"
#include "../StateMachine/StateMachine.h"

template <typename T>
class SuperState : public State
{
	
public:
	T mInternalStateMachine;

	SuperState(const char* name = nullptr) :State(name) {

	}
	virtual ~SuperState() {}


	 void Initialize() override {
		//Reset the internal state machine
		//mInternalStateMachine.Reset();

		//Do the internap enter of the super state
		State::Initialize();

		//if this crashes you need to create this function in the internal state machine, and the variable for setting it
		mInternalStateMachine.SetOwner(this);

		mInternalStateMachine.Initialize();
	}
	 void Update() override{
		//Update the internal state machine
		mInternalStateMachine.Update();

		//Do the internal update of the super state
		State::Update();
	}

	 void LogicEnter() override
	 {
		 mInternalStateMachine.mCurrentState->LogicEnter();
		 
	 }



};

