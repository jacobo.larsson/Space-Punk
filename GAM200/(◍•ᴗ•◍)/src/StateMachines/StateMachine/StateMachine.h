#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "../../RTTI/SM_RTTI.h"
#include "../../RTTI/IComp.h"
#include "../../Extern/json/json.hpp"

#include "../../LogicSystem/LogicComponent.h"

#include "../State/State.h"

class BrainComponent;
template<typename T>
class SuperState;

class StateMachine : public IBase
{
	friend class BrainComponent;
	template<typename T>
	friend class SuperState;
	SM_RTTI_DECL(StateMachine, IBase);
public:

	StateMachine();
	virtual ~StateMachine();
	StateMachine& operator=(const StateMachine& rhs);
	virtual StateMachine* Clone();

	virtual void OnCollisionStarted(GameObject* other);
	virtual void OnCollisionPersisted(GameObject* other);
	virtual void OnCollisionEnded(GameObject* other);

	virtual void Initialize();
	virtual void Update();
	virtual void Shutdown();
	virtual bool Edit();

	void Reset();
	void Clear();

	void AddState(State * state);
	void RemoveState(State * state);

	void SetInitState(State* state);
	void SetInitState(const char* stateName);
	void ChangeState(State* state);
	void ChangeState(const char* stateName);
	float GetCurrentStateTime(void) { return mCurrentState->GetTimeInState(); }
	std::string GetCurrentStateName();
	State* GetState(const char* stateName);
	GameObject* GetActor() { return mActor; };

protected:

	BrainComponent* mBrainOwner;
	GameObject* mActor;
	std::vector<State*> mStates;

	State* mInitialState;
	State* mCurrentState;
	State* mNextState;
};