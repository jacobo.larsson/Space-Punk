// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior 
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		StateMachine.cpp
//
//	Purpose:		Implementation of state, statemachines and super states
//					functions.
//
//	Project:		State Machines 
//			
//	Author:			Juan Pacheco Larrucea, pacheco.j, 540000219
// ----------------------------------------------------------------------------

#include "../../Factory/Factory.h"
#include "StateMachine.h"

StateMachine::StateMachine()
{
	mBrainOwner = NULL;

	//Set the state machine current state to null
	mCurrentState = nullptr;

	//Set the state machine next state to null
	mNextState = nullptr;

	//Set the state machine initial state to null
	mInitialState = nullptr;

	mActor = nullptr;
}
StateMachine::~StateMachine()
{
	//Clear all the states
	for (auto it = mStates.begin(); it != mStates.end(); it++)
		(*it)->Shutdown();

	StateMachine::Clear();
}
StateMachine& StateMachine::operator=(const StateMachine& rhs)
{
	for (int i = 0; i < rhs.mStates.size(); i++)
	{
		State* newstate = reinterpret_cast<State*>(FactorySys->Create<State>());
		*newstate = *rhs.mStates[i];

		AddState(newstate);

	}
	return *this;
}
StateMachine* StateMachine::Clone()
{
	StateMachine* temp = FactorySys->Create<StateMachine>();
	(*temp) = (*this);
	return temp;
}

void StateMachine::OnCollisionStarted(GameObject* other)
{
	if (mCurrentState)
		mCurrentState->OnCollisionStarted(other);
}
void StateMachine::OnCollisionPersisted(GameObject* other)
{
	if (mCurrentState)
		mCurrentState->OnCollisionPersisted(other);
}
void StateMachine::OnCollisionEnded(GameObject* other)
{
	if (mCurrentState)
		mCurrentState->OnCollisionEnded(other);
}

void StateMachine::Initialize()
{
	for (auto it = mStates.begin(); it != mStates.end(); it++) 
	{
		(*it)->mActor = mActor;
		(*it)->Initialize();
	}
	mInitialState->LogicEnter();

}
void StateMachine::Update()
{
	//Execute if next state is valid and if current state is not equal to next state
	if (mNextState && mCurrentState != mNextState)
	{
		//Execute if current state is valid
		if (mCurrentState)
		{
			//Do the internal exit of the current state
			mCurrentState->LogicExit();
		}

		//Change the current state to the next state
		mCurrentState = mNextState;

		//Do the internal enter of the current state
		mCurrentState->LogicEnter();

	}

	//Execute if current state is valid
	if (mCurrentState)
	{
		//Do the internal update of the current state
		mCurrentState->Update();
	}

}
void StateMachine::Shutdown()
{
	for (auto it = mStates.begin(); it != mStates.end(); it++)
		(*it)->Shutdown();

	StateMachine::Clear();
}

void StateMachine::Reset()
{
	//Execute if current state is valid
	if (mCurrentState)
	{
		//Do the internal exit of the current state 
		mCurrentState->Shutdown();
	}

	//Set current state to null
	mCurrentState = mInitialState;
	mCurrentState->mTimeInState = 0.0f;

	//Set next state to the initial state
	//mNextState = mInitialState;
}
void StateMachine::Clear()
{
	//Do a loop to iterate through all the states
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
	{
		//Create a temporal pointer to the actual state
		State* tmp = *it;

		//Delete the actual state
		delete tmp;

	}

	//Clear the states list
	mStates.clear();

	//Set the state machine current state to null
	mCurrentState = NULL;

	//Set the state machine next state to null
	mNextState = NULL;

	//Set the state machine initial state to null
	mInitialState = NULL;

}


void StateMachine::AddState(State * state)
{
	//Execute if state is valid
	if (state)
	{
		//Do a loop to iterate through all the states
		for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
		{
			//Execute if the name of the actual state is equal to the given state name (duplicated name)
			if ((*it)->mName == state->mName)
			{
				//Exit the function
				return;
			}
		}
		//Push to the states list the given state
		mStates.push_back(state);

		//Set the state actor to the mActor variable
		state->mActor = mActor;

		//Set the state machine owner to this state machine
		state->mOwnerStateMachine = this;
	}

	//Exit the function
	return;

}
void StateMachine::RemoveState(State * state)
{
	//Execute if state is valid
	if (state)
	{
		//Do a loop to iterate through all the states
		for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
		{
			//Execute if the given state is equal to the actual state
			if (*it == state)
			{
				//Create a temporal pointer to the actual state
				State * tmp = *it;

				//Delete the actual state
				delete tmp;

				//Erase the actual state from the list
				mStates.erase(it);

				//Exit the loop
				break;
			}
		}
	}
}

void StateMachine::SetInitState(State * state)
{
	//Do a loop to iterate through all the states
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
	{
		//Execute if the given name is equal to the actual state name
		if (*it == state)
		{
			//Set initial state to the actual state
			mInitialState = state;

			//Exit the loop
			break;
		}
	}


}
void StateMachine::SetInitState(const char * stateName)
{
	//Set initial state to the returned state of Get State using the state name
	//as a parameter
	mInitialState = GetState(stateName);

}
void StateMachine::ChangeState(State * state)
{
	//Execute if state is valid
	if (state)
	{
		//Do a loop to iterate through all the states
		for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
		{
			//Execute if the given state is equal to the actual state
			if (*it == state)
			{
				//Set next state to the given state
				mNextState = state;
			}
		}

	}
}
void StateMachine::ChangeState(const char * stateName)
{
	//Set next state to the returned state of Get State using the state name
	//as a parameter

	mNextState = GetState(stateName);
}

State* StateMachine::GetState(const char* stateName)
{
	//Execute if state name is valid
	if (stateName)
	{
		//Do a loop to iterate through all the states
		for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); it++)
		{
			//Execute if the given name is equal to the actual state name
			if ((*it)->mName == stateName)
			{
				//Return the actual state
				return *it;
			}
		}
	}
	//Return null
	return NULL;
}

std::string StateMachine::GetCurrentStateName()
{
	return mCurrentState->GetName();
}

bool StateMachine::Edit()
{
	return false;
}