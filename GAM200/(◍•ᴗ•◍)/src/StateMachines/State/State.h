#pragma once

#include <string>
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"

class StateMachine;
class GameObject;

class State : public IBase
{
	friend class StateMachine;
	SM_RTTI_DECL(State, IBase);

public:

	State(const char* name = nullptr, GameObject* actor = nullptr);
	virtual ~State() {}
	State& operator=(const State& rhs);
	State* Clone();

	virtual void OnCollisionStarted(GameObject* other) {}
	virtual void OnCollisionPersisted(GameObject* other) {}
	virtual void OnCollisionEnded(GameObject* other) {}

	virtual void Initialize();
	virtual void Shutdown();
	virtual void Update();

	virtual void LogicEnter() {}
	virtual void LogicUpdate() {}
	virtual void LogicExit() {}

	virtual void Edit() {}

	StateMachine* GetOwnerStateMachine();
	float GetTimeInState() { return mTimeInState; }
	
	bool edit = false;
protected:
	StateMachine* mOwnerStateMachine;
	GameObject* mActor;
	float mTimeInState;


};