#include "../../Time/Time.h"
#include "../../Factory/Factory.h"
#include "State.h"

State::State(const char* name, GameObject* actor)
{
	//Execute if name is valid
	if (name)
	{
		//Set the name of the state to the given name
		mName = name;
	}

	//Else execute this
	else
	{
		//Set the name of the state to unnamed
		mName = "unnamed";
	}

	mActor = actor;

	//Set the time in state to 0
	mTimeInState = 0.0f;

	//Set the owner state machine to null
	mOwnerStateMachine = NULL;
}
State& State::operator=(const State& rhs)
{
	mName = rhs.mName;
	mTimeInState = rhs.mTimeInState;
	return *this;
}
State* State::Clone()
{
	State* temp = FactorySys->Create<State>();
	(*temp) = (*this);
	return temp;
}

void State::Initialize()
{
	//Set tume in state to 0
	mTimeInState = 0.0f;

	//Do the logic enter of the state
	//LogicEnter();
}
void State::Shutdown()
{
	//Do the logic exit of the state
	//LogicExit();
}
void State::Update()
{
	//Add to time in state the frame time
	mTimeInState += (float)TimeSys->GetFrameTime();

	//Do the logic update of the state
	LogicUpdate();
}

StateMachine* State::GetOwnerStateMachine() 
{
	return mOwnerStateMachine;
}
