#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"
#include "../AssertSystem/AssertSystem.h"

#include "../Archetype/ArchetypeFunctions.h"
#include "../Factory/Factory.h"
#include "../Input/Input.h"
#include "../Scene/Scene.h"
#include "../Space/Space.h"
#include "../Collisions/Collisions.h"
#include "../PropertySystem/PropertyMap.h"
#include "../Changer/Changer.h"
#include "../Input/Input.h"
#include "../Time/Time.h"
#include "../Editor/Menus/Menus.h"
#include "../Game/GameInput/GameInput.h"
#include "../Graphics/RenderManager/RenderManager.h"

#include "Cursor.h"

Cursor::Cursor()
{
	mName = "Cursor";


}
Cursor::Cursor(Cursor& rhs)
{
	

}
Cursor::~Cursor()
{

}
Cursor& Cursor::operator=(const Cursor& rhs)
{
	
	return *this;

}
Cursor* Cursor::Clone()
{

	Cursor* temp = FactorySys->Create<Cursor>();
	(*temp) = (*this);
	return temp;
}

void Cursor::OnCreate()
{


}
void Cursor::Initialize()
{
	GameObject* object_owner = GetOwner();
	LogicComponent::Initialize();
	

}
void Cursor::Update()
{

	if (mOwner)
	{

		TransformComponent* to_change = mOwner->GetComp<TransformComponent>();
		if (to_change)
		{

			if (InputSys->using_gamepad)
			{
				float max_movement = 5.f;
				float min_movement = 0.f;
				float x_movement = 0.f;
				float y_movement = 0.f;
				InputSys->get_left_joystick_state(xgamepad, ygamepad);


				if (xgamepad > 10000)
				{
					to_change->SetWorldPosition(glm::vec3(to_change->GetWorldPosition().x + 5.f, to_change->GetWorldPosition().y, to_change->GetWorldPosition().z));

				}
				else if (xgamepad < -10000)
				{
					to_change->SetWorldPosition(glm::vec3(to_change->GetWorldPosition().x - 5.f, to_change->GetWorldPosition().y, to_change->GetWorldPosition().z));
				}
				if (ygamepad > 10000)
				{
					to_change->SetWorldPosition(glm::vec3(to_change->GetWorldPosition().x, to_change->GetWorldPosition().y + 5.f, to_change->GetWorldPosition().z));
				}
				else if (ygamepad < -10000)
				{

					to_change->SetWorldPosition(glm::vec3(to_change->GetWorldPosition().x, to_change->GetWorldPosition().y - 5.f, to_change->GetWorldPosition().z));
				}
			}
			else
			{
				glm::vec2 mousepos = InputSys->get_current_mouse_position();
				glm::vec3 mousepos3(mousepos.x, mousepos.y, 0);

				GameObject* camera = mOwner->GetParentSpace()->FindObjectByName("Camera");
				if (camera)
				{
					CameraComponent* mCamera = camera->GetComp<CameraComponent>();
					if (mCamera)
					{
						mousepos3 = RndMngr->ProjToWindowInv(mousepos3);
						mousepos3 = glm::inverse(mCamera->GetProjectionMatrix()) * glm::vec4(mousepos3, 1.0f);
						mousepos3 = glm::inverse(mCamera->GetViewMatrix()) * glm::vec4(mousepos3, 1.0f);
						to_change->SetWorldPosition(mousepos3);
					}
				}
			}

		}

	}
}
void Cursor::Shutdown()
{
	LogicComponent::Shutdown();

}




void Cursor::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	// Write the IBase data (name, enabled, guid)
	IBase::ToJson(j);

}
void Cursor::FromJson(const nlohmann::json& j)
{
	using nlohmann::json;
	// read from json as IBase (name, uid, enabled)
	IBase::FromJson(j);
	


}


bool Cursor::Edit()
{
	
	return false;
}

