#pragma once
#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../EventSystem/EventHandler.h"
#include "../LogicSystem/LogicComponent.h"
#include "../Transform2D/TransformComponent.h"
#include "../Graphics/Font/Font.h"

class Cursor : public LogicComponent
{
	SM_RTTI_DECL(Cursor, IComp);
public:

	Cursor();
	Cursor(Cursor& rhs);
	~Cursor();
	Cursor& operator=(const Cursor& rhs);
	Cursor* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;


	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;

private:
	int xgamepad = 0;
	int ygamepad = 0;
};