/**********************************************************************************/
/*
\file   Input.cpp
\author Juan Pacheco
\par    email: pachecho.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on Input.h
*/
/*********************************************************************************/

#include <algorithm>
#include <iostream>

#include "../Extern/ImGui/imgui_impl_sdl.h"

#include "../Graphics/Window/Window.h"
#include "../MainEngine/MainEngine.h"
#include "../Scene/Scene.h"

#include "Input.h"

InputManager* InputManager::instance = nullptr;

InputManager::InputManager() :key_current_state{ 0 }, key_previous_state{ 0 },
mouse_current_state{ 0 }, mouse_previous_state{ 0 },
gamepadbutton_current_state{ 0 }, gamepadbutton_previous_state{ 0 }
{}

#pragma region keys

const bool InputManager::key_is_down(const keys& key)
{
	if (!key_is_triggered(key))
		return key_current_state[static_cast<size_t>(key)] == true;
	return false;
}

const bool InputManager::key_is_up(const keys& key)
{
	return key_current_state[static_cast<size_t>(key)] == false;
}

const bool InputManager::key_is_triggered(const keys& key)
{
	if (key_current_state[static_cast<size_t>(key)] == true &&
		key_current_state[static_cast<size_t>(key)] == key_previous_state[static_cast<size_t>(key)])
	{
		return true;
	}
	return false;
}

const bool InputManager::key_is_released(const keys& key)
{
	if (key_current_state[static_cast<size_t>(key)] == false && key_current_state[static_cast<size_t>(key)] != key_previous_state[static_cast<size_t>(key)])
	{
		return true;
	}
	return false;
}

#pragma endregion

#pragma region mouse
const bool InputManager::mouse_is_down(mouse_buttons button)
{
	if (!mouse_is_triggered(button))
		return mouse_current_state[static_cast<size_t>(button)] == true;
	else
		return false;
}
const bool InputManager::mouse_is_up(mouse_buttons button)
{
	return mouse_current_state[static_cast<size_t>(button)] == false;
}
const bool InputManager::mouse_is_triggered(mouse_buttons button)
{
	if (mouse_current_state[static_cast<size_t>(button)] == true && mouse_current_state[static_cast<size_t>(button)] != mouse_previous_state[static_cast<size_t>(button)])
	{
		return true;
	}
	return false;
}
const bool InputManager::mouse_is_released(mouse_buttons button)
{
	if (mouse_current_state[static_cast<size_t>(button)] == false && mouse_current_state[static_cast<size_t>(button)] != mouse_previous_state[static_cast<size_t>(button)])
	{
		return true;
	}
	return false;
}

#pragma endregion

#pragma region gamepad
const bool InputManager::gamepadbutton_is_down(const SDL_GameControllerButton& button)
{
	if (!gamepadbutton_is_triggered(button))
		return gamepadbutton_current_state[button] == true;
	return false;
}
const bool InputManager::gamepadbutton_is_up(const SDL_GameControllerButton& button)
{
	return gamepadbutton_current_state[button] == false;
}
const bool InputManager::gamepadbutton_is_triggered(const SDL_GameControllerButton& button)
{
	if (gamepadbutton_current_state[button] == true && gamepadbutton_current_state[button] != gamepadbutton_previous_state[button])
	{
		return true;
	}
	return false;
}
const bool InputManager::gamepadbutton_is_released(const SDL_GameControllerButton& button)
{
	if (gamepadbutton_current_state[button] == false && gamepadbutton_current_state[button] != gamepadbutton_previous_state[button])
	{
		return true;
	}
	return false;
}
#pragma endregion

#pragma region GameLoop
bool InputManager::Initialize()
{
	if (SDL_Init(SDL_INIT_GAMECONTROLLER) < 0)
	{
		std::cout << "Could not initialize SDL: " << SDL_GetError() << std::endl;
		return false;
	}
	return true;
}
void InputManager::Update()
{
	while (SDL_PollEvent(&_event))
	{

		ImGui_ImplSDL2_ProcessEvent(&_event);
		switch (_event.type)
		{
		case SDL_WINDOWEVENT:
		{
			Uint8 window_event = _event.window.event;
			if (window_event == SDL_WINDOWEVENT_CLOSE)
				Engine->Quit();
		}
			break;
		case SDL_QUIT:
			Engine->Quit();
			break;
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			handle_key_event();
			break;

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			handle_mouse_event();
			break;
		case SDL_MOUSEMOTION:
			handle_mouse_motion();
			break;
		case SDL_MOUSEWHEEL:
			handle_mouse_mousewheel();
			break;

		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
			handle_gamepad_event();

			break;

		case SDL_CONTROLLERAXISMOTION:
			handle_gamepad_joystick();
			break;
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
			handle_gamepad_triggers();
			break;

		case SDL_CONTROLLERDEVICEADDED:
		case SDL_CONTROLLERDEVICEREMOVED:
			handle_gamepad_number();
			break;


		case SDL_DROPFILE:
			handle_file_drop();
			break;

		}

	}
}
void InputManager::LateUpdate()
{
	for (unsigned i = 0; i < (unsigned)key_current_state.size(); ++i)
		key_previous_state[i] = key_current_state[i];

	for (unsigned i = 0; i < (unsigned)mouse_current_state.size(); ++i)
		mouse_previous_state[i] = mouse_current_state[i];

	for (unsigned i = 0; i < (unsigned)gamepadbutton_current_state.size(); ++i)
		gamepadbutton_previous_state[i] = gamepadbutton_current_state[i];


	if (left_axis_x< 5000 && left_axis_x >-5000)
		left_axis_x = 0;
	if (left_axis_y< 5000 && left_axis_y >-5000)
		left_axis_y = 0;
	if (right_axis_x< 5000 && right_axis_x >-5000)
		right_axis_x = 0;
	if (right_axis_y< 5000 && right_axis_y >-5000)
		right_axis_y = 0;


	mouse_scroll_up = false;
	mouse_scroll_down = false;
	//if (left_trigger< 5000 && left_trigger >-5000)
	//	left_trigger = 0;
	//if (right_trigger< 5000 && right_trigger >-5000)
	//	right_trigger = 0;


}
void InputManager::Shutdown()
{
	int number = get_gamepad_number();
	for (int i = 0; i < number; i++)
	{
		SDL_GameController* controller = gamepads.back();
		gamepads.pop_back();

		SDL_GameControllerClose(controller);
	}

}

#pragma endregion

#pragma region Handlers
void InputManager::handle_key_event()
{
	key_current_state[_event.key.keysym.scancode] = _event.type == SDL_KEYDOWN ? true : false;
	using_gamepad = false;
	
}
void InputManager::handle_mouse_event()
{

	mouse_current_state[_event.button.button] = _event.type == SDL_MOUSEBUTTONDOWN ? true : false;
	using_gamepad = false;

}
void InputManager::handle_mouse_mousewheel()
{
	// scroll up
	if (_event.wheel.y > 0.0f)
	{
		mouse_scroll_up = true;
	}
	// scroll down
	else if (_event.wheel.y < 0.0f)
	{
		mouse_scroll_down = true;
	}
	//else
	//{
	//}
	using_gamepad = false;
}
void InputManager::handle_gamepad_event()
{
	if (gamepads[0] == SDL_GameControllerFromInstanceID(_event.cdevice.which))
	{
		gamepadbutton_current_state[_event.cbutton.button] = _event.type == SDL_CONTROLLERBUTTONDOWN ? true : false;
		using_gamepad = true;
	}
}
void InputManager::handle_gamepad_number()
{
	if (_event.type == SDL_CONTROLLERDEVICEADDED)
	{

		for (int i = 0; i < SDL_NumJoysticks(); i++)
		{
			if (SDL_IsGameController(i))
			{
				gamepads.push_back(SDL_GameControllerOpen(i));
				break;
			}
		}
	}
	else if (_event.type == SDL_CONTROLLERDEVICEREMOVED)
	{



		SDL_GameController* controller = SDL_GameControllerFromInstanceID(_event.cdevice.which);

		std::vector<SDL_GameController*>::iterator it;
		for (it = gamepads.begin(); it != gamepads.end(); it++)
		{
			if (*it == controller)
			{
				gamepads.erase(it);
				break;
			}
		}

		SDL_GameControllerClose(controller);
		for (unsigned i = 0; i < SDL_CONTROLLER_BUTTON_MAX; ++i)
		{
			gamepadbutton_previous_state[i] = false;
			gamepadbutton_current_state[i] = false;
		}


	}

}
void InputManager::handle_file_drop()
{
	file_dir = _event.drop.file;
	size_t found = file_dir.find("data");
	if (found != std::string::npos)
	{
		std::string slash = file_dir.substr(found);
		ExtensionTypes ext =RsrcMan->GetExtensionOfFile(file_dir);
		
		size_t found2 = file_dir.find("Textures");
		if (found2 != std::string::npos)
		{
			file_dir = std::string("./") + file_dir.substr(found, 4) + std::string("/") + file_dir.substr(found2, 8) + std::string("/") + file_dir.substr(found2 + 9);
			RsrcMan->Load(file_dir);
		}
		else
		{
			file_dir = std::string("./") + file_dir.substr(found, 4) + std::string("/") + file_dir.substr(found + 5);
		}
	}
}
void InputManager::handle_mouse_motion()
{
	using_gamepad = false;
}
void InputManager::handle_gamepad_joystick()
{
	if (gamepads[0] == SDL_GameControllerFromInstanceID(_event.cdevice.which))
	{
		if (_event.jaxis.axis == 0)
		{
			left_axis_x = _event.jaxis.value;
		}
		if (_event.jaxis.axis == 1)
		{
			left_axis_y = -_event.jaxis.value;
		}
		if (_event.jaxis.axis == 2)
		{
			right_axis_x = _event.jaxis.value;
		}
		if (_event.jaxis.axis == 3)
		{
			right_axis_y = _event.jaxis.value;
		}
		using_gamepad = true;
	}
}
void InputManager::handle_gamepad_triggers()
{
	if (gamepads[0] == SDL_GameControllerFromInstanceID(_event.cdevice.which))
	{
		if (_event.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERLEFT)
		{
			left_trigger = _event.caxis.value;
		}
		if (_event.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERRIGHT)
		{
			right_trigger = _event.caxis.value;
		}
		using_gamepad = true;
	}

}

#pragma endregion

#pragma region Gettors
glm::vec2 InputManager::get_current_mouse_position()
{
	int xcont, ycont = 0;
	float x, y;
	SDL_GetMouseState(&xcont, &ycont);

	x = (float)xcont;
	y = (float)ycont;

	SDL_Window* currentwindow = WindowSys->GetWindow();
	glm::uvec2 size = WindowSys->GetSize();

	x -= (float)size.x / 2;

	y *= -1;
	y += (float)size.y / 2;

	mouse_previous_position = mouse_current_position;
	mouse_current_position = glm::vec2(x, y);

	return glm::vec2(x, y);
}
glm::vec2 InputManager::get_previous_mouse_position()
{
	return mouse_previous_position;
}
glm::vec2 InputManager::get_difference_mouse_position()
{
	get_current_mouse_position();
	return mouse_current_position - mouse_previous_position;
}
void InputManager::get_left_joystick_state(int& x, int& y)
{

	x = left_axis_x;
	y = left_axis_y;

}
void InputManager::get_right_joystick_state(int& x, int& y)
{
	x = right_axis_x;
	y = right_axis_y;
}
void InputManager::get_left_trigger_state(int& x)
{
	x = left_trigger;
}
void InputManager::get_right_trigger_state(int& x)
{
	x = right_trigger;
}
int InputManager::get_gamepad_number()
{
	return (int)gamepads.size();
}
#pragma endregion

int InputManager::gamepad_rumble(SDL_GameController* gamecontroller, unsigned int low_frequency, unsigned int high_frequency, unsigned int duration_in_ms)
{
	if (gamepads.size() >= 1)
	{
		return SDL_GameControllerRumble(gamecontroller, low_frequency, high_frequency, duration_in_ms);
	}
	return -1;
}


