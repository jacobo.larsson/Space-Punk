/**********************************************************************************/
/*
\file   Input.h
\author Juan Pacheco
\par    email: pachecho.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions to take input from the computer
*/
/*********************************************************************************/
#pragma once
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

#include <array>
#include <vector>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../ResourceManager/ResourceManager.h"
#include "../Singleton/Singleton.h"

enum class keys :size_t
{
#pragma region Letters
	A = SDL_SCANCODE_A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
#pragma endregion

#pragma region Numbers
	num_1,
	num_2,
	num_3,
	num_4,
	num_5,
	num_6,
	num_7,
	num_8,
	num_9,
	num_0,
#pragma endregion

#pragma region SpecialKeys
	Return,
	Escape,
	Backspace,
	Tab,
	Space,
	Minus,
	Equal,
	LeftBracket,
	RightBracket,
	Backslash,
	Slash = SDL_SCANCODE_SLASH,
#pragma endregion

#pragma region F
	F1 = SDL_SCANCODE_F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
#pragma endregion

#pragma region ArrowsSection
	Home = SDL_SCANCODE_HOME,
	PageUp,
	Delete,
	End,
	PageDown,
	Right,
	Left,
	Down,
	Up,
#pragma endregion

#pragma region KeyPad
	pad_Divide = SDL_SCANCODE_KP_DIVIDE,
	pad_Multiply,
	pad_Minus,
	pad_Plus,
	pad_Enter,
	pad_1,
	pad_2,
	pad_3,
	pad_4,
	pad_5,
	pad_6,
	pad_7,
	pad_8,
	pad_9,
	pad_0,
#pragma endregion

	LeftControl = SDL_SCANCODE_LCTRL,
	LeftShift,
	LeftAlt,
	LeftWindows,
	RightControl,
	RightShift,
	RightAlt,
	RightWindows

};

enum class mouse_buttons :size_t
{
	Left_click = SDL_BUTTON_LEFT,
	Middle_click,
	Right_click,
	Side_1,
	Side_2,
	Mouse_count = 5
};

enum class gamepad_buttons :size_t
{
	A = SDL_CONTROLLER_BUTTON_A,
	B,
	X,
	Y,
	Back,
	Guide,
	Start,
	Left_joystick,
	Right_joystick,
	Left_shoulder,
	Right_shoulder,
	Up,
	Down,
	Left,
	Right,
	gamepad_count
};


class InputManager :public IBase
{
	SM_RTTI_DECL(InputManager, IBase);
	Make_Singleton_Decl(InputManager)

public:
#pragma region keys
	const bool key_is_down(const keys& key);
	const bool key_is_up(const keys& key);
	const bool key_is_triggered(const keys& key);
	const bool key_is_released(const keys& key);
#pragma endregion

#pragma region mouse
	const bool mouse_is_triggered(mouse_buttons button);
	const bool mouse_is_released(mouse_buttons button);
	const bool mouse_is_down(mouse_buttons button);
	const bool mouse_is_up(mouse_buttons button);

	glm::vec2 get_current_mouse_position();
	glm::vec2 get_previous_mouse_position();
	glm::vec2 get_difference_mouse_position();

	bool is_mouse_scroll_up() { return mouse_scroll_up; }
	bool is_mouse_scroll_down() { return mouse_scroll_down; }
#pragma endregion

#pragma region gamepad
	const bool gamepadbutton_is_triggered(const SDL_GameControllerButton& button);
	const bool gamepadbutton_is_released(const SDL_GameControllerButton& button);
	const bool gamepadbutton_is_down(const SDL_GameControllerButton& button);
	const bool gamepadbutton_is_up(const SDL_GameControllerButton& button);
	int gamepad_rumble(SDL_GameController* gamecontroller, unsigned int low_frequency, unsigned int high_frequency, unsigned int duration_in_ms);


	void get_left_joystick_state(int& x, int& y);
	void get_right_joystick_state(int& x, int& y);
	void get_left_trigger_state(int& x);
	void get_right_trigger_state(int& x);

	int get_gamepad_number();

#pragma endregion

#pragma region GameLoop
	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();
#pragma endregion

	bool using_gamepad = false;


private:
#pragma region Members
	SDL_Event _event;
	std::array<bool, SDL_NUM_SCANCODES> key_current_state;
	std::array<bool, SDL_NUM_SCANCODES> key_previous_state;

	std::array<bool, static_cast<size_t>(mouse_buttons::Mouse_count)> mouse_current_state;
	std::array<bool, static_cast<size_t>(mouse_buttons::Mouse_count)> mouse_previous_state;

	glm::vec2 mouse_current_position = { 0, 0 };
	glm::vec2 mouse_previous_position = { 0, 0 };

	bool mouse_scroll_up = false;
	bool mouse_scroll_down = false;


	std::array<bool, static_cast<size_t>(gamepad_buttons::gamepad_count)> gamepadbutton_current_state;
	std::array<bool, static_cast<size_t>(gamepad_buttons::gamepad_count)> gamepadbutton_previous_state;

	std::vector<SDL_GameController*> gamepads;

	int right_axis_x = 0;
	int right_axis_y = 0;
	int left_axis_x = 0;
	int left_axis_y = 0;
	int right_trigger = 0;
	int left_trigger = 0;
	std::string file_dir;
#pragma endregion

#pragma region Handlers
	void handle_key_event();
	void handle_mouse_event();
	void handle_mouse_mousewheel();
	void handle_gamepad_event();
	void handle_gamepad_joystick();
	void handle_gamepad_triggers();
	void handle_gamepad_number();
	void handle_file_drop();
	void handle_mouse_motion();
#pragma endregion

};
#define InputSys InputManager::Get()


