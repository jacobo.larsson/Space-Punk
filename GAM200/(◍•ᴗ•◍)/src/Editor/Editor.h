/**********************************************************************************/
/*!
\file   Editor.h
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that use ImGui to make an editor that displays all the 
components.
*/
/*********************************************************************************/
#pragma once
#include <glm/glm.hpp>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "EditorStateMachine/EditorStateMachine.h"


class GameObject;
class CameraComponent;
class Renderable;
class Space;

namespace EditorConstants
{
	const float autoSavePeriod = 15.0f;
	const unsigned bufferSize = 64;
	const float cameraSpeed = 15.0f;
}

class Editor :public IBase
{
	SM_RTTI_DECL(Editor, IBase);
	Make_Singleton(Editor)

	friend class BrainComponent;
	friend class Menus;
	friend class Settings;

public:
	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();

	Space* GetActiveSpace();
	GameObject* GetPickedObject();

	Space* SetActiveSpace(Space* activeSpace);
	GameObject* SetPickedObject(GameObject* pickedObject);
	void UnPickObject();

	bool SetAutoSaveTime(float newTime);

	void ChangeEditorState(std::string state);

	void CenterCameraAt(const glm::vec3& pos);
public:
	void MoveCamera();

	void DrawColliders();
	
	void CopyObject();
	void PasteObject();

	void Undo();
	void Redo();

	void RegisterComponents();
	void RegisterAllStateMachines();

	void ShowGrid();

	float mAutoSaveTime = 0.0f;

	Space* active_space;
	EditorStateMachine mStateMachine;
	GameObject* picked_object = nullptr;
	std::map<std::string,std::vector<GameObject*>> mPickedObjects;
	std::vector<std::string> AllStateMachines;
	std::vector<std::string> allComponents;
	bool moving = false;
	bool showing_colliders = false;
};

#define EditorSys Editor::Get()