/**********************************************************************************/
/*!
\file   Editor.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the function declarations of Editor.h
*/
/*********************************************************************************/
#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include "../Graphics/Window/Window.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Input/Input.h"
#include "../MainEngine/MainEngine.h"
#include "../Factory/Factory.h"
#include "../Time/Time.h"

#include "../Space/Space.h"
#include "../Scene/Scene.h"

#include "../Collisions/Collisions.h"

#include "../Transform2D/TransformComponent.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Audio/SoundEmitter.h"
#include "../Audio/AudioListener.h"
#include "../Physics/RigidBody.h"
#include "../Collisions/ICollider.h"
#include "../Graphics/Camera/Camera.h"
#include "../Game/CameraFollow/CameraFollow.h"
#include "../Game/PlatformSystem/PlatformSystem.h"
#include "../Game/PlayerMovement/PlayerMovement.h"
#include "../Game/BasicEnemy/BasicEnemySystem.h"
#include "../Game/BasicEnemy/BasicTicketer/BasicTicketer.h"
#include "../Game/BasicEnemy/BasicTicketer/BasicEnemyTicketSystem.h"
#include "../Game/ChargerEnemy/ChargerEnemySystem.h"
#include "../Game/FlyingEnemy/FlyingEnemySystem.h"
#include "../Game/FlyingEnemy/FlockComp/FlockComp.h"
#include "../Game/TurretEnemy/TurretEnemySystem.h"
#include "../Game/Lifebar/Lifebar.h"
#include "../Game/ArenaManager/ArenaManager.h"
#include "../Game/ArenaManager/TriggerArena/TriggerArena.h"
#include "../Game/HoverBoard/TriggerHoverBoard/TriggerHoverBoard.h"
#include "../Game/HoverBoard/RespawnHoverboard/RespawnHoverboard.h"
#include "../Game/Health/Health.h"
#include "../StateMachines/StateMachine/StateMachine.h"
#include "../StateMachines/BrainComponent/BrainComponent.h"
#include "../RayCast/RayCaster.h"
#include "../Animation/Path2D.h"
#include "../Changer/Changer.h"
#include "../Game/SimplePlayerMovement/SimpleMovementStates.h"
#include "../Game/SimplePlayerMovement/SimplePlayerMovement.h"
#include "../Animation/SM_Animation.h"
#include "../Game/GasEnemy/GasEnemySystem.h"
#include "../Game/GasEnemy/GasCloud.h"
#include "../Game/GasEnemy/GasExplosion.h"
#include "../Game/TurretEnemy/BulletTimer.h"
#include "../Game/CameraFollow/CameraMachine.h"
#include "../Game/CameraFollow/CameraTriggerer/CameraTriggerer.h"
#include "../Cheats/Cheats.h"
#include "../Graphics/Font/Font.h"
#include "../Settings/Settings.h"
#include "../Graphics/Particle System/ParticleSystem.h"
#include "../Game/Dialog/Dialog.h"
#include "../SelectableComp/SelectableComp.h"
#include "../Game/Hitbox/Hitbox.h"
#include "../Game/ShotReceiver/ShotReceiver.h"
#include "../Game/Breakable/Breakable.h"
#include "../Game/CameraFollow/Parallax/Parallax.h"
#include "../Game/GasEnemy/GasTicketer/GasTicketer.h"
#include "../TutorialDummy/TutorialDummy.h"
#include "../Cursor/Cursor.h"
#include "../Splines/SplineComp.h"
#include "../SpacePunkGame/PlayerShipMovement/PlayerShipMovementMachine.h"
#include "../SpacePunkGame/PlayerShipAttack/PlayerShipAttackMachine.h"
#include "../SpacePunkGame/LaserBullet/LaserBullet.h"
#include "../SpacePunkGame/Asteroid/AsteroidMachine.h"
#include "../SpacePunkGame/ScreenWraper/ScreenWraper.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyMovement/StandardEnemyMovementMachine.h"
#include "../SpacePunkGame/Evading/EvadingComponent.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyAttack/StandardEnemyAttackMachine.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyLaserBulletComp/StandardEnemyLaserBulletComp.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyMovementMachine/HeavyEnemyMovementMachine.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyAttackMachine/HeavyEnemyAttackMachine.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyLaserBulletComp/HeavyEnemyLaserBulletComp.h"
#include "../SpacePunkGame/CameraShake/CameraShake.h"
#include "../SpacePunkGame/HealthComp/HealthComp.h"
#include "../SpacePunkGame/WaveComponent/WaveComp.h"
#include "../SpacePunkGame/WaveComponent/WaveManager.h"

#include "../Game/Checkpoint/Checkpoint.h"
#include "../Game/GoBackToMenu/GoBackToMenu.h"

#include "Menus/Menus.h"
#include "ObjectEdition/ObjectEdition.h"

#include "Editor.h"

Editor* Editor::instance = nullptr;

bool Editor::Initialize()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	//ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	ImGuiIO& io = ImGui::GetIO();
	(void)io;
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForOpenGL(WindowSys->GetWindow(), WindowSys->GetContext());
	ImGui_ImplOpenGL3_Init("#version 400");

	RegisterComponents();
	RegisterAllStateMachines();
	MenusSys->Initialize();
	mStateMachine.Initialize();

	return true;
}
void Editor::Update()
{
	// Setup ImGui for a new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(WindowSys->GetWindow());
	ImGui::NewFrame();
	//Create the menus we will use
	
	if (SettingsSys->mStartInGame==false)
	{
		MenusSys->MenuBar();
		MenusSys->ShowDebugConsole();
	}
	mStateMachine.Update();
	if (SettingsSys->inEditor)
	{

		MenusSys->SpaceMenu();
		MenusSys->GameObjectsMenu();
		MenusSys->ShowSearchByCollisionGroup();
		MenusSys->ShowSearchByArchetype();
		MenusSys->ShowSearchByComponent();
		MenusSys->ShowSearchByTag();
		MenusSys->ShowSearchByUID();
		MenusSys->ShowArchetypes();
		MenusSys->ShowResourceList();
		if(SettingsSys->VisualGrid)
			ShowGrid();
	}

	if(showing_colliders)
		DrawColliders();
	
}
void Editor::LateUpdate()
{
	mAutoSaveTime += TimeSys->GetFrameTime();
	if (SettingsSys->inEditor && (mAutoSaveTime >= EditorConstants::autoSavePeriod))
	{
		SceneSys->SaveLevel();
		CollTable->Save();
		mAutoSaveTime = 0.0f;
	}

	//for (int i = 0; i < 10; ++i)
	//	RndMngr->DrawLine({ i%2? i : -i, i }, { i % 2 ? i : -i, i + 1 }, Colors::orange);

}
void Editor::Shutdown()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
	MenusSys->Shutdown();
}

Space* Editor::GetActiveSpace()
{
	return active_space;
}
GameObject* Editor::GetPickedObject()
{
	return picked_object;
}

Space* Editor::SetActiveSpace(Space* activeSpace)
{
	return active_space = activeSpace;
}
GameObject* Editor::SetPickedObject(GameObject* pickedObject)
{
	using nlohmann::json;
	std::string test = std::string("./data/temp/picked_obj_before.json");
	json h;
	if (pickedObject)
		pickedObject->ToJson(h);

	std::ofstream fp_out(test);
	if (fp_out.is_open() && fp_out.good())
	{
		fp_out << std::setw(4) << h;
		fp_out.close();
	}
	mStateMachine.SetPickedObject(pickedObject);
	picked_object = pickedObject;
	return pickedObject;
}
void Editor::UnPickObject()
{
	mStateMachine.UnpickObject();
}

bool Editor::SetAutoSaveTime(float newTime)
{
	if (newTime >= 15.0)
	{
		mAutoSaveTime = newTime;
		return true;
	}
	else
		return false;
}

void Editor::ChangeEditorState(std::string state)
{
	mStateMachine.ChangeState(state.c_str());
}

void Editor::MoveCamera()
{
	auto camera_pos = &RndMngr->editor_camera->mTransform.mPosition;
	glm::vec3 mouse_pos = glm::vec3(InputSys->get_current_mouse_position(), 0.0f);

	mouse_pos = RndMngr->ProjToWindowInv(mouse_pos);
	mouse_pos = RndMngr->EditorCamToProjInv(mouse_pos);
	mouse_pos = RndMngr->WorldToEditorCamInv(mouse_pos);

	auto dash = *camera_pos - mouse_pos;
	dash = glm::normalize(dash);

	*camera_pos = glm::vec3(camera_pos->x + dash.x * SettingsSys->camera_move_speed,
		camera_pos->y + dash.y * SettingsSys->camera_move_speed,
		camera_pos->z);// glm::vec3(mouse_pos, camera_pos->z);

}

void Editor::DrawColliders()
{
	if (SettingsSys->SpacePartitioning)
	{
		std::vector<Collider*> _Colliders;
		for (auto& _Space : SceneSys->GetAllSpaces())
		{
			_Space->GetActiveComponents<Collider>(_Colliders, "Collider");
		}

		for (auto& collider : _Colliders)
		{
			glm::vec3 pos = collider->GetCollideBox().GetWorldPosition();
			pos.z = 19.6f;
			glm::vec3 scale = collider->GetCollideBox().GetWorldScale();
			float orientation = collider->GetCollideBox().GetWorldOrientation();
			if (collider->GetOwner()->Enabled())
				SceneSys->GetMainSpace()->Drawrectangle(pos, scale.x, scale.y, Colors::red, orientation);
			else 
				SceneSys->GetMainSpace()->Drawrectangle(pos, scale.x, scale.y, Colors::blue, orientation);
		}
	}
	else
	{
		std::map <unsigned, std::vector<Collider*>*> mAllColliders;
		for (auto& _Space : SceneSys->GetAllSpaces())
		{
			std::vector<Collider*> Colliders = *(reinterpret_cast<std::vector<Collider*>*>(&_Space->GetComponents<Collider>()));
			for (auto& collider : Colliders)
			{
				glm::vec3 pos = collider->GetCollideBox().GetWorldPosition();
				pos.z = 19.0f;
				glm::vec3 scale = collider->GetCollideBox().GetWorldScale();
				float orientation = collider->GetCollideBox().GetWorldOrientation();
				if (collider->GetOwner()->Enabled())
					_Space->Drawrectangle(pos, scale.x, scale.y, Colors::red, orientation);
				else
					_Space->Drawrectangle(pos, scale.x, scale.y, Colors::blue, orientation);
			}
		}
	}
}

void Editor::CenterCameraAt(const glm::vec3& pos)
{
	RndMngr->editor_camera->mTransform.mPosition.x = pos.x;
	RndMngr->editor_camera->mTransform.mPosition.y = pos.y;
}

void Editor::CopyObject()
{
	if (mStateMachine.GetPickedObject() == nullptr)
		return;
	using nlohmann::json;

	std::ofstream fp1("./data/temp/copied_object.json");
	json j;
	mStateMachine.GetPickedObject()->ToJson(j);

	if (fp1.is_open() && fp1.good())
	{
		fp1 << std::setw(4) << j;
		fp1.close();
	}
}
void Editor::PasteObject()
{
	using nlohmann::json;

	std::ifstream fp1("./data/temp/copied_object.json");
	json j;
	if (fp1.is_open() && fp1.good())
	{
		fp1 >> j;
		fp1.close();
	}
	auto obj = SceneSys->GetMainSpace()->NewGameObject();
	unsigned olduid = obj->GetUID();
	obj->FromJson(j);
	obj->mUID = olduid;
	std::string _Name = obj->GetName();
	_Name += " (Copy)";
	obj->SetName(_Name);

	auto mouse_pos = InputSys->get_current_mouse_position();

	mouse_pos = RndMngr->ProjToWindowInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->EditorCamToProjInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->WorldToEditorCamInv(glm::vec3(mouse_pos, 0.0f));
	if (obj->HasComp<TransformComponent>())
		obj->GetComp<TransformComponent>()->SetWorldPosition(glm::vec3(mouse_pos, obj->GetComp<TransformComponent>()->GetWorldPosition().z));
	picked_object = obj;
}

void Editor::Undo()
{
	Changer* changerpoint = ChangerSys;

	if (changerpoint)
	{
		if (changerpoint->currentchange != 0)
		{
			int id_to_search = changerpoint->changes[changerpoint->currentchange - 1].first;
			
			
			
			std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("ctrl_z")+ std::to_string(changerpoint->ctrlz_count) + std::string(".json");
			json h;
			changerpoint->ctrlz_count++;
			if (changerpoint->changes[changerpoint->currentchange - 1].second != ObjectDestroyed && changerpoint->changes[changerpoint->currentchange - 1].second != SpaceErased)
			{
				std::experimental::filesystem::remove(to_add.c_str());


				GameObject* toset = SceneSys->FindObjectById(id_to_search);
				toset->ToJson(h);

				std::ofstream fp_out(to_add);
				if (fp_out.is_open() && fp_out.good())
				{
					fp_out << std::setw(4) << h;
					fp_out.close();
				}

			}
		}
		if (changerpoint->currentchange != 0)
		{
			int id_to_search = changerpoint->changes[changerpoint->currentchange - 1].first;
			int changetype = changerpoint->changes[changerpoint->currentchange - 1].second;
			if (changetype == ObjectDestroyed)
			{
				GameObject* newobj = FactorySys->Create<GameObject>();
				std::string object_to_load = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(changerpoint->currentchange - 1) + std::string(".json");

				json j;


				std::ifstream fp_in(object_to_load, std::ios_base::out | std::ios_base::in);
				if (fp_in.is_open() && fp_in.good())
				{
					fp_in >> j;

					newobj->FromJson(j);
					newobj->mUID = id_to_search;

					fp_in.close();
				}



				Space* space_to_be_added = dynamic_cast<Space*>(SceneSys->FindObjectById(newobj->spaceId));
				if (space_to_be_added)
				{
					space_to_be_added->AddGameObject(newobj);
					space_to_be_added->LinkPointers();
				}

				changerpoint->currentchange--;

			}
			else if (changetype == SpaceErased)
			{
				Space* newspace = FactorySys->Create<Space>();
				std::string space_to_load = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(changerpoint->currentchange - 1) + std::string(".json");
				json j;



				std::ifstream fp_in(space_to_load, std::ios_base::out | std::ios_base::in);
				if (fp_in.is_open() && fp_in.good())
				{
					fp_in >> j;
					newspace->FromJson(j);

					newspace->mUID = id_to_search;

					SceneSys->AddSpace(newspace);
					fp_in.close();
					changerpoint->currentchange--;
				}
			}
			else if (changetype == ObjectAdded)
			{
				GameObject* searched = SceneSys->FindObjectById(id_to_search);
				if (searched)
				{
					Space* to_check = searched->GetParentSpace();
					to_check->DestroyObject(searched);
					changerpoint->currentchange--;
				}
			}
			else if (changetype == SpaceAdded)
			{
				GameObject* searched = SceneSys->FindObjectById(id_to_search);
				if (searched)
				{
					Space* to_check = dynamic_cast<Space*>(searched);
					if (to_check)
					{
						SceneSys->DestroySpace(to_check);

					}
					changerpoint->currentchange--;
				}
			}
			else if (changetype == ObjectChanged)
			{
				GameObject* searched = SceneSys->FindObjectById(id_to_search);
				if (searched)
				{
					std::string object_to_load = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(changerpoint->currentchange - 1) + std::string(".json");
					json j;

					std::ifstream fp_in(object_to_load, std::ios_base::out | std::ios_base::in);
					if (fp_in.is_open() && fp_in.good())
					{
						fp_in >> j;
						searched->FromJsonUndo(j);
						fp_in.close();
					}
					searched->GetParentSpace()->LinkPointers();

					changerpoint->currentchange--;
				}
			}

		}

	}
}
void Editor::Redo()
{
 	Changer* changerpoint = ChangerSys;
	int actualpos = changerpoint->currentchange;


	if (changerpoint->changes.size() == 0)
		return;
	//use actualpos
	if (actualpos == changerpoint->changes.size())
		return;
	else
	{
		int id_to_search = changerpoint->changes[changerpoint->currentchange].first;

		int changetype = changerpoint->changes[changerpoint->currentchange].second;

		if (changetype == ObjectDestroyed)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (picked_object == searched)
				picked_object == nullptr;
			if (searched)
			{
				Space* to_check = searched->GetParentSpace();
				to_check->DestroyObject(searched);
				changerpoint->currentchange++;
				changerpoint->ctrlz_count--;
			}



		}
		else if (changetype == SpaceErased)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				
				Space* to_check = dynamic_cast<Space*>(searched);
				if (to_check)
				{
					SceneSys->DestroySpace(to_check);

				}
				changerpoint->currentchange++;
				changerpoint->ctrlz_count--;
			}
		}
		else if (changetype == ObjectAdded)
		{
			int id_to_search = changerpoint->changes[changerpoint->currentchange].first;
			GameObject* newobj = FactorySys->Create<GameObject>();
			//std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(id_to_search) + std::string(".json");
			std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("ctrl_z") + std::to_string(changerpoint->ctrlz_count-1) + std::string(".json");
			json j;

			std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
			if (fp_in.is_open() && fp_in.good())
			{
				fp_in >> j;
				newobj->FromJson(j);
				newobj->mUID = id_to_search;

				fp_in.close();
			}


			Space* space_to_be_added = dynamic_cast<Space*>(SceneSys->FindObjectById(newobj->spaceId));
			if (space_to_be_added)
				space_to_be_added->AddGameObject(newobj);
			changerpoint->currentchange++;
			changerpoint->ctrlz_count--;
			std::experimental::filesystem::remove(to_add);


		}
		else if (changetype == SpaceAdded)
		{
			Space* newspace = FactorySys->Create<Space>();
			//std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(id_to_search) + std::string(".json");
			std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("ctrl_z") + std::to_string(changerpoint->ctrlz_count-1) + std::string(".json");
			json j;
			std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
			if (fp_in.is_open() && fp_in.good())
			{
				fp_in >> j;
				newspace->FromJson(j);
				newspace->mUID = id_to_search;

				fp_in.close();
			}

			SceneSys->AddSpace(newspace);
			changerpoint->currentchange++;
			changerpoint->ctrlz_count--;
			std::experimental::filesystem::remove(to_add);
		}
		else if (changetype == ObjectChanged)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				//std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(id_to_search) + std::string(".json");
				std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("ctrl_z") + std::to_string(changerpoint->ctrlz_count-1) + std::string(".json");

				json j;





				std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
				if (fp_in.is_open() && fp_in.good())
				{
					fp_in >> j;
					searched->FromJsonUndo(j);


					fp_in.close();
				}

				std::experimental::filesystem::remove(to_add);

				changerpoint->currentchange++;
				changerpoint->ctrlz_count--;
			}
		}

	}


	/*int et = changerpoint->currentchange;
	int test = changerpoint->changes.size() - 1;

	if (et == changerpoint->changes.size())
		return;
	else if (et == changerpoint->changes.size() - 1)
	{
		int id_to_search = changerpoint->changes[changerpoint->currentchange].first;
		int changetype = changerpoint->changes[changerpoint->currentchange].second;
		if (changetype == ObjectDestroyed)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				Space* to_check = searched->GetParentSpace();
				to_check->DestroyObject(searched);
				changerpoint->currentchange++;
			}



		}
		else if (changetype == SpaceErased)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				Space* to_check = dynamic_cast<Space*>(searched);
				if (to_check)
				{
					SceneSys->DestroySpace(to_check);

				}
				changerpoint->currentchange++;
			}
		}
		else if (changetype == ObjectAdded)
		{
			int id_to_search = changerpoint->changes[changerpoint->currentchange].first;
			GameObject* newobj = FactorySys->Create<GameObject>();
			std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("last_state") + std::string(".json");
			json j;

			std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
			if (fp_in.is_open() && fp_in.good())
			{
				fp_in >> j;
				newobj->FromJson(j);
				newobj->mUID = id_to_search;

				fp_in.close();
			}


			Space* space_to_be_added = dynamic_cast<Space*>(SceneSys->FindObjectById(newobj->spaceId));
			if (space_to_be_added)
				space_to_be_added->AddGameObject(newobj);
			changerpoint->currentchange++;




		}
		else if (changetype == SpaceAdded)
		{
			Space* newspace = FactorySys->Create<Space>();
			std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("last_state") + std::string(".json");
			json j;
			std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
			if (fp_in.is_open() && fp_in.good())
			{
				fp_in >> j;
				newspace->FromJson(j);
				newspace->mUID = id_to_search;

				fp_in.close();
			}

			SceneSys->AddSpace(newspace);
			changerpoint->currentchange++;
		}
		else if (changetype == ObjectChanged)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				std::string to_add = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::string("last_state") + std::string(".json");
				json j;





				std::ifstream fp_in(to_add, std::ios_base::out | std::ios_base::in);
				if (fp_in.is_open() && fp_in.good())
				{
					fp_in >> j;
					searched->FromJsonUndo(j);


					fp_in.close();
				}


				changerpoint->currentchange++;
			}
		}

	}
	else
	{
		int id_to_search = changerpoint->changes[changerpoint->currentchange + 1].first;
		int changetype = changerpoint->changes[changerpoint->currentchange + 1].second;
		if (changetype == ObjectDestroyed)
		{
			
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				Space* to_check = searched->GetParentSpace();
				to_check->DestroyObject(searched);
			}


			changerpoint->currentchange++;

		}
		else if (changetype == SpaceErased)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				Space* to_check = dynamic_cast<Space*>(searched);
				if (to_check)
				{
					SceneSys->DestroySpace(to_check);

				}
				changerpoint->currentchange++;
			}
		}
		else if (changetype == ObjectAdded)
		{
			GameObject* newobj = FactorySys->Create<GameObject>();
			std::string object_to_load = std::string("./data/ErasedThings/") + std::to_string(id_to_search) + '/' + std::to_string(changerpoint->currentchange + 1) + std::string(".json");
			json j;

			std::ifstream fp_in(object_to_load, std::ios_base::out | std::ios_base::in);
			if (fp_in.is_open() && fp_in.good())
			{
				fp_in >> j;
				newobj->FromJson(j);


				fp_in.close();
			}


			Space* space_to_be_added = dynamic_cast<Space*>(SceneSys->FindObjectById(newobj->spaceId));
			if (space_to_be_added)
				space_to_be_added->AddGameObject(newobj);
			changerpoint->currentchange++;
		}
		else if (changetype == SpaceAdded)
		{
			Space* newspace = FactorySys->Create<Space>();
			std::string space_to_load = std::string("./data/ErasedThings/") + std::to_string(id_to_search) + '/' + std::to_string(changerpoint->currentchange + 1) + std::string(".json");
			json j = space_to_load;
			newspace->FromJson(space_to_load);
			changerpoint->currentchange++;
		}
		else if (changetype == ObjectChanged)
		{
			GameObject* searched = SceneSys->FindObjectById(id_to_search);
			if (searched)
			{
				std::string object_to_load = std::string("./data/temp/") + std::to_string(id_to_search) + '_' + std::to_string(changerpoint->currentchange + 1) + std::string(".json");
				json j;





				std::ifstream fp_in(object_to_load, std::ios_base::out | std::ios_base::in);
				if (fp_in.is_open() && fp_in.good())
				{
					fp_in >> j;
					searched->FromJsonUndo(j);


					fp_in.close();
				}


				changerpoint->currentchange++;
			}
		}
	
	}*/


	
}

void Editor::RegisterComponents()
{
	allComponents.push_back(TransformComponent::TYPE().GetName());
	allComponents.push_back(RigidBody::TYPE().GetName());
	allComponents.push_back(Collider::TYPE().GetName());
	allComponents.push_back(Renderable::TYPE().GetName());
	allComponents.push_back(CameraComponent::TYPE().GetName());
	allComponents.push_back(SoundEmiter::TYPE().GetName());
	allComponents.push_back(AudioListener::TYPE().GetName());
	allComponents.push_back(CameraFollow::TYPE().GetName());
	allComponents.push_back(BrainComponent::TYPE().GetName());
	allComponents.push_back(RayCaster::TYPE().GetName());
	allComponents.push_back(Path2D::TYPE().GetName());
	allComponents.push_back(SkeletonRenderable::TYPE().GetName());
	allComponents.push_back(TextRenderable::TYPE().GetName());
	allComponents.push_back(ArenaManager::TYPE().GetName());
	allComponents.push_back(TriggerArena::TYPE().GetName());
	allComponents.push_back(TriggerHoverBoard::TYPE().GetName());
	allComponents.push_back(Health::TYPE().GetName());
	allComponents.push_back(GasCloud::TYPE().GetName());
	allComponents.push_back(GasExplosion::TYPE().GetName());
	allComponents.push_back(BulletTimer::TYPE().GetName());
	allComponents.push_back(Cheats::TYPE().GetName());
	allComponents.push_back(ParticleEmitter::TYPE().GetName());
	allComponents.push_back(ShotReceiver::TYPE().GetName());
	allComponents.push_back(Breakable::TYPE().GetName());
	allComponents.push_back(ParallaxComponent::TYPE().GetName());
	allComponents.push_back(FlockComp::TYPE().GetName());
	allComponents.push_back(Checkpoint::TYPE().GetName());
	allComponents.push_back(GoBackToMenu::TYPE().GetName());
	allComponents.push_back(CameraTriggerer::TYPE().GetName());
	allComponents.push_back(DialogComponent::TYPE().GetName());
	allComponents.push_back(SelectableComp::TYPE().GetName());
	allComponents.push_back(Hitbox::TYPE().GetName());
	allComponents.push_back(RespawnHoverboard::TYPE().GetName());
	allComponents.push_back(TutorialDummy::TYPE().GetName());
	allComponents.push_back(Cursor::TYPE().GetName());
	allComponents.push_back(SplineComp::TYPE().GetName());
	allComponents.push_back(BasicTicketer::TYPE().GetName());
	allComponents.push_back(GasTicketer::TYPE().GetName());
	allComponents.push_back(LaserBulletComponent::TYPE().GetName());
	allComponents.push_back(ScreenWraperComponent::TYPE().GetName());
	allComponents.push_back(EvadingComponent::TYPE().GetName());
	allComponents.push_back(StandardEnemyLaserBulletComp::TYPE().GetName());
	allComponents.push_back(HeavyEnemyLaserBulletComp::TYPE().GetName());
	allComponents.push_back(CameraShakeComp::TYPE().GetName());
	allComponents.push_back(HealthComp::TYPE().GetName());
	allComponents.push_back(WaveComponent::TYPE().GetName());
	allComponents.push_back(WaveManager::TYPE().GetName());
}
void Editor::RegisterAllStateMachines()
{
	AllStateMachines.push_back(MovementMachine::TYPE().GetName());
	AllStateMachines.push_back(SimpleMovementMachine::TYPE().GetName());
	AllStateMachines.push_back(BasicEnemyMachine::TYPE().GetName());
	AllStateMachines.push_back(BasicEnemyTicketMachine::TYPE().GetName());
	AllStateMachines.push_back(ChargerEnemyMachine::TYPE().GetName());
	AllStateMachines.push_back(FlyingEnemyMachine::TYPE().GetName());
	AllStateMachines.push_back(TurretEnemyMachine::TYPE().GetName());
	AllStateMachines.push_back(LifebarMachine::TYPE().GetName());
	AllStateMachines.push_back(PlatformMachine::TYPE().GetName());
	AllStateMachines.push_back(GasEnemyMachine::TYPE().GetName());
	AllStateMachines.push_back(CameraMachine::TYPE().GetName());
	AllStateMachines.push_back(PlayerShipMovementMachine::TYPE().GetName());
	AllStateMachines.push_back(PlayerShipAttackMachine::TYPE().GetName());
	AllStateMachines.push_back(AsteroidMachine::TYPE().GetName());
	AllStateMachines.push_back(StandardEnemyMovementMachine::TYPE().GetName());
	AllStateMachines.push_back(StandardEnemyAttackMachine::TYPE().GetName());
	AllStateMachines.push_back(HeavyEnemyMovementMachine::TYPE().GetName());
	AllStateMachines.push_back(HeavyEnemyAttackMachine::TYPE().GetName());
}

void Editor::ShowGrid()
{
	Space* _Space = nullptr; 
	int _Order = 500;
	// Find the first space to be rendered
	std::for_each(SceneSys->GetAllSpaces().begin(), SceneSys->GetAllSpaces().end(), [&](auto& _SpaceIT)
	{
		if (_SpaceIT->Order < _Order)
			_Space = _SpaceIT;
	});

	for (int i = -SettingsSys->GridSize * 500; i < SettingsSys->GridSize * 500; i += SettingsSys->GridSize)
	{
		_Space->DrawVerticalLine(glm::vec3{i, 0, -480.0f}, 100000.0f , Colors::white);
		_Space->DrawHorizontalLine(glm::vec3{0, i, -480.0f}, 100000.0f , Colors::white);
	}
}
