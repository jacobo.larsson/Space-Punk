#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Graphics/RenderManager/RenderManager.h"
#include "../../Graphics/Camera/Camera.h"
#include "../Editor.h"
#include "../Menus/Menus.h"
#include "../ObjectEdition/ObjectEdition.h"
#include "../../Changer/Changer.h"

#include "../../Collisions/Collisions.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Settings/Settings.h"

#include "../../Input/Input.h"
#include "EditorStateMachine.h"
#include "EditorStates.h"

EditorIdle::EditorIdle()
{
	SetName("EditorIdle");
}

void EditorIdle::Initialize()
{
	State::Initialize();
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}


void EditorIdle::LogicEnter()
{
}

void EditorIdle::LogicUpdate()
{
	if (SettingsSys->inEditor)
	{
		if (InputSys->mouse_is_triggered(mouse_buttons::Left_click) && !ImGui::IsAnyItemHovered() && !ImGui::IsAnyWindowHovered())
		{
			mOwnerStateMachine->ChangeState("PickObject");
			std::cout << "LeftClicked down" << std::endl;
		}
		else if (InputSys->mouse_is_down(mouse_buttons::Left_click) && !ImGui::IsAnyItemHovered() && !ImGui::IsAnyWindowHovered() && SettingsSys->inEditor)
		{
			//Start computing the AABB
			mOwner->mouse_initial = InputSys->get_current_mouse_position();
			mOwner->UnpickObject();
			mOwner->mouse_initial = RndMngr->ProjToWindowInv(glm::vec3(mOwner->mouse_initial, 0.0f));
			mOwner->mouse_initial = RndMngr->EditorCamToProjInv(glm::vec3(mOwner->mouse_initial, 0.0f));
			mOwner->mouse_initial = RndMngr->WorldToEditorCamInv(glm::vec3(mOwner->mouse_initial, 0.0f));
			mOwnerStateMachine->ChangeState("PickMultipleObject");
		}
	}
	else if(InputSys->key_is_triggered(keys::LeftControl)&& InputSys->mouse_is_triggered(mouse_buttons::Left_click) && !ImGui::IsAnyItemHovered() && !ImGui::IsAnyWindowHovered())
	{
		mOwnerStateMachine->ChangeState("PickObject");
	}
}

void EditorIdle::LogicExit()
{
}

PickObject::PickObject()
{
	SetName("PickObject");
}

void PickObject::Initialize()
{
	State::Initialize();
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}

void PickObject::LogicEnter()
{
}

void PickObject::LogicUpdate()
{
	auto mouse_pos = InputSys->get_current_mouse_position();

	if (SettingsSys->inEditor)
	{
		mouse_pos = RndMngr->ProjToWindowInv(glm::vec3(mouse_pos, 0.0f));
		mouse_pos = RndMngr->EditorCamToProjInv(glm::vec3(mouse_pos, 0.0f));
		mouse_pos = RndMngr->WorldToEditorCamInv(glm::vec3(mouse_pos, 0.0f));
	}
	else
	{
		mouse_pos = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraComponent>()->GetInvProj2Window() * glm::vec4(mouse_pos, 0.0f, 1.0f);
		mouse_pos = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraComponent>()->GetInvCam2Proj() * glm::vec4(mouse_pos, 0.0f, 1.0f); 
		mouse_pos = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraComponent>()->GetInvWorld2Cam() * glm::vec4(mouse_pos, 0.0f, 1.0f);
	}
	
	mOwner->mPickedObjects.clear();

	for (auto& space : SceneSys->GetAllSpaces())
	{
		// Check with every object in the space
		for (auto& it : space->GetAllObjects())
		{
			if (space->Enabled())
			{
				if (auto transform = it->GetComp<TransformComponent>())
				{
					if (StaticPointToOrientedRect(glm::vec3(mouse_pos, 0.0f), transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, transform->GetWorldOrientation()))
					{
						mOwner->mPickedObjects[space->GetName()].push_back(it);
					}
				}
			}
		}
	}
	if (!mOwner->mPickedObjects.empty())
		mOwner->ChangeState("SelectObjectFromList");
	else
		mOwner->ChangeState("EditorIdle");

}

void PickObject::LogicExit()
{
}

SelectObjectFromList::SelectObjectFromList()
{
	SetName("SelectObjectFromList");
}

void SelectObjectFromList::Initialize()
{
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}

void SelectObjectFromList::LogicEnter()
{
}

void SelectObjectFromList::LogicUpdate()
{
	// This may work to make faster selecting just one object
	if (mOwner->mPickedObjects.size() == 1)
	{
		if (mOwner->mPickedObjects.begin()->second.size() == 1)
		{
			mOwner->SetPickedObject(mOwner->mPickedObjects.begin()->second[0]);
			mOwner->mPickedObjects.clear();
			mOwner->ChangeState("EditSelectedObject");
			return;
		}
	
	}
	ImGui::OpenPopup("Picked Objects");
	if (ImGui::BeginPopup("Picked Objects"))
	{
		if (!ImGui::IsWindowHovered() && InputSys->mouse_is_triggered(mouse_buttons::Left_click))
		{
			mOwner->mPickedObjects.clear();
			mOwner->ChangeState("EditorIdle");
			ImGui::EndPopup();
			return;
		}
		for (auto& space : mOwner->mPickedObjects)
		{
			if (ImGui::CollapsingHeader(space.first.c_str(), nullptr, 0))
			{
				for (auto& object : space.second)
				{
					ImGui::PushID(&object);
					if (ImGui::Button(object->GetName().c_str()))
					{
						mOwner->SetPickedObject(object);
						mOwner->mPickedObjects.clear();
						mOwner->ChangeState("EditSelectedObject");
						ImGui::PopID();
						ImGui::EndPopup();
						return;
					}
					ImGui::PopID();
				}
			}
		}
		ImGui::EndPopup();
	}
}

void SelectObjectFromList::LogicExit()
{
}

EditSelectedObject::EditSelectedObject()
{
	SetName("EditSelectedObject");
}

void EditSelectedObject::Initialize()
{
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}

void EditSelectedObject::LogicEnter()
{
}

void EditSelectedObject::LogicUpdate()
{
	if (mOwner->mPickedObject == nullptr)
	{
		mOwner->ChangeState("EditorIdle");
		return;
	}
	// ShortCuts

	if (mOwner->mPickedObject != nullptr && InputSys->key_is_down(keys::Delete) && InputSys->key_is_triggered(keys::LeftControl))
	{
		ChangerSys->AddChange(mOwner->mPickedObject, ObjectDestroyed);
		mOwner->mPickedObject->GetParentSpace()->DestroyObject(mOwner->mPickedObject);
		mOwner->ChangeState("EditorIdle");
		mOwner->UnpickObject();
		return;
	}

	if (ImGui::Begin("Game Object Inspector", nullptr, 0) )
	{
		static char RenameBuffer[EditorConstants::bufferSize];
		std::string outStr = "Name: ";
		outStr += mOwner->mPickedObject->GetName();

		ImGui::Text(outStr.c_str());
		outStr.clear();
		outStr += "UID: ";
		outStr += std::to_string( mOwner->mPickedObject->GetUID());
		ImGui::SameLine();
		if(ImGui::Button("Generate New ID"))
		{
			mOwner->mPickedObject->mUID = FactorySys->GenerateNewID();
		}


		static char export_buffer[50] = {""};

		ImGui::InputText("Export name",export_buffer,50);

		if (ImGui::Button("Export"))
		{
			std::string a = export_buffer;
			std::string new_string = std::string("./data/ExportedObjects/") +a +std::string("_")+
				mOwner->mPickedObject->GetName() + std::string("_") + std::to_string(mOwner->mPickedObject->GetUID()) +std::string(".json");

			json j;

			std::ofstream fp1(new_string);
			mOwner->mPickedObject->ToJson(j);

			if (fp1.is_open() && fp1.good())
			{
				fp1 << std::setw(4) << j;
				fp1.close();
			}


			
		}


		ImGui::Text(outStr.c_str());
		if (MenusSys->ShowInputText("Rename", RenameBuffer, EditorConstants::bufferSize))
		{
			mOwner->mPickedObject->SetName(RenameBuffer);
			MenusSys->ResetBuffer(RenameBuffer, EditorConstants::bufferSize);
		}
		outStr.clear();
		outStr += "Parent Space: ";

		if (mOwner->mPickedObject->GetParentSpace() != nullptr)
			outStr += mOwner->mPickedObject->GetParentSpace()->GetName();

		ImGui::Text(outStr.c_str());

		outStr.clear();
		outStr += "Archetype: ";
		outStr += mOwner->mPickedObject->GetArchetypeName();
		ImGui::Text(outStr.c_str());
		if (ImGui::Selectable("Save To Archetype"))
		{
			if (mOwner->mPickedObject->GetName() != "")
				RsrcMan->RegisterArchetype(*mOwner->mPickedObject);
			else
				std::cout << "Cannot save \"\" archetype" << std::endl;
		}
		if (ImGui::Selectable("Remove from archetype"))
		{
			mOwner->mPickedObject->UnLinkFromArchetype();
		}

		// Tags management
		if (ImGui::Button("ManageTags"))
			ImGui::OpenPopup("Game Object Tags Menu");

		MenusSys->TagsMenu(mOwner->mPickedObject);
		MenusSys->ShowComponentManagement(mOwner->mPickedObject);
		MenusSys->ShowComponentsMenu(mOwner->mPickedObject);

		if (ImGui::Button("Delete"))
		{
			ChangerSys->AddChange(mOwner->mPickedObject, ObjectDestroyed);
			mOwner->mPickedObject->GetParentSpace()->DestroyObject(mOwner->mPickedObject);
			mOwner->ChangeState("EditorIdle");
			ImGui::End();
			mOwner->UnpickObject();
			return;
		}

		if (!ImGui::IsAnyItemHovered() && !ImGui::IsWindowHovered() && InputSys->mouse_is_triggered(mouse_buttons::Middle_click))// && !isAnyMenuHovered)
		{
			mOwner->ChangeState("EditorIdle");
			ImGui::End();
			mOwner->UnpickObject();
			return;
		}

		// Draw selection rectangle
		if(auto transform = mOwner->mPickedObject->GetComp<TransformComponent>())
			SceneSys->GetMainSpace()->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Color(1.0f, 0.0f, 1.0f, 1.0f), transform->GetWorldOrientation());
		//ObjectEdition::EditSelectedObject(mOwner->mPickedObject->GetComp<TransformComponent>());

		ImGui::End();
	}
	else
		ImGui::End();
}

void EditSelectedObject::LogicExit()
{
}

bool  ImGui::IsAnyWindowHovered()
{ 
	return ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow); 
}

PickMultipleObject::PickMultipleObject()
{
	SetName("PickMultipleObject");
}

void PickMultipleObject::Initialize()
{
	State::Initialize();
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}

void PickMultipleObject::LogicEnter()
{
}

void PickMultipleObject::LogicUpdate()
{
	auto mouse_pos = InputSys->get_current_mouse_position();
	mouse_pos = RndMngr->ProjToWindowInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->EditorCamToProjInv(glm::vec3(mouse_pos, 0.0f));
	mouse_pos = RndMngr->WorldToEditorCamInv(glm::vec3(mouse_pos, 0.0f));
	Space* main_space = SceneSys->GetMainSpace();
	float width = glm::abs(mouse_pos.x - mOwner->mouse_initial.x);
	float height = glm::abs(mouse_pos.y - mOwner->mouse_initial.y);
	
	
	glm::vec2 drawcenter = glm::vec2((mOwner->mouse_initial.x - mouse_pos.x) / 2 + mouse_pos.x, (mOwner->mouse_initial.y - mouse_pos.y) / 2 + mouse_pos.y);
	main_space->Drawrectangle(drawcenter, width, height, 0);




	if (InputSys->mouse_is_released(mouse_buttons::Left_click))
	{
		glm::vec2 center = (mouse_pos - mOwner->mouse_initial) * (1.f / 2);
		glm::vec3 realcenter(center.x + mOwner->mouse_initial.x, center.y + mOwner->mouse_initial.y, 0);
		mOwner->multiple_selection.clear();

		// Check with every object in the space
		for (auto& it : main_space->GetAllObjects())
		{
			if (main_space->Enabled())
			{
				if (auto transform = it->GetComp<TransformComponent>())
				{
					if (StaticRectToStaticRect(realcenter, width, height, transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y))
					{
						mOwner->multiple_selection.push_back(it);
					}
				}
			}
		}

		if (mOwner->multiple_selection.size() == 1)
		{
			mOwner->SetPickedObject(mOwner->multiple_selection[0]);
			mOwner->multiple_selection.clear();
			mOwner->ChangeState("EditSelectedObject");
		}
		else if (mOwner->multiple_selection.size() == 0)
			mOwner->ChangeState("EditorIdle");
		else 
			mOwner->ChangeState("EditMultipleObjects");
	}
}

void PickMultipleObject::LogicExit()
{
}

EditMultipleObjects::EditMultipleObjects()
{
	SetName("EditMultipleObjects");
}

void EditMultipleObjects::Initialize()
{
	State::Initialize();
	mOwner = reinterpret_cast<EditorStateMachine*>(mOwnerStateMachine);
}

void EditMultipleObjects::LogicEnter()
{
}

void EditMultipleObjects::LogicUpdate()
{
	if (mOwner->multiple_selection.size() == 0)
	{
		mOwner->ChangeState("EditorIdle");
		return;
	}

	

	if (!ImGui::IsAnyItemHovered() && !ImGui::IsWindowHovered() && InputSys->mouse_is_triggered(mouse_buttons::Middle_click))// && !isAnyMenuHovered)
	{
		mOwner->ChangeState("EditorIdle");
		
		mOwner->multiple_selection.clear();
		return;
	}

	if (moving == true)
	{
		
		glm::vec2 difference;
		glm::vec2 current_pos;
		current_pos = InputSys->get_current_mouse_position();
		current_pos = RndMngr->ProjToWindowInv(glm::vec3(current_pos, 0.0f));
		current_pos = RndMngr->EditorCamToProjInv(glm::vec3(current_pos, 0.0f));
		current_pos = RndMngr->WorldToEditorCamInv(glm::vec3(current_pos, 0.0f));

		difference = current_pos - initial_pos;

		for (auto object : mOwner->multiple_selection)
		{
			if (auto transform = object->GetComp<TransformComponent>())
			{
				transform->SetWorldPosition(transform->GetWorldPosition() + glm::vec3(difference, 0.f));
			}
		}


	}
	else if (scaling == true)
	{
		glm::vec2 difference;
		glm::vec2 current_pos;
		current_pos = InputSys->get_current_mouse_position();
		current_pos = RndMngr->ProjToWindowInv(glm::vec3(current_pos, 0.0f));
		current_pos = RndMngr->EditorCamToProjInv(glm::vec3(current_pos, 0.0f));
		current_pos = RndMngr->WorldToEditorCamInv(glm::vec3(current_pos, 0.0f));

		difference = current_pos - initial_pos;

		for (auto object : mOwner->multiple_selection)
		{
			if (auto transform = object->GetComp<TransformComponent>())
			{
				transform->SetScale(transform->GetScale() + glm::vec3(difference, 0.f));
			}
		}
	}

	if (InputSys->mouse_is_up(mouse_buttons::Right_click))
	{

		moving = false;
		scaling = false;
	}

	
	if (!ImGui::IsAnyItemHovered() && !ImGui::IsWindowHovered() && InputSys->mouse_is_down(mouse_buttons::Right_click) && InputSys->key_is_triggered(keys::LeftControl))// && !isAnyMenuHovered)
	{
		scaling = true;
		initial_pos = InputSys->get_current_mouse_position();
		initial_pos = RndMngr->ProjToWindowInv(glm::vec3(initial_pos, 0.0f));
		initial_pos = RndMngr->EditorCamToProjInv(glm::vec3(initial_pos, 0.0f));
		initial_pos = RndMngr->WorldToEditorCamInv(glm::vec3(initial_pos, 0.0f));
	}
	else if(!ImGui::IsAnyItemHovered() && !ImGui::IsWindowHovered() && InputSys->mouse_is_down(mouse_buttons::Right_click))// && !isAnyMenuHovered)
	{
		moving = true;
		initial_pos = InputSys->get_current_mouse_position();
		initial_pos = RndMngr->ProjToWindowInv(glm::vec3(initial_pos, 0.0f));
		initial_pos = RndMngr->EditorCamToProjInv(glm::vec3(initial_pos, 0.0f));
		initial_pos = RndMngr->WorldToEditorCamInv(glm::vec3(initial_pos, 0.0f));
	}

	if (InputSys->key_is_down(keys::Delete) && InputSys->key_is_triggered(keys::LeftControl))
	{
		for (auto object : mOwner->multiple_selection)
		{
			//ChangerSys->AddChange(object, ObjectDestroyed);
			object->GetParentSpace()->DestroyObject(object);
		}
		mOwner->ChangeState("EditorIdle");
		mOwner->multiple_selection.clear();
		return;
	}

	if (InputSys->mouse_is_down(mouse_buttons::Left_click) && !ImGui::IsAnyItemHovered() && !ImGui::IsAnyWindowHovered() && SettingsSys->inEditor)
	{
		//Start computing the AABB
		mOwner->mouse_initial = InputSys->get_current_mouse_position();
		mOwner->UnpickObject();
		mOwner->mouse_initial = RndMngr->ProjToWindowInv(glm::vec3(mOwner->mouse_initial, 0.0f));
		mOwner->mouse_initial = RndMngr->EditorCamToProjInv(glm::vec3(mOwner->mouse_initial, 0.0f));
		mOwner->mouse_initial = RndMngr->WorldToEditorCamInv(glm::vec3(mOwner->mouse_initial, 0.0f));
		mOwnerStateMachine->ChangeState("PickMultipleObject");
	}

	for (auto object : mOwner->multiple_selection)
	{
		if (auto transform = object->GetComp<TransformComponent>())
			SceneSys->GetMainSpace()->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Color(1.0f, 0.0f, 1.0f, 1.0f), transform->GetWorldOrientation());
	}
	
}

void EditMultipleObjects::LogicExit()
{
}
