#pragma once

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"

#include "../../StateMachines/StateMachine/StateMachine.h"

class GameObject;
class EditorStateMachine;
class EditorIdle : public State
{
	SM_RTTI_DECL(EditorIdle, State);
public:
	EditorIdle();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;
};

class PickObject : public State
{
	SM_RTTI_DECL(PickObject, State);
public:
	PickObject();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;

};

class PickMultipleObject : public State
{
	SM_RTTI_DECL(PickMultipleObject, State);
public:
	PickMultipleObject();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;

};

class SelectObjectFromList : public State
{
	SM_RTTI_DECL(SelectObjectFromList, State);
public:
	SelectObjectFromList();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;
};

class EditSelectedObject :public State
{
	SM_RTTI_DECL(EditSelectedObject, State);
public:
	EditSelectedObject();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;

};

class EditMultipleObjects :public State
{
	SM_RTTI_DECL(EditMultipleObjects, State);
public:
	EditMultipleObjects();

	void Initialize() override;

	void LogicEnter() override;
	void LogicUpdate()override;
	void LogicExit()override;
private:
	EditorStateMachine* mOwner;
	bool moving;
	bool scaling;
	glm::vec2 initial_pos;

};