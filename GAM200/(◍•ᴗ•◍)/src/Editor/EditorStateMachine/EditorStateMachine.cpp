#include <fstream>
#include <experimental/filesystem>

#include "../../Extern/json/json.hpp"
#include "../../GameObject/GameObject.h"
#include "EditorStates.h"
#include "EditorStateMachine.h"
#include "../../Animation/SM_Animation.h"
#include "../../Graphics/Particle System/ParticleSystem.h"

EditorStateMachine::EditorStateMachine()
{
	SetName("EditorStateMachine");
}

EditorStateMachine::~EditorStateMachine()
{
}

void EditorStateMachine::Initialize()
{
	mInitialState = new EditorIdle();
	mCurrentState = mInitialState;
	AddState(mInitialState);
	AddState(new PickObject);
	AddState(new PickMultipleObject);
	AddState(new EditMultipleObjects);
	AddState(new SelectObjectFromList);
	AddState(new EditSelectedObject);
	StateMachine::Initialize();
}

void EditorStateMachine::Update()
{
	StateMachine::Update();
}

void EditorStateMachine::Shutdown()
{
	StateMachine::Clear();
}

void EditorStateMachine::SetPickedObject(GameObject* pickedObject)
{
	using nlohmann::json;
	std::string test = std::string("./data/temp/picked_obj_before.json");

	std::experimental::filesystem::remove(test);

	json h;
	if (pickedObject)
		pickedObject->ToJson(h);

	std::ofstream fp_out(test);
	if (fp_out.is_open() && fp_out.good())
	{
		fp_out << std::setw(4) << h;
		fp_out.close();
	}
	mPickedObject = pickedObject;
	if (mPickedObject)
	{
		if (mPickedObject->GetComp<ParticleEmitter>())
		{
			mPickedObject->GetComp<ParticleEmitter>()->mActiveSystem = true;
		}
		if (mPickedObject->GetComp<SkeletonRenderable>())
		{
			mPickedObject->GetComp<SkeletonRenderable>()->mPause = false;
		}
	}

}

void EditorStateMachine::UnpickObject()
{
	if (mPickedObject)
	{
		if (mPickedObject->GetComp<ParticleEmitter>())
		{
			mPickedObject->GetComp<ParticleEmitter>()->mActiveSystem = false;
		}
		if (mPickedObject->GetComp<SkeletonRenderable>())
		{
			mPickedObject->GetComp<SkeletonRenderable>()->mPause = true;
		}
	}
	mPickedObject = nullptr;
}

GameObject* EditorStateMachine::GetPickedObject()
{
	return mPickedObject;
}
