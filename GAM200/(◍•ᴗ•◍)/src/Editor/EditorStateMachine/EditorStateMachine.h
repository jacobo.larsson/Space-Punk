#pragma once

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"

#include "../../StateMachines/StateMachine/StateMachine.h"

class GameObject;

class EditorStateMachine :public StateMachine
{
	SM_RTTI_DECL(EditorStateMachine, StateMachine);
	friend class PickObject;
	friend class Menus;
	friend class SelectObjectFromList;
	friend class EditSelectedObject;
	friend class EditorIdle;
	friend class PickMultipleObject;
	friend class EditMultipleObjects;
public:
	EditorStateMachine();
	~EditorStateMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	void SetPickedObject(GameObject * obj);
	void UnpickObject();
	GameObject* GetPickedObject();
private:
	GameObject* mPickedObject;
	std::map<std::string, std::vector<GameObject*>> mPickedObjects;
	std::vector<GameObject*> multiple_selection;
	glm::vec2 mouse_initial;
};