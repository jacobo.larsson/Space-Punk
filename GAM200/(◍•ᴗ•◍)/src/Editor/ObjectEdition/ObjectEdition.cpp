#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "../../Input/Input.h"
#include "../../Graphics/RenderManager/RenderManager.h"

#include "../../Graphics/Color/Color.h"
#include "../../AssertSystem/AssertSystem.h"

#include "../../Collisions/Collisions.h"
#include "../../GameObject/GameObject.h"
#include "../../Collisions/ICollider.h"
#include "../../Transform2D/Transform2D.h"
#include "../../Transform2D/TransformComponent.h"
#include "../Editor.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"

#include "ObjectEdition.h"

namespace ObjectEdition
{
	static glm::vec3 vResize;
	static glm::vec3 hResize;
	static glm::vec3 dResize;
	static glm::vec3 Rotation;

	static glm::vec3 originalScale;
	static glm::vec2 originalMouse;

	static bool rotate = false;
	static bool scaleH = false;
	static bool scaleV = false;

	Gizmos editing_gizmo = None;
	Gizmos prev_editing_gizmo = None;

	enum Action
	{
		Idle,
		ScaleH,
		ScaleV,
		ScaleD,
		Move,
		Rotate
	};
	static Action editMode;
	static Action editModeColl;

	bool EditSelectedObject(TransformComponent* transform)
	{
		bool changed = false;
		if (transform)
		{
			DrawGizmos(transform);
			UpdateEditors(transform);
			changed = EditObject(transform);
		}
		return changed;
	}

	void DrawGizmos(TransformComponent* transform)
	{
		glm::mat3 rotMtx = glm::rotate(glm::mat3(1.0), transform->GetWorldOrientation());
		glm::vec3 pos = transform->GetWorldPosition();
		float width = transform->GetWorldScale().x;
		float height = transform->GetWorldScale().y;
		// Draw selection rectangle
		SceneSys->GetMainSpace()->Drawrectangle(pos, (int)width, (int)height, Colors::red, transform->GetWorldOrientation());

		// Draw top-right gizmo to resize both
		dResize = rotMtx * glm::vec3(width / 2.0f, height / 2.0f, 0.0f) + pos;

		SceneSys->GetMainSpace()->Drawrectangle(dResize,
			(int)(10 * RndMngr->GetEditorRatio()), (int)(10 * RndMngr->GetEditorRatio()), Color(1.0f, 1.0f, 1.0f, 1.0f),
			transform->GetWorldOrientation());

		// Draw right gizmo to resize horizontally
		hResize = rotMtx * glm::vec3(width / 2.0f, 0.0f, 0.0f) + pos;

		SceneSys->GetMainSpace()->Drawrectangle(hResize,
			(int)(5 * RndMngr->GetEditorRatio()), (int)(10 * RndMngr->GetEditorRatio()), Color(1.0f, 1.0f, 1.0f, 1.0f),
			transform->GetWorldOrientation());

		vResize = rotMtx * glm::vec3(0, height / 2.0f, 0.0f) + pos;
		// Draw top gizmo to resize vertically
		SceneSys->GetMainSpace()->Drawrectangle(vResize,
			(int)(10 * RndMngr->GetEditorRatio()), (int)(5 * RndMngr->GetEditorRatio()), Color(1.0f, 1.0f, 1.0f, 1.0f),
			transform->GetWorldOrientation());

		Rotation = rotMtx * glm::vec3(0, height / 2.0f + 15.0f, 0.0f) + pos; 

		// Draw rotation gizmo, in the future it will be a circle
		SceneSys->GetMainSpace()->DrawCircle(Rotation,
			5 * RndMngr->GetEditorRatio(), Color(1.0f, 1.0f, 1.0f, 1.0f));
	}

	void UpdateEditors(TransformComponent* transform)
	{
		// Set the action
		if (InputSys->mouse_is_triggered(mouse_buttons::Right_click))
		{
			auto mousePos = InputSys->get_current_mouse_position();

			mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
			mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
			mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));
			// Rotation Mode
			if (editMode == Action::Idle && StaticPointToStaticCircle(glm::vec3(mousePos, 0.0f), Rotation, 5.0f * RndMngr->GetEditorRatio()))
			{
				editMode = Action::Rotate;
				originalMouse = mousePos;
			}
			// Horizontal Scale
			if (editMode == Action::Idle && StaticPointToOrientedRect(glm::vec3(mousePos, 0.0f), hResize, 5.0f * RndMngr->GetEditorRatio(), 10.0f * RndMngr->GetEditorRatio(), transform->GetWorldOrientation()))
			{
				editMode = Action::ScaleH;
				originalMouse = mousePos;
				originalScale = transform->GetScale();
			}
			// Vertical Scale
			if (editMode == Action::Idle && StaticPointToOrientedRect(glm::vec3(mousePos, 0.0f), vResize, 10.0f * RndMngr->GetEditorRatio(), 5.0f * RndMngr->GetEditorRatio(), transform->GetWorldOrientation()))
			{
				editMode = Action::ScaleV;
				originalMouse = mousePos;
				originalScale = transform->GetScale();
			}
			// Diagonal Scale
			if (editMode == Action::Idle && StaticPointToOrientedRect(glm::vec3(mousePos, 0.0f), dResize, 10.0f * RndMngr->GetEditorRatio(), 10.0f * RndMngr->GetEditorRatio(), transform->GetWorldOrientation()))
			{
				editMode = Action::ScaleD;
				originalMouse = mousePos;
				originalScale = transform->GetScale();
			}
			if (editMode == Action::Idle && (StaticPointToOrientedRect(glm::vec3(mousePos, 0.0f), transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, transform->GetWorldOrientation()) || (InputSys->key_is_triggered(keys::LeftControl) && InputSys->mouse_is_triggered(mouse_buttons::Right_click))))
			{
				editMode = Action::Move;
			}
		}
		if (editMode != Action::Idle && InputSys->mouse_is_released(mouse_buttons::Right_click))
		{
			editMode = Action::Idle;
		}
	}

	bool EditObject(TransformComponent* transform)
	{
		bool changed = false;
		switch (editMode)
		{
		case Action::Idle:
			break;
		case Action::Rotate:
		{
			if (InputSys->mouse_is_down(mouse_buttons::Right_click))
			{
				auto mousePos = InputSys->get_current_mouse_position();

				mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));

				auto angle = atan2(mousePos.y - transform->GetWorldPosition().y, mousePos.x - transform->GetWorldPosition().x);
				transform->SetWorldOrientation((float)(angle /*- (3.141 / 2)*/));

				changed = true;
			}
		}
		break;
		case Action::ScaleH:
		{
			if (InputSys->mouse_is_down(mouse_buttons::Right_click))
			{
				auto mousePos = InputSys->get_current_mouse_position();

				mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));

				//Get the scale increment
				auto temp = mousePos - originalMouse;
				glm::vec2 normal = glm::normalize(glm::rotate(glm::vec2(1, 0), transform->GetWorldOrientation()));
				float angle = 0;
				if(temp.x != 0 || temp.y != 0)
					angle = glm::orientedAngle(normal, glm::normalize(glm::vec2(temp.x, temp.y)));
				auto factor = glm::cos(angle) * glm::length(temp);

				//Check if the scale won't be negative
				glm::vec3 newScale{ originalScale.x + 2 * factor , originalScale.y , originalScale.z };
				transform->SetWorldScale(newScale);
				changed = true;
			}
		}
		break;
		case Action::ScaleV:
		{
			if (InputSys->mouse_is_down(mouse_buttons::Right_click))
			{
				auto mousePos = InputSys->get_current_mouse_position();

				mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));

				//Get the scale increment
				auto temp = mousePos - originalMouse;
				glm::vec2 normal = glm::normalize(glm::rotate(glm::vec2(0, 1), transform->GetWorldOrientation()));
				float angle = 0;
				if (temp.x != 0 || temp.y != 0)
					angle = glm::orientedAngle(normal, glm::normalize(glm::vec2(temp.x, temp.y)));
				auto factor = glm::cos(angle) * glm::length(temp);

				//Check if the scale won't be negative
				glm::vec3 newScale{ originalScale.x, originalScale.y + 2 * factor , originalScale.z };
				transform->SetWorldScale(newScale);
				changed = true;
			}
		}
		break;
		case Action::ScaleD:
		{
			if (InputSys->mouse_is_down(mouse_buttons::Right_click))
			{
				auto mousePos = InputSys->get_current_mouse_position();

				mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));


				float aspect_ratio = originalScale.y != 0 ? originalScale.x / originalScale.y : originalScale.x;

				auto factor = mousePos.y - originalMouse.y;
				glm::vec3 new_scale(originalScale.x + 2*factor*aspect_ratio, originalScale.y + 2*factor, originalScale.z);

				transform->SetWorldScale(new_scale);
				changed = true;
			}
		}
		break;
		case Action::Move:
		{
			if (InputSys->mouse_is_down(mouse_buttons::Right_click))
			{
				auto mousePos = InputSys->get_current_mouse_position();

				mousePos = RndMngr->ProjToWindowInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->EditorCamToProjInv(glm::vec3(mousePos, 0.0f));
				mousePos = RndMngr->WorldToEditorCamInv(glm::vec3(mousePos, 0.0f));

				transform->SetWorldPosition(glm::vec3(mousePos, transform->GetWorldPosition().z));
				changed = true;

			}
		}
		break;
		}
		return changed;
	}

	void DrawTransformRect(Transform2D& transform, Space* parent_space, Color color)
	{
		parent_space->Drawrectangle(transform.mPosition, (int)transform.mScale.x, (int)transform.mScale.y, color);
	}
}