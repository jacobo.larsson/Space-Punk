#pragma once
class GameObject;
class Transform2D;
class TransformComponent;
class Color;
class Space;

namespace ObjectEdition
{
	enum Gizmos 
	{
		Transform,
		Collider,
		None
	};

	extern Gizmos editing_gizmo;
	extern Gizmos prev_editing_gizmo;
	

	bool EditSelectedObject(TransformComponent* object);
	void DrawGizmos(TransformComponent* transform);
	void UpdateEditors(TransformComponent* transform);
	bool EditObject(TransformComponent* object);


	void DrawTransformRect(Transform2D& transform, Space* parent_space, Color color);
}