#include <map>
#include <math.h>
#include <algorithm>

#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Graphics/Color/Color.h"
#include "../../Graphics/RenderManager/RenderManager.h"
#include "../../Graphics/Window/Window.h"
#include "../../Input/Input.h"
#include "../../Physics/PhysicSystem.h"
#include "../../Audio/AudioManager.h"
#include "../../Input/Input.h"
#include "../../MainEngine/MainEngine.h"
#include "../../Factory/Factory.h"
#include "../../Changer/Changer.h"
#include "../../Debug/Console.h"
#include "../../Settings/Settings.h"
#include "../../Collisions/CollisionSystem.h"

#include "../../Transform2D/TransformComponent.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../Audio/SoundEmitter.h"
#include "../../Physics/RigidBody.h"
#include "../../Collisions/ICollider.h"
#include "../../Collisions/CollisionTable.h"
#include "../../Graphics/Camera/Camera.h"
#include "../../Game/CameraFollow/CameraFollow.h"
#include "../../RayCast/Raycast.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Editor/ObjectEdition/ObjectEdition.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../Editor.h"
#include "../../OpenSaveFile/OpenSaveFile.h"

#include "Menus.h"

using namespace MenusConstans;

Menus* Menus::instance = nullptr;

void Menus::Initialize()
{
	VectorToCharArray(EditorSys->allComponents, compBuffer, bufferSize);
	VectorToCharArray(EditorSys->AllStateMachines, statesBuffer, bufferSize);
}
void Menus::Shutdown()
{
	DeleteBuffer(compBuffer, EditorSys->allComponents.size());
	DeleteBuffer(statesBuffer, EditorSys->AllStateMachines.size());
}

void Menus::MenuBar()
{
	ImGui::ShowDemoWindow();
	static char mInputbuffer[bufferSize];

	ImGuiWindowFlags window_flags = 0;
	window_flags |= ImGuiWindowFlags_MenuBar;
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("Files"))
		{
			HelpMenu("The level must be already loaded by dragging and dropping,\nor be loaded in this menu.");
			ImGui::SameLine();
			if (ImGui::BeginMenu("Load Level"))
			{
				Level* lvl;
				for (auto& it : RsrcMan->Resourcesmap[TypeLevel])
				{
					lvl = reinterpret_cast<Level*>(it.second->mRawData);
					if (ImGui::Button(lvl->level_name.c_str()))
					{
						SceneSys->SaveLevel();
						SceneSys->ChangeLevel(lvl->level_name.c_str());
						ChangerSys->Clearchanges();
						EditorSys->SetActiveSpace(nullptr);
						EditorSys->SetPickedObject(nullptr);
					}
				}
				if (ImGui::Button("Add Level"))
				{
					std::string filename; // default;
					OpenSaveFileDlg dlg;
					if (dlg.Open("Open Level")) {
						filename = dlg.GetFiles().front();
					}

					size_t found = filename.find("data");
					if (found != std::string::npos)
					{
						size_t found = filename.find("data");
						if (found != std::string::npos)
						{
							std::string slash = filename.substr(found);
							ExtensionTypes ext = RsrcMan->GetExtensionOfFile(filename);
							filename = std::string("./") + filename.substr(found, 4) + std::string("/") + filename.substr(found + 5);
							RsrcMan->Load(filename);
						}
					}
				}
				ImGui::EndMenu();
			}
			HelpMenu("Level name is required");
			ImGui::SameLine();
			if (ImGui::BeginMenu("New Level"))
			{
				static char mLevelName[bufferSize];

				ImGui::PushID("mLevelName");
				ImGui::InputText("Level Name: ", mLevelName, bufferSize);
				ImGui::SameLine();
				if (ImGui::Button("Create"))
				{
					SceneSys->NewLevel(mLevelName);
					ResetBuffer(mLevelName, bufferSize);
					EditorSys->SetActiveSpace(nullptr);
					EditorSys->SetPickedObject(nullptr);
				}
				ImGui::PopID();
				ImGui::EndMenu();
			}

			HelpMenu("Click on the level you want to delete.(WIP, not working at the moment)");
			ImGui::SameLine();
			if (ImGui::BeginMenu("Delete Level"))
			{
				ImGui::EndMenu();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Option"))
		{
			HelpMenu("Simply go back to editor. If already in editor, reset the scene.");
			ImGui::SameLine();
			if (ImGui::MenuItem("Go Back to Editor"))
			{
				Engine->OpenEditor();
				SceneSys->Reset();
				EditorSys->SetActiveSpace(nullptr);
				EditorSys->SetPickedObject(nullptr);
			}

			HelpMenu("Generates new ids for all the objects in the scene");
			ImGui::SameLine();
			if (ImGui::MenuItem("Remove all duplicated IDs"))
			{
				for (auto& _Space : SceneSys->GetAllSpaces())
				{
					for (auto& _Object : _Space->GetAllObjects())
					{
						_Object->mUID = FactorySys->GenerateNewID();
					}
				}
			}
			HelpMenu("Menu for the editor camera.");
			ImGui::SameLine();
			if(ImGui::Button("Editor Camera Menu"))
				ImGui::OpenPopup("Editor Camera menu"); 
			ShowEditorCameraMenu();

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Search"))
		{
			HelpMenu("Search all the objects with the specified collision group");
			ImGui::SameLine();
			if (ImGui::MenuItem("Search By Collision Group"))
				ShowSearchByCollisionGroupMenu = !ShowSearchByCollisionGroupMenu;

			HelpMenu("Search all the objects with the specified archetype");
			ImGui::SameLine();
			if (ImGui::MenuItem("Search By Archetype"))
				ShowSearchByArchetypeMenu = !ShowSearchByArchetypeMenu;

			HelpMenu("Search all the objects with the component searched");
			ImGui::SameLine();
			if (ImGui::MenuItem("Search By Component"))
				ShowSearchByComponentMenu = !ShowSearchByComponentMenu;

			HelpMenu("Search all the objects with the tag searched");
			ImGui::SameLine();
			if (ImGui::MenuItem("Search By Tag"))
				ShowSearchByTagsMenu = !ShowSearchByTagsMenu;



			if (ImGui::MenuItem("Search By UID"))
			{
				ShowSearchByUIDMenu = !ShowSearchByUIDMenu;
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Management"))
		{
			HelpMenu("Create or remove tags.");
			ImGui::SameLine();
			if (ImGui::Button("Manage Registered Tags"))
				ImGui::OpenPopup("TagsManagement");
			ShowTagsManagement(SceneSys->allTags);

			HelpMenu("Create game objects from archetypes");
			ImGui::SameLine();
			if (ImGui::Button("Archetypes Menu"))
			{
				ShowArchetypesMenu = !ShowArchetypesMenu;
			}

			HelpMenu("Add or remove collision groups");
			ImGui::SameLine();
			if (ImGui::Button("Manage Collision Groups"))
				ImGui::OpenPopup("Collision Groups menu");
			CollisionGroupsMenu();

			HelpMenu("Edit the collision policies of the existing groups.");
			ImGui::SameLine();
			if (ImGui::Button("Edit collision table"))
				ImGui::OpenPopup("Collision Table menu");
			CollisionTableMenu();

			if (ImGui::Button("Import object"))
			{
				std::string filename; // default;
				OpenSaveFileDlg dlg;
				if (dlg.Open("Open Object")) {
					filename = dlg.GetFiles().front();
				}


				size_t found = filename.find("data");
				size_t found2 = filename.find("ExportedObjects");
				if (found != std::string::npos && found2 != std::string::npos)
				{
					
					std::string slash = filename.substr(found);
					filename = std::string("./") + filename.substr(found, 4) + std::string("/") + filename.substr(found2,15)
						+ std::string("/") + filename.substr(found2+16);


					Space* curr_space = SceneSys->GetMainSpace();
					GameObject* to_create = curr_space->NewGameObject();
					

					std::ifstream fp1(filename, std::ios_base::out | std::ios_base::in);
					json j;
					if (fp1.is_open() && fp1.good())
					{
						fp1 >> j;
						fp1.close();
						to_create->FromJson(j);
						to_create->mUID = FactorySys->GenerateNewID();
						to_create->spaceId = to_create->GetParentSpace()->GetUID();
					}


				}


			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Debug Console"))
		{
			HelpMenu("Show debug console to see printed messages.");
			ImGui::SameLine();
			if (ImGui::MenuItem("Show debug console"))
				DebugConsole = !DebugConsole;

			ImGui::EndMenu();

		}

		if (ImGui::BeginMenu("Resource list"))
		{
			HelpMenu("Show a list of all the loaded resources.");
			ImGui::SameLine();
			if (ImGui::MenuItem("Show Resource List"))
				ResourceList = !ResourceList;

			ImGui::EndMenu();

		}

		if (ImGui::BeginMenu("Settings menu"))
		{
			HelpMenu("Show the settings menu.");
			ImGui::SameLine();
			if (ImGui::Button("Open settings"))
				ImGui::OpenPopup("Settings");
			ShowSettingsMenu();

			HelpMenu("Show all the objects colliders.");
			ImGui::SameLine();
			ImGui::Checkbox("Show Colliders", &EditorSys->showing_colliders);

			ImGui::EndMenu();

		}

		ImGui::EndMainMenuBar();
	}
}
void Menus::SpaceMenu()
{
	if (ImGui::Begin(("Spaces Menu"), nullptr, 0))
	{

		ImGui::PushID("SceneButtons");
		ImGui::Columns(3, "SceneButtons", true);

		if (ImGui::Button("Play") || (InputSys->key_is_triggered(keys::LeftControl)&&InputSys->key_is_down(keys::F5)))
		{
			SceneSys->SaveLevel();
			RsrcMan->FindDumpResourcesInLevel();
			Engine->CloseEditor();
			EditorSys->SetActiveSpace(nullptr);
			EditorSys->UnPickObject();
		}

		ImGui::NextColumn();

		if (ImGui::Button("Reset"))
		{
			SceneSys->Reset();
			EditorSys->SetActiveSpace(nullptr);
			EditorSys->SetPickedObject(nullptr);
		}

		ImGui::NextColumn();

		if (ImGui::Button("Save"))
		{
			SceneSys->SaveLevel();
		}

		ImGui::Columns(1);
		ImGui::PopID();


		ImGui::InputText("Level to export name", level_to_export_name, 50);

		if (ImGui::Button("Export Level"))
		{
			GameObject* to_export = FactorySys->Create<GameObject>();
			to_export->SetName(std::string(level_to_export_name) +std::string("_") + std::string("lvl"));
			
			to_export->AddComp(reinterpret_cast<IComp*>(FactorySys->Create<TransformComponent>()));
			
			
			auto end = SceneSys->GetMainSpace()->AllParents.end();
			for (auto it = SceneSys->GetMainSpace()->AllParents.begin(); it != end; it++)
			{
				std::string name_exception = (*it)->GetName();
				std::transform(name_exception.begin(), name_exception.end(), name_exception.begin(), ::toupper);

				if (name_exception.find("PLAYER") != std::string::npos || name_exception=="CAMERA")
				{

					continue;

				}

				to_export->AddChild(*it);


			}


			std::string a = level_to_export_name;
			std::string new_string = std::string("./data/ExportedObjects/") + a + std::string("_")  + std::string("lvl") +
				std::string("_")+std::string(".json");

			json j;

			std::ofstream fp1(new_string);
			to_export->ToJson(j);

			if (fp1.is_open() && fp1.good())
			{
				fp1 << std::setw(4) << j;
				fp1.close();
			}


		}

		

		static char SpaceNewName[bufferSize];
		ImGui::InputText("NewSpaceName", SpaceNewName, bufferSize);
		if (ImGui::Button("Create Space") && SpaceNewName[0] != '\0')
		{
			ChangerSys->AddChange(SceneSys->NewSpace(SpaceNewName), SpaceAdded);
			ResetBuffer(SpaceNewName, bufferSize);
		}

		if (ImGui::CollapsingHeader("Spaces List"))
		{
			for (auto& space : SceneSys->GetAllSpaces())
			{
				if (ImGui::TreeNode(space->GetName().c_str()))
				{
					EnabledMenu(space);
					ImGui::Checkbox("Space Partitioning Enabled", &space->mSpacePartitioningEnabled);
					if (space->GetName() != "Main")
					{
						static char SpaceName[bufferSize];
						if (ShowInputText("Rename", SpaceName, bufferSize))
						{
							space->SetName(SpaceName);
							ResetBuffer(SpaceName, bufferSize);
						}
						if (ImGui::Button("Delete"))
						{
							ChangerSys->AddChange(const_cast<Space*>(EditorSys->GetActiveSpace()), SpaceErased);
							SceneSys->DestroySpace(space->GetName().c_str());
						}
					}
					if (ImGui::Button("Create Game Object"))
					{
						auto _NewObject = space->NewGameObject("Game Object");
						_NewObject->AddComp<TransformComponent>(FactorySys->Create<TransformComponent>());
						ChangerSys->AddChange(_NewObject, ObjectAdded);
					}
					if (ImGui::InputInt("Set Order", &space->GetOrder()))
					{
						//ChangerSys->AddChange(space->NewGameObject(""), ObjectAdded);
					}
					ImGui::TreePop();
				}
			}
		}
		ImGui::End();
	}
	else
	{
		ImGui::End();
		return;
	}
}
void Menus::GameObjectsMenu()
{
	if (ImGui::Begin("Game Objects Menu", nullptr, 0))
	{
		if (ImGui::BeginTabBar("", 0))
		{
			for (auto& _space : SceneSys->GetAllSpaces())
			{
				if (ImGui::BeginTabItem(_space->GetName().c_str()))
				{
					ImGui::InputText("Search object",searcher,50);
					if (searcher[0] == '\0')
					{
						std::vector<GameObject*>& Parents = _space->GetAllParents();
						std::vector<HierarchyChange> Changes;

						for (auto& _object : Parents)
						{
							ImGui::PushID(&(*_object));
							
							bool selected = ImGui::Selectable(_object->GetName().c_str());
							if (ImGui::IsItemHovered())
							{
								if (auto transform = _object->GetComp<TransformComponent>())
									SceneSys->GetMainSpace()->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Color(1.0f, 0.0f, 1.0f, 1.0f), transform->GetWorldOrientation());

							}
							_object->EditHierarchy(true, &Changes);
							
							
							if (selected && InputSys->key_is_triggered(keys::LeftControl))
							{
								std::cout << "hey" << std::endl;
								EditorSys->mStateMachine.ChangeState("EditMultipleObjects");
								EditorSys->mStateMachine.multiple_selection.push_back(_object);
								/*EditorSys->SetPickedObject(_object);
								EditorSys->mStateMachine.ChangeState("EditSelectedObject");*/
							}
							else if (selected)
							{
								EditorSys->mStateMachine.multiple_selection.clear();
								EditorSys->SetPickedObject(_object);
								EditorSys->mStateMachine.ChangeState("EditSelectedObject");

							}
							ImGui::PopID();
						}
						if (Changes.size())
						{
							for (auto change : Changes)
							{
								//If the NewParent is null then we are adding to the space
								if (change.NewParent || change.OldParent)
								{
									if (change.NewParent)
										change.NewParent->AddChild(change.ChildToChange);
									else
									{
										change.ChildToChange->mParent = nullptr;
										Parents.push_back(change.ChildToChange);
										auto trans = change.ChildToChange->GetComp<TransformComponent>();
										if (trans)
											trans->SetParentTransform(nullptr);
									}

									//If the OldParent is null then we are removing from the space
									if (change.OldParent)
									{
										auto child_it = std::find(change.OldParent->mChildren.begin(), change.OldParent->mChildren.end(), change.ChildToChange);
										if (child_it != change.OldParent->mChildren.end())
											change.OldParent->mChildren.erase(child_it);
									}
									else
									{
										auto child_it = std::find(Parents.begin(), Parents.end(), change.ChildToChange);
										if (child_it != Parents.end())
											Parents.erase(child_it);
									}
								}
								else
								{
									auto& ParentList = change.ChildToChange->mParent->mChildren;
									auto child_it = std::find(ParentList.begin(), ParentList.end(), change.ChildToChange);
									if (child_it != ParentList.end())
										ParentList.erase(child_it);
									Space* space = change.ChildToChange->mParentSpace;
									space->DestroyObject(change.ChildToChange);

								}
							}

						}
					}
					else
					{
						std::vector<GameObject*>& Parents = _space->GetAllParents();
						std::vector<HierarchyChange> Changes;

						for (auto& _object : Parents)
						{
							bool found = true;
							int i = 0;
							while (searcher[i] != '\0' && i<50)
							{
								if (searcher[i] != _object->GetName()[i] )
								{
									found = false;
									break;
								}
								i++;
							}
							if (found == true)
							{
								ImGui::PushID(&(*_object));
								bool selected = ImGui::Selectable(_object->GetName().c_str());
								if (ImGui::IsItemHovered())
								{
									if (auto transform = _object->GetComp<TransformComponent>())
										SceneSys->GetMainSpace()->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Color(1.0f, 0.0f, 1.0f, 1.0f), transform->GetWorldOrientation());

								}
								_object->EditHierarchy(true, &Changes);
								if (selected && InputSys->key_is_triggered(keys::LeftControl))
								{
									std::cout << "hey" << std::endl;
									EditorSys->mStateMachine.ChangeState("EditMultipleObjects");
									EditorSys->mStateMachine.multiple_selection.push_back(_object);
									/*EditorSys->SetPickedObject(_object);
									EditorSys->mStateMachine.ChangeState("EditSelectedObject");*/
								}
								else if (selected)
								{
									EditorSys->mStateMachine.multiple_selection.clear();
									EditorSys->SetPickedObject(_object);
									EditorSys->mStateMachine.ChangeState("EditSelectedObject");

								}
								ImGui::PopID();
							}
						}
						if (Changes.size())
						{
							for (auto change : Changes)
							{
								//If the NewParent is null then we are adding to the space
								if (change.NewParent || change.OldParent)
								{
									if (change.NewParent)
										change.NewParent->AddChild(change.ChildToChange);
									else
									{
										change.ChildToChange->mParent = nullptr;
										Parents.push_back(change.ChildToChange);
										auto trans = change.ChildToChange->GetComp<TransformComponent>();
										if (trans)
											trans->SetParentTransform(nullptr);
									}

									//If the OldParent is null then we are removing from the space
									if (change.OldParent)
									{
										auto child_it = std::find(change.OldParent->mChildren.begin(), change.OldParent->mChildren.end(), change.ChildToChange);
										if (child_it != change.OldParent->mChildren.end())
											change.OldParent->mChildren.erase(child_it);
									}
									else
									{
										auto child_it = std::find(Parents.begin(), Parents.end(), change.ChildToChange);
										if (child_it != Parents.end())
											Parents.erase(child_it);
									}
								}
								else
								{
									auto& ParentList = change.ChildToChange->mParent->mChildren;
									auto child_it = std::find(ParentList.begin(), ParentList.end(), change.ChildToChange);
									if (child_it != ParentList.end())
										ParentList.erase(child_it);
									Space* space = change.ChildToChange->mParentSpace;
									space->DestroyObject(change.ChildToChange);

								}
							}

						}
					}
					ImGui::EndTabItem();
				}
			}
			ImGui::EndTabBar();
		}
		ImGui::End();
	}
	else
		ImGui::End();
	
}

void Menus::ShowTagsManagement(std::vector<std::string>& tags)
{
	if (ImGui::BeginPopupModal("TagsManagement", nullptr, 0))
	{
		static char TagsManagementBuffer[bufferSize];

		ImGui::InputText("Register new tag", TagsManagementBuffer, bufferSize);
		if (ImGui::Button("CreateTag") && (TagsManagementBuffer[0] != '\0'))
		{
			SceneSys->RegisterTag(TagsManagementBuffer);
			ResetBuffer(TagsManagementBuffer, bufferSize);
		}

		ImGui::PushID("Delete tag");
		if (ImGui::CollapsingHeader("Remove Tag"))
		{
			for (auto tag : tags)
			{
				if (ImGui::Selectable(tag.c_str()))
				{
					SceneSys->RemoveTag(tag);
				}
			}
		}
		ImGui::PopID();
		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		ImGui::EndPopup();

	}
}

void Menus::ShowSearchByCollisionGroup()
{
	if (ShowSearchByCollisionGroupMenu)
	{
		if (ImGui::Begin("Search By Collision Group", nullptr, 0))
		{
			static Space* active_space = nullptr;

			ImGui::Text("Select the space in which you want to look for.");
			ImGui::SameLine();
			if (ImGui::Button("Close"))
			{
				ShowSearchByCollisionGroupMenu = false;
			}
			for (auto& space : SceneSys->mSpaces)
			{
				if (ImGui::Button(space->GetName().c_str()))
				{
					active_space = space;
				}
				ImGui::SameLine();
			}

			if (active_space != nullptr)
			{
				ImGui::NewLine();

				ImGui::PushID("SearchByCollisionGroup");
				if (ImGui::CollapsingHeader("CollisionGroupToSearch"))
				{
					for (auto group = CollTable->CollisionTypesVector.begin(); group != CollTable->CollisionTypesVector.end(); group++)
					{
						if (ImGui::Selectable(group->second.c_str()))
						{
							collgroupSearch = active_space->FindAllObjectsByCollisionGroup(group->second.c_str());
						}
					}
				}
				ImGui::PopID();
			}

			ImGui::Text("Found Objects");
			if (collgroupSearch.size())
			{
				TransformComponent* transform;
				for (auto it : collgroupSearch)
				{
					ImGui::PushID(&(*it));
					transform = it->GetComp<TransformComponent>();
					if (transform != nullptr)
						active_space->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Colors::red);
					if (ImGui::CollapsingHeader(it->GetName().c_str()))
					{
							EditorSys->SetPickedObject(it);
							EditorSys->mStateMachine.ChangeState("EditSelectedObject");
					}
					ImGui::PopID();
				}
			}

			ImGui::End();
		}
		else
			ImGui::End();
	}
}
void Menus::ShowSearchByArchetype()
{
	if (ShowSearchByArchetypeMenu)
	{
		if (ImGui::Begin("Search By Archetype", nullptr, 0))
		{
			static Space* active_space = nullptr;

			ImGui::Text("Select the space in which you want to look for.");
			ImGui::SameLine();
			if (ImGui::Button("Close"))
			{
				ShowSearchByArchetypeMenu = false;
			}
			for (auto& space : SceneSys->mSpaces)
			{
				if (ImGui::Button(space->GetName().c_str()))
				{
					active_space = space;
				}
				ImGui::SameLine();
			}

			if (active_space != nullptr)
			{
				ImGui::NewLine();

				ImGui::PushID("SearchByArchetype");
				if (ImGui::CollapsingHeader("ArchetypeToSearch"))
				{
					auto archetype_map = RsrcMan->Resourcesmap[ExtensionTypes::TypeArchetype];
					for (auto archetype = archetype_map.begin(); archetype != archetype_map.end(); archetype++)
					{
						std::string temp = archetype->first;
						temp.erase(temp.find("./data/Archetypes/"), 18);
						temp.erase(temp.find(".archetype"), 10);

						if (ImGui::Selectable(temp.c_str()))
							archetypeSearch = active_space->FindAllObjectsByArchetype(temp.c_str());

					}
				}
				ImGui::PopID();
			}

			ImGui::Text("Found Objects");
			if (archetypeSearch.size())
			{
				TransformComponent* transform;
				for (auto it : archetypeSearch)
				{
					ImGui::PushID(&(*it));
					transform = it->GetComp<TransformComponent>();
					if (transform != nullptr)
						active_space->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Colors::red);
					if (ImGui::CollapsingHeader(it->GetName().c_str()))
					{
						EditorSys->SetPickedObject(it);
						EditorSys->mStateMachine.ChangeState("EditSelectedObject");
					}
					ImGui::PopID();
				}
			}
			ImGui::End();
		}
		else
			ImGui::End();
	}
}
void Menus::ShowSearchByComponent()
{
	if (ShowSearchByComponentMenu)
	{
		if (ImGui::Begin("SearchByComponent", nullptr, 0))
		{
			static Space* active_space = EditorSys->GetActiveSpace();

			ImGui::Text("Select the space in which you want to look for.");
			ImGui::SameLine();
			if (ImGui::Button("Close"))
			{
				ShowSearchByComponentMenu = false;
			}
			for (auto& space : SceneSys->mSpaces)
			{
				if (ImGui::Button(space->GetName().c_str()))
				{
					active_space = space;
				}
				ImGui::SameLine();
			}
			if (active_space != nullptr)
			{
				static unsigned compidx = 0;
				static std::string searchedComp;
				ImGui::NewLine();
				if (ImGui::BeginCombo("Components", compBuffer[compidx], 0))
				{
					for (unsigned i = 0; i < EditorSys->allComponents.size(); i++)
					{
						if (ImGui::Selectable(compBuffer[i], compidx == i))
						{
							compidx = i;
							searchedComp = compBuffer[i];
							compSearch.clear();
							active_space->FindAllObjectsWithComponent(searchedComp.c_str(), compSearch);
						}
						if (compidx == i)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
				TransformComponent* transform;
				for (auto it : compSearch)
				{
					ImGui::PushID(&(*it));
					transform = it->GetComp<TransformComponent>();
					if (transform != nullptr)
						active_space->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Colors::red);
					if (ImGui::CollapsingHeader(it->GetName().c_str()))
					{
						if (EditorSys->GetPickedObject() == nullptr)
							EditorSys->SetPickedObject(it);
					}
					ImGui::PopID();
				}
			}
			ImGui::End();
		}
		else
			ImGui::End();
	}
}
void Menus::ShowSearchByTag()
{
	if (ShowSearchByTagsMenu)
	{
		if (ImGui::Begin("Search By Tag", nullptr, 0))
		{
			static Space* active_space = nullptr;

			ImGui::Text("Select the space in which you want to look for.");
			ImGui::SameLine();
			if (ImGui::Button("Close"))
			{
				ShowSearchByTagsMenu = false;
			}
			for (auto& space : SceneSys->mSpaces)
			{
				if (ImGui::Button(space->GetName().c_str()))
				{
					active_space = space;
				}
				ImGui::SameLine();
			}

			if (active_space != nullptr)
			{
				static char TagName[bufferSize];
				ImGui::NewLine();

				ImGui::PushID("SearchByTag");
				if (ImGui::CollapsingHeader("TagToSearch"))
				{
					for (auto tag : SceneSys->GetAllTags())
					{
						if (ImGui::Selectable(tag.c_str()))
						{
							tagVector = active_space->FindAllObjectsByTag(tag.c_str());
						}
					}
				}
				ImGui::PopID();
			}

			ImGui::Text("Found Objects");
			if (tagVector != nullptr)
			{
				TransformComponent* transform;
				for (auto it : *tagVector)
				{
					ImGui::PushID(&(*it));
					transform = it->GetComp<TransformComponent>();
					if (transform != nullptr)
						active_space->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Colors::red);
					if (ImGui::CollapsingHeader(it->GetName().c_str()))
					{
						if (EditorSys->GetPickedObject() == nullptr)
							EditorSys->SetPickedObject(it);
					}
					ImGui::PopID();
				}
			}

			ImGui::End();
		}
		else
			ImGui::End();
	}
}
void Menus::ShowArchetypes()
{
	if (ShowArchetypesMenu)
	{
		if (ImGui::Begin("Archetypes Menu", nullptr, 0))
		{
			Space** active_space = &EditorSys->active_space;

			ImGui::Text("Select the space in which you want to add the archetype.");
			ImGui::NewLine();
			for (auto& space : SceneSys->mSpaces)
			{
				if (ImGui::Button(space->GetName().c_str()))
				{
					*active_space = space;
				}
				ImGui::SameLine();
			}

			if (*active_space != nullptr)
			{
				ImGui::Text("The archetype will be added to: ");
				ImGui::SameLine();
				ImGui::Text((*active_space)->GetName().c_str());
				for (auto archetype : RsrcMan->name_files[TypeArchetype])
				{
					if (ImGui::Selectable(archetype.c_str()))
					{
						auto archetypeInstance = reinterpret_cast<Resource<Archetype>*>(RsrcMan->Load(archetype));
						glm::vec3 windowCenter(0.0f, 0.0f, 0.0f);
						windowCenter = RndMngr->EditorCamToProjInv(windowCenter);
						windowCenter = RndMngr->WorldToEditorCamInv(windowCenter);

						auto newObj = archetypeInstance->get()->GetObject()->Clone();
						if (newObj->HasComp<TransformComponent>())
							newObj->GetComp<TransformComponent>()->SetWorldPosition((windowCenter));

						(*active_space)->AddGameObject(newObj);
					}
				}
			}
			else
			{
				ImGui::NewLine();
				ImGui::Text("There is no active space to add the object.");
			}
			if (ImGui::Button("Close"))
				ShowArchetypesMenu = false;

			ImGui::End();
			return;
		}
		else
		{
			ImGui::End();
		}
	}
}

void Menus::ShowSearchByUID()
{
	if (ShowSearchByUIDMenu)
	{
		if (ImGui::Begin("Search By UID", nullptr, 0))
		{
			static std::string _SearchedID;
			static char _InputText[64];
			ShowInputText("Searched ID: ", _InputText, 64);
			if (ImGui::Button("Search"))
			{
				mIDSearch.clear();
				for (auto& space : SceneSys->mSpaces)
				{
					space->FindAllObjectsByUID(std::atoi(_InputText), mIDSearch);
					ImGui::SameLine();
				}
			}

			ImGui::Text("Found Objects");
			for (auto _Object : mIDSearch)
			{
				ImGui::PushID(&(*_Object));
				if (ImGui::CollapsingHeader(_Object->GetName().c_str()))
				{
					if (EditorSys->GetPickedObject() == nullptr)
						EditorSys->SetPickedObject(_Object);
				}

				TransformComponent* transform;
				transform = _Object->GetComp<TransformComponent>();
				if (transform != nullptr)
					SceneSys->GetMainSpace()->Drawrectangle(transform->GetWorldPosition(), transform->GetWorldScale().x, transform->GetWorldScale().y, Colors::red);

				ImGui::PopID();
			}
			if (ImGui::Button("Generate new IDS for them"))
			{
				for (auto _Object : mIDSearch)
				{
					_Object->mUID = FactorySys->GenerateNewID();
				}
				mIDSearch.clear();
			}


			ImGui::End();
		}
		else
			ImGui::End();
	}
}

bool Menus::ShowInputText(std::string name, char* buffer, unsigned size)
{
	ImGui::PushID(name.c_str());
	ImGui::Columns(2, name.c_str(), true);
	ImGui::InputText(name.c_str(), buffer, size);
	ImGui::NextColumn();

	bool res = ImGui::Button("Apply");

	ImGui::Columns(1);
	ImGui::PopID();

	return res;
}
void Menus::ResetBuffer(char* buffer, unsigned size)
{
	for (unsigned i = 0; i < size; i++)
		buffer[i] = '\0';
}
void Menus::HelpMenu(const char* text)
{
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(text);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}
void Menus::EnabledMenu(IBase* obj)
{
	ImGui::Checkbox("Enabled", &(obj->Enabled()));
}

bool Menus::ShowComponentsMenu(GameObject* object)
{
	bool changed = false;
	for (auto& comp : object->mComponents)
	{
		if (ImGui::CollapsingHeader(comp->GetName().c_str()))
		{
			changed = comp->Edit();

			ImGui::PushID(&comp);
			if (ImGui::Button("Remove this components"))
			{
				object->RemoveComp(comp);
				ChangerSys->AddChange(object, ObjectChanged);
			}
			if (changed == true)
			{
				ChangerSys->AddChange(object, ObjectChanged);
			}
			ImGui::PopID();
		}
		changed = false;
	}

	ObjectEdition::prev_editing_gizmo = ObjectEdition::editing_gizmo;
	ObjectEdition::editing_gizmo = ObjectEdition::None;

	return changed;
}
void Menus::ShowComponentManagement(GameObject* object)
{
	if (ImGui::CollapsingHeader("AddComponent"))
	{
		static unsigned compIndex = 0;
		if (ImGui::BeginCombo("Components", compBuffer[compIndex], 0))
		{
			for (unsigned i = 0; i < EditorSys->allComponents.size(); i++)
			{
				if (ImGui::Selectable(compBuffer[i], compIndex == i))
				{
					ChangerSys->AddChange(object, ObjectChanged);
					compIndex = i;
					object->AddComp(reinterpret_cast<IComp*>(FactorySys->Create(compBuffer[i])));
				}

				if (compIndex == i)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();

		}
	}
}
bool Menus::TagsMenu(GameObject* object)
{
	bool isOpened = false;
	if (ImGui::BeginPopupModal("Game Object Tags Menu", nullptr, 0))
	{
		static char TagBuffer[bufferSize];

		ImGui::InputText("NewTag", TagBuffer, bufferSize);
		if (ImGui::Button("CreateTag") && (TagBuffer[0] != '\0'))
		{
			object->AddTag(TagBuffer);
			ResetBuffer(TagBuffer, bufferSize);
		}
		ImGui::NewLine();
		if (ImGui::CollapsingHeader("My Tags", 0))
		{
			for (auto& tag : object->GetTags())
			{
				ImGui::Text(tag.c_str());
			}
		}

		ImGui::PushID(&object);
		if (ImGui::BeginCombo("", "Add Existing Tag", 0))
		{
			for (auto tag : SceneSys->GetAllTags())
			{
				if (ImGui::Selectable(tag.c_str()))
				{
					object->AddTag(tag);
				}
			}
			ImGui::EndCombo();
		}
		ImGui::PopID();
		if (object->GetTags().size() > 0)
		{
			ImGui::PushID("RemoveTag");
			if (ImGui::BeginCombo("", "Remove Tag", 0))
			{
				for (auto tag : object->GetTags())
				{
					if (ImGui::Selectable(tag.c_str()))
					{
						object->RemoveTag(tag);
					}
				}
				ImGui::EndCombo();
			}
			ImGui::PopID();
		}
		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		ImGui::EndPopup();
		return isOpened = true;
	}
	return isOpened;
}

void Menus::CollisionGroupsMenu()
{
	if (ImGui::BeginPopupModal("Collision Groups menu", nullptr, 0))
	{
		static char CollisionBuffer[bufferSize];

		ImGui::InputText("NewCollisionGroup", CollisionBuffer, bufferSize);
		if (ImGui::Button("CreateCollisionGroup") && (CollisionBuffer[0] != '\0'))
		{
			CollTable->CreateNewCollisionGroup(CollisionBuffer);
			ResetBuffer(CollisionBuffer, bufferSize);

		}
		ImGui::PushID("Delete Collision Group");
		if (ImGui::CollapsingHeader("Delete Collision Group"))
		{
			for (auto collisiongroup : CollTable->CollisionTypesVector)
			{
				if (ImGui::Selectable(collisiongroup.second.c_str()))
				{
					CollTable->RemoveCollisionGroup(collisiongroup.second);
					break;
				}
			}
		}
		ImGui::PopID();
		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		ImGui::EndPopup();
	}
}
void Menus::CollisionTableMenu()
{
	if (ImGui::BeginPopupModal("Collision Table menu", nullptr, 0))
	{
		ImGui::PushID("CollisionTableColumns");
		ImGui::Columns(CollTable->CollisionTypesVector.size() + 1, "CollisionTableColumns", true);

		auto it = CollTable->CollisionTypesVector.begin();
		ImGui::PushItemWidth(-1);
		ImGui::Text("CollisionTable");
		ImGui::Dummy(ImVec2(0.0f, 6.5f));

		for (; it != CollTable->CollisionTypesVector.end(); it++)
		{
			ImGui::Text(it->second.c_str());
			ImGui::Dummy(ImVec2(0.0f, 6.5f));
		}

		ImGui::NextColumn();
		int i = 0;
		const char* policies[] = { "Ignore", "DetectButNotResolve", "Resolve" };
		static int policyindex = 0;
		const char* label2 = policies[policyindex];
		CollisionPolicies values[] = { Ignore, DetectNotResolve, Resolve };

		int o = 0;
		for (auto it = CollTable->CollisionTypesVector.begin(); it != CollTable->CollisionTypesVector.end(); it++, i++)
		{
			ImGui::Text(it->second.c_str());

			if (i == CollTable->CollisionTypesVector.size() - 1)
				ImGui::Separator();
			else
				ImGui::Dummy(ImVec2(0.0f, 0.9f));
			for (auto et = CollTable->CollisionTypesVector.begin(); et != CollTable->CollisionTypesVector.end(); et++)
			{

				label2 = policies[CollTable->resolvetable(it->first, et->first) - 1];
				ImGui::PushID(o);
				if (ImGui::BeginCombo("", label2, 0))
				{
					for (unsigned i = 0; i < IM_ARRAYSIZE(policies); i++)
					{
						if (ImGui::Selectable(policies[i], CollTable->resolvetable(it->first, et->first) - 1 == i))
						{

							CollTable->mCollisionTableTests[it->first | et->first] = values[i];
						}
						if (CollTable->resolvetable(it->first, et->first) - 1 == i)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
				ImGui::PopID();
				o++;
				if (i == CollTable->CollisionTypesVector.size() - 1)
					ImGui::Separator();
				else
					ImGui::Dummy(ImVec2(0.0f, 0.9f));

			}
			ImGui::NextColumn();
		}
		ImGui::Columns(5);
		ImGui::PopID();
		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		ImGui::EndPopup();
	}
}

void Menus::ShowDebugConsole()
{
	if (DebugConsole)
	{
		bool show = true;
		if (ImGui::Begin("Debug Console Menu", &show, 0))
		{
			ImGui::PushID("Debug Console Menu");
			ConsoleSys->Update();
			if (show == false)
				DebugConsole = false;

			if (ConsoleSys->added == true)
			{
				ImGui::SetScrollHereY(1.f);
				ConsoleSys->added = false;
			}
			ImGui::PopID();
			ImGui::End();
			return;
		}
		else
		{
			ImGui::End();
		}


	}
}
void Menus::ShowResourceList()
{
	if (ResourceList)
	{
		bool show = true;
		if (ImGui::Begin("Resource List Menu", &show, 0))
		{
			
			for (auto resource : RsrcMan->Resourcesmap)
			{
				
				for (auto resourcestring : resource.second)
				{
					
					ImGui::Text(resourcestring.first.c_str());

				}
			}

			if (show == false)
				ResourceList = false;
			if (ImGui::Button("Close"))
				ResourceList = false;
			
			ImGui::End();
			return;
		}
		else
		{
			ImGui::End();
		}


	}
}
void Menus::ShowSettingsMenu()
{
	bool show = true;
	if (ImGui::BeginPopupModal("Settings", &show, 0))
	{
		ImGui::PushID("Settings");
		
		if (ImGui::Checkbox("Toggle FullScreen", &SettingsSys->fullscreen))
		{
			SettingsSys->ToggleFullScreen();
		}

		std::vector<glm::vec2> resolutions;
		resolutions.push_back( glm::vec2(720, 480));
		resolutions.push_back( glm::vec2(1280, 720));
		resolutions.push_back( glm::vec2(1920, 1080));
		
		static int resolution_index = std::distance(resolutions.begin(), std::find(resolutions.begin(), resolutions.end(),SettingsSys->current_res));

		const char* resols[] = { "720x480" , "1280x720", "1920x1080" };
		const char * label = resols[resolution_index];
		
		auto viewport = ImGui::GetMainViewport();
		
		ImGui::PushID("ResolutionsCombo");
		if (ImGui::BeginCombo("Resolutions", label,0))
		{
			for (int i= 0;i<IM_ARRAYSIZE(resols);i++)
			{
				if (ImGui::Selectable(resols[i],i == resolution_index))
				{
					resolution_index=i;
					SettingsSys->current_res = resolutions[i];
					WindowSys->ChangeResolution(resolutions[i]); 			
				}
				if(i== resolution_index)
					ImGui::SetItemDefaultFocus();
			}
			
			ImGui::EndCombo();
		}
		ImGui::PopID();

		ImGui::Checkbox("Pause the game", &SettingsSys->paused);
		ImGui::Checkbox("Draw the debug lines", &SettingsSys->debug_draw);
		ImGui::Checkbox("Print the console in a txt", &SettingsSys->console_to_text);
		ImGui::Checkbox("Set fast load", &SettingsSys->fast_load);
		ImGui::Checkbox("Enable/Disable Space Partitioning", &SettingsSys->SpacePartitioning);
		ImGui::Checkbox("Draw Space Partitioning partitions", &SettingsSys->DrawPartitions);
		ImGui::Checkbox("Draw Active Space Partitioning partitions", &SettingsSys->DrawActivePartitions);
		ImGui::Checkbox("Draw the scene as it would look ingame", &SettingsSys->EditorViewport);
		ImGui::Checkbox("Enable the profiler", &SettingsSys->EnableProfiler);
		ImGui::Checkbox("Enable hot reload for resources", &SettingsSys->EnableHotReload);
		ImGui::Checkbox("Use threads for collisions", &SettingsSys->threadedCollsions);
		ImGui::Text("Last ID registered  %i",SettingsSys->current_max_id);
		ImGui::Checkbox("Visual Grid", &SettingsSys->VisualGrid);
		ImGui::SliderInt("Grid Size", &SettingsSys->GridSize, 10, 500);

		if (ImGui::CollapsingHeader("Order Components"))
		{
			unsigned n = 0;
			for (auto& _Entry : SettingsSys->mCompKeys)
			{
				unsigned key = _Entry.second;

				ImGui::Selectable(_Entry.first.c_str());
				ImGui::SameLine();
				ImGui::Button(std::to_string(_Entry.second).c_str());

				//if (ImGui::IsItemActive() && !ImGui::IsItemHovered())
				//{
				//	int n_next = n + (ImGui::GetMouseDragDelta(0).y < 0.f ? -1 : 1);
				//
				//	if (n_next >= 0 && n_next < SettingsSys->mCompKeys.size())
				//	{
				//		SettingsSys->mCompKeys[_Entry.first] = ;
				//		item_names[n] = item_names[n_next];
				//		item_names[n_next] = item;
				//		ImGui::ResetMouseDragDelta();
				//	}
				//}
				//n++;
			}
		}

		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		if (show == false)
			ImGui::CloseCurrentPopup();
		
		ImGui::PopID();
		ImGui::EndPopup();
	}
}
void Menus::ShowEditorCameraMenu()
{
	static glm::vec3 temp_cam_pos(0, 0, 20);
	static float rotation_degrees = 0.0f;
	if (ImGui::BeginPopupModal("Editor Camera menu", nullptr, 0))
	{
		ImGui::Columns(3, nullptr, true);
		ImGui::DragFloat("X", &temp_cam_pos.x);
		ImGui::NextColumn();
		ImGui::DragFloat("Y", &temp_cam_pos.y);
		ImGui::NextColumn();
		if (ImGui::Button("Set Position"))
		{
			RndMngr->editor_camera->mTransform.mPosition = temp_cam_pos;
			temp_cam_pos = glm::vec3(0, 0, 20);
		}

		ImGui::Columns(1);

		ImGui::DragFloat("Camera Move Velocity", &SettingsSys->camera_move_speed, 1.0f, 1.0f, 150.0f);
		if (SettingsSys->camera_move_speed > 150.0f)
			SettingsSys->camera_move_speed = 150.0f;
		if (SettingsSys->camera_move_speed < 1.0f)
			SettingsSys->camera_move_speed = 1.0f;

		ImGui::Columns(2, nullptr, true);
		ImGui::DragFloat("Rotation", &rotation_degrees);
		ImGui::NextColumn();
		if (ImGui::Button("Set Rotation"))
		{
			RndMngr->editor_camera->mTransform.mOrientation = (2*M_PI*rotation_degrees)/360.0f;
			rotation_degrees = 0.0f;
		}
		
		ImGui::Columns(1);

		float temp_rotation = SettingsSys->camera_rotate_speed / 0.003;
		ImGui::DragFloat("Camera Rotate Velocity", &temp_rotation, 1.0f, 1.0f, 20.0f);
		if (temp_rotation > 20.0f)
			temp_rotation = 2.0f;
		if (temp_rotation < 1.0f)
			temp_rotation = 1.0f;

		SettingsSys->camera_rotate_speed = temp_rotation * 0.003;


		if (ImGui::Button("Close"))
			ImGui::CloseCurrentPopup();
		ImGui::EndPopup();
	}
}
const char* Menus::VectorToCharArray(std::vector<std::string>& vector, char **& buffer, unsigned wordSize)
{
	buffer = new char* [vector.size()];
	for (unsigned i = 0; i < vector.size(); i++)
	{
		buffer[i] = new char[wordSize]();
		for (unsigned j = 0; j < vector[i].size(); j++)
			buffer[i][j] = vector[i][j];
	}
	return nullptr;
}
void Menus::DeleteBuffer(char**& buffer, unsigned size)
{
	for (unsigned i = 0; i < size; i++)
		delete[] buffer[i];
	delete[] buffer;
}