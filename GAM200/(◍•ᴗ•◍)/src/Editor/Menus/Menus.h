#pragma once
#include <string>
#include <vector>

#include "../../RTTI/IBase.h"
#include "../../Singleton/Singleton.h"


class Space;
class GameObject;
class TransformComponent;
class Renderable;
class RigidBody;
class Collider;
class CameraComponent;
class SoundEmiter;
class BrainComponent;
class Ray;

namespace MenusConstans
{
	const unsigned bufferSize = 64;
}

class Menus :public IBase
{
	friend class BrainComponent;
	SM_RTTI_DECL(Menus, IBase);
	Make_Singleton(Menus)

public:
	void Initialize();
	void Shutdown();

	void MenuBar();
	void SpaceMenu();
	void GameObjectsMenu();

	void ShowTagsManagement(std::vector<std::string>& tags);
	void ShowSearchByCollisionGroup();
	void ShowSearchByArchetype();
	void ShowSearchByComponent();
	void ShowSearchByTag();
	void ShowArchetypes();
	void ShowSearchByUID();

	// Helpers needed to be public
	bool ShowInputText(std::string name, char* buffer, unsigned size);
	void ResetBuffer(char* buffer, unsigned size);
	void HelpMenu(const char* text);
	void EnabledMenu(IBase* obj);

	bool ShowComponentsMenu(GameObject* object);
	void ShowComponentManagement(GameObject* object);
	bool TagsMenu(GameObject* object);

	void CollisionGroupsMenu();
	void CollisionTableMenu();
	void ShowDebugConsole();
	void ShowResourceList();
	void ShowSettingsMenu();
	void ShowEditorCameraMenu();
private:
		
	const char* VectorToCharArray(std::vector<std::string>& vector, char **& buffer, unsigned wordSize);
	void DeleteBuffer(char ** & buffer, unsigned size);

	bool ShowSearchByCollisionGroupMenu = false;
	bool ShowSearchByArchetypeMenu = false;
	bool ShowSearchByComponentMenu = false;
	bool ShowSearchByTagsMenu = false;
	bool ShowSearchByUIDMenu = false;
	bool ShowArchetypesMenu = false;
	bool DebugConsole = false;
	bool ShowSettings= false;
	bool ResourceList= false;

	char** compBuffer = nullptr;
	char** statesBuffer = nullptr;
	char level_to_export_name[50] = {'\0'};
	char  searcher[50] = {'\0'};

	std::vector<GameObject*> collgroupSearch;
	std::vector<GameObject*> archetypeSearch;
	std::vector<GameObject*> compSearch;
	std::vector<GameObject*> mIDSearch;
	const std::vector<GameObject*>* tagVector = nullptr;
};
#define MenusSys Menus::Get()