#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"
#include "../AssertSystem/AssertSystem.h"

#include "../Factory/Factory.h"
#include "../PropertySystem/PropertyMap.h"
#include "../Changer/Changer.h"
#include "../Input/Input.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Audio/SoundEmitter.h"
#include "../Transform2D/TransformComponent.h"
#include "../Collisions/Collisions.h"
#include "../Scene/Scene.h"
#include "../MainEngine/MainEngine.h"
#include "SelectableComp.h"
#include "../MenuManager/MenuManager.h"


SelectableComp::SelectableComp()
{
	mName = "SelectableComponent";

}
SelectableComp::SelectableComp(SelectableComp& rhs)
{
	go_to_name = rhs.go_to_name;

}
SelectableComp::~SelectableComp()
{
	transform = nullptr;

}
SelectableComp& SelectableComp::operator=(const SelectableComp& rhs)
{
	go_to_name = rhs.go_to_name;
	return *this;

}
SelectableComp* SelectableComp::Clone()
{
	SelectableComp* temp = FactorySys->Create<SelectableComp>();
	(*temp) = (*this);
	return temp;

}

void SelectableComp::OnCreate()
{
	

}
void SelectableComp::Initialize()
{

	GameObject* object_owner = GetOwner();
	MenuComponent::Initialize();
	transform = object_owner->GetComp<TransformComponent>();
	initial_scale = transform->GetScale();
	
	GameObject * cursor = SceneSys->GetMainSpace()->FindObjectByName("Cursor");
	
	if(cursor)
		cursor_transform = cursor->GetComp<TransformComponent>();
	

	my_rend = object_owner->GetComp<Renderable>();
	my_sound_emitter = object_owner->GetComp<SoundEmiter>();
	my_text_render = object_owner->GetComp<TextRenderable>();
	
	if (my_text_render)
	{
		initial_text_scale = my_text_render->mTransform.mScale;
		initial_text_col = my_text_render->col;
	}
}
void SelectableComp::Update()
{
	if (transform)
	{
		bool hovered = false;
		bool selected = false;
		glm::vec2 mousepos = InputSys->get_current_mouse_position();
		glm::vec3 mousepos3(mousepos.x, mousepos.y,0);

		GameObject* camera = mOwner->GetParentSpace()->FindObjectByName("Camera");
		if (camera)
		{
			CameraComponent* mCamera = camera->GetComp<CameraComponent>();
			if (mCamera)
			{
				mousepos3 = RndMngr->ProjToWindowInv(mousepos3);
				mousepos3 = glm::inverse(mCamera->GetProjectionMatrix()) * glm::vec4(mousepos3, 1.0f);
				mousepos3 = glm::inverse(mCamera->GetViewMatrix()) * glm::vec4(mousepos3, 1.0f);
			}
		}

		if (InputSys->using_gamepad)
		{
			if (cursor_transform)
			{
				if (StaticRectToStaticRect(cursor_transform->GetWorldPosition(), cursor_transform->GetScale().x, cursor_transform->GetScale().y, transform->GetWorldPosition(), transform->GetScale().x, transform->GetScale().y))
				{
					transform->SetWorldScale(glm::vec2(initial_scale.x * 1.25f, initial_scale.y * 1.25f));
					hovered = true;


					if (my_text_render && change_color)
					{
						my_text_render->col = glm::vec3(color_changed.x, color_changed.y, color_changed.z);

						if (my_text_render->pFontInfo)
						{
							my_text_render->mTransform.mScale = glm::vec3(initial_text_scale.x / 1.25f, initial_text_scale.y / 1.25f, 1);
						}

					}

					if (InputSys->gamepadbutton_is_triggered(SDL_CONTROLLER_BUTTON_A))
					{
						if (my_sound_emitter && play_sound)
						{
							my_sound_emitter->Play();
							my_sound_emitter->played = true;
						}

						switch (my_action)
						{
						case None:
							break;
						case ChangeLevel:
							SceneSys->to_change = true;
							SceneSys->ChangeLevelGameCompleteName(go_to_name);
							SceneSys->to_change = true;
							SettingsSys->paused = false;

							break;
						case GoToMenu:




							break;
						case Credits:




							break;
						case Quit:
							Engine->Quit();

							break;
						case Restart:
							SceneSys->to_change = true;
							SceneSys->ResetLevelInGame();
							SettingsSys->paused = false;

							break;
						case Resume:
							SettingsSys->paused = !SettingsSys->paused;
							GameMenuSys->ChangeButtonVisibility();;

							break;
						}

					}

				}
			}
		}
		else
		{
			if (StaticPointToStaticRect(mousepos3, transform->GetWorldPosition(), transform->GetScale().x, transform->GetScale().y))
			{
				transform->SetWorldScale(glm::vec2(initial_scale.x * 1.25f, initial_scale.y * 1.25f));
				hovered = true;


				if (my_text_render && change_color)
				{
					my_text_render->col = glm::vec3(color_changed.x, color_changed.y, color_changed.z);

					if (my_text_render->pFontInfo)
					{
						my_text_render->mTransform.mScale = glm::vec3(initial_text_scale.x / 1.25f, initial_text_scale.y / 1.25f, 1);
					}

				}

				if (InputSys->mouse_is_triggered(mouse_buttons::Left_click))
				{
					if (my_sound_emitter && play_sound)
					{
						my_sound_emitter->Play();
						my_sound_emitter->played = true;
					}

					switch (my_action)
					{
					case None:
						break;
					case ChangeLevel:
						SceneSys->to_change = true;
						SceneSys->ChangeLevelGameCompleteName(go_to_name);
						SceneSys->to_change = true;
						SettingsSys->paused = false;

						break;
					case GoToMenu:




						break;
					case Credits:




						break;
					case Quit:
						Engine->Quit();

						break;
					case Restart:
						SceneSys->to_change = true;
						SceneSys->ResetLevelInGame();
						SettingsSys->paused = false;

						break;
					case Resume:
						SettingsSys->paused = !SettingsSys->paused;
						GameMenuSys->ChangeButtonVisibility();;

						break;
					}

				}

			}


		}


		if (transform && hovered == false)
		{
			transform->SetWorldScale(initial_scale);
		}

		if (my_text_render && hovered == false)
		{
			my_text_render->col = glm::vec3(initial_text_col.x, initial_text_col.y, initial_text_col.z);
			if (my_text_render->pFontInfo)
			{
				my_text_render->mTransform.mScale = glm::vec3(initial_text_scale.x, initial_text_scale.y,1);

			}

		}
	}

}
void SelectableComp::Shutdown()
{
	MenuComponent::Shutdown();
}


void SelectableComp::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	using nlohmann::json;

	j["go_to_name"] = go_to_name;
	j["my_action"] = my_action;
	j["play_sound"] = play_sound;
	j["change_color"] = change_color;


	json& Col = j["color_changed"];
	Col["Color"].push_back(color_changed.r);
	Col["Color"].push_back(color_changed.g);
	Col["Color"].push_back(color_changed.b);
	Col["Color"].push_back(color_changed.a);

}
void SelectableComp::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	using nlohmann::json;

	if (j.find("go_to_name") != j.end())
		go_to_name = j["go_to_name"].get<std::string>();
	if (j.find("my_action") != j.end())
		my_action = j["my_action"];
	if (j.find("play_sound") != j.end())
		play_sound = j["play_sound"];
	if (j.find("change_color") != j.end())
		change_color = j["change_color"];

	
	if (j.find("color_changed") != j.end())
	{
		if (j["color_changed"].find("Color") != j["color_changed"].end() && j["color_changed"]["Color"].size() >= 3)
		{
			color_changed.r = j["color_changed"]["Color"][0];
			color_changed.g = j["color_changed"]["Color"][1];
			color_changed.b = j["color_changed"]["Color"][2];
			color_changed.a = j["color_changed"]["Color"][3];
		}
	}



	index_selectable = (int)my_action;
	if (my_action == ChangeLevel)
	{
		level_index_selectable = (int)std::distance(RsrcMan->Resourcesmap[TypeLevel].begin(), RsrcMan->Resourcesmap[TypeLevel].find(go_to_name));
		active_strlvlname = RsrcMan->Resourcesmap[TypeLevel].find(go_to_name)->first;
	}
}


bool SelectableComp::Edit()
{
	bool changed = false;

	ImGui::PushID("Selectable");

	
	const char* shapes_lvl[] = { "None", "ChangeLevel", "Credits","Quit" ,"GoToMenu", "Restart", "Resume"};
	const char* label = shapes_lvl[index_selectable];
	ButtonActions values_lvl[] = { None, ChangeLevel, Credits, Quit,GoToMenu,Restart,Resume };

	ImGui::PushID("ActionSelect");
	if (ImGui::BeginCombo("Action", label, 0))
	{
		for (unsigned i = 0; i < IM_ARRAYSIZE(shapes_lvl); i++)
		{
			if (ImGui::Selectable(shapes_lvl[i], index_selectable == i))
			{
				index_selectable = i;
				my_action = values_lvl[index_selectable];
				changed = true;
			}
			if (index_selectable == i)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}
	ImGui::PopID();


	ImGui::Checkbox("Play sound", &play_sound);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::Checkbox("Change color", &change_color);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	if (change_color)
	{
		ImGui::PushID("Color picker");
		ImGui::ColorPicker4("Color Modulation", &color_changed.r);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;


		ImGui::SliderFloat("Alpha", &color_changed.a, 0.f, 1.f);
		if (ImGui::IsItemDeactivatedAfterEdit())
			changed = true;

		ImGui::PopID();
	}

	if (my_action == ChangeLevel)
	{
		auto it = RsrcMan->Resourcesmap[TypeLevel].begin();
		
		ImGui::PushID("LevelSelect");
		if (ImGui::BeginCombo("Select level", active_strlvlname.c_str(), 0))
		{
			for (unsigned i = 0; i < RsrcMan->Resourcesmap[TypeLevel].size(); it++, i++)
			{
				if (ImGui::Selectable(it->first.c_str(), level_index_selectable == i))
				{
					level_index_selectable = i;
					go_to_name = it->first.c_str();
					active_strlvlname = it->first.c_str();
					changed = true;
				}
				if (level_index_selectable == i)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}

		ImGui::PopID();

	}


	

	ImGui::PopID();


	return changed;
}