#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Graphics/Font/Font.h"
#include "../EventSystem/EventHandler.h"
#include "../MenuManager/MenuComponent.h"

enum ButtonActions
{
	None,
	ChangeLevel,
	Credits,
	Quit,
	GoToMenu,
	Restart,
	Resume
};
class SelectableComp : public MenuComponent
{
	SM_RTTI_DECL(SelectableComp, IComp);
public:

	SelectableComp();
	SelectableComp(SelectableComp& rhs);
	~SelectableComp();
	SelectableComp& operator=(const SelectableComp& rhs);
	SelectableComp* Clone();

	void OnCreate() override;
	void Initialize()override;
	void Update()override;
	void Shutdown()override;

	
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;


	bool Edit()override;


private:
	TransformComponent* transform;
	TransformComponent* cursor_transform;
	glm::vec2 initial_scale;
	glm::vec2 initial_text_scale;
	glm::vec3 initial_text_col;
	std::string go_to_name = "./data/Level/Default.lvl";
	std::string sound_name="./data/Sounds/epic_song.mp3";
	bool play_sound = false;
	bool change_color = false;
	Color color_changed = 0;
	ButtonActions my_action=None;
	unsigned index_selectable = 0;
	unsigned level_index_selectable = 0;
	std::string active_strlvlname;
	Renderable* my_rend = nullptr;
	SoundEmiter* my_sound_emitter = nullptr;
	TextRenderable* my_text_render = nullptr;
};