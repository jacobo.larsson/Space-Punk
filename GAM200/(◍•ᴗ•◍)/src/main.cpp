/**********************************************************************************/
/*
\file   main.cpp
\author David Miranda
\par    email: m.david@digipen.edu
\par    DigiPen login: m.david
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the initialize, run and shutdown functions that make the game work.
*/
/*********************************************************************************/

#define SDL_MAIN_HANDLED
#include "MainEngine/MainEngine.h"
#include "AssertSystem/AssertSystem.h"

int main()
{	
	if (!Engine->Initialize())
		return -1;
	Engine->Run();
	Engine->Shutdown();

	return 0;
}