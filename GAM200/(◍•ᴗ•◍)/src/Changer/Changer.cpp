#include <string>
#include <fstream>
#include "Changer.h"

Changer* Changer::SingleChanger = nullptr;

Changer* Changer::Get()
{
	if (Changer::SingleChanger == nullptr)
		Changer::SingleChanger = new Changer;
	return Changer::SingleChanger;
}


void Changer::AddChange(GameObject* changed, ChangeType type_of_change)
{
	auto et = changes.begin() + currentchange;
	auto mt = changes.begin() + currentchange;
	if (et != changes.end())
	{
		
		et++;
		int counter = currentchange+1;
		for (; et != changes.end(); et++)
		{
			//Erase the files
			int file_id = et->first;
			std::string to_erase = std::string("./data/temp/") + std::to_string(file_id) + '_' + std::to_string(counter) + std::string(".json");
			
			
			std::experimental::filesystem::remove(to_erase.c_str());
			counter++;
		}

	
		
		ctrlz_count--;
		for (; mt != changes.end(); mt++)
		{

			//Erase the files
			int file_id = mt->first;
			std::string to_add = std::string("./data/temp/") + std::to_string(file_id) + '_' + std::string("ctrl_z") + std::to_string(ctrlz_count) + std::string(".json");

			std::experimental::filesystem::remove(to_add.c_str());
			ctrlz_count--;
		}
		if (ctrlz_count < 0)
			ctrlz_count = 0;
		int size = (int)changes.size();
		int counter2 = currentchange+1;

		for (; counter2 != size; counter2++)
		{
			
			
			changes.pop_back();
			
		}


		std::string test = std::string("./data/temp/picked_obj_before.json");

		std::experimental::filesystem::remove(test.c_str());
		using nlohmann::json;
		json h;

		changed->ToJson(h);

		std::ofstream fp_out(test);
		if (fp_out.is_open() && fp_out.good())
		{
			fp_out << std::setw(4) << h;
			fp_out.close();
		}

		currentchange++;
	}


	else if (changed && type_of_change != SpaceAdded && type_of_change != SpaceErased && type_of_change != ObjectAdded)
	{
		unsigned int id = changed->GetUID();
		std::pair<int, ChangeType> newpair = std::make_pair(id, type_of_change);

		std::string to_add = std::string("./data/temp/") + std::to_string(id) + '_' + std::to_string(currentchange) + std::string(".json");

		std::string test = std::string("./data/temp/picked_obj_before.json");
		
		std::experimental::filesystem::remove(to_add.c_str());

		std::experimental::filesystem::copy(test.c_str(), to_add.c_str());

		std::experimental::filesystem::remove(test.c_str());
		using nlohmann::json;
		json h;

		changed->ToJson(h);

		std::ofstream fp_out(test);
		if (fp_out.is_open() && fp_out.good())
		{
			fp_out << std::setw(4) << h;
			fp_out.close();
		}

		
		changes.push_back(newpair);
		currentchange++;


	}
	else if (changed && (type_of_change == SpaceAdded || type_of_change == SpaceErased))
	{
		unsigned int id = changed->GetUID();
		std::pair<int, ChangeType> newpair = std::make_pair(id, type_of_change);

		std::string to_add = std::string("./data/temp/") + std::to_string(id) + '_' + std::to_string(currentchange) + std::string(".json");
		
		
		using nlohmann::json;
		json h;

		changed->ToJson(h);

		std::experimental::filesystem::remove(to_add.c_str());

		std::ofstream fp_outspace(to_add);
		if (fp_outspace.is_open() && fp_outspace.good())
		{
			fp_outspace << std::setw(4) << h;
			fp_outspace.close();
		}

		


		changes.push_back(newpair);
		currentchange++;
	}
	else if (type_of_change == ObjectAdded)
	{
		unsigned int id = changed->GetUID();
		std::pair<int, ChangeType> newpair = std::make_pair(id, type_of_change);




		changes.push_back(newpair);
		currentchange++;

	}

}





void Changer::Clearchanges()
{
	
		
	std::experimental::filesystem::remove_all("./data/temp");
		



	changes.clear();
	currentchange = 0;
	ctrlz_count = 0;
	std::experimental::filesystem::create_directory("./data/temp");
}
