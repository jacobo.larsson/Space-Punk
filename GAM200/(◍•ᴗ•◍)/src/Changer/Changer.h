#pragma once
#include <experimental/filesystem>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "../Factory/Factory.h"
#include "../Space/Space.h"
class GameObject;

enum ChangeType
{

	ObjectChanged,
	ObjectDestroyed,
	ObjectAdded,
	SpaceErased,
	SpaceAdded,
	Nullchange
};

/*struct URChange {
	nlohmann::json j;
	int tgt_id;
	GameObject* tgt;
	ChangeType;
};*/

class Changer : public IBase
{
	SM_RTTI_DECL(Changer, IBase);
	//Singleton(Changer) {}
	Changer() {}
	static Changer* SingleChanger;
	static Changer* Get();

public:
	bool Initialize()
	{
		if (std::experimental::filesystem::exists("./data/temp"))
			std::experimental::filesystem::remove_all("./data/temp");
		std::experimental::filesystem::create_directory("./data/temp");
		currentchange = 0;
		return true;
	}
	void Shutdown()
	{
		std::experimental::filesystem::remove_all("./data/temp");
		
	}
	void AddChange(GameObject* changed, ChangeType type_of_change);
	
	void Change();
	void Clearchanges();


	std::vector<std::pair<int, ChangeType>> changes;

	/*std::list<URChange> undoStack;
	std::list<URChange> redoStack;*/


	int currentchange;

	int ctrlz_count = 0;

};

#define ChangerSys Changer::Get()