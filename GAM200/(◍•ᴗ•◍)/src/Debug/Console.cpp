#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "Console.h"

Console* Console::SingleConsole = nullptr;

Console::Console()
{
}

Console* Console::Get()
{
	if (Console::SingleConsole == nullptr)
	{
		Console::SingleConsole = new Console;
		AddSingleton(Console::SingleConsole)
	}
	return Console::SingleConsole;
}

void Console::Shutdown()
{
	if (SettingsSys->console_to_text)
	{
		std::string to_save = "./data/Console_output.txt";

		std::ofstream fp_out(to_save);
		if (fp_out.is_open() && fp_out.good())
		{
			fp_out << std::setw(4) << j;
			fp_out.close();
		}
	}
	debug_messages.clear();
}

void Console::Update()
{
	for (auto message : debug_messages)
	{
		ImGui::Text(message.c_str());
	}
	
}



int Console::DebugPrint(const char* format, ...)
{
		va_list arg;
		int done;
		std::string a;
		char buffer[10000];
		va_start(arg, format);
		done = vsnprintf(buffer, sizeof(buffer), format, arg);
		vfprintf(stdout, format, arg);
		va_end(arg);

		std::string message(buffer);
		j.push_back(message);
		if (debug_messages.size() >= 100)
			debug_messages.erase(debug_messages.begin());
		debug_messages.push_back(message);
		added = true; 
		return done;
}
