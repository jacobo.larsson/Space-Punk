#pragma once
#include <vector>
#include <string>
#include <fstream>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"
#include "../Settings/Settings.h"

using namespace nlohmann;
class Console : public IBase
{
	SM_RTTI_DECL(Console, IBase);

	Console();
	static Console* SingleConsole;
	static Console* Get();

public:
	bool Initialize() { return true; }
	void Shutdown();
	void Update();
	int DebugPrint(const char* format, ...);




	
	int GetSize() { return (int)debug_messages.size(); };

	bool added = 0;
	json j;
private:
	std::vector<std::string> debug_messages;
};

#define ConsoleSys Console::Get()