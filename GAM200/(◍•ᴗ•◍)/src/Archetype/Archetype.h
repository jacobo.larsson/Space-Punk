#pragma once
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"

class GameObject;

class Archetype : public IBase
{
	SM_RTTI_DECL(Archetype, IBase)
public:
	Archetype();

	void Load(std::string object_name);
	void ReLoad(std::string object_name);

	GameObject* GetObject();
	GameObject* mObject;
private:
	
};