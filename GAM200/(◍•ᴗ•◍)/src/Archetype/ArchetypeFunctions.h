#pragma once
#include <glm/glm.hpp>
#include <string>
class GameObject;
class Space;

GameObject* CreateFromArchetype(const std::string & name, Space * space);
GameObject* CreateFromArchetype(const std::string & name, Space * space, const glm::vec3 & position);