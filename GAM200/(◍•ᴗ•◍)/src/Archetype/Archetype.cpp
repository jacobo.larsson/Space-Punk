#include <fstream>

#include "../Factory/Factory.h"

#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/ArchetypesEvents/ArchetypesEvents.h"

#include "../GameObject/GameObject.h"
#include "Archetype.h"

Archetype::Archetype()
{
	mObject = nullptr;
	mName = "Archetype";
}

void Archetype::Load(std::string object_name)
{
	if (mObject)
		delete mObject;
	mObject = FactorySys->Create<GameObject>();
	using nlohmann::json;

	std::ifstream fp1(object_name);
	json j;

	if (fp1.is_open() && fp1.good())
	{
		fp1 >> j;
		fp1.close();
	}

	mObject->FromJson(j);
	EventSys->unsubscribe(mObject, ArchetypeUpdated());
}
void Archetype::ReLoad(std::string object_name)
{
	using nlohmann::json;

	std::ifstream fp1(object_name);
	json j;

	if (fp1.is_open() && fp1.good())
	{
		fp1 >> j;
		fp1.close();
	}
	mObject->Shutdown();
	mObject->FromJson(j);
}

GameObject* Archetype::GetObject()
{
	return mObject;
}