#include "../ResourceManager/ResourceManager.h"
#include "../GameObject/GameObject.h"
#include "../Space/Space.h"

#include "../Transform2D/TransformComponent.h"

#include "ArchetypeFunctions.h"

/* name: CreateFromArchetype
*  param name: Name of the archetype, not the whole path to the archetype path
*  param space: The parent space of the new object.
*/
GameObject* CreateFromArchetype(const std::string& name, Space* space)
{
	// Get the whole path to the archetype based on the name of the asked archetype
	std::string filepath;
	filepath += "./data/Archetypes/";
	filepath += name;
	filepath += ".archetype";

	auto archetypeInstance = reinterpret_cast<Resource<Archetype>*>(RsrcMan->Load(filepath));

	auto newObj = archetypeInstance->get()->GetObject()->Clone();
	// Add the object to the given space which we suppose will be the parent space
	space->AddGameObject(newObj);

	// Initialize the object's components since this is supposed to happen ingame
	for (auto comp : newObj->mComponents)
		comp->Initialize();

	return newObj;
}

/* name: CreateFromArchetype
*  param name: Name of the archetype, not the whole path to the archetype path
*  param space: The parent space of the new object.
*  param position: The position the object will have if TransformComponent is found.
*/
GameObject* CreateFromArchetype(const std::string& name, Space * space, const glm::vec3& position)
{
	// Get the whole path to the archetype based on the name of the asked archetype
	std::string filepath;
	filepath += "./data/Archetypes/";
	filepath += name;
	filepath += ".archetype";

	auto archetypeInstance = reinterpret_cast<Resource<Archetype>*>(RsrcMan->GetResource(filepath));

	auto newObj = archetypeInstance->get()->GetObject()->Clone();

	auto transform = newObj->GetComp<TransformComponent>();
	if (transform != nullptr)
	{
		transform->SetWorldPosition(position);
		transform->Initialize();
		transform->Update();
	}
	// Add the object to the given space which we suppose will be the parent space
	space->AddGameObject(newObj);

	// Initialize the object's components since this is supposed to happen ingame
	for (auto comp : newObj->mComponents)
		comp->Initialize();

	newObj->to_serialize = false;
	return newObj;
}