#include "Profiler.h"
#include "../Settings/Settings.h"

ProfilerSystem* ProfilerSystem::instance = nullptr;

bool ProfilerSystem::Initialize()
{
	std::experimental::filesystem::remove("./data/Profiler.txt");
	if (SettingsSys->EnableProfiler)
	{
		FILE* fp;
		fopen_s(&fp, "./data/Profiler.txt", "wt");
		if (fp)
		{
			double total_time = 0.0;
			for (auto time : profiler_times)
			{
				total_time += time.second;

			}

			fputs("PROFILER IN INITIALIZE\n", fp);
			for (auto time : profiler_times)
			{
				double percentage = time.second / total_time * 100;
				fputs(time.first.c_str(), fp);
				double a = time.second;
				
				fprintf(fp, " %f%%\t%fs\n", percentage, a);

			}
			fclose(fp);
		}
		else
		{
			printf("Could not open the meta file\n");

		}
		
	}
	profiler_times.clear();
	profiler_times_in_game.clear();
	init_check = false;
	
	return true;
}



void ProfilerSystem::Update()
{
	if (SettingsSys->EnableProfiler)
	{
		double total_time = 0.0;

		if (SettingsSys->inEditor)
		{
			each_pass++;
			for (auto time : profiler_times)
			{
				total_time += time.second;

			}

			for (auto time : profiler_times)
			{
				double percentage = time.second / total_time * 100;

				std::pair<double, double> newpair = std::make_pair(percentage + average[time.first].first, time.second + average[time.first].second);
				average[time.first] = newpair;

			}

			for (auto it = profiler_times.begin(); it != profiler_times.end(); it++)
			{
				it->second = 0.0;

			}
		}
		else
		{
			each_pass_in_game++;
			for (auto time : profiler_times_in_game)
			{
				total_time += time.second;

			}

			for (auto time : profiler_times_in_game)
			{
				double percentage = time.second / total_time * 100;

				std::pair<double, double> newpair = std::make_pair(percentage + average_in_game[time.first].first, time.second + average_in_game[time.first].second);
				average_in_game[time.first] = newpair;

			}

			for (auto it = profiler_times_in_game.begin(); it != profiler_times_in_game.end(); it++)
			{
				it->second = 0.0;

			}


		}
	}
}

void ProfilerSystem::Shutdown()
{
	if (SettingsSys->EnableProfiler)
	{
			FILE* fp;
			fopen_s(&fp, "./data/Profiler.txt", "at");
			if (fp)
			{
				if (each_pass != 0)
				{
					fputs("PROFILER IN EDITOR\n", fp);
					for (auto time : average)
					{
						fputs(time.first.c_str(), fp);
						double a = time.second.first / each_pass;
						double b = time.second.second / each_pass;
						fprintf(fp, " %f%%\t%fs\n", a, b);

					}
				}

				if (each_pass_in_game != 0)
				{
					fputs("PROFILER IN GAME\n", fp);
					for (auto time : average_in_game)
					{
						fputs(time.first.c_str(), fp);
						double a = time.second.first / each_pass_in_game;
						double b = time.second.second / each_pass_in_game;
						fprintf(fp, " %f%%\t%fs\n", a, b);

					}
				}
				fclose(fp);
			}
			else
			{
				printf("Could not open the meta file\n");

			}


		
	}


}

void ProfilerSystem::StartTimer()
{
	start_time = std::chrono::high_resolution_clock::now();
}

void ProfilerSystem::EndTimer(const std::string& profiler_part)
{
	end_time = std::chrono::high_resolution_clock::now();
	auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
	
	if(SettingsSys->inEditor || init_check==true)
		profiler_times[profiler_part] += diff.count() * 1e-9;
	else
		profiler_times_in_game[profiler_part] += diff.count() * 1e-9;
}

void ProfilerSystem::ComputePercentages()
{
}
