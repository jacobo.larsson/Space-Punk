#pragma once
#include <chrono>
#include "../RTTI/SM_RTTI.h"

#include "../Singleton/Singleton.h"
class ProfilerSystem : public IBase
{
	SM_RTTI_DECL(ProfilerSystem, IBase);
	Make_Singleton(ProfilerSystem)
public:
	bool Initialize();
	void Update();
	void Shutdown();

	void StartTimer();

	void EndTimer(const std::string & profiler_part);
	void ComputePercentages();
	
private:
	std::chrono::time_point<std::chrono::steady_clock> start_time;
	std::chrono::time_point<std::chrono::steady_clock> end_time;
	std::map<std::string,double> profiler_times_in_game;
	std::map<std::string,double> profiler_times;
	std::map<std::string,std::pair<double,double>> average_in_game;
	std::map<std::string,std::pair<double,double>> average;
	long int each_pass = 0;
	long int each_pass_in_game = 0;
	bool init_check = true;
};
#define ProfSys ProfilerSystem::Get()