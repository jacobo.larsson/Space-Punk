#pragma once
#include <experimental/filesystem>
#include <glm/glm.hpp>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"


class EditorCamera;
class IComp;
class Settings : public IBase
{
	SM_RTTI_DECL(Settings, IBase);
	//Singleton(Changer) {}
	Settings() {}
	static Settings* SingleSettings;
public:
	static Settings* Get();

	bool Initialize();
	void Shutdown();
	void Update();
	void ToJson();
	void FromJson();
	//void InitDirectPlay();
	void ToggleFullScreen();
	void AddKey(IComp* _Comp);
	void ShortCuts();


	unsigned int current_max_id = 0;
	int mLastKey = 0;
	bool fast_load = false;
	bool paused = false;
	bool fullscreen = false;
	bool debug_draw = false;
	bool console_to_text = 0;
	bool inEditor = true;
	bool mStartInGame = false;
	bool EnableProfiler = true;
	bool EnableHotReload = true;

	// Render Manager
	bool WireframeMode = false;
	bool EditorViewport = false;
	// Space Partitioning
	bool SpacePartitioning = false;
	bool DrawPartitions = false;
	bool DrawActivePartitions = false;
	bool threadedCollsions = false;
	std::string initial_level = "./data/Levels/Default.lvl";
	//EditorRelated
	bool VisualGrid = false;
	int GridSize = 100;
	float camera_move_speed = 5.0f;
	float camera_rotate_speed = 0.03f;

	glm::vec2 current_res = glm::vec2{ 1280, 720};
	std::map<std::string, unsigned> mCompKeys;
};

#define SettingsSys Settings::Get()