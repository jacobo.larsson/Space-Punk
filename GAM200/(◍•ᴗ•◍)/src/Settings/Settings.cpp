#include <string>
#include <fstream>
#include "../RTTI/IComp.h"
#include "../Input/Input.h"
#include "../Graphics/Window/Window.h"
#include "../Graphics/Camera/Camera.h"
#include "../Factory/Factory.h"
#include "../Space/Space.h"
#include "../MenuManager/MenuManager.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../MainEngine/MainEngine.h"
#include "../Scene/Scene.h"
#include "../Editor/Editor.h"
#include "Settings.h"

Settings* Settings::SingleSettings = nullptr;

Settings* Settings::Get()
{
	if (Settings::SingleSettings == nullptr)
		Settings::SingleSettings = new Settings;
	return Settings::SingleSettings;
}

bool Settings::Initialize()
{
	FromJson();
	return true;
}

void Settings::Shutdown()
{
	ToJson();
}

void Settings::Update()
{
	ShortCuts();
}

void Settings::ToJson()
{
	json j;
	std::string to_j = "./data/Settings.txt";
	j["fullscreen"] = fullscreen;
	//j["paused"] = paused;
	j["console_to_text"] = console_to_text;
	j["current_res.x"] = current_res.x;
	j["current_res.y"] = current_res.y;
	j["debug_draw"] = debug_draw;
	j["fast_load"] = fast_load;
	j["inEditor"] = inEditor;
	j["SpacePartitioning"] = SpacePartitioning;
	j["DrawPartitions"] = DrawPartitions;
	j["DrawActivePartitions"] = DrawActivePartitions;
	j["EditorViewport"] = EditorViewport;
	j["EnableHotReload"] = EnableHotReload;
	j["threadedCollsions"] = threadedCollsions;
	j["VisualGrid"] = VisualGrid;
	j["GridSize"] = GridSize;
	j["levelname"] = SceneSys->GetLevelName();
	j["camera_move_speed"] = camera_move_speed;
	j["camera_rotate_speed"] = camera_rotate_speed;
	json& _Comps = j["ComponentsKeys"];	
	_Comps["mLastKey"] = mLastKey;
	_Comps["count"] = mCompKeys.size();
	for (auto& _Entry : mCompKeys)
	{
		_Comps["CompName"].push_back(_Entry.first.c_str());
		_Comps["Key"].push_back(_Entry.second);
	}
	std::ofstream fp_out(to_j);
	if (fp_out.is_open() && fp_out.good())
	{
		fp_out << std::setw(4) << j;
		fp_out.close();
	}

	
}

void Settings::FromJson()
{
	std::string from_j = "./data/Settings.txt";
	json j;
	std::ifstream fp_in(from_j);
	if (fp_in.is_open() && fp_in.good())
	{
		fp_in >> j;
		fp_in.close();
	}

	if (j.find("fullscreen") != j.end())
		fullscreen = j["fullscreen"];
	//if (j.find("paused") != j.end())
	//	paused = j["paused"];
	if (j.find("console_to_text") != j.end())
		console_to_text = j["console_to_text"];
	/*if (j.find("current_max_id") != j.end())
		current_max_id = j["current_max_id"];*/
	if (j.find("current_res.x") != j.end())
		current_res.x = j["current_res.x"];
	if (j.find("current_res.y") != j.end())
		current_res.y = j["current_res.y"];

	if (j.find("debug_draw") != j.end())
		debug_draw = j["debug_draw"];

	if (j.find("fast_load") != j.end())
		fast_load = j["fast_load"];


	if (j.find("inEditor") != j.end())
		inEditor = j["inEditor"];

	// Editor Related
	if (j.find("EditorViewport") != j.end())
		EditorViewport = j["EditorViewport"];
	if (j.find("VisualGrid") != j.end())
		VisualGrid = j["VisualGrid"];
	if (j.find("GridSize") != j.end())
		GridSize = j["GridSize"];

	// Space Partitioning
	if (j.find("SpacePartitioning") != j.end())
		SpacePartitioning = j["SpacePartitioning"];
	if (j.find("DrawPartitions") != j.end())
		DrawPartitions = j["DrawPartitions"];
	if (j.find("DrawActivePartitions") != j.end())
		DrawActivePartitions = j["DrawActivePartitions"];
	if (j.find("EnableHotReload") != j.end())
		EnableHotReload = j["EnableHotReload"];
	if (j.find("camera_move_speed") != j.end())
		camera_move_speed = j["camera_move_speed"];
	if (j.find("camera_rotate_speed") != j.end())
		camera_rotate_speed = j["camera_rotate_speed"];

	if (j.find("threadedCollsions") != j.end())
		threadedCollsions = j["threadedCollsions"];

	if (j.find("levelname") != j.end())
		initial_level = j["levelname"].get<std::string>();


	if (inEditor)
		mStartInGame = false;
	else
		mStartInGame = true;
	json& _Comps = j["ComponentsKeys"];

	if (_Comps.find("mLastKey") != _Comps.end())
	{
		mLastKey = _Comps["mLastKey"];
		unsigned count = _Comps["count"];

		for (unsigned i = 0; i < count; i++)
		{
			mCompKeys[_Comps["CompName"][i].get<std::string>()] = _Comps["Key"][i];
		}
	}

	if (fullscreen)
	{
		ToggleFullScreen();
	}

	if (fullscreen == false)
	{
		SDL_SetWindowSize(WindowSys->GetWindow(), current_res.x, current_res.y);	
	}
	else
	{
		RndMngr->editor_camera->mViewRectangle = RndMngr->editor_camera->normalizeRec * current_res;
	}
}

//void Settings::InitDirectPlay()
//{
//	if (mStartInGame==true)
//	{
//		SceneSys->InitializeSpacesComponents();
//		Engine->CloseEditor();
//		EditorSys->SetActiveSpace(nullptr);
//		EditorSys->UnPickObject();
//	}
//}

void Settings::ToggleFullScreen()
{
	WindowSys->ToggleFullScreen(fullscreen);
}

void Settings::AddKey(IComp * _Comp)
{
	std::string _TestName1 = _Comp->GetType().GetName();
	std::string _TestName2 = _Comp->TYPE().GetName();
	std::string _TestName3 = _Comp->GetName();
	auto _Duplicated = mCompKeys.find(_Comp->GetName());

	// If it is not duplicated
	if (_Duplicated == mCompKeys.end())
	{
		mCompKeys[_Comp->GetName()] = mLastKey++;
		_Comp->SetOrderKey(mCompKeys[_Comp->GetName()]);
	}
	else
		_Comp->SetOrderKey(mCompKeys[_Comp->GetName()]);
}

void Settings::ShortCuts()
{
	//Go into editor
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::F8))
	{
		Engine->OpenEditor();
		SceneSys->Reset();
		EditorSys->SetActiveSpace(nullptr);
		EditorSys->SetPickedObject(nullptr);
		mStartInGame = false;
	}
	//Toggle collisions with multithreading
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::I))
		threadedCollsions = !threadedCollsions;

	//General editor shortcuts
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->mouse_is_down(mouse_buttons::Middle_click))
		EditorSys->MoveCamera();
	if (InputSys->mouse_is_down(mouse_buttons::Middle_click))
		EditorSys->UnPickObject();
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::C))
		EditorSys->CopyObject();
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::V))
		EditorSys->PasteObject();
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::Z))
		EditorSys->Undo();
	if (InputSys->key_is_triggered(keys::LeftControl) && InputSys->key_is_down(keys::Y))
		EditorSys->Redo();

	//EDITOR CAMERA
	{
		//Move
		if (InputSys->key_is_triggered(keys::Up) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mPosition.y += camera_move_speed;

		if (InputSys->key_is_triggered(keys::Left) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mPosition.x -= camera_move_speed;

		if (InputSys->key_is_triggered(keys::Down) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mPosition.y -= camera_move_speed;

		if (InputSys->key_is_triggered(keys::Right) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mPosition.x += camera_move_speed;

		//Rotate
		if (InputSys->key_is_triggered(keys::K) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mOrientation -= camera_rotate_speed;

		if (InputSys->key_is_triggered(keys::L) && InputSys->key_is_triggered(keys::LeftControl))
			RndMngr->editor_camera->mTransform.mOrientation += camera_rotate_speed;

		//Reset
		if (InputSys->key_is_triggered(keys::R) && InputSys->key_is_triggered(keys::LeftControl))
		{
			RndMngr->editor_camera->DefaultValue();
		}

		//Zoom in 
		if (InputSys->is_mouse_scroll_up() && InputSys->key_is_triggered(keys::LeftControl))
		{
			// viewport cannot be less than 480:270 
			if ((RndMngr->editor_camera->mViewRectangle.x) - 50.0f >= 480.0f)
			{
				RndMngr->editor_camera->mViewRectangle.x -= 50.0f;
				RndMngr->editor_camera->mViewRectangle.y = RndMngr->editor_camera->mViewRectangle.x * 9.0f / 16.0f;
			}
			else
				RndMngr->editor_camera->mViewRectangle.x = 480.0f;

		}
		//Zoom out 
		else if (InputSys->is_mouse_scroll_down() && InputSys->key_is_triggered(keys::LeftControl))
		{
			// viewport cannot be less than 3840:2160 
			if ((RndMngr->editor_camera->mViewRectangle.x) + 50 <= 38400.0f)
			{
				RndMngr->editor_camera->mViewRectangle.x += 50.0;
				RndMngr->editor_camera->mViewRectangle.y = RndMngr->editor_camera->mViewRectangle.x * 9.0f / 16.0f;
			}
			else
				RndMngr->editor_camera->mViewRectangle.x = 3840.0f;
		}

		//Go to selected object
		if (InputSys->key_is_triggered(keys::Z) && InputSys->key_is_triggered(keys::LeftShift))
		{
			RndMngr->editor_camera->mTransform.mPosition = EditorSys->GetPickedObject()->GetComp<TransformComponent>()->GetWorldPosition();
			RndMngr->editor_camera->mTransform.mPosition.z = 20;
		}
	}

	if (InputSys->key_is_down(keys::Escape) && !inEditor)
	{
		paused = !paused;
		GameMenuSys->ChangeButtonVisibility();
	}
}
