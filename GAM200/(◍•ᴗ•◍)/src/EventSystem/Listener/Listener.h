#pragma once

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"

class Event;

class Listener
{
public:
    virtual void handle_event(const Event&) = 0;
    virtual ~Listener() {}
};