/*! 
******************************************************************************
    \file    event.cc
    \author  David Miranda
    \par     DP email: m.david@digipen.edu
    \par     Course: CS225
    \par     Assignment 2
    \date    10/14/2019

    \brief  This file contains all the definitions for the Event, 
            HandlerFunction, MemberFuctionHandler and EventHandler class.
*******************************************************************************/
#include "Event/Event.h"
#include "EventHandler.h"
        
void EventHandler::handle(const Event& event)
{
   auto it = handlers.find(typeid(event).name());
    if(it!=handlers.end())
        it->second->handle(event);
}

EventHandler::~EventHandler()
{
    for(auto it = handlers.begin() ;it!=handlers.end();++it)
         delete it->second;
    handlers.clear();
}
