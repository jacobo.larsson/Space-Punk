#pragma once
#include <string>
#include "../../Event/Event.h"

class Event;
class Rigidbody;

class CollisionGroupUpdated :public Event
{
public:
	CollisionGroupUpdated();
	CollisionGroupUpdated(int to_change);
	~CollisionGroupUpdated();
	int GetCollisionType() const { return change_type; }
private:
	int change_type;

};