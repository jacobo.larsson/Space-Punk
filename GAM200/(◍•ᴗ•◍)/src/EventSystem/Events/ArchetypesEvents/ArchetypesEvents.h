#pragma once
#include <string>
#include "../../Event/Event.h"

class Event;
class GameObject;

class ArchetypeUpdated :public Event
{
public:
	ArchetypeUpdated();
	ArchetypeUpdated(GameObject* archetype);
	~ArchetypeUpdated();

	const std::string GetName()const;
	const GameObject* GetObject()const;
private:
	std::string mArchetypeName;
	GameObject* mArchetype;

};