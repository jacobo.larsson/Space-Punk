#include "../../../GameObject/GameObject.h"
#include "ArchetypesEvents.h"

ArchetypeUpdated::ArchetypeUpdated():mArchetype(nullptr)
{
}

ArchetypeUpdated::ArchetypeUpdated(GameObject* archetype):mArchetype(archetype)
{
	mArchetypeName = mArchetype->GetArchetypeName();
}


ArchetypeUpdated::~ArchetypeUpdated()
{
}

const std::string ArchetypeUpdated::GetName()const
{
	return mArchetypeName;
}

const GameObject* ArchetypeUpdated::GetObject() const
{
	return mArchetype;
}
