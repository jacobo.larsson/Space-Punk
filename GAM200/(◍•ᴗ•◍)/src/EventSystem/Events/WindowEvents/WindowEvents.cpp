#include "WindowEvents.h"

FullScreenToggled::FullScreenToggled(bool newState):mState(newState)
{}

FullScreenToggled::~FullScreenToggled()
{}

bool FullScreenToggled::GetState()const
{
	return mState;
}

ResolutionChanged::ResolutionChanged(int _width, int _height):width{_width},height{_height}
{
}

ResolutionChanged::~ResolutionChanged()
{
}

glm::ivec2 ResolutionChanged::GetResolution() const
{
	return glm::ivec2(width, height);
}
