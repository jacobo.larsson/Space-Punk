#pragma once
#include <glm/glm.hpp>
#include "../../Event/Event.h"

class Event;

class FullScreenToggled : public Event
{
public:
	FullScreenToggled(bool newState = false);
	~FullScreenToggled();
	bool GetState()const;
private:
	bool mState;
};

class ResolutionChanged : public Event
{
public:
	ResolutionChanged(int _width = 1280, int _height = 720 );
	~ResolutionChanged();
	glm::ivec2 GetResolution()const;
private:
	int width;
	int height;
};