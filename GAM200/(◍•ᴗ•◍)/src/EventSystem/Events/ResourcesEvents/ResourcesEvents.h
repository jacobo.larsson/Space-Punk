#pragma once
#include <string>
#include "../../Event/Event.h"

class Event;
class GameObject;

class DumpResource :public Event
{
public:
	DumpResource();
	DumpResource(GameObject* checker);
	~DumpResource();

	const std::vector<std::string> GetName()const;
private:
	std::vector<std::string> mResourcesName;

};
