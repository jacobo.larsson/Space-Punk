#pragma once

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"

class Event : public IBase
{
    SM_RTTI_DECL(Event, IBase);
public:
    virtual ~Event() {}
};