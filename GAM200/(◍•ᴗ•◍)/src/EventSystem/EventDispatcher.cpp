/*! 
******************************************************************************
    \file    event_dispatcher.cc
    \author  David Miranda
    \par     DP email: m.david@digipen.edu
    \par     Course: CS225
    \par     Assignment 2
    \date    10/14/2019

    \brief  This file contains all the definitions for the Listener and
            EventDispatcher.
*******************************************************************************/
#include "Listener/Listener.h"
#include "Event/Event.h"

#include "EventDispatcher.h"

EventDispatcher* EventDispatcher::instance = nullptr;

void EventDispatcher::subscribe(Listener * obj, const Event& event)
{
    // add the object to the registered event 
    if (obj != nullptr)
    {
        // Check for duplicates
        auto duplicated = std::find( listeners[typeid(event).name()].begin(), listeners[typeid(event).name()].end(), obj);
        if(duplicated == listeners[typeid(event).name()].end())
            listeners[typeid(event).name()].push_back(obj);

    }
}
void EventDispatcher::unsubscribe(Listener * obj,const  Event& event)
{
    auto _event = listeners.find(typeid(event).name());

    // check if the event has been previously registered.
    // If not, remove the obj
    if (_event != listeners.end())
    {
        auto listener = std::find(_event->second.begin(), _event->second.end(), obj);
        if(listener != _event->second.end())
            _event->second.erase(listener);
    }
}
void EventDispatcher::clear()
{
    std::for_each(listeners.begin(), listeners.end(), 
    [&](auto& it)
    {
        it.second.clear();
    });
    //clean the map
    listeners.clear();
}
void EventDispatcher::trigger_event(const Event& _event)
{
   auto it = listeners.find(typeid(_event).name());
    
    // sanity check
    // if the event is registered, pass the given event
    // to all its subscribers
    if(it!=listeners.end())        
        for( auto ot = it->second.begin()  ; ot!= it->second.end(); ++ot)
            (*ot)->handle_event(_event);            
}
