/*! 
******************************************************************************
    \file    event_dispatcher.hh
    \author  David Miranda
    \par     DP email: m.david@digipen.edu
    \par     Course: CS225
    \par     Assignment 2
    \date    10/14/2019

    \brief  This file contains all the declarations for the Listener and
            EventDispatcher.
*******************************************************************************/
#pragma once
#include <vector>
#include <sstream>
#include <algorithm>

#include "../RTTI/IBase.h"
#include "../RTTI/IComp.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"

class Listener;
class Event;

class EventDispatcher:public IBase
{
    SM_RTTI_DECL(EventDispatcher, IBase);
    Make_Singleton(EventDispatcher)

public:
    void subscribe(Listener * obj, const Event& event);

    void unsubscribe(Listener * obj, const Event& event);

    void clear();
    void trigger_event(const Event& event);

    protected:
    EventDispatcher(const EventDispatcher&){}

    private:
    std::map<std::string, std::vector<Listener* > > listeners;
};


#define EventSys EventDispatcher::Get()

