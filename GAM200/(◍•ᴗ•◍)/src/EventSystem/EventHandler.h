/*! 
******************************************************************************
    \file    event.hh
    \author  David Miranda
    \par     DP email: m.david@digipen.edu
    \par     Course: CS225
    \par     Assignment 2
    \date    10/14/2019

    \brief  This file contains all the declarations for the Event, 
            HandlerFunction, MemberFuctionHandler and EventHandler class.
*******************************************************************************/
#pragma once
#include <map>
#include <string>
#include <typeinfo>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"

class Event;

class HandlerFunction : public IBase
{
    SM_RTTI_DECL(HandlerFunction, IBase);

    public:

    template<typename T>
    void handle(const T & event){call(event);}
    virtual void call(const Event&)= 0;
    virtual ~HandlerFunction(){}
};

// T1 obj T2 event
template<typename T1, typename T2>
class MemberFunctionHandler:public HandlerFunction
{
    SM_RTTI_DECL(MemberFunctionHandler, HandlerFunction);
    public:
    typedef void (T1::*MemberFP)(const T2&);

    MemberFunctionHandler(T1 * obj,MemberFP fp):mObj(obj),mFunction(fp){}

    void call(const Event& event ){(mObj->*mFunction)(static_cast<const T2&>(event));}

    private:
    T1 * mObj;
    MemberFP mFunction;
};

class EventHandler: public IBase
{
    SM_RTTI_DECL(EventHandler, IBase);
    public:    

    template<typename T1, typename T2>
    void register_handler(T1& obj,void (T1::*fp)(const T2&))
    {
        // find if there is another instance of the handler to register
        auto it = handlers.find(typeid(T2).name());

        // if there is one, return
        if(it!=handlers.end()) 
            return;

        // create and store the new handler
        handlers[typeid(T2).name()] = new MemberFunctionHandler<T1,T2>(&obj,fp);
    }

    void handle(const Event& event);

    ~EventHandler();

    private:
    std::map<std::string, HandlerFunction*> handlers;
};

#define EVENTHANDLER_DECL \
protected:\
    EventHandler mEventHandler;\
public:\
    void handle_event(const Event& _event)override\
{\
        mEventHandler.handle(_event); \
}
