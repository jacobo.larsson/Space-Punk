#pragma once
#include <vector>
class GameObject;
class Space;
struct Point
{
	Point();
	Point(float _x, float _y);
	Point(int _x, int _y);
	int x;
	int y;
};

struct TreeNode
{
	// Store the objects in a map based on the UID
	// to avoid duplicates
	std::map<unsigned, GameObject*> mObjects;
};
enum DrawDirection
{
	DrawTop,
	DrawBot,
	DrawLeft,
	DrawRight
};

class Quad
{

public:
	Quad(); 
	Quad(Point _BotLeft, Point _TopRight);

	// We will insert based on the objects position
	void Insert(GameObject* _Object, bool _Displaced = false);
	TreeNode* Search(Point _Point);
	Quad* SearchQuad(Point _Point);
	void RemoveObject(GameObject* _Object);
	void GetActiveObjects(Point _BotLeft, Point _TopRight, std::vector<GameObject*> & _OutObjects);
	void GetActiveObjects(Point _BotLeft, Point _TopRight, std::map<unsigned, GameObject*>& _OutObjects, std::vector<Quad*> & _OutQuads);

	void Reset(DrawDirection _Direction);
	void Destroy(DrawDirection _Direction);

	bool IsEnabled();
	void SetEnabled(bool _State);

	bool DrawQuad(Space * _Space, DrawDirection _Direction);

public:

	bool InQuad(Point _Point);
	void DrawConnections(Space* _Space);
	void DrawSelected(Space* _Space);
	void CheckAdjacency(GameObject* _Object, bool _Displaced);

//	 Biggest points of this quad, based on our coordinate system
//		|------------| This
//		|			 |
//		|			 |
//		|			 |
//This	|------------|
	Point mTopRight;
	Point mBotLeft;

	Quad* mRoot;

	TreeNode* mNode;

	Quad * mTopTree;
	Quad * mBotTree;
	Quad * mLeftTree;
	Quad * mRightTree;

	bool mEnabled = false;
	int mTotalObjects = 0;
	unsigned TotalQuads = 0;
	unsigned ClearedThisFrame = 0;
	unsigned DrawnThisFrame = 0;
	int mWidth = 500;
	int mHeight = 500;
};
