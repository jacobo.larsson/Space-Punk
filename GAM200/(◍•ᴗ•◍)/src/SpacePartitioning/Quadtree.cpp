#include <glm/glm.hpp>

#include "../Space/Space.h"
#include "../Scene/Scene.h"
#include "../Settings/Settings.h"
#include "../Graphics/Color/Color.h"

#include "../GameObject/GameObject.h"
#include "../Space/Space.h"
#include "Quadtree.h"


Point::Point() :x{ 0 }, y{ 0 }
{
}

Point::Point(float _x, float _y) : x( _x ), y( _y )
{
}

Point::Point(int _x, int _y) : x{ _x }, y{ _y }
{
}


Quad::Quad() : mBotLeft{ 0,0 }, mTopRight{ 0, 0 }, mNode{ nullptr }, mTopTree{ nullptr }, mBotTree{ nullptr }, mRightTree{ nullptr }, mLeftTree{ nullptr }
{
}

Quad::Quad(Point _BotLeft, Point _TopRight) :
	mBotLeft{ _BotLeft }, mTopRight{ _TopRight }, mNode{ nullptr }, mTopTree{ nullptr }, mBotTree{ nullptr }, mRightTree{ nullptr }, mLeftTree{ nullptr }
{
	mWidth = _TopRight.x - _BotLeft.x;
	mHeight = _TopRight.y - _BotLeft.y;
}

void Quad::Insert(GameObject* _Object, bool _Displaced)
{
    if (_Object == nullptr)
        return;
	if (!_Displaced)
	{
		// The object must be inserted in the right quad
		if (_Object->mPos.x > mTopRight.x)
		{
			if (mRightTree == nullptr)
			{
				// Create a new quad on the right knowing that 
				// our maximum x point will be its minimum and its maximum will be our maximum
				// plus the difference between our maximum and minimum
				// Maximum and minimum y will be the same as this
				mRightTree = new Quad(Point(mTopRight.x, mBotLeft.y),
					Point(mTopRight.x + (mTopRight.x - mBotLeft.x), mTopRight.y));

				// Link it to the quad
				mRoot->TotalQuads++;
				mRightTree->mRoot = mRoot;
				mRightTree->mLeftTree = this;

				// The quad on our right could have a quad on top, therefore we must link that quad bot pointer
				// since it did not have it
				mRightTree->mTopTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mTopRight.y + 1 });
				if (mRightTree->mTopTree && mRightTree->mTopTree->mBotTree == nullptr)
				{
					mRightTree->mTopTree->mBotTree = mRightTree;
				}
				mRightTree->mBotTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mBotLeft.y - 1 });
				if (mRightTree->mBotTree && mRightTree->mBotTree->mTopTree == nullptr)
				{
					mRightTree->mBotTree->mTopTree = mRightTree;
				}
				mRightTree->mRightTree = mRoot->SearchQuad(Point{ mRightTree->mTopRight.x + 1, mTopRight.y - 1 });
				if (mRightTree->mRightTree && mRightTree->mRightTree->mLeftTree == nullptr)
				{
					mRightTree->mRightTree->mLeftTree = mRightTree;
				}
			}
			mRightTree->Insert(_Object);
		}
		// The object must be inserted in the left quad
		else if (_Object->mPos.x < mBotLeft.x)
		{
			if (mLeftTree == nullptr)
			{
				mLeftTree = new Quad(Point{ mBotLeft.x - (mTopRight.x - mBotLeft.x), mBotLeft.y },
					Point{ mBotLeft.x, mTopRight.y });

				mRoot->TotalQuads++;
				mLeftTree->mRoot = mRoot;
				mLeftTree->mRightTree = this;
				mLeftTree->mTopTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mTopRight.y + 1 });
				if (mLeftTree->mTopTree && mLeftTree->mTopTree->mBotTree == nullptr)
				{
					mLeftTree->mTopTree->mBotTree = mLeftTree;
				}
				mLeftTree->mBotTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mBotLeft.y - 1 });
				if (mLeftTree->mBotTree && mLeftTree->mBotTree->mTopTree == nullptr)
				{
					mLeftTree->mBotTree->mTopTree = mLeftTree;
				}
				mLeftTree->mLeftTree = mRoot->SearchQuad(Point{ mLeftTree->mBotLeft.x - 1, mTopRight.y - 1 });
				if (mLeftTree->mLeftTree && mLeftTree->mLeftTree->mRightTree == nullptr)
				{
					mLeftTree->mLeftTree->mRightTree = mLeftTree;
				}
			}
			mLeftTree->Insert(_Object);
		}
		// The object must be inserted in the top quad
		else if (_Object->mPos.y > mTopRight.y)
		{
			if (mTopTree == nullptr)
			{
				mTopTree = new Quad(Point{ mBotLeft.x, mTopRight.y },
					Point{ mTopRight.x, mTopRight.y + (mTopRight.y - mBotLeft.y) });

				mRoot->TotalQuads++;
				mTopTree->mRoot = mRoot;
				mTopTree->mBotTree = this;

				mTopTree->mRightTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mTopRight.y + 1 });
				if (mTopTree->mRightTree && mTopTree->mRightTree->mLeftTree == nullptr)
				{
					mTopTree->mRightTree->mLeftTree = mTopTree;
				}
				mTopTree->mLeftTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mTopRight.y + 1 });
				if (mTopTree->mLeftTree && mTopTree->mLeftTree->mRightTree == nullptr)
				{
					mTopTree->mLeftTree->mRightTree = mTopTree;
				}
				mTopTree->mTopTree = mRoot->SearchQuad(Point{ mTopRight.x - 1, mTopTree->mTopRight.y + 1 });
				if (mTopTree->mTopTree && mTopTree->mTopTree->mBotTree == nullptr)
				{
					mTopTree->mTopTree->mBotTree = mTopTree;
				}
			}
			mTopTree->Insert(_Object);
		}
		// The object must be inserted in the bottom quad
		else if (_Object->mPos.y < mBotLeft.y)
		{
			if (mBotTree == nullptr)
			{
				mBotTree = new Quad(Point{ mBotLeft.x, mBotLeft.y - (mTopRight.y - mBotLeft.y) },
					Point{ mTopRight.x, mBotLeft.y });

				mRoot->TotalQuads++;
				mBotTree->mRoot = mRoot;
				mBotTree->mTopTree = this;
				mBotTree->mRightTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mBotLeft.y - 1 });
				if (mBotTree->mRightTree && mBotTree->mRightTree->mLeftTree == nullptr)
				{
					mBotTree->mRightTree->mLeftTree = mBotTree;
				}
				mBotTree->mLeftTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mBotLeft.y - 1 });
				if (mBotTree->mLeftTree && mBotTree->mLeftTree->mRightTree == nullptr)
				{
					mBotTree->mLeftTree->mRightTree = mBotTree;
				}
				mBotTree->mBotTree = mRoot->SearchQuad(Point{ mTopRight.x - 1, mBotTree->mBotLeft.y - 1 });
				if (mBotTree->mBotTree && mBotTree->mBotTree->mBotTree == nullptr)
				{
					mBotTree->mBotTree->mTopTree = mBotTree;
				}
			}
			mBotTree->Insert(_Object);
		}
		// The object must be inserted in this quad
		else
		{
			if (mNode == nullptr)
				mNode = new TreeNode();
			mNode->mObjects[_Object->GetUID()] = _Object;
			// Check if the object must be inserted in the adjacent partitions as well
			CheckAdjacency(_Object, false);
			_Object->mQuad.push_back(this);
		}
		mTotalObjects++;
	}
	else
	{
		if (_Object->mDisplacedPos.x > mTopRight.x)
		{
			if (mRightTree == nullptr)
			{
				// Create a new quad on the right knowing that 
				// our maximum x point will be its minimum and its maximum will be our maximum
				// plus the difference between our maximum and minimum
				// Maximum and minimum y will be the same as this
				mRightTree = new Quad(Point(mTopRight.x, mBotLeft.y),
					Point(mTopRight.x + (mTopRight.x - mBotLeft.x), mTopRight.y));

				// Link it to the quad
				mRoot->TotalQuads++;
				mRightTree->mRoot = mRoot;
				mRightTree->mLeftTree = this;

				// The quad on our right could have a quad on top, therefore we must link that quad bot pointer
				// since it did not have it
				mRightTree->mTopTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mTopRight.y + 1 });
				if (mRightTree->mTopTree && mRightTree->mTopTree->mBotTree == nullptr)
				{
					mRightTree->mTopTree->mBotTree = mRightTree;
				}
				mRightTree->mBotTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mBotLeft.y - 1 });
				if (mRightTree->mBotTree && mRightTree->mBotTree->mTopTree == nullptr)
				{
					mRightTree->mBotTree->mTopTree = mRightTree;
				}
				mRightTree->mRightTree = mRoot->SearchQuad(Point{ mRightTree->mTopRight.x + 1, mTopRight.y - 1 });
				if (mRightTree->mRightTree && mRightTree->mRightTree->mLeftTree == nullptr)
				{
					mRightTree->mRightTree->mLeftTree = mRightTree;
				}
			}
			mRightTree->Insert(_Object, _Displaced);
		}
		// The object must be inserted in the left quad
		else if (_Object->mDisplacedPos.x < mBotLeft.x)
		{
			if (mLeftTree == nullptr)
			{
				mLeftTree = new Quad(Point{ mBotLeft.x - (mTopRight.x - mBotLeft.x), mBotLeft.y },
					Point{ mBotLeft.x, mTopRight.y });

				mRoot->TotalQuads++;
				mLeftTree->mRoot = mRoot;
				mLeftTree->mRightTree = this;
				mLeftTree->mTopTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mTopRight.y + 1 });
				if (mLeftTree->mTopTree && mLeftTree->mTopTree->mBotTree == nullptr)
				{
					mLeftTree->mTopTree->mBotTree = mLeftTree;
				}
				mLeftTree->mBotTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mBotLeft.y - 1 });
				if (mLeftTree->mBotTree && mLeftTree->mBotTree->mTopTree == nullptr)
				{
					mLeftTree->mBotTree->mTopTree = mLeftTree;
				}
				mLeftTree->mLeftTree = mRoot->SearchQuad(Point{ mLeftTree->mBotLeft.x - 1, mTopRight.y - 1 });
				if (mLeftTree->mLeftTree && mLeftTree->mLeftTree->mRightTree == nullptr)
				{
					mLeftTree->mLeftTree->mRightTree = mLeftTree;
				}
			}
			mLeftTree->Insert(_Object, _Displaced);
		}
		// The object must be inserted in the top quad
		else if (_Object->mDisplacedPos.y > mTopRight.y)
		{
			if (mTopTree == nullptr)
			{
				mTopTree = new Quad(Point{ mBotLeft.x, mTopRight.y },
					Point{ mTopRight.x, mTopRight.y + (mTopRight.y - mBotLeft.y) });

				mRoot->TotalQuads++;
				mTopTree->mRoot = mRoot;
				mTopTree->mBotTree = this;

				mTopTree->mRightTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mTopRight.y + 1 });
				if (mTopTree->mRightTree && mTopTree->mRightTree->mLeftTree == nullptr)
				{
					mTopTree->mRightTree->mLeftTree = mTopTree;
				}
				mTopTree->mLeftTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mTopRight.y + 1 });
				if (mTopTree->mLeftTree && mTopTree->mLeftTree->mRightTree == nullptr)
				{
					mTopTree->mLeftTree->mRightTree = mTopTree;
				}
				mTopTree->mTopTree = mRoot->SearchQuad(Point{ mTopRight.x - 1, mTopTree->mTopRight.y + 1 });
				if (mTopTree->mTopTree && mTopTree->mTopTree->mBotTree == nullptr)
				{
					mTopTree->mTopTree->mBotTree = mTopTree;
				}
			}
			mTopTree->Insert(_Object, _Displaced);
		}
		// The object must be inserted in the bottom quad
		else if (_Object->mDisplacedPos.y < mBotLeft.y)
		{
			if (mBotTree == nullptr)
			{
				mBotTree = new Quad(Point{ mBotLeft.x, mBotLeft.y - (mTopRight.y - mBotLeft.y) },
					Point{ mTopRight.x, mBotLeft.y });

				mRoot->TotalQuads++;
				mBotTree->mRoot = mRoot;
				mBotTree->mTopTree = this;
				mBotTree->mRightTree = mRoot->SearchQuad(Point{ mTopRight.x + 1, mBotLeft.y - 1 });
				if (mBotTree->mRightTree && mBotTree->mRightTree->mLeftTree == nullptr)
				{
					mBotTree->mRightTree->mLeftTree = mBotTree;
				}
				mBotTree->mLeftTree = mRoot->SearchQuad(Point{ mBotLeft.x - 1, mBotLeft.y - 1 });
				if (mBotTree->mLeftTree && mBotTree->mLeftTree->mRightTree == nullptr)
				{
					mBotTree->mLeftTree->mRightTree = mBotTree;
				}
				mBotTree->mBotTree = mRoot->SearchQuad(Point{ mTopRight.x - 1, mBotTree->mBotLeft.y - 1 });
				if (mBotTree->mBotTree && mBotTree->mBotTree->mBotTree == nullptr)
				{
					mBotTree->mBotTree->mTopTree = mBotTree;
				}
			}
			mBotTree->Insert(_Object, _Displaced);
		}
		// The object must be inserted in this quad
		else
		{
			if (mNode == nullptr)
				mNode = new TreeNode();
			mNode->mObjects[_Object->GetUID()] = _Object;
			// Check if the object must be inserted in the adjacent partitions as well
			CheckAdjacency(_Object, true);
			_Object->mQuad.push_back(this);
		}
		mTotalObjects++;

	}
}

TreeNode* Quad::Search(Point _Point)
{
	// The object must be inserted in the right quad
	if (_Point.x > mTopRight.x)
	{ 
		if (mRightTree != nullptr)
			return mRightTree->Search(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the left quad
	else if (_Point.x < mBotLeft.x)
	{
		if (mLeftTree != nullptr)
			return mLeftTree->Search(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the top quad
	else if (_Point.y > mTopRight.y)
	{
		if (mTopTree != nullptr)
			return mTopTree->Search(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the bottom quad
	else if (_Point.y < mBotLeft.y)
	{
		if (mBotTree != nullptr)
			return mBotTree->Search(_Point);
		else
			return nullptr;
	}
	else
	{
		return mNode;
	}
}

Quad* Quad::SearchQuad(Point _Point)
{
	// The object must be inserted in the right quad
	if (_Point.x > mTopRight.x)
	{
		if (mRightTree != nullptr)
			return mRightTree->SearchQuad(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the left quad
	else if (_Point.x < mBotLeft.x)
	{
		if (mLeftTree != nullptr)
			return mLeftTree->SearchQuad(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the top quad
	else if (_Point.y > mTopRight.y)
	{
		if (mTopTree != nullptr)
			return mTopTree->SearchQuad(_Point);
		else
			return nullptr;
	}
	// The object must be inserted in the bottom quad
	else if (_Point.y < mBotLeft.y)
	{
		if (mBotTree != nullptr)
			return mBotTree->SearchQuad(_Point);
		else
			return nullptr;
	}
	else
	{
		if (SettingsSys->DrawActivePartitions)
		{
			DrawSelected(SceneSys->GetMainSpace());
		}
		return this;
	}
}

void Quad::RemoveObject(GameObject* _Object)
{
	if (_Object != nullptr)
	{
		auto _Found = mNode->mObjects.find(_Object->GetUID());
		if (_Found != mNode->mObjects.end())
			mNode->mObjects.erase(_Found);
	}
}

void Quad::GetActiveObjects(Point _BotLeft, Point _TopRight, std::vector<GameObject*>& _OutObjects)
{
	// Iterate over the quads
	// Increment the size of each partition
	int _MinX = _BotLeft.x;
	int _MaxX = _TopRight.x;
	int _MinY = _BotLeft.y;
	int _MaxY = _TopRight.y;

	unsigned _IteratedQuads = 0;

	for (int _x = _MinX ; _x <= _MaxX + mWidth; _x += mWidth)
	{
		for (int _y = _MinY ; _y <= _MaxY + mHeight ; _y += mHeight)
		{
			Quad* _vector = SearchQuad(Point(_x, _y));
			_IteratedQuads++;
			_vector = nullptr;
		}
	}
}

void Quad::GetActiveObjects(Point _BotLeft, Point _TopRight, std::map<unsigned, GameObject*>& _OutObjects, std::vector<Quad*>& _OutQuads)
{
	int _MinX = _BotLeft.x+1;
	int _MaxX = _TopRight.x;
	int _MinY = _BotLeft.y+1;
	int _MaxY = _TopRight.y;

	Quad* _MinQuad = SearchQuad(_BotLeft);
	Quad* _MaxQuad = SearchQuad(_TopRight);

	if(_MinQuad != _MaxQuad)
	{
		if (_MinQuad)
		{
			_MinX = _MinQuad->mBotLeft.x+1;
			_MinY = _MinQuad->mBotLeft.y+1;
		}
		if (_MaxQuad)
		{
			_MaxX = _MaxQuad->mTopRight.x;
			_MaxY = _MaxQuad->mTopRight.y;
		}
	}

	unsigned _IteratedQuads = 0;

	// Iterate over the quad knowing that its size is 500
	for (int _x = _MinX; _x < _MaxX /*+ mWidth*/; _x += mWidth)
	{
		for (int _y = _MinY; _y < _MaxY /*+ mHeight*/; _y += mHeight)
		{
			Quad* _vector = SearchQuad(Point(_x, _y));
			if (_vector && _vector->mNode)
			{
				_OutObjects.insert(std::begin(_vector->mNode->mObjects), std::end(_vector->mNode->mObjects));
				_OutQuads.push_back(_vector);
				_vector->mEnabled = true;
			}
			_IteratedQuads++;
		}
	}
}

void Quad::Reset(DrawDirection _Direction)
{
	// We are going to have two different behaviors
	// Starting from root we will clear all the partitions on the sides
	// O each one we will clean their up and bot partitions
	// Because of that we need to know in which direction are we moving
	if (mRightTree != nullptr && _Direction == DrawDirection::DrawRight)
	{
		mRightTree->Reset(DrawDirection::DrawRight);

		if (mTopTree != nullptr)
			mTopTree->Reset(DrawDirection::DrawTop);
		if (mBotTree != nullptr)
			mBotTree->Reset(DrawDirection::DrawBot);
	}

	if (mLeftTree != nullptr && _Direction == DrawDirection::DrawLeft)
	{
		mLeftTree->Reset(DrawDirection::DrawLeft);

		if (mTopTree != nullptr)
			mTopTree->Reset(DrawDirection::DrawTop);
		if (mBotTree != nullptr)
			mBotTree->Reset(DrawDirection::DrawBot);
	}

	if (mTopTree != nullptr && _Direction == DrawDirection::DrawTop)
		mTopTree->Reset(DrawDirection::DrawTop);

	if (mBotTree != nullptr && _Direction == DrawDirection::DrawBot)
		mBotTree->Reset(DrawDirection::DrawBot);
	if (mNode)
		mNode->mObjects.clear();
	mRoot->ClearedThisFrame++;
}

void Quad::Destroy(DrawDirection _Direction)
{
	// We are going to have two different behaviors
	// Starting from root we will clear all the partitions on the sides
	// O each one we will clean their up and bot partitions
	// Because of that we need to know in which direction are we moving
	if (mRightTree != nullptr && _Direction == DrawDirection::DrawRight)
	{
		mRightTree->Destroy(DrawDirection::DrawRight);
		delete mRightTree;
		mRightTree = nullptr;

		if (mTopTree != nullptr)
		{
			mTopTree->Destroy(DrawDirection::DrawTop);
			delete mTopTree;
			mTopTree = nullptr;
		}
		if (mBotTree != nullptr)
		{
			mBotTree->Destroy(DrawDirection::DrawBot);
			delete mBotTree;
			mBotTree = nullptr;
		}
	}

	if (mLeftTree != nullptr && _Direction == DrawDirection::DrawLeft)
	{
		mLeftTree->Destroy(DrawDirection::DrawLeft);
		delete mLeftTree;
		mLeftTree = nullptr;

		if (mTopTree != nullptr)
		{
			mTopTree->Destroy(DrawDirection::DrawTop);
			delete mTopTree;
			mTopTree = nullptr;
		}
		if (mBotTree != nullptr)
		{
			mBotTree->Destroy(DrawDirection::DrawBot);
			delete mBotTree;
			mBotTree = nullptr;
		}
	}

	if (mTopTree != nullptr && _Direction == DrawDirection::DrawTop)
	{
		mTopTree->Destroy(DrawDirection::DrawTop);
		delete mTopTree;
		mTopTree = nullptr;
	}

	if (mBotTree != nullptr && _Direction == DrawDirection::DrawBot)
	{
		mBotTree->Destroy(DrawDirection::DrawBot);
		delete mBotTree;
		mBotTree = nullptr;
	}
	if (mNode)
	{
		mNode->mObjects.clear();
		delete mNode;
		mNode = nullptr;
	}
}

bool Quad::IsEnabled()
{
	return mEnabled;
}

void Quad::SetEnabled(bool _State)
{
	mEnabled = _State;
}

bool Quad::DrawQuad(Space * _Space, DrawDirection _Direction)
{
	// We are going to have two different behaviors
	// Starting from root we will clear all the partitions on the sides
	// O each one we will clean their up and bot partitions
	// Because of that we need to know in which direction are we moving

	// TODO: Clean this drawing calls
	if (mRightTree != nullptr &&  DrawDirection::DrawRight == _Direction)
	{
		mRightTree->DrawQuad(_Space, DrawDirection::DrawRight);

		//DrawConnections(_Space);
		if (mRightTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mTopRight.x, (mTopRight.y + mBotLeft.y) / 2 - 50 }, 50, Colors::green);
			_Space->DrawCircle(glm::vec2{ mTopRight.x + 50, (mTopRight.y + mBotLeft.y) / 2 - 50 }, 15, Colors::green);
		}
		if (mLeftTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mBotLeft.x, (mTopRight.y + mBotLeft.y) / 2 }, 50, Colors::green);
			_Space->DrawCircle(glm::vec2{ mBotLeft.x - 50, (mTopRight.y + mBotLeft.y) / 2 }, 15, Colors::green);
		}
		if (mTopTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 50, mTopRight.y }, 50, Colors::green);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 50, mTopRight.y + 50 }, 15, Colors::green);
		}
		if (mBotTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y }, 50, Colors::green);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y - 50 }, 15, Colors::green);
		}

		if (mTopTree != nullptr)
			mTopTree->DrawQuad(_Space, DrawDirection::DrawTop);
		if (mBotTree != nullptr)
			mBotTree->DrawQuad(_Space, DrawDirection::DrawBot);
	}

	if (mLeftTree != nullptr && DrawDirection::DrawLeft == _Direction)
	{
		mLeftTree->DrawQuad(_Space, DrawDirection::DrawLeft);

		//DrawConnections(_Space);
		if (mRightTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mTopRight.x, (mTopRight.y + mBotLeft.y) / 2 - 25 }, 50, Colors::yellow);
			_Space->DrawCircle(glm::vec2{ mTopRight.x + 25, (mTopRight.y + mBotLeft.y) / 2 - 25 }, 15, Colors::yellow);
		}
		if (mLeftTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mBotLeft.x, (mTopRight.y + mBotLeft.y) / 2 }, 50, Colors::yellow);
			_Space->DrawCircle(glm::vec2{ mBotLeft.x - 25, (mTopRight.y + mBotLeft.y) / 2 }, 15, Colors::yellow);
		}
		if (mTopTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 50, mTopRight.y }, 50, Colors::yellow);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 25, mTopRight.y + 25 }, 15, Colors::yellow);
		}
		if (mBotTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y }, 50, Colors::yellow);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y - 25 }, 15, Colors::yellow);
		}

		if (mTopTree != nullptr)
			mTopTree->DrawQuad(_Space, DrawDirection::DrawTop);
		if (mBotTree != nullptr)
			mBotTree->DrawQuad(_Space, DrawDirection::DrawBot);
	}
	if (_Direction == DrawDirection::DrawTop)
	{
		if (mTopTree != nullptr)
			mTopTree->DrawQuad(_Space, _Direction);

		//DrawConnections(_Space);
		if (mRightTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mTopRight.x, (mTopRight.y + mBotLeft.y) / 2 - 75 }, 50, Colors::red);
			_Space->DrawCircle(glm::vec2{ mTopRight.x + 75, (mTopRight.y + mBotLeft.y) / 2 - 75 }, 15, Colors::red);
		}
		if (mLeftTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mBotLeft.x, (mTopRight.y + mBotLeft.y) / 2 }, 50, Colors::red);
			_Space->DrawCircle(glm::vec2{ mBotLeft.x - 75, (mTopRight.y + mBotLeft.y) / 2 }, 15, Colors::red);
		}
		if (mTopTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 75, mTopRight.y }, 50, Colors::red);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 75, mTopRight.y + 75 }, 15, Colors::red);
		}
		if (mBotTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y }, 50, Colors::red);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y - 75 }, 15, Colors::red);
		}
	}

	if (_Direction == DrawDirection::DrawBot)
	{
		if (mBotTree != nullptr)
			mBotTree->DrawQuad(_Space, _Direction);
		//DrawConnections(_Space);
		if (mRightTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mTopRight.x, (mTopRight.y + mBotLeft.y) / 2 - 100 }, 50, Colors::blue);
			_Space->DrawCircle(glm::vec2{ mTopRight.x + 100, (mTopRight.y + mBotLeft.y) / 2 - 100 }, 15, Colors::blue);
		}
		if (mLeftTree != nullptr)
		{
			_Space->DrawHorizontalLine(glm::vec2{ mBotLeft.x, (mTopRight.y + mBotLeft.y) / 2 }, 50, Colors::blue);
			_Space->DrawCircle(glm::vec2{ mBotLeft.x - 100, (mTopRight.y + mBotLeft.y) / 2 }, 15, Colors::blue);
		}
		if (mTopTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 100, mTopRight.y }, 50, Colors::blue);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 100, mTopRight.y + 100 }, 15, Colors::blue);
		}
		if (mBotTree != nullptr)
		{
			_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y }, 50, Colors::blue);
			_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y - 100 }, 15, Colors::blue);
		}
	}

	glm::vec2 _Pos{(mBotLeft.x + mTopRight.x)/2, (mBotLeft.y + mTopRight.y)/2};
	_Space->Drawrectangle(_Pos, mTopRight.x - mBotLeft.x, mTopRight.y - mBotLeft.y, Colors::green);
	mRoot->DrawnThisFrame++;
	return true;
}

bool Quad::InQuad(Point _P)
{
	return (_P.x >= mBotLeft.x && _P.x <= mTopRight.x && _P.y >= mBotLeft.y && _P.y <= mTopRight.y);
}

void Quad::DrawConnections(Space* _Space)
{
	if (mRightTree != nullptr)
	{
		_Space->DrawHorizontalLine(glm::vec2{ mTopRight.x, (mTopRight.y + mBotLeft.y) / 2 - 25 }, 50, Colors::red);
		_Space->DrawCircle(glm::vec2{ mTopRight.x + 50, (mTopRight.y + mBotLeft.y) / 2 - 25 }, 15, Colors::red);
	}
	if (mLeftTree != nullptr)
	{
		_Space->DrawHorizontalLine(glm::vec2{ mBotLeft.x, (mTopRight.y + mBotLeft.y) / 2 }, 50, Colors::yellow);
		_Space->DrawCircle(glm::vec2{ mBotLeft.x - 50, (mTopRight.y + mBotLeft.y) / 2 }, 15, Colors::yellow);
	}
	if (mTopTree != nullptr)
	{
		_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 25, mTopRight.y }, 50, Colors::red);
		_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2 + 25, mTopRight.y + 50 }, 15, Colors::red);
	}
	if (mBotTree != nullptr)
	{
		_Space->DrawVerticalLine(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y }, 50, Colors::yellow);
		_Space->DrawCircle(glm::vec2{ (mBotLeft.x + mTopRight.x) / 2, mBotLeft.y - 50 }, 15, Colors::yellow);
	}
}

void Quad::DrawSelected(Space* _Space)
{
	_Space->DrawLine(glm::vec2{ mBotLeft.x, mTopRight.y }, glm::vec2{ mTopRight.x, mBotLeft.y }, Colors::blue);
	_Space->DrawLine(glm::vec2{ mBotLeft.x, mBotLeft.y }, glm::vec2{ mTopRight.x, mTopRight.y }, Colors::blue);
}

void Quad::CheckAdjacency(GameObject* _Object, bool _Displaced)
{
	Point _TopRight{ _Object->mPos.x + _Object->mScale.x, _Object->mPos.y + _Object->mScale.y };
	Point _BotLeft{ _Object->mPos.x - _Object->mScale.x, _Object->mPos.y - _Object->mScale.y };

	if (_Displaced)
	{
		_TopRight = Point{ _Object->mDisplacedPos.x + _Object->mScale.x, _Object->mDisplacedPos.y + _Object->mScale.y };
		_BotLeft =  Point{ _Object->mDisplacedPos.x - _Object->mScale.x, _Object->mDisplacedPos.y - _Object->mScale.y };
	}

	// The object is as well in the right partitions
	if(_TopRight.x > mTopRight.x)
	{ 
		// Sanity check
		if (mRightTree)
		{
			// The oject is in the top right corner partition
			if(_TopRight.y > mRightTree->mTopRight.y)
			{
				// Sanity check
				if (mRightTree->mTopTree)
				{
					if (mRightTree->mTopTree->mNode == nullptr)
						mRightTree->mTopTree->mNode = new TreeNode();
					mRightTree->mTopTree->mNode->mObjects[_Object->GetUID()] = _Object;
					_Object->mQuad.push_back(mRightTree->mTopTree);
				}
			}
			// The object is in the bot right corner partition
			else if(mBotLeft.y < mRightTree->mBotLeft.y)
			{
				// Sanity check
				if (mRightTree->mBotTree)
				{
					if (mRightTree->mBotTree->mNode == nullptr)
						mRightTree->mBotTree->mNode = new TreeNode();
					mRightTree->mBotTree->mNode->mObjects[_Object->GetUID()] = _Object;
					_Object->mQuad.push_back(mRightTree->mBotTree);
				}
			}
			// The object is in the right partition
			else
			{
				if (mRightTree->mNode == nullptr)
					mRightTree->mNode = new TreeNode();
				mRightTree->mNode->mObjects[_Object->GetUID()] = _Object;
				_Object->mQuad.push_back(mRightTree);
			}
		}
	}
	// The object is as well in the left partitions
	else if(_BotLeft.x < mBotLeft.x)
	{
		// Sanity check
		if (mLeftTree)
		{
			// The oject is in the top right corner partition
			if (_TopRight.y > mLeftTree->mTopRight.y)
			{
				// Sanity check
				if (mLeftTree->mTopTree)
				{
					if (mLeftTree->mTopTree->mNode == nullptr)
						mLeftTree->mTopTree->mNode = new TreeNode();
					mLeftTree->mTopTree->mNode->mObjects[_Object->GetUID()] = _Object;
					_Object->mQuad.push_back(mLeftTree->mTopTree);
				}
			}
			// The object is in the bot right corner partition
			else if (mBotLeft.y < mLeftTree->mBotLeft.y)
			{
				// Sanity check
				if (mLeftTree->mBotTree)
				{
					if (mLeftTree->mBotTree->mNode == nullptr)
						mLeftTree->mBotTree->mNode = new TreeNode();
					mLeftTree->mBotTree->mNode->mObjects[_Object->GetUID()] = _Object;
					_Object->mQuad.push_back(mLeftTree->mBotTree);
				}
			}
			// The object is in the left partition
			else
			{
				if (mLeftTree->mNode == nullptr)
					mLeftTree->mNode = new TreeNode();
				mLeftTree->mNode->mObjects[_Object->GetUID()] = _Object;
				_Object->mQuad.push_back(mLeftTree);
			}
		}
	}
	// The object is as well in the top partition
	if(_TopRight.y > mTopRight.y)
	{
		// Sanity check
		if (mTopTree)
		{
			if (mTopTree->mNode == nullptr)
				mTopTree->mNode = new TreeNode();
			mTopTree->mNode->mObjects[_Object->GetUID()] = _Object;
			_Object->mQuad.push_back(mTopTree);
		}
	}
	// the object is as well in the bot partition
	else if(_BotLeft.y < mBotLeft.y)
	{
		// Sanity check
		if (mBotTree)
		{
			if (mBotTree->mNode == nullptr)
				mBotTree->mNode = new TreeNode();
			mBotTree->mNode->mObjects[_Object->GetUID()] = _Object;
			_Object->mQuad.push_back(mBotTree);
		}
	}
}
