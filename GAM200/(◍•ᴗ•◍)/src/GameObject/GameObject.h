/**********************************************************************************/
/*!
\file   GameObject.h
\author David Miranda & Juan Pacheco
\par    email: m.david@digipen.edu & pacheco.j@digipen.edu
\par    DigiPen login: m.david & pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing functions to create a gameobject, serialize from json.
*/
/*********************************************************************************/
#pragma once
#include <vector>
#include <glm/glm.hpp>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"

#include "../EventSystem/Listener/Listener.h"
#include "../EventSystem/EventHandler.h"

#include "../SpacePartitioning/Quadtree.h"

class Space;
class ArchetypeUpdated;
class Quad;
//struct Point;

struct HierarchyChange
{
	GameObject* ChildToChange;
	GameObject* NewParent;
	GameObject* OldParent;
};

class GameObject : public IBase, public Listener
{
	friend class Space;
	friend class Editor;
	friend class Menus;
	friend class Quad;
	friend class TransformComponent;
	friend GameObject* CreateFromArchetype(const std::string& name, Space* space);
	friend GameObject* CreateFromArchetype(const std::string& filepath, Space* space, const glm::vec3& position);
	SM_RTTI_DECL(GameObject, IBase)

public:
	GameObject();
	GameObject(GameObject& rhs);
	virtual ~GameObject();
	GameObject& operator= (const GameObject& rhs);
	GameObject* Clone();
	virtual void CreateArchetype(std::string & filename);


#pragma region Collisions
	void OnCollisionStarted(GameObject* other);
	void OnCollisionPersisted(GameObject* other);
	void OnCollisionEnded(GameObject* other);
#pragma endregion

#pragma region GameLoop
	virtual void OnCreate();
	virtual void Shutdown();
#pragma endregion

#pragma region IBase
	bool SetEnabled(bool enabled) override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	void FromJsonUndo(const nlohmann::json& j);
#pragma endregion

	Space* GetParentSpace();
	int GetSpaceID();
	Quad* GetQuad();
	bool IsQuadEnabled();
	bool IsDeleted();

#pragma region Tags
	void AddTag(std::string _tag);
	void RemoveTag(std::string _tag);
	std::vector<std::string> GetTags();
	bool CompareTag(std::string tag);
	bool HasTag(std::string tag);
	int FindComponentMaxId();
#pragma endregion

#pragma region ComponentManagement

	unsigned GetCompCount();
	IComp* GetComp(const char* type);
	template<typename T>
	T* GetComp()
	{
		auto comp = std::find_if(mComponents.begin(), mComponents.end(), [&](auto& it)
		{
			return dynamic_cast<T*>(it) != nullptr;
		});
		if (comp != mComponents.end())
			return  dynamic_cast<T*> (*comp);
		else
			return nullptr;
	}

	IComp* AddComp(IComp* comp);
	template<typename T>
	IComp* AddComp(IComp* _comp)
	{
		auto comp = std::find_if(mComponents.begin(), mComponents.end(), [&](auto& it)
		{
			return dynamic_cast<T*>(it) != nullptr;
		});
		// Component not duplicated
		if (comp == mComponents.end())
		{
			_comp->mOwner = this;
			_comp->mEnabled = mEnabled;
			//_comp->Initialize();
			RegisterComponent(reinterpret_cast<IComp*>(_comp));
			mComponents.push_back(_comp);

			std::sort(mComponents.begin(), mComponents.end(), [&](auto& _Comp1, auto& _Comp2)
			{
				return _Comp1->GetOrderKey() < _Comp2->GetOrderKey();
			});
			return _comp;
		}
		else
			return nullptr;
	}
	template<typename T>
	bool HasComp()
	{
		if (GetComp<T>() == nullptr)
			return false;

		else
			return true;
	}
	void RegisterComponent(IComp* comp);

	void RemoveComp(IComp* comp);
	void RemoveCompType(const char* compType);
	void RemoveAllComp();

	const std::string& GetArchetypeName();
	void SetArchetypeName(const std::string& _archetypeName);
	void UnLinkFromArchetype();
#pragma endregion

#pragma region HierarchyManagement
	GameObject* AddChild(GameObject* _child);
	void RemoveChild(GameObject* _child);
	GameObject* SetParent(GameObject* _parent);
	GameObject* GetParent();
	void EditHierarchy(bool show_children, std::vector<HierarchyChange>* Changes);
#pragma endregion




#pragma region Events
	//void handle_event(const Event& _event)override;
	EVENTHANDLER_DECL
		void OnArchetypeUpdated(const ArchetypeUpdated& _event);
#pragma endregion

	bool HasChangedPartition();
	void RemoveFromPartitions();

private:
	std::vector<Quad*> mQuad;
	Point mPos;
	Point mDisplacedPos = Point{0, 0};
	glm::ivec2 mScale = glm::ivec2{ 0, 0 };
	bool mDisplacedCollider = false;

	int spaceId;
	bool mbDeleted = false;
	Space* mParentSpace = nullptr;
	GameObject* mParent;
	std::string mArchetype;
	std::vector<GameObject*> mChildren;
	std::vector<IComp*> mComponents;
	std::vector<std::string> mTags;



	bool to_serialize=true;
};


