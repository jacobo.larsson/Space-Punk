/**********************************************************************************/
/*
\file   GameObject.h
\author David Miranda & Juan Pacheco
\par    email: m.david@digipen.edu & pachecho.j@digipen.edu
\par    DigiPen login: m.david & pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the definitions for the functions declared on GameObject.h
*/
/*********************************************************************************/

#include <fstream>
#include <iostream>
#include <glm/gtx/rotate_vector.hpp>


#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"

#include "../Factory/Factory.h"
#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/ArchetypesEvents/ArchetypesEvents.h"
#include "../Editor/Editor.h"

#include "../Transform2D/TransformComponent.h"
#include "../Editor/Editor.h"

#include "../Space/Space.h"
#include "../Scene/Scene.h"
#include "../SpacePartitioning/Quadtree.h"

#include "GameObject.h"

GameObject::GameObject() :mParent{ nullptr }, mParentSpace{ nullptr }, mArchetype("")
{
	EventSys->subscribe(this, ArchetypeUpdated());
	mEventHandler.register_handler(*this, &GameObject::OnArchetypeUpdated);
}
GameObject::GameObject(GameObject& rhs)
{
	//IBase variables
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;

	//GameObject variables
	mbDeleted = rhs.mbDeleted;
	mParent = rhs.mParent;
	mParentSpace = rhs.mParentSpace;

	for (unsigned i = 0; i < rhs.mComponents.size(); i++)
		AddComp(dynamic_cast<IComp*>(rhs.mComponents[i]->Clone()));

	for (unsigned i = 0; i < rhs.mChildren.size(); i++)
		AddChild(rhs.mChildren[i]->Clone());
	mQuad.reserve(4);
}
GameObject::~GameObject()
{
	Shutdown();
	EventSys->unsubscribe(this, ArchetypeUpdated());
	// Delete all the components of the object
}

GameObject& GameObject::operator= (const GameObject& rhs)
{
	if (&rhs == this)
		return *this;
	auto transform = GetComp<TransformComponent>();
	glm::vec3 prevPos(0.0f, 0.0f, 0.0f);
	if (transform)
		prevPos = transform->GetWorldPosition();
	Shutdown();

	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
	mArchetype = rhs.mArchetype;

	mbDeleted = rhs.mbDeleted;

	if (rhs.mParentSpace)
		mParentSpace = rhs.mParentSpace;

	for (auto& tag : rhs.mTags) {
		AddTag(tag);
	}

	// To be discussed 
	//mParent = rhs.mParent; 


	for (unsigned i = 0; i < rhs.mComponents.size(); i++)
	{
		auto comp = dynamic_cast<IComp*>(rhs.mComponents[i]->Clone());
		comp->SetOwner(this);
		comp->SetOrderKey(rhs.mComponents[i]->GetOrderKey());
		AddComp(comp);
	}

	for (unsigned i = 0; i < rhs.mChildren.size(); i++)
		AddChild(rhs.mChildren[i]->Clone());

	if (GetComp<TransformComponent>())
		GetComp<TransformComponent>()->SetWorldPosition(prevPos);

	return *this;
}
GameObject* GameObject::Clone()
{
	GameObject* temp = FactorySys->Create<GameObject>();
	(*temp) = (*this);
	return temp;
}
void GameObject::CreateArchetype(std::string& filename)
{
	using nlohmann::json;

	json j;
	ToJson(j);

	std::ofstream fp;
	fp.open(filename);

	if (fp.is_open())
	{
		fp << std::setw(4) << j;
		fp.close();
	}
}

void GameObject::OnCollisionStarted(GameObject* other)
{
	for (auto& comp : mComponents)
		comp->OnCollisionStarted(other);
}
void GameObject::OnCollisionPersisted(GameObject* other)
{
	for (auto& comp : mComponents)
		comp->OnCollisionPersisted(other);
}
void GameObject::OnCollisionEnded(GameObject* other)
{
	if (other)
	{
		for (auto& comp : mComponents)
			comp->OnCollisionEnded(other);
	}
}

void GameObject::OnCreate()
{
}
void GameObject::Shutdown()
{
	for (auto& it : mComponents)
	{
		it->Shutdown();
		if (mParentSpace)
		{
			mParentSpace->DeleteComponent(it);
			// TODO: This could be problematic
		}
		else
			delete it;
	}
	mComponents.clear();

	for (auto& _Tag : mTags)
	{
		if(mParentSpace)
			mParentSpace->RemoveObjectFromTag(_Tag, this);
	}

	if (mQuad.size())
		RemoveFromPartitions();
}

bool  GameObject::SetEnabled(bool enabled)
{
	// change state
	this->mEnabled = enabled;

	// enable/disable all components
	for (auto& comp : mComponents)
		comp->SetEnabled(enabled);

	// enable/disable all children
	/*for (auto& c : mChildren)
		c->SetEnabled(enabled);*/

	// return enabled
	return mEnabled;
}
void GameObject::ToJson(nlohmann::json& j)
{
	using nlohmann::json;

	// Write the IBase data (name, enabled, guid)
	IBase::ToJson(j);
	if (mTags.size())
	{
		json& mTagsJson = j["tags"];
		for (auto& tag : mTags)
		{
			json tagJson;
			mTagsJson.push_back(tag);
		}

	}
	j["deleted"] = mbDeleted;
	j["ArchetypeName"] = mArchetype;
	if (mComponents.size())
	{
		json& componentsJson = j["components"];
		for (auto& comp : mComponents)
		{
			json compJson;
			compJson["_type"] = comp->GetType().GetName();
			comp->ToJson(compJson);
			componentsJson.push_back(compJson);
		}
	}

	if (mChildren.size())
	{
		json& childrenJson = j["children"];
		for (auto& c : mChildren) {
			json cJson;
			c->ToJson(cJson);
			childrenJson.push_back(cJson);
		}
	}

	j["spaceId"] = spaceId;
}
void GameObject::FromJson(const nlohmann::json& j)
{
	std::vector<int> foo(3, 0);
	std::vector<int> bar(5, 0);


	using nlohmann::json;
	// read from json as IBase (name, uid, enabled)
	IBase::FromJson(j);
	auto tags = j.find("tags");
	if (tags != j.end() /*&& j["tags"].get<std::string>() != ""*/)
	{
		for (auto& tag : *tags)
		{
			AddTag(tag);
			if (mParentSpace != nullptr) {
				mParentSpace->AddObjectToTag(tag, this);
				SceneSys->RegisterTag(tag);
			}
		}
	}
	if (j.find("deleted") != j.end())
		mbDeleted = j["deleted"];

	if (j.find("spaceId") != j.end())
		spaceId = j["spaceId"];

	if (j.find("ArchetypeName") != j.end())
	{
		mArchetype = j["ArchetypeName"].get<std::string>();
		if (mArchetype == "NotValidArchetype")
			EventSys->unsubscribe(this, ArchetypeUpdated());
	}


	auto componentsJson = j.find("components");
	if (componentsJson != j.end()) {
		for (const auto& compJson : *componentsJson) {
			// get the type name
			if (compJson.find("_type") != compJson.end())
			{
				std::string compType = compJson["_type"];

				// create with the factory from the type
				if (IBase* newComp = FactorySys->Create(compType.c_str()))
				{
					this->AddComp((IComp*)newComp);
					newComp->FromJson(compJson);
				}
			}
			// else  PRINT ERROR
		}
	}

	auto childrenJson = j.find("children");
	if (childrenJson != j.end()) {
		for (const auto& cJson : *childrenJson)
		{
			if (cJson.find("name") != cJson.end())
			{
				std::string objectName = cJson["name"];
				GameObject* newobj = FactorySys->Create<GameObject>();
				if (newobj)
				{
					newobj->mParentSpace = mParentSpace;
					newobj->SetName(objectName);
					mParentSpace->GetAllObjects().push_back(newobj);
					newobj->FromJson(cJson);
					AddChild(newobj);

				}
			}
		}
	}
}

void GameObject::FromJsonUndo(const nlohmann::json& j)
{
	Shutdown();
	std::vector<int> foo(3, 0);
	std::vector<int> bar(5, 0);
	
	
	using nlohmann::json;
	// read from json as IBase (name, uid, enabled)
	IBase::FromJson(j);
	auto tags = j.find("tags");
	if (tags != j.end() /*&& j["tags"].get<std::string>() != ""*/)
	{
		for (auto& tag : *tags)
		{
			AddTag(tag);
			if (mParentSpace != nullptr) {
				mParentSpace->AddObjectToTag(tag, this);
				SceneSys->RegisterTag(tag);
			}
		}
	}
	if (j.find("deleted") != j.end())
		mbDeleted = j["deleted"];
	
	if (j.find("ArchetypeName") != j.end())
	{
		mArchetype = j["ArchetypeName"].get<std::string>();
		if (mArchetype == "NotValidArchetype")
			EventSys->unsubscribe(this, ArchetypeUpdated());
	}

	auto componentsJson = j.find("components");
	int a = 0;
	if (componentsJson != j.end()) {


		for (const auto& compJson : *componentsJson) {
			// get the type name
			if (compJson.find("_type") != compJson.end())
			{
				std::string compType = compJson["_type"];
				IComp* a = GetComp(compType.c_str());

				//
				if (a == nullptr)
				{
					// create with the factory from the type
					if (IBase* newComp = FactorySys->Create(compType.c_str()))
					{
						this->AddComp((IComp*)newComp);
						newComp->FromJson(compJson);
					}
				}
				else
				{
					
						a->FromJson(compJson);

				}

			}
			a++;
		}

	}

	/*auto componentsJson2 = j.find("components");
	if (a != GetCompCount())
	{
		for (const auto& compJson : *componentsJson) {
			// get the type name
			if (compJson.find("_type") != compJson.end())
			{
				std::string compType = compJson["_type"];
				IComp* a = GetComp(compType.c_str());

				//
				if (a == nullptr)
				{
					// create with the factory from the type
					if (IBase* newComp = FactorySys->Create(compType.c_str()))
					{
						this->AddComp((IComp*)newComp);
						newComp->FromJson(compJson);
					}
				}
				else
				{

					a->FromJson(compJson);

				}

			}
			a++;
		}



	}*/
	auto childrenJson = j.find("children");
	if (childrenJson != j.end()) {
		for (const auto& cJson : *childrenJson) {
	
			// create with the factory from the type
			GameObject* newObj = nullptr;
		}
	}

	if (j.find("spaceId") != j.end())
		spaceId = j["spaceId"];
}


GameObject* GameObject::AddChild(GameObject* _child)
{
	if (_child == nullptr)
	{
		std::cout << "Could not add child to " << mName << " nullptr" << std::endl;
		return nullptr;
	}

	auto child = std::find(mChildren.begin(), mChildren.end(), _child);
	if (child == mChildren.end())
	{
		mChildren.push_back(_child);
		_child->mParent = this;
		_child->mParentSpace = mParentSpace;
		_child->mEnabled = mEnabled;
		TransformComponent* trans = _child->GetComp<TransformComponent>();
		if (trans)
		{
			IComp* parent_transform2 = _child->GetParent()->GetComp<TransformComponent>();
			if (parent_transform2)
			{
				TransformComponent* parent_transform_casted = dynamic_cast<TransformComponent*>(parent_transform2);
				if (parent_transform_casted)
				{
					trans->SetParentTransform(parent_transform_casted);
					return _child;
				}
				trans->SetParentTransform(nullptr);
			}
			trans->SetParentTransform(nullptr);
		}
	}
	return _child;
}

void GameObject::RemoveChild(GameObject* _child)
{
	auto child = std::find(mChildren.begin(), mChildren.end(), _child);

	if (child != mChildren.end())
	{
		_child->Shutdown();
		delete _child;
		mChildren.erase(child);
	}
}

GameObject* GameObject::SetParent(GameObject* _parent)
{
	if (_parent != nullptr)
	{
		mParent = _parent;
		mParentSpace = _parent->mParentSpace;
		return _parent;
	}
	else
	{
		std::cout << "Could not set parent to " << mName << " nullptr" << std::endl;
		return nullptr;
	}

}
GameObject* GameObject::GetParent()
{
	return mParent;
}
void GameObject::EditHierarchy(bool show_children, std::vector<HierarchyChange>* Changes)
{
	GameObject* test = this;

	//The hierarchy drag and drop functionality
	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
	{
		// Set payload to carry the index of our item (could be anything)
		ImGui::SetDragDropPayload("DND_DEMO_CELL", &test, sizeof(GameObject*));
		ImGui::EndDragDropSource();
	}
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DND_DEMO_CELL"))
		{
			IM_ASSERT(payload->DataSize == sizeof(GameObject*));
			GameObject* children = *((GameObject**)payload->Data);
			if (children)
			{
				if (children->mParent == this)
				{
					HierarchyChange temp;
					temp.ChildToChange = children;
					temp.NewParent = mParent;
					temp.OldParent = this;
					Changes->push_back(temp);

				}
				else
				{
					//Check that we are not adding a parent to a child
					bool Add = true;
					GameObject* parent = mParent;
					while (parent != nullptr)
					{
						if (parent == children)
						{
							Add = false;
							break;
						}
						parent = parent->mParent;

					}

					//We can safely add the children
					if (Add)
					{
						HierarchyChange temp;
						temp.ChildToChange = children;
						temp.NewParent = this;
						temp.OldParent = children->mParent;
						Changes->push_back(temp);
					}
				}
			}
		}
		ImGui::EndDragDropTarget();
	}


	if (mChildren.size() && show_children)
	{
		if (ImGui::TreeNode("Children"))
		{
			for (auto& et : mChildren)
			{
				ImGui::PushID(&(*et));
				bool selected = ImGui::Selectable(et->GetName().c_str());
				et->EditHierarchy(true, Changes);
				if (selected)
				{
					EditorSys->SetPickedObject(et);
					EditorSys->ChangeEditorState("EditSelectedObject");
					if (et->HasComp<TransformComponent>())
						EditorSys->CenterCameraAt(et->GetComp<TransformComponent>()->GetWorldPosition());
				}
				ImGui::PopID();
				
			}
			ImGui::TreePop();
		}
	}
}
Space* GameObject::GetParentSpace()
{
	return mParentSpace;
}

int GameObject::GetSpaceID()
{
	return spaceId;
}

bool GameObject::IsDeleted()
{
	return mbDeleted;
}

//Quad* GameObject::GetQuad()
//{
//	return mQuad;
//}
//
//bool GameObject::IsQuadEnabled()
//{
//	return mQuad->IsEnabled();
//}

void GameObject::AddTag(std::string _tag)
{
	auto tag = std::find(mTags.begin(), mTags.end(), _tag);
	if (tag == mTags.end())
	{
		SceneSys->RegisterTag(_tag);
		if (mParentSpace != nullptr) {
			this->mParentSpace->AddObjectToTag(_tag, this);
		}
		mTags.push_back(_tag);
	}

}
void GameObject::RemoveTag(std::string _tag)
{
	auto tag = std::find(mTags.begin(), mTags.end(), _tag);
	if (tag != mTags.end())
	{
		mTags.erase(tag);
	}
}
std::vector<std::string> GameObject::GetTags()
{
	return mTags;
}
bool GameObject::CompareTag(std::string _tag)
{
	for (auto tag : mTags)
	{
		if (tag == _tag)
			return true;
	}
	return false;
}
bool GameObject::HasTag(std::string _tag)
{
	return CompareTag(_tag);
}

int GameObject::FindComponentMaxId()
{
	int max_id = 0;
	for (auto it = mComponents.begin(); it != mComponents.end(); it++)
	{
		if ((*it)->GetUID() > max_id)
			max_id = (*it)->GetUID();

		

	}
	return max_id;
}

unsigned GameObject::GetCompCount()
{
	return (unsigned)mComponents.size();
}
IComp* GameObject::GetComp(const char* type)
{
	auto comp = std::find_if(mComponents.begin(), mComponents.end(),
		[&](auto & it)
	{
		return(it->mName == type);
	});
	
	if (comp != mComponents.end())
		return *comp;

	return nullptr;
}
IComp* GameObject::AddComp(IComp* comp)
{
	// Check for duplicates
	auto duplicated = std::find_if(mComponents.begin(), mComponents.end(), [&](auto& it)
	{
		return comp->GetName() == it->GetName();
	});

	if (duplicated == mComponents.end())
	{
		comp->mOwner = this;
		comp->mEnabled = mEnabled;
		SettingsSys->AddKey(comp);
		//comp->Initialize();
		RegisterComponent(comp);
		mComponents.push_back(comp);

		std::sort(mComponents.begin(), mComponents.end(), [&](auto& _Comp1, auto& _Comp2)
		{
			return _Comp1->GetOrderKey() < _Comp2->GetOrderKey();
		});
		return comp;
	}
	return nullptr;
}
void GameObject::RegisterComponent(IComp* comp)
{
	if (mParentSpace == nullptr)
		return;
	mParentSpace->RegisterComponent(comp);
	SettingsSys->AddKey(comp);
}

void GameObject::RemoveComp(IComp* comp)
{
	// Sanity check to avoid deleting other's components
	if (comp == nullptr || comp->GetOwner() != this)
		return;
	// Find the component in the vector
	auto to_delete = std::find(mComponents.begin(), mComponents.end(), comp);

	// If not found just ignore it
	if (to_delete != mComponents.end())
	{
		// Delete the component from both the vector and memory.
		comp->Shutdown();
		mParentSpace->DeleteComponent(comp);
		//delete comp;
		mComponents.erase(to_delete);
	}
}
void GameObject::RemoveCompType(const char* compType)
{
	auto comp = std::find_if(mComponents.begin(), mComponents.end(),
		[&](auto& it)
	{
		return (it->TYPE().GetName() == compType);
	});

	if (comp != mComponents.end())
	{
		(*comp)->Shutdown();
		mParentSpace->DeleteComponent(*comp);
		//delete *comp;
		mComponents.erase(comp);
	}
}
void GameObject::RemoveAllComp()
{
	for (auto& it : mComponents)
	{
		it->Shutdown();
		delete it;
	}
	mComponents.clear();
}

const std::string& GameObject::GetArchetypeName()
{
	return mArchetype;
}
void GameObject::SetArchetypeName(const std::string& _archetypeName)
{
	mArchetype = _archetypeName;
}
void GameObject::UnLinkFromArchetype()
{
	mArchetype = "";
}


void GameObject::OnArchetypeUpdated(const ArchetypeUpdated& _event)
{
	if (mArchetype == _event.GetName() && (this != _event.GetObject()))
	{
		(*this) = *_event.GetObject();
	}
}

bool GameObject::HasChangedPartition()
{
	bool _Exited = true;

	for (auto& _Quad : mQuad)
	{
		// if the object is still inside one of the partitions, keep it in there
		if (mPos.x <= _Quad->mTopRight.x && mPos.y <= _Quad->mTopRight.y &&
			mPos.x >= _Quad->mBotLeft.x && mPos.y >= _Quad->mBotLeft.y)
			_Exited = false;
	}
	return _Exited;
}

void GameObject::RemoveFromPartitions()
{
	for (auto& _Quad : mQuad)
	{
		_Quad->RemoveObject(this);
	}
	if(mQuad.size())
		mQuad.clear();
}
