/**********************************************************************************/
/*!
\file   ICollider.cpp
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions declarations of ICollider.h
*/
/*********************************************************************************/
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>


#include "../Extern/ImGui/imgui.h"
#include "../Extern/ImGui/imgui_impl_sdl.h"
#include "../Extern/ImGui/imgui_impl_opengl3.h"


#include "../Factory/Factory.h"
#include "../Transform2D/TransformComponent.h"
#include "../Graphics/Color/Color.h"
#include "../Graphics/RenderManager/RenderManager.h"
#include "../Input/Input.h"
#include "../Editor/ObjectEdition/ObjectEdition.h"

#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/RigidBodyEvents/RigidBodyEvents.h"

#include "../GameObject/GameObject.h"

#include "../Scene/Scene.h"
#include "../Space/Space.h"
#include "../Settings/Settings.h"
#include "../Physics/RigidBody.h"

#include "ICollider.h"
#include "CollisionSystem.h"

Collider::Collider():type_of_object(1)
{
	mName = "Collider";
	EventSys->subscribe(this, CollisionGroupUpdated());
	mEventHandler.register_handler(*this, &Collider::OnCollisionGroupUpdated);
}
Collider::Collider(Collider& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;

	isignored = rhs.isignored;
	shape = rhs.shape;

	collide_box = rhs.collide_box;
}
Collider::~Collider()
{
	Collsys->RemoveCollider(this);
}
Collider& Collider::operator=(Collider& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;

	isignored = rhs.isignored;
	shape = rhs.shape;

	collide_box.local_transform.mPosition = rhs.collide_box.local_transform.mPosition;
	collide_box.local_transform.mScale = rhs.collide_box.local_transform.mScale;
	collide_box.local_transform.mOrientation = rhs.collide_box.local_transform.mOrientation;

	type_of_object = rhs.type_of_object;

	return *this;
}
Collider* Collider::Clone()
{
	Collider* temp = FactorySys->Create<Collider>();
	(*temp) = (*this);
	return temp;
}

void Collider::OnCreate()
{
}

void Collider::Initialize()
{
	if (mOwner != nullptr)
	{
		TransformComponent* _Transform = mOwner->GetComp<TransformComponent>();
		if (_Transform != nullptr)
		{
			collide_box.SetParentTransformNoChange(dynamic_cast<TransformComponent*>(_Transform));
			if (collide_box.GetWorldOrientation() != 0)
				shape = CSHAPE_OBB;
		}
		RigidBody* _RigidBody = mOwner->GetComp<RigidBody>();
		if (_RigidBody != nullptr)
		{
			mRigidBody = _RigidBody;
			Collsys->AddCollider(this);
		}
		else
		{
			mOwner->RemoveComp(this);
			ConsoleSys->DebugPrint("Owner does not have rigidbody");
		}
	}

	Collsys->AddCollider(this);
}
void Collider::Update()
{
	collide_box.UpdateWorld();

	if (shape == CSHAPE_AABB && collide_box.GetWorldOrientation() != 0)
		shape = CSHAPE_OBB;

	if (SettingsSys->debug_draw)
		Draw();
}
void Collider::Shutdown()
{
	Collsys->RemoveCollider(this);
}

bool Collider::Edit()
{
	bool changed = false;

	ImGui::PushID("Collider");
	ImGui::Checkbox("IsIgnored", &isignored);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	unsigned index = shape / 2;
	const char* shapes[] = { "AABB", "CIRCLE", "OBB" };
	const char* label = shapes[index];
	CollisionShape values[] = { CSHAPE_AABB, CSHAPE_CIRCLE, CSHAPE_OBB };

	if (ImGui::BeginCombo("Shape", label, 0))
	{
		for (unsigned i = 0; i < IM_ARRAYSIZE(shapes); i++)
		{
			if (ImGui::Selectable(shapes[i], index == i))
			{
				index = i;
				shape = values[index];
				changed = true;
			}
			if (index == i)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}

	glm::vec3 temp_vec;

	//Position
	temp_vec = collide_box.GetPosition();
	ImGui::Text("Position");
	ImGui::PushID("Position");
	ImGui::Columns(3, "Position", true);

	changed |= ImGui::DragFloat("X", &temp_vec.x);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	changed |= ImGui::DragFloat("Y", &temp_vec.y);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::NextColumn();
	changed |= ImGui::DragFloat("Z", &temp_vec.z);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	collide_box.SetPosition(temp_vec);

	ImGui::Columns(1);
	ImGui::PopID();
	ImGui::Separator();

	// Scale
	temp_vec = collide_box.GetScale();
	ImGui::Text("Scale");
	ImGui::PushID("Scale");
	ImGui::Columns(2, "Scale", true);
	changed |= ImGui::DragFloat("X", &temp_vec.x, 1.0f, 0.0f, 100000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;


	ImGui::NextColumn();
	changed |= ImGui::DragFloat("Y", &temp_vec.y, 1.0f, 0.0f, 100000.0f);
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	collide_box.SetScale(temp_vec);

	ImGui::NextColumn();
	ImGui::Columns(1);
	ImGui::PopID();

	ImGui::Separator();

	static int objtypeIndex = (int)std::distance(CollTable->CollisionTypesVector.begin(), CollTable->CollisionTypesVector.find(type_of_object));
	static std::string active_str_objtype = CollTable->CollisionTypesVector.find(type_of_object)->second;


	auto it = CollTable->CollisionTypesVector.begin();

	if (ImGui::BeginCombo("Type of object", active_str_objtype.c_str(), 0))
	{
		for (unsigned i = 0; i < CollTable->CollisionTypesVector.size(); it++, i++)
		{
			if (ImGui::Selectable(it->second.c_str(), objtypeIndex == i))
			{
				objtypeIndex = i;
				active_str_objtype = it->second.c_str();
				type_of_object = it->first;
				changed = true;
			}
			if (objtypeIndex == i)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}


	// Rotation
	static float orientation = collide_box.GetOrientation();
	changed |= ImGui::SliderFloat("Rotation", &orientation, 0.0f, 6.28f, "%.3f");
	collide_box.SetOrientation(orientation);
	if (shape == CSHAPE_AABB && collide_box.GetWorldOrientation() != 0)
		shape = CSHAPE_OBB;
	else if(collide_box.GetWorldOrientation() == 0)
		shape = CSHAPE_AABB;
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	ImGui::PopID();
	if (ObjectEdition::editing_gizmo == ObjectEdition::None && (ObjectEdition::prev_editing_gizmo == ObjectEdition::None || ObjectEdition::prev_editing_gizmo == ObjectEdition::Collider))
	{
		ObjectEdition::editing_gizmo = ObjectEdition::Collider;
		moving |= ObjectEdition::EditSelectedObject(&collide_box);
		if (moving && InputSys->mouse_is_released(mouse_buttons::Right_click))
		{
			changed = true;
			moving = false;
		}
	}

	
	return changed;
}


void Collider::Draw()
{
	//First get the four vertices of the collide box
	Transform2D rect = collide_box.GetWorldTransform();
	glm::vec2 boxvertices[4];
	glm::vec2 vertex;
	glm::mat3 vertexrot = glm::rotate(glm::mat3(1.0), rect.mOrientation);



	//Right-Top
	vertex.x = (rect.mScale.x / 2);
	vertex.y = (rect.mScale.y / 2);
	vertex = vertexrot * glm::vec3(vertex.x, vertex.y, 0);
	vertex = vertex + glm::vec2(rect.mPosition.x, rect.mPosition.y);
	boxvertices[0] = vertex;


	//Left-Top
	vertex.x = -(rect.mScale.x / 2);
	vertex.y = (rect.mScale.y / 2);
	vertex = vertexrot * glm::vec3(vertex.x, vertex.y, 0);
	vertex = vertex + glm::vec2(rect.mPosition.x, rect.mPosition.y);
	boxvertices[1] = vertex;


	//Left-Bottom
	vertex.x = -(rect.mScale.x / 2);
	vertex.y = -(rect.mScale.y / 2);
	vertex = vertexrot * glm::vec3(vertex.x, vertex.y, 0);
	vertex = vertex + glm::vec2(rect.mPosition.x, rect.mPosition.y);
	boxvertices[2] = vertex;



	//Right-Bottom
	vertex.x = (rect.mScale.x / 2);
	vertex.y = -(rect.mScale.y / 2);
	vertex = vertexrot * glm::vec3(vertex.x, vertex.y, 0);
	vertex = vertex + glm::vec2(rect.mPosition.x, rect.mPosition.y);
	boxvertices[3] = vertex;


	if (shape == CSHAPE_OBB || shape == CSHAPE_AABB)
	{
		if (GetOwner()->Enabled())
		{
			for (int i = 0; i < 4; i++)
			{
				if (i != 3)
					mOwner->GetParentSpace()->DrawLine(boxvertices[i], boxvertices[i + 1], Colors::red);
				else
					mOwner->GetParentSpace()->DrawLine(boxvertices[3], boxvertices[0], Colors::red);
			}
		}
		else
		{
			for (int i = 0; i < 4; i++)
			{
				if (i != 3)
					mOwner->GetParentSpace()->DrawLine(boxvertices[i], boxvertices[i + 1], Colors::blue);
				else
					mOwner->GetParentSpace()->DrawLine(boxvertices[3], boxvertices[0], Colors::blue);
			}
		}
	}
	else
	{
		if (GetOwner()->Enabled())
			mOwner->GetParentSpace()->DrawCircle(collide_box.GetWorldPosition(), collide_box.GetWorldScale().x, Colors::red);
		else
			mOwner->GetParentSpace()->DrawCircle(collide_box.GetWorldPosition(), collide_box.GetWorldScale().x, Colors::blue);
	}
}

void Collider::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
	using nlohmann::json;

	j["Shape"] = shape;
	j["isignored"] = isignored;
	j["DrawInGame"] = DrawInGame;


	json& temp_trans = j["local_transform"];

	temp_trans["Position"].push_back(collide_box.local_transform.mPosition.x);
	temp_trans["Position"].push_back(collide_box.local_transform.mPosition.y);
	temp_trans["Position"].push_back(collide_box.local_transform.mPosition.z);

	temp_trans["Scale"].push_back(collide_box.local_transform.mScale.x);
	temp_trans["Scale"].push_back(collide_box.local_transform.mScale.y);

	temp_trans["Orientation"] = collide_box.local_transform.mOrientation;


	json& temp_trans2 = j["world_transform"];

	temp_trans2["Position"].push_back(collide_box.world_transform.mPosition.x);
	temp_trans2["Position"].push_back(collide_box.world_transform.mPosition.y);
	temp_trans2["Position"].push_back(collide_box.world_transform.mPosition.z);

	temp_trans2["Scale"].push_back(collide_box.world_transform.mScale.x);
	temp_trans2["Scale"].push_back(collide_box.world_transform.mScale.y);

	temp_trans2["Orientation"] = collide_box.world_transform.mOrientation;
	j["TypeOfObject"] = type_of_object;

}
void Collider::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
	using nlohmann::json;

	if (j.find("Shape") != j.end())
		shape = j["Shape"];
	if (j.find("isignored") != j.end())
		isignored = j["isignored"];
	if (j.find("DrawInGame") != j.end())
		DrawInGame = j["DrawInGame"];


	if (j.find("local_transform") != j.end())
	{
		if (j["local_transform"].find("Position") != j["local_transform"].end() && j["local_transform"]["Position"].size() >= 3)
		{
			collide_box.local_transform.mPosition.x = j["local_transform"]["Position"][0];
			collide_box.local_transform.mPosition.y = j["local_transform"]["Position"][1];
			collide_box.local_transform.mPosition.z = j["local_transform"]["Position"][2];
		}

		if (j["local_transform"].find("Scale") != j["local_transform"].end() && j["local_transform"]["Scale"].size() >= 2)
		{
			collide_box.local_transform.mScale.x = j["local_transform"]["Scale"][0];
			collide_box.local_transform.mScale.y = j["local_transform"]["Scale"][1];
		}

		if (j["local_transform"].find("Orientation") != j["local_transform"].end())
			collide_box.local_transform.mOrientation = j["local_transform"]["Orientation"];
	}


	if (j.find("world_transform") != j.end())
	{
		if (j["world_transform"].find("Position") != j["world_transform"].end() && j["world_transform"]["Position"].size() >= 3)
		{
			collide_box.world_transform.mPosition.x = j["world_transform"]["Position"][0];
			collide_box.world_transform.mPosition.y = j["world_transform"]["Position"][1];
			collide_box.world_transform.mPosition.z = j["world_transform"]["Position"][2];
		}

		if (j["world_transform"].find("Scale") != j["world_transform"].end() && j["world_transform"]["Scale"].size() >= 2)
		{
			collide_box.world_transform.mScale.x = j["world_transform"]["Scale"][0];
			collide_box.world_transform.mScale.y = j["world_transform"]["Scale"][1];
		}

		if (j["world_transform"].find("Orientation") != j["world_transform"].end())
			collide_box.world_transform.mOrientation = j["world_transform"]["Orientation"];
	}


	if (j.find("TypeOfObject") != j.end())
	{
		type_of_object = j["TypeOfObject"];
	}


}

void Collider::SetShape(CollisionShape _shape)
{
	shape = _shape;
}
void Collider::SetIsIgnored(bool _isignored)
{
	isignored = _isignored;
}

TransformComponent& Collider::GetCollideBox()
{
	return collide_box;
}
CollisionShape Collider::GetShape()
{
	return shape;
}
RigidBody* Collider::GetRigidBody()
{
	return mRigidBody;
}
bool Collider::GetIsIgnored()
{
	return isignored;
}

void Collider::OnCollisionGroupUpdated(const CollisionGroupUpdated& _event)
{
	if (_event.GetCollisionType() == type_of_object)
		type_of_object = CollTable->CollisionTypesVector.begin()->first;
}







