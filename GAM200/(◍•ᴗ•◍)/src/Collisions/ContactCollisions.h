/**********************************************************************************/
/*!
\file   ContactCollisions.h
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that check the collision between circles, abb and obbs
*/
/*********************************************************************************/
#pragma once

#include <algorithm>

#include <glm/glm.hpp>

class Transform2D;
struct Polygon2D;
class RigidBody;
class GameObject;

struct Contact
{
	GameObject* rb1;
	GameObject* rb2;
	glm::vec3	mPi;
	glm::vec3	mNormal;
	float	mPenetration;

	bool operator==(const Contact& rhs);

};

bool StaticCircleToStaticCircleEx(const glm::vec3& Center1, float Radius1, const glm::vec3& Center2, float Radius2, Contact * pResult);

bool StaticRectToStaticCircleEx(const glm::vec3& Rect, float Width, float Height, const glm::vec3& Center, float Radius, Contact* pResult);

bool StaticOBBToStaticCircleEx(Transform2D * OBB, const glm::vec3& Center, float Radius, Contact * pResult);

bool StaticRectToStaticRectEx(const glm::vec3 &pos1, const glm::vec3&size1, const glm::vec3&pos2, const glm::vec3&size2, Contact * pResult);

bool OrientedRectToOrientedRectEx(Transform2D * OBB1, Transform2D * OBB2, Contact * pResult);

bool PolygonToPolygon(Polygon2D * p1, Transform2D * tr1, Polygon2D * p2, Transform2D * tr2, Contact * pResult);

