/**********************************************************************************/
/*!
\file   ICollider.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions of the Collision component
*/
/*********************************************************************************/
#pragma once
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../Transform2D/TransformComponent.h"

class RigidBody;
class CollisionGroupUpdated;
enum CollisionShape
{
	CSHAPE_AABB = 1,	// 001
	CSHAPE_CIRCLE = 2,	// 010
	CSHAPE_OBB = 4,	// 100
	CSHAPE_INDEX_MAX = (CSHAPE_OBB | CSHAPE_CIRCLE) + 1
};

class Collider : public IComp 
{
	friend class Editor;
	SM_RTTI_DECL(Collider,IComp);
public:
	Collider();
	Collider(Collider& rhs);
	~Collider();
	Collider& operator=(Collider& rhs);
	Collider* Clone()override;

	void OnCreate()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	bool Edit()override;

	void Draw();

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void SetShape(CollisionShape _shape);
	void SetIsIgnored(bool _isignored);

	TransformComponent& GetCollideBox();
	CollisionShape GetShape();
	RigidBody* GetRigidBody();
	bool GetIsIgnored();

	void OnCollisionGroupUpdated(const CollisionGroupUpdated& _event);
	int type_of_object;


	bool DrawInGame = false;
private:
	RigidBody* mRigidBody;
	TransformComponent collide_box;
	CollisionShape shape = CSHAPE_AABB;
	bool isignored = false;
	bool moving = false;
};



