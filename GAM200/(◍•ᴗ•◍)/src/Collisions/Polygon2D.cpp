/**********************************************************************************/
/*!
\file   Polygon2D.cpp
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions declarations of Polygon2D.h
*/
/*********************************************************************************/
#include <glm\gtx\projection.hpp>
#include <glm\gtx\matrix_transform_2d.hpp>

#include "Polygon2D.h"




//! @PROVIDED
//	\brief	Draws the polygon with the provided color and transform.
//
void Polygon2D::Draw(unsigned long color, glm::mat3* vtx_transform) const
{

	std::cout << "THE DRAW FUNCTION HAS TO BE IMPLEMENTED\n";
	/*// can't draw polygon if we don't have at least
	// 2 vertices
	if (mVertices.size() <= 1)
		return;

	// set transform if necessary transform 
	if (vtx_transform != NULL) {
		glm::mat3 mat = *vtx_transform;
		AEGfxSetTransform(&mat);
	}

	// Draw line between each vertices
	for (u32 i = 0; i < mVertices.size() - 1; ++i)
	{
		auto & v1 = mVertices[i];
		auto & v2 = mVertices[i + 1];
		AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color);
	}
	// Draw last line from last to first vertex
	auto & v1 = mVertices[0];
	auto & v2 = mVertices[mVertices.size() - 1];
	AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color);

	// force draw on graphics system
	AEGfxFlush();*/
}


// ------------------------------------------------------------------------
// STATIC INTERFACE
// ------------------------------------------------------------------------
//! @PROVIDED
//	\brief	Creates a quad polygon.
//
Polygon2D Polygon2D::MakeQuad()
{
	// result polygon
	Polygon2D res(4);

	// create vertices
	res[0] = { -0.5f, 0.5f , 0.0f };
	res[1] = { -0.5f, -0.5f , 0.0f };
	res[2] = { 0.5f, -0.5f , 0.0f };
	res[3] = { 0.5f, 0.5f , 0.0f };

	return res;
}

//! @PROVIDED
//	\brief	Creates a 5-sided polygon.
//
Polygon2D Polygon2D::MakePentagon()
{
	return MakeStandardPoly(5);
}

//! @PROVIDED
//	\brief	Creates a 6-sided polygon.
//
Polygon2D Polygon2D::MakeHexagon()
{
	return MakeStandardPoly(6);
}

//! @PROVIDED
//	\brief	Creates a 7-sided polygon.
//
Polygon2D Polygon2D::MakeSeptagon()
{
	return MakeStandardPoly(7);
}

//! @PROVIDED
//	\brief	Creates a 8-sided polygon.
//
Polygon2D Polygon2D::MakeOctagon()
{
	return MakeStandardPoly(8);
}

//! @PROVIDED
//	\brief	Creates a n-sided polygon.
//
Polygon2D Polygon2D::MakeStandardPoly(unsigned long side)
{
	Polygon2D res(side);
	res[0] = { 0.5f, 0.0f, 0.0f};
	float alpha = (2.0f*3.1415926f) / (float)side;
	for (unsigned long i = 1; i < side; ++i) {

		// set current point to previous
		// before rotation
		res[i] = res[i - 1];
		
		// compute rotation vars
		float tmp = res[i].x;
		float cA = cosf(alpha);
		float sA = sinf(alpha);

		// apply rotation to get new point
		res[i].x = cA * res[i].x - sA * res[i].y;
		res[i].y = sA * tmp + cA * res[i].y;
	}
	return res;
}


//!	@TODO
//	\brief	Default constructor taking the size of the polygon.
//	\remark	Does nothing.
//
Polygon2D::Polygon2D()
{
}

//!	@TODO
//	\brief	Custom constructor taking the size of the polygon.
//	\remark	This function should call the SetSize method below.
//
Polygon2D::Polygon2D(unsigned long size)
{
	//Call setsize with the actual size
	SetSize(size);

}

// ------------------------------------------------------------------------
// Vertex Management 

//	@TODO
//\brief	Adds a vertex to the polygon.
//\remark	The vertex should be added at the end of the polygon. Use the
//		push_back() method of std::vector.
//
void		 Polygon2D::AddVertex(const glm::vec3 & vtx)
{
	//Add the vertex to the vertex list
	mVertices.push_back(vtx);

}

//!	@TODO
//	\brief	returns the vertex at the index specifed
//	\remark	This function shouldn't have to do any sanity check as it returns by
//			reference. If a erroneous index is passed. An error will be thrown.
//
glm::vec3 &	 Polygon2D::operator[](unsigned long idx)
{
	//Create a counter
	int counter = 0;
	
	//Create an iterator of the vertex list
	std::vector<glm::vec3>::iterator it = mVertices.begin();
	
	//Do a loop to iterate through the vertices
	for (; it != mVertices.end(); it++)
	{
		//Execute if the counter is equal to the index
		if (counter == idx)
		{
			//Exit the loop
			break;

		}
		//Add 1 to counter
		counter++;
	}

	//Return the iterator
	return *it;
}

//!	@TODO
//	\brief	Removes all vertices in the polygon
//	\remark	Use the clear method of std::vector. No need for more.
//
void		 Polygon2D::Clear()
{
	
	//Clear the vertices list
	mVertices.clear();
}


//!	@TODO
//	\brief	Returns the size of the mVertices array
//	\remark	Use the size method of std::vector. No need for more.
//
unsigned long			 Polygon2D::GetSize() const
{
	//Get the number of vertices
	return (unsigned long)mVertices.size();
}

//! @TODO
//	\brief	Sets the size of the mVertices array
//	\remark	Use the resize method of std::vector. No need for more.
//
void		 Polygon2D::SetSize(unsigned long size)
{
	//Change the size of the list
	mVertices.resize(size);

}

// @TODO
//\brief	Returns a copy of the vertices transformed by the provided matrix
//
std::vector<glm::vec3>  Polygon2D::GetTransformedVertices(const glm::mat3 &mat_transform) const
{
	//Create a copy of the vertices list
	std::vector<glm::vec3> newvertices = mVertices;
	
	//Iterate through the vertices
	for (std::vector<glm::vec3>::iterator it = newvertices.begin(); it != newvertices.end(); it++)
	{
		//Apply the transformation to the respective vertex
		*it = mat_transform * *it;

	}

	//Return the transformed vertex list
	return newvertices;
}





