/**********************************************************************************/
/*!
\file   CollisionSystem.h
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the implementation of resolving contact velocity and penetration
between bodies (along other provided functions regarding the CollisionSystem class)
*/
/*********************************************************************************/
#pragma once
#include <list>
#include <mutex>
#include <limits>	// UINT_MAX
#include <cfloat>	// FLT_MAX

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"

#include "ICollider.h"
#include "ContactCollisions.h"

class RigidBody;
class Collider;

bool CollideCircles(Collider* body1, Collider* body2, Contact * c);
bool CollideAABBs(Collider* body1, Collider* body2, Contact * c);
bool CollideOBBs(Collider* body1, Collider* body2, Contact * c);
bool CollideAABBToCircle(Collider* body1, Collider* body2, Contact * c);
bool CollideOBBToCircle(Collider* body1, Collider* body2, Contact * c);
// typedef for function pointer CollisionFn
typedef bool(*CollisionFn)(Collider*, Collider*, Contact *);


class CollisionSystem : public IBase
{
	SM_RTTI_DECL(CollisionSystem, IBase);
	Make_Singleton(CollisionSystem)

public:

	bool Initialize();
	void Update();
	void LateUpdate();
	void Shutdown();

	void AddCollider(Collider* obj);
	void RemoveCollider(Collider* obj);
	void ClearColliders(void);

	CollisionFn GetCollisionFn(Collider* b1, Collider* b2);
	std::vector<Contact>& GetPreviousContacts();
	std::vector<Contact>& GetCurrentContacts();

	void CollideAllBodies();
private:

	void GetActiveColliders();

	std::map<unsigned, std::vector<Collider*>*> mAllColliders;
	std::vector<Collider*> mDynamicColliders;
	std::vector<Collider*> mStaticColliders;
	std::vector<Collider*> mActiveColliders;


	std::vector<Contact> mPreviousContacts;
	std::vector<Contact> mCurrentContacts;

	std::mutex	collLock;

	unsigned int mCollisionIterations = 10;
	unsigned int mCollisionsThisFrame = 0;
	unsigned mObjectsThisFrame = 0;
	unsigned mMaxDynamic = 0;
	unsigned mMaxStatic = 0;
	unsigned mMaxCollisions = 0;
	unsigned mMaxObjects = 0;

	CollisionFn mCollisionTests[CSHAPE_INDEX_MAX] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
};
#define Collsys CollisionSystem::Get()