/**********************************************************************************/
/*!
\file   Collisions.cpp
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that detect if a point is inside another polygon
*/
/*********************************************************************************/

#include <glm\gtx\projection.hpp>
#include "Collisions.h"


bool StaticPointToStaticCircle(const glm::vec3&P, const glm::vec3&Center, float Radius)
{
	// Get the distance between the point and the circle center
	float _Distance = static_cast<float>(glm::distance(glm::vec2(Center), glm::vec2(P)));

	_Distance *= _Distance;

	// If the squared distance is smaller than the squared radius they are colliding
	return (_Distance <= Radius * Radius);
}
bool StaticPointToStaticRect(const glm::vec3 &Pos, const glm::vec3& Rect, float Width, float Height)
{
	//Create a new variable
	glm::vec2 _DeltaVector{ Rect.x - Pos.x, Rect.y - Pos.y };

	//If the vector is inside the limits of the rectangle collide
	return (glm::abs(_DeltaVector.x) <= Width / 2 && glm::abs(_DeltaVector.y) <= Height / 2);	 
}
bool StaticPointToOrientedRect(const glm::vec3&Pos, const glm::vec3&Rect, float Width, float Height, float AngleRad)
{
	//Create new variables
	glm::vec2 distancevector{ Rect.x - Pos.x , Rect.y - Pos.y };

	//Store in vectors the inverse transformation rotation of the OBB
	glm::vec2 anglex = { cos(AngleRad), sin(AngleRad) };
	glm::vec2 angley = { -sin(AngleRad), cos(AngleRad) };

	glm::vec2 u{ glm::proj(distancevector, anglex) };
	glm::vec2 w{ glm::proj(distancevector, angley) };

	float U_Length = glm::length(u);
	U_Length *= U_Length;
	float W_Length = glm::length(w);
	W_Length *= W_Length;

	return (U_Length <= (Width * Width) / 4 && W_Length <= (Height * Height) / 4);
}

bool StaticRectToStaticRect(const glm::vec3& Rect0, float Width0, float Height0, const glm::vec3 &Rect1, float Width1, float Height1)
{
	//Check the collision of the two rectangles as if one of them has the newwidth and the newheigth
	return StaticPointToStaticRect(Rect0, Rect1, Width1 + Width0, Height0 + Height1);

}
bool StaticRectToStaticCirlce(const glm::vec3& Rect, float Width, float Height, const glm::vec3 & Center, float Radius)
{
	

	glm::vec3 clamped_point{ glm::clamp(Center.x, Rect.x - Width / 2, Rect.x + Width / 2),
							 glm::clamp(Center.y, Rect.y - Height / 2, Rect.y + Height / 2), 0.0f };

	//Check the collision between the clamped point and the circle
	return StaticPointToStaticCircle(clamped_point, Center, Radius);
}

bool OrientedRectToStaticCircle(const glm::vec3& Rect, float Width, float Height, float AngleRad, const glm::vec3& Center, float Radius)
{
	//Create new variables
	glm::vec2 _Delta{ Center.x - Rect.x , Center.y - Rect.y };

	glm::vec3 new_circle{ cos(AngleRad) * _Delta.x + sin(AngleRad) * _Delta.y,
						 -sin(AngleRad) * _Delta.x + cos(AngleRad) * _Delta.y, 0.0f };
	
	//Check if the circle and the OBB collide as a circle and an OBB
	return StaticRectToStaticCirlce(glm::vec3{0.0f, 0.0f, 0.0f}, Width, Height, new_circle, Radius);
}
bool StaticCircleToStaticCircle(const glm::vec3& Center0, float Radius0, const glm::vec3& Center1, float Radius1)
{
	//Store the distance between radii
	float radius_distance = (Radius0 + Radius1) * (Radius0 + Radius1);

	//Create a new vector
	glm::vec2 center_distance{ Center0.x - Center1.x ,
							   Center0.y - Center1.y };

	float _SquaredDistance = glm::length(center_distance);

	return ((_SquaredDistance * _SquaredDistance) <= radius_distance);
}