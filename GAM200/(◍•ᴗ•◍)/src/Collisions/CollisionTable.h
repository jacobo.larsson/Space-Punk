#pragma once

#include <string>
#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../Singleton/Singleton.h"



enum CollisionType
{
	Default_type = 1,	// 00001
	Wall_type = 2,	// 00010
	Floor_type = 4,	// 00100
	Flying_enemy_type = 8, //01000
	Walking_enemy_type = 16, // 10000
	Player_type = 32, // 100000
};


enum CollisionPolicies
{
	Ignore =1,
	DetectNotResolve =2,
	Resolve=3

};

class CollisionTable : public IBase
{
	SM_RTTI_DECL(CollisionTable, IBase);
	Make_Singleton_Decl(CollisionTable)

public:
	bool Initialize();
	void Shutdown();
	CollisionPolicies resolvetable(const int& b1,const int& b2);
	void CreateNewCollisionGroup(const std::string& to_add);
	void RemoveCollisionGroup(const std::string& to_removed);
	std::string GetCollisionGroupNameByNumber(const int& group_number);
	int GetCollisionGroupNumberByName(const std::string& group_name);
	void EditCollisionTable(int group_1, int group_2, CollisionPolicies policy);
	std::map<int, CollisionPolicies> mCollisionTableTests;
	std::map<int, std::string> CollisionTypesVector;

	void Save();
private:
	void Load();
	
	int previous_binary_number;
};

#define CollTable CollisionTable::Get()