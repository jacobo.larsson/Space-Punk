/**********************************************************************************/
/*!
\file   ContactCollisions.cpp
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions declarations of ContactCollisions.h
*/
/*********************************************************************************/

#define GLM_FORCE_RADIANS

#include <array>
#include <glm/glm.hpp>
#include <glm\gtx\projection.hpp>
#include <glm\gtx\perpendicular.hpp>
#include <glm\gtx\matrix_transform_2d.hpp>

#include "..\Transform2D\Transform2D.h"

#include "Collisions.h"
#include "Polygon2D.h"

#include "ContactCollisions.h"

bool Contact::operator==(const Contact& rhs)
{
	return ((rb1 == rhs.rb1 || rb1 == rhs.rb2) && (rb2 == rhs.rb1 || rb2 == rhs.rb2));
}
bool StaticCircleToStaticCircleEx(const glm::vec3& Center1, float Radius1, const glm::vec3& Center2, float Radius2, Contact * pResult)
{
	//Execute if pResult is valid
	if (StaticCircleToStaticCircle(Center1, Radius1, Center2, Radius2))
	{
		if (pResult != nullptr)
		{//Create a vector to store the normal
			glm::vec3 normal{ Center2.x - Center1.x , Center2.y - Center1.y , 0 };

			//Normalize the normal
			pResult->mNormal = glm::normalize(normal);

			//Store the _Penetration as the sum of the radii minus the length of the distance between centers;
			pResult->mPenetration = Radius1 + Radius2 - glm::length(normal);

			//Store the contact point as the center of body 1 plus the normal times the radius of body 1
			pResult->mPi.x = Center1.x + pResult->mNormal.x * Radius1;
			pResult->mPi.y = Center1.y + pResult->mNormal.y * Radius1;
			pResult->mPi.z = 0;
		}
		return true;
	}
	return false;
}

bool StaticRectToStaticCircleEx(const glm::vec3 & Rect, float Width, float Height, const glm::vec3& Center, float Radius, Contact * pResult)
{
	if (StaticRectToStaticCirlce(Rect, Width, Height, Center, Radius))
	{
		if (pResult != nullptr)
		{
			//Create a vector from the AABB center to the circle center (we will do the absolute value of this)
			glm::vec3 _AbsRectToCircle{ glm::abs(Center.x - Rect.x), glm::abs(Center.y - Rect.y), 0 };

			//Create a vector from the AABB center to the circle center (we will not do the absolute value of this)
			glm::vec3 _RectToCircle = Center - Rect;

			//Execute if contained is true
			if (StaticPointToStaticRect(Center, Rect, Width, Height))
			{
				float _XPenetration = Width / 2 - _AbsRectToCircle.x;
				float _YPenetration = Height / 2 - _AbsRectToCircle.y;

				//Store the _Penetration as the minimun between the _XPenetration and _YPenetration and then add the radius
				pResult->mPenetration = std::min(_XPenetration, _YPenetration) + Radius;

				//Execute if _XPenetration is bigger than _YPenetration
				if (_XPenetration < _YPenetration)
				{
					pResult->mPenetration = _XPenetration + Radius;
					pResult->mNormal = glm::vec3(1, 0, 0);
				}
				else
				{
					pResult->mPenetration = _YPenetration + Radius;
					pResult->mNormal = glm::vec3(0, 1, 0);
				}

				//Execute if the x coordinate of _RectToCircle times the normal is less than 0
				if (glm::dot(_RectToCircle, pResult->mNormal) < 0)
					pResult->mNormal = -pResult->mNormal;

				//Store the intersection point as the center
				pResult->mPi = Center;
			}
			else
			{
				//Compute the distance bewteen the centers
				glm::vec3 distance_between_centers = Center - Rect;

				//Create a clamped point
				glm::vec3 clamped_point{ glm::clamp(Center.x, Rect.x - Width / 2, Rect.x + Width / 2),
										 glm::clamp(Center.y, Rect.y - Height / 2, Rect.y + Height / 2), 0 };

				//Store the normal as the center minus the clamped point in normalvector
				glm::vec3 normalvector{ Center.x - clamped_point.x, Center.y - clamped_point.y, 0 };

				//Normalize the normal to store it in mNormal
				pResult->mNormal = glm::normalize(normalvector);

				//Store in _Penetration the radius minus the normalvector length
				pResult->mPenetration = Radius - glm::length(normalvector);

				//Store in the contact point as the clamped point
				pResult->mPi = clamped_point;
			}
		}
		return true;
	}
	//Return true if they collide and false if not
	return false;
}

bool StaticOBBToStaticCircleEx(Transform2D * OBB, const glm::vec3& Center, float Radius, Contact * pResult)
{

	glm::mat3 _InvTransformation = glm::rotate(glm::mat3(1), -(OBB->mOrientation));

	//Store the center in a temporal vector
	glm::vec3 _TransformedCenter{ Center.x - OBB->mPosition.x, Center.y - OBB->mPosition.y, 0 };

	_TransformedCenter = _InvTransformation * _TransformedCenter;

	if (StaticRectToStaticCircleEx(glm::vec3{ 0, 0, 0 }, OBB->mScale.x, OBB->mScale.y, _TransformedCenter, Radius, pResult))
	{
		if (pResult != nullptr)
		{
			//Store in Transformation the  Transformation of the OBB
			glm::mat3 Transformation = glm::rotate(glm::mat3(1), OBB->mOrientation);

			//Apply to the normal the Transformation of the OBB
			pResult->mNormal = Transformation * pResult->mNormal;

			//Apply to the point of contact the Transformation of the OBB
			pResult->mPi = Transformation * pResult->mPi;
			pResult->mPi += OBB->mPosition;

		}
		return true;
	}
	return false;
}

bool StaticRectToStaticRectEx(const glm::vec3& pos1, const glm::vec3& size1, const glm::vec3& pos2, const glm::vec3& size2, Contact* pResult)
{
	bool _Collided = false;
	if (pResult != nullptr)
	{
		glm::vec3 _Distance{ pos2.x - pos1.x,
							 pos2.y - pos1.y, 0 };

		float _XPenetration = (size1.x + size2.x) / 2 - glm::abs(_Distance.x);
		float _YPenetration = (size1.y + size2.y) / 2 - glm::abs(_Distance.y);

		pResult->mPenetration = std::min(_XPenetration, _YPenetration);

		if (_XPenetration > _YPenetration)
			pResult->mNormal = glm::vec3{ 0, 1, 0 };
		else
			pResult->mNormal = glm::vec3{ 1, 0, 0 };

		if (glm::dot(pResult->mNormal, _Distance) < 0)
			pResult->mNormal = -pResult->mNormal;

		std::array<glm::vec3, 4> _Vertices1 = { glm::vec3{pos1.x - size1.x / 2, pos1.y + size1.y / 2, 0}, glm::vec3{pos1.x + size1.x / 2, pos1.y + size1.y / 2, 0},
												glm::vec3{pos1.x - size1.x / 2, pos1.y - size1.y / 2, 0}, glm::vec3{pos1.x + size1.x / 2, pos1.y - size1.y / 2, 0} };

		std::array<glm::vec3, 4> _Vertices2 = { glm::vec3{pos2.x - size2.x / 2, pos2.y + size2.y / 2, 0}, glm::vec3{pos2.x + size2.x / 2, pos2.y + size2.y / 2, 0},
												glm::vec3{pos2.x - size2.x / 2, pos2.y - size2.y / 2, 0}, glm::vec3{pos2.x + size2.x / 2, pos2.y - size2.y / 2, 0} };

		std::vector<glm::vec3*> _PossibleContacts;

		for (auto _Vertex : _Vertices2)
		{
			if (StaticPointToStaticRect(_Vertex, pos1, size1.x, size1.y))
			{
				_PossibleContacts.push_back(&_Vertex);
				pResult->mPi = _Vertex;
				_Collided = true;
			}
		}

		if (_Collided == true && _PossibleContacts.size() == 2)
		{
			pResult->mPi = *_PossibleContacts[0];

			if (_PossibleContacts[0]->x == _PossibleContacts[1]->x)
				pResult->mPi = glm::vec3(_PossibleContacts[0]->x, (_PossibleContacts[0]->y + _PossibleContacts[1]->y) / 2, 0);

			else if (_PossibleContacts[0]->y == _PossibleContacts[1]->y)
				pResult->mPi = glm::vec3((_PossibleContacts[0]->x + _PossibleContacts[1]->x) / 2, _PossibleContacts[0]->y, 0);
		}

		_PossibleContacts.clear();

		if (_Collided == false)
		{
			for (auto _Vertex : _Vertices1)
			{
				if (StaticPointToStaticRect(_Vertex, pos2, size2.x, size2.y))
				{
					_PossibleContacts.push_back(&_Vertex);
					pResult->mPi = _Vertex;
				}
			}
		}

		if (_Collided == false && _PossibleContacts.size() == 2)
		{
			pResult->mPi = *_PossibleContacts[0];

			if (_PossibleContacts[0]->x == _PossibleContacts[1]->x)
				pResult->mPi = glm::vec3(_PossibleContacts[0]->x, (_PossibleContacts[0]->y + _PossibleContacts[1]->y) / 2, 0);

			else if (_PossibleContacts[0]->y == _PossibleContacts[1]->y)
				pResult->mPi = glm::vec3((_PossibleContacts[0]->x + _PossibleContacts[1]->x) / 2, _PossibleContacts[0]->y, 0);
		}

		if (_Collided = StaticRectToStaticRect(pos1, size1.x, size1.y, pos2, size2.x, size2.y))
		{
			// set the point of intersection
			pResult->mPi = _Vertices1[0];
			return true;
		}
	}
	return _Collided;
}

bool OrientedRectToOrientedRectEx(Transform2D * OBB1, Transform2D * OBB2, Contact * pResult)
{
	glm::vec3 _Pos1 = OBB1->mPosition;
	glm::vec3 _Pos2 = OBB2->mPosition;

	glm::vec2 _Size1 = OBB1->mScale;
	glm::vec2 _Size2 = OBB2->mScale;

	std::array<glm::vec3, 4> _OBB1Vertices{ glm::vec3{-_Size1.x / 2, _Size1.y / 2, 0}, glm::vec3{_Size1.x / 2, _Size1.y / 2, 0},
										   glm::vec3{-_Size1.x / 2, -_Size1.y / 2, 0}, glm::vec3{_Size1.x / 2, -_Size1.y / 2, 0} };
	
	std::array<glm::vec3, 4> _OBB2Vertices{ glm::vec3{-_Size2.x / 2, _Size2.y / 2, 0}, glm::vec3{_Size2.x / 2, _Size2.y / 2, 0},
										   glm::vec3{-_Size2.x / 2, -_Size2.y / 2, 0}, glm::vec3{_Size2.x / 2, -_Size2.y / 2, 0} };
	
	//Create a Transformation of the rotation of the first OBB
	glm::mat3 _RotMat1 = glm::rotate(glm::mat3(1.0), OBB1->mOrientation);
	glm::mat3 _RotMat2 = glm::rotate(glm::mat3(1.0), OBB2->mOrientation);

	for (unsigned i = 0; i < 4; i++)
	{
		_OBB1Vertices[i] = _RotMat1 * _OBB1Vertices[i] + _Pos1;
		_OBB2Vertices[i] = _RotMat2 * _OBB2Vertices[i] + _Pos2;
	}

	//Create an array to store the axis of the first OBB
	std::array<glm::vec3, 2> _OBBAxis1{ _RotMat1 * glm::vec3{_Size1.x / 2, 0, 0} , _RotMat1 * glm::vec3{0, _Size1.y / 2, 0} };
	std::array<glm::vec3, 2> _OBBAxis2{ _RotMat2 * glm::vec3{_Size2.x / 2, 0, 0} , _RotMat2 * glm::vec3{0, _Size2.y / 2, 0} };

	std::array<glm::vec3, 2> _HalfAxis1{ _OBBAxis1[0] , _OBBAxis1[1] };
	std::array<glm::vec3, 2> _HalfAxis2{ _OBBAxis2[0] , _OBBAxis2[1] };

	//Compute the distance between the centers of the OBB
	glm::vec3 _Distance = _Pos2 - _Pos1;

	//Create a temporal _MaxValue that has value minus infinity
	glm::vec2 _DistanceProjection{ 0,0 };

	
	for (auto& _Axis : _OBBAxis1)
	{
		_Axis = glm::normalize(_Axis);
	}

	for (auto& _Axis : _OBBAxis2)
	{
		_Axis = glm::normalize(_Axis);
	}
	glm::vec3 _MinAxis{0, 0, 0};

	bool _Collided = false;
	float _Length = 0;
	float _MinPenetration = FLT_MAX;
	float _MaxValue = -INFINITY;
	float _TempMaxValue = 0.0f;

	float _HalfAxis1ProjX = 0.0f;
	float _HalfAxis1ProjY = 0.0f;
	float _HalfAxis2ProjX = 0.0f;
	float _HalfAxis2ProjY = 0.0f;
	float _DistanceProj = 0.0f;
	float _Penetration = 0.0f;

	for (auto & _Axis : _OBBAxis1)
	{
		_HalfAxis1ProjX = glm::length(glm::proj(_HalfAxis1[0], _Axis));
		_HalfAxis1ProjY = glm::length(glm::proj(_HalfAxis1[1], _Axis));

		_HalfAxis2ProjX = glm::length(glm::proj(_HalfAxis2[0], _Axis));
		_HalfAxis2ProjY = glm::length(glm::proj(_HalfAxis2[1], _Axis));

		_Length = _HalfAxis1ProjX + _HalfAxis1ProjY + _HalfAxis2ProjX + _HalfAxis2ProjY;

		_DistanceProj = glm::length(glm::proj(_Distance, _Axis));
		_DistanceProj = glm::abs(_DistanceProj);
		_Penetration = _DistanceProj - _Length;

		if (0 < _Penetration)
			return false;

		_TempMaxValue = _MaxValue;
		_MaxValue = glm::max(_MaxValue, _Penetration);

		//Execute if _MaxValue is not equal to _TempMaxValue
		if (_MaxValue != _TempMaxValue)
		{
			_MinAxis = _Axis;
			_MinPenetration = abs(_MaxValue);
		}

	}
	for (auto & _Axis : _OBBAxis2)
	{
		_HalfAxis1ProjX = glm::length(glm::proj(_HalfAxis1[0], _Axis));
		_HalfAxis1ProjY = glm::length(glm::proj(_HalfAxis1[1], _Axis));

		_HalfAxis2ProjX = glm::length(glm::proj(_HalfAxis2[0], _Axis));
		_HalfAxis2ProjY = glm::length(glm::proj(_HalfAxis2[1], _Axis));

		_Length = _HalfAxis1ProjX + _HalfAxis1ProjY + _HalfAxis2ProjX + _HalfAxis2ProjY;

		_DistanceProj = glm::length(glm::proj(_Distance, _Axis));
		_DistanceProj = glm::abs(_DistanceProj);
		_Penetration = _DistanceProj - _Length;

		if (0 < _Penetration)
			return false;
		
		_TempMaxValue = _MaxValue;
		_MaxValue = glm::max(_MaxValue, _Penetration);

		if (_MaxValue != _TempMaxValue)
		{
			_MinAxis = _Axis;
			_MinPenetration = abs(_MaxValue);
		}
	}
	if (pResult)
	{
		pResult->mNormal = _MinAxis;
		pResult->mPenetration = _MinPenetration;

		if (glm::dot( _Distance, pResult->mNormal)< 0)
			pResult->mNormal = -pResult->mNormal;


		std::vector<glm::vec3*> _PossibleContacts;

		for (auto & _Vertex : _OBB1Vertices)
		{
			if (StaticPointToOrientedRect(_Vertex, OBB2->mPosition, OBB2->mScale.x, OBB2->mScale.y, OBB2->mOrientation))
			{
				_PossibleContacts.push_back(&_Vertex);
				pResult->mPi = _Vertex;
				_Collided = true;
				break;
			}

		}
		if (_Collided == true && _PossibleContacts.size() == 2)
		{
			pResult->mPi = *_PossibleContacts[0];

			if (_PossibleContacts[0]->x == _PossibleContacts[1]->x)
			{
				glm::vec3 newpoint = glm::vec3(_PossibleContacts[0]->x, (_PossibleContacts[0]->y + _PossibleContacts[1]->y) / 2, 0);
				pResult->mPi = newpoint;
			}
			else if (_PossibleContacts[0]->y == _PossibleContacts[1]->y)
			{
				glm::vec3 newpoint = glm::vec3((_PossibleContacts[0]->x + _PossibleContacts[1]->x) / 2, _PossibleContacts[0]->y, 0);
				pResult->mPi = newpoint;
			}
		}

		_PossibleContacts.clear();

		if (_Collided == false)
		{
			for (auto & _Vertex : _OBB2Vertices)
			{
				if (StaticPointToOrientedRect(_Vertex, OBB1->mPosition, OBB1->mScale.x, OBB1->mScale.y, OBB1->mOrientation))
				{
					_PossibleContacts.push_back(&_Vertex);
					pResult->mPi = _Vertex;
					break;
				}
			}
		}

		if (_Collided == false && _PossibleContacts.size() == 2)
		{
			pResult->mPi = *_PossibleContacts[0];

			if (_PossibleContacts[0]->x == _PossibleContacts[1]->x)
			{
				glm::vec3 newpoint = glm::vec3(_PossibleContacts[0]->x, (_PossibleContacts[0]->y + _PossibleContacts[1]->y) / 2, 0);
				pResult->mPi = newpoint;
			}
			else if (_PossibleContacts[0]->y == _PossibleContacts[1]->y)
			{
				glm::vec3 newpoint = glm::vec3((_PossibleContacts[0]->x + _PossibleContacts[1]->x) / 2, _PossibleContacts[0]->y, 0);
				pResult->mPi = newpoint;
			}
		}
	}

	return _Collided;
}

bool PolygonToPolygon(Polygon2D * p1, Transform2D * tr1, Polygon2D * p2, Transform2D * tr2, Contact * pResult)
{
	//Create a variable to store the _Penetration
	float penetration;
	
	//Create variables to store the minimun axis
	glm::vec3 minaxis;
	glm::vec3 minaxis2;
	
	//Create variables to store max/min values
	float temp = +INFINITY;
	float temp2 = +INFINITY;
	float min1 =+INFINITY;
	float max1 = -INFINITY;
	float min2 = +INFINITY;
	float max2= -INFINITY;
	
	//Boolean to know if it is the first time iterating
	bool firsttime = true;


	//Store the Transformation of the first body
	glm::mat3 world1 = glm::translate(glm::mat3(), glm::vec2(tr1->mPosition.x, tr1->mPosition.y)) * glm::rotate(glm::mat3(), tr1->mOrientation) *
					   glm::scale(glm::mat3(), glm::vec2(tr1->mScale.x, tr1->mScale.y));
	
	//Store the Transformation of the second body
	glm::mat3 world2 = glm::translate(glm::mat3(), glm::vec2(tr2->mPosition.x, tr2->mPosition.y)) * glm::rotate(glm::mat3(), tr2->mOrientation) *
					   glm::scale(glm::mat3(), glm::vec2(tr2->mScale.x, tr2->mScale.y));
	
	
	//Store the vertices of the first body
	std::vector<glm::vec3> firstbody = p1->GetTransformedVertices(world1);
	
	//Store the vertices of the first body
	std::vector<glm::vec3> secondbody = p2->GetTransformedVertices(world2);
	
	//Do a loop to iterate through the vertices of the first body
	for(std::vector<glm::vec3>::iterator it=firstbody.begin(); it != firstbody.end();it++)
	{
		//Create a new vector 
		glm::vec3 nextvertex;
		
		//Execute if first time is false
		if (firsttime == false)
		{
			//Store in newvertex the previous vertex
			nextvertex = *(it - 1);
		}
		//Else execute this
		else
		{
			//Store in newvertex the last vertex of the list
			nextvertex = firstbody.back();
			
			//Set first time to false
			firsttime = false;
		}
 
		//Store in current_vertex the respective vertex
		glm::vec3 current_vertex = *it;
		
		//Store in current_axis the respective axis
		glm::vec3 current_axis = current_vertex - nextvertex;
		
		//Store in current_axis the perpendicular of the axis
		current_axis = glm::vec3(-current_axis.y, current_axis.x, 0);
		
		//Normalize the axis
		current_axis = glm::normalize(current_axis);//NormalizeThis();

		//Do a loop to iterate through the first body vertices
		for (std::vector<glm::vec3>::iterator et = firstbody.begin(); et != firstbody.end(); et++)
		{
			//Store in dotp the dot product between the respective vertex and the axis
			float dotp = glm::dot(*et,current_axis);
			
			//Store in min1 the minimun between dotp and min1
			min1 = std::min(min1,dotp);
			
			//Store in max1 the minimun between dotp and max1
			max1 = std::max(max1,dotp);
	
		}
	
		//Do a loop to iterate through the second body vertices
		for (std::vector<glm::vec3>::iterator et = secondbody.begin(); et != secondbody.end(); et++)
		{
			//Store in dotp the dot product between the respective vertex and the axis
			float dotp = glm::dot(*et,current_axis);
			
			//Store in min2 the minimun between dotp and min2
			min2 = std::min(min2, dotp);
			
			//Store in max2 the minimun between dotp and max2
			max2 = std::max(max2, dotp);
	
		}
		
		//Execute if max1 is less than min2 or if max2 is less than min1
		if (max1 < min2 || max2 < min1)
		{
			//Return false (separating axis _Collided)
			return false;
		}

		//Execute if max1 is bigger than min2
		if (max1 > min2)
		{
			//Store in _Penetration the subtraction between max1 and min2
			penetration = max1 - min2;

		}
		//Else execute if max2 is bigger than min1
		else if (max2 > min1)
		{
			//Store in _Penetration the subtraction between max2 and min1
			penetration = max2 - min1;

		}

		//Store in tempax the _TransformedCenter value
		float tempmax = temp;

		//Store in _TransformedCenter the minimun value between _TransformedCenter and _Penetration
		temp = std::min(temp, penetration);
		
		//Execute if _TempMaxValue is not equal to _TransformedCenter
		if (tempmax != temp)
		{
			//Store in _MinAxis the current axis
			minaxis = current_axis;

		}

		//Set the max/min values to -INFINITY/INFINITY
		min1 = +INFINITY;
		max1 = -INFINITY;
		min2 = +INFINITY;
		max2 = -INFINITY;
		
	}

	//Set first time to true
	firsttime = true;
	
	//Do a loop to iterate through the vertices of the first body
	for (std::vector<glm::vec3>::iterator it = secondbody.begin(); it != secondbody.end(); it++)
	{
		//Create a new vector 
		glm::vec3 nextvertex;
		
		//Execute if first time is false
		if (firsttime == false)
		{
			//Store in newvertex the previous vertex
			nextvertex = *(it - 1);
		}
		else
		{
			//Store in newvertex the last vertex of the list
			nextvertex = secondbody.back();
			
			//Set first time to false
			firsttime = false;
		}

		//Store in current_vertex the respective vertex
		glm::vec3 current_vertex = *it;
		
		//Store in current_axis the respective axis
		glm::vec3 current_axis = current_vertex - nextvertex;
		
		//Store in current_axis the perpendicular of the axis
		current_axis = glm::vec3(-current_axis.y, current_axis.x, 0);
		
		//Normalize the axis
		current_axis = glm::normalize(current_axis);//NormalizeThis();
		
		//Do a loop to iterate through the first body vertices
		for (std::vector<glm::vec3>::iterator et = firstbody.begin(); et != firstbody.end(); et++)
		{
			//Store in dotp the dot product between the respective vertex and the axis
			float dotp = glm::dot(*et, current_axis);
			
			//Store in min1 the minimun between dotp and min1
			min1 = std::min(min1, dotp);
			
			//Store in max1 the maximun between dotp and max1
			max1 = std::max(max1, dotp);

		}

		//Do a loop to iterate through the second body vertices
		for (std::vector<glm::vec3>::iterator et = secondbody.begin(); et != secondbody.end(); et++)
		{
			float dotp = glm::dot(*et, current_axis);
			
			//Store in min2 the minimun between dotp and min2
			min2 = std::min(min2, dotp);
			
			//Store in max2 the maximun between dotp and max2
			max2 = std::max(max2, dotp);

		}

		if (max1 < min2 || max2 < min1)
		{
			//Return false (separating axis _Collided)
			return false;
		}

		//Execute if max1 is bigger than min2
		if (max1 > min2)
		{
			//Store in _Penetration the subtraction between max1 and min2
			penetration = max1 - min2;
		}
		else if (max2 > min1)
		{
			//Store in _Penetration the subtraction between max2 and min1
			penetration = max2 - min1;
		}
		
		//Store in tempax the _Origin value
		float tempmax = temp2;
		
		//Store in _Origin the minimun value between _Origin and _Penetration
		temp2 = std::min(temp2, penetration);
		
		//Execute if _TempMaxValue is not equal to _Origin
		if (tempmax != temp2)
		{
			//Store in _MinAxis the current axis
			minaxis2 = current_axis;
		}

		//Set the max/min values to -INFINITY/INFINITY
		min1 = +INFINITY;
		max1 = -INFINITY;
		min2 = +INFINITY;
		max2 = -INFINITY;
		
	}
	
	//Execute if pResult is valid
	if (pResult)
	{
		

		//Execute if _Origin is bigger than _TransformedCenter
		if (temp2 > temp)
		{
			//Store _Origin in mPenetration
			pResult->mPenetration = temp2;
			
			//Store minaxis2 in mNormal
			pResult->mNormal = minaxis2;
		}
		//Else execute this
		else
		{
			//Store _TransformedCenter in mPenetration
			pResult->mPenetration = temp;
			
			//Store minaxis1 in mNormal
			pResult->mNormal = minaxis;
		}


		//Execute if the normal times the distance between the bodies is bigger than 0
		if (glm::dot(pResult->mNormal, (tr2->mPosition - tr1->mPosition)) > 0)
		{
			//Do not change the normal
			pResult->mNormal = pResult->mNormal;

		}

		//Else execute this
		else
		{
			//Flip the normal
			pResult->mNormal = -pResult->mNormal;

		}
	}
		
	//Return true (they collide)
	return true;
}


