/**********************************************************************************/
/*!
\file   Collisions.h
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the collisions implementations
*/
/*********************************************************************************/
#pragma once

#include <iostream>

#include <glm/glm.hpp>

bool StaticPointToStaticCircle(const glm::vec3 &P, const glm::vec3&Center, float Radius);
bool StaticPointToStaticRect(const glm::vec3& Pos,const glm::vec3& rect, float Width, float Height);
bool StaticPointToOrientedRect(const glm::vec3&Pos, const glm::vec3&Rect, float Width, float Height, float AngleRad);

bool StaticRectToStaticRect(const glm::vec3 &Rect0, float Width0, float Height0, const glm::vec3&Rect1, float Width1, float Height1);
bool StaticRectToStaticCirlce(const glm::vec3& Rect, float Width, float Height, const glm::vec3& Center, float Radius);

bool OrientedRectToStaticCircle(const glm::vec3& Rect, float Width, float Height, float AngleRad, const glm::vec3& Center, float Radius);
bool StaticCircleToStaticCircle(const glm::vec3& Center0, float Radius0, const glm::vec3& Center1, float Radius1);


