#include "CollisionTable.h"
#include "../EventSystem/EventDispatcher.h"
#include "../EventSystem/Events/RigidbodyEvents/RigidbodyEvents.h"

CollisionTable* CollisionTable::instance = nullptr;

CollisionTable::CollisionTable():previous_binary_number(1)
{}

bool CollisionTable::Initialize()
{
	previous_binary_number = 1;
	Load();
	return true;
}

void CollisionTable::Shutdown()
{
	Save();
}

CollisionPolicies CollisionTable::resolvetable(const int& b1, const int& b2)
{
	return mCollisionTableTests.find(b1| b2)->second;
}

void CollisionTable::CreateNewCollisionGroup(const std::string& to_add)
{
	for (auto ut = CollisionTypesVector.begin(); ut != CollisionTypesVector.end(); ++ut)
		if (ut->second == to_add)
			return;

	auto it = CollisionTypesVector.begin();
	mCollisionTableTests[previous_binary_number | previous_binary_number] = Resolve;
	for (auto it = CollisionTypesVector.begin(); it != CollisionTypesVector.end();it++)
	{
		mCollisionTableTests[previous_binary_number| it->first] = Resolve;

	}
	CollisionTypesVector[previous_binary_number] = to_add;
	previous_binary_number *= 2;


}

void CollisionTable::RemoveCollisionGroup(const std::string& to_removed)
{
	int binary_to_remove = 0;

	if (to_removed == "Default_type")
		return;

	

	for (auto it = CollisionTypesVector.begin(); it != CollisionTypesVector.end(); ++it)
		if (it->second == to_removed)
			binary_to_remove= it->first;

	std::map<int, std::string>::iterator et = CollisionTypesVector.find(binary_to_remove);


	if (binary_to_remove != 0)
	{
		
		for (auto it = CollisionTypesVector.begin(); it != CollisionTypesVector.end(); it++)
		{
			auto ft = mCollisionTableTests.find(binary_to_remove | it->first);
			if (ft != mCollisionTableTests.end())
			{
				mCollisionTableTests.erase(ft);
			}

		}
	}
	

	if (binary_to_remove != 0)
	{
		bool first_time = true;
		auto gt = et;
		gt++;
		if (gt == CollisionTypesVector.end())
		{
			CollisionTypesVector.erase(et);
			EventSys->trigger_event(CollisionGroupUpdated(binary_to_remove));
			return;
		}
		if (et != CollisionTypesVector.end())
		{
			auto it = et;
			it++;
			for (; it != CollisionTypesVector.end();)
			{
				auto entry = CollisionTypesVector.find(it->first)->second;
				int bin = CollisionTypesVector.find(it->first)->first;
				auto mt = CollisionTypesVector.find(it->first);

				bool after = false;
				for (std::map<int, std::string>::iterator ot = CollisionTypesVector.begin(); ot != CollisionTypesVector.end(); ot++)
				{
					int test = bin | ot->first;
					auto ut = mCollisionTableTests.find(bin | ot->first);
					if (ot->first == bin)
					{
						mCollisionTableTests[bin / 2 | bin / 2] = ut->second;
						after = true;
						gt = ot;
						gt++;
						if (gt == CollisionTypesVector.end())
						{
							ut = mCollisionTableTests.find(bin  | bin );
							mCollisionTableTests.erase(ut);
						}
					}
					else if (ut != mCollisionTableTests.end())
					{
						
						
						
						if (after==false)
						{
							mCollisionTableTests[bin/2 | ot->first] = ut->second;
							
						}
						else
						{
							mCollisionTableTests[bin/2 | ot->first ] = ut->second;;
						}

					mCollisionTableTests.erase(ut);
					}

					
				}
				after = false;
				++it;
				CollisionTypesVector.erase(mt);
				auto pair = std::make_pair(bin/2, std::move(entry));
				//CollisionTypesVector.insert(const_cast<int&>(bin), std::move(entry));
				
				if (first_time == true)
				{
					CollisionTypesVector.erase(bin/2);
				}
				CollisionTypesVector.insert(pair);
				
			}

		}
		
	}

	EventSys->trigger_event(CollisionGroupUpdated(binary_to_remove));


	
}

std::string CollisionTable::GetCollisionGroupNameByNumber(const int& group_number)
{
	std::string to_return = std::string("NotFound");
	auto it = CollisionTypesVector.find(group_number);
	if (it != CollisionTypesVector.end())
	{
		to_return = it->second;

	}

	return to_return;
}

int CollisionTable::GetCollisionGroupNumberByName(const std::string& group_name)
{
	for (auto it = CollisionTypesVector.begin(); it != CollisionTypesVector.end(); ++it)
		if (it->second == group_name)
			return it->first;
	return -1;
}


void CollisionTable::EditCollisionTable(int group_1, int group_2, CollisionPolicies policy)
{

	mCollisionTableTests[group_1 | group_2] = policy;
}

//CollisionPolicies CollisionTable::GetCollisionPolicy(const int& group_1, const int& group_2)
//{
//	CollisionPolicies policy;
//	return CollisionPolicies();
//}

void CollisionTable::Load()
{
	FILE* fp;
	fopen_s(&fp, "./data/CollisionGroups.txt", "rt");
	if (fp)
	{
		std::string to_add;
		int number_to_store =0;
		while (!feof(fp))
		{
			
			char current_char = fgetc(fp);
			if (current_char == EOF)
			{
				previous_binary_number = 2 * number_to_store;
				break;
			}
			if (current_char != ' ' && current_char != '\n')
			{
				to_add += current_char;
			}
			else if (current_char == '\n')
			{

				CollisionTypesVector[number_to_store] = to_add;
				to_add.clear();
				
				
			}
			else
			{
				
				number_to_store = atoi(to_add.c_str());
				to_add.clear();
				
				
			}
		}
		fclose(fp);
		
	}
	else
	{
		std::cout << "Could not open Collision Table file for loading" << std::endl;
	}

	fopen_s(&fp, "./data/CollisionTable.txt", "rt");
	if (fp)
	{
		std::string to_add;
		int number_to_store = 0;
		while (!feof(fp))
		{

			char current_char = fgetc(fp);
			if (current_char == EOF)
				break;

			if (current_char != ' ' && current_char != '\n')
			{
				to_add += current_char;
			}
			else if (current_char == '\n')
			{

				mCollisionTableTests[number_to_store] = CollisionPolicies(atoi(to_add.c_str()));
				to_add.clear();

			}
			else
			{

				number_to_store = atoi(to_add.c_str());
				to_add.clear();


			}
		}
		fclose(fp);
	}
	else
	{
		std::cout << "Could not open Collision Table file for saving" << std::endl;
	}
}

void CollisionTable::Save()
{
	FILE* fp;
	fopen_s(&fp, "./data/CollisionGroups.txt", "wt");
	if (fp)
	{
		for (auto it = CollisionTypesVector.begin(); it != CollisionTypesVector.end(); it++)
		{
			std::string number = std::to_string(it->first);
			fputs(number.c_str(), fp);
			fputc(' ', fp);
			fputs(it->second.c_str(), fp);
			fputc('\n', fp);

		}
		fclose(fp);
	}
	else
	{
		std::cout << "Could not open Collision Table file for saving" << std::endl;
	}


	fopen_s(&fp, "./data/CollisionTable.txt", "wt");
	if (fp)
	{
		for (auto it = mCollisionTableTests.begin(); it != mCollisionTableTests.end(); it++)
		{
			std::string number = std::to_string(it->first);
			fputs(number.c_str(), fp);
			fputc(' ', fp);
			std::string collpolicy = std::to_string(it->second);
			fputs(collpolicy.c_str(), fp);
			fputc('\n', fp);
		}
		fclose(fp);
	}
	else
	{
		std::cout << "Could not open Collision Table file for saving" << std::endl;
	}

}
