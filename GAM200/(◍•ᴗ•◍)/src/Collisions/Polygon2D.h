/**********************************************************************************/
/*!
\file   Polygon2D.h
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that make polygons. (add vertices, get vertices)
*/
/*********************************************************************************/
#pragma once
#include <vector>
#include <cmath>
#include <iostream>

#include <glm/glm.hpp>

struct Polygon2D
{
private:
	// Vertices of the polygon
	std::vector<glm::vec3>	mVertices;	
public:

	//!	@TODO
	//	\brief	Default constructor taking the size of the polygon.
	//	\remark	Does nothing.
	//
	Polygon2D();

	//!	@TODO
	//	\brief	Custom constructor taking the size of the polygon.
	//	\remark	This function should call the SetSize method below.
	//
	Polygon2D(unsigned long size);

	// ------------------------------------------------------------------------
	// Vertex Management 

	//!	@TODO
	//	\brief	Adds a vertex to the polygon. 
	//	\remark	The vertex should be added at the end of the polygon. Use the
	//			push_back() method of std::vector.
	//
	void		AddVertex(const glm::vec3& vtx);

	//!	@TODO
	//	\brief	returns the vertex at the index specifed 
	//	\remark	This function shouldn't have to do any sanity check as it returns by
	//			reference. If a erroneous index is passed. An error will be thrown.
	//
	glm::vec3 &	operator[](unsigned long idx);

	//!	@TODO
	//	\brief	Removes all vertices in the polygon
	//	\remark	Use the clear method of std::vector. No need for more. 
	//
	void		Clear();

	
	//!	@TODO
	//	\brief	Returns the size of the mVertices array
	//	\remark	Use the size method of std::vector. No need for more. 
	//
	unsigned long			GetSize() const;

	//! @TODO
	//	\brief	Sets the size of the mVertices array
	//	\remark	Use the resize method of std::vector. No need for more.
	//
	void		SetSize(unsigned long size);

	//! @TODO
	//	\brief	Returns a copy of the vertices transformed by the provided matrix
	//
	std::vector<glm::vec3> GetTransformedVertices(const glm::mat3 &mat_transform) const;

	//! @PROVIDED
	//	\brief	Draws the polygon with the provided color and transform.
	//
	void Draw(unsigned long color, glm::mat3 * vtx_transform = NULL) const;

	// ------------------------------------------------------------------------
	// STATIC INTERFACE
	// ------------------------------------------------------------------------

	//! @PROVIDED
	//	\brief	Creates a quad polygon.
	//
	static Polygon2D MakeQuad();

	//! @PROVIDED
	//	\brief	Creates a 5-sided polygon.
	//
	static Polygon2D MakePentagon();

	//! @PROVIDED
	//	\brief	Creates a 6-sided polygon.
	//
	static Polygon2D MakeHexagon();

	// @PROVIDED
	//\brief	Creates a 7-sided polygon.
	//
	static Polygon2D MakeSeptagon();

	//! @PROVIDED
	//	\brief	Creates a 8-sided polygon.
	//
	static Polygon2D MakeOctagon();

	//! @PROVIDED
	//	\brief	Creates a n-sided polygon.
	//
	static Polygon2D MakeStandardPoly(unsigned long side);
};


