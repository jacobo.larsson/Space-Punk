/**********************************************************************************/
/*!
\file   CollisionSystem.cpp
\author Juan Pacheco
\par    email: pacheco.j@digipen.edu
\par    DigiPen login: pacheco.j
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the implementation of resolving contact velocity and penetration
between bodies (along other provided functions regarding the CollisionSystem class)
*/
/*********************************************************************************/

#include "../Space/Space.h"
#include "../Scene/Scene.h"
#include "../Physics/PhysicSystem.h"
#include "../ThreadPool/ThreadPool.h"
#include "../GameObject/GameObject.h"
#include "../Settings/Settings.h"

#include "ContactCollisions.h"

#include "../Physics/RigidBody.h"
#include "../Transform2D/TransformComponent.h"

#include "CollisionSystem.h"

// ----------------------------------------------------------------------------

CollisionSystem* CollisionSystem::instance = nullptr;

bool CollisionSystem::Initialize()
{
	// default
	mCollisionIterations = 3;

	for (unsigned int i = 0; i < CSHAPE_INDEX_MAX; ++i)
		mCollisionTests[i] = NULL;

	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_CIRCLE] = CollideCircles;
	mCollisionTests[CSHAPE_AABB | CSHAPE_AABB] = CollideAABBs;
	mCollisionTests[CSHAPE_OBB | CSHAPE_OBB] = CollideOBBs;
	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_AABB] = CollideAABBToCircle;
	mCollisionTests[CSHAPE_OBB | CSHAPE_AABB] = CollideOBBs;
	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_OBB] = CollideOBBToCircle;

	// Clear all the vectors containing colliders
	ClearColliders();

	// Get all the colliders of the scene
	for (auto& _Space : SceneSys->GetAllSpaces())
	{
		mAllColliders[_Space->GetUID()] = reinterpret_cast<std::vector<Collider*>*>(&_Space->GetComponents<Collider>());
	}
	std::for_each(mAllColliders.begin(), mAllColliders.end(), [&](auto& _Colliders)
	{
		std::for_each(_Colliders.second->begin(), _Colliders.second->end(), [&](auto& _Collider)
		{
			if (_Collider->GetRigidBody())
			{
				bool _Dynamic = _Collider->GetRigidBody()->InvMass != 0.0f;
				if (_Dynamic)
					mDynamicColliders.push_back(_Collider);
				else
					mStaticColliders.push_back(_Collider);
			}
		});
	});
	return true;
}
void CollisionSystem::Update()
{
	if(SettingsSys->SpacePartitioning)
		GetActiveColliders();
	else
	{
		std::for_each(mAllColliders.begin(), mAllColliders.end(), [&](auto& _SpaceColliders)
		{
			std::for_each(_SpaceColliders.second->begin(), _SpaceColliders.second->end(), [&](auto& _Collider)
			{
				if (_Collider->GetOwner()->Enabled() && _Collider->Enabled())
					_Collider->Update();
			}
			);
		});
	}
	CollideAllBodies();
}
void CollisionSystem::LateUpdate()
{
}
void CollisionSystem::Shutdown()
{
	mAllColliders.clear();
	mDynamicColliders.clear();
	mStaticColliders.clear();
	mPreviousContacts.clear();
	mCurrentContacts.clear();
}

void CollisionSystem::AddCollider(Collider* _Collider)
{
	if (_Collider)
	{
		auto _RigidBody = _Collider->GetRigidBody();
		if (_RigidBody)
		{
			if (_RigidBody->InvMass != 0.0f)
			{
				auto _Duplicated = std::find(mDynamicColliders.begin(), mDynamicColliders.end(), _Collider);
				if (_Duplicated == mDynamicColliders.end())
				{
					mDynamicColliders.push_back(_Collider);
				}
			}
			else
			{
				auto _Duplicated = std::find(mStaticColliders.begin(), mStaticColliders.end(), _Collider);
				if (_Duplicated == mStaticColliders.end())
				{
					mStaticColliders.push_back(_Collider);
				}
			}
		}
	}
}
void CollisionSystem::RemoveCollider(Collider* obj)
{
	if (obj->GetRigidBody() && obj->GetRigidBody()->InvMass != 0.0f)
	{
		auto _ToDelete = std::find(mDynamicColliders.begin(), mDynamicColliders.end(), obj);
		if (_ToDelete != mDynamicColliders.end())
		{
			mDynamicColliders.erase(_ToDelete);
		}
	}
	else if (obj->GetRigidBody())
	{
		auto _ToDelete = std::find(mStaticColliders.begin(), mStaticColliders.end(), obj);
		if (_ToDelete != mStaticColliders.end())
		{
			mStaticColliders.erase(_ToDelete);
		}

	}
}
void CollisionSystem::ClearColliders(void)
{
	mAllColliders.clear();
	mActiveColliders.clear();
	mDynamicColliders.clear();
	mStaticColliders.clear();
}

CollisionFn CollisionSystem::GetCollisionFn(Collider* _Collider1, Collider* _Collider2)
{
	unsigned int collision_index = 0;
	if (_Collider1 && _Collider2)
		collision_index = _Collider1->GetShape() | _Collider2->GetShape();
	return mCollisionTests[collision_index];
}
std::vector<Contact>& CollisionSystem::GetPreviousContacts()
{
	return mPreviousContacts;
}
std::vector<Contact>& CollisionSystem::GetCurrentContacts()
{
	return mCurrentContacts;
}

void CollisionSystem::CollideAllBodies()
{
	CollisionTable* collisionTable = CollTable;
	PhysicSystem* physicsSystem = Physys;

	int EstimatedTasks = mDynamicColliders.size() * mCollisionIterations * 2;
	int CompletedTasks = 0;

	//Reset the collisions in this frame
	mCollisionsThisFrame = 0;

	//Boolean to know if is the first loop done
	bool firstloop = true;

	//Create a contact
	Contact stored;

	mPreviousContacts.clear();
	mPreviousContacts = mCurrentContacts;
	mCurrentContacts.clear();

	bool _SameSpace = true;
	bool _Enabled = true;

	//Do a loop until the given mCollisionIterations
	if (SettingsSys->threadedCollsions)
	{
		for (unsigned int i = 0; i < mCollisionIterations; i++)
		{
			std::for_each(mDynamicColliders.begin(), mDynamicColliders.end(), [&](auto& _Collider1)
			{
				ThreadP->enqueue([=, &CompletedTasks]() mutable
					{
						//Do a loop to iterate through the dynamic bodies
						std::for_each(mDynamicColliders.begin(), mDynamicColliders.end(), [&](auto& _Collider2)
								{
									_SameSpace = (_Collider1->GetOwner()->GetSpaceID() == _Collider2->GetOwner()->GetSpaceID());
									_Enabled = (_Collider1->GetOwner()->Enabled() && _Collider2->GetOwner()->Enabled() && _Collider1->Enabled() && _Collider2->Enabled());

									if ((_Collider1 != _Collider2) && _SameSpace && _Enabled)
									{
										CollisionPolicies possible_col = collisionTable->resolvetable(_Collider1->type_of_object, _Collider2->type_of_object);

										if (possible_col != CollisionPolicies::Ignore)
										{
											//Get the correct collision function
											CollisionFn fn = GetCollisionFn(_Collider1, _Collider2);

											//Execute if they collide
											if (fn(_Collider1, _Collider2, &stored))
											{
												if (firstloop)
												{
													stored.rb1 = _Collider1->GetOwner();
													stored.rb2 = _Collider2->GetOwner();

													collLock.lock();
													bool temp = std::find(mCurrentContacts.begin(), mCurrentContacts.end(), stored) == mCurrentContacts.end() ? true : false;
													collLock.unlock();
													if (temp)
													{
														collLock.lock();
														mCurrentContacts.push_back(stored);
														collLock.unlock();
													}
												}

												if ((_Collider1->GetIsIgnored() == false && _Collider2->GetIsIgnored() == false) && (possible_col != CollisionPolicies::DetectNotResolve))
												{
													//Resolve the contact penetration
													collLock.lock();
													physicsSystem->ResolveContactPenetration(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
													_Collider1->GetCollideBox().UpdateWorld();
													_Collider2->GetCollideBox().UpdateWorld();
													collLock.unlock();

													if (firstloop == true)
													{
														collLock.lock();
														physicsSystem->ResolveContactVelocity(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
														mCollisionsThisFrame++;
														collLock.unlock();
													}
												}
											}
										}
									}
								});
						collLock.lock();
						CompletedTasks++;
						collLock.unlock();
					});

				ThreadP->enqueue([=, &CompletedTasks]() mutable
					{
						//Do a loop to iterate through the static bodies
						std::for_each(mStaticColliders.begin(), mStaticColliders.end(), [&](auto& _Collider2)
									{
										_SameSpace = (_Collider1->GetOwner()->GetSpaceID() == _Collider2->GetOwner()->GetSpaceID());
										_Enabled = (_Collider1->GetOwner()->Enabled() && _Collider2->GetOwner()->Enabled() && _Collider1->Enabled() && _Collider2->Enabled());

										if (_SameSpace && _Enabled)
										{
											CollisionPolicies possible_col = collisionTable->resolvetable(_Collider1->type_of_object, _Collider2->type_of_object);
											if (possible_col != CollisionPolicies::Ignore)
											{
												CollisionFn fn = GetCollisionFn(_Collider1, _Collider2);

												if (fn(_Collider1, _Collider2, &stored))
												{
													if (firstloop)
													{
														stored.rb1 = _Collider1->GetOwner();
														stored.rb2 = _Collider2->GetOwner();

														collLock.lock();
														bool temp = std::find(mCurrentContacts.begin(), mCurrentContacts.end(), stored) == mCurrentContacts.end() ? true : false;
														collLock.unlock();
														if (temp)
														{
															collLock.lock();
															mCurrentContacts.push_back(stored);
															collLock.unlock();
														}
													}

													if ((_Collider2->GetIsIgnored() != true && _Collider1->GetIsIgnored() != true) && possible_col != CollisionPolicies::DetectNotResolve)
													{
														collLock.lock();
														physicsSystem->ResolveContactPenetration(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
														_Collider1->GetCollideBox().UpdateWorld();
														_Collider2->GetCollideBox().UpdateWorld();
														collLock.unlock();

														if (firstloop == true)
														{
															collLock.lock();
															physicsSystem->ResolveContactVelocity(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
															mCollisionsThisFrame++;
															collLock.unlock();
														}

													}
												}
											}
										}
									});
						collLock.lock();
						CompletedTasks++;
						collLock.unlock();
					});
			}); 
			firstloop = false;
			//Set first loop to false
		}
	}
	else
	{
		for (unsigned int i = 0; i < mCollisionIterations; i++)
		{
			std::for_each(mDynamicColliders.begin(), mDynamicColliders.end(), [&](auto& _Collider1)
				{
					//Do a loop to iterate through the dynamic bodies
					std::for_each(mDynamicColliders.begin(), mDynamicColliders.end(), [&](auto& _Collider2)
							{
								_SameSpace = (_Collider1->GetOwner()->GetSpaceID() == _Collider2->GetOwner()->GetSpaceID());
								_Enabled = (_Collider1->GetOwner()->Enabled() && _Collider2->GetOwner()->Enabled() && _Collider1->Enabled() && _Collider2->Enabled());

								if ((_Collider1 != _Collider2) && _SameSpace && _Enabled)
								{
									CollisionPolicies possible_col = collisionTable->resolvetable(_Collider1->type_of_object, _Collider2->type_of_object);

									if (possible_col != CollisionPolicies::Ignore)
									{
										//Get the correct collision function
										CollisionFn fn = GetCollisionFn(_Collider1, _Collider2);

										//Execute if they collide
										if (fn(_Collider1, _Collider2, &stored))
										{
											if (firstloop)
											{
												stored.rb1 = _Collider1->GetOwner();
												stored.rb2 = _Collider2->GetOwner();

												if (std::find(mCurrentContacts.begin(), mCurrentContacts.end(), stored) == mCurrentContacts.end())
													mCurrentContacts.push_back(stored);
											}

											if ((_Collider1->GetIsIgnored() == false && _Collider2->GetIsIgnored() == false) && (possible_col != CollisionPolicies::DetectNotResolve))
											{
												//Resolve the contact penetration
												physicsSystem->ResolveContactPenetration(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
												_Collider1->GetCollideBox().UpdateWorld();
												_Collider2->GetCollideBox().UpdateWorld();

												if (firstloop == true)
												{
													physicsSystem->ResolveContactVelocity(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
													mCollisionsThisFrame++;
												}
											}
										}
									}
								}
							});

					std::for_each(mStaticColliders.begin(), mStaticColliders.end(), [&](auto& _Collider2)
							{
								_SameSpace = (_Collider1->GetOwner()->GetSpaceID() == _Collider2->GetOwner()->GetSpaceID());
								_Enabled = (_Collider1->GetOwner()->Enabled() && _Collider2->GetOwner()->Enabled() && _Collider1->Enabled() && _Collider2->Enabled());

								if (_SameSpace && _Enabled)
								{
									CollisionPolicies possible_col = collisionTable->resolvetable(_Collider1->type_of_object, _Collider2->type_of_object);
									if (possible_col != CollisionPolicies::Ignore)
									{
										CollisionFn fn = GetCollisionFn(_Collider1, _Collider2);

										if (fn(_Collider1, _Collider2, &stored))
										{
											if (firstloop)
											{
												stored.rb1 = _Collider1->GetOwner();
												stored.rb2 = _Collider2->GetOwner();

												if (std::find(mCurrentContacts.begin(), mCurrentContacts.end(), stored) == mCurrentContacts.end())
													mCurrentContacts.push_back(stored);
											}

											if ((_Collider2->GetIsIgnored() != true && _Collider1->GetIsIgnored() != true) && possible_col != CollisionPolicies::DetectNotResolve)
											{
												physicsSystem->ResolveContactPenetration(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
												_Collider1->GetCollideBox().UpdateWorld();
												_Collider2->GetCollideBox().UpdateWorld();

												if (firstloop == true)
												{
													physicsSystem->ResolveContactVelocity(_Collider1->GetRigidBody(), _Collider2->GetRigidBody(), &stored);
													mCollisionsThisFrame++;
												}

											}
										}
									}
								}
							});
				});
			firstloop = false;
			//Set first loop to false
		}
	}
	//Wait for all the threads
	while (CompletedTasks < EstimatedTasks && SettingsSys->threadedCollsions) {}

	std::for_each(mCurrentContacts.begin(), mCurrentContacts.end(), [&](auto& it)
	{
		auto temp = std::find(mPreviousContacts.begin(), mPreviousContacts.end(), it);
		if (temp != mPreviousContacts.end())
		{
			it.rb1->OnCollisionPersisted(it.rb2);
			it.rb2->OnCollisionPersisted(it.rb1);

			mPreviousContacts.erase(temp);
		}
		else
		{
			it.rb1->OnCollisionStarted(it.rb2);
			it.rb2->OnCollisionStarted(it.rb1);
		}

	});

	std::for_each(mPreviousContacts.begin(), mPreviousContacts.end(), [&](auto& it)
	{
		it.rb1->OnCollisionEnded(it.rb2);
		it.rb2->OnCollisionEnded(it.rb1);
	});

	// Debug Information
	/*if (mDynamicColliders.size() > mMaxDynamic)
		mMaxDynamic = mDynamicColliders.size();
	if (mStaticColliders.size() > mMaxStatic)
		mMaxStatic = mStaticColliders.size();
	if (mCollisionsThisFrame > mMaxCollisions)
		mMaxCollisions = mCollisionsThisFrame;
	if (mObjectsThisFrame > mMaxObjects)
	{
		mMaxObjects = mObjectsThisFrame;
		std::cout << "Ative objects this frame: " << mObjectsThisFrame << std::endl;
		std::cout << "Dynamic bodies this frame: " << mDynamicColliders.size() << std::endl;
		std::cout << "Static bodies this frame: " << mStaticColliders.size() << std::endl;
		std::cout << "Collisions this frame: " << mCollisionsThisFrame << std::endl;

		std::cout << "Maximum Objects: " << mMaxObjects << std::endl;
		std::cout << "Maximum Dynamic bodies: " << mMaxDynamic << std::endl;
		std::cout << "Maximum Static bodies: " << mMaxStatic << std::endl;
		std::cout << "Maximum Collisions: " << mMaxCollisions << std::endl << std::endl;
	}*/
}

void CollisionSystem::GetActiveColliders()
{
	ClearColliders();

	for (auto& _Space : SceneSys->GetAllSpaces())
	{
		_Space->GetActiveComponents<Collider>(mActiveColliders, "Collider");
		mObjectsThisFrame = _Space->GetActiveObjects().size();
	}

	for (auto& _Collider : mActiveColliders)
	{
		if (_Collider != nullptr)
		{
			if (_Collider->GetRigidBody())
			{
				bool _Dynamic = _Collider->GetRigidBody()->InvMass != 0.0f;
				if (_Dynamic)
					mDynamicColliders.push_back(_Collider);
				else
					mStaticColliders.push_back(_Collider);
			}
			_Collider->Update();
		}
	}
}

bool CollideCircles(Collider* _Collider1, Collider* _Collider2, Contact* _Contact)
{
	glm::vec3 pos1 = _Collider1->GetCollideBox().GetWorldPosition();
	glm::vec3 pos2 = _Collider2->GetCollideBox().GetWorldPosition();
	glm::vec3 sca1 = _Collider1->GetCollideBox().GetWorldScale();
	glm::vec3 sca2 = _Collider2->GetCollideBox().GetWorldScale();

	return StaticCircleToStaticCircleEx(pos1, sca1.x, pos2, sca2.x, _Contact);
}
bool CollideAABBs(Collider* _Collider1, Collider* _Collider2, Contact* _Contact)
{
	glm::vec3 pos1 = _Collider1->GetCollideBox().GetWorldPosition();
	glm::vec3 pos2 = _Collider2->GetCollideBox().GetWorldPosition();
	glm::vec3 sca1 = _Collider1->GetCollideBox().GetWorldScale();
	glm::vec3 sca2 = _Collider2->GetCollideBox().GetWorldScale();

	return StaticRectToStaticRectEx(pos1, sca1, pos2, sca2, _Contact);
}
bool CollideOBBs(Collider* _Collider1, Collider* _Collider2, Contact* _Contact)
{
	glm::vec3 pos1 = _Collider1->GetCollideBox().GetWorldPosition();
	glm::vec3 pos2 = _Collider2->GetCollideBox().GetWorldPosition();
	glm::vec3 sca1 = _Collider1->GetCollideBox().GetWorldScale();
	glm::vec3 sca2 = _Collider2->GetCollideBox().GetWorldScale();

	float or1 = _Collider1->GetCollideBox().GetWorldOrientation();
	float or2 = _Collider2->GetCollideBox().GetWorldOrientation();

	Transform2D obb1(pos1, sca1, or1);
	Transform2D obb2(pos2, sca2, or2);

	return OrientedRectToOrientedRectEx(&obb1, &obb2, _Contact);
}
bool CollideAABBToCircle(Collider* _Collider1, Collider* _Collider2, Contact* _Contact)
{
	Collider* rect = nullptr;
	Collider* circle = nullptr;

	// which is which
	if (_Collider1)
		rect = _Collider1->GetShape() == CSHAPE_AABB ? _Collider1 : _Collider2;

	if (_Collider2)
		circle = _Collider2->GetShape() == CSHAPE_AABB ? _Collider1 : _Collider2;

	if (StaticRectToStaticCircleEx(glm::vec3(rect->GetCollideBox().GetWorldPosition()), rect->GetCollideBox().GetWorldScale().x, rect->GetCollideBox().GetWorldScale().y, glm::vec3(circle->GetCollideBox().GetWorldPosition()), circle->GetCollideBox().GetWorldScale().x, _Contact))
	{
		if (circle == _Collider1) // flip normal to match our convention
			_Contact->mNormal = -_Contact->mNormal;
		return true;
	}
	return false;
}
bool CollideOBBToCircle(Collider* _Collider1, Collider* _Collider2, Contact* _Contact)
{
	Collider* rect = nullptr;
	Collider* circle = nullptr;

	// which is which
	if (_Collider1)
		rect = _Collider1->GetShape() == CSHAPE_OBB ? _Collider1 : _Collider2;

	if (_Collider2)
		circle = _Collider2->GetShape() == CSHAPE_OBB ? _Collider1 : _Collider2;

	Transform2D rect_trans(rect->GetCollideBox().GetWorldPosition(), rect->GetCollideBox().GetWorldScale(), rect->GetCollideBox().GetWorldOrientation());

	if (StaticOBBToStaticCircleEx(&rect_trans, glm::vec3(circle->GetCollideBox().GetWorldPosition().x, circle->GetCollideBox().GetWorldPosition().y, 0),
		circle->GetCollideBox().GetWorldScale().x, _Contact))
	{
		if (circle == _Collider1) // flip normal to match our convention
			_Contact->mNormal = -_Contact->mNormal;
		return true;
	}
	return false;
}





