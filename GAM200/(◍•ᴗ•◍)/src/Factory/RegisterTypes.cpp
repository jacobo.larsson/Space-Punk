﻿#pragma once

#include "../RTTI/IComp.h"
#include "../Transform2D/TransformComponent.h"
#include "../Collisions/ICollider.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Graphics/Camera/Camera.h"
#include "../Physics/RigidBody.h"
#include "../Audio/SoundEmitter.h"
#include "../Audio/AudioListener.h"

#include "../PropertySystem/PropertyMap.h"
#include "../Graphics/Shaders/Shader.h"
#include "../Graphics/Texture/Texture.h"
#include "../GameObject/GameObject.h"
#include "../Graphics/Mesh/Mesh.h"
#include "../Graphics/Font/Font.h"
#include "../LogicSystem/LogicComponent.h"
#include "../Game/CameraFollow/CameraFollow.h"
#include "../RayCast/Raycast.h"
#include "../StateMachines/BrainComponent/BrainComponent.h"
#include "../Animation/Path2D.h"
#include "../RayCast/RayCaster.h"
#include "../Archetype/Archetype.h"


#include "../Game/PlayerMovement/PlayerMovement.h"
#include "../Game/SimplePlayerMovement/SimplePlayerMovement.h"
#include "../Game/PlatformSystem/PlatformSystem.h"
#include "../Game/BasicEnemy/BasicEnemySystem.h"
#include "../Game/BasicEnemy/BasicTicketer/BasicTicketer.h"
#include "../Game/BasicEnemy/BasicTicketer/BasicEnemyTicketSystem.h"
#include "../Game/ChargerEnemy/ChargerEnemySystem.h"
#include "../Game/FlyingEnemy/FlyingEnemySystem.h"
#include "../Game/FlyingEnemy/FlockComp/FlockComp.h"
#include "../Game/TurretEnemy/TurretEnemySystem.h"
#include "../Game/Lifebar/Lifebar.h"
#include "../Game/Health/Health.h"
#include "../StateMachines/StateMachine/StateMachine.h"
#include "../Game/SimplePlayerMovement/SimpleMovementStates.h"
#include "../Game/SimplePlayerMovement/SimplePlayerMovement.h"
#include "../Game/GasEnemy/GasEnemySystem.h"
#include "../Game/GasEnemy/GasCloud.h"
#include "../Game/GasEnemy/GasExplosion.h"
#include "../Game/TurretEnemy/BulletTimer.h"
#include "../Cheats/Cheats.h"
#include "../SelectableComp/SelectableComp.h"
#include "../Game/ArenaManager/ArenaManager.h"
#include "../Game/ArenaManager/TriggerArena/TriggerArena.h"
#include "../Game/HoverBoard/TriggerHoverBoard/TriggerHoverBoard.h"
#include "../Game/HoverBoard/RespawnHoverboard/RespawnHoverboard.h"
#include "../Game/Checkpoint/Checkpoint.h"
#include "../Game/GoBackToMenu/GoBackToMenu.h"
#include "../Game/CameraFollow/CameraMachine.h"
#include "../Game/CameraFollow/CameraTriggerer/CameraTriggerer.h"
#include "../Game/Dialog/Dialog.h"
#include "../Game/Hitbox/Hitbox.h"
#include "../Game/ShotReceiver/ShotReceiver.h"
#include "../Game/Breakable/Breakable.h"
#include "../Game/CameraFollow/Parallax/Parallax.h"
#include "../Game/GasEnemy/GasTicketer/GasTicketer.h"
#include "../SpacePunkGame/PlayerShipMovement/PlayerShipMovementMachine.h"
#include "../SpacePunkGame/PlayerShipAttack/PlayerShipAttackMachine.h"
#include "../SpacePunkGame/LaserBullet/LaserBullet.h"
#include "../SpacePunkGame/Asteroid/AsteroidMachine.h"
#include "../SpacePunkGame/ScreenWraper/ScreenWraper.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyMovement/StandardEnemyMovementMachine.h"
#include "../SpacePunkGame/Evading/EvadingComponent.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyAttack/StandardEnemyAttackMachine.h"
#include "../SpacePunkGame/StandardEnemy/StandardEnemyLaserBulletComp/StandardEnemyLaserBulletComp.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyMovementMachine/HeavyEnemyMovementMachine.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyAttackMachine/HeavyEnemyAttackMachine.h"
#include "../SpacePunkGame/HeavyEnemy/HeavyEnemyLaserBulletComp/HeavyEnemyLaserBulletComp.h"
#include "../SpacePunkGame/HealthComp/HealthComp.h"
#include "../SpacePunkGame/CameraShake/CameraShake.h"
#include "../SpacePunkGame/WaveComponent/WaveComp.h"
#include "../SpacePunkGame/WaveComponent/WaveManager.h"

#include "../PropertySystem/PropertyMap.h"


#include "../Graphics/Shaders/Shader.h"
#include "../Graphics/Texture/Texture.h"
#include "../GameObject/GameObject.h"
#include "../Graphics/Mesh/Mesh.h"
#include "../Space/Space.h"
#include "../Animation/SM_Animation.h"
#include "../Graphics/Particle System/ParticleSystem.h"
#include "../TutorialDummy/TutorialDummy.h"
#include "../Cursor/Cursor.h"
#include "../Splines/SplineComp.h"

#include "../Factory/Factory.h"
#include "RegisterTypes.h"

void RegisterAllTypes()
{
	// Components
	FactorySys->Register<IComp>();
	FactorySys->Register<TransformComponent>();
	FactorySys->Register<Collider>();
	FactorySys->Register<Renderable>();
	FactorySys->Register<CameraComponent>();
	FactorySys->Register<RigidBody>();
	FactorySys->Register<SoundEmiter>();
	FactorySys->Register<AudioListener>();
	FactorySys->Register<LogicComponent>();
	FactorySys->Register<CameraFollow>();
	FactorySys->Register<Ray>();
	FactorySys->Register<BrainComponent>();
	FactorySys->Register<Path2D>();
	FactorySys->Register<RayCaster>();
	FactorySys->Register<ArenaManager>();
	FactorySys->Register<TriggerArena>();
	FactorySys->Register<TriggerHoverBoard>();
	FactorySys->Register<Health>();
	FactorySys->Register<Archetype>();
	FactorySys->Register<GasCloud>();
	FactorySys->Register<BulletTimer>();
	FactorySys->Register<GasExplosion>();
	FactorySys->Register<Cheats>();
	FactorySys->Register<CameraTriggerer>();
	FactorySys->Register<DialogComponent>();
	FactorySys->Register<SelectableComp>();
	FactorySys->Register<Hitbox>();
	FactorySys->Register<ShotReceiver>();
	FactorySys->Register<Breakable>();
	FactorySys->Register<ParallaxComponent>();
	FactorySys->Register<RespawnHoverboard>();
	FactorySys->Register<FlockComp>();
	FactorySys->Register<Checkpoint>();
	FactorySys->Register<GoBackToMenu>();
	FactorySys->Register<TutorialDummy>();
	FactorySys->Register<Cursor>();
	FactorySys->Register<SplineComp>();
	FactorySys->Register<BasicTicketer>();
	FactorySys->Register<GasTicketer>();
	FactorySys->Register<LaserBulletComponent>();
	FactorySys->Register<ScreenWraperComponent>();
	FactorySys->Register<EvadingComponent>();
	FactorySys->Register<StandardEnemyLaserBulletComp>();
	FactorySys->Register<HeavyEnemyLaserBulletComp>();
	FactorySys->Register<CameraShakeComp>();
	FactorySys->Register<HealthComp>();
	FactorySys->Register<WaveComponent>();
	FactorySys->Register<WaveManager>();

	// State Machines
	FactorySys->Register<MovementMachine>();
	FactorySys->Register<SimpleMovementMachine>();
	FactorySys->Register<PlatformMachine>();
	FactorySys->Register<BasicEnemyMachine>();
	FactorySys->Register<BasicEnemyTicketMachine>();
	FactorySys->Register<ChargerEnemyMachine>();
	FactorySys->Register<FlyingEnemyMachine>();
	FactorySys->Register<TurretEnemyMachine>();
	FactorySys->Register<LifebarMachine>();
	FactorySys->Register<GasEnemyMachine>();
	FactorySys->Register<CameraMachine>();
	FactorySys->Register<PlayerShipMovementMachine>();
	FactorySys->Register<PlayerShipAttackMachine>();
	FactorySys->Register<AsteroidMachine>();
	FactorySys->Register<StandardEnemyMovementMachine>();
	FactorySys->Register<StandardEnemyAttackMachine>();
	FactorySys->Register<HeavyEnemyMovementMachine>();
	FactorySys->Register<HeavyEnemyAttackMachine>();


	// Handled properties types
	FactorySys->Register<Property<int>>();
	FactorySys->Register<Property<float>>();
	FactorySys->Register<Property<std::string>>();
	FactorySys->Register<Property<bool>>();


	// Everthing else
	FactorySys->Register<Shader>();
	FactorySys->Register<Texture>();
	FactorySys->Register<GameObject>();
	FactorySys->Register<Mesh>();
	FactorySys->Register<Space>();
	//FactorySys->Register<SkeletonAnimComp>();
	FactorySys->Register<SkeletonRenderable>();
	FactorySys->Register<TextRenderable>();
	FactorySys->Register<ParticleEmitter>();
}