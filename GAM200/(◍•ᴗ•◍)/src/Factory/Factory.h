/**********************************************************************************/
/*!
\file   Factory.h
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the functions that make a factory that creates components.
*/
/*********************************************************************************/
#pragma once

#include <map>
#include <string>

#include "../RTTI/IBase.h"
#include "../RTTI/SM_RTTI.h"
#include "../RTTI/IComp.h"
#include "../Singleton/Singleton.h"
#include "../Settings/Settings.h"


class ICreator
{
public:
	ICreator() {}

	virtual IBase* Create() = 0;
};

template <typename type>
class TCreator : public ICreator
{
public:
	TCreator() : ICreator() {}

	virtual IBase* Create()
	{
		//TODO: Sanity check, see if the type derives from IBase
		return dynamic_cast<IBase*>(new type);
	}
};

class Factory : public IBase
{
	SM_RTTI_DECL(Factory, IBase);
	Make_Singleton(Factory)
public:
	bool Initialize();

	void AddCreator(const char* class_type, ICreator* creator);
	template <typename T>
	void Register(ICreator* creator = nullptr)
	{
		AddCreator(T::TYPE().GetName().c_str(), creator ? creator : new TCreator<T>());
	}

	IBase* Create(const char* class_type);
	template <typename T>
	T* Create()
	{
		std::string class_type = T::TYPE().GetName();

		auto it = mCreators.find(class_type);
		if (it == mCreators.end())
			Register<T>();

		auto obj = dynamic_cast<T*>(mCreators[class_type]->Create());
		obj->mUID = uId;
		if(uId > (unsigned int)SettingsSys->current_max_id)
			SettingsSys->current_max_id = uId;
		
		uId++;
		auto temp = dynamic_cast<IComp*>(obj);
		if (temp)
			temp->OnCreate();

		return obj;

	}
	unsigned GenerateNewID();

	unsigned int uId = 0;
private:
	std::map<std::string, ICreator*> mCreators;
};

#define FactorySys Factory::Get()