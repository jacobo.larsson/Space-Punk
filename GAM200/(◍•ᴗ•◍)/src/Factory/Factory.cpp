/**********************************************************************************/
/*!
\file   Factory.cpp
\author Jaime Sanchez
\par    email: jaime.s@digipen.edu
\par    DigiPen login: jaime.s
\par    Course: GAM200
\date   16/10/2020
\brief
File containing the function declarations of Factory.h
*/
/*********************************************************************************/
#include "Factory.h"
#include "RegisterTypes.h"

Factory* Factory::instance = nullptr;

bool Factory::Initialize()
{
	RegisterAllTypes();

	return true;
}

void Factory::AddCreator(const char* class_type, ICreator* creator)
{
	// first check if the creator exists already, if so, free it. 
	auto it = mCreators.find(class_type);
	if (it != mCreators.end())// exists, we overwrite
		delete it->second;

	// now register the creator
	mCreators[class_type] = creator;
}

IBase* Factory::Create(const char* class_type)
{
	// first get the creator associated with the type
	auto it = mCreators.find(class_type);
	if (it != mCreators.end() && it->second != nullptr) // exists
	{
		auto obj = it->second->Create();
		obj->mUID = uId;
		uId++;

		auto temp = dynamic_cast<IComp*>(obj);
		if (temp)
			temp->OnCreate();

		return obj;
	}
	// creator doesn't exist, we don't know how to create the requested type. 
	return nullptr;
}

unsigned Factory::GenerateNewID()
{
	return ++mUID;
}

