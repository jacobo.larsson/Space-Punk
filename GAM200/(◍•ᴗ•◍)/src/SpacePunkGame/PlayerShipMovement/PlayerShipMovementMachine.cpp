#pragma once
#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"
#include "../../Factory/Factory.h" 

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"
#include "../../Graphics/Camera/Camera.h"

#include "PlayerShipMovementMachine.h"
#include "PlayerShipMovementStates.h"

PlayerShipMovementMachine::PlayerShipMovementMachine()
{
	mName = "PlayerShipMovementMachine";
	
	mInitialState = new PlayerShipIdle("PlayerShipIdle");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new PlayerShipMove("PlayerShipMove"));
	AddState(new PlayerShipDash("PlayerShipDash"));
}

PlayerShipMovementMachine::~PlayerShipMovementMachine()
{
	//StateMachine::~StateMachine();
}

void PlayerShipMovementMachine::Initialize()
{
	StateMachine::Initialize();

	pTransform = mActor->GetComp<TransformComponent>();
	pRigidbody = mActor->GetComp<RigidBody>();

	camShakeComp = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraShakeComp>();
}

void PlayerShipMovementMachine::Update()
{
	StateMachine::Update();
}

void PlayerShipMovementMachine::Shutdown()
{
	StateMachine::Clear();
}

bool PlayerShipMovementMachine::Edit()
{
	//if (ImGui::Begin(mName.c_str(), nullptr, 0))
	//{
	//	ImGui::DragFloat("Max X velocity", &MaxXVelocity);
	//	ImGui::DragFloat("Max Y velocity", &yMaxVelocity);
	//	ImGui::DragFloat("Acceleration", &Acceleration);
	//	ImGui::DragFloat("Max X velocity", &MaxXVelocity);
	//	ImGui::End();
	//}
	//else
	//{
	//	ImGui::End();
	//}
	return false;
}

PlayerShipMovementMachine& PlayerShipMovementMachine::operator=(const PlayerShipMovementMachine& rhs)
{
	//MaxXVelocity = rhs.MaxXVelocity;
	//CurrentVelocity = rhs.CurrentVelocity;
	//yVelocity = rhs.yVelocity;
	//yMaxVelocity = rhs.yMaxVelocity;
	//Acceleration = rhs.Acceleration;
	//
	//FacingRight = rhs.FacingRight;
	//PreviousFacingRight = rhs.PreviousFacingRight;
	//canJump = rhs.canJump;

	return *this;
}

PlayerShipMovementMachine* PlayerShipMovementMachine::Clone()
{
	PlayerShipMovementMachine* temp = FactorySys->Create<PlayerShipMovementMachine>();
	*temp = *this;
	return temp;
}

void PlayerShipMovementMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
}

void PlayerShipMovementMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
}

void PlayerShipMovementMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);

	if (other->HasTag("EnemyBullet"))
	{
		SceneSys->GetMainSpace()->DestroyObject(other);
		camShakeComp->activateEffect = true;
	}
}