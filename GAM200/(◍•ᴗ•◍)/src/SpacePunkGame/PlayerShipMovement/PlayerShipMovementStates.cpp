#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Time/Time.h"

#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/Raycast.h"

#include "PlayerShipMovementStates.h"
#include "PlayerShipMovementMachine.h"

#pragma region PlayerShipIdle
PlayerShipIdle::PlayerShipIdle(const char* name) : State(name) {}

void PlayerShipIdle::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipMovementMachine*>(mOwnerStateMachine);

	if(mActor->HasComp<TransformComponent>())
		pTransform = mActor->GetComp<TransformComponent>();

	State::Initialize();
}

void PlayerShipIdle::LogicEnter()
{
	mTimeInState = 0.0f;
}


void PlayerShipIdle::LogicExit()
{
}

void PlayerShipIdle::LogicUpdate()
{
	if (GameInput::DashKeyPressed() && castedMachine->mDashCoolDownCounter > castedMachine->mDashCoolDown)
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShipDash"));

	if (GameInput::RightKeyPressed() || GameInput::LeftKeyPressed() || GameInput::UpKeyPressed())
		mOwnerStateMachine->ChangeState(mOwnerStateMachine->GetState("PlayerShipMove"));

	castedMachine->mDashCoolDownCounter += TimeSys->GetFrameTime();
}
#pragma endregion

#pragma region PlayerShipMove

PlayerShipMove::PlayerShipMove(const char* name) : State(name) {}

void PlayerShipMove::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipMovementMachine*>(mOwnerStateMachine);
	pTransform = mActor->GetComp<TransformComponent>();
	State::Initialize();
}

void PlayerShipMove::LogicEnter()
{
	mTimeInState = 0.0f;
}

void PlayerShipMove::LogicExit()
{

}
void PlayerShipMove::LogicUpdate()
{
	castedMachine->mDashCoolDownCounter += TimeSys->GetFrameTime();
	auto angle = pTransform->GetWorldOrientation();
	castedMachine->dir = { cosf(angle), sinf(angle) };
	auto &vel = castedMachine->pRigidbody->Velocity;
	float maxVel = castedMachine->mMaxVelocity;


	if (GameInput::UpKeyPressed())
	{
		vel += castedMachine->dir * castedMachine->mAcceleration * (float) TimeSys->GetFrameTime();
	}

	if (GameInput::DashKeyPressed() && castedMachine->mDashCoolDownCounter > castedMachine->mDashCoolDown/*|| (GameInput::DashKeyPressed() && GameInput::UpKeyPressed())*/)
		castedMachine->ChangeState("PlayerShipDash");

	if (GameInput::RightKeyPressed())
	{
		angle -= castedMachine->mRotatingVelocity * TimeSys->GetFrameTime();
	}

	else if (GameInput::LeftKeyPressed())
	{
		angle += castedMachine->mRotatingVelocity * TimeSys->GetFrameTime();
	}

	else if(!GameInput::DashKeyPressed())
	{
		castedMachine->ChangeState("PlayerShipIdle");
	}


	if (vel.length() >= maxVel) {
		castedMachine->dir = glm::normalize(vel);
		vel = castedMachine->dir * maxVel;
	}

	pTransform->SetWorldOrientation(angle);
}

#pragma endregion

#pragma region PlayerShipDash

PlayerShipDash::PlayerShipDash(const char* name) : State(name) {}

void PlayerShipDash::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipMovementMachine*>(mOwnerStateMachine);
	pTransform = mActor->GetComp<TransformComponent>();
	State::Initialize();
}

void PlayerShipDash::LogicEnter()
{
	mTimeInState = 0.0f;
	castedMachine->mMaxVelocity = 12000;
	castedMachine->mDashCoolDownCounter = 0.0f;
	castedMachine->mAcceleration = 9000.0f;
	castedMachine->mDashCoolDownCounter = 0.0f;
}

void PlayerShipDash::LogicExit()
{
	castedMachine->mMaxVelocity = 1000.0f;
	castedMachine->mAcceleration = 750.0f;
}

void PlayerShipDash::LogicUpdate()
{
	mTimeInState += TimeSys->GetFrameTime();
	auto& vel = castedMachine->pRigidbody->Velocity;

	if (mTimeInState < castedMachine->mDashTime)
		vel += castedMachine->dir * castedMachine->mAcceleration * (float)TimeSys->GetFrameTime();

	//if(mTimeInState < castedMachine->mDashTime)
	//	castedMachine->Accelerate(castedMachine->mDashAcceleration);
	//
	//if (mTimeInState > castedMachine->mDashTime)
	//{
	//	castedMachine->Accelerate(-castedMachine->mDashAcceleration / 2);
	//}

	if (mTimeInState > castedMachine->mDashTime)
		castedMachine->ChangeState("PlayerShipIdle");
}

void PlayerShipDash::OnCollisionStarted(GameObject* other)
{
}

#pragma endregion