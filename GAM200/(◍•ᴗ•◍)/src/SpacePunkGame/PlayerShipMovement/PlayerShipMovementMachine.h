#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../RayCast/RayCaster.h"
#include "../../StateMachines/SuperState/SuperState.h"
#include "../../SpacePunkGame/CameraShake/CameraShake.h"


class RigidBody;
class TransformComponent;


class PlayerShipMovementMachine : public StateMachine
{
	SM_RTTI_DECL(PlayerShipMovementMachine, StateMachine);

public:
	PlayerShipMovementMachine();
	~PlayerShipMovementMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	PlayerShipMovementMachine& operator=(const PlayerShipMovementMachine& rhs);
	PlayerShipMovementMachine* Clone()override;
	
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	
	void OnCollisionStarted(GameObject* other) override;

	float mMaxVelocity = 1000.0f;
	float mCurrentVelocity = 0.0f;
	float mRotatingVelocity = 3.0f;
	float mAcceleration = 750.0f;
	float mOrientation = 0.0f;
	float mDashTime = 0.5f;
	float mDashAcceleration = 15000.0f;
	float mDashCoolDown = 3.0f;
	float mDashCoolDownCounter = 3.0f;
	glm::vec2 dir;

	TransformComponent* pTransform;
	CameraShakeComp* camShakeComp;
	RigidBody* pRigidbody;
};