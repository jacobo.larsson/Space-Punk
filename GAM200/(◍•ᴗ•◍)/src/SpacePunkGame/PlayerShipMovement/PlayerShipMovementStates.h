#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"
#include "../../Transform2D/TransformComponent.h"

class PlayerShipMovementMachine;

class PlayerShipIdle : public State
{
public:
	PlayerShipIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* pTransform;
	PlayerShipMovementMachine* castedMachine;
};

class PlayerShipMove : public State
{
public:
	PlayerShipMove(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	RigidBody* pBody;
	TransformComponent* pTransform;
	PlayerShipMovementMachine* castedMachine;
};

class PlayerShipDash : public State
{
public:
	PlayerShipDash(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();
	void OnCollisionStarted(GameObject* other) override;

	TransformComponent* pTransform;
	PlayerShipMovementMachine* castedMachine;
};