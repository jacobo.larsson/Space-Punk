#include "CameraShake.h"

CameraShakeComp::CameraShakeComp()
{
	mName = "CameraShakeComp";
}

CameraShakeComp::CameraShakeComp(CameraShakeComp& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

CameraShakeComp& CameraShakeComp::operator=(CameraShakeComp& rhs)
{
	return (*this);
}

CameraShakeComp* CameraShakeComp::Clone()
{
	CameraShakeComp* temp = FactorySys->Create<CameraShakeComp>();
	(*temp) = (*this);
	return temp;
}

void CameraShakeComp::Initialize()
{
	mTransform = mOwner->GetComp<TransformComponent>();

	activateEffect = false;

	LogicComponent::Initialize();
}

void CameraShakeComp::Update()
{
	if (activateEffect)
	{
		mTransform->SetWorldPosition(glm::vec3{ mTransform->GetWorldPosition().x + shakeMag, mTransform->GetWorldPosition().y, mTransform->GetWorldPosition().z });

		shakeMag = -shakeMag;

		timeCounter += TimeSys->GetFrameTime();
		if (timeCounter >= effectDuration)
		{
			activateEffect = false;
			timeCounter = 0.0f;
		}
	}
}

void CameraShakeComp::Shutdown()
{
	LogicComponent::Shutdown();
}

//bool CameraShakeComp::Edit()
//{
//	return false;
//}

//void CameraShakeComp::ToJson(nlohmann::json& j)
//{
//	j["ownerShipID"] = ownerShipID;
//}

//void CameraShakeComp::FromJson(const nlohmann::json& j)
//{
//	if (j.find("ownerShipID") != j.end())
//		ownerShipID = j["ownerShipID"];
//}
