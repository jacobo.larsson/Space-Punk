#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Graphics/Camera/Camera.h"
#include "../../Time/Time.h"
#include "../../Settings/Settings.h"
#include "../../Physics/RigidBody.h"

class CameraShakeComp : public LogicComponent
{
	SM_RTTI_DECL(CameraShakeComp, LogicComponent);

public:
	CameraShakeComp();
	CameraShakeComp(CameraShakeComp& rhs);
	CameraShakeComp& operator=(CameraShakeComp& rhs);
	CameraShakeComp* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	//bool Edit()override;
	//void ToJson(nlohmann::json& j) override;
	//void FromJson(const nlohmann::json& j) override;

	bool activateEffect;

private:
	TransformComponent* mTransform;

	float shakeMag = 25.0f;
	float effectDuration = 0.2f;
	float timeCounter = 0.0f;
};