#pragma once
#include "../../RayCast/Raycast.h"
#include "../../RayCast/RayCastTest.h"
#include "../../Transform2D/Transform2D.h"

#include "../../Space/Space.h"
#include "../../Scene/Scene.h"

#include "../../RayCast/LineSegment2D.h"
#include "../../Transform2D/Transform2D.h"
#include "../../Graphics/Color/Color.h"
#include "../../Graphics/RenderManager/RenderManager.h"
#include "../../GameObject/GameObject.h"
#include "../../Space/Space.h"
#include "../../Collisions/ICollider.h"

#include "EvadingComponent.h"

EvadingComponent::EvadingComponent()
{
	mName = "EvadingComponent";
}

EvadingComponent::EvadingComponent(EvadingComponent& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

EvadingComponent& EvadingComponent::operator=(EvadingComponent& rhs)
{

	return (*this);
}

EvadingComponent* EvadingComponent::Clone()
{
	EvadingComponent* temp = FactorySys->Create<EvadingComponent>();
	(*temp) = (*this);
	return temp; 
}

void EvadingComponent::Initialize()
{
	mTransform = mOwner->GetComp<TransformComponent>();
	if (SceneSys->GetMainSpace()->FindObjectById(ownerShipID)->HasTag("Enemy"))
	{
		shipTransform = SceneSys->GetMainSpace()->FindObjectById(ownerShipID)->GetComp<TransformComponent>();
		shipRigidBody = SceneSys->GetMainSpace()->FindObjectById(ownerShipID)->GetComp<RigidBody>();
		mShip = SceneSys->GetMainSpace()->FindObjectById(ownerShipID);
	}

	LogicComponent::Initialize();
}

void EvadingComponent::Update()
{
	if (mShip->IsDeleted() || !mShip->Enabled())
		mOwner->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(mOwner);

	if (mShip->HasTag("Enemy"))
		mTransform->SetWorldPosition(shipTransform->GetWorldPosition());

	else
		mOwner->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(mOwner);

	AvoidObjects();
}

void EvadingComponent::Shutdown()
{
	LogicComponent::Shutdown();
}

bool EvadingComponent::Edit()
{
	bool changed = false;
	ImGui::PushID(&ownerShipID);
	ImGui::InputInt("MyOwner's ID", &ownerShipID);
	ImGui::PopID();

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

void EvadingComponent::ToJson(nlohmann::json& j)
{
	j["ownerShipID"] = ownerShipID;
}

void EvadingComponent::FromJson(const nlohmann::json& j)
{
	if (j.find("ownerShipID") != j.end())
		ownerShipID = j["ownerShipID"];
}

void EvadingComponent::OnCollisionStarted(GameObject* other)
{
	if (!other->IsDeleted() && other->GetUID() != ownerShipID)
		mObjectsList.push_back(other);
}

void EvadingComponent::OnCollisionEnded(GameObject* other)
{
	if (mObjectsList.size() == 1)
	{
		if (other->GetUID() == mObjectsList[0]->GetUID())
			mObjectsList.pop_back();
	}

	else
	{
		for (auto it = mObjectsList.begin(); it != mObjectsList.end(); it++)
		{
			if ((*it)->GetUID() == other->GetUID())
			{
				mObjectsList.erase(it);
				break;
			}

			if (mObjectsList.size() == 1)
				break;
		}
	}
}

void EvadingComponent::AvoidObjects()
{
	if (mObjectsList.size() == 1)
	{
		if (mObjectsList[0]->IsDeleted())
			return;

		TransformComponent* objectTransform = mObjectsList[0]->GetComp<TransformComponent>();
		Transform2D rectTransform = Transform2D(objectTransform->GetWorldPosition(), objectTransform->GetWorldScale(), objectTransform->GetWorldOrientation());

		glm::vec2 rayDirection = objectTransform->GetWorldPosition() - mTransform->GetWorldPosition();
		rayDirection = glm::normalize(rayDirection);

		glm::vec2 mPos2Obj = { 0.0f, 0.0f };
		glm::vec2 mPos = mTransform->GetWorldPosition();

		float rayCastDistance = RayCastRect(mPos, rayDirection, rectTransform, &mPos2Obj);

		float importance = 1 - (rayCastDistance / maxDist);
		mPos2Obj = objectTransform->GetWorldPosition() - mTransform->GetWorldPosition();

		glm::vec2 velocity = shipRigidBody->Velocity;
		glm::vec2 proj = ((glm::dot(velocity, mPos2Obj)) / (glm::length(velocity) * glm::length(velocity))) * velocity;

		glm::vec2 perpProj = mPos2Obj - proj;
		perpProj = glm::normalize(perpProj);

		glm::vec2 evadingVec = -perpProj * importance;

		shipRigidBody->Velocity += evadingVec * (float)TimeSys->GetFrameTime() * 5500.0f;

		return;
	}

	for (auto it = mObjectsList.begin(); it != mObjectsList.end(); it++)
	{
		if ((*it)->IsDeleted())
			return;

		TransformComponent* objectTransform = (*it)->GetComp<TransformComponent>();
		Transform2D rectTransform = Transform2D(objectTransform->GetWorldPosition(), objectTransform->GetWorldScale(), objectTransform->GetWorldOrientation());

		glm::vec2 rayDirection = objectTransform->GetWorldPosition() - mTransform->GetWorldPosition();
		rayDirection = glm::normalize(rayDirection);

		glm::vec2 mPos2Obj = { 0.0f, 0.0f };
		glm::vec2 mPos = mTransform->GetWorldPosition();

		float rayCastDistance = RayCastRect(mPos, rayDirection, rectTransform, &mPos2Obj);

		float importance = 1 - (rayCastDistance / maxDist);
		mPos2Obj = objectTransform->GetWorldPosition() - mTransform->GetWorldPosition();

		glm::vec2 velocity = shipRigidBody->Velocity;
		glm::vec2 proj = ((glm::dot(velocity, mPos2Obj)) / (glm::length(velocity) * glm::length(velocity))) * velocity;

		glm::vec2 perpProj = mPos2Obj - proj;
		perpProj = glm::normalize(perpProj);

		glm::vec2 evadingVec = -perpProj * importance;

		shipRigidBody->Velocity += evadingVec * (float)TimeSys->GetFrameTime() * 5500.0f;
	}
}

