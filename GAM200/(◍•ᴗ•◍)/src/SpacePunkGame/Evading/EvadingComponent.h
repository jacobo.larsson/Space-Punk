#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../Settings/Settings.h"
#include "../../Physics/RigidBody.h"

class EvadingComponent : public LogicComponent
{
	SM_RTTI_DECL(EvadingComponent, LogicComponent);

public:
	EvadingComponent();
	EvadingComponent(EvadingComponent& rhs);
	EvadingComponent& operator=(EvadingComponent& rhs);
	EvadingComponent* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

	void AvoidObjects();

private:
	TransformComponent* mTransform;
	GameObject* mShip;
	TransformComponent* shipTransform;
	RigidBody* shipRigidBody;
	std::vector<GameObject*> mObjectsList;
	int ownerShipID;
	float maxDist = 700.0f;
};