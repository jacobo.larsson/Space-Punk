#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"
#include "../../Factory/Factory.h" 

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Space/Space.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"
#include "AsteroidMachine.h"
#include "AsteroidStates.h"

AsteroidMachine::AsteroidMachine()
{
	mName = "AsteroidMachine";

	mInitialState = new AsteroidBig("AsteroidBig");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new AsteroidMedium("AsteroidMedium"));
	AddState(new AsteroidSmall("AsteroidSmall"));
}

AsteroidMachine::~AsteroidMachine()
{
	StateMachine::~StateMachine();
}

void AsteroidMachine::Initialize()
{
	StateMachine::Initialize();

	mTransform = mActor->GetComp<TransformComponent>();
	mRigidBody = mActor->GetComp<RigidBody>();

	pRigidBody = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<RigidBody>();
	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
}

void AsteroidMachine::Update()
{
	StateMachine::Update();
}

void AsteroidMachine::Shutdown()
{
	StateMachine::Clear();
}

bool AsteroidMachine::Edit()
{
	if (ImGui::Begin(mName.c_str(), nullptr, 0))
	{
		ImGui::DragFloat("Big asteroid maximum speed", &bigSpeedMax);
		ImGui::DragFloat("Big asteroid minimum speed", &bigSpeedMin);

		ImGui::DragFloat("Medium asteroid maximum speed", &mediumSpeedMax);
		ImGui::DragFloat("Medium asteroid minimum speed", &mediumSpeedMin);

		ImGui::DragFloat("Small asteroid maximum speed", &smallSpeedMax);
		ImGui::DragFloat("Small asteroid minimum speed", &smallSpeedMin);
		
		ImGui::End();
	}
	else
	{
		ImGui::End();
	}
	return false;
}

AsteroidMachine& AsteroidMachine::operator=(const AsteroidMachine& rhs)
{
	bigSpeedMax = rhs.bigSpeedMax;
	bigSpeedMin = rhs.bigSpeedMin;
	mediumSpeedMax = rhs.mediumSpeedMax;
	mediumSpeedMin = rhs.mediumSpeedMin;
	smallSpeedMax = rhs.smallSpeedMax;
	smallSpeedMin = rhs.smallSpeedMin;

	return (*this);
}

AsteroidMachine* AsteroidMachine::Clone()
{
	AsteroidMachine* temp = FactorySys->Create<AsteroidMachine>();
	*temp = *this;
	return temp;
}

void AsteroidMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
}

void AsteroidMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
}

void AsteroidMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);
}
