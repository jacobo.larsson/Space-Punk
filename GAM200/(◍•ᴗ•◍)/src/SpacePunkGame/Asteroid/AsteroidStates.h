#pragma once
#include "../../Archetype/ArchetypeFunctions.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../GameObject/GameObject.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Collisions/ICollider.h"
#include "../../Time/Time.h"
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../Asteroid/AsteroidMachine.h"

class AsteroidMachine;

class AsteroidBig : public State
{
public:
	AsteroidBig(const char* name = nullptr);
	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();
	void OnCollisionStarted(GameObject* other)override;

	TransformComponent* mTransform;
	AsteroidMachine* castedMachine;
	Collider* mCollider;
	glm::vec3 mDirection;
	int mSpeed;
	float pSpeed;
	bool bHitByPlayer = false;
	bool bCollided = false;
};

class AsteroidMedium : public State
{
public:
	AsteroidMedium(const char* name = nullptr);
	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();
	void OnCollisionStarted(GameObject* other)override;

	TransformComponent* mTransform;
	AsteroidMachine* castedMachine;
	glm::vec3 mDirection;
	int mSpeed;
	float pSpeed;
	bool bHitByPlayer = false;
	bool bCollided = false;
};

class AsteroidSmall : public State
{
public:
	AsteroidSmall(const char* name = nullptr);
	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();
	void OnCollisionStarted(GameObject* other)override;

	TransformComponent* mTransform;
	AsteroidMachine* castedMachine;
	glm::vec3 mDirection;
	int mSpeed;
	float pSpeed;
	bool bHitByPlayer = false;
	bool bCollided = false;
};