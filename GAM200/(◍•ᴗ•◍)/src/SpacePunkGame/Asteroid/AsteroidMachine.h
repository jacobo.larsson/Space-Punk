#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"
#include "../../StateMachines/State/State.h"
#include "../../GameObject/GameObject.h"

class TransformComponent;

class AsteroidMachine : public StateMachine
{
	SM_RTTI_DECL(AsteroidMachine, StateMachine);

public:
	AsteroidMachine();
	~AsteroidMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	AsteroidMachine& operator=(const AsteroidMachine& rhs);
	AsteroidMachine* Clone()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	void OnCollisionStarted(GameObject* other);

	TransformComponent* pTransform;
	RigidBody* mRigidBody;
	RigidBody* pRigidBody;
	glm::vec3 playerToAster;
	float bigSpeedMax = 400.0f;
	float bigSpeedMin = 300.0f;

	float mediumSpeedMax;
	float mediumSpeedMin;

	float smallSpeedMax;
	float smallSpeedMin;

	float rotationSpeed = 0;

private:
	TransformComponent* mTransform;
};