#pragma once
#include"AsteroidStates.h"

#pragma region AsteroidBig
AsteroidBig::AsteroidBig(const char* name) : State(name) {}

void AsteroidBig::Initialize()
{
	castedMachine = dynamic_cast<AsteroidMachine*>(mOwnerStateMachine);

	mTransform = mActor->GetComp<TransformComponent>();
	mCollider = mActor->GetComp<Collider>();

	mDirection.x = (float)rand() / RAND_MAX;
	mDirection.y = (float)rand() / RAND_MAX;

	if (rand() % 2 == 0)
		mDirection.x = -mDirection.x;

	if (rand() % 2 == 0)
		mDirection.y = -mDirection.y;

	glm::normalize(mDirection);

	mSpeed = rand() % (int)castedMachine->bigSpeedMax + (int)castedMachine->bigSpeedMin;

	State::Initialize();
}

void AsteroidBig::LogicEnter()
{
	mTimeInState = 0;
}

void AsteroidBig::LogicExit()
{
}

void AsteroidBig::LogicUpdate()
{
	if (mCollider->Enabled())
	{
		if (bHitByPlayer)
			castedMachine->mRigidBody->Velocity = glm::vec2{ castedMachine->playerToAster.x * pSpeed / 100, castedMachine->playerToAster.y * pSpeed / 100 };

		else
		{
			mTransform->SetWorldPosition(glm::vec3(mTransform->GetWorldPosition().x + mDirection.x * (float)mSpeed * TimeSys->GetFrameTime(),
				mTransform->GetWorldPosition().y + mDirection.y * (float)mSpeed * TimeSys->GetFrameTime(),
				mTransform->GetWorldPosition().z));

			castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
			glm::normalize(castedMachine->playerToAster);
		}
	}
}

void AsteroidBig::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("EvadingHitbox"))
		return;

	pSpeed = glm::length(castedMachine->pRigidBody->Velocity);

	if (other->HasTag("Player"))
	{
		castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
		glm::normalize(castedMachine->playerToAster);
		bHitByPlayer = true;
	}

	else if (other->HasTag("Enemy"))
		other->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(other);

	if (!other->HasTag("Player") && mTimeInState > 0.3f && !bCollided)
	{
		if (other->HasTag("EnemyBullet"))
			other->SetEnabled(false);
			//SceneSys->GetMainSpace()->DestroyObject(other);

		GameObject* asteroid1 = CreateFromArchetype("AsteroidMedium", SceneSys->GetMainSpace(), mTransform->GetWorldPosition() - mDirection);
		GameObject* asteroid2 = CreateFromArchetype("AsteroidMedium", SceneSys->GetMainSpace(), mTransform->GetWorldPosition() + mDirection);

		asteroid1->GetComp<BrainComponent>()->GetStateMachine("AsteroidMachine")->ChangeState("AsteroidMedium");
		asteroid2->GetComp<BrainComponent>()->GetStateMachine("AsteroidMachine")->ChangeState("AsteroidMedium");

		//SceneSys->GetMainSpace()->DestroyObject(mActor);
		mActor->SetEnabled(false);
		bCollided = true;
	}

}
#pragma endregion

AsteroidMedium::AsteroidMedium(const char* name) : State(name) {}

void AsteroidMedium::Initialize()
{
	castedMachine = dynamic_cast<AsteroidMachine*>(mOwnerStateMachine);

	mTransform = mActor->GetComp<TransformComponent>();

	castedMachine->rotationSpeed = (float)rand() / RAND_MAX;
	if (rand() % 2 == 0)
		castedMachine->rotationSpeed = -castedMachine->rotationSpeed;

	mDirection.x = (float)rand() / RAND_MAX;
	mDirection.y = (float)rand() / RAND_MAX;

	if (rand() % 2 == 0)
		mDirection.x = -mDirection.x;

	if (rand() % 2 == 0)
		mDirection.y = -mDirection.y;

	glm::normalize(mDirection);

	mSpeed = rand() % (int)castedMachine->bigSpeedMax + (int)castedMachine->bigSpeedMin;

	State::Initialize();
}

void AsteroidMedium::LogicEnter()
{
	mTimeInState = 0;
}

void AsteroidMedium::LogicExit()
{
}

void AsteroidMedium::LogicUpdate()
{
	mTransform->SetWorldOrientation(mTransform->GetWorldOrientation() + castedMachine->rotationSpeed * TimeSys->GetFrameTime());

	if (bHitByPlayer)
		castedMachine->mRigidBody->Velocity = glm::vec2{ castedMachine->playerToAster.x * pSpeed / 100, castedMachine->playerToAster.y * pSpeed / 100 };

	else
	{
		mTransform->SetWorldPosition(glm::vec3(mTransform->GetWorldPosition().x + mDirection.x * (float)mSpeed * TimeSys->GetFrameTime(),
			mTransform->GetWorldPosition().y + mDirection.y * (float)mSpeed * TimeSys->GetFrameTime(),
			mTransform->GetWorldPosition().z));

		castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
		glm::normalize(castedMachine->playerToAster);
	}
}

void AsteroidMedium::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("EvadingHitbox"))
		return;

	pSpeed = glm::length(castedMachine->pRigidBody->Velocity);

	if (other->HasTag("Player"))
	{
		castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
		glm::normalize(castedMachine->playerToAster);
		bHitByPlayer = true;
	}

	else if (other->HasTag("Enemy"))
		other->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(other);

	if (!other->HasTag("Player") && mTimeInState > 0.3f && !bCollided)
	{
		if (other->HasTag("EnemyBullet"))
			other->SetEnabled(false);
			//SceneSys->GetMainSpace()->DestroyObject(other);

		GameObject* asteroid1 = CreateFromArchetype("AsteroidSmall", SceneSys->GetMainSpace(), mTransform->GetWorldPosition() + mDirection);
		GameObject* asteroid2 = CreateFromArchetype("AsteroidSmall", SceneSys->GetMainSpace(), mTransform->GetWorldPosition() - mDirection);
		asteroid1->GetComp<BrainComponent>()->GetStateMachine("AsteroidMachine")->ChangeState("AsteroidSmall");
		asteroid2->GetComp<BrainComponent>()->GetStateMachine("AsteroidMachine")->ChangeState("AsteroidSmall");

		//SceneSys->GetMainSpace()->DestroyObject(mActor);
		mActor->SetEnabled(false);
		bCollided = true;
	}
}

AsteroidSmall::AsteroidSmall(const char* name) : State(name) {}

void AsteroidSmall::Initialize()
{
	castedMachine = dynamic_cast<AsteroidMachine*>(mOwnerStateMachine);

	mTransform = mActor->GetComp<TransformComponent>();

	castedMachine->rotationSpeed = (float)rand() / RAND_MAX;
	if (rand() % 2 == 0)
		castedMachine->rotationSpeed = -castedMachine->rotationSpeed;

	mDirection.x = (float)rand() / RAND_MAX;
	mDirection.y = (float)rand() / RAND_MAX;

	if (rand() % 2 == 0)
		mDirection.x = -mDirection.x;

	if (rand() % 2 == 0)
		mDirection.y = -mDirection.y;

	glm::normalize(mDirection);

	mSpeed = rand() % (int)castedMachine->bigSpeedMax + (int)castedMachine->bigSpeedMin;

	State::Initialize();
}

void AsteroidSmall::LogicEnter()
{
}

void AsteroidSmall::LogicExit()
{
}

void AsteroidSmall::LogicUpdate()
{
	mTransform->SetWorldOrientation(mTransform->GetWorldOrientation() + castedMachine->rotationSpeed * 1.5f * TimeSys->GetFrameTime());

	if (bHitByPlayer)
		castedMachine->mRigidBody->Velocity = glm::vec2{ castedMachine->playerToAster.x * pSpeed / 100, castedMachine->playerToAster.y * pSpeed / 100 };

	else
	{
		mTransform->SetWorldPosition(glm::vec3(mTransform->GetWorldPosition().x + mDirection.x * (float)mSpeed * TimeSys->GetFrameTime(),
			mTransform->GetWorldPosition().y + mDirection.y * (float)mSpeed * TimeSys->GetFrameTime(),
			mTransform->GetWorldPosition().z));

		castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
		glm::normalize(castedMachine->playerToAster);
	}
}

void AsteroidSmall::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("EvadingHitbox"))
		return;

	pSpeed = glm::length(castedMachine->pRigidBody->Velocity);

	if (other->HasTag("Player"))
	{
		castedMachine->playerToAster = mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
		glm::normalize(castedMachine->playerToAster);
		bHitByPlayer = true;
	}

	else if (other->HasTag("Enemy"))
		other->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(other);

	if (other->HasTag("EnemyBullet"))
	{
		mActor->SetEnabled(false);
		other->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(other);
	}

	else if (!other->HasTag("Player") && mTimeInState > 0.3f && !bCollided)
	{ 
		mActor->SetEnabled(false);
		bCollided = true;
	}
		//SceneSys->GetMainSpace()->DestroyObject(mActor);
	
}
