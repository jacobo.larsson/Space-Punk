#include "ScreenWraper.h"

ScreenWraperComponent::ScreenWraperComponent()
{
	mName = "ScreenWraperComponent";
}

ScreenWraperComponent& ScreenWraperComponent::operator=(ScreenWraperComponent& rhs)
{
	//mDirection = rhs.mDirection;
	//mBulletSpeed = rhs.mBulletSpeed;
	return (*this);
}

ScreenWraperComponent* ScreenWraperComponent::Clone()
{
	ScreenWraperComponent* temp = FactorySys->Create<ScreenWraperComponent>();
	(*temp) = (*this);
	return temp;
}

void ScreenWraperComponent::Initialize()
{
	mCamera = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraComponent>()->GetCamera();
	mTransform = mOwner->GetComp<TransformComponent>();
	mWaveComp = mOwner->GetComp<WaveComponent>();

	LogicComponent::Initialize();
}

void ScreenWraperComponent::Update()
{
	glm::vec3 mPos = mTransform->GetWorldPosition();

	//right edge
	if (mPos.x - mTransform->GetWorldScale().x / 2 > mCamera->mViewRectangle.x / 2)
		mTransform->SetWorldPosition(glm::vec3{ -mCamera->mViewRectangle.x / 2 - mTransform->GetWorldScale().x / 2, mPos.y, mPos.z });

	//left edge
	if (mPos.x + mTransform->GetWorldScale().x / 2 < -mCamera->mViewRectangle.x / 2)
		mTransform->SetWorldPosition(glm::vec3{ mCamera->mViewRectangle.x / 2 + mTransform->GetWorldScale().x / 2, mPos.y, mPos.z });

	//top edge
	if (mPos.y - mTransform->GetWorldScale().y / 2 > mCamera->mViewRectangle.y / 2)
		mTransform->SetWorldPosition(glm::vec3{ mPos.x, -mCamera->mViewRectangle.y / 2 - mTransform->GetWorldScale().y / 2, mPos.z });

	//bottom edge
	if (mPos.y + mTransform->GetWorldScale().y / 2 < -mCamera->mViewRectangle.y / 2)
		mTransform->SetWorldPosition(glm::vec3{ mPos.x, mCamera->mViewRectangle.y / 2 + mTransform->GetWorldScale().y / 2, mPos.z });
}

void ScreenWraperComponent::Shutdown()
{
	LogicComponent::Shutdown();
}
