#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../Settings/Settings.h"
#include "../../Graphics/Camera/Camera.h"
#include "../WaveComponent/WaveComp.h"

class ScreenWraperComponent : public LogicComponent
{
	SM_RTTI_DECL(ScreenWraperComponent, LogicComponent);

public:
	ScreenWraperComponent();
	ScreenWraperComponent& operator=(ScreenWraperComponent& rhs);
	ScreenWraperComponent* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	//bool Edit()override;

private:
	TransformComponent* mTransform;
	Camera* mCamera;
	WaveComponent* mWaveComp;
};