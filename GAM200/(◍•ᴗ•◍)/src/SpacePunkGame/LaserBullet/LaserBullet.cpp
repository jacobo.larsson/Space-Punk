#pragma once
#include <cmath>
#include "../../Animation/Path2D.h"
#include "../../Animation/Easing.h"
#include "LaserBullet.h"

LaserBulletComponent::LaserBulletComponent()
{
	mName = "LaserBulletComponent";
}

LaserBulletComponent::LaserBulletComponent(LaserBulletComponent& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

LaserBulletComponent& LaserBulletComponent::operator=(LaserBulletComponent& rhs)
{
	mDirection = rhs.mDirection;
	mBulletSpeed = rhs.mBulletSpeed;
	return (*this);
}

LaserBulletComponent* LaserBulletComponent::Clone()
{
	LaserBulletComponent* temp = FactorySys->Create<LaserBulletComponent>();
	(*temp) = (*this);
	return temp;
}

void LaserBulletComponent::Initialize()
{
	mTransform = mOwner->GetComp<TransformComponent>();
	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();

	mDirection.x = cos(pTransform->GetOrientation());
	mDirection.y = sin(pTransform->GetOrientation());
	glm::normalize(mDirection);

	//mOriginalPos = mTransform->GetWorldTransform().mPosition;
	//mDestination = mOriginalPos + mDirection * mBulletSpeed * mDestructionTime *(4.0f);

	LogicComponent::Initialize();
}

void LaserBulletComponent::Update()
{
	mTimer += TimeSys->GetFrameTime();

	glm::vec3 pos;
 	pos.x = mTransform->GetPosition().x + mDirection.x * mBulletSpeed * TimeSys->GetFrameTime();
	pos.y = mTransform->GetPosition().y + mDirection.y * mBulletSpeed * TimeSys->GetFrameTime();
	pos.z = mTransform->GetPosition().z;
	
	mTransform->SetWorldPosition(pos);

	mDestructionTimer += TimeSys->GetFrameTime();

	if (mDestructionTimer > mDestructionTime)
		SceneSys->GetMainSpace()->DestroyObject(mOwner);
		//mOwner->SetEnabled(false);
}

void LaserBulletComponent::Shutdown()
{
	LogicComponent::Shutdown();
}

bool LaserBulletComponent::Edit()
{
	bool changed = false;
	ImGui::PushID(&bChargedChild);
	ImGui::Checkbox("Charged Child", &bChargedChild);
	ImGui::PopID();
	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;
	return changed;
}

void LaserBulletComponent::OnCollisionStarted(GameObject* other)
{
	if (!other->HasTag("Player") && mTimer > 0.2f)
	{
		mOwner->SetEnabled(false);
		//SceneSys->GetMainSpace()->DestroyObject(mOwner);

		if (bChargedShot)
		{
			mOwner->SetEnabled(false);
			//SceneSys->GetMainSpace()->DestroyObject(mOwner);

			const int laserChilds = 18;

			GameObject* laserBullet[laserChilds];

			float deltaAngle = 2 * 3.141590f / laserChilds;

			for (int i = 0; i < laserChilds; i++)
			{
				laserBullet[i] = CreateFromArchetype("ChargedChild", SceneSys->GetMainSpace(), mTransform->GetWorldPosition());

				TransformComponent* laserTransform = laserBullet[i]->GetComp<TransformComponent>();
				laserTransform->SetWorldOrientation(3.141590f / 2 + (i * deltaAngle));

				LaserBulletComponent* laserComp = laserBullet[i]->GetComp<LaserBulletComponent>();
				laserComp->mDirection.x = cos(i * deltaAngle);
				laserComp->mDirection.y = sin(i * deltaAngle);

				laserComp->mDestructionTime = 0.5f;

				laserComp->bChargedShot = true;
			}
		}
	}
}

void LaserBulletComponent::ToJson(nlohmann::json& j)
{
	j["mDestructionTime"] = mDestructionTime;
}

void LaserBulletComponent::FromJson(const nlohmann::json& j)
{
	if (j.find("mDestructionTime") != j.end())
		mDestructionTime = j["mDestructionTime"];
}

glm::vec3 LaserBulletComponent::BulletLerp(const glm::vec3& start, const glm::vec3& end, float tn)
{
	float temp_x = start.x + (end.x - start.x) * EaseInQuad(tn); //interpolation of x
	float temp_y = start.y + (end.y - start.y) * EaseOutQuad(tn); //interpolation of y

	glm::vec3 vector = { temp_x, temp_y, 0 }; //set the vector

	return vector; //return the vector
}
