#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../Settings/Settings.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/State/State.h"
#include "../PlayerShipAttack/PlayerShipAttackMachine.h"

class LaserBulletComponent : public LogicComponent
{
	SM_RTTI_DECL(LaserBulletComponent, LogicComponent);
	
public: 
	LaserBulletComponent();
	LaserBulletComponent(LaserBulletComponent& rhs);
	LaserBulletComponent& operator=(LaserBulletComponent& rhs);
	LaserBulletComponent* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	void OnCollisionStarted(GameObject* other)override;
	glm::vec3 BulletLerp(const glm::vec3& start, const glm::vec3& end, float tn);

	glm::vec3 mDirection;
	float mDestructionTime = 2.0f;
	bool bChargedShot = false;
	bool bChargedChild = false;

private:
	TransformComponent* pTransform;
	TransformComponent* mTransform;
	glm::vec3 mOriginalPos;
	glm::vec3 mDestination;
	float mTimer = 0.0f;
	float mBulletSpeed = 2000.0f;
	float mDestructionTimer = 0.0f;
};