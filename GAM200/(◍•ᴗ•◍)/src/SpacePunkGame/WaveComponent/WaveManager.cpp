#include "WaveManager.h"

WaveManager::WaveManager()
{
	mName = "WaveManager";
}

WaveManager& WaveManager::operator=(WaveManager& rhs)
{
	return (*this);
}

WaveManager* WaveManager::Clone()
{
	WaveManager* temp = FactorySys->Create<WaveManager>();
	(*temp) = (*this);
	return temp;
}

void WaveManager::Initialize()
{
	//mCam = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<Camera>();

	LogicComponent::Initialize();
}

void WaveManager::Update()
{
	//auto objectsVector = SceneSys->GetMainSpace()->GetAllObjects();
	//bool enemiesDead = true;
	//bool insideViewRect = false;
	//
	//for (auto it = objectsVector.begin(); it != objectsVector.end(); it++)
	//{
	//	if ((*it)->HasComp<TransformComponent>())
	//	{
	//		glm::vec3 mPos = (*it)->GetComp<TransformComponent>()->GetWorldPosition();
	//
	//		if (mPos.x < 6000 / 2 && mPos.x > -6000 &&
	//			mPos.y < 1750 / 2 && mPos.y > -1750 / 2)
	//		{
	//			insideViewRect = true;
	//		}
	//
	//		if ((*it)->Enabled() && insideViewRect && ((*it)->HasTag("Enemy") || (*it)->HasTag("HeavyEnemy")))
	//		{
	//			enemiesDead = false;
	//		}
	//	}
	//}
	//
	//if (enemiesDead)
	//{
	//	timeCounter += TimeSys->GetFrameTime();
	//	if (timeCounter >= waitingTime)
	//	{
	//		SceneSys->ChangeLevelGame("SpacePunkLvl");
	//		SceneSys->to_change = true;
	//	}
	//}
	//
 	//if (mKillCount >= enemiesCount)
	//{
	//	timeCounter += TimeSys->GetFrameTime();
	//	if (timeCounter >= waitingTime)
	//	{
	//		SceneSys->ChangeLevelGame("SpacePunkLvl");
	//		SceneSys->to_change = true;
	//	}
	//}
}

void WaveManager::Shutdown()
{
	LogicComponent::Shutdown();
}

bool WaveManager::Edit()
{
	bool changed = false;
	ImGui::PushID(&enemiesCount);
	ImGui::DragInt("Enemies count", &enemiesCount);
	ImGui::PopID();

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

void WaveManager::ToJson(nlohmann::json& j)
{
	j["enemiesCount"] = enemiesCount;
}

void WaveManager::FromJson(const nlohmann::json& j)
{
	if (j.find("enemiesCount") != j.end())
		enemiesCount = j["enemiesCount"];
}
