#include "WaveComp.h"

WaveComponent::WaveComponent()
{
	mName = "WaveComponent";
}

WaveComponent& WaveComponent::operator=(WaveComponent& rhs)
{
	//mDirection = rhs.mDirection;
	//mBulletSpeed = rhs.mBulletSpeed;
	return (*this);
}

WaveComponent* WaveComponent::Clone()
{
	WaveComponent* temp = FactorySys->Create<WaveComponent>();
	(*temp) = (*this);
	return temp;
}

void WaveComponent::Initialize()
{
	mCamera = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraComponent>()->GetCamera();
	mTransform = mOwner->GetComp<TransformComponent>();
	mCollider = mOwner->GetComp<Collider>();
	mRigidBody = mOwner->GetComp<RigidBody>();

	mCollider->SetEnabled(false);

	LogicComponent::Initialize();
}

void WaveComponent::Update()
{
	timeCounter += TimeSys->GetFrameTime();
	if (timeCounter < timeWainting)
	{
		mRigidBody->Velocity = glm::vec2{ 0,0 };
		return;
	}

	glm::vec3 mPos = mTransform->GetWorldPosition();

	//right edge
	if (mPos.x - mTransform->GetWorldScale().x / 2 > mCamera->mViewRectangle.x / 2)
	{
		mRigidBody->Velocity += glm::vec2{ -200, 0 };

	}

	//left edge
	else if (mPos.x + mTransform->GetWorldScale().x / 2 < -mCamera->mViewRectangle.x / 2)
	{
		mRigidBody->Velocity += glm::vec2{ 200, 0 };
	}

	//top edge
	else if (mPos.y - mTransform->GetWorldScale().y / 2 > mCamera->mViewRectangle.y / 2)
	{
		mRigidBody->Velocity += glm::vec2{ 0, -200 };
	}

	//bottom edge
	else if (mPos.y + mTransform->GetWorldScale().y / 2 < -mCamera->mViewRectangle.y / 2)
	{
		mRigidBody->Velocity += glm::vec2{ 0, 200 };
	}

	else
		mCollider->SetEnabled(true);
}

void WaveComponent::Shutdown()
{
	LogicComponent::Shutdown();
}

bool WaveComponent::Edit()
{
	bool changed = false;
	ImGui::PushID(&timeWainting);
	ImGui::DragFloat("Time wainting", &timeWainting);
	ImGui::PopID();

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

void WaveComponent::ToJson(nlohmann::json& j)
{
	j["timeWainting"] = timeWainting;
}

void WaveComponent::FromJson(const nlohmann::json& j)
{
	if (j.find("timeWainting") != j.end())
		timeWainting = j["timeWainting"];
}
