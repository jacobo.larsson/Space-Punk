#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../Settings/Settings.h"
#include "../../Graphics/Camera/Camera.h"
#include "../../Collisions/ICollider.h"
#include "../../Physics/RigidBody.h"

class WaveComponent : public LogicComponent
{
	SM_RTTI_DECL(WaveComponent, LogicComponent);

public:
	WaveComponent();
	WaveComponent& operator=(WaveComponent& rhs);
	WaveComponent* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	bool bIsInsideViewRect = false;

private:
	TransformComponent* mTransform;
	Camera* mCamera;
	Collider* mCollider;
	RigidBody* mRigidBody;

	float timeCounter = 0.0f;
	float timeWainting = 5.0f;
};