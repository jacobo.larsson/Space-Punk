#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"


class RigidBody;
class TransformComponent;


class PlayerShipAttackMachine : public StateMachine
{
	SM_RTTI_DECL(PlayerShipAttackMachine, StateMachine);

public:
	PlayerShipAttackMachine();
	~PlayerShipAttackMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	//bool Edit()override;
	//PlayerShipAttackMachine& operator=(const PlayerShipAttackMachine& rhs);
	//PlayerShipAttackMachine* Clone()override;
	//
	//void ToJson(nlohmann::json& j) override;
	//void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;

	float mTimeCounter = 0.0f;
private:
	TransformComponent* pTransform;
	
};