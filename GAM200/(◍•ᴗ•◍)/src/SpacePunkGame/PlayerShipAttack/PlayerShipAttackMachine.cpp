#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"
#include "../../Factory/Factory.h" 

#include "../../GameObject/GameObject.h"

#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"

#include "../../Time/Time.h"

#include "PlayerShipAttackMachine.h"
#include "PlayerShipAttackStates.h"

PlayerShipAttackMachine::PlayerShipAttackMachine()
{
	mName = "PlayerShipAttackMachine";

	mInitialState = new PlayerShipHold("PlayerShipHold");
	mCurrentState = mInitialState;

	AddState(mInitialState);
	AddState(new PlayerShipFirstAttack("PlayerShipFirstAttack"));
	AddState(new PlayerShipSecondAttack("PlayerShipSecondAttack"));
	AddState(new PlayerShipThirdAttack("PlayerShipThirdAttack"));
}

PlayerShipAttackMachine::~PlayerShipAttackMachine()
{
	StateMachine::~StateMachine();
}

void PlayerShipAttackMachine::Initialize()
{
	StateMachine::Initialize();

	pTransform = mActor->GetComp<TransformComponent>();
}

void PlayerShipAttackMachine::Update()
{
	mTimeCounter += TimeSys->GetFrameTime();

	StateMachine::Update();
}

void PlayerShipAttackMachine::Shutdown()
{
	StateMachine::Clear();
}

void PlayerShipAttackMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);
}
