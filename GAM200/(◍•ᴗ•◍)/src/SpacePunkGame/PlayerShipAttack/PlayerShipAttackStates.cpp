#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"

#include "../../GameObject/GameObject.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Physics/RigidBody.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../Time/Time.h"
#include "../../SpacePunkGame/LaserBullet/LaserBullet.h"
#include "../../Archetype/ArchetypeFunctions.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../SpacePunkGame/LaserBullet/LaserBullet.h"

#include "PlayerShipAttackStates.h"
#include "PlayerShipAttackMachine.h"

#pragma region PlayerShipHold
PlayerShipHold::PlayerShipHold(const char* name) : State(name) {}

void PlayerShipHold::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipAttackMachine*>(mOwnerStateMachine);

	if (mActor->HasComp<TransformComponent>())
		pTransform = mActor->GetComp<TransformComponent>();

	pRendereable = mActor->GetComp<Renderable>();

	State::Initialize();
}

void PlayerShipHold::LogicEnter()
{
	mTimeInState = 0.0f;

	pRendereable->SetModulationColor(Color());
	pRendereable->SetVisible(true);
}

void PlayerShipHold::LogicExit()
{
}

void PlayerShipHold::LogicUpdate()
{
	if (GameInput::JumpKeyPressed() && castedMachine->mTimeCounter < 15.0f)
		castedMachine->ChangeState("PlayerShipFirstAttack");

	else if (GameInput::JumpKeyPressed() && castedMachine->mTimeCounter < 30.0f && castedMachine->mTimeCounter > 15.0f)
		castedMachine->ChangeState("PlayerShipSecondAttack");

	else if (GameInput::JumpKeyPressed() && castedMachine->mTimeCounter > 30.0f)
		castedMachine->ChangeState("PlayerShipThirdAttack");
}
#pragma endregion

#pragma region PlayerShipFirstAttack
PlayerShipFirstAttack::PlayerShipFirstAttack(const char* name) : State(name) {}

void PlayerShipFirstAttack::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipAttackMachine*>(mOwnerStateMachine);

	if (mActor->HasComp<TransformComponent>())
		pTransform = mActor->GetComp<TransformComponent>(); 

	pCollider = mActor->GetComp<Collider>();

	State::Initialize();
}

void PlayerShipFirstAttack::LogicEnter()
{
}

void PlayerShipFirstAttack::LogicExit()
{
}

void PlayerShipFirstAttack::LogicUpdate() 
{
	timeCounter += TimeSys->GetFrameTime();

	if (pCollider->Enabled() && timeCounter > fireRate)
	{
        GameObject* laserBullet = CreateFromArchetype("LaserBullet", SceneSys->GetMainSpace(), pTransform->GetPosition());
		timeCounter = 0.0f;

		TransformComponent* laserTransform = laserBullet->GetComp<TransformComponent>();

		laserTransform->SetWorldOrientation(pTransform->GetWorldOrientation() + 3.141590f / 2);
	}

	else
		castedMachine->ChangeState("PlayerShipHold");
}
#pragma endregion

#pragma region PlayerShipSecondAttack
PlayerShipSecondAttack::PlayerShipSecondAttack(const char* name) : State(name) {}

void PlayerShipSecondAttack::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipAttackMachine*>(mOwnerStateMachine);

	if (mActor->HasComp<TransformComponent>())
		pTransform = mActor->GetComp<TransformComponent>();

	pCollider = mActor->GetComp<Collider>();

	State::Initialize();
}

void PlayerShipSecondAttack::LogicEnter()
{
}

void PlayerShipSecondAttack::LogicExit()
{
}

void PlayerShipSecondAttack::LogicUpdate()
{
	timeCounter += TimeSys->GetFrameTime();

	if (pCollider->Enabled() && timeCounter > fireRate)
	{
		GameObject* laserBullet[4];
		
		float deltaAngle = (3.141590f / 4) / bulletCount;

		for (int i = 0; i < bulletCount; i++)
		{
			laserBullet[i] = CreateFromArchetype("LaserBullet", SceneSys->GetMainSpace(), pTransform->GetPosition());

			TransformComponent* laserTransform = laserBullet[i]->GetComp<TransformComponent>();
			laserTransform->SetWorldOrientation(pTransform->GetWorldOrientation() + 3.141590f / 2 - 3.141590f / 8 + (i * deltaAngle));

			LaserBulletComponent* laserComp = laserBullet[i]->GetComp<LaserBulletComponent>();
			laserComp->mDirection.x = cos(pTransform->GetWorldOrientation() - 3.141590f / 8 + (i * deltaAngle));
			laserComp->mDirection.y = sin(pTransform->GetWorldOrientation() - 3.141590f / 8 + (i * deltaAngle));

			laserComp->mDestructionTime = 0.5f;
		}

		timeCounter = 0.0f;
	}

	else
		castedMachine->ChangeState("PlayerShipHold");
}
#pragma endregion

#pragma region PlayerShipThirdAttack
PlayerShipThirdAttack::PlayerShipThirdAttack(const char* name) : State(name) {}

void PlayerShipThirdAttack::Initialize()
{
	castedMachine = dynamic_cast<PlayerShipAttackMachine*>(mOwnerStateMachine);

	if (mActor->HasComp<TransformComponent>())
		pTransform = mActor->GetComp<TransformComponent>();

	pCollider = mActor->GetComp<Collider>();
	pRendereable = mActor->GetComp<Renderable>();

	State::Initialize();
}

void PlayerShipThirdAttack::LogicEnter()
{
	shotIsCharged = false;
}

void PlayerShipThirdAttack::LogicExit()
{
}

void PlayerShipThirdAttack::LogicUpdate()
{
	if (GameInput::JumpKeyPressed())
	{
		if (pRendereable->GetModulationColor().g >= 0.4f)
		{
			pRendereable->SetModulationColor(Color(pRendereable->GetModulationColor().r,
												   pRendereable->GetModulationColor().g - 1.0f / chargeTime * TimeSys->GetFrameTime(),
												   pRendereable->GetModulationColor().b - 1.0f / chargeTime * TimeSys->GetFrameTime(),
												   pRendereable->GetModulationColor().a));
		}

		timeCounter += TimeSys->GetFrameTime();
		if (timeCounter > chargeTime)
		{
			//pRendereable->SetVisible(false);
			shotIsCharged = true;

			blikingTimer += TimeSys->GetFrameTime();
			if (blikingTimer >= blikingTime)
			{
				//pRendereable->SetVisible(true);
				blikingTimer = 0.0f;

			}
		}
	}

	else
	{
		castedMachine->ChangeState("PlayerShipHold");
		timeCounter = 0.0f;

		if (shotIsCharged)
		{
			GameObject* laserBullet = CreateFromArchetype("ChargedBall", SceneSys->GetMainSpace(), pTransform->GetPosition());
			laserBullet->GetComp<LaserBulletComponent>()->bChargedShot = true;
		}
	}
}
#pragma endregion