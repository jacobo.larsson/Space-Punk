#pragma once
#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../StateMachines/StateMachine/StateMachine.h"
#include "../../StateMachines/SuperState/SuperState.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Collisions/ICollider.h"
#include "../../Graphics/Renderable/Renderable.h"

class PlayerShipAttackMachine;

class PlayerShipHold : public State
{
public:
	PlayerShipHold(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* pTransform;
	Renderable* pRendereable;
	PlayerShipAttackMachine* castedMachine;
};

class PlayerShipFirstAttack : public State
{
public:
	PlayerShipFirstAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* pTransform;
	Collider* pCollider;
	PlayerShipAttackMachine* castedMachine;
	float timeCounter = 0.0f;
	float fireRate = 0.3f;
};

class PlayerShipSecondAttack : public State
{
public:
	PlayerShipSecondAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* pTransform;
	Collider* pCollider;
	PlayerShipAttackMachine* castedMachine;
	float timeCounter = 0.0f;
	float fireRate = 0.3f;
	int bulletCount = 5;
};

class PlayerShipThirdAttack : public State
{
public:
	PlayerShipThirdAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* pTransform;
	PlayerShipAttackMachine* castedMachine;
	Collider* pCollider;
	Renderable* pRendereable;
	float timeCounter = 0.0f;
	float fireRate = 0.3f;
	float chargeTime = 1.0f;
	float blikingTime = 0.3f;
	float blikingTimer = 0.0f;
	bool shotIsCharged = false;
};