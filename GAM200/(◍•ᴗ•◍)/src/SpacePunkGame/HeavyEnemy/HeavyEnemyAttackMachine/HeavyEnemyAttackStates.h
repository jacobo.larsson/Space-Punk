#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Collisions/ICollider.h"

class HeavyEnemyAttackMachine;

class HeavyEnemyHold : public State
{
public:
	HeavyEnemyHold(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	HeavyEnemyAttackMachine* castedMachine;
};

class HeavyEnemyAttack : public State
{
public:
	HeavyEnemyAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	HeavyEnemyAttackMachine* castedMachine;
	Collider* mCollider;
	float timeCounter;
	float bulletTimeCounter = 0.0f;
	float bulletTimeCharging = 2.0f;
	float fireRate = 3.0f;
	GameObject* laserBullet = nullptr;
	bool fired = false;
};