#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"

#include "../../../GameObject/GameObject.h"
#include "../../../Space/Space.h"
#include "../../../Scene/Scene.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Time/Time.h"
#include "../../../SpacePunkGame/LaserBullet/LaserBullet.h"
#include "../../../Archetype/ArchetypeFunctions.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../HeavyEnemyLaserBulletComp/HeavyEnemyLaserBulletComp.h"

#include "HeavyEnemyAttackMachine.h"
#include "HeavyEnemyAttackStates.h"

#pragma region HeavyEnemyHold
HeavyEnemyHold::HeavyEnemyHold(const char* name) : State(name) {}

void HeavyEnemyHold::Initialize()
{
	castedMachine = dynamic_cast<HeavyEnemyAttackMachine*>(mOwnerStateMachine);

	State::Initialize();
}

void HeavyEnemyHold::LogicEnter()
{
}

void HeavyEnemyHold::LogicExit()
{
}

void HeavyEnemyHold::LogicUpdate()
{
	float distanceToPlayer = glm::distance(castedMachine->mTransform->GetWorldPosition(), castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer > 2000.0f)
		castedMachine->ChangeState("HeavyEnemyAttack");
}
#pragma endregion

#pragma region HeavyEnemyAttack
HeavyEnemyAttack::HeavyEnemyAttack(const char* name) : State(name) {}

void HeavyEnemyAttack::Initialize()
{
	castedMachine = dynamic_cast<HeavyEnemyAttackMachine*>(mOwnerStateMachine);
	mCollider = mActor->GetComp<Collider>();

	State::Initialize();
}

void HeavyEnemyAttack::LogicEnter()
{
}

void HeavyEnemyAttack::LogicExit()
{
}

void HeavyEnemyAttack::LogicUpdate()
{
	float distanceToPlayer = glm::distance(castedMachine->mTransform->GetWorldPosition(), castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer < 1700.0f)
		castedMachine->ChangeState("HeavyEnemyHold");

	timeCounter += TimeSys->GetFrameTime();
	if (mCollider->Enabled() && timeCounter > fireRate)
	{
		laserBullet = CreateFromArchetype("HeavyEnemyLaserBullet", SceneSys->GetMainSpace(), castedMachine->mTransform->GetPosition());
		fired = false;

		laserBullet->GetComp<HeavyEnemyLaserBulletComp>()->heavyShipTransform = castedMachine->mTransform;
		laserBullet->GetComp<HeavyEnemyLaserBulletComp>()->heavyShip = mActor;

		timeCounter = 0.0f;
	}

	if (laserBullet && !fired)
	{
		bulletTimeCounter += TimeSys->GetFrameTime();
	
		if (bulletTimeCounter >= bulletTimeCharging)
		{
			if (laserBullet->Enabled() && !fired)
			{
				if(laserBullet->HasComp<HeavyEnemyLaserBulletComp>())
					laserBullet->GetComp<HeavyEnemyLaserBulletComp>()->bFired = true;

				fired = true;

				bulletTimeCounter = 0.0f;
			}
		}
	
		//else
		//{
		//	TransformComponent* laserTransform = laserBullet->GetComp<TransformComponent>();
		//
		//	offset = { 150, 150, 1 };
		//	offset.x *= cosf(castedMachine->mTransform->GetWorldOrientation());
		//	offset.y *= sinf(castedMachine->mTransform->GetWorldOrientation());
		//
		//	laserTransform->SetWorldPosition(castedMachine->mTransform->GetPosition() + offset);
		//}
	}
}
#pragma endregion