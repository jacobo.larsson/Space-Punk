#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"
#include "../../../Factory/Factory.h" 

#include "../../../GameObject/GameObject.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"

#include "../../../Time/Time.h"

#include "HeavyEnemyAttackMachine.h"
#include "HeavyEnemyAttackStates.h"

HeavyEnemyAttackMachine::HeavyEnemyAttackMachine()
{
	mName = "HeavyEnemyAttackMachine";

	mInitialState = new HeavyEnemyHold("HeavyEnemyHold");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new HeavyEnemyAttack("HeavyEnemyAttack"));
}

HeavyEnemyAttackMachine::~HeavyEnemyAttackMachine()
{
	StateMachine::~StateMachine();
}

void HeavyEnemyAttackMachine::Initialize()
{
	mTransform = mActor->GetComp<TransformComponent>();
	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	mShipToPlayer = pTransform->GetWorldPosition() - mTransform->GetWorldPosition();

	StateMachine::Initialize();
}

void HeavyEnemyAttackMachine::Update()
{
	mShipToPlayer = pTransform->GetWorldPosition() - mTransform->GetWorldPosition();

	StateMachine::Update();
}

void HeavyEnemyAttackMachine::Shutdown()
{
	StateMachine::Clear();
}

void HeavyEnemyAttackMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);
}
