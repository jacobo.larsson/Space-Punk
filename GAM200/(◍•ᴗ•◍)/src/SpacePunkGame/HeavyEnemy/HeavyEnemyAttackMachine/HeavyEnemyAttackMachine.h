#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"

class TransformComponent;

class HeavyEnemyAttackMachine : public StateMachine
{
	SM_RTTI_DECL(HeavyEnemyAttackMachine, StateMachine);

public:
	HeavyEnemyAttackMachine();
	~HeavyEnemyAttackMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	//bool Edit()override;
	//HeavyEnemyAttackMachine& operator=(const HeavyEnemyAttackMachine& rhs);
	//HeavyEnemyAttackMachine* Clone()override;
	//
	//void ToJson(nlohmann::json& j) override;
	//void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;

	TransformComponent* pTransform;
	TransformComponent* mTransform;
	glm::vec2 mShipToPlayer;
};