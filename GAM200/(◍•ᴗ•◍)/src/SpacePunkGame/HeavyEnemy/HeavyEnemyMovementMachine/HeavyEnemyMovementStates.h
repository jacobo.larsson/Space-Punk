#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../Physics/RigidBody.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Collisions/ICollider.h"

#include "../HeavyEnemyAttackMachine/HeavyEnemyAttackMachine.h"
#include "../HeavyEnemyAttackMachine/HeavyEnemyAttackStates.h"

class HeavyEnemyMovementMachine;

class HeavyEnemyIdle : public State
{
public:
	HeavyEnemyIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	HeavyEnemyMovementMachine* castedMachine;
	Collider* mCollider;
	float mTimeInIdle = 3.0f;
	float distanceToPlayer = 0.0f;
};

class HeavyEnemyGetFarFromPlayer : public State
{
public:
	HeavyEnemyGetFarFromPlayer(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	HeavyEnemyMovementMachine* castedMachine;
	float mMovementTime = 0;
	float angle = 0.0f;
	float distanceToPlayer = 0.0f;
};