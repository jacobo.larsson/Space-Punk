#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../Graphics/Renderable/Renderable.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../WaveComponent/WaveManager.h"

class RigidBody;
class TransformComponent;

class HeavyEnemyMovementMachine : public StateMachine
{
	SM_RTTI_DECL(HeavyEnemyMovementMachine, StateMachine);

public:
	HeavyEnemyMovementMachine();
	~HeavyEnemyMovementMachine();
	
	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	HeavyEnemyMovementMachine& operator=(const HeavyEnemyMovementMachine& rhs);
	HeavyEnemyMovementMachine* Clone()override;
	
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	
	void OnCollisionStarted(GameObject* other) override;
	
	float mMaxVelocity = 600.0f;
	float mCurrentVelocity = 0.0f;
	float mRotatingVelocity = 3.0f;
	float mAcceleration = 750.0f;
	float minDistance = 1700.0f;
	float maxDistance = 2700.0f;
	glm::vec2 mDirection;
	
	TransformComponent* mTransform;
	Renderable* mRenderable;
	RigidBody* mRigidbody;
	Collider* mCollider;
	TransformComponent* pTransform;
	WaveManager* pWaveManager;

private:
	bool bGotHit = false;
	float timeCounter = 0.0f;
	float blinkCounter = 0.0f;
};
