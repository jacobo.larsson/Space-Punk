#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"

#include "../../../GameObject/GameObject.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Time/Time.h"

#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../RayCast/Raycast.h"

#include "HeavyEnemyMovementStates.h"
#include "HeavyEnemyMovementMachine.h"

#pragma region HeavyEnemyIdle
HeavyEnemyIdle::HeavyEnemyIdle(const char* name) : State(name) {}

void HeavyEnemyIdle::Initialize()
{
	castedMachine = dynamic_cast<HeavyEnemyMovementMachine*>(mOwnerStateMachine);
	mCollider = mActor->GetComp<Collider>();

	State::Initialize();
}

void HeavyEnemyIdle::LogicEnter()
{
	mTimeInState = 0.0f;
}

void HeavyEnemyIdle::LogicExit()
{
}

void HeavyEnemyIdle::LogicUpdate()
{
	mTimeInState += TimeSys->GetFrameTime();

	distanceToPlayer = glm::length(castedMachine->mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer < castedMachine->minDistance)
		castedMachine->ChangeState("HeavyEnemyGetFarFromPlayer");

	float angle = atan2f(castedMachine->pTransform->GetWorldPosition().y - castedMachine->mTransform->GetWorldPosition().y,
						 castedMachine->pTransform->GetWorldPosition().x - castedMachine->mTransform->GetWorldPosition().x);

	castedMachine->mTransform->SetWorldOrientation(angle);

	auto& vel = castedMachine->mRigidbody->Velocity;
	float maxVel = castedMachine->mMaxVelocity;

	

	if (distanceToPlayer >= castedMachine->maxDistance)
	{
		glm::vec2 mPos2Player = castedMachine->pTransform->GetWorldPosition() - castedMachine->mTransform->GetWorldPosition();
		mPos2Player = glm::normalize(mPos2Player);

		vel += mPos2Player * 10.0f;
	}

	else if (glm::length(vel) > 10.0f && mCollider->Enabled())
		vel -= vel / 10.0f * (float)TimeSys->GetFrameTime();

	if (vel.length() >= maxVel) {
		castedMachine->mDirection = glm::normalize(vel);
		vel = castedMachine->mDirection * maxVel;
	}
}
#pragma endregion


#pragma region HeavyEnemyGetFarFromPlayer
HeavyEnemyGetFarFromPlayer::HeavyEnemyGetFarFromPlayer(const char* name) : State(name) {}

void HeavyEnemyGetFarFromPlayer::Initialize()
{
	castedMachine = dynamic_cast<HeavyEnemyMovementMachine*>(mOwnerStateMachine);

	State::Initialize();
}

void HeavyEnemyGetFarFromPlayer::LogicEnter()
{
	castedMachine->mDirection = castedMachine->mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
	castedMachine->mDirection = glm::normalize(castedMachine->mDirection);

	angle = atan2f(castedMachine->mDirection.y, castedMachine->mDirection.x);
}

void HeavyEnemyGetFarFromPlayer::LogicExit()
{
}

void HeavyEnemyGetFarFromPlayer::LogicUpdate()
{
	castedMachine->mDirection = castedMachine->mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition();
	castedMachine->mDirection = glm::normalize(castedMachine->mDirection);

	distanceToPlayer = glm::length(castedMachine->mTransform->GetWorldPosition() - castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer >= castedMachine->minDistance)
		castedMachine->ChangeState("HeavyEnemyIdle");

	auto& vel = castedMachine->mRigidbody->Velocity;
	float maxVel = castedMachine->mMaxVelocity;

	vel += castedMachine->mDirection * castedMachine->mAcceleration * (float)TimeSys->GetFrameTime();

	if (vel.length() >= maxVel) {
		castedMachine->mDirection = glm::normalize(vel);
		vel = castedMachine->mDirection * maxVel;
	}

	angle = atan2f(castedMachine->pTransform->GetWorldPosition().y - castedMachine->mTransform->GetWorldPosition().y,
				   castedMachine->pTransform->GetWorldPosition().x - castedMachine->mTransform->GetWorldPosition().x);

	castedMachine->mTransform->SetWorldOrientation(angle);
}
#pragma endregion