#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"

#include "../../../GameObject/GameObject.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Collisions/ICollider.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Time/Time.h"

#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../RayCast/Raycast.h"

#include "HeavyEnemyMovementMachine.h"
#include "HeavyEnemyMovementStates.h"

HeavyEnemyMovementMachine::HeavyEnemyMovementMachine()
{
	mName = "HeavyEnemyMovementMachine";

	mInitialState = new HeavyEnemyIdle("HeavyEnemyIdle");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new HeavyEnemyGetFarFromPlayer("HeavyEnemyGetFarFromPlayer"));
}

HeavyEnemyMovementMachine::~HeavyEnemyMovementMachine()
{
}

void HeavyEnemyMovementMachine::Initialize()
{
	StateMachine::Initialize();

	mTransform = mActor->GetComp<TransformComponent>();
	mRenderable = mActor->GetComp<Renderable>();
	mCollider = mActor->GetComp<Collider>();
	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	pWaveManager = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<WaveManager>();
	mRigidbody = mActor->GetComp<RigidBody>();

	bGotHit = false;
}

void HeavyEnemyMovementMachine::Update()
{
	if (bGotHit)
	{
		blinkCounter += TimeSys->GetFrameTime();
		if (blinkCounter < 0.6f)
		{
			mCollider->SetEnabled(false);
			mRenderable->SetVisible(false);

			timeCounter += TimeSys->GetFrameTime();
			if (timeCounter >= 0.1f)
			{
				mRenderable->SetVisible(true);
				timeCounter = 0.0f;
			}
		}
		
		else
		{
			mRenderable->SetVisible(true);
			mCollider->SetEnabled(true);
		}
	}


	StateMachine::Update();
}

void HeavyEnemyMovementMachine::Shutdown()
{
	StateMachine::Clear();
}

bool HeavyEnemyMovementMachine::Edit()
{
	return false;
}

HeavyEnemyMovementMachine& HeavyEnemyMovementMachine::operator=(const HeavyEnemyMovementMachine& rhs)
{
	return *this;
}

HeavyEnemyMovementMachine* HeavyEnemyMovementMachine::Clone()
{
	HeavyEnemyMovementMachine* temp = FactorySys->Create<HeavyEnemyMovementMachine>();
	*temp = *this;
	return temp;
}

void HeavyEnemyMovementMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
}

void HeavyEnemyMovementMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
}

void HeavyEnemyMovementMachine::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("LaserBullet"))
	{
		if (bGotHit)
		{
			pWaveManager->mKillCount++;

			mActor->SetEnabled(false);
		}
			//SceneSys->GetMainSpace()->DestroyObject(mActor);

		else
			bGotHit = true;
	}
}

