#include <cmath>
#include "HeavyEnemyLaserBulletComp.h"

HeavyEnemyLaserBulletComp::HeavyEnemyLaserBulletComp()
{
	mName = "HeavyEnemyLaserBulletComp";
}

HeavyEnemyLaserBulletComp::HeavyEnemyLaserBulletComp(HeavyEnemyLaserBulletComp& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

HeavyEnemyLaserBulletComp& HeavyEnemyLaserBulletComp::operator=(HeavyEnemyLaserBulletComp& rhs)
{
	mDirection = rhs.mDirection;
	mBulletSpeed = rhs.mBulletSpeed;
	return (*this);
}

HeavyEnemyLaserBulletComp* HeavyEnemyLaserBulletComp::Clone()
{
	HeavyEnemyLaserBulletComp* temp = FactorySys->Create<HeavyEnemyLaserBulletComp>();
	(*temp) = (*this);
	return temp;
}

void HeavyEnemyLaserBulletComp::Initialize()
{
	mTransform = mOwner->GetComp<TransformComponent>();

	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	
	mDirection.x = cos(atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x));
	mDirection.y = sin(atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x));
	glm::normalize(mDirection);

	LogicComponent::Initialize();
}

void HeavyEnemyLaserBulletComp::Update()
{
	mTimer += TimeSys->GetFrameTime();

	if (bFired)
	{
		glm::vec3 pos;
		pos.x = mTransform->GetPosition().x + mDirection.x * mBulletSpeed * TimeSys->GetFrameTime();
		pos.y = mTransform->GetPosition().y + mDirection.y * mBulletSpeed * TimeSys->GetFrameTime();
		pos.z = mTransform->GetPosition().z;

		mTransform->SetWorldPosition(pos);

		mDestructionTimer += TimeSys->GetFrameTime();

		if (mDestructionTimer > mDestructionTime)
		{
			bFired = false;
			mOwner->SetEnabled(false);
			//SceneSys->GetMainSpace()->DestroyObject(mOwner);
		}
	}

	else if (heavyShipTransform)
	{
		glm::vec3 offset = { 150, 150, 1 };
		offset.x *= cosf(heavyShipTransform->GetWorldOrientation());
		offset.y *= sinf(heavyShipTransform->GetWorldOrientation());

		mDirection.x = cosf(heavyShipTransform->GetWorldOrientation());
		mDirection.y = sinf(heavyShipTransform->GetWorldOrientation());
		mDirection = glm::normalize(mDirection);
		
		mTransform->SetWorldPosition(heavyShipTransform->GetPosition() + offset);
	}

	mTransform->SetWorldScale(mTransform->GetWorldScale() + mScaleIncremental * (float)TimeSys->GetFrameTime());

	if (heavyShip && !heavyShip->Enabled())
		mOwner->SetEnabled(false);
}

void HeavyEnemyLaserBulletComp::Shutdown()
{
	LogicComponent::Shutdown();
}

bool HeavyEnemyLaserBulletComp::Edit()
{
	bool changed = false;
	ImGui::PushID(&mDestructionTime);
	ImGui::DragFloat("Destruction Time", &mDestructionTime);
	ImGui::PopID();

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

void HeavyEnemyLaserBulletComp::ToJson(nlohmann::json& j)
{
	j["mDestructionTime"] = mDestructionTime;
}

void HeavyEnemyLaserBulletComp::FromJson(const nlohmann::json& j)
{
	if (j.find("mDestructionTime") != j.end())
		mDestructionTime = j["mDestructionTime"];
}

void HeavyEnemyLaserBulletComp::OnCollisionStarted(GameObject* other)
{
}
