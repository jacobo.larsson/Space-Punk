#include "../../Graphics/Mesh/Mesh.h"
#include "../../Input/Input.h" 
#include "../../Game/GameInput/GameInput.h"
#include "HealthComp.h"

HealthComp::HealthComp()
{
	mName = "HealthComp";
}

HealthComp::HealthComp(HealthComp& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

HealthComp& HealthComp::operator=(HealthComp& rhs)
{
	return (*this);
}

HealthComp* HealthComp::Clone()
{
	HealthComp* temp = FactorySys->Create<HealthComp>();
	(*temp) = (*this);
	return temp;
}

void HealthComp::Initialize()
{
	mRenderable = mOwner->GetComp<Renderable>();
	mCollider = mOwner->GetComp<Collider>();

	mBloodRenderable = SceneSys->GetMainSpace()->FindObjectByName("BloodOverlay")->GetComp<Renderable>();
	mGlassRenderable = SceneSys->GetMainSpace()->FindObjectByName("GlassOverlay")->GetComp<Renderable>();
	mCameraShake = SceneSys->GetMainSpace()->FindObjectByName("Camera")->GetComp<CameraShakeComp>();

	LogicComponent::Initialize();
}

void HealthComp::Update()
{
	if (GameInput::ShieldKeyPressed)
		mHealth += 1000;

	if (mHealth <= 50)
	{
		mBloodRenderable->SetVisible(true);
		bBloodyScreen = true;
	}
	
	if (mHealth <= 30)
		mGlassRenderable->SetVisible(true);
	
	if (mHealth <= 0)
	{
		SceneSys->ResetLevelInGame();
		SceneSys->to_change = true;
	}
		//SceneSys->ChangeLevelGame("SpacePunkLvl2");
	
	if (bGotHit)
	{
		mCollider->SetEnabled(false);
	
		mRenderable->SetVisible(false);
	
		mBlikingTimer += TimeSys->GetFrameTime();
		if (mBlikingTimer >= mBlikingTime)
		{
			mRenderable->SetVisible(true);
			mBlikingTimer = 0.0f;
		}
	
		mTimer += TimeSys->GetFrameTime();
		if (mTimer >= mInvincibilityTime)
		{
			mCollider->SetEnabled(true);
			mTimer = 0.0f;
	
			mRenderable->SetVisible(true);
			bGotHit = false;
		}
	}
}

void HealthComp::Shutdown()
{
	LogicComponent::Shutdown();
}

void HealthComp::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("EnemyBullet"))
	{
		mHealth -= 10;
		bGotHit = true;
		mCameraShake->activateEffect = true;
	
		if (bBloodyScreen)
		{
			if (mBloodRenderable->GetModulationColor().a < 0.4f)
			{
				mBloodRenderable->SetModulationColor(Color{ mBloodRenderable->GetModulationColor().r,
					mBloodRenderable->GetModulationColor().g,
					mBloodRenderable->GetModulationColor().b,
					mBloodRenderable->GetModulationColor().a + 0.05f });
			}
		}
	}
}
