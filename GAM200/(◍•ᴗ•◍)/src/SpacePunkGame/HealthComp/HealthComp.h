#pragma once
#include <glm/glm.hpp>
#include "../../Extern/ImGui/imgui.h"
#include "../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../RTTI/IBase.h"
#include "../../RTTI/SM_RTTI.h"
#include "../../LogicSystem/LogicComponent.h"
#include "../../Transform2D/TransformComponent.h"
#include "../../Space/Space.h"
#include "../../Scene/Scene.h"
#include "../../Time/Time.h"
#include "../../Graphics/Renderable/Renderable.h"
#include "../../Collisions/ICollider.h"
#include "../../Settings/Settings.h"
#include "../CameraShake/CameraShake.h"

class HealthComp : public LogicComponent
{
	SM_RTTI_DECL(HealthComp, LogicComponent);

public:
	HealthComp();
	HealthComp(HealthComp& rhs);
	HealthComp& operator=(HealthComp& rhs);
	HealthComp* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	//bool Edit()override;
	//void ToJson(nlohmann::json& j) override;
	//void FromJson(const nlohmann::json& j) override;
	void OnCollisionStarted(GameObject* other)override;

private:
	Renderable* mRenderable;
	Renderable* mBloodRenderable;
	Renderable* mGlassRenderable;
	Collider* mCollider;
	CameraShakeComp* mCameraShake;

	bool bGotHit = false;
	bool bBloodyScreen = false;
	int mHealth = 100;
	float mTimer = 0.0f;
	float mBlikingTime = 0.1f;
	float mBlikingTimer = 0.0f;
	float mInvincibilityTime = 0.6f;
};