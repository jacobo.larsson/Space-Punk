#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"
#include "../../../Factory/Factory.h" 

#include "../../../GameObject/GameObject.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"

#include "../../../Time/Time.h"
#include "StandardEnemyAttackMachine.h"
#include "StandardEnemyAttackStates.h"

StandardEnemyAttackMachine::StandardEnemyAttackMachine()
{
	mName = "StandardEnemyAttackMachine";

	mInitialState = new StandardEnemyHold("StandardEnemyHold");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new StandardEnemyAttack("StandardEnemyAttack"));
}

StandardEnemyAttackMachine::~StandardEnemyAttackMachine()
{
	StateMachine::~StateMachine();
}

void StandardEnemyAttackMachine::Initialize()
{
	mTransform = mActor->GetComp<TransformComponent>();
	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	mShipToPlayer = pTransform->GetWorldPosition() - mTransform->GetWorldPosition();

	StateMachine::Initialize();
}

void StandardEnemyAttackMachine::Update()
{
	mShipToPlayer = pTransform->GetWorldPosition() - mTransform->GetWorldPosition();

	StateMachine::Update();
}

void StandardEnemyAttackMachine::Shutdown()
{
	StateMachine::Clear();
}

void StandardEnemyAttackMachine::OnCollisionStarted(GameObject* other)
{
	StateMachine::OnCollisionStarted(other);
}
