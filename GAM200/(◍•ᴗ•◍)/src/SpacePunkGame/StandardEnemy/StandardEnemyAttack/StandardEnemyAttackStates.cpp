#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"

#include "../../../GameObject/GameObject.h"
#include "../../../Space/Space.h"
#include "../../../Scene/Scene.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Time/Time.h"
#include "../../../SpacePunkGame/LaserBullet/LaserBullet.h"
#include "../../../Archetype/ArchetypeFunctions.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"

#include "StandardEnemyAttackMachine.h"
#include "StandardEnemyAttackStates.h"

#pragma region StandardEnemyHold
StandardEnemyHold::StandardEnemyHold(const char* name) : State(name) {}

void StandardEnemyHold::Initialize()
{
	castedMachine = dynamic_cast<StandardEnemyAttackMachine*>(mOwnerStateMachine);

	State::Initialize();
}

void StandardEnemyHold::LogicEnter()
{
}

void StandardEnemyHold::LogicExit()
{
}

void StandardEnemyHold::LogicUpdate()
{
	float distanceToPlayer = glm::distance(castedMachine->mTransform->GetWorldPosition(), castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer < castedMachine->attackRange)
		castedMachine->ChangeState("StandardEnemyAttack");
}
#pragma endregion

#pragma region StandardEnemyAttack
StandardEnemyAttack::StandardEnemyAttack(const char* name) : State(name) {}

void StandardEnemyAttack::Initialize()
{
	castedMachine = dynamic_cast<StandardEnemyAttackMachine*>(mOwnerStateMachine);

	State::Initialize();
}

void StandardEnemyAttack::LogicEnter()
{
}

void StandardEnemyAttack::LogicExit()
{
}

void StandardEnemyAttack::LogicUpdate()
{
	float distanceToPlayer = glm::distance(castedMachine->mTransform->GetWorldPosition(), castedMachine->pTransform->GetWorldPosition());

	if (distanceToPlayer > castedMachine->attackRange)
		castedMachine->ChangeState("StandardEnemyHold");

	timeCounter += TimeSys->GetFrameTime();
	if (timeCounter > fireRate)
	{
		GameObject* laserBullet = CreateFromArchetype("StandardEnemyLaserBullet", SceneSys->GetMainSpace(), castedMachine->mTransform->GetPosition());
		timeCounter = 0.0f;

		TransformComponent* laserTransform = laserBullet->GetComp<TransformComponent>();
		laserTransform->SetWorldOrientation(castedMachine->mTransform->GetWorldOrientation() + 3.141590f / 2);
	}
}
#pragma endregion