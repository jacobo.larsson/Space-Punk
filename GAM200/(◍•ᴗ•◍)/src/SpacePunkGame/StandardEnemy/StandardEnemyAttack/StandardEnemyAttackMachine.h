#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"

class TransformComponent;

class StandardEnemyAttackMachine : public StateMachine
{
	SM_RTTI_DECL(StandardEnemyAttackMachine, StateMachine);

public:
	StandardEnemyAttackMachine();
	~StandardEnemyAttackMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	//bool Edit()override;
	//StandardEnemyAttackMachine& operator=(const PlayerShipAttackMachine& rhs);
	//StandardEnemyAttackMachine* Clone()override;
	//
	//void ToJson(nlohmann::json& j) override;
	//void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;

	TransformComponent* pTransform;
	TransformComponent* mTransform;
	glm::vec2 mShipToPlayer;
	float attackRange = 1700.0f;
};