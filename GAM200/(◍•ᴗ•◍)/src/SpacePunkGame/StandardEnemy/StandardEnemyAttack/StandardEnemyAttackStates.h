#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../../Transform2D/TransformComponent.h"

class StandardEnemyAttackMachine;

class StandardEnemyHold : public State
{
public:
	StandardEnemyHold(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	StandardEnemyAttackMachine* castedMachine;
};

class StandardEnemyAttack : public State
{
public:
	StandardEnemyAttack(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	StandardEnemyAttackMachine* castedMachine;
	float timeCounter;
	float fireRate = 2.0f;
};