#pragma once
#include <glm/glm.hpp>
#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Space/Space.h"
#include "../../../Scene/Scene.h"
#include "../../../Time/Time.h"
#include "../../../Settings/Settings.h"

class StandardEnemyLaserBulletComp : public LogicComponent
{
	SM_RTTI_DECL(StandardEnemyLaserBulletComp, LogicComponent);

public:
	StandardEnemyLaserBulletComp();
	StandardEnemyLaserBulletComp(StandardEnemyLaserBulletComp& rhs);
	StandardEnemyLaserBulletComp& operator=(StandardEnemyLaserBulletComp& rhs);
	StandardEnemyLaserBulletComp* Clone()override;
	void Initialize()override;
	void Update() override;
	void Shutdown()override;
	bool Edit()override;
	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;
	void OnCollisionStarted(GameObject* other)override;

private:
	TransformComponent* pTransform;
	TransformComponent* mTransform;
	glm::vec3 mDirection;
	float mTimer = 0.0f;
	float mBulletSpeed = 1000.0f;
	float mDestructionTimer = 0.0f;
	float mDestructionTime = 2.0f;
};