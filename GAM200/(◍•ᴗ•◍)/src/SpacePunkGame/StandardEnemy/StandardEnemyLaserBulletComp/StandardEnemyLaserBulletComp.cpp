#include <cmath>
#include "StandardEnemyLaserBulletComp.h"

StandardEnemyLaserBulletComp::StandardEnemyLaserBulletComp()
{
	mName = "StandardEnemyLaserBulletComp";
}

StandardEnemyLaserBulletComp::StandardEnemyLaserBulletComp(StandardEnemyLaserBulletComp& rhs)
{
	mName = rhs.mName;
	mEnabled = rhs.mEnabled;
}

StandardEnemyLaserBulletComp& StandardEnemyLaserBulletComp::operator=(StandardEnemyLaserBulletComp& rhs)
{
	mDirection = rhs.mDirection;
	mBulletSpeed = rhs.mBulletSpeed;
	return (*this);
}

StandardEnemyLaserBulletComp* StandardEnemyLaserBulletComp::Clone()
{
	StandardEnemyLaserBulletComp* temp = FactorySys->Create<StandardEnemyLaserBulletComp>();
	(*temp) = (*this);
	return temp;
}

void StandardEnemyLaserBulletComp::Initialize()
{
	mTransform = mOwner->GetComp<TransformComponent>();

	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();

	mDirection.x = cos(atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x));
	mDirection.y = sin(atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x));
	glm::normalize(mDirection);

	LogicComponent::Initialize();
}

void StandardEnemyLaserBulletComp::Update()
{
	mTimer += TimeSys->GetFrameTime();

	glm::vec3 pos;
	pos.x = mTransform->GetPosition().x + mDirection.x * mBulletSpeed * TimeSys->GetFrameTime();
	pos.y = mTransform->GetPosition().y + mDirection.y * mBulletSpeed * TimeSys->GetFrameTime();
	pos.z = mTransform->GetPosition().z;

	mTransform->SetWorldPosition(pos);

	mDestructionTimer += TimeSys->GetFrameTime();

	if (mDestructionTimer > mDestructionTime)
		SceneSys->GetMainSpace()->DestroyObject(mOwner);
}

void StandardEnemyLaserBulletComp::Shutdown()
{
	LogicComponent::Shutdown();
}

bool StandardEnemyLaserBulletComp::Edit()
{
	bool changed = false;
	ImGui::PushID(&mDestructionTime);
	ImGui::DragFloat("Destruction Time", &mDestructionTime);
	ImGui::PopID();

	if (ImGui::IsItemDeactivatedAfterEdit())
		changed = true;

	return changed;
}

void StandardEnemyLaserBulletComp::ToJson(nlohmann::json& j)
{
	j["mDestructionTime"] = mDestructionTime;
}

void StandardEnemyLaserBulletComp::FromJson(const nlohmann::json& j)
{
	if (j.find("mDestructionTime") != j.end())
		mDestructionTime = j["mDestructionTime"];
}

void StandardEnemyLaserBulletComp::OnCollisionStarted(GameObject* other)
{
}
