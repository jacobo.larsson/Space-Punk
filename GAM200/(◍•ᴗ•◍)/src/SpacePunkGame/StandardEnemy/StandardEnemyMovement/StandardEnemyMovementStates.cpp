#pragma once
#include <iostream>

#include <glm/glm.hpp>

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"

#include "../../../GameObject/GameObject.h"

#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../StateMachines/BrainComponent/BrainComponent.h"
#include "../../../Time/Time.h"

#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../RayCast/Raycast.h"

#include "StandardEnemyMovementMachine.h"
#include "StandardEnemyMovementStates.h"

#pragma region StandardEnemyIdle
StandardEnemyIdle::StandardEnemyIdle(const char* name) : State(name) {}

void StandardEnemyIdle::Initialize()
{
	castedMachine = dynamic_cast<StandardEnemyMovementMachine*>(mOwnerStateMachine);
	
	if (mActor->HasComp<TransformComponent>())
		mTransform = mActor->GetComp<TransformComponent>();

	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();
	
	State::Initialize();
}

void StandardEnemyIdle::LogicEnter()
{
	mTimeInState = 0.0f;
}

void StandardEnemyIdle::LogicExit()
{
}

void StandardEnemyIdle::LogicUpdate()
{
	mTimeInState += TimeSys->GetFrameTime();

	float angle = atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x);

	mTransform->SetWorldOrientation(angle);

	auto& vel = castedMachine->mRigidbody->Velocity;
	float maxVel = castedMachine->mMaxVelocity;
	
	if (vel.length() >= maxVel) {
		castedMachine->mDirection = glm::normalize(vel);
		vel = castedMachine->mDirection * maxVel;
	}

	if (mTimeInState > mTimeInIdle)
		castedMachine->ChangeState("StandardEnemyMoveRand");
}
#pragma endregion


#pragma region StandardEnemyMoveRand
StandardEnemyMoveRand::StandardEnemyMoveRand(const char* name) : State(name) {}

void StandardEnemyMoveRand::Initialize()
{
	castedMachine = dynamic_cast<StandardEnemyMovementMachine*>(mOwnerStateMachine);

	if (mActor->HasComp<TransformComponent>())
		mTransform = mActor->GetComp<TransformComponent>();

	pTransform = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<TransformComponent>();

	State::Initialize();
}

void StandardEnemyMoveRand::LogicEnter()
{
	mTimeInState = 0;
	mMovementTime = (float)((rand() % 4) + 2);

	castedMachine->mDirection.x = rand() / double(RAND_MAX);
	castedMachine->mDirection.y = rand() / double(RAND_MAX);

	if (rand() % 2 == 0)
		castedMachine->mDirection = -castedMachine->mDirection;

	glm::normalize(castedMachine->mDirection);

	mPos = mTransform->GetWorldPosition();
	angle = atan2f(castedMachine->mDirection.y, castedMachine->mDirection.x);
}

void StandardEnemyMoveRand::LogicExit()
{
}

void StandardEnemyMoveRand::LogicUpdate()
{
	mTimeInState += (float)TimeSys->GetFrameTime();
	if (mTimeInState > mMovementTime)
		castedMachine->ChangeState("StandardEnemyIdle");

	auto& vel = castedMachine->mRigidbody->Velocity;
	float maxVel = castedMachine->mMaxVelocity;
	
	vel += castedMachine->mDirection * castedMachine->mAcceleration * (float)TimeSys->GetFrameTime();
 	
	mPos = mTransform->GetWorldPosition();
	
	if (vel.length() >= maxVel) {
		castedMachine->mDirection = glm::normalize(vel);
		vel = castedMachine->mDirection * maxVel;
	}
	
	angle = atan2f(pTransform->GetWorldPosition().y - mTransform->GetWorldPosition().y, pTransform->GetWorldPosition().x - mTransform->GetWorldPosition().x);
	
	mTransform->SetWorldOrientation(angle);
}
#pragma endregion
