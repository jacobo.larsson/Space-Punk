#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../Physics/RigidBody.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../../Transform2D/TransformComponent.h"

class StandardEnemyMovementMachine;

class StandardEnemyIdle : public State
{
public:
	StandardEnemyIdle(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* mTransform;
	TransformComponent* pTransform;
	StandardEnemyMovementMachine* castedMachine;
	float mTimeInIdle = 3.0f;
};

class StandardEnemyMoveRand : public State
{
public:
	StandardEnemyMoveRand(const char* name = nullptr);

	void Initialize() override;
	void LogicEnter() override;
	void LogicExit();
	void LogicUpdate();

	TransformComponent* mTransform;
	TransformComponent* pTransform;
	StandardEnemyMovementMachine* castedMachine;
	float mMovementTime = 0;
	glm::vec3 mPos;
	float angle = 0;
};

//class PlayerShipDash : public State
//{
//public:
//	PlayerShipDash(const char* name = nullptr);
//
//	void Initialize() override;
//	void LogicEnter() override;
//	void LogicExit();
//	void LogicUpdate();
//	void OnCollisionStarted(GameObject* other) override;
//
//	TransformComponent* mTransform;
//	PlayerShipMovementMachine* castedMachine;
//};