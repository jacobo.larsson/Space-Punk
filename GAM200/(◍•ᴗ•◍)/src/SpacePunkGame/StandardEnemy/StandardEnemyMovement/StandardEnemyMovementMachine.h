#pragma once
#include "../../../RTTI/IBase.h"
#include "../../../RTTI/SM_RTTI.h"
#include "../../../LogicSystem/LogicComponent.h"
#include "../../../StateMachines/StateMachine/StateMachine.h"
#include "../../../StateMachines/SuperState/SuperState.h"
#include "../../WaveComponent/WaveManager.h"

class Collider;
class RigidBody;
class TransformComponent;


class StandardEnemyMovementMachine : public StateMachine
{
	SM_RTTI_DECL(StandardEnemyMovementMachine, StateMachine);

public:
	StandardEnemyMovementMachine();
	~StandardEnemyMovementMachine();

	void Initialize()override;
	void Update()override;
	void Shutdown()override;
	bool Edit()override;
	StandardEnemyMovementMachine& operator=(const StandardEnemyMovementMachine& rhs);
	StandardEnemyMovementMachine* Clone()override;

	void ToJson(nlohmann::json& j) override;
	void FromJson(const nlohmann::json& j) override;

	void OnCollisionStarted(GameObject* other) override;
	void OnCollisionEnded(GameObject* other) override;

	float mMaxVelocity = 1000.0f;
	float mCurrentVelocity = 0.0f;
	float mRotatingVelocity = 3.0f;
	float mAcceleration = 750.0f;
	glm::vec2 mDirection;

	TransformComponent* mTransform;
	RigidBody* mRigidbody;
	WaveManager* pWaveManager;
};
