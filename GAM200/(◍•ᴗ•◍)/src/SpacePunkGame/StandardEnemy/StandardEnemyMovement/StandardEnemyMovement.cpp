#pragma once
#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include "../../../Extern/ImGui/imgui.h"
#include "../../../Extern/ImGui/imgui_impl_sdl.h"
#include "../../../Extern/ImGui/imgui_impl_opengl3.h"

#include "../../../Input/Input.h" 
#include "../../../Game/GameInput/GameInput.h"
#include "../../../Factory/Factory.h" 

#include "../../../GameObject/GameObject.h"
#include "../../../Scene/Scene.h"
#include "../../../Space/Space.h"
#include "../../../Physics/RigidBody.h"
#include "../../../Transform2D/TransformComponent.h"
#include "../../../Time/Time.h"
#include "../../../Collisions/ICollider.h"

#include "StandardEnemyMovementMachine.h"
#include "StandardEnemyMovementStates.h"

StandardEnemyMovementMachine::StandardEnemyMovementMachine()
{
	mName = "StandardEnemyMovementMachine";

	mInitialState = new StandardEnemyIdle("StandardEnemyIdle");
	mCurrentState = mInitialState;
	
	AddState(mInitialState);
	AddState(new StandardEnemyMoveRand("StandardEnemyMoveRand"));
}

StandardEnemyMovementMachine::~StandardEnemyMovementMachine()
{
	StateMachine::~StateMachine();
}

void StandardEnemyMovementMachine::Initialize()
{
	StateMachine::Initialize();

	mTransform = mActor->GetComp<TransformComponent>();
	mRigidbody = mActor->GetComp<RigidBody>();

	pWaveManager = SceneSys->GetMainSpace()->FindObjectByName("Player")->GetComp<WaveManager>();
}

void StandardEnemyMovementMachine::Update()
{
	StateMachine::Update();
}

void StandardEnemyMovementMachine::Shutdown()
{
	StateMachine::Clear();
}

bool StandardEnemyMovementMachine::Edit()
{
	//if (ImGui::Begin(mName.c_str(), nullptr, 0))
	//{
	//	ImGui::DragFloat("Max X velocity", &MaxXVelocity);
	//	ImGui::DragFloat("Max Y velocity", &yMaxVelocity);
	//	ImGui::DragFloat("Acceleration", &Acceleration);
	//	ImGui::DragFloat("Max X velocity", &MaxXVelocity);
	//	ImGui::End();
	//}
	//else
	//{
	//	ImGui::End();
	//}
	return false;
}

StandardEnemyMovementMachine& StandardEnemyMovementMachine::operator=(const StandardEnemyMovementMachine& rhs)
{
	//MaxXVelocity = rhs.MaxXVelocity;
	//CurrentVelocity = rhs.CurrentVelocity;
	//yVelocity = rhs.yVelocity;
	//yMaxVelocity = rhs.yMaxVelocity;
	//Acceleration = rhs.Acceleration;
	//
	//FacingRight = rhs.FacingRight;
	//PreviousFacingRight = rhs.PreviousFacingRight;
	//canJump = rhs.canJump;

	return *this;
}

StandardEnemyMovementMachine* StandardEnemyMovementMachine::Clone()
{
	StandardEnemyMovementMachine* temp = FactorySys->Create<StandardEnemyMovementMachine>();
	*temp = *this;
	return temp;
}

void StandardEnemyMovementMachine::ToJson(nlohmann::json& j)
{
	IBase::ToJson(j);
}

void StandardEnemyMovementMachine::FromJson(const nlohmann::json& j)
{
	IBase::FromJson(j);
}

void StandardEnemyMovementMachine::OnCollisionStarted(GameObject* other)
{
	if (other->HasTag("LaserBullet") || other->HasTag("Enemy"))
	{
		if(mActor->Enabled())
			pWaveManager->mKillCount++;

		mActor->SetEnabled(false);
	}

	StateMachine::OnCollisionStarted(other);
}

void StandardEnemyMovementMachine::OnCollisionEnded(GameObject* other)
{
	StateMachine::OnCollisionEnded(other);
}