#pragma once
#include "../LogicSystem/LogicComponent.h"

class Health;
class SkeletonRenderable;
class GameObject;

class TutorialDummy : public LogicComponent
{
	SM_RTTI_DECL(TutorialDummy, LogicComponent);
public:
	TutorialDummy();

	void Initialize() override;
	void Update() override;
	void Shutdown()override;
	
	void OnCollisionStarted(GameObject* other)override;
	void OnCollisionEnded(GameObject* other)override;

private:
	SkeletonRenderable* mRenderable = nullptr;
	GameObject* mLock = nullptr;
	Health* mHealth = nullptr;
	bool dead = false;
	float alpha = 0.5f;
};