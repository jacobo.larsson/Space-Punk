#pragma once
#include "../GameObject/GameObject.h"
#include "../Scene/Scene.h"
#include "../Space/Space.h"
#include "TutorialDummy.h"
#include "../Animation/SM_Animation.h"
#include "../Game/Health/Health.h"
#include "../Collisions/ICollider.h"
#include "../Graphics/Particle System/ParticleSystem.h"
#include "../Graphics/Renderable/Renderable.h"
#include "../Game/Hitbox/Hitbox.h"

TutorialDummy::TutorialDummy()
{ 
	SetName("TutorialDummy");
}

void TutorialDummy::Initialize()
{
	mRenderable = mOwner->GetComp<SkeletonRenderable>();
	mRenderable->SetFlipX(true);
	mHealth = mOwner->GetComp<Health>();

	if (mRenderable->GetCurrentAnimationName() != "Nero_Idle")
		mRenderable->SetCurrentAnimation("Nero_Idle");

	mRenderable->SetTranslucent(true);
	mRenderable->SetModulationColor(Color(1.0f, 1.0f, 1.0f, 0.5f));

	LogicComponent::Initialize();
}

void TutorialDummy::Update()
{
	if (dead && alpha > 0.0f)
	{
		alpha -= 0.01;
		mRenderable->SetModulationColor(Color(1.0f, 0.0f, 0.0f, alpha));
	}
}

void TutorialDummy::Shutdown()
{
	LogicComponent::Shutdown();
}

void TutorialDummy::OnCollisionStarted(GameObject* other)
{
	if (dead)
		return;

	if (other->CompareTag("Hitbox") && other->HasComp<Hitbox>())
	{
		if (mRenderable->GetCurrentAnimationName() != "Nero_Damaged")
			mRenderable->SetCurrentAnimation("Nero_Damaged");

			mRenderable->SetTranslucent(true);
			mRenderable->SetModulationColor(Color(1.0f, 0.0f, 0.0f, 0.5f));

		mHealth->Damage(other->GetComp<Hitbox>()->mDamage);
	}

	if (mHealth->GetHealth() <= 0.0f)
	{
		dead = true;

		if (auto part = mOwner->GetComp<ParticleEmitter>())
			part->Load("./data/Particles/ExplosionDummy.json");

		auto _Lock = SceneSys->GetMainSpace()->FindObjectByName("DummyTrigger");
		_Lock->GetParentSpace()->DestroyObject(_Lock);
	}
}		

void TutorialDummy::OnCollisionEnded(GameObject* other)
{
	if (dead)
		return;
	if (mOwner->GetName() == "DummyTrigger")
		return;

	if (other->CompareTag("Hitbox") && other->HasComp<Hitbox>())
	{
		if (mRenderable->GetCurrentAnimationName() != "Nero_Idle")
			mRenderable->SetCurrentAnimation("Nero_Idle");

		mRenderable->SetTranslucent(true);
		mRenderable->SetModulationColor(Color(1.0f, 1.0f, 1.0f, 0.5f));
	}
}
