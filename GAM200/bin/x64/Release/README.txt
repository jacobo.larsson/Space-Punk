﻿/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2021 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
Project: gam250_weeklybuild_2
Author: AvocadoGames
Creation date: 02/22/2021
----------------------------------------------------------------------------------------------------------*/
•	Player controls with keyboard:
	WADS: Move the player
	Space: Jump and double jump
	Left click: Light attack
	Right click: Heavy attack
	LShift: Dash
	Q: Blaster
	E: Purification

•	Player controls with gamepad(not fully implemented):
	Left joystick: Movement
	A button: Jump and double jump
	X button: Light attack
	Y button: Heavy attack
	B button: Dash
	Left low trigger: Blaster
	Right low trigger: Purification

•	Purification:
	So as to be triggered, the player must be colliding with the enemy that activated that state.
	There will be some icons that look like joystick positions but are triggered with the d-pad.
•	Editor:
	Press LeftCtrl+F8 to open the editor.

IMPORTANT: 
Blaster only works to destroy some objects.

What's new?
- New flying enemy
- New flying enemy ticketing system
- New basic enemy ticketing system
- New gas enemy
- New gas enemy ticketing system
- New hoverboard respawn
- New hoverboard feedback colliding with walls.
- New forgiveness time in hoverboard.
- More feedback in hoverboard.
- New parallax y movement
- Dialogues with gamepad
- New dummy
- New player double jump
- New player attack in the air
- New player dash attack
- Checkpoints now restore life
- New spline component
- Dialogues now complete upon pressing dialog button
- Instanced rendering

If the 	(◍•ᴗ•◍) project does not compile it might be because of Spine. Simply unload (◍•ᴗ•◍) and compile the spine projects. 
They will by default put the output in the correct folder to compile. It must also be mentioned that those .lib and .dll files must be put in the .exe folder to execute the project outside visual studio.

Unzip data.zip and start the executable.

The project can only be build in x64.
