/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2021 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
Project: 
Author: Jacobo Larsson Liñares
Creation date: 04/12/2021
----------------------------------------------------------------------------------------------------------*/
CONTROLS:
	A and D: rotate the player
	SPACE: shoot bullets
	SHIFT: Dash

NEW FEATURES AND NOTES
	-I started implementing the evading mechanic for the enemies we talked about the other day in class.
	Now enemies with this component will detect if a game object is nearby, and will add it to a list,
	and then removed from that list if it exits the detection zone.
	-I changed the behabiour of the asteroids, now they brake in smaller parts in every collision except
 	for the player's one, and destroy enemies if it collides with them, between other new features.
	-I also fixed a bug that made the asteroids' collisions brake in determined cases.
	-I started implementing the standard enemy's attack
	-I fixed numerous small bugs
	-IMPORTANT: the program crashes if space partitioning is not enabled. To able/diable it, click on 
	settings menu, in the upper part of the screen in the editor, and then click open settings.
	Sorry for the inconvenience.